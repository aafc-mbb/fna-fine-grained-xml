<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="Reveal" date="1985" rank="variety">juniporinum</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>45: 279. 1985</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety juniporinum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060557</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, mostly spreading to erect, 4–8 × 5–10 dm.</text>
      <biological_entity id="o25702" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="mostly spreading" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="4" from_unit="dm" name="length" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="mostly spreading" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="4" from_unit="dm" name="length" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, 1–2.5 dm, floccose or glabrous, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o25704" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="2.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25705" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o25704" id="r2854" name="without" negation="false" src="d0_s1" to="o25705" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in loose rosettes;</text>
      <biological_entity id="o25706" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25707" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o25706" id="r2855" name="in" negation="false" src="d0_s2" to="o25707" />
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic, (0.7–) 1–2 × (0.3–) 0.5–1 (–1.2) cm, floccose or glabrous on both surfaces, margins plane.</text>
      <biological_entity id="o25708" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_length" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_width" src="d0_s3" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
        <character constraint="on surfaces" constraintid="o25709" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25709" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o25710" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences compound-umbellate, branched 2–5 times;</text>
      <biological_entity id="o25711" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound-umbellate" value_original="compound-umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="" src="d0_s4" value="2-5 times" value_original="2-5 times " />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches floccose or glabrous, without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o25712" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25713" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o25714" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o25712" id="r2856" name="without" negation="false" src="d0_s5" to="o25713" />
      <relation from="o25713" id="r2857" name="part_of" negation="false" src="d0_s5" to="o25714" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes (2.5–) 3–3.5 mm, lobes 1–2.5 mm.</text>
      <biological_entity id="o25715" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25716" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers (4–) 5–6 mm;</text>
      <biological_entity id="o25717" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth cream, whitish, or pale-yellow to greenish yellow, without large reddish spot on midrib.</text>
      <biological_entity id="o25718" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="cream" value_original="cream" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s8" to="greenish yellow" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s8" to="greenish yellow" />
      </biological_entity>
      <biological_entity id="o25719" name="midrib" name_original="midrib" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="large" value_original="large" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish spot" value_original="reddish spot" />
      </biological_entity>
      <relation from="o25718" id="r2858" name="without" negation="false" src="d0_s8" to="o25719" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, saltbush and sagebrush communities, pinyon-juniper and occasionally montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="montane conifer woodlands" modifier="occasionally" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300-2300(-2500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2500" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107hh.</number>
  <other_name type="common_name">Juniper sulphur flower</other_name>
  <discussion>Variety juniporinum is widespread and infrequent in widely scattered and disjunct populations in isolated desert mountain ranges from southern Utah (Navajo Mountains, San Juan County and Beaverdam Mountains, Washington County) and north-central Arizona (Kaibab Plateau, Coconino County) westward across southern Nevada (Spring Range of Clark County, Mt. Irish and Mormon Range in Lincoln County, and Schell Creek and Snake ranges in White Pine County) to California (southeastern Inyo and northeastern San Bernardino counties, mainly Clark Mountain, Kingston, New York, and Providence ranges, and in the Mid Hills). A collection supposedly from the San Bernardino Mountains of California (Meebold 20381, M) surely is mislabeled.</discussion>
  <discussion>Variety juniporinum is most closely related to var. subaridum, but immature plants (or specimens without habit information) may be confused with var. versicolor. These plants most frequently are seen in San Bernardino County, California, and in White Pine County, Nevada. They would make an attractive addition to the garden.</discussion>
  
</bio:treatment>