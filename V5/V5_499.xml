<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="mention_page">236</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="mention_page">257</other_info_on_meta>
    <other_info_on_meta type="mention_page">269</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">308</other_info_on_meta>
    <other_info_on_meta type="illustration_page">243</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">brevicaule</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 15. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species brevicaule;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060201</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">campanulatum</taxon_name>
    <taxon_name authority="(Nuttall) S. Stokes" date="unknown" rank="subspecies">brevicaule</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species campanulatum;subspecies brevicaule;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, matted, cespitose, pulvinate, erect or spreading, sometimes scapose, (0.3–) 1–5 × 1–5 (–8) dm, tomentose to floccose or glabrous, grayish or greenish to green.</text>
      <biological_entity id="o23509" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="shape" src="d0_s0" value="pulvinate" value_original="pulvinate" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_length" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s0" to="floccose or glabrous" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s0" to="green" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems matted to spreading, occasionally with persistent leaf-bases, up to 1/4 or more height of plant;</text>
      <biological_entity id="o23510" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="matted" value_original="matted" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o23511" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="1/4" />
      </biological_entity>
      <biological_entity id="o23512" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <relation from="o23510" id="r2615" modifier="occasionally" name="with" negation="false" src="d0_s1" to="o23511" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems matted or spreading;</text>
      <biological_entity constraint="caudex" id="o23513" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems spreading to erect or nearly so, slender, rarely stout, solid, not fistulose, (0.4–) 0.5–2 (–2.5) dm, glabrous, floccose, or sparsely to densely tomentose to lanate.</text>
      <biological_entity id="o23514" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="erect" />
        <character name="orientation" src="d0_s3" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="rarely" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="0.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="floccose or" name="pubescence" src="d0_s3" to="sparsely densely tomentose" />
        <character char_type="range_value" from="floccose or" name="pubescence" src="d0_s3" to="sparsely densely tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal or more commonly sheathing 1–7 (–15) cm up stem, 1 per node;</text>
      <biological_entity id="o23515" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="commonly" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" constraint="up stem" constraintid="o23516" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23516" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o23517" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o23517" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.2–2 (–4) cm, tomentose to floccose;</text>
      <biological_entity id="o23518" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s5" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade linear, oblanceolate, or spatulate to elliptic, (0.2–) 1–10 (–12) × 0.1–0.9 (–1.2) cm, densely tomentose abaxially, less so to floccoseadaxially, margins plane or revolute, sometimes crenulate.</text>
      <biological_entity id="o23519" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s6" to="elliptic" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s6" to="elliptic" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_length" src="d0_s6" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s6" to="0.9" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o23520" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="less; floccoseadaxially" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose, subumbellate, umbellate, or capitate, (1–) 3–10 (–25) × (0.7–) 1–10 (–15) cm;</text>
      <biological_entity id="o23521" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subumbellate" value_original="subumbellate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s7" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="25" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_width" src="d0_s7" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, sometimes absent, tomentose to floccose or glabrous;</text>
      <biological_entity id="o23522" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s8" to="floccose or glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, triangular, scalelike, 1–3 (–5) mm.</text>
      <biological_entity id="o23523" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent or erect, 0.3–3 cm, tomentose to floccose or glabrous.</text>
      <biological_entity id="o23524" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s10" to="floccose or glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node or 3–7 (–9) per cluster, turbinate to turbinate-campanulate, 1.5–4 (–5) × (1–) 1.5–3 (–3.5) mm, tomentose to floccose or glabrous;</text>
      <biological_entity id="o23525" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o23526" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="9" />
        <character char_type="range_value" constraint="per cluster" from="3" name="quantity" src="d0_s11" to="7" />
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s11" to="turbinate-campanulate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s11" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s11" to="floccose or glabrous" />
      </biological_entity>
      <biological_entity id="o23526" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect to spreading, 0.3–1 mm.</text>
      <biological_entity id="o23527" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s12" to="spreading" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers (1–) 2–4 mm;</text>
      <biological_entity id="o23528" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth various shades of white to cream or yellow, glabrous or pubescent;</text>
      <biological_entity id="o23529" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="variability" src="d0_s14" value="various" value_original="various" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="cream or yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4–1/3, monomorphic, lanceolate, oblong to obovate or ovate to oval;</text>
      <biological_entity id="o23530" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s15" to="1/3" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="obovate or ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 2–4 mm;</text>
      <biological_entity id="o23531" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose basally.</text>
      <biological_entity id="o23532" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown to brown, 2–3 mm, glabrous except for roughened to papillate beak.</text>
      <biological_entity id="o23533" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s18" to="brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
        <character constraint="except-for beak" constraintid="o23534" is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23534" name="beak" name_original="beak" src="d0_s18" type="structure">
        <character char_type="range_value" from="roughened" is_modifier="true" name="relief" src="d0_s18" to="papillate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., Nebr., Nev., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <other_name type="past_name">brevicaulis</other_name>
  <discussion>Varieties 8 (8 in the flora).</discussion>
  <discussion>Eriogonum brevicaule is highly variable, and the variation has yet to be fully resolved taxonomically. The expressions recognized here will encompass the vast majority of populations. The extreme variation previously under the name var. laxifolium is now reduced with the recognition of var. bannockense (low-elevation or northern phase), var. nanum, and var. caelitum (high-elevation, southern phases).</discussion>
  <discussion>Essentially all of the following species (28–63 below) belong to the Eriogonum brevicaule complex. Eriogonum desertorum, E. loganum, E. spathulatum, E. ostlundii, and E. artificis are allied to the complex associated with var. laxifolium, while E. natum is related to var. cottamii. Eriogonum viridulum and E. ephedroides are allied to E. brevicaule var. brevicaule as are E. contortum and E. acaule. Eriogonum brandegeei is also related, but exactly how is less certain. Allied to this complex of species are on the one hand those related to E. batemanii, and on the other all of the matted perennials belonging to the E. ochrocephalum complex. Essentially all of these species form relatively small populations on discrete edaphic sites and are well isolated one from the other. Unfortunately, a clear separation of E. brevicaule from E. desertorum, E. loganum, and E. spathulatum is not always possible.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering stems and inflorescence branches glabrous</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering stems and inflorescence branches floccose to tomentose or lanate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants not pulvinate or cespitose, (0.8-)1.5-5 dm; inflorescences cymose, open, divided 3 times or more; widespread</description>
      <determination>27a Eriogonum brevicaule var. brevicaule</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants pulvinate and cespitose, 0.3-1.5(-1.8) dm; inflorescences capitate or umbellate to cymose and divided 1-2 times; Utah</description>
      <determination>27g Eriogonum brevicaule var. nanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perianths pubescent; se Montana, ne Wyoming</description>
      <determination>27b Eriogonum brevicaule var. canum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perianths glabrous; sc Idaho, ne Nevada, Utah, sw Wyoming, not se Montana or ne Wyoming</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Inflorescences divided (2-)3-5 times; perianths usually yellow, rarely ochroleucous</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Inflorescences capitate or umbellate to cymose and divided 1-2 times; perianths ochroleucous or yellow</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers (1-)1.5-2.5 mm; sw Wyoming</description>
      <determination>27c Eriogonum brevicaule var. micranthum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers (2.5-)3-4 mm; Utah</description>
      <determination>27d Eriogonum brevicaule var. cottamii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades linear to narrowly oblanceolate; inflorescences capitate or divided</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades linear, oblanceolate, or narrowly oblanceolate to elliptic; inflorescences capitate. [8. Shifted to left margin.—Ed.]</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades (1.5-)3-9(-12) × 0.1-0.5(-0.7) cm, tomentose abaxially, less so and grayish or occasionally greenish adaxially, margins usually revolute, occasionally plane; perianths ochroleucous or yellow; n Utah, se Idaho, below 2800 m</description>
      <determination>27e Eriogonum brevicaule var. laxifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades 0.2-4.5(-5) × (0.2-)0.3-0.6(-0.7) cm, tomentose abaxially, thinly floccose and bright green adaxially, margins plane or slightly thickened; perianths yellow; c Utah, above 2700 m</description>
      <determination>27f Eriogonum brevicaule var. caelitum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades and flowering stems bright green under the thinly floccose pubescence; ne Nevada and nw Utah</description>
      <determination>27e Eriogonum brevicaule var. laxifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades and flowering stems usually grayish to dull greenish under tomentum, rarely thinly floccose; se Idaho and sw Wyoming s to n Utah and ne Nevada</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf margins crenulate; leaf blades narrowly oblanceolate to narrowly elliptic, (0.3-)0.5-1.5(-2) × 0.2-0.5(-7) cm, densely white-tomentose abaxially, floccose and greenish adaxially</description>
      <determination>27g Eriogonum brevicaule var. nanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf margins usually plane (rarely crenulate in Wyoming); leaf blades narrowly oblanceolate to oblanceolate, (0.8-)1-4(-4.5) × (0.3-)0.4-0.8 cm, densely tomentose abaxially, tomentose to floccose and grayish to greenish adaxially</description>
      <determination>27h Eriogonum brevicaule var. bannockense</determination>
    </key_statement>
  </key>
</bio:treatment>