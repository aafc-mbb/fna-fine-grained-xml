<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Torrey ex Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">lachnogynum</taxon_name>
    <taxon_name authority="Reveal &amp; A. Clifford" date="2004" rank="variety">colobum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>86: 169. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species lachnogynum;variety colobum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060356</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, flattened and matlike, 0.05–0.2 × 0.5–3.5 dm.</text>
      <biological_entity id="o9656" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="shape" src="d0_s0" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matlike" value_original="matlike" />
        <character char_type="range_value" from="0.05" from_unit="dm" name="length" src="d0_s0" to="0.2" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="3.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems (0.01–) 0.02–0.05 (–0.12) dm, silky-tomentose.</text>
      <biological_entity id="o9657" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="0.01" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="0.02" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.05" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="0.12" to_unit="dm" />
        <character char_type="range_value" from="0.02" from_unit="dm" name="some_measurement" src="d0_s1" to="0.05" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="silky-tomentose" value_original="silky-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 0.1–0.3 cm;</text>
      <biological_entity id="o9658" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o9659" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s2" to="0.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly elliptic to oblanceolate, 0.4–0.8 (–1.2) × 0.1–0.3 (–0.35) cm.</text>
      <biological_entity id="o9660" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9661" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s3" to="oblanceolate" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s3" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.35" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="0.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences capitate, 0.3–0.85 cm;</text>
      <biological_entity id="o9662" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s4" to="0.85" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches absent.</text>
      <biological_entity id="o9663" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 1 per node, 2–3.5 mm.</text>
      <biological_entity id="o9664" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o9665" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9665" name="node" name_original="node" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3–4.5 mm.</text>
      <biological_entity id="o9666" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky limestone flats and slopes, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky limestone flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>69c.</number>
  <other_name type="common_name">Clipped wild buckwheat</other_name>
  <discussion>Variety colobum occurs on a low, windswept, limestone ridgetop north and east of Thoreau, McKinley County. It also occurs to the northeast on gravelly soil on the western edge of the Rio Grande Gorge in Taos County. The scapes and inflorescences are buried among the leaves, the flowers spotting the tops of the mats with specks of yellow. The variety is worthy of cultivation.</discussion>
  
</bio:treatment>