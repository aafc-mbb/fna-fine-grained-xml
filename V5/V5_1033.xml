<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">504</other_info_on_meta>
    <other_info_on_meta type="mention_page">491</other_info_on_meta>
    <other_info_on_meta type="mention_page">505</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Field Mus. Nat. Hist., Bot. Ser.</publication_title>
      <place_in_publication>17: 6. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not developing basal rosette of leaves, sometimes with long-creeping rhizomes.</text>
      <biological_entity id="o25344" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="basal" id="o25345" name="rosette" name_original="rosette" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="not" name="development" src="d0_s0" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity id="o25346" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o25347" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="long-creeping" value_original="long-creeping" />
      </biological_entity>
      <relation from="o25345" id="r2816" name="consist_of" negation="false" src="d0_s0" to="o25346" />
      <relation from="o25345" id="r2817" modifier="sometimes" name="with" negation="false" src="d0_s0" to="o25347" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, ascending, procumbent, or decumbent, normally with regular, leafy axillary shoots that tend to develop 2d-order axillary inflorescences (often overtopping 1st-order ones).</text>
      <biological_entity id="o25348" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o25349" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="regular" value_original="regular" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o25350" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <relation from="o25348" id="r2818" modifier="normally" name="with" negation="false" src="d0_s1" to="o25349" />
      <relation from="o25349" id="r2819" name="tend to" negation="false" src="d0_s1" to="o25350" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades mostly lanceolate, elliptic, ovatelanceolate, or ovate-elliptic, base cuneate, or in some species broadly cuneate, rounded, truncate-cuneate, or indistinctly cordate.</text>
      <biological_entity id="o25351" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
      </biological_entity>
      <biological_entity id="o25352" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate-cuneate" value_original="truncate-cuneate" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate-cuneate" value_original="truncate-cuneate" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o25353" name="species" name_original="species" src="d0_s2" type="taxon_name">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <relation from="o25352" id="r2820" name="in" negation="false" src="d0_s2" to="o25353" />
    </statement>
    <statement id="d0_s3">
      <text>Inner tepals with margins entire, rarely minutely erose-denticulate.</text>
      <biological_entity constraint="inner" id="o25354" name="tepal" name_original="tepals" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="rarely minutely" name="shape" notes="" src="d0_s3" value="erose-denticulate" value_original="erose-denticulate" />
      </biological_entity>
      <biological_entity id="o25355" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o25354" id="r2821" name="with" negation="false" src="d0_s3" to="o25355" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, e Asia, Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26c.1.</number>
  <discussion>Species ca. 40 (21) in the flora).</discussion>
  <discussion>North american representatives of this section (except Rumex venosus, a distinctive species) form a polymorphic complex consisting of often interfertile races (N. M. Sarkar 1958). In most cases those races are separated morphologically and restricted geographically. They may be arranged into at least three aggregates (groups) that approximate the subsections described by K. H. Rechinger (1937): R. verticillatus aggr. (subsect. Verticillati Rechinger f.), R. altissimus aggr. (subsect. Salicifolii Rechinger f., in part), and R. salicifolius aggr. (subsect. Salicifolii, in part). In case of difficulty with identification, inevitable with immature or intermediate specimens, determination to the aggregate level is recommended. Those preferring more broadly circumscribed species may use existing infrageneric combinations (see J. C. Hickman 1984). Attempts have been made to submerge various “microspecies” (especially in the R. salicifolius group) into more broadly circumscribed taxa, usually R. salicifolius and R. mexicanus, and various combinations at subspecific and varietal ranks have been proposed (see Hickman). In my opinion, results of this approach are inconsistent (e.g., some taxa are treated as varieties while others, not less distinct, are accepted as species or subspecies) and in most cases less convincing than the original treatment by Rechinger and the thorough study by Sarkar. I agree with Sarkar (p. 993) that “any drastic taxonomic revision of the species of the Axillares section of Rumex should be postponed until more complete cytogenetic data have accumulated concerning the interrelationships of all the taxa in this section.”</discussion>
  <references>
    <reference>Sarkar, N. M. 1958. Cytotaxonomic studies on Rumex sect. Axillares. Canad. J. Bot. 36: 947–996.</reference>
  </references>
  
</bio:treatment>