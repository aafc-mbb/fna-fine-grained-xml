<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">179</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="(Linnaeus) E. H. L. Krause in J. Sturm et al." date="1901" rank="species">chalcedonica</taxon_name>
    <place_of_publication>
      <publication_title>in J. Sturm et al., Deutsch. Fl. ed.</publication_title>
      <place_in_publication>2, 5: 96. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species chalcedonica;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250060837</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lychnis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">chalcedonica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 436. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lychnis;species chalcedonica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrostemma</taxon_name>
    <taxon_name authority="(Linnaeus) Doellinger" date="unknown" rank="species">chalcedonica</taxon_name>
    <taxon_hierarchy>genus Agrostemma;species chalcedonica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, coarse, rhizomatous;</text>
      <biological_entity id="o12817" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="relief" src="d0_s0" value="coarse" value_original="coarse" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizome branched, stout.</text>
      <biological_entity id="o12818" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, few-branched, 50–100 cm, hispid.</text>
      <biological_entity id="o12819" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="few-branched" value_original="few-branched" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves rounded into tightly sessile base;</text>
      <biological_entity id="o12820" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="into base" constraintid="o12821" is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o12821" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="tightly" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade lanceolate to ovate, 5–12 cm × 20–60 mm, apex acute, sparsely scabrous-pubescent on both surfaces, scabrous-ciliate on abaxial margins and midrib;</text>
      <biological_entity id="o12822" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12823" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character constraint="on surfaces" constraintid="o12824" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="scabrous-pubescent" value_original="scabrous-pubescent" />
        <character constraint="on midrib" constraintid="o12826" is_modifier="false" name="architecture_or_pubescence_or_shape" notes="" src="d0_s4" value="scabrous-ciliate" value_original="scabrous-ciliate" />
      </biological_entity>
      <biological_entity id="o12824" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o12825" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o12826" name="midrib" name_original="midrib" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>basal leaf-blades broadly spatulate.</text>
      <biological_entity constraint="basal" id="o12827" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences subcapitate between terminal pair of leaves, 10–50-flowered, congested, bracteate;</text>
      <biological_entity id="o12828" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character constraint="between terminal pair" constraintid="o12829" is_modifier="false" name="architecture_or_shape" src="d0_s6" value="subcapitate" value_original="subcapitate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="10-50-flowered" value_original="10-50-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o12829" name="pair" name_original="pair" src="d0_s6" type="structure" />
      <biological_entity id="o12830" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o12829" id="r1401" name="part_of" negation="false" src="d0_s6" to="o12830" />
    </statement>
    <statement id="d0_s7">
      <text>bracts lanceolate, herbaceous, ciliate.</text>
      <biological_entity id="o12831" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers sessile to subsessile, 10–16 mm diam.;</text>
      <biological_entity id="o12832" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s8" to="subsessile" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>calyx 10-veined, narrow and tubular in flower, clavate in fruit, 12–17 mm, margins dentate, lobes triangular-lanceolate, 2.5–2.5 mm, coarsely hirsute;</text>
      <biological_entity id="o12833" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="10-veined" value_original="10-veined" />
        <character is_modifier="false" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
        <character constraint="in flower" constraintid="o12834" is_modifier="false" name="shape" src="d0_s9" value="tubular" value_original="tubular" />
        <character constraint="in fruit" constraintid="o12835" is_modifier="false" name="shape" notes="" src="d0_s9" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12834" name="flower" name_original="flower" src="d0_s9" type="structure" />
      <biological_entity id="o12835" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o12836" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o12837" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular-lanceolate" value_original="triangular-lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s9" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals scarlet, sometimes white or pink, clawed, claw equaling calyx, limb spreading, obovate, deeply 2-lobed, 6–11 mm, shorter than calyx, appendages tubular, 2–3 mm;</text>
      <biological_entity id="o12838" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="scarlet" value_original="scarlet" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o12839" name="claw" name_original="claw" src="d0_s10" type="structure" />
      <biological_entity id="o12840" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o12841" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s10" value="2-lobed" value_original="2-lobed" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character constraint="than calyx" constraintid="o12842" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o12842" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o12843" name="appendage" name_original="appendages" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens equaling calyx;</text>
      <biological_entity id="o12844" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o12845" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 5, equaling calyx.</text>
      <biological_entity id="o12846" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o12847" name="calyx" name_original="calyx" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules ovoid, 8–10 mm, opening by 5 teeth;</text>
      <biological_entity id="o12848" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12849" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
      <relation from="o12848" id="r1402" name="opening by" negation="false" src="d0_s13" to="o12849" />
    </statement>
    <statement id="d0_s14">
      <text>carpophore 4–6 mm.</text>
      <biological_entity id="o12850" name="carpophore" name_original="carpophore" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds dark reddish-brown, reniform-rotund, 0.7–1 mm diam., coarsely papillate;</text>
      <biological_entity id="o12851" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="reniform-rotund" value_original="reniform-rotund" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="diameter" src="d0_s15" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s15" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>papillae ca. as high as wide.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 24 (Europe).</text>
      <biological_entity id="o12852" name="papilla" name_original="papillae" src="d0_s16" type="structure" />
      <biological_entity constraint="2n" id="o12853" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, waste places, open woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="open woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., N.S., Ont., P.E.I., Que., Sask.; Conn., Idaho, Ill., Ind., Iowa, Maine, Mass., Mich., Minn., N.H., N.J., N.Y., Pa., Vt., Wis.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Maltese-cross</other_name>
  <other_name type="common_name">scarlet lychnis</other_name>
  <other_name type="common_name">lychnide de Chalcédoine</other_name>
  <other_name type="common_name">croix de Jérusalem</other_name>
  <discussion>Silene chalcedonica is widely cultivated but rarely escapes and probably does not persist.</discussion>
  
</bio:treatment>