<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="illustration_page">274</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">pauciflorum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 735. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species pauciflorum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060456</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">depauperatum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species depauperatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">gnaphalodes</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species gnaphalodes;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="species">multiceps</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species multiceps;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pauciflorum</taxon_name>
    <taxon_name authority="(Bentham) Reveal" date="unknown" rank="variety">gnaphalodes</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species pauciflorum;variety gnaphalodes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, loosely matted, usually scapose, 0.5–2 × 0.5–3 dm, tomentose, grayish.</text>
      <biological_entity id="o8498" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, usually with persistent leaf-bases, up to 1/4 height of plant;</text>
      <biological_entity id="o8499" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o8500" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/4 height of plant" />
      </biological_entity>
      <relation from="o8499" id="r952" modifier="usually" name="with" negation="false" src="d0_s1" to="o8500" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems matted;</text>
      <biological_entity constraint="caudex" id="o8501" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems scapelike, erect or nearly so, slender, solid, not fistulose, 0.3–2 dm, tomentose.</text>
      <biological_entity id="o8502" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="scapelike" value_original="scapelike" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character name="orientation" src="d0_s3" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves fasciculate in terminal tufts, sometimes 1 per node and sheathing up stem 1–5 cm;</text>
      <biological_entity id="o8503" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="in terminal tufts" constraintid="o8504" is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="fasciculate" value_original="fasciculate" />
        <character constraint="up stem" constraintid="o8506" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o8504" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o8505" modifier="sometimes" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8505" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity id="o8506" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1–5 cm, tomentose to lanate;</text>
      <biological_entity id="o8507" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s5" to="lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade linear-oblanceolate to oblanceolate or narrowly spatulate to elliptic, 1–4 × 0.1–1 cm, grayish or whitish-tomentose abaxially, less so to subglabrous (or rarely glabrous) adaxially or white-lanate on both surfaces, margins plane.</text>
      <biological_entity id="o8508" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s6" to="oblanceolate or narrowly spatulate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="whitish-tomentose" value_original="whitish-tomentose" />
        <character is_modifier="false" modifier="less; adaxially" name="pubescence" src="d0_s6" value="subglabrous" value_original="subglabrous" />
        <character constraint="on surfaces" constraintid="o8509" is_modifier="false" name="pubescence" src="d0_s6" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity id="o8509" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o8510" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences capitate, subcapitate, or cymose-umbellate, 1–5 × 1–2 cm;</text>
      <biological_entity id="o8511" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subcapitate" value_original="subcapitate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose-umbellate" value_original="cymose-umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subcapitate" value_original="subcapitate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose-umbellate" value_original="cymose-umbellate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches absent or dichotomous;</text>
      <biological_entity id="o8512" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 2–6, linear to lanceolate, scalelike to semileaflike, 1.5–20 × 1–5 mm.</text>
      <biological_entity id="o8513" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="6" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="lanceolate scalelike" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="lanceolate scalelike" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o8514" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node or 2–5 (–7) per cluster, narrowly turbinate, (3.5–) 4–5 × (1.5–) 2–3 mm, rigid, floccose to tomentose;</text>
      <biological_entity id="o8515" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o8516" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="7" />
        <character char_type="range_value" constraint="per cluster" from="2" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s11" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s11" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="rigid" value_original="rigid" />
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s11" to="tomentose" />
      </biological_entity>
      <biological_entity id="o8516" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.5–0.8 mm.</text>
      <biological_entity id="o8517" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 2–2.5 mm;</text>
      <biological_entity id="o8518" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth whitish brown to rose, pubescent, rarely glabrous;</text>
      <biological_entity id="o8519" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="whitish brown" name="coloration" src="d0_s14" to="rose" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/3, monomorphic, oblong;</text>
      <biological_entity id="o8520" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 2.5–3 mm;</text>
      <biological_entity id="o8521" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o8522" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown to brown, 2–2.5 mm, glabrous.</text>
      <biological_entity id="o8523" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s18" to="brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clay to gravelly flats, washes, and slopes, grassland and sagebrush communities, juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clay" />
        <character name="habitat" value="gravelly flats" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="grassland" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Sask.; Colo., Mont., Nebr., N.Dak., S.Dak., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>64.</number>
  <other_name type="common_name">Few-flower wild buckwheat</other_name>
  <discussion>Eriogonum pauciflorum is the common, matted wild buckwheat on the Great Plains. The species occurs in southern Manitoba and Saskatchewan, northeastern Colorado, Montana, western Nebraska, North Dakota, Nebraska, South Dakota, and Wyoming. Plants from southeastern Wyoming, northeastern Colorado and western Nebraska have somewhat broader leaf blades (spatulate to elliptic rather than linear-oblanceolate to oblanceolate) that are densely tomentose to lanate on both surfaces (rather than loosely tomentose abaxially and less so or glabrous adaxially). Such plants have been distinguished as var. gnaphalodes. This expression would be worthy of a place in the rock garden. A hybrid between var. pauciflorum and E. effusum has been named E. ×nebraskense Rydberg (see 2. E. effusum). The hybrid is known from Weld County, Colorado, Cheyenne, Dawes, Kimball, and Sioux counties, Nebraska, and Converse and Platte counties, Wyoming.</discussion>
  
</bio:treatment>