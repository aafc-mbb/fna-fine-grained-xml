<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">75</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cerastium</taxon_name>
    <taxon_name authority="Persoon" date="1805" rank="species">diffusum</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>1: 520. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus cerastium;species diffusum;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242416263</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="Babington" date="unknown" rank="species">atrovirens</taxon_name>
    <taxon_hierarchy>genus Cerastium;species atrovirens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="Curtis" date="unknown" rank="species">tetrandrum</taxon_name>
    <taxon_hierarchy>genus Cerastium;species tetrandrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, with slender taproot.</text>
      <biological_entity id="o6185" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="slender" id="o6186" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o6185" id="r678" name="with" negation="false" src="d0_s0" to="o6186" />
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent or ascending, diffusely branched, 7.5–30 cm, densely covered and viscid with short, glandular-hairs;</text>
      <biological_entity id="o6187" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="diffusely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="7.5" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="position_relational" src="d0_s1" value="covered" value_original="covered" />
        <character constraint="with short" is_modifier="false" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o6188" name="glandular-hair" name_original="glandular-hairs" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>small axillary tufts of leaves absent.</text>
      <biological_entity constraint="axillary" id="o6189" name="tuft" name_original="tufts" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o6190" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o6189" id="r679" name="consist_of" negation="false" src="d0_s2" to="o6190" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves not marcescent, sessile distally, spatulate to pseudopetiolate proximally;</text>
      <biological_entity id="o6191" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="condition" src="d0_s3" value="marcescent" value_original="marcescent" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s3" value="pseudopetiolate" value_original="pseudopetiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 5–10 × 2–4 mm, covered with short, glandular and eglandular hairs;</text>
      <biological_entity id="o6192" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o6193" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o6192" id="r680" name="covered with" negation="false" src="d0_s4" to="o6193" />
    </statement>
    <statement id="d0_s5">
      <text>proximal blades oblanceolate, apex obtuse;</text>
      <biological_entity constraint="proximal" id="o6194" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o6195" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline blades ovate or oblong-ovate, apex acute.</text>
      <biological_entity constraint="cauline" id="o6196" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong-ovate" value_original="oblong-ovate" />
      </biological_entity>
      <biological_entity id="o6197" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences lax, 3–30-flowered cymes;</text>
      <biological_entity id="o6198" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o6199" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="3-30-flowered" value_original="3-30-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts lanceolate to ovate, herbaceous, glandular-pubescent.</text>
      <biological_entity id="o6200" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="ovate" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels straight, ultimately erect in fruit, slender, 2–15 mm, much longer than capsule, glandular.</text>
      <biological_entity id="o6201" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character constraint="in fruit" constraintid="o6202" is_modifier="false" modifier="ultimately" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character constraint="than capsule" constraintid="o6203" is_modifier="false" name="length_or_size" src="d0_s9" value="much longer" value_original="much longer" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o6202" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o6203" name="capsule" name_original="capsule" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers 4 (–5) -merous;</text>
      <biological_entity id="o6204" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="4(-5)-merous" value_original="4(-5)-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals lanceolate, 4–7 mm, margins narrow distally, apex acute or acuminate, glandular-pubescent, hairs usually not projecting beyond apex;</text>
      <biological_entity id="o6205" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6206" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="distally" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o6207" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o6208" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character constraint="beyond apex" constraintid="o6209" is_modifier="false" modifier="usually not" name="orientation" src="d0_s11" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o6209" name="apex" name_original="apex" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals ca. 3 mm, ca. 0.75 times as long as sepals, apex 2-fid;</text>
      <biological_entity id="o6210" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
        <character constraint="sepal" constraintid="o6211" is_modifier="false" name="length" src="d0_s12" value="0.75 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o6211" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o6212" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 4 (–5);</text>
      <biological_entity id="o6213" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="5" />
        <character name="quantity" src="d0_s13" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 4 (–5).</text>
      <biological_entity id="o6214" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="5" />
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules narrowly cylindric, nearly straight, 5–7.5 mm, 1–1.5 times as long as sepals;</text>
      <biological_entity id="o6215" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="7.5" to_unit="mm" />
        <character constraint="sepal" constraintid="o6216" is_modifier="false" name="length" src="d0_s15" value="1-1.5 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o6216" name="sepal" name_original="sepals" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>teeth 8 or 10, erect, margins convolute.</text>
      <biological_entity id="o6217" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" unit="or" value="8" value_original="8" />
        <character name="quantity" src="d0_s16" unit="or" value="10" value_original="10" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o6218" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="convolute" value_original="convolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds reddish-brown, 0.5–0.7 mm, bluntly tuberculate;</text>
      <biological_entity id="o6219" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="0.7" to_unit="mm" />
        <character is_modifier="false" modifier="bluntly" name="relief" src="d0_s17" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>testa not inflated.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 72.</text>
      <biological_entity id="o6220" name="testa" name_original="testa" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s18" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6221" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy places on coast, rarely inland in similar places and on railway ballast</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coast" modifier="sandy places on" />
        <character name="habitat" value="similar places" modifier="rarely inland in" />
        <character name="habitat" value="railway ballast" modifier="and on" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Ill.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Dark-green mouse-ear chickweed</other_name>
  <discussion>This species was abundant on the sandy shore at Fort Bragg, Mendocino County, California, in 1985 and should be looked for elsewhere. The entirely herbaceous bracts, short capsule, and the floral parts usually in fours identify this small weedy species.</discussion>
  <discussion>Previous reports of this species (as Cerastium tetrandrum) by J. A. Steyermark (1963) from Missouri and M. L. Fernald (1950) from Virginia are referable to C. pumilum and C. brachypetalum, respectively.</discussion>
  
</bio:treatment>