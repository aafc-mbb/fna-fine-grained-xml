<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">329</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="illustration_page">328</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="1835" rank="species">longifolium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">longifolium</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eriogonum;species longifolium;variety longifolium;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060374</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Shinners" date="unknown" rank="species">longifolium</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="subspecies">diffusum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species longifolium;subspecies diffusum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longifolium</taxon_name>
    <taxon_name authority="Engelmann &amp; A. Gray" date="unknown" rank="variety">lindheimeri</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species longifolium;variety lindheimeri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longifolium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">plantagineum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species longifolium;variety plantagineum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vespinum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species vespinum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–20 dm, tomentose to nearly glabrous.</text>
      <biological_entity id="o1053" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s0" to="nearly glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal leaf-blade 0.5–20 × 0.3–3 cm, tomentose abaxially, less so to floccose or nearly glabrous adaxially.</text>
      <biological_entity id="o1054" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o1055" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="less" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
        <character is_modifier="false" modifier="nearly; adaxially" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 0.3–3 cm.</text>
      <biological_entity id="o1056" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres turbinate to campanulate, 4–6 × 2.5–6 mm.</text>
      <biological_entity id="o1057" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s3" to="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 5–11 mm, including 0.5–2.5 mm stipelike base.</text>
      <biological_entity id="o1058" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1059" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="true" name="shape" src="d0_s4" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <relation from="o1058" id="r104" name="including" negation="false" src="d0_s4" to="o1059" />
    </statement>
    <statement id="d0_s5">
      <text>Achenes 4–6 mm.</text>
      <biological_entity id="o1060" name="achene" name_original="achenes" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly, often calcareous flats, slopes, and outcrops, mixed grassland, creosote bush, and mesquite communities, oak and conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to" modifier="gravelly" />
        <character name="habitat" value="sandy to calcareous flats" modifier="often" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="creosote bush" />
        <character name="habitat" value="mesquite communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60-1100(-1300) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="60" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1300" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Kans., La., Mo., N.Mex., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>105a.</number>
  <other_name type="common_name">Long-leaf wild buckwheat</other_name>
  <discussion>Variety longifolium is widespread and common in the south-central United States. Its roots were used as food by the Kiowa (P. A. Vestal and R. E. Schultes 1939), and an infusion of the roots was taken by the Comanche for stomach trouble (G. G. Carlson and V. H. Jones 1940).</discussion>
  
</bio:treatment>