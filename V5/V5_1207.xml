<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">593</other_info_on_meta>
    <other_info_on_meta type="mention_page">583</other_info_on_meta>
    <other_info_on_meta type="mention_page">594</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1754" rank="genus">persicaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Persicaria</taxon_name>
    <taxon_name authority="(Hudson) Opiz" date="1852" rank="species">minor</taxon_name>
    <place_of_publication>
      <publication_title>Seznam,</publication_title>
      <place_in_publication>72. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus persicaria;section persicaria;species minor;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250060700</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="Hudson" date="unknown" rank="species">minus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Angl.,</publication_title>
      <place_in_publication>148. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polygonum;species minus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">minus</taxon_name>
    <taxon_name authority="(Meisner) Fernald" date="unknown" rank="variety">subcontinuum</taxon_name>
    <taxon_hierarchy>genus Polygonum;species minus;variety subcontinuum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, 0.5–3 (–4) dm;</text>
      <biological_entity id="o17698" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots also sometimes at from proximal nodes;</text>
      <biological_entity id="o17699" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o17700" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o17699" id="r1967" modifier="sometimes" name="at" negation="false" src="d0_s1" to="o17700" />
    </statement>
    <statement id="d0_s2">
      <text>rhizomes and stolons absent.</text>
      <biological_entity id="o17701" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o17702" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems decumbent or ascending, branched proximally, scarcely ribbed, glabrous or scabrous distally.</text>
      <biological_entity id="o17703" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="scarcely" name="architecture_or_shape" src="d0_s3" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: ocrea brownish, cylindric, 3–10 mm, chartaceous, base not inflated, margins truncate, ciliate with bristles (0.3–) 1–3 (–5) mm, surface glabrous or strigose, not glandular-punctate;</text>
      <biological_entity id="o17704" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17705" name="ocreum" name_original="ocrea" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o17706" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o17707" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character constraint="with bristles" constraintid="o17708" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o17708" name="bristle" name_original="bristles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17709" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s4" value="glandular-punctate" value_original="glandular-punctate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.1–0.2 cm, glabrous or strigose, leaves sometimes sessile;</text>
      <biological_entity id="o17710" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o17711" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s5" to="0.2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o17712" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade without dark triangular or lunate blotch adaxially, linear to linear-lanceolate, (1–) 2–7.5 (–10) × (0.2–) 0.4–1 (–2.3) cm, base tapered to cuneate, margins antrorsely scabrous, apex acute to acuminate, faces glabrous or sparingly strigose, especially along midveins, not glandular-punctate.</text>
      <biological_entity id="o17713" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o17714" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s6" to="linear-lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s6" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s6" to="0.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="2.3" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17715" name="blotch" name_original="blotch" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="dark" value_original="dark" />
        <character is_modifier="true" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="true" name="shape" src="d0_s6" value="lunate" value_original="lunate" />
      </biological_entity>
      <biological_entity id="o17716" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s6" to="cuneate" />
      </biological_entity>
      <biological_entity id="o17717" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="antrorsely" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o17718" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <biological_entity id="o17719" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" notes="" src="d0_s6" value="glandular-punctate" value_original="glandular-punctate" />
      </biological_entity>
      <biological_entity id="o17720" name="midvein" name_original="midveins" src="d0_s6" type="structure" />
      <relation from="o17714" id="r1968" name="without" negation="false" src="d0_s6" to="o17715" />
      <relation from="o17719" id="r1969" modifier="especially" name="along" negation="false" src="d0_s6" to="o17720" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal and axillary, ascending to erect, usually interrupted proximally, uninterrupted distally, 10–50 × 2–4 mm;</text>
      <biological_entity id="o17721" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="erect" />
        <character is_modifier="false" modifier="usually; proximally" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s7" value="uninterrupted" value_original="uninterrupted" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncle (0–) 2–25 mm, sometimes absent on axillary inflorescences and flowers thus enclosed in ocreae, glabrous;</text>
      <biological_entity id="o17722" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
        <character constraint="on flowers" constraintid="o17724" is_modifier="false" modifier="sometimes" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o17723" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o17724" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o17725" name="ocrea" name_original="ocreae" src="d0_s8" type="structure" />
      <relation from="o17724" id="r1970" modifier="thus" name="enclosed in" negation="false" src="d0_s8" to="o17725" />
    </statement>
    <statement id="d0_s9">
      <text>ocreolae not overlapping proximally, usually overlapping distally, margins ciliate with bristles (0.1–) 0.6–2 (–2.7) mm.</text>
      <biological_entity id="o17726" name="ocreola" name_original="ocreolae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not; proximally" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="usually; distally" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o17727" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character constraint="with bristles" constraintid="o17728" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o17728" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels ascending, 0.5–1 mm.</text>
      <biological_entity id="o17729" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 1–3 (–4) per ocreate fascicle, homostylous;</text>
      <biological_entity id="o17730" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="4" />
        <character char_type="range_value" constraint="per fascicle" constraintid="o17731" from="1" name="quantity" src="d0_s11" to="3" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="homostylous" value_original="homostylous" />
      </biological_entity>
      <biological_entity id="o17731" name="fascicle" name_original="fascicle" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>perianth roseate to red, rarely white, glabrous, not glandular-punctate, scarcely accrescent;</text>
      <biological_entity id="o17732" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="roseate" name="coloration" src="d0_s12" to="red" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s12" value="glandular-punctate" value_original="glandular-punctate" />
        <character is_modifier="false" modifier="scarcely" name="size" src="d0_s12" value="accrescent" value_original="accrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals 5, connate ca. 1/3 their length, obovate to elliptic, 2.5–3 mm, veins not prominent, not anchor-shaped, margins entire, apex obtuse to rounded;</text>
      <biological_entity id="o17733" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character name="length" src="d0_s13" value="1/3" value_original="1/3" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s13" to="elliptic" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17734" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="anchor--shaped" value_original="anchor--shaped" />
      </biological_entity>
      <biological_entity id="o17735" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17736" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s13" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 5 (–6), included;</text>
      <biological_entity id="o17737" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="6" />
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers yellow to pink, elliptic;</text>
      <biological_entity id="o17738" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s15" to="pink" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s15" value="elliptic" value_original="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 2 (–3), connate at bases.</text>
      <biological_entity id="o17739" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="3" />
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character constraint="at bases" constraintid="o17740" is_modifier="false" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o17740" name="base" name_original="bases" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Achenes included, brownish black to black, biconvex or, rarely, 3-gonous, (1.5–) 1.8–2.3 (– 2.7) × (1.1–) 1.3–1.5 (–1.8) mm, shiny, smooth.</text>
      <biological_entity id="o17741" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="included" value_original="included" />
        <character char_type="range_value" from="brownish black" name="coloration" src="d0_s17" to="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="biconvex" value_original="biconvex" />
        <character name="shape" src="d0_s17" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s17" to="1.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s17" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s17" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="atypical_width" src="d0_s17" to="1.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s17" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s17" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, open places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open places" modifier="damp" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.B., Ont., Que.; Conn., Ind., La., Mass., Nebr., Pa., Vt., Va.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Small water-pepper</other_name>
  <other_name type="common_name">petite renouée</other_name>
  <discussion>Persicaria minor is synonymized with P. maculosa in most North American floras; its distribution in the flora area is poorly known. Hybrids between P. minor and P. maculosa have been documented in Europe (R. H. Roberts 1977).</discussion>
  
</bio:treatment>