<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">511</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="mention_page">505</other_info_on_meta>
    <other_info_on_meta type="mention_page">510</other_info_on_meta>
    <other_info_on_meta type="mention_page">512</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="Meisner in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">mexicanus</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 45. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species mexicanus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060788</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous;</text>
    </statement>
    <statement id="d0_s1">
      <text>with vertical rootstock, occasionally with short, creeping rhizomes.</text>
      <biological_entity id="o7827" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7828" name="rootstock" name_original="rootstock" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o7829" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
      <relation from="o7827" id="r856" name="with" negation="false" src="d0_s1" to="o7828" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending, usually producing axillary shoots below 1st-order inflorescence or at proximal nodes, 30–60 (–90) cm.</text>
      <biological_entity id="o7830" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o7831" name="shoot" name_original="shoots" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o7832" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="90" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
      </biological_entity>
      <relation from="o7830" id="r857" modifier="usually" name="producing" negation="false" src="d0_s2" to="o7831" />
      <relation from="o7830" id="r858" modifier="usually" name="below 1st-order inflorescence or at" negation="false" src="d0_s2" to="o7832" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades light green to yellowish green, linear-lanceolate, occasionally lanceolate, 6–14 × 1–3.5 (–4) cm, usually ca. 5–7 times as long as wide, widest near middle, thin, not coriaceous, base cuneate, margins entire, flat or undulate, apex acute or attenuate.</text>
      <biological_entity id="o7833" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s3" to="yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s3" to="14" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="5-7" value_original="5-7" />
        <character constraint="near middle" constraintid="o7834" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o7834" name="middle" name_original="middle" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" notes="" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o7835" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o7836" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o7837" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal and axillary, terminal usually occupying distal 1/5–1/3 of stem, rather dense or interrupted in proximal 1/2, usually broadly paniculate (branches simple or with few 2d-order branches).</text>
      <biological_entity id="o7838" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="rather" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character constraint="in proximal 1/2" constraintid="o7840" is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="usually broadly" name="arrangement" notes="" src="d0_s4" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o7839" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/5" is_modifier="true" name="quantity" src="d0_s4" to="1/3" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7840" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <relation from="o7838" id="r859" name="occupying" negation="false" src="d0_s4" to="o7839" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels articulated in proximal 1/3 or almost near base, filiform (thickened distally), 4–7 mm, not more than 2–2.5 times as long as inner tepals, articulation indistinctly swollen.</text>
      <biological_entity id="o7841" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="in " constraintid="o7843" is_modifier="false" name="architecture" src="d0_s5" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character constraint="tepal" constraintid="o7846" is_modifier="false" name="length" src="d0_s5" value="2+-2.5 times as long as inner tepals" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7842" name="1/3" name_original="1/3" src="d0_s5" type="structure" />
      <biological_entity id="o7843" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o7844" name="1/3" name_original="1/3" src="d0_s5" type="structure" />
      <biological_entity id="o7845" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity constraint="inner" id="o7846" name="tepal" name_original="tepals" src="d0_s5" type="structure" />
      <biological_entity id="o7847" name="articulation" name_original="articulation" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s5" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o7843" id="r860" name="in" negation="false" src="d0_s5" to="o7844" />
      <relation from="o7843" id="r861" name="in" negation="false" src="d0_s5" to="o7845" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 10–20 in whorls;</text>
      <biological_entity id="o7848" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o7849" from="10" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o7849" name="whorl" name_original="whorls" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>inner tepals broadly ovate-triangular, occasionally broadly triangular, 3.5–4.5 (–5) × 3.5–4 (–5) mm, base truncate or indistinctly cordate, margins entire or indistinctly erose, apex obtuse or subacute;</text>
      <biological_entity constraint="inner" id="o7850" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" modifier="occasionally broadly" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s7" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7851" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s7" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o7852" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="indistinctly" name="architecture" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o7853" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tubercles 3, equal or subequal (much narrower than inner tepals).</text>
      <biological_entity id="o7854" name="tubercle" name_original="tubercles" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes brown or dark reddish-brown, 2–3 × 1.5–2 mm. 2n = 40.</text>
      <biological_entity id="o7855" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7856" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shores of streams and rivers, wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shores" constraint="of streams and rivers , wet meadows" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Mexican willow or dock</other_name>
  <discussion>Some authors recognize Rumex mexicanus in the broad sense, including in it many other taxa treated here as separate entities. For consistency, the entities of the R. salicifolius aggregate that are recognized herein are kept separate pending additional taxonomic research.</discussion>
  
</bio:treatment>