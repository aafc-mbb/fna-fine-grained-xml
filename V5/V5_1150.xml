<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">563</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="mention_page">561</other_info_on_meta>
    <other_info_on_meta type="mention_page">562</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">polygonum</taxon_name>
    <taxon_name authority="S. Watson" date="1873" rank="section">Duravia</taxon_name>
    <taxon_name authority="M. Peck &amp; Ownbey" date="1950" rank="species">heterosepalum</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>10: 250. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonum;section duravia;species heterosepalum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060739</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, compact, often cushionlike.</text>
      <biological_entity id="o17742" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s0" value="cushionlike" value_original="cushionlike" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, green or reddish, simple or branched near base, not wiry, 1.5–5 cm, glabrous.</text>
      <biological_entity id="o17743" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="near base" constraintid="o17744" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s1" value="wiry" value_original="wiry" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17744" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves uniformly distributed, dense, not articulated to ocreae, basal leaves persistent or caducous, distal leaves gradually reduced to bracts;</text>
      <biological_entity id="o17745" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="uniformly" name="arrangement" src="d0_s2" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character constraint="to ocreae" constraintid="o17746" is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="articulated" value_original="articulated" />
      </biological_entity>
      <biological_entity id="o17746" name="ocrea" name_original="ocreae" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o17747" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17748" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="to bracts" constraintid="o17749" is_modifier="false" modifier="gradually" name="size" src="d0_s2" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o17749" name="bract" name_original="bracts" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>ocrea 3–6 mm, glabrous, proximal part cylindric, distal part disintegrating almost to base, with whitish, straight, rigid fibers;</text>
      <biological_entity id="o17750" name="ocreum" name_original="ocrea" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17751" name="part" name_original="part" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17752" name="part" name_original="part" src="d0_s3" type="structure">
        <character constraint="to base" constraintid="o17753" is_modifier="false" name="dehiscence" src="d0_s3" value="disintegrating" value_original="disintegrating" />
      </biological_entity>
      <biological_entity id="o17753" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o17754" name="fiber" name_original="fibers" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="true" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
      </biological_entity>
      <relation from="o17752" id="r1971" name="with" negation="false" src="d0_s3" to="o17754" />
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o17755" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 3-veined, without pleats, linear to lanceolate, 10–20 × 0.6–2.7 mm, margins revolute, smooth, apex spine-tipped.</text>
      <biological_entity id="o17756" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s5" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17757" name="pleat" name_original="pleats" src="d0_s5" type="structure" />
      <biological_entity id="o17758" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o17759" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
      <relation from="o17756" id="r1972" name="without" negation="false" src="d0_s5" to="o17757" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary;</text>
      <biological_entity id="o17760" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cymes in most axils, 2–3-flowered.</text>
      <biological_entity id="o17761" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="2-3-flowered" value_original="2-3-flowered" />
      </biological_entity>
      <biological_entity id="o17762" name="axil" name_original="axils" src="d0_s7" type="structure" />
      <relation from="o17761" id="r1973" name="in" negation="false" src="d0_s7" to="o17762" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels enclosed in ocreae, erect, 0.1–1 mm.</text>
      <biological_entity id="o17763" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17764" name="ocrea" name_original="ocreae" src="d0_s8" type="structure" />
      <relation from="o17763" id="r1974" name="enclosed in" negation="false" src="d0_s8" to="o17764" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers closed;</text>
      <biological_entity id="o17765" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="condition" src="d0_s9" value="closed" value_original="closed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth 2.3–2.7 mm;</text>
      <biological_entity id="o17766" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s10" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tube 3–7% of perianth length;</text>
      <biological_entity id="o17767" name="tube" name_original="tube" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>tepals overlapping, whitish with whitish or pink margins, petaloid, oblong, navicular, dimorphic, outer 2 shorter than inner 3, outer 2 0.8–1.2 mm, inner 2.3–2.7 mm, papillose at base, apex acute or acuminate;</text>
      <biological_entity id="o17768" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="overlapping" value_original="overlapping" />
        <character constraint="with margins" constraintid="o17769" is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="navicular" value_original="navicular" />
        <character is_modifier="false" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o17769" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity constraint="outer" id="o17770" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character constraint="than inner tepals" constraintid="o17771" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17771" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o17772" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8-1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17773" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s12" to="2.7" to_unit="mm" />
        <character constraint="at base" constraintid="o17774" is_modifier="false" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o17774" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o17775" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>midveins unbranched;</text>
      <biological_entity id="o17776" name="midvein" name_original="midveins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 5–6.</text>
      <biological_entity id="o17777" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s14" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes enclosed in perianth, olive brown, narrowly ovate to ovatelanceolate, 1.5–2 mm, faces subequal, shiny, smooth.</text>
      <biological_entity id="o17778" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="olive brown" value_original="olive brown" />
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s15" to="ovatelanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17779" name="perianth" name_original="perianth" src="d0_s15" type="structure" />
      <biological_entity id="o17780" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o17778" id="r1975" name="enclosed in" negation="false" src="d0_s15" to="o17779" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry waste ground, open flats in sagebrush plains, ponderosa pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry waste ground" />
        <character name="habitat" value="open flats" constraint="in sagebrush plains , ponderosa pine forests" />
        <character name="habitat" value="sagebrush plains" />
        <character name="habitat" value="ponderosa pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Dwarf desert knotweed</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>