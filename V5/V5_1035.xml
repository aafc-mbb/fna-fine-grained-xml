<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">505</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">507</other_info_on_meta>
    <other_info_on_meta type="illustration_page">506</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">verticillatus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 334. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species verticillatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417187</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous or nearly so, with vertical rootstock.</text>
      <biological_entity id="o31385" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s0" value="nearly" value_original="nearly" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o31386" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o31385" id="r3548" name="with" negation="false" src="d0_s0" to="o31386" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or, rarely, ascending, simple or producing axillary shoots below 1st-order inflorescence or at proximal nodes, 40–100 (–150) cm.</text>
      <biological_entity id="o31387" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character name="orientation" src="d0_s1" value="," value_original="," />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s1" value="producing axillary shoots" value_original="producing axillary shoots" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31388" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="150" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
      <relation from="o31387" id="r3549" name="below 1st-order inflorescence or at" negation="false" src="d0_s1" to="o31388" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades with lateral-veins forming angle of 45–60° with midvein, linear-lanceolate or lanceolate, 5–30 (–40) × 1–5 cm, usually 5–7 (–10) times as long as wide, normally rather thin or at most subcoriaceous, base narrowly cuneate, margins entire, flat or slightly undulate, apex acute or acuminate.</text>
      <biological_entity id="o31389" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="5-7(-10)" value_original="5-7(-10)" />
        <character is_modifier="false" modifier="normally rather" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s2" value="0-subcoriaceous" value_original="0-subcoriaceous" />
      </biological_entity>
      <biological_entity id="o31390" name="lateral-vein" name_original="lateral-veins" src="d0_s2" type="structure" />
      <biological_entity id="o31391" name="angle" name_original="angle" src="d0_s2" type="structure" />
      <biological_entity id="o31392" name="midvein" name_original="midvein" src="d0_s2" type="structure" />
      <biological_entity id="o31393" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o31394" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o31395" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o31389" id="r3550" name="with" negation="false" src="d0_s2" to="o31390" />
      <relation from="o31390" id="r3551" name="forming" negation="false" src="d0_s2" to="o31391" />
      <relation from="o31389" id="r3552" modifier="45-60°" name="with" negation="false" src="d0_s2" to="o31392" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal and axillary, terminal usually occupying distal 1/3–1/2 of stem, usually lax, interrupted at least in basal 1/2, narrowly paniculate.</text>
      <biological_entity id="o31396" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character constraint="in basal 1/2" constraintid="o31398" is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="narrowly" name="arrangement" notes="" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o31397" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s3" to="1/2" />
      </biological_entity>
      <biological_entity constraint="basal" id="o31398" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <relation from="o31396" id="r3553" name="occupying" negation="false" src="d0_s3" to="o31397" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels articulated in proximal part, distinctly thickened distally, 10–17 mm, (2.5–) 3–5 times as long as inner tepals, articulation distinctly or slightly swollen.</text>
      <biological_entity id="o31399" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character constraint="in proximal part" constraintid="o31400" is_modifier="false" name="architecture" src="d0_s4" value="articulated" value_original="articulated" />
        <character is_modifier="false" modifier="distinctly; distally" name="size_or_width" notes="" src="d0_s4" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="17" to_unit="mm" />
        <character constraint="tepal" constraintid="o31401" is_modifier="false" name="length" src="d0_s4" value="(2.5-)3-5 times as long as inner tepals" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31400" name="part" name_original="part" src="d0_s4" type="structure" />
      <biological_entity constraint="inner" id="o31401" name="tepal" name_original="tepals" src="d0_s4" type="structure" />
      <biological_entity id="o31402" name="articulation" name_original="articulation" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 10–15 (–25) in remote whorls;</text>
      <biological_entity id="o31403" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="25" />
        <character char_type="range_value" constraint="in whorls" constraintid="o31404" from="10" name="quantity" src="d0_s5" to="15" />
      </biological_entity>
      <biological_entity id="o31404" name="whorl" name_original="whorls" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s5" value="remote" value_original="remote" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner tepals ovate-triangular or ovate-deltoid, 3.5–5 × 2.5–4 mm, longer than wide or, very rarely, as long as wide, base truncate or rounded, margins entire or, rarely, very indistinctly erose, apex acute or subacute (then with broadly triangular-lingulate tip);</text>
      <biological_entity constraint="inner" id="o31405" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-deltoid" value_original="ovate-deltoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="longer than wide" value_original="longer than wide" />
        <character name="length_or_size" src="d0_s6" value="," value_original="," />
        <character is_modifier="false" name="width" src="d0_s6" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o31406" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o31407" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s6" value="," value_original="," />
        <character is_modifier="false" modifier="very indistinctly" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o31408" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tubercles 3, equal or subequal, minutely punctate and/or transversely rugose (wrinkled) in proximal part.</text>
      <biological_entity id="o31409" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s7" value="punctate" value_original="punctate" />
        <character constraint="in proximal part" constraintid="o31410" is_modifier="false" modifier="transversely" name="relief" src="d0_s7" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31410" name="part" name_original="part" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Achenes brown or dark-brown, 2.3–3.1 × 1.6–2.2 mm. 2n = 60.</text>
      <biological_entity id="o31411" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" src="d0_s8" to="3.1" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s8" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31412" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swamps, bogs, marshes, wet meadows, irrigation ditches, wet alluvial woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swamps" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="irrigation ditches" />
        <character name="habitat" value="wet alluvial woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Swamp dock</other_name>
  <discussion>Reports of Rumex verticillatus for New Mexico (W. C. Martin and C. R. Hutchins 1980) need confirmation. The species was reported erroneously from Colorado (S. L. O’Kane et al. 1988) as a result of misidentification of R. fueginus (see W. A. Weber and R. C. Wittmann 1992).</discussion>
  <discussion>I have not seen specimens of Rumex verticillatus from Delaware, Maine, New Hampshire, and New Jersey, but the species probably occurs in those states.</discussion>
  <discussion>The following two species are closely related to Rumex verticillatus and sometimes treated as subspecies of it.</discussion>
  
</bio:treatment>