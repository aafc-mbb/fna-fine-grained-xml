<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="(Reveal &amp; Henrickson) L. M. Shultz" date="1998" rank="species">thornei</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>3: 51. 1998</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species thornei;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060527</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">ericifolium</taxon_name>
    <taxon_name authority="Reveal &amp; Henrickson" date="unknown" rank="variety">thornei</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>23: 205, fig. 1. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species ericifolium;variety thornei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ericifolium</taxon_name>
    <taxon_name authority="(Reveal &amp; Henrickson) Thorne" date="unknown" rank="subspecies">thornei</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species ericifolium;subspecies thornei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, spreading and matted, not scapose, 0.4–0.8 × 0.4–1 (–2.5) dm.</text>
      <biological_entity id="o25611" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="length" src="d0_s0" to="0.8" to_unit="dm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="width" src="d0_s0" to="1" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, with persistent leaf-bases, up to 1/4 height of plant;</text>
      <biological_entity id="o25612" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o25613" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/4 height of plant" />
      </biological_entity>
      <relation from="o25612" id="r2845" name="with" negation="false" src="d0_s1" to="o25613" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems compact;</text>
      <biological_entity constraint="caudex" id="o25614" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems spreading, slender, solid, not fistulose, 0.1–0.2 dm, floccose to slightly tomentose.</text>
      <biological_entity id="o25615" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s3" to="0.2" to_unit="dm" />
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s3" to="slightly tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline, 1 per node or fasciculate;</text>
      <biological_entity id="o25616" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character constraint="per node" constraintid="o25617" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
      <biological_entity id="o25617" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.01–0.02 cm, tomentose;</text>
      <biological_entity id="o25618" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.01" from_unit="cm" name="some_measurement" src="d0_s5" to="0.02" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade linear, 0.4–0.6 × 0.05–0.1 cm, densely white-tomentose adaxially, finely villous and green adaxially, margins revolute.</text>
      <biological_entity id="o25619" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s6" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="0.05" from_unit="cm" name="width" src="d0_s6" to="0.1" to_unit="cm" />
        <character is_modifier="false" modifier="densely; adaxially" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o25620" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences umbellate-cymose, compact, 0.5–1 × 0.5–1 cm;</text>
      <biological_entity id="o25621" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="umbellate-cymose" value_original="umbellate-cymose" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="compact" value_original="compact" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s7" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s7" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches usually dichotomous, otherwise with secondaries suppressed, glabrous or nearly so;</text>
      <biological_entity id="o25622" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s8" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o25623" name="secondary" name_original="secondaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="suppressed" value_original="suppressed" />
      </biological_entity>
      <relation from="o25622" id="r2846" modifier="otherwise" name="with" negation="false" src="d0_s8" to="o25623" />
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, linear, 0.5–1 mm.</text>
      <biological_entity id="o25624" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o25625" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node, turbinate, 1.5–2 × 1–1.5 mm, floccose;</text>
      <biological_entity id="o25626" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o25627" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o25627" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.4–0.6 mm.</text>
      <biological_entity id="o25628" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 1.5–2 mm;</text>
      <biological_entity id="o25629" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white, glabrous;</text>
      <biological_entity id="o25630" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, dimorphic, those of outer whorl obovate, 1–1.5 mm wide, those of inner whorl oblanceolate, 0.8–1 mm wide, connate proximally;</text>
      <biological_entity id="o25631" name="tepal" name_original="tepals" src="d0_s15" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s15" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o25632" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <biological_entity constraint="inner" id="o25633" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <relation from="o25631" id="r2847" name="part_of" negation="false" src="d0_s15" to="o25632" />
      <relation from="o25631" id="r2848" name="part_of" negation="false" src="d0_s15" to="o25633" />
    </statement>
    <statement id="d0_s16">
      <text>stamens slightly exserted, 2–3 mm;</text>
      <biological_entity id="o25634" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o25635" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 2–2.5 mm, glabrous except for papillate beak.</text>
      <biological_entity id="o25636" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25637" name="beak" name_original="beak" src="d0_s18" type="structure">
        <character is_modifier="true" name="relief" src="d0_s18" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Copper-rich quartzite gravel on ridges, pinyon woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="copper-rich quartzite gravel" constraint="on ridges , pinyon woodlands" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="pinyon woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Thorne’s wild buckwheat</other_name>
  <discussion>Eriogonum thornei is known from a single canyon area in the New York Mountains, San Bernardino County. Shultz elevated Thorne’s wild buckwheat to species status when she proposed E. phoeniceum, here considered a variety of E. microthecum. Eriogonum thornei and E. ericifolium are weakly differentiated, with minimal morphologic differences, and yet they occur on different substrates and are well-removed biogeographically from each other. Thorne’s wild buckwheat is a protected plant in California.</discussion>
  
</bio:treatment>