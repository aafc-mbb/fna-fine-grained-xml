<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">579</other_info_on_meta>
    <other_info_on_meta type="mention_page">574</other_info_on_meta>
    <other_info_on_meta type="mention_page">581</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1754" rank="genus">persicaria</taxon_name>
    <taxon_name authority="(Meisner) H. Gross" date="1913" rank="section">Cephalophilon</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Acad. Int. Géogr. Bot.</publication_title>
      <place_in_publication>4: 27. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus persicaria;section cephalophilon;</taxon_hierarchy>
    <other_info_on_name type="fna_id">316730</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="section">Cephalophilon</taxon_name>
    <place_of_publication>
      <publication_title>in N. Wallich, Pl. Asiat. Rar.</publication_title>
      <place_in_publication>3: 59. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polygonum;section Cephalophilon;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems prostrate or decumbent to ascending or erect, branched, unarmed.</text>
      <biological_entity id="o28623" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character name="growth_form_or_orientation" src="d0_s0" value="decumbent to ascending or erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: ocrea usually chartaceous, rarely coriaceous proximally and chartaceous distally, margins eciliate or ciliate;</text>
      <biological_entity id="o28624" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o28625" name="ocreum" name_original="ocrea" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_texture" src="d0_s1" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" modifier="rarely; proximally" name="texture" src="d0_s1" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_texture" src="d0_s1" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o28626" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eciliate" value_original="eciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole usually winged, auriculate;</text>
      <biological_entity id="o28627" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o28628" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s2" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually with dark triangular or lunate blotch adaxially, broadly elliptic to ovate, base cordate, rounded, truncate, or cuneate, margins entire.</text>
      <biological_entity id="o28629" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o28630" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" notes="" src="d0_s3" to="ovate" />
      </biological_entity>
      <biological_entity id="o28631" name="blotch" name_original="blotch" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="dark" value_original="dark" />
        <character is_modifier="true" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="true" name="shape" src="d0_s3" value="lunate" value_original="lunate" />
      </biological_entity>
      <biological_entity id="o28632" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o28633" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o28630" id="r3193" name="with" negation="false" src="d0_s3" to="o28631" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal or terminal and axillary, capitate, uninterrupted.</text>
      <biological_entity id="o28634" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="uninterrupted" value_original="uninterrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers homostylous, articulation swollen or not;</text>
      <biological_entity id="o28635" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="homostylous" value_original="homostylous" />
      </biological_entity>
      <biological_entity id="o28636" name="articulation" name_original="articulation" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="swollen" value_original="swollen" />
        <character name="shape" src="d0_s5" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth urceolate or campanulate;</text>
      <biological_entity id="o28637" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals (4–) 5, connate 1/3–1/2 their length;</text>
      <biological_entity id="o28638" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/3" name="length" src="d0_s7" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens (5–) 8;</text>
      <biological_entity id="o28639" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s8" to="8" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles deciduous, 2–3, included, spreading.</text>
      <biological_entity id="o28640" name="style" name_original="styles" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Asia; introduced also in Europe, Africa, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>32c.</number>
  <discussion>Species ca. 16 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achenes biconvex; abaxial surface of leaf blades glandular-punctate</description>
      <determination>7 Persicaria nepalensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achenes 3-gonous; abaxial surface of leaf blades not glandular-punctate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade margins ciliate with reddish, multicellular hairs; ocreae lanate; peduncles glabrous or stipitate-glandular distally</description>
      <determination>8 Persicaria capitata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade margins glabrous or antrorsely scabrous with whitish hairs; ocreae glabrous or pubescent; peduncles stipitate-glandular along entire length</description>
      <determination>9 Persicaria chinensis</determination>
    </key_statement>
  </key>
</bio:treatment>