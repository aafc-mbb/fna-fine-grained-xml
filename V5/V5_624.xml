<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">312</other_info_on_meta>
    <other_info_on_meta type="mention_page">309</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="mention_page">313</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="1836" rank="species">nudum</taxon_name>
    <taxon_name authority="(Bentham) J. P. Tracy ex Jepson" date="1914" rank="variety">auriculatum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Calif.</publication_title>
      <place_in_publication>1: 420. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species nudum;variety auriculatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060415</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">auriculatum</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>17: 412. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species auriculatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="species">latifolium</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="variety">alternans</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species latifolium;variety alternans;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">latifolium</taxon_name>
    <taxon_name authority="(Bentham) S. Stokes" date="unknown" rank="subspecies">auriculatum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species latifolium;subspecies auriculatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–15 (–20) dm.</text>
      <biological_entity id="o17111" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems occasionally fistulose, 2–5 (–10) dm, glabrous.</text>
      <biological_entity id="o17112" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s1" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sheathing;</text>
      <biological_entity id="o17113" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 3–7 × 2–4 cm, tomentose abaxially, subglabrous or glabrous adaxially, margins strongly undulate-crisped.</text>
      <biological_entity id="o17114" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17115" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="undulate-crisped" value_original="undulate-crisped" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences cymose, 30–100 (–150) × 10–80 cm;</text>
      <biological_entity id="o17116" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="150" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s4" to="100" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s4" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches glabrous.</text>
      <biological_entity id="o17117" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres (2–) 3–5 per cluster, 3–4 mm, glabrous or sparsely pubescent.</text>
      <biological_entity id="o17118" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s6" to="3" to_inclusive="false" />
        <character char_type="range_value" constraint="per cluster" from="3" name="quantity" src="d0_s6" to="5" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 2.5–3 mm;</text>
      <biological_entity id="o17119" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth white to pink, rarely yellowish, glabrous or sparsely pubescent.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 80.</text>
      <biological_entity id="o17120" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17121" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly or clayey flats and slopes, mixed grassland and chaparral communities, oak and conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly" />
        <character name="habitat" value="sandy to clayey flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>91i.</number>
  <other_name type="common_name">Ear-shaped wild buckwheat</other_name>
  <discussion>Variety auriculatum is a highly variable taxon that occurs along the coast and in the adjacent mountains (Alameda, Contra Costa, Humboldt, Lake, Marin, Monterey, Napa, San Benito, San Francisco, San Luis Obispo, San Mateo, Santa Clara, Santa Cruz, Solano, Sonoma, and western Stanislaus counties).</discussion>
  <discussion>A related and as yet undescribed variety, known only from the Antioch sand dunes area of Contra Costa County, California, is the host for the federally endangered Lange’s metalmark butterfly (Apodemia mormo langei).</discussion>
  
</bio:treatment>