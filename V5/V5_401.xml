<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">197</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="S. Watson" date="1882" rank="species">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 366. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species parishii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060875</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silene</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">parishii</taxon_name>
    <taxon_name authority="C. L. Hitchcock &amp; Maguire" date="unknown" rank="variety">latifolia</taxon_name>
    <taxon_hierarchy>genus Silene;species parishii;variety latifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silene</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">parishii</taxon_name>
    <taxon_name authority="C. L. Hitchcock &amp; Maguire" date="unknown" rank="variety">viscida</taxon_name>
    <taxon_hierarchy>genus Silene;species parishii;variety viscida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o7295" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot stout;</text>
      <biological_entity id="o7296" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex much-branched, woody.</text>
      <biological_entity id="o7297" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="much-branched" value_original="much-branched" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems many, decumbent to erect, usually simple, 10–40 cm, woody, pilose and glandular (rarely eglandular proximally).</text>
      <biological_entity id="o7298" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="many" value_original="many" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="40" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="woody" value_original="woody" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves in 5–8 pairs, sessile but proximal ones narrowed into pseudopetiole, largest in midstem region, reduced proximally, scalelike at base, blade narrowly lanceolate to oblanceolate-elliptic or obovate, 2–6 cm × 3–20 mm, apex acute and acuminate, usually thick, leathery, densely puberulent and viscid-glandular or eglandular.</text>
      <biological_entity id="o7299" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o7300" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7301" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="into pseudopetiole" constraintid="o7302" is_modifier="false" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
        <character constraint="at base" constraintid="o7304" is_modifier="false" name="shape" src="d0_s4" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o7302" name="pseudopetiole" name_original="pseudopetiole" src="d0_s4" type="structure">
        <character constraint="in midstem region" constraintid="o7303" is_modifier="false" name="size" notes="" src="d0_s4" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o7303" name="region" name_original="region" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="size" notes="" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o7304" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o7305" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s4" to="oblanceolate-elliptic or obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7306" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="usually" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="viscid-glandular" value_original="viscid-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o7299" id="r783" name="in" negation="false" src="d0_s4" to="o7300" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymose, open or congested, 3–15 (–30) -flowered, sometimes compound, leafy.</text>
      <biological_entity id="o7307" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="congested" value_original="congested" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-15(-30)-flowered" value_original="3-15(-30)-flowered" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels to 1 (–1.5) cm, shortly pilose, viscid-glandular, flowers sometimes sessile.</text>
      <biological_entity id="o7308" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="viscid-glandular" value_original="viscid-glandular" />
      </biological_entity>
      <biological_entity id="o7309" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx prominently 10-veined, tubular, clavate in fruit, constricted proximally around carpophore, (20–) 25–30 × 4–7 mm, papery, densely glandular-puberulent, viscid, veins parallel, green, with pale commissures, lobes narrowly lanceolate, acuminate, 5–8 mm, herbaceous;</text>
      <biological_entity id="o7310" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7311" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s7" value="10-veined" value_original="10-veined" />
        <character is_modifier="false" name="shape" src="d0_s7" value="tubular" value_original="tubular" />
        <character constraint="in fruit" constraintid="o7312" is_modifier="false" name="shape" src="d0_s7" value="clavate" value_original="clavate" />
        <character constraint="proximally around carpophore" constraintid="o7313" is_modifier="false" name="size" notes="" src="d0_s7" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_length" notes="" src="d0_s7" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" notes="" src="d0_s7" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" notes="" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="papery" value_original="papery" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="coating" src="d0_s7" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o7312" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o7313" name="carpophore" name_original="carpophore" src="d0_s7" type="structure" />
      <biological_entity id="o7314" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o7315" name="commissure" name_original="commissures" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o7316" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <relation from="o7314" id="r784" name="with" negation="false" src="d0_s7" to="o7315" />
    </statement>
    <statement id="d0_s8">
      <text>corolla pale greenish yellow to white, clawed, claw equaling calyx, ligulate, as broad as limb, limb 7–8 mm, deeply laciniate into 6 or more linear lobes, appendages oblong, laciniate, 2 mm;</text>
      <biological_entity id="o7317" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o7318" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale greenish" value_original="pale greenish" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o7319" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="ligulate" value_original="ligulate" />
      </biological_entity>
      <biological_entity id="o7320" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="true" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o7321" name="limb" name_original="limb" src="d0_s8" type="structure" />
      <biological_entity id="o7322" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character constraint="into lobes" constraintid="o7323" is_modifier="false" modifier="deeply" name="shape" src="d0_s8" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity id="o7323" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o7324" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="laciniate" value_original="laciniate" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <relation from="o7319" id="r785" name="as broad as" negation="false" src="d0_s8" to="o7321" />
    </statement>
    <statement id="d0_s9">
      <text>stamens equaling calyx;</text>
      <biological_entity id="o7325" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7326" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity id="o7327" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="true" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 3, exserted.</text>
      <biological_entity id="o7328" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7329" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules equaling calyx, opening by 6 ascending teeth;</text>
      <biological_entity id="o7330" name="capsule" name_original="capsules" src="d0_s11" type="structure" />
      <biological_entity id="o7331" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o7332" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="6" value_original="6" />
        <character is_modifier="true" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o7330" id="r786" name="opening by" negation="false" src="d0_s11" to="o7332" />
    </statement>
    <statement id="d0_s12">
      <text>carpophore ca. 3 mm.</text>
      <biological_entity id="o7333" name="carpophore" name_original="carpophore" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds brown, reniform, 1.5–2 mm, margins papillate;</text>
      <biological_entity id="o7334" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7335" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>papillae large, inflated.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 48.</text>
      <biological_entity id="o7336" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="large" value_original="large" />
        <character is_modifier="false" name="shape" src="d0_s14" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7337" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring and summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky ledges and slopes, stream banks, open coniferous woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky ledges" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="open coniferous woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40.</number>
  <other_name type="common_name">Parish’s catchfly</other_name>
  <discussion>Silene parishii varies considerably in pubescence and leaf shape. C. L. Hitchcock and B. Maguire (1947) recognized three varieties on the basis of this variation. However, the characters vary independently and have only a weak geographical correlation. Hence recognition of the three varieties serves little useful purpose.</discussion>
  <discussion>Silene parishii is confined to the mountains of southern California.</discussion>
  
</bio:treatment>