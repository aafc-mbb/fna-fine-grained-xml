<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
    <other_info_on_meta type="illustration_page">436</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Small" date="1898" rank="genus">acanthoscyphus</taxon_name>
    <taxon_name authority="(Parry) Small" date="1898" rank="species">parishii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">parishii</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus acanthoscyphus;species parishii;variety parishii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060005</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="(E. A. McGregor) S. Stokes" date="unknown" rank="species">abramsii</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="subspecies">acanthoscyphus</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species abramsii;subspecies acanthoscyphus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, 1–6 dm.</text>
      <biological_entity id="o17495" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually stout, (1–) 5–15 cm.</text>
      <biological_entity id="o17496" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 1–7 cm.</text>
      <biological_entity id="o17497" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: bracts (2–) 3, triangular, awns 0.2–0.5 mm.</text>
      <biological_entity id="o17498" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o17499" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s3" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s3" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o17500" name="awn" name_original="awns" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles (1–) 2–5 (–7.5) cm.</text>
      <biological_entity id="o17501" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres: awns (10–) 13–30 (–36), ivory colored, (3–) 4–5 mm. 2n = 40.</text>
      <biological_entity id="o17502" name="involucre" name_original="involucres" src="d0_s5" type="structure" />
      <biological_entity id="o17503" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s5" to="13" to_inclusive="false" />
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="36" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s5" to="30" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="ivory colored" value_original="ivory colored" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17504" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, chaparral communities, montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="conifer woodlands" modifier="montane" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900-2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <other_name type="common_name">Parish’s flowery puncturebract</other_name>
  <discussion>Variety parishii occurs from the Pine Mountains, Ventura County, eastward through the San Gabriel Mountains to the central San Bernardino Mountains of San Bernardino County. It is the most common expression of the species.</discussion>
  
</bio:treatment>