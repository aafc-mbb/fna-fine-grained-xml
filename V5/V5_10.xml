<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Tanfani in F. Parlatore" date="unknown" rank="subfamily">Polycarpoideae</taxon_name>
    <taxon_name authority="Willdenow ex Schultes in J. J. Roemer et al." date="1819" rank="genus">drymaria</taxon_name>
    <taxon_name authority="Bentham" date="1839" rank="species">laxiflora</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>73. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily polycarpoideae;genus drymaria;species laxiflora;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060120</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Drymaria</taxon_name>
    <taxon_name authority="Briquet" date="unknown" rank="species">chihuahuensis</taxon_name>
    <taxon_hierarchy>genus Drymaria;species chihuahuensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial, herbaceous, glabrous or densely stipitate-glandular, not glaucous.</text>
      <biological_entity id="o1612" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s0" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sprawling, branching proximally, 10–30+ cm.</text>
      <biological_entity id="o1613" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o1614" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules ± persistent, divided into 2 or 3 filiform segments, 0.5–3.5 mm;</text>
      <biological_entity id="o1615" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="into segments" constraintid="o1616" is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1616" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (1–) 3–6 mm;</text>
      <biological_entity id="o1617" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade broadly ovate, deltate, cordate, or reniform, (0.2–) 0.5–1.6 cm × 4–12 mm, base truncate to rounded, apex acute to cuspidate.</text>
      <biological_entity id="o1618" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_length" src="d0_s5" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.6" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1619" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity id="o1620" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, open, 3–15-flowered cymes.</text>
      <biological_entity id="o1621" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o1622" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="3-15-flowered" value_original="3-15-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels shorter to longer than subtending bracts at maturity.</text>
      <biological_entity id="o1623" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character constraint="than subtending bracts" constraintid="o1624" is_modifier="false" name="size_or_length" src="d0_s7" value="shorter to longer" value_original="shorter to longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o1624" name="bract" name_original="bracts" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals with 3 distinct veins arcing outward at midsection and ± confluent apically, lanceovate to elliptic (herbaceous portion linear-lanceolate to narrowly ovate), 3–4 (–5) mm, subequal, apex acute to acuminate (herbaceous portion often obtuse), not hooded, glabrous;</text>
      <biological_entity id="o1625" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1626" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less; apically" name="arrangement" src="d0_s8" value="confluent" value_original="confluent" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o1627" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="true" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o1628" name="midsection" name_original="midsection" src="d0_s8" type="structure" />
      <biological_entity id="o1629" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o1626" id="r173" name="with" negation="false" src="d0_s8" to="o1627" />
      <relation from="o1627" id="r174" name="arcing outward" negation="false" src="d0_s8" to="o1628" />
    </statement>
    <statement id="d0_s9">
      <text>petals 2-fid for 1/2–3/4 their length, 2.7–6 mm, subequal to sepals, lobes 1-veined, vein dichotomously branched distally, oblong, trunk entire, base abruptly tapered, apex deeply notched.</text>
      <biological_entity id="o1630" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1631" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="length" src="d0_s9" value="2-fid" value_original="2-fid" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character constraint="to sepals" constraintid="o1632" is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o1632" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o1633" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o1634" name="vein" name_original="vein" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="dichotomously; distally" name="architecture" src="d0_s9" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o1635" name="trunk" name_original="trunk" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o1636" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o1637" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s9" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds dark-brown to blackish, snail-shell-shaped, 0.5–0.7 mm;</text>
      <biological_entity id="o1638" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s10" to="blackish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="snail-shell--shaped" value_original="snail-shell--shaped" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tubercles prominent, conic.</text>
      <biological_entity id="o1639" name="tubercle" name_original="tubercles" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane chaparral and woodlands, igneous-rock mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane chaparral" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="igneous-rock mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila, San Luis Potosí, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  
</bio:treatment>