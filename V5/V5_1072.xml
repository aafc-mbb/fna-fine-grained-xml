<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">522</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="mention_page">521</other_info_on_meta>
    <other_info_on_meta type="mention_page">523</other_info_on_meta>
    <other_info_on_meta type="mention_page">524</other_info_on_meta>
    <other_info_on_meta type="illustration_page">520</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rumex Linnaeus Sect.. Rumex" date="unknown" rank="section">Rumex</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">crispus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 335. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section rumex;species crispus;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200006746</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lapathum</taxon_name>
    <taxon_name authority="(Linnaeus) Scopoli" date="unknown" rank="species">crispum</taxon_name>
    <taxon_hierarchy>genus Lapathum;species crispum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, occasionally biennial, glabrous or very indistinctly papillose normally only on veins of leaf-blades abaxially, with fusiform, vertical rootstock.</text>
      <biological_entity id="o11254" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="occasionally" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="on veins" constraintid="o11255" is_modifier="false" modifier="very indistinctly; normally only" name="relief" src="d0_s0" value="papillose" value_original="papillose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11255" name="vein" name_original="veins" src="d0_s0" type="structure" />
      <biological_entity id="o11256" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure" />
      <biological_entity id="o11257" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="fusiform" value_original="fusiform" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o11255" id="r1245" name="part_of" negation="false" src="d0_s0" to="o11256" />
      <relation from="o11254" id="r1246" name="with" negation="false" src="d0_s0" to="o11257" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched distal to middle, 40–100 (–150) cm.</text>
      <biological_entity id="o11258" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="position" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="distal" name="position" src="d0_s1" to="middle" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="150" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: ocrea deciduous, rarely partially persistent at maturity;</text>
      <biological_entity id="o11259" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o11260" name="ocreum" name_original="ocrea" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character constraint="at maturity" is_modifier="false" modifier="rarely partially" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate to narrowly lanceolate or lanceolate-linear, normally 15–30 (–35) × 2–6 cm, base cuneate, truncate, or weakly cordate, margins entire to subentire, strongly crisped and undulate, apex acute.</text>
      <biological_entity id="o11261" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11262" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="narrowly lanceolate or lanceolate-linear" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="35" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11263" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="normally" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o11264" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s3" to="subentire" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o11265" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, occupying distal 1/2 of stem, dense or interrupted at base, narrowly to broadly paniculate, branches usually straight or arcuate.</text>
      <biological_entity id="o11266" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character constraint="at base" constraintid="o11268" is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="narrowly to broadly" name="arrangement" notes="" src="d0_s4" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o11267" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o11268" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o11269" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s4" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <relation from="o11266" id="r1247" name="occupying" negation="false" src="d0_s4" to="o11267" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels articulated in proximal 1/3, filiform, (3–) 4–8 mm, articulation distinctly swollen.</text>
      <biological_entity id="o11270" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="in proximal 1/3" constraintid="o11271" is_modifier="false" name="architecture" src="d0_s5" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11271" name="1/3" name_original="1/3" src="d0_s5" type="structure" />
      <biological_entity id="o11272" name="articulation" name_original="articulation" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s5" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 10–25 in whorls;</text>
      <biological_entity id="o11273" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o11274" from="10" name="quantity" src="d0_s6" to="25" />
      </biological_entity>
      <biological_entity id="o11274" name="whorl" name_original="whorls" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>inner tepals orbiculate-ovate or ovate-deltoid, 3.5–6 × 3–5 mm, base truncate or subcordate, margins entire or subentire to very weakly erose, flat, apex obtuse or subacute;</text>
      <biological_entity constraint="inner" id="o11275" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="orbiculate-ovate" value_original="orbiculate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-deltoid" value_original="ovate-deltoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11276" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o11277" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character constraint="to apex" constraintid="o11278" is_modifier="false" name="shape" src="d0_s7" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o11278" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="very weakly" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tubercles normally 3, rarely 1 or 2, unequal, at least 1 distinctly larger, more than (1–) 1.5 mm wide.</text>
      <biological_entity id="o11279" name="tubercle" name_original="tubercles" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character modifier="rarely" name="quantity" src="d0_s8" unit="or" value="1" value_original="1" />
        <character modifier="rarely" name="quantity" src="d0_s8" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character modifier="at least" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" modifier="distinctly" name="size" src="d0_s8" value="larger" value_original="larger" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="more than" name="width" src="d0_s8" to="1.5" to_inclusive="false" to_unit="mm" />
        <character modifier="more than" name="width" src="d0_s8" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes usually reddish-brown, 2–3 × 1.5–2 mm. 2n = 60.</text>
      <biological_entity id="o11280" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11281" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Very broad range of ruderal, segetal, and seminatural habitats, disturbed soil, waste places, cultivated fields, roadsides, meadows, shores of water bodies, edges of woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="broad range" modifier="very" constraint="of ruderal" />
        <character name="habitat" value="ruderal" />
        <character name="habitat" value="segetal" />
        <character name="habitat" value="seminatural habitats" modifier="and" />
        <character name="habitat" value="disturbed soil" />
        <character name="habitat" value="waste" />
        <character name="habitat" value="fields" modifier="cultivated" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="shores" constraint="of water bodies" />
        <character name="habitat" value="water bodies" />
        <character name="habitat" value="edges" constraint="of woods" />
        <character name="habitat" value="woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Eurasia; introduced almost worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="almost worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>47.</number>
  <other_name type="common_name">Curly or yellow dock</other_name>
  <other_name type="common_name">patience crépue</other_name>
  <other_name type="common_name">rumex crépu</other_name>
  <other_name type="common_name">reguette</other_name>
  <discussion>Rumex crispus (belonging to subsect. Crispi Rechinger f.; see K. H. Rechinger 1937) is the most widespread and ecologically successful species of the genus, occuring almost worldwide as a completely naturalized and sometimes invasive alien. It has not been reported from Greenland, but it probably occurs there.</discussion>
  <discussion>Rumex crispus hybridizes with many other species of subg. Rumex. Hybrids with R. obtusifolius (Rumex ×pratensis Mertens &amp; Koch) are the most common in the genus, at least in Europe, and have been reported for several localities in North America. Rumex crispus × R. patientia (Rumex ×confusus Simonkai) was reported from New York. According to R. S. Mitchell (1986, p. 47), “this hybrid is now spreading along highway shoulders, and it has replaced R. crispus in some local areas.” However, that information should be confirmed by more detailed studies since spontaneous hybrids between species of sect. Rumex usually are much less fertile and ecologically successful than the parental species. Hybrids of Rumex occuring in North America need careful revision.</discussion>
  <discussion>Numerous infraspecific taxa and even segregate species have been described in the Rumex crispus aggregate. Many seem to represent minor variation of little or no taxonomic significance, but some are geographically delimited entities that may deserve recognition as subspecies or varieties. The typical variety has inner tepals with three well-developed tubercles; the less common var. unicallosus Petermann, with one tubercle, occurs sporadically in North America.</discussion>
  <discussion>Some eastern Asian plants differ from typical Rumex crispus is having somewhat smaller inner tepals, longer pedicels, lax inflorescences with remote whorls, and narrower leaves that are almost flat or indistinctly undulate at the margins. These plants, originally described as R. fauriei Rechinger f., are now treated as R. crispus subsp. fauriei (Rechinger f.) Mosyakin &amp; W. L. Wagner; the subspecies was recently reported from Hawaii (S. L. Mosyakin and W. L. Wagner 1998) and may be expected as introduced in western North America.</discussion>
  
</bio:treatment>