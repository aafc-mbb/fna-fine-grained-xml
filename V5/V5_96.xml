<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John W. Thieret,Richard K. Rabeler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="subfamily">Paronychioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CORRIGIOLA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 271. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 132. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily paronychioideae;genus corrigiola;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin corrigia, shoelace, perhaps alluding to the slender stems</other_info_on_name>
    <other_info_on_name type="fna_id">108066</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or biennial [perennial].</text>
      <biological_entity id="o11374" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots slender.</text>
      <biological_entity id="o11375" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent to ascending, branched, terete.</text>
      <biological_entity id="o11376" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate or rarely subopposite, sometimes forming basal rosette, not connate, petiolate or sessile;</text>
      <biological_entity id="o11377" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s3" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" modifier="sometimes; not" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o11378" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o11377" id="r1258" modifier="sometimes" name="forming" negation="false" src="d0_s3" to="o11378" />
    </statement>
    <statement id="d0_s4">
      <text>stipules 2 per node, white, ovate, margins mostly entire, apex acuminate;</text>
      <biological_entity id="o11379" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o11380" name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o11380" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity id="o11381" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11382" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole minute (subopposite leaves);</text>
      <biological_entity id="o11383" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade 1-veined, narrowly oblanceolate to spatulate, not succulent, apex obtuse.</text>
      <biological_entity id="o11384" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s6" to="spatulate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s6" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o11385" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal or axillary, dense cymes;</text>
      <biological_entity id="o11386" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o11387" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="true" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts paired, smaller, scarious.</text>
      <biological_entity id="o11388" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="paired" value_original="paired" />
        <character is_modifier="false" name="size" src="d0_s8" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect.</text>
      <biological_entity id="o11389" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: perianth and androecium slightly perigynous;</text>
      <biological_entity id="o11390" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11391" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s10" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o11392" name="androecium" name_original="androecium" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s10" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>hypanthium cupshaped, not abruptly expanded distally;</text>
      <biological_entity id="o11393" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11394" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cup-shaped" value_original="cup-shaped" />
        <character is_modifier="false" modifier="not abruptly; distally" name="size" src="d0_s11" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals 5, distinct, green or reddish-brown, 1–1.5 mm, herbaceous, margins white-scarious, apex blunt, not hooded, not awned;</text>
      <biological_entity id="o11395" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o11396" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s12" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o11397" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="white-scarious" value_original="white-scarious" />
      </biological_entity>
      <biological_entity id="o11398" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="hooded" value_original="hooded" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectaries not apparent;</text>
      <biological_entity id="o11399" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o11400" name="nectary" name_original="nectaries" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s13" value="apparent" value_original="apparent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 5;</text>
      <biological_entity id="o11401" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o11402" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments distinct;</text>
      <biological_entity id="o11403" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o11404" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>staminodes 5, arising from hypanthium rim, ovate to oblong;</text>
      <biological_entity id="o11405" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o11406" name="staminode" name_original="staminodes" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character constraint="from hypanthium rim" constraintid="o11407" is_modifier="false" name="orientation" src="d0_s16" value="arising" value_original="arising" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s16" to="oblong" />
      </biological_entity>
      <biological_entity constraint="hypanthium" id="o11407" name="rim" name_original="rim" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles 3, distinct, capitate, 0.1–0.2 mm, glabrous proximally;</text>
      <biological_entity id="o11408" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o11409" name="style" name_original="styles" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s17" to="0.2" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas 3, capitate, obscurely papillate (at 50×).</text>
      <biological_entity id="o11410" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o11411" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s18" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Utricles at least partly enclosed by persistent calyx/hypanthium, obscurely 3-gonous, indehiscent.</text>
      <biological_entity id="o11412" name="utricle" name_original="utricles" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s19" value="3-gonous" value_original="3-gonous" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o11413" name="hypanthium" name_original="calyx/hypanthium" src="d0_s19" type="structure">
        <character is_modifier="true" name="duration" src="d0_s19" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o11412" id="r1259" modifier="at-least partly" name="enclosed by" negation="false" src="d0_s19" to="o11413" />
    </statement>
    <statement id="d0_s20">
      <text>Seeds white with tan band, reniform or subglobose, not compressed, finely papillose, marginal wing absent, appendage absent;</text>
      <biological_entity id="o11414" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character constraint="with band" constraintid="o11415" is_modifier="false" name="coloration" src="d0_s20" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s20" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s20" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s20" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s20" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o11415" name="band" name_original="band" src="d0_s20" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s20" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o11416" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11417" name="appendage" name_original="appendage" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>embryo peripheral, curved.</text>
    </statement>
    <statement id="d0_s22">
      <text>x = 8, 9.</text>
      <biological_entity id="o11418" name="embryo" name_original="embryo" src="d0_s21" type="structure">
        <character is_modifier="false" name="position" src="d0_s21" value="peripheral" value_original="peripheral" />
        <character is_modifier="false" name="course" src="d0_s21" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="x" id="o11419" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="8" value_original="8" />
        <character name="quantity" src="d0_s22" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; South America, Eurasia, Africa; introduced elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="elsewhere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">Strapwort</other_name>
  <discussion>Species ca. 10 (1 in the flora).</discussion>
  <references>
    <reference>Coker, P. D. 1962. Corrigiola litoralis L. J. Ecol. 50: 833–840.</reference>
    <reference>Gilbert, M. G. 1987. The taxonomic position of the genera Telephium and Corrigiola. Taxon 36: 47–49.</reference>
    <reference>Vogel, A. 1997. Die Verbreitung, Vergesellschaftung und Populationsökologie von Corrigiola litoralis, Illecebrum verticillatum und Herniaria glabra (Illecebraceae). Berlin.</reference>
  </references>
  
</bio:treatment>