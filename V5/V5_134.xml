<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(Abrams) R. L. Hartman &amp; Rabeler" date="2004" rank="species">ferrisiae</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 754. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species ferrisiae;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060144</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">macradenia</taxon_name>
    <taxon_name authority="Abrams" date="unknown" rank="subspecies">ferrisiae</taxon_name>
    <place_of_publication>
      <publication_title>in L. Abrams and R. S. Ferris, Ill. Fl. Pacific States</publication_title>
      <place_in_publication>2: 151. 1944</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species macradenia;subspecies ferrisiae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eremogone</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">macradenia</taxon_name>
    <taxon_name authority="(Abrams) R. L. Hartman &amp; Rabeler" date="unknown" rank="variety">ferrisiae</taxon_name>
    <taxon_hierarchy>genus Eremogone;species macradenia;variety ferrisiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tufted, green, not glaucous, with woody base.</text>
      <biological_entity id="o13086" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13087" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o13086" id="r1423" name="with" negation="false" src="d0_s0" to="o13087" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, (10–) 20–40 (–100) cm, glabrous to stipitate-glandular.</text>
      <biological_entity id="o13088" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s1" to="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves sparse or absent;</text>
      <biological_entity id="o13089" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o13090" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves usually in 5–7 pairs, not significantly reduced;</text>
      <biological_entity id="o13091" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o13092" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not significantly" name="size" notes="" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o13093" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <relation from="o13092" id="r1424" name="in" negation="false" src="d0_s3" to="o13093" />
    </statement>
    <statement id="d0_s4">
      <text>basal blades ascending, needlelike or narrowly linear, 2–6 (–7) cm × 0.5–1 mm, ± rigid, herbaceous to subsucculent, apex blunt to spinose, usually glabrous, not glaucous.</text>
      <biological_entity id="o13094" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o13095" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s4" value="needlelike" value_original="needlelike" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o13096" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s4" to="spinose" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 10–30 (–80) -flowered, diffuse cymes;</text>
      <biological_entity id="o13097" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="10-30(-80)-flowered" value_original="10-30(-80)-flowered" />
      </biological_entity>
      <biological_entity id="o13098" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches spreading.</text>
      <biological_entity id="o13099" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 15–55 mm, glabrous or stipitate-glandular.</text>
      <biological_entity id="o13100" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="55" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 1–3-veined, ovate to lanceolate or elliptic, 3–4.3 mm, to 5.5 mm in fruit, margins narrow to broad, apex acute to acuminate, glabrous to sparsely stipitate-glandular;</text>
      <biological_entity id="o13101" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13102" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-3-veined" value_original="1-3-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate or elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.3" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o13103" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13103" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o13104" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="narrow" name="width" src="d0_s8" to="broad" />
      </biological_entity>
      <biological_entity id="o13105" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s8" to="sparsely stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white or yellowish, oblanceolate to spatulate, 6–9 mm, 1–1.5 times as long as sepals, apex entire or erose;</text>
      <biological_entity id="o13106" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13107" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s9" to="spatulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character constraint="sepal" constraintid="o13108" is_modifier="false" name="length" src="d0_s9" value="1-1.5 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o13108" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o13109" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nectaries as lateral and abaxial rounding of base of filaments opposite sepals, 0.3–0.4 mm.</text>
      <biological_entity id="o13110" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13111" name="nectary" name_original="nectaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral and abaxial" id="o13112" name="rounding" name_original="rounding" src="d0_s10" type="structure" />
      <biological_entity id="o13113" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o13114" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o13115" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o13111" id="r1425" name="as" negation="false" src="d0_s10" to="o13112" />
      <relation from="o13112" id="r1426" name="part_of" negation="false" src="d0_s10" to="o13113" />
      <relation from="o13112" id="r1427" name="part_of" negation="false" src="d0_s10" to="o13114" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules 6–7 mm, glabrous.</text>
      <biological_entity id="o13116" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds reddish-brown to blackish, suborbicular to pyriform or ovoid, 1.3–3.2 mm, tuberculate;</text>
      <biological_entity id="o13117" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s12" to="blackish" />
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s12" to="pyriform or ovoid" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s12" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tubercles low, rounded to conic.</text>
      <biological_entity id="o13118" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="low" value_original="low" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s13" to="conic" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine and oak woodlands, granitic alluvium on foothills and mountain slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="granitic alluvium" constraint="on foothills and mountain slopes" />
        <character name="habitat" value="foothills" />
        <character name="habitat" value="mountain slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Ferris’s sandwort</other_name>
  <discussion>We now believe that Eremogone macradenia (in the sense of R. L. Hartman 1993) should be split into two species, with E. macradenia var. ferrisiae (Abrams) R. L. Hartman &amp; Rabeler being elevated to species rank (Hartman and R. K. Rabeler 2004), as here. This became particularly obvious when comparing nectary morphology of E. macradenia (rectangular, two-lobed or truncate, 0.7–1.5 mm or narrowly longitudinally rectangular, truncate, densely minutely pubescent with erect to spreading hairs, 0.7–0.8 mm) with that of E. ferrisiae (rounded, 0.3–0.4 mm). Furthermore, the nectary types correlate well with sepal size and inflorescence type, as indicated in the key. This disposition agrees with the conclusions of M. F. Baad (1969).</discussion>
  
</bio:treatment>