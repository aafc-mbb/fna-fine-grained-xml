<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">190</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="Cavanilles" date="1801" rank="species">laciniata</taxon_name>
    <taxon_name authority="(Durand) J. K. Morton" date="2004" rank="subspecies">californica</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 888. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species laciniata;subspecies californica;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060863</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silene</taxon_name>
    <taxon_name authority="Durand" date="unknown" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia, n. s.</publication_title>
      <place_in_publication>3: 83. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Silene;species californica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silene</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laciniata</taxon_name>
    <taxon_name authority="(Durand) A. Gray" date="unknown" rank="variety">californica</taxon_name>
    <taxon_hierarchy>genus Silene;species laciniata;variety californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems decumbent at base, straggling to erect, leafy throughout, sparsely branched, scaly proximally below-ground, with soft, short pubescence.</text>
      <biological_entity id="o3951" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="at base" constraintid="o3952" is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="straggling" name="orientation" notes="" src="d0_s0" to="erect" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s0" value="scaly" value_original="scaly" />
        <character is_modifier="false" modifier="proximally" name="location" src="d0_s0" value="below-ground" value_original="below-ground" />
      </biological_entity>
      <biological_entity id="o3952" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: proximal blades oblanceolate, sometimes broadly so, narrowed into short pseudopetiole, 2–6 cm × 6–25 mm, shortly pubescent abaxially, subglabrous adaxially;</text>
      <biological_entity id="o3953" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o3954" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="oblanceolate" value_original="oblanceolate" />
        <character constraint="into pseudopetiole" constraintid="o3955" is_modifier="false" modifier="sometimes broadly; broadly" name="shape" src="d0_s1" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" notes="" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" notes="" src="d0_s1" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="shortly; abaxially" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s1" value="subglabrous" value_original="subglabrous" />
      </biological_entity>
      <biological_entity id="o3955" name="pseudopetiole" name_original="pseudopetiole" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>distal sessile, blade lanceolate or ovatelanceolate to broadly elliptic, 2–9 cm × 5–30 mm, apex acute to shortly acuminate.</text>
      <biological_entity id="o3956" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o3957" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o3958" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="broadly elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3959" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="shortly acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences poorly developed, usually 1–3 (–5) -flowered, open cymes, bracteate;</text>
      <biological_entity id="o3960" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="poorly" name="development" src="d0_s3" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="1-3(-5)-flowered" value_original="1-3(-5)-flowered" />
      </biological_entity>
      <biological_entity id="o3961" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts leaflike, lanceolate to ovatelanceolate.</text>
      <biological_entity id="o3962" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyces broadly tubular, widened distally, 15–25 × 4–8 mm in flower, obovate to turbinate in fruit and to 13 mm broad, more than 1/2 as broad as long.</text>
      <biological_entity id="o3963" name="calyx" name_original="calyces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="tubular" value_original="tubular" />
        <character is_modifier="false" modifier="distally" name="width" src="d0_s5" value="widened" value_original="widened" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" constraint="in flower" constraintid="o3964" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o3965" from="obovate" name="shape" notes="" src="d0_s5" to="turbinate" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" value="/2" value_original="/2" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o3964" name="flower" name_original="flower" src="d0_s5" type="structure" />
      <biological_entity id="o3965" name="fruit" name_original="fruit" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" notes="" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules ovoid.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 48, 72.</text>
      <biological_entity id="o3966" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3967" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="48" value_original="48" />
        <character name="quantity" src="d0_s7" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open woodlands, chaparral, rocky hillsides and cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woodlands" modifier="dry" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27b.</number>
  <other_name type="common_name">California pink</other_name>
  <discussion>The recently described Silene serpentinicola is similar to subsp. californica but differs in its short, erect, more or less solitary flowering stems and the much larger appendages of the flowers.</discussion>
  
</bio:treatment>