<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Craig C. Freeman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">534</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">POLYGONELLA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 240. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonella;</taxon_hierarchy>
    <other_info_on_name type="etymology">genus name Polygonum and Latin - ella, diminutive</other_info_on_name>
    <other_info_on_name type="fna_id">126395</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, subshrubs, or herbs, perennial or annual, synoecious, dioecious, gynodioecious, or gynomonoecious;</text>
      <biological_entity id="o11550" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="synoecious" value_original="synoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynodioecious" value_original="gynodioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynomonoecious" value_original="gynomonoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynodioecious" value_original="gynodioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynomonoecious" value_original="gynomonoecious" />
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="synoecious" value_original="synoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynodioecious" value_original="gynodioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynomonoecious" value_original="gynomonoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynodioecious" value_original="gynodioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynomonoecious" value_original="gynomonoecious" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots woody.</text>
      <biological_entity id="o11553" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, decumbent, or prostrate, glabrous or scabrous.</text>
      <biological_entity id="o11554" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Branches adnate to stems, appearing to arise internodally.</text>
      <biological_entity id="o11555" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character constraint="to stems" constraintid="o11556" is_modifier="false" name="fusion" src="d0_s3" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o11556" name="stem" name_original="stems" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves deciduous or, rarely, with leaves persisting more than 1 year, sometimes fugacious, cauline, alternate;</text>
      <biological_entity id="o11557" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character name="duration" src="d0_s4" value="," value_original="," />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s4" value="fugacious" value_original="fugacious" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o11558" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persisting" value_original="persisting" />
      </biological_entity>
      <relation from="o11557" id="r1279" name="with" negation="false" src="d0_s4" to="o11558" />
    </statement>
    <statement id="d0_s5">
      <text>ocrea usually persistent, sometimes disintegrating with age and deciduous distally, chartaceous or coriaceous;</text>
      <biological_entity id="o11559" name="ocreum" name_original="ocrea" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character constraint="with age" constraintid="o11560" is_modifier="false" modifier="sometimes" name="dehiscence" src="d0_s5" value="disintegrating" value_original="disintegrating" />
        <character is_modifier="false" modifier="distally" name="duration" notes="" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="texture" src="d0_s5" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o11560" name="age" name_original="age" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>petiole apparently absent, articulate basally;</text>
      <biological_entity id="o11561" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="apparently" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s6" value="articulate" value_original="articulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade filiform to broadly obovate, margins entire.</text>
      <biological_entity id="o11562" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s7" to="broadly obovate" />
      </biological_entity>
      <biological_entity id="o11563" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, racemelike, pedunculate.</text>
      <biological_entity id="o11564" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s8" value="racemelike" value_original="racemelike" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels present.</text>
      <biological_entity id="o11565" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers bisexual, or some or all functionally unisexual, 1 per ocreate fascicle, base stipelike;</text>
      <biological_entity id="o11566" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="functionally" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
        <character constraint="per fascicle" constraintid="o11567" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o11567" name="fascicle" name_original="fascicle" src="d0_s10" type="structure" />
      <biological_entity id="o11568" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="stipelike" value_original="stipelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perianth nonaccrescent, white, pink, red, greenish, or yellowish, campanulate, glabrous;</text>
      <biological_entity id="o11569" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="nonaccrescent" value_original="nonaccrescent" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>tepals 5, distinct, petaloid, dimorphic, in 2 whorls with 2 outer and 3 inner or 2 outer and 2 inner plus 1 transitional;</text>
      <biological_entity id="o11570" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="transitional" value_original="transitional" />
      </biological_entity>
      <biological_entity id="o11571" name="whorl" name_original="whorls" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="outer" id="o11573" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11574" name="tepal" name_original="tepals" src="d0_s12" type="structure" />
      <biological_entity constraint="outer" id="o11576" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11577" name="tepal" name_original="tepals" src="d0_s12" type="structure" />
      <relation from="o11570" id="r1280" name="in" negation="false" src="d0_s12" to="o11571" />
      <relation from="o11571" id="r1281" name="with" negation="false" src="d0_s12" to="o11573" />
      <relation from="o11571" id="r1282" name="with" negation="false" src="d0_s12" to="o11576" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 8, in 2 series with 5 outer and 3 inner;</text>
      <biological_entity id="o11578" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="8" value_original="8" />
      </biological_entity>
      <biological_entity id="o11579" name="series" name_original="series" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o11580" name="outer" name_original="outer" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="5" value_original="5" />
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11581" name="tepal" name_original="tepals" src="d0_s13" type="structure" />
      <relation from="o11578" id="r1283" name="in" negation="false" src="d0_s13" to="o11579" />
      <relation from="o11579" id="r1284" name="with" negation="false" src="d0_s13" to="o11580" />
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct, free, dilated proximally, dimorphic, inner 3 dilated more abruptly than outer 5, with toothed or horned shoulders, or monomorphic (in P. fimbriata and P. robusta), glabrous (pubescent basally in P. basiramia);</text>
      <biological_entity id="o11582" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="free" value_original="free" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="position" src="d0_s14" value="inner" value_original="inner" />
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="outer" id="o11583" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o11584" name="shoulder" name_original="shoulders" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="toothed" value_original="toothed" />
        <character is_modifier="true" name="shape" src="d0_s14" value="horned" value_original="horned" />
      </biological_entity>
      <relation from="o11582" id="r1285" name="with" negation="false" src="d0_s14" to="o11584" />
    </statement>
    <statement id="d0_s15">
      <text>anthers white, yellow, orange, pink, or dark red, elliptic to ovate or round;</text>
      <biological_entity id="o11585" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark red" value_original="dark red" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s15" to="ovate or round" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles (2–) 3, erect, distinct;</text>
      <biological_entity id="o11586" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s16" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas (2–) 3, capitate.</text>
      <biological_entity id="o11587" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s17" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes included or exserted, yellowbrown, brown, or reddish-brown, wingless or narrowly winged, (2–) 3 (–4) -gonous, glabrous.</text>
      <biological_entity id="o11588" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s18" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="wingless" value_original="wingless" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s18" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s18" value="(2-)3(-4)-gonous" value_original="(2-)3(-4)-gonous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds: embryo straight or slightly curved.</text>
      <biological_entity id="o11589" name="seed" name_original="seeds" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>x = 11.</text>
      <biological_entity id="o11590" name="embryo" name_original="embryo" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s19" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="x" id="o11591" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e, sc United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="sc United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="common_name">Jointweed</other_name>
  <other_name type="common_name">wireweed</other_name>
  <other_name type="common_name">polygonelle</other_name>
  <discussion>Species 11 (11 in flora).</discussion>
  <discussion>Polygonella is distinct from other genera of Polygonaceae in having branches adnate to the stem and thus appearing to arise internodally. Palynological, anatomical, and morphological evidence suggests Polygonella is closely related to Polygonum sect. Duravia (L.-P. Ronse Decraene et al. 2004; Hong S. P. et al. 1998; P. O. Lewis 1991).</discussion>
  <discussion>Within-population allozyme diversity is lower in the two most widespread species of the genus as compared to their narrowly endemic congeners (P. O. Lewis and D. J. Crawford 1995). High levels of selfing or depletion of diversity due to Pleistocene glaciation have been suggested as possible explanations for the lower allozyme diversity within populations of Polygonella americana and P. articulata.</discussion>
  <references>
    <reference>Horton, J. H. 1963. A taxonomic revision of Polygonella (Polygonaceae). Brittonia 15: 177–203.</reference>
    <reference>Lewis, P. O. 1991. Allozyme Variation and Evolution in Polygonella (Polygonaceae). Ph.D. dissertation. Ohio State University.</reference>
    <reference>Nesom, G. L. and V. M. Bates. 1984. Reevaluation of infraspecific taxonomy in Polygonella (Polygonaceae). Brittonia 36: 37–44.</reference>
    <reference>Nesom, G. L. and V. M. Bates. 1984b. A phylogenetic reconstruction of Polygonella. [Abstract.] A. S. B. Bull. 31: 74.</reference>
    <reference>Ronse Decraene, L.-P., Hong S. P., and E. F. Smets. 2004. What is the taxonomic status of Polygonella? Evidence from floral morphology. Ann. Missouri Bot. Gard. 91: 320–345.</reference>
    <reference>Lewis, P. O. and D. J. Crawford. 1995. Pleistocene refugium endemics exhibit greater allozymic diversity than widespread congeners in the genus Polygonella (Polygonaceae). Amer. J. Bot. 82: 141–149.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Margins of inner tepal deeply fringed; filaments of stamens monomorphic</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Margins of inner tepal entire to erose; filaments of stamens dimorphic</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Margins of leaf blade not hyaline; stems scabrous or, sometimes, glabrous proximally</description>
      <determination>1 Polygonella fimbriata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Margins of leaf blade hyaline; stems glabrous or sparingly scabrous on angles distally</description>
      <determination>2 Polygonella robusta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ocreae margins ciliate</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ocreae margins not ciliate</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ocreolae not encircling rachis of raceme, their sides and bases adnate to rachis; inflorescences 2-6(-8) mm; outer tepals 0.5-0.9 mm in anthesis</description>
      <determination>8 Polygonella parksii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ocreolae encircling rachis of raceme, only their bases adnate to rachis; inflorescences 10-45 mm; outer tepals 0.7-1.5 mm in anthesis</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems branched at or below ground level</description>
      <determination>3 Polygonella basiramia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems simple, sometimes branched distally, if present, well above ground</description>
      <determination>4 Polygonella ciliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades (3-)9-30 mm wide, oblanceolate to obovate or broadly spatulate</description>
      <determination>6 Polygonella macrophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades 0.3-6(-8) mm wide, filiform to broadly spatulate</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Styles and stigmas 0.4-1 mm in anthesis; plants perennial</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Styles and stigmas ca. 0.1 mm or less in anthesis; plants annual or perennial</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stems erect; outer tepals sharply reflexed early in anthesis and in fruit</description>
      <determination>10 Polygonella americana</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stems prostrate; outer tepals loosely appressed in anthesis, sometimes spreading in fruit</description>
      <determination>11 Polygonella myriophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants perennial; margins of leaf blade hyaline at least along distal 1/ 2; outer tepals usually reflexed in fruit</description>
      <determination>7 Polygonella polygama</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants annual; margins of leaf blade not hyaline; outer tepals loosely appressed to spreading in fruit</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Pedicels 0.9-3 mm, longer than subtending ocreolae; leaf blades 0.4-1.2 mm wide</description>
      <determination>9 Polygonella articulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Pedicles 0.1-0.3 mm, as long as subtending ocreolae; leaf blades 0.8-5(-8) mm wide</description>
      <determination>5 Polygonella gracilis</determination>
    </key_statement>
  </key>
</bio:treatment>