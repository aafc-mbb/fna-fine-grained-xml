<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">344</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="(Small) Munz &amp; Reveal in P. A. Munz" date="1968" rank="variety">covillei</taxon_name>
    <place_of_publication>
      <publication_title>in P. A. Munz, Suppl. Calif. Fl.,</publication_title>
      <place_in_publication>43. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety covillei;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060545</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">covillei</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 42. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species covillei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">umbellatum</taxon_name>
    <taxon_name authority="(Small) Munz" date="unknown" rank="subspecies">covillei</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species umbellatum;subspecies covillei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, typically prostrate mats, 0.5–1 × 1–5 dm.</text>
      <biological_entity id="o1160" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o1161" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="typically" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="1" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems spreading to erect, 0.3–0.9 dm, thinly tomentose to nearly glabrous, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o1162" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s1" to="0.9" to_unit="dm" />
        <character char_type="range_value" from="thinly tomentose" name="pubescence" src="d0_s1" to="nearly glabrous" />
      </biological_entity>
      <biological_entity id="o1163" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o1162" id="r120" name="without" negation="false" src="d0_s1" to="o1163" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in tight rosettes;</text>
      <biological_entity id="o1164" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1165" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s2" value="tight" value_original="tight" />
      </biological_entity>
      <relation from="o1164" id="r121" name="in" negation="false" src="d0_s2" to="o1165" />
    </statement>
    <statement id="d0_s3">
      <text>blade usually narrowly elliptic, 0.3–0.6 (–1) × 0.2–0.4 (–0.6) cm, white-tomentose abaxially, slightly less so and greenish adaxially, margins plane.</text>
      <biological_entity id="o1166" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s3" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.4" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="slightly less; less; adaxially" name="coloration" src="d0_s3" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o1167" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences compact-umbellate;</text>
      <biological_entity id="o1168" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="compact-umbellate" value_original="compact-umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 0.2–1 cm, thinly tomentose to floccose, without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o1169" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="thinly tomentose" name="pubescence" src="d0_s5" to="floccose" />
      </biological_entity>
      <biological_entity id="o1170" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o1171" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o1169" id="r122" name="without" negation="false" src="d0_s5" to="o1170" />
      <relation from="o1170" id="r123" name="part_of" negation="false" src="d0_s5" to="o1171" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 1.5–2.5 mm, lobes 1–3 mm.</text>
      <biological_entity id="o1172" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1173" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 2–4 (–5) mm;</text>
      <biological_entity id="o1174" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth bright-yellow.</text>
      <biological_entity id="o1175" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly to rocky or talus slopes and ridges, high-elevation sagebrush communities, alpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly to rocky" />
        <character name="habitat" value="gravelly to talus slopes" />
        <character name="habitat" value="gravelly to ridges" />
        <character name="habitat" value="high-elevation sagebrush communities" />
        <character name="habitat" value="alpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3000-3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="3000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107p.</number>
  <other_name type="common_name">Coville’s sulphur flower</other_name>
  <discussion>Variety covillei is restricted to the backbone of the Sierra Nevada in Inyo and Tulare counties, and the White Mountains in Mono County; it is rare throughout its range. Clearly it is an alpine derivative of var. nevadense, and some specimens can be difficult to place.</discussion>
  
</bio:treatment>