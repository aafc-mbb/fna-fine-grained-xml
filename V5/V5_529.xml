<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">276</other_info_on_meta>
    <other_info_on_meta type="mention_page">225</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Reveal" date="1972" rank="species">prociduum</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>7: 417. 1972</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species prociduum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060468</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, matted, scapose, 0.3–1 × 1–3 dm, glabrous, greenish.</text>
      <biological_entity id="o30288" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="length" src="d0_s0" to="1" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, with persistent leaf-bases, up to 1/5 height of plant;</text>
      <biological_entity id="o30289" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o30290" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/5 height of plant" />
      </biological_entity>
      <relation from="o30289" id="r3415" name="with" negation="false" src="d0_s1" to="o30290" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems matted;</text>
      <biological_entity constraint="caudex" id="o30291" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems scapelike,weakly erect, slender, solid, not fistulose, 0.2–0.8 (–0.9) dm, glabrous.</text>
      <biological_entity id="o30292" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="scapelike" value_original="scapelike" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="0.9" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s3" to="0.8" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal, fasciculate in terminal tufts;</text>
      <biological_entity id="o30293" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="in terminal tufts" constraintid="o30294" is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o30294" name="tuft" name_original="tufts" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.2–1.5 (–2) cm, tomentose;</text>
      <biological_entity id="o30295" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s5" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblanceolate to spatulate or elliptic to ovate, 0.3–1.5 × 0.15–0.8 cm, densely white-tomentose on both surfaces or floccose and greenish adaxially, margins plane.</text>
      <biological_entity id="o30296" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="spatulate or elliptic" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s6" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.15" from_unit="cm" name="width" src="d0_s6" to="0.8" to_unit="cm" />
        <character constraint="on " constraintid="o30300" is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity id="o30297" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o30298" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o30299" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="floccose" value_original="floccose" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o30300" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o30301" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o30302" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
      <relation from="o30300" id="r3416" name="on" negation="false" src="d0_s6" to="o30301" />
      <relation from="o30300" id="r3417" modifier="on both surfaces or floccose and greenish , margins" name="on" negation="false" src="d0_s6" to="o30302" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences capitate, 0.8–1.2 cm wide;</text>
      <biological_entity id="o30303" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s7" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches absent;</text>
      <biological_entity id="o30304" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3–6, triangular, scalelike, 1–3 (–4) mm.</text>
      <biological_entity id="o30305" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="6" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o30306" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 4–7 per cluster, campanulate, 2–4 × (2.5–) 3–4 mm, weakly rigid, glabrous or sparsely floccose;</text>
      <biological_entity id="o30307" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per cluster" from="4" name="quantity" src="d0_s11" to="7" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_width" src="d0_s11" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="weakly" name="texture" src="d0_s11" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>teeth 5–6, erect, 0.8–1.4 mm.</text>
      <biological_entity id="o30308" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="6" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 2–4 mm;</text>
      <biological_entity id="o30309" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth bright-yellow, glabrous;</text>
      <biological_entity id="o30310" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4–1/3, monomorphic, oblong to oblong-obovate;</text>
      <biological_entity id="o30311" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s15" to="1/3" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="oblong-obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 2.5–3.5 mm;</text>
      <biological_entity id="o30312" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o30313" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 2–3.5 mm, glabrous.</text>
      <biological_entity id="o30314" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>47.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 0.3-1(-1.4) × 0.15-0.4(-0.6) cm, tomentose on both surfaces; petioles 0.2-0.5 cm; ne California, nw Nevada, and sc Oregon</description>
      <determination>47a Eriogonum prociduum var. prociduum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 0.7-1.5 × 0.3-0.8 cm, floccose and greenish adaxially; petioles 1-1.5(-2) cm; se Oregon, sw Idaho, and nc Nevada</description>
      <determination>47b Eriogonum prociduum var. mystrium</determination>
    </key_statement>
  </key>
</bio:treatment>