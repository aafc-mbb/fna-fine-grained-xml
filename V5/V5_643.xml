<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">319</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="1836" rank="species">niveum</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>17: 414. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species niveum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060411</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">niveum</taxon_name>
    <taxon_name authority="(Bentham) S. Stokes" date="unknown" rank="subspecies">decumbens</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species niveum;subspecies decumbens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">niveum</taxon_name>
    <taxon_name authority="(Douglas ex Bentham) S. Stokes" date="unknown" rank="subspecies">dichotomum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species niveum;subspecies dichotomum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect to decumbent or prostrate, loosely matted, not scapose, 2–6 × 1–4 (–6) dm, tomentose to floccose.</text>
      <biological_entity id="o9334" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect to decumbent" value_original="erect to decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="4" to_unit="dm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s0" to="floccose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, with or without persistent leaf-bases, up to 1/4 height of plant;</text>
      <biological_entity id="o9335" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o9336" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/4 height of plant" />
      </biological_entity>
      <relation from="o9335" id="r1033" name="with or without" negation="false" src="d0_s1" to="o9336" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems absent or spreading to matted;</text>
      <biological_entity constraint="caudex" id="o9337" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems erect to spreading or decument to prostrate, slender, solid, not fistulose, (0.5–) 1–3 dm, tomentose to floccose.</text>
      <biological_entity id="o9338" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="spreading" />
        <character name="orientation" src="d0_s3" value="decument" value_original="decument" />
        <character char_type="range_value" from="decument" name="growth_form_or_orientation" src="d0_s3" to="prostrate" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s3" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves 1 per node, sheathing up stems (1–) 3–10 (–12) cm;</text>
      <biological_entity id="o9339" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o9340" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="up stems" constraintid="o9341" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o9340" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity id="o9341" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole often twisted or curled, 1–10 cm, usually tomentose;</text>
      <biological_entity id="o9342" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="curled" value_original="curled" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade lanceolate or broadly lanceolate to elliptic or narrowly ovate, (1–) 1.5–6 (–8) × 0.6–1.5 cm, densely lanate on both surfaces or densely white-tomentose abaxially and grayish-tomentose to floccose adaxially, margins plane, rarely brownish.</text>
      <biological_entity id="o9343" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s6" to="elliptic or narrowly ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s6" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s6" to="1.5" to_unit="cm" />
        <character constraint="on " constraintid="o9347" is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="lanate" value_original="lanate" />
        <character is_modifier="false" modifier="rarely" name="coloration" notes="" src="d0_s6" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o9344" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o9345" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o9346" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="densely" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity id="o9347" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o9348" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o9349" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="on " constraintid="o9353" from="grayish-tomentose" name="pubescence" src="d0_s6" to="floccose" />
      </biological_entity>
      <biological_entity id="o9350" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o9351" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o9352" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="densely" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity id="o9353" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
      <relation from="o9347" id="r1034" name="on" negation="false" src="d0_s6" to="o9348" />
      <relation from="o9347" id="r1035" modifier="on both surfaces or densely white-tomentose and , margins" name="on" negation="false" src="d0_s6" to="o9349" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose, 10–30 × 50–250 cm;</text>
      <biological_entity id="o9354" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="width" src="d0_s7" to="250" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, tomentose to floccose;</text>
      <biological_entity id="o9355" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s8" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, semileaflike to leaflike, lanceolate to narrowly elliptic, and 5–30 × 3–20 mm proximally, scalelike, triangular, and 1–4 (–5) mm distally.</text>
      <biological_entity id="o9356" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="semileaflike to leaflike" value_original="semileaflike to leaflike" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="narrowly elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" modifier="proximally" name="width" src="d0_s9" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="distally" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o9357" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node, turbinate, 3–5 × 2–3 mm, tomentose to floccose;</text>
      <biological_entity id="o9358" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o9359" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s11" to="floccose" />
      </biological_entity>
      <biological_entity id="o9359" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.5–1.5 mm.</text>
      <biological_entity id="o9360" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 3–6 mm;</text>
      <biological_entity id="o9361" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth cream to reddish, glabrous;</text>
      <biological_entity id="o9362" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s14" to="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximally, dimorphic, those of outer whorl oblong to oval, 3–6 × 2–4 mm, those of inner whorl oblanceolate to oblong, 4–6 × 1–2 mm;</text>
      <biological_entity id="o9363" name="tepal" name_original="tepals" src="d0_s15" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="oval" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="4" to_unit="mm" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s15" to="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s15" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o9364" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <biological_entity constraint="inner" id="o9365" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <relation from="o9363" id="r1036" name="part_of" negation="false" src="d0_s15" to="o9364" />
      <relation from="o9363" id="r1037" name="part_of" negation="false" src="d0_s15" to="o9365" />
    </statement>
    <statement id="d0_s16">
      <text>stamens included to slightly exserted, 2–5 mm;</text>
      <biological_entity id="o9366" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="included" name="position" src="d0_s16" to="slightly exserted" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o9367" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown to brown, (3.5–) 4–4.5 (–5) mm, glabrous.</text>
      <biological_entity id="o9368" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s18" to="brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s18" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats, slopes, bluffs, and rocky, often volcanic outcrops, mixed grassland and sagebrush communities, montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="volcanic outcrops" modifier="often" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="conifer woodlands" modifier="montane" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30-1100(-1300) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="30" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1300" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>97.</number>
  <other_name type="common_name">Snow wild buckwheat</other_name>
  <discussion>Eriogonum niveum is a highly variable species with a multitude of minor expressions that do not appear to have any biogeographic or taxonomic significance. The species is found mainly on the grassy plains east of the Cascade Range in southern British Columbia, west-central Idaho, northeastern Oregon, and eastern Washington. Some populations closely approach E. strictum var. proliferum, but the densely lanate leaves and semileaflike to leaflike bracts nearly always distinguish E. niveum from that taxon where their ranges overlap. It may well prove that E. niveum would be better treated as a subspecies of E. strictum, but the nomenclatural combination is not available and it is not suggested here. The plants do well in cultivation.</discussion>
  <discussion>N. J. Turner et al. (1980) reported that the snow wild buckwheat was used by the Okanagan-Colville people for colds and as a wash for cuts.</discussion>
  
</bio:treatment>