<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">347</other_info_on_meta>
    <other_info_on_meta type="mention_page">337</other_info_on_meta>
    <other_info_on_meta type="mention_page">348</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="Reveal" date="1985" rank="variety">furcosum</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>45: 278. 1985</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety furcosum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060551</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, spreading to rounded, 3–6 × 3–8 dm.</text>
      <biological_entity id="o26517" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="3" from_unit="dm" name="length" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, 0.5–2 dm, sparsely floccose or glabrous, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o26518" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="2" to_unit="dm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26519" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o26518" id="r2942" name="without" negation="false" src="d0_s1" to="o26519" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in loose rosettes;</text>
      <biological_entity id="o26520" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26521" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o26520" id="r2943" name="in" negation="false" src="d0_s2" to="o26521" />
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic to oblong, (0.7–) 1–2.5 (–3) × 0.3–0.8 (–1.3) cm, white-tomentose abaxially, floccose to mostly glabrous and green adaxially, margins plane.</text>
      <biological_entity id="o26522" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="oblong" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_length" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1.3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="0.8" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="white-tomentose" value_original="white-tomentose" />
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s3" to="mostly glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o26523" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences compound-umbellate, branched 2–4 times;</text>
      <biological_entity id="o26524" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound-umbellate" value_original="compound-umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="" src="d0_s4" value="2-4 times" value_original="2-4 times " />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o26525" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <biological_entity id="o26526" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o26527" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o26525" id="r2944" name="without" negation="false" src="d0_s5" to="o26526" />
      <relation from="o26526" id="r2945" name="part_of" negation="false" src="d0_s5" to="o26527" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 2–3 (–4.5) mm, lobes 1.5–3 (–4) mm.</text>
      <biological_entity id="o26528" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26529" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers (5–) 6–8 mm;</text>
      <biological_entity id="o26530" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth bright-yellow.</text>
      <biological_entity id="o26531" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, sagebrush communities, oak and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107y.</number>
  <other_name type="common_name">Sierra Nevada sulphur flower</other_name>
  <discussion>Variety furcosum is the common, compound-umbellate form of the species encountered in the Sierra Nevada of California (Alpine, Amador, Calaveras, El Dorado, Fresno, Inyo, Kern, Madera, Mariposa, Mono, Nevada, Placer, Sierra, Tulare, and Tuolumne counties) and in the Mt. Rose/Slide Mountain area of southern Washoe County, Nevada.</discussion>
  
</bio:treatment>