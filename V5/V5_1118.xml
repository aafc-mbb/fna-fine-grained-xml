<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">548</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">polygonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Polygonum</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonum;section polygonum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">304213</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual (perennial and rhizomatous in P. striatulum), not compact, not cushionlike, homophyllous or heterophyllous, frequently heterocarpic, rarely subsucculent (in P. marinense and P. fowleri).</text>
      <biological_entity id="o31860" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s0" value="cushionlike" value_original="cushionlike" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="homophyllous" value_original="homophyllous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="heterophyllous" value_original="heterophyllous" />
        <character is_modifier="false" modifier="frequently" name="reproduction" src="d0_s0" value="heterocarpic" value_original="heterocarpic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, decumbent, or ascending to erect, straight (zigzag in P. fowleri and P. humifusum), distinctly and ± regularly 8–16-ribbed, smooth (papillose-scabridulous in P. plebeium).</text>
      <biological_entity id="o31861" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="distinctly; more or less regularly" name="architecture_or_shape" src="d0_s1" value="8-16-ribbed" value_original="8-16-ribbed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: ocrea 4–12-veined (1-veined in P. plebeium), proximal part not pruinose (pruinose in P. glaucum and P. oxyspermum);</text>
      <biological_entity id="o31862" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o31863" name="ocreum" name_original="ocrea" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="4-12-veined" value_original="4-12-veined" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31864" name="part" name_original="part" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s2" value="pruinose" value_original="pruinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole articulated to proximal part of ocrea, when absent, blade directly articulated to ocrea;</text>
      <biological_entity id="o31865" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o31866" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="articulated" value_original="articulated" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31867" name="part" name_original="part" src="d0_s3" type="structure" />
      <biological_entity id="o31868" name="ocreum" name_original="ocrea" src="d0_s3" type="structure" />
      <biological_entity id="o31869" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="to ocrea" constraintid="o31870" is_modifier="false" modifier="when absent; directly" name="architecture" src="d0_s3" value="articulated" value_original="articulated" />
      </biological_entity>
      <biological_entity id="o31870" name="ocreum" name_original="ocrea" src="d0_s3" type="structure" />
      <relation from="o31867" id="r3597" name="part_of" negation="false" src="d0_s3" to="o31868" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear to elliptic or obovate, rarely coriaceous (in P. glaucum), smooth (papillose in P. plebeium, rugulose and glaucous in P. glaucum);</text>
      <biological_entity id="o31871" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o31872" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="rarely" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>venation pinnate, secondary-veins conspicuous.</text>
      <biological_entity id="o31873" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o31874" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences usually axillary, less commonly axillary and terminal;</text>
      <biological_entity id="o31875" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="less commonly" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cymes 1–7 (–10) -flowered.</text>
      <biological_entity id="o31876" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-7(-10)-flowered" value_original="1-7(-10)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect to spreading, 0.5–7 mm.</text>
      <biological_entity id="o31877" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="spreading" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers usually closed, sometimes semi-open;</text>
      <biological_entity id="o31878" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="condition" src="d0_s9" value="closed" value_original="closed" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s9" value="semi-open" value_original="semi-open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals ± monomorphic, outer tepals equaling or somewhat larger than inner, apices of outer tepals rounded;</text>
      <biological_entity id="o31879" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s10" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity constraint="outer" id="o31880" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
        <character constraint="than inner tepals" constraintid="o31881" is_modifier="false" name="size" src="d0_s10" value="somewhat larger" value_original="somewhat larger" />
      </biological_entity>
      <biological_entity constraint="inner" id="o31881" name="tepal" name_original="tepals" src="d0_s10" type="structure" />
      <biological_entity id="o31882" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="outer" id="o31883" name="tepal" name_original="tepals" src="d0_s10" type="structure" />
      <relation from="o31882" id="r3598" name="part_of" negation="false" src="d0_s10" to="o31883" />
    </statement>
    <statement id="d0_s11">
      <text>anthers whitish yellow.</text>
      <biological_entity id="o31884" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="whitish yellow" value_original="whitish yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes ovate to ovatelanceolate, 2–3-gonous, shiny or dull, smooth to roughened or tubercled.</text>
      <biological_entity id="o31885" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="2-3-gonous" value_original="2-3-gonous" />
        <character is_modifier="false" name="reflectance" src="d0_s12" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="reflectance" src="d0_s12" value="dull" value_original="dull" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s12" to="roughened" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="tubercled" value_original="tubercled" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30a.</number>
  <discussion>Species ca. 45 (13 in the flora).</discussion>
  <discussion>Plants of sect. Polygonum produce two types of fruits (heterocarpy). Summer achenes are brown, ovate, and tubercled to smooth; late-season (autumn) achenes are hypertrophied, olivaceous, two to five times as long as the summer achenes, lanceolate, and smooth. The two types differ in their germination biology. Late-season achenes possess a low, innate dormancy and can germinate immediately after being produced, at 20–25°C (O. V. Yurtseva et al. 1999). Lower temperatures will delay germination until the following spring. Summer achenes are dormant when produced and undergo a cyclical dormant/nondormant pattern in the soil (A. D. Courtney 1968; J. M. Baskin and C. C. Baskin 1990).</discussion>
  <discussion>Mature, early-season plants bearing leaves, flowers, and achenes are necessary for accurate determinations. Late-autumn specimens with hypertrophied achenes often are difficult or impossible to identify. In the descriptions, measurements of leaves, ocreae, and petioles are from the middle of the main stem. Leaf length includes the petiole. Heterophyllous taxa have cauline leaves at least three times as long as the branch leaves; homophyllous plants have stem and branch leaves about equal in size. Heterophylly is easily detected in most cases. However, some taxa show considerable variability in leaf morphology; identification should not rely on heterophylly alone. The character states “flowers closed” and “flowers semi-open” should be observed on herbarium specimens. Perianth description refers to the fruiting perianth measured from the pedicel joint to the apex of the tepals. Tepal descriptions refer to the outer three tepals (except for P. heterosepalum). Trigonous achenes have one face broader than the other two. Descriptions refer only to the two narrower faces, which can be subequal or distinctly unequal. Surface of achenes is best observed at magnifications of (or higher than) 100×. The achene surface may be: smooth, when no ornamentation is visible; roughened, when the tubercles are not discernible but the surface appears roughened; striate-tubercled, when conspicuous tubercles are oriented in rows; uniformly tubercled, when the tubercles are dense and rows difficult to distinguish; and obscurely tubercled, when the tubercles are weakly marked, inconspicuous, or restricted to a few (three to 15) rows on the central parts of faces. Late-season achenes in all species are hypertrophied, olivaceous, lanceolate, exserted, and smooth. They have little taxonomic significance.</discussion>
  <references>
    <reference>Mertens, T. R. and P. H. Raven. 1965. Taxonomy of Polygonum, section Polygonum (Avicularia) in North America. Madroño 18: 85–92.</reference>
    <reference>Wolf, S. J. and J. McNeill. 1986. Synopsis and achene morphology of Polygonum section Polygonum (Polygonaceae) in Canada. Rhodora 88: 457–479.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants perennial, rhizomatous</description>
      <determination>1 Polygonum striatulum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants annual, not rhizomatous</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves in distal part of inflorescence reduced, not overtopping flowers (shorter than or equaling flowers); inflorescences axillary and terminal, spikelike</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves in distal part of inflorescence overtopping flowers; inflorescences entirely axillary</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Achenes striate-tubercled</description>
      <determination>12 Polygonum patulum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Achenes smooth or roughened</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Margins of tepals pink, rarely red or white; achenes 1.3-2.3 mm</description>
      <determination>13 Polygonum argyrocoleon</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Margins of tepals greenish yellow or yellow, rarely pink or white; achenes (2.3-)2.5-3.5 mm</description>
      <determination>4 Polygonum ramosissimum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Proximal parts of ocreae pruinose</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Proximal parts of ocreae not pruinose</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Distal parts of distal ocreae silvery, persistent; leaf blades glaucous, rugulose; perianth 2-3(-4) mm</description>
      <determination>6 Polygonum glaucum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Distal parts of ocreae hyaline, soon disintegrating into fibers or deciduous;leaf blades not glaucous, not rugulose; perianth 3.5-5.5 mm</description>
      <determination>7 Polygonum oxyspermum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Achenes striate-tubercled, uniformly tubercled, or obscurely tubercled</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Achenes smooth to roughened</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Plants green to bluish green; margins of tepals white, pink, or red</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Plants light green or yellowish; margins of tepals green to yellow</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Achenes coarsely striate-tubercled</description>
      <determination>11 Polygonum aviculare</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Achenes obscurely tubercled</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants dark brown to black after drying; distal part of ocreae dis-integrating into persistent fibers, brown</description>
      <determination>4 Polygonum ramosissimum</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants green after drying (sometimes whitish from powdery mildew); distal part of ocreae persistent, silvery</description>
      <determination>11 Polygonum aviculare</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Perianth tube 40-55% of perianth length; tepals ± keeled; pedicels 1.3-1.8(-2) mm, enclosed in ocreae</description>
      <determination>3 Polygonum achoreum</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Perianth tube 20-38% of perianth length; tepals not keeled; pedicels 2-7 mm, exserted from ocreae</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades elliptic to obovate; distal parts of ocreae ± persistent, silvery; achenes striate-tubercled</description>
      <determination>2 Polygonum erectum</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades narrowly elliptic to lanceolate, rarely ovate; distal parts of ocreae soon disintegrating into persistent brown fibers; achenes uniformly tubercled</description>
      <determination>4 Polygonum ramosissimum</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Stems and leaf blades papillose-scabridulous; ocreae 1-veined</description>
      <determination>10 Polygonum plebeium</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Stems and leaf blades glabrous; ocreae 4-12-veined. [14. Shifted to left margin.—Ed.]</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaves often opposite at proximal nodes; achenes 1.4-1.6(-2.2) mm; Yukon, N.W.T., Nunavut, Alaska 9 Polygonum humifusum</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaves all alternate; achenes 1.6-4.5 mm; broad distribution (including Yukon, N.W.T., Nunavut, Alaska)</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Perianth tube 40-57% of perianth length</description>
      <determination>11 Polygonum aviculare</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Perianth tube 18-38% of perianth length</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Achenes beaked at apex, edges strongly concave; stems sometimes zigzagged</description>
      <determination>8 Polygonum fowleri</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Achenes not beaked at apex, edges straight; stems not zigzagged</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Plants homophyllous; pedicels 1-2 mm, enclosed in ocreae</description>
      <determination>4 Polygonum ramosissimum</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Plants heterophyllous; pedicels 2-6 mm, mostly exserted from ocreae</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Plants subsucculent; leaf blade apices rounded; proximal parts of ocreae funnelform; flowers semi-open; cymes in axils of most leaves; California</description>
      <determination>5 Polygonum marinense</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Plants not subsucculent; leaf blade apices acute to acuminate; proximal parts of ocreae cylindric; flowers closed; cymes crowded toward tips of branches; broad distribution in United States and Canada (including California)</description>
      <determination>4 Polygonum ramosissimum</determination>
    </key_statement>
  </key>
</bio:treatment>