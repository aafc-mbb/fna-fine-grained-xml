<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="mention_page">422</other_info_on_meta>
    <other_info_on_meta type="illustration_page">417</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Oregonium</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="species">luteolum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">luteolum</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oregonium;species luteolum;variety luteolum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060377</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, (2–) 3–5 (–6) dm, glabrous or occasionally tomentose.</text>
      <biological_entity id="o10945" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, 0.5–1 dm.</text>
      <biological_entity id="o10946" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="1" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o10947" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblong-ovate or rounded to cordate.</text>
      <biological_entity id="o10948" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 10–45 cm.</text>
      <biological_entity id="o10949" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s4" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres sessile and appressed to branches, cylindric, 3–3.5 mm.</text>
      <biological_entity id="o10950" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character constraint="to branches" constraintid="o10951" is_modifier="false" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10951" name="branch" name_original="branches" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1.8–2 mm;</text>
      <biological_entity id="o10952" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth white to rose or yellow.</text>
      <biological_entity id="o10953" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="rose or yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes 1.8–2 mm. 2n = 24.</text>
      <biological_entity id="o10954" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10955" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly serpentine flats and slopes, mixed grassland and chaparral communities, oak and pine woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly serpentine flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-1600(-2100) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="50" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2100" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>208a.</number>
  <other_name type="common_name">Golden carpet wild buckwheat</other_name>
  <discussion>Variety luteolum is rather common in northern California, where it is seen primarily in the North Coast Ranges and across the northern tier of counties to the northern tip of the Sierra Nevada. It continues northward into southern Oregon. In some places, it can even be abundant or infrequently rather weedy. It is difficult to distinguish from E. gracile in the southern part of its range and from E. vimineum in the northeastern portion. A collection from Crook County, Oregon, (Steward &amp; Steward 6199, KANU, IA, OKLA, PENN, UTC) seems to be this species rather than E. vimineum.</discussion>
  
</bio:treatment>