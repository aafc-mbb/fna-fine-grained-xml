<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">441</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="genus">NEMACAULIS</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 18. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus nemacaulis;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek nema, thread, and Greek kaulos, stem</other_info_on_name>
    <other_info_on_name type="fna_id">121801</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Michaux" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Roberty &amp; Vautier" date="unknown" rank="section">Nemacaulis</taxon_name>
    <taxon_hierarchy>genus Eriogonum;section Nemacaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual;</text>
      <biological_entity id="o3995" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot thin.</text>
      <biological_entity id="o3996" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems arising from the root, prostrate to spreading or erect, glabrous to glandular or lanate.</text>
      <biological_entity id="o3997" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from root" constraintid="o3998" is_modifier="false" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character char_type="range_value" from="prostrate" name="orientation" notes="" src="d0_s2" to="spreading or erect" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s2" to="glandular or lanate" />
      </biological_entity>
      <biological_entity id="o3998" name="root" name_original="root" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent through anthesis, essentially basal, rosulate;</text>
      <biological_entity id="o3999" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="through anthesis leaves" constraintid="o4000" is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s3" value="rosulate" value_original="rosulate" />
      </biological_entity>
      <biological_entity id="o4000" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="through" name="life_cycle" src="d0_s3" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" modifier="essentially" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole indistinct;</text>
      <biological_entity id="o4001" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear to spatulate, margins entire.</text>
      <biological_entity id="o4002" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="spatulate" />
      </biological_entity>
      <biological_entity id="o4003" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, cymose;</text>
      <biological_entity id="o4004" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches mostly dichotomous, not brittle or disarticulating into segments, round, glabrous or sparsely lanate, sometimes glandular, secondary branches sometimes developing tardily;</text>
      <biological_entity id="o4005" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s7" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s7" value="brittle" value_original="brittle" />
        <character constraint="into segments" constraintid="o4006" is_modifier="false" name="architecture" src="d0_s7" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="round" value_original="round" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="lanate" value_original="lanate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o4006" name="segment" name_original="segments" src="d0_s7" type="structure" />
      <biological_entity constraint="secondary" id="o4007" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes; tardily" name="development" src="d0_s7" value="developing" value_original="developing" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, connate basally, triangular to obovate, leaflike or scalelike, not awned, glabrous or glandular.</text>
      <biological_entity id="o4008" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s8" to="obovate leaflike or scalelike" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s8" to="obovate leaflike or scalelike" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s8" value="awned" value_original="awned" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles erect, slender, or absent.</text>
      <biological_entity id="o4009" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucral-bracts obscure to obvious, in tight spiral, linear to lanceolate or narrowly ovate, not awn-tipped.</text>
      <biological_entity id="o4010" name="involucral-bract" name_original="involucral-bracts" src="d0_s10" type="structure">
        <character char_type="range_value" from="obscure" name="prominence" src="d0_s10" to="obvious" />
        <character char_type="range_value" from="linear" modifier="in tight spiral" name="shape" src="d0_s10" to="lanceolate or narrowly ovate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5–30 per involucre, sessile or pedicellate;</text>
      <biological_entity id="o4011" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per involucre" constraintid="o4012" from="5" name="quantity" src="d0_s11" to="30" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o4012" name="involucre" name_original="involucre" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>perianth white to rose, campanulate when open, narrowly urceolate when closed, glabrous or with gland-tipped hairs;</text>
      <biological_entity id="o4013" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="rose" />
        <character is_modifier="false" modifier="when open" name="shape" src="d0_s12" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="when closed" name="shape" src="d0_s12" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="with gland-tipped hairs" value_original="with gland-tipped hairs" />
      </biological_entity>
      <biological_entity id="o4014" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o4013" id="r450" name="with" negation="false" src="d0_s12" to="o4014" />
    </statement>
    <statement id="d0_s13">
      <text>tepals 6, connate proximally, slightly dimorphic, entire;</text>
      <biological_entity id="o4015" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 3;</text>
      <biological_entity id="o4016" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments basally adnate, glabrous;</text>
      <biological_entity id="o4017" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s15" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers pink to red, oval.</text>
      <biological_entity id="o4018" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s16" to="red" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oval" value_original="oval" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes mostly included, brown to deep maroon or black, not winged, 3-gonous, glabrous.</text>
      <biological_entity id="o4019" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s17" value="included" value_original="included" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s17" to="deep maroon or black" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds: embryo curved.</text>
      <biological_entity id="o4020" name="seed" name_original="seeds" src="d0_s18" type="structure" />
      <biological_entity id="o4021" name="embryo" name_original="embryo" src="d0_s18" type="structure">
        <character is_modifier="false" name="course" src="d0_s18" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Cottonheads</other_name>
  <discussion>Species 1.</discussion>
  <discussion>Nemacaulis is allied to Eriogonum subg. Ganysma and especially E. gossypinum. Like that species, N. denudata has flowers embedded in a dense mass of hairs, which is the only significant feature that the two have in common aside from being annuals, generally spreading in habit, and having rather densely pubescent stems and leaves. The flowers in Nemacaulis are arranged in glomerules, a feature unique in Eriogonoideae. The involucre is markedly different, being composed of several whorls of bracts, each of which subtends a flower. In this and all the other members of Eriogoneae, modification of the involucre appears to be a significant factor in the evolution of distinctive genera. In Nemacaulis, the entire glomerule is dispersed by on-shore breezes along the coast, an effective transport mechanism for the tiny seeds. Entire glomerules may be trapped in moist pockets of sand, thereby positioning several seeds in a favorable location. In the rapidly evolving derived annuals of Eriogonineae, any selective advantage is quickly adopted and survival enhanced in the rather harsh environment, where members of the subtribe abound.</discussion>
  <references>
    <reference>Reveal, J. L. and B. Ertter. 1980. The genus Nemacaulis Nutt. (Polygonaceae). Madroño 27: 101–109.</reference>
  </references>
  
</bio:treatment>