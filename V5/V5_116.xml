<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">59</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(S. Watson) Ikonnikov" date="1973" rank="species">aculeata</taxon_name>
    <place_of_publication>
      <publication_title>Novosti Sist. Vyssh. Rast.</publication_title>
      <place_in_publication>10: 139. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species aculeata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060126</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">aculeata</taxon_name>
    <place_of_publication>
      <publication_title>Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>40. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species aculeata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">fendleri</taxon_name>
    <taxon_name authority="(S. Watson) S. L. Welsh" date="unknown" rank="variety">aculeata</taxon_name>
    <taxon_hierarchy>genus Arenaria;species fendleri;variety aculeata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Coville &amp; Leiberg" date="unknown" rank="species">pumicola</taxon_name>
    <taxon_name authority="Maguire" date="unknown" rank="variety">californica</taxon_name>
    <taxon_hierarchy>genus Arenaria;species pumicola;variety californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants strongly mat-forming, glaucous, often with woody base.</text>
      <biological_entity id="o16218" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="strongly" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16219" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o16218" id="r1801" modifier="often" name="with" negation="false" src="d0_s0" to="o16219" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 7–25 (–30) cm, densely stipitate-glandular distally.</text>
      <biological_entity id="o16220" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves persistent;</text>
      <biological_entity id="o16221" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o16222" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves in 1–3 pairs, abruptly reduced distal to lowest pair;</text>
      <biological_entity id="o16223" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o16224" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abruptly" name="position" notes="" src="d0_s3" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="distal" name="position" src="d0_s3" to="lowest" />
      </biological_entity>
      <biological_entity id="o16225" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <relation from="o16224" id="r1802" name="in" negation="false" src="d0_s3" to="o16225" />
    </statement>
    <statement id="d0_s4">
      <text>basal blades ascending or often arcuate-spreading, needlelike, (0.5–) 1–2.5 (–3.5) cm × 0.5–1.5 mm, rigid, herbaceous, apex spinose, glabrous to puberulent, glaucous.</text>
      <biological_entity id="o16226" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o16227" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s4" value="arcuate-spreading" value_original="arcuate-spreading" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="needlelike" value_original="needlelike" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o16228" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinose" value_original="spinose" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 5–25+-flowered, open cymes.</text>
      <biological_entity id="o16229" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-25+-flowered" value_original="5-25+-flowered" />
      </biological_entity>
      <biological_entity id="o16230" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 3–25 mm, stipitate-glandular.</text>
      <biological_entity id="o16231" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 1–3-veined, lateral-veins less developed or all obscure, ovate, 3–4.5 mm, to 6 mm in fruit, margins broad, apex usually obtuse to rounded, abruptly acute, sparsely to densely stipitate-glandular;</text>
      <biological_entity id="o16232" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o16233" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o16234" name="lateral-vein" name_original="lateral-veins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="less" name="development" src="d0_s7" value="developed" value_original="developed" />
        <character name="development" src="d0_s7" value="all" value_original="all" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o16235" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16235" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o16236" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o16237" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="usually obtuse" name="shape" src="d0_s7" to="rounded" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, obovate to oblanceolate, 4.5–10 mm, 1.5–3 times as long as sepals, apex rounded;</text>
      <biological_entity id="o16238" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16239" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="oblanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character constraint="sepal" constraintid="o16240" is_modifier="false" name="length" src="d0_s8" value="1.5-3 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o16240" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity id="o16241" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectaries as lateral and abaxial rounding, with slight lateral expansion, at base of filaments opposite sepals, 0.3 mm.</text>
      <biological_entity id="o16242" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16243" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character name="some_measurement" notes="" src="d0_s9" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity constraint="lateral and abaxial" id="o16244" name="rounding" name_original="rounding" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o16245" name="expansion" name_original="expansion" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="slight" value_original="slight" />
      </biological_entity>
      <biological_entity id="o16246" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o16247" name="filament" name_original="filaments" src="d0_s9" type="structure" />
      <biological_entity id="o16248" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o16243" id="r1803" name="as" negation="false" src="d0_s9" to="o16244" />
      <relation from="o16243" id="r1804" name="with" negation="false" src="d0_s9" to="o16245" />
      <relation from="o16243" id="r1805" name="at" negation="false" src="d0_s9" to="o16246" />
      <relation from="o16246" id="r1806" name="part_of" negation="false" src="d0_s9" to="o16247" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules 5–9 mm, glabrous.</text>
      <biological_entity id="o16249" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds yellowish tan to gray, ellipsoid-oblong, 1.8–2.5 (–3.2) mm, tuberculate;</text>
      <biological_entity id="o16250" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellowish tan" name="coloration" src="d0_s11" to="gray" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid-oblong" value_original="ellipsoid-oblong" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>tubercles rounded, elongate.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 22.</text>
      <biological_entity id="o16251" name="tubercle" name_original="tubercles" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16252" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, alluvium, volcanic areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="alluvium" />
        <character name="habitat" value="volcanic areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Nev., Oreg., Utah, Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Prickly sandwort</other_name>
  <discussion>Some specimens from north-central California and southwestern Oregon have been named Arenaria pumicola var. californica. R. L. Hartman (1993) considered those plants to be robust forms of Eremogone aculeata, not deserving formal recognition. Based on work by M. F. Baad (1969), they warrant further study.</discussion>
  <discussion>Reports of Eremogone aculeata from Arizona, New Mexico, and Wyoming are erroneous.</discussion>
  
</bio:treatment>