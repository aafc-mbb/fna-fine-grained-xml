<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">162</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">dianthus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">plumarius</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">plumarius</taxon_name>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus dianthus;species plumarius;subspecies plumarius;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060114</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not matted.</text>
      <biological_entity id="o18791" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple, 13–40 cm, glabrous, often glaucous.</text>
      <biological_entity id="o18792" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: sheath (1–) 2–4 mm, 1–2 times as long as stem diam.;</text>
      <biological_entity id="o18793" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o18794" name="sheath" name_original="sheath" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
        <character constraint="stem" constraintid="o18795" is_modifier="false" name="length_or_diameter" src="d0_s2" value="1-2 times as long as stem diam" />
      </biological_entity>
      <biological_entity id="o18795" name="stem" name_original="stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade linear, 2–7.5 cm, margins glabrous, often glaucous.</text>
      <biological_entity id="o18796" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18797" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="7.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18798" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences open, 2–4-flowered cymes, or sometimes flowers solitary;</text>
      <biological_entity id="o18799" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o18800" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="2-4-flowered" value_original="2-4-flowered" />
      </biological_entity>
      <biological_entity id="o18801" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts absent;</text>
      <biological_entity id="o18802" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracteoles 4, green, obovate, 1/4–1/3 times as long as calyx, herbaceous, apex abruptly acuminate or truncate.</text>
      <biological_entity id="o18803" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character constraint="calyx" constraintid="o18804" is_modifier="false" name="length" src="d0_s6" value="1/4-1/3 times as long as calyx" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o18804" name="calyx" name_original="calyx" src="d0_s6" type="structure" />
      <biological_entity id="o18805" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 8–25 mm.</text>
      <biological_entity id="o18806" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 40–45-veined, 14–22 mm, glabrous, lobes ovate, 3–6 mm;</text>
      <biological_entity id="o18807" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18808" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="40-45-veined" value_original="40-45-veined" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s8" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18809" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white or pale-pink, often with darker center, bearded, 8–15 mm, apex divided into narrow segments to 1/2 as long as blade.</text>
      <biological_entity id="o18810" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18811" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s9" value="bearded" value_original="bearded" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18812" name="center" name_original="center" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o18813" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character constraint="into segments" constraintid="o18814" is_modifier="false" name="shape" src="d0_s9" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o18814" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="1/2" />
      </biological_entity>
      <biological_entity id="o18815" name="blade" name_original="blade" src="d0_s9" type="structure" />
      <relation from="o18811" id="r2109" modifier="often" name="with" negation="false" src="d0_s9" to="o18812" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules 23–27 mm, slightly exceeding calyx.</text>
      <biological_entity id="o18816" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s10" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18817" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <relation from="o18816" id="r2110" modifier="slightly" name="exceeding" negation="false" src="d0_s10" to="o18817" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds 2.4–3 mm. 2n = 30, 60, 90 (all Europe).</text>
      <biological_entity id="o18818" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18819" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="30" value_original="30" />
        <character name="quantity" src="d0_s11" value="60" value_original="60" />
        <character name="quantity" src="d0_s11" value="90" value_original="90" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring and summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, sandy fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="sandy fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.S., Ont., Que., Yukon; Ala., Calif., Conn., Maine, Mass., Mich., Mo., N.H., N.Y., N.C., Pa., S.C., Vt., Va., Wis.; e Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="e Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5a.</number>
  <discussion>Subspecies plumarius is often cultivated and occasionally escapes, perhaps not persisting in the northern part of its range.</discussion>
  
</bio:treatment>