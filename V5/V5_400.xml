<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="mention_page">197</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">ovata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 316. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species ovata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060874</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, rhizomatous;</text>
      <biological_entity id="o29567" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizome creeping.</text>
      <biological_entity id="o29568" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, usually simple, 30–150 cm, with short, dense, eglandular pubescence, sparsely so toward base.</text>
      <biological_entity id="o29569" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29570" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o29569" id="r3327" modifier="sparsely" name="toward" negation="false" src="d0_s2" to="o29570" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile, 2 per node;</text>
      <biological_entity id="o29571" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character constraint="per node" constraintid="o29572" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o29572" name="node" name_original="node" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade prominently 3–5-veined, ovate-acuminate, round at base, (4–) 6–10 (–13) cm × (20–) 30–50 (–90) mm, appressed-pubescent on both surfaces.</text>
      <biological_entity id="o29573" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s4" value="3-5-veined" value_original="3-5-veined" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate-acuminate" value_original="ovate-acuminate" />
        <character constraint="at base" constraintid="o29574" is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
        <character constraint="on surfaces" constraintid="o29575" is_modifier="false" name="pubescence" src="d0_s4" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o29574" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_length" notes="alterIDs:o29574" src="d0_s4" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" notes="alterIDs:o29574" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" notes="alterIDs:o29574" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_width" notes="alterIDs:o29574" src="d0_s4" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_width" notes="alterIDs:o29574" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" notes="alterIDs:o29574" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29575" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences paniculate, narrow, many-flowered, open, bracteate, pedunculate, 10–50 × 3–5 cm, densely puberulent;</text>
      <biological_entity id="o29576" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="many-flowered" value_original="many-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="50" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts narrowly lanceolate, 3–15 mm, apex acuminate;</text>
      <biological_entity id="o29577" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29578" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle ascending.</text>
      <biological_entity id="o29579" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels ascending, recurved near apex, ca. equaling calyx.</text>
      <biological_entity id="o29580" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character constraint="near apex" constraintid="o29581" is_modifier="false" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o29581" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o29582" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="true" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers nocturnal;</text>
      <biological_entity id="o29583" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="nocturnal" value_original="nocturnal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calyx prominently 10-veined, tubular to narrowly campanulate and 6–9 × 3–4 mm in flower, turbinate and 10–12 × 4–5 mm in fruit, narrowed proximally around carpophore, veins parallel, green, broad, with pale commissures, puberulent, sometimes with few glands, lobes triangular-acute, 2–3 mm;</text>
      <biological_entity id="o29584" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s10" value="10-veined" value_original="10-veined" />
        <character char_type="range_value" from="tubular" name="shape" src="d0_s10" to="narrowly campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" constraint="in flower" constraintid="o29585" from="3" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o29586" from="4" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
        <character constraint="proximally around carpophore" constraintid="o29587" is_modifier="false" name="shape" notes="" src="d0_s10" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o29585" name="flower" name_original="flower" src="d0_s10" type="structure" />
      <biological_entity id="o29586" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity id="o29587" name="carpophore" name_original="carpophore" src="d0_s10" type="structure" />
      <biological_entity id="o29588" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o29589" name="commissure" name_original="commissures" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o29590" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o29591" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="triangular-acute" value_original="triangular-acute" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o29588" id="r3328" name="with" negation="false" src="d0_s10" to="o29589" />
      <relation from="o29588" id="r3329" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o29590" />
    </statement>
    <statement id="d0_s11">
      <text>corolla white, clawed, claw equaling calyx, broadened into limb, limb obtriangular, deeply lobed, 7–9 mm, lobes ca. 8, linear, appendages minute;</text>
      <biological_entity id="o29592" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o29593" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character constraint="into limb" constraintid="o29595" is_modifier="false" name="width" notes="" src="d0_s11" value="broadened" value_original="broadened" />
      </biological_entity>
      <biological_entity id="o29594" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o29595" name="limb" name_original="limb" src="d0_s11" type="structure" />
      <biological_entity id="o29596" name="limb" name_original="limb" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtriangular" value_original="obtriangular" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s11" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29597" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="8" value_original="8" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o29598" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens slightly longer than corolla;</text>
      <biological_entity id="o29599" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character constraint="than corolla" constraintid="o29600" is_modifier="false" name="length_or_size" src="d0_s12" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o29600" name="corolla" name_original="corolla" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>styles 3, ca. 2 times as long as corolla.</text>
      <biological_entity id="o29601" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character constraint="corolla" constraintid="o29602" is_modifier="false" name="length" src="d0_s13" value="2 times as long as corolla" />
      </biological_entity>
      <biological_entity id="o29602" name="corolla" name_original="corolla" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules narrowly ovoid, slightly longer than calyx, opening by 3 (splitting into 6) ascending teeth;</text>
      <biological_entity id="o29603" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character constraint="than calyx" constraintid="o29604" is_modifier="false" name="length_or_size" src="d0_s14" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o29604" name="calyx" name_original="calyx" src="d0_s14" type="structure" />
      <biological_entity id="o29605" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="true" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o29603" id="r3330" name="opening by" negation="false" src="d0_s14" to="o29605" />
    </statement>
    <statement id="d0_s15">
      <text>carpophore 2–2.5 mm.</text>
      <biological_entity id="o29606" name="carpophore" name_original="carpophore" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark-brown, reniform, 0.8–1.5 mm, shallowly tuberculate.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 48.</text>
      <biological_entity id="o29607" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="shallowly" name="relief" src="d0_s16" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29608" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., Ky., Miss., N.C., S.C., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39.</number>
  <other_name type="common_name">Ovate-leaved campion or catchfly</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Silene ovata is a very distinctive species with large, ovate, acuminate, sessile, paired leaves, and very narrowly lobed white petals. The flowers open at night and are moth-pollinated.</discussion>
  
</bio:treatment>