<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">499</other_info_on_meta>
    <other_info_on_meta type="mention_page">490</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="(Meisner) Rechinger f." date="1937" rank="subgenus">Acetosella</taxon_name>
    <taxon_name authority="Rudolph ex Lambert" date="1811" rank="species">graminifolius</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>10: 264, plate 10. 1811</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus acetosella;species graminifolius;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250060781</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acetosella</taxon_name>
    <taxon_name authority="(Rudolph ex Lambert) Á. Löve" date="unknown" rank="species">graminifolia</taxon_name>
    <taxon_hierarchy>genus Acetosella;species graminifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">acetosella</taxon_name>
    <taxon_name authority="(Rudolph ex Lambert) Schrenk" date="unknown" rank="variety">graminifolius</taxon_name>
    <taxon_hierarchy>genus Rumex;species acetosella;variety graminifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Ledebour" date="unknown" rank="species">angustissimus</taxon_name>
    <taxon_hierarchy>genus Rumex;species angustissimus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous, with creeping rhizomes and elongated underground stolons.</text>
      <biological_entity id="o13536" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13537" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
      <biological_entity id="o13538" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="true" name="length" src="d0_s0" value="elongated" value_original="elongated" />
        <character is_modifier="true" name="location" src="d0_s0" value="underground" value_original="underground" />
      </biological_entity>
      <relation from="o13536" id="r1478" name="with" negation="false" src="d0_s0" to="o13537" />
      <relation from="o13536" id="r1479" name="with" negation="false" src="d0_s0" to="o13538" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, rarely almost prostrate, branched at base and in distal 1/2 (in inflorescence), 7–30 (–40) cm;</text>
      <biological_entity id="o13539" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely almost" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character constraint="at base and in distal 1/2" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>shoots not crowded, ± elongated.</text>
      <biological_entity id="o13540" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="more or less" name="length" src="d0_s2" value="elongated" value_original="elongated" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: ocrea whitish or silvery, membranous;</text>
      <biological_entity id="o13541" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13542" name="ocreum" name_original="ocrea" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="silvery" value_original="silvery" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade normally narrowly linear, or occasionally linear-lanceolate, usually not hastate, rarely some with indistinct basal lobes, 3–10 × 0.1–0.2 (–0.4) cm, base narrowly cuneate, margins entire, flat or occasionally slightly revolute, apex acute or obtuse.</text>
      <biological_entity id="o13543" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o13544" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="normally narrowly" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="normally narrowly" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s4" value="hastate" value_original="hastate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" notes="" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="cm" name="atypical_width" notes="" src="d0_s4" to="0.4" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" notes="" src="d0_s4" to="0.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o13545" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o13546" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13547" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="occasionally slightly" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o13548" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o13544" id="r1480" modifier="rarely" name="with" negation="false" src="d0_s4" to="o13545" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, occupying distal 2/3 of stem, usually lax and interrupted to top, paniculate, with branches often reflexed.</text>
      <biological_entity id="o13549" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="position" src="d0_s5" value="top" value_original="top" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o13550" name="stem" name_original="stem" src="d0_s5" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o13551" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <relation from="o13549" id="r1481" name="occupying" negation="false" src="d0_s5" to="o13550" />
      <relation from="o13549" id="r1482" name="with" negation="false" src="d0_s5" to="o13551" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–4 mm.</text>
      <biological_entity id="o13552" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers (3–) 4–6 (–8) in whorls;</text>
      <biological_entity id="o13553" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s7" to="4" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="8" />
        <character char_type="range_value" constraint="in whorls" constraintid="o13554" from="4" name="quantity" src="d0_s7" to="6" />
      </biological_entity>
      <biological_entity id="o13554" name="whorl" name_original="whorls" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>inner tepals distinctly enlarged, normally 2–2.6 × 1.5–2 (–2.2) mm (free wing 0.3–0.5 mm wide), base cuneate, apex obtuse or subacute.</text>
      <biological_entity constraint="inner" id="o13555" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distinctly" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13556" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="normally" name="shape" src="d0_s8" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13557" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes brown or yellowish-brown, 1.5–2 × 1–1.5 mm. 2n = 56.</text>
      <biological_entity id="o13558" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish-brown" value_original="yellowish-brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13559" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy and gravelly shores and slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly shores" />
        <character name="habitat" value="slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alaska; n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Grass-leaved or grassleaf sorrel</other_name>
  <discussion>Records of Rumex graminifolius from Alaska in most cases refer to R. beringensis and R. krausei. The occurrence of typical R. graminifolius in northwestern North America remains uncertain. Some literature records of R. acetosella from northeastern North America (Greenland, Labrador, Newfoundland) may refer to R. graminifolius or R. acetosella subsp. arenicola. Rumex graminifolius was reported from Newfoundland also by M. L. Fernald (1950), but that record requires confirmation.</discussion>
  <discussion>Some plants from northeastern Eurasia (northeastern Russian Far East and northern Siberia) are known in Russian literature as Rumex aureostigmaticus Komarov [Acetosella aureostigmatica (Komarov) Tzvelev], R. acetosella var. subspathulatus Trautvetter, or R. graminifolius var. subspathulatus (Trautvetter) Tolmatchew (see A. I. Tolmachew 1966; N. N. Tzvelev 1989b). They differ from R. graminifolius in having narrower inner tepals and wider spatulate leaves, usually without basal lobes. I have seen only one North American collection approaching this entity. Some specimens (mostly immature or staminate plants) from western Alaska differ from both R. graminifolius and R. beringensis in their habit; they need additional study. Some chromosome counts different from the most typical number (2n = 56) that have been reported for R. graminifolius in the broad sense from northeastern Russian Far East by several Russian authors (see references in Tzvelev) most probably also refer to R. aureostigmaticus. It is also possible that arctic and subarctic plants identified by various authors as R. aureostigmaticus, R. acetosella var. subspathulatus, R. graminifolius var. subspathulatus, and R. acetosella subsp. arenicola belong to one polymorphic complex of plants intermediate between R. acetosella and R. graminifolius.</discussion>
  
</bio:treatment>