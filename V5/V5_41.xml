<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John W. Thieret,Richard K. Rabeler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">25</other_info_on_meta>
    <other_info_on_meta type="mention_page">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Tanfani in F. Parlatore" date="unknown" rank="subfamily">Polycarpoideae</taxon_name>
    <taxon_name authority="Loefling in C. Linnaeus" date="unknown" rank="genus">POLYCARPON</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 859 (as Polycarpa), 881, 1360. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily polycarpoideae;genus polycarpon;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek polys, many, and karpos, fruit, alluding to numerous capsules</other_info_on_name>
    <other_info_on_name type="fna_id">126356</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o3617" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots slender.</text>
      <biological_entity id="o3618" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate to erect, branched, terete to finely ridged.</text>
      <biological_entity id="o3619" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s2" to="finely ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite or in whorls of 4, not connate, petiolate;</text>
      <biological_entity id="o3620" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3621" name="whorl" name_original="whorls" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="4" value_original="4" />
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <relation from="o3620" id="r394" name="opposite or in" negation="false" src="d0_s3" to="o3621" />
    </statement>
    <statement id="d0_s4">
      <text>stipules 2 per node, silvery, lanceolate to triangular-ovate, margins entire or irregularly cut, apex acuminate to aristate;</text>
      <biological_entity id="o3622" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o3623" name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="silvery" value_original="silvery" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="triangular-ovate" />
      </biological_entity>
      <biological_entity id="o3623" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity id="o3624" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="cut" value_original="cut" />
      </biological_entity>
      <biological_entity id="o3625" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 1-veined, spatulate or oblanceolate to ovate or elliptic, not succulent, apex obtuse, sometimes mucronate.</text>
      <biological_entity id="o3626" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="ovate or elliptic" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o3627" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary, dense or lax cymes;</text>
      <biological_entity id="o3628" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o3629" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts paired or absent.</text>
      <biological_entity id="o3630" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect.</text>
      <biological_entity id="o3631" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth and androecium perigynous;</text>
      <biological_entity id="o3632" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3633" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o3634" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium minute, cupshaped, not abruptly expanded distally;</text>
      <biological_entity id="o3635" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3636" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="minute" value_original="minute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cup-shaped" value_original="cup-shaped" />
        <character is_modifier="false" modifier="not abruptly; distally" name="size" src="d0_s10" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals distinct, green, lanceolate to elliptic or ovate, often keeled, 1–2.5 mm, herbaceous, margins white, scarious, apex acute, ± hooded, ± awned;</text>
      <biological_entity id="o3637" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3638" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="elliptic or ovate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s11" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o3639" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o3640" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s11" value="hooded" value_original="hooded" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals often fugacious, 5, white, blade apex emarginate;</text>
      <biological_entity id="o3641" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3642" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="often" name="duration" src="d0_s12" value="fugacious" value_original="fugacious" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="blade" id="o3643" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectaries between filament bases;</text>
      <biological_entity id="o3644" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="between bases" constraintid="o3646" id="o3645" name="nectary" name_original="nectaries" src="d0_s13" type="structure" constraint_original="between  bases, " />
      <biological_entity constraint="filament" id="o3646" name="base" name_original="bases" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 3–5;</text>
      <biological_entity id="o3647" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o3648" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments shortly connate distally around ovary;</text>
      <biological_entity id="o3649" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o3650" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character constraint="distally around ovary" constraintid="o3651" is_modifier="false" modifier="shortly" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o3651" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style 1, obscurely 3-branched, filiform, 0.1–0.3 mm, glabrous proximally;</text>
      <biological_entity id="o3652" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o3653" name="style" name_original="style" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s16" value="3-branched" value_original="3-branched" />
        <character is_modifier="false" name="shape" src="d0_s16" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.3" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas 3, linear along adaxial surface of style-branches, papillate (30×).</text>
      <biological_entity id="o3654" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o3655" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character constraint="along adaxial surface" constraintid="o3656" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" name="relief" notes="" src="d0_s17" value="papillate" value_original="papillate" />
        <character name="quantity" src="d0_s17" value="[30" value_original="[30" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3656" name="surface" name_original="surface" src="d0_s17" type="structure" />
      <biological_entity id="o3657" name="style-branch" name_original="style-branches" src="d0_s17" type="structure" />
      <relation from="o3656" id="r395" name="part_of" negation="false" src="d0_s17" to="o3657" />
    </statement>
    <statement id="d0_s18">
      <text>Capsules ovoid to spherical, opening by 3 incurved or twisting valves;</text>
      <biological_entity id="o3658" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s18" to="spherical" />
      </biological_entity>
      <biological_entity id="o3659" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s18" value="3" value_original="3" />
        <character is_modifier="true" name="orientation" src="d0_s18" value="incurved" value_original="incurved" />
        <character is_modifier="true" name="architecture" src="d0_s18" value="twisting" value_original="twisting" />
      </biological_entity>
      <relation from="o3658" id="r396" name="opening by" negation="false" src="d0_s18" to="o3659" />
    </statement>
    <statement id="d0_s19">
      <text>carpophore present.</text>
      <biological_entity id="o3660" name="carpophore" name_original="carpophore" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds ca. 8–15, whitish, ovoid to lenticular or triangular, laterally compressed to angular, papillate or granular, marginal wing absent, appendage absent.</text>
      <biological_entity id="o3661" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s20" to="15" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s20" to="lenticular or triangular laterally compressed" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s20" to="lenticular or triangular laterally compressed" />
        <character is_modifier="false" name="relief" src="d0_s20" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="relief" src="d0_s20" value="granular" value_original="granular" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o3662" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>x = [7], 8, 9.</text>
      <biological_entity id="o3663" name="appendage" name_original="appendage" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o3664" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="atypical_quantity" src="d0_s21" value="7" value_original="7" />
        <character name="quantity" src="d0_s21" value="8" value_original="8" />
        <character name="quantity" src="d0_s21" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, South America, Europe (including Mediterranean region), Asia, Africa; Polycarpon tetraphyllum introduced widely, including e North America, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe (including Mediterranean region)" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Polycarpon tetraphyllum  widely" establishment_means="introduced" />
        <character name="distribution" value="including e North America" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Manyseed</other_name>
  <discussion>Species ca. 9 or 15 (2 in the flora).</discussion>
  <discussion>C. Mohr’s (1901) report of Polycarpon alsinifolium (Bivona-Bernardi) de Candolle from Alabama was based on misidentification of P. tetraphyllum subsp. tetraphyllum.</discussion>
  <discussion>The shape of the sepal apex in Polycarpon is varied, with the herbaceous central portion sometimes prolonged beyond the scarious margins into a brief, narrow hood or awn, suggesting an aristate apex.</discussion>
  <references>
    <reference>Howell, J. T. 1941. Notes on Polycarpon. Leafl. W. Bot. 3: 80.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals 1.5-2.5 mm, keeled; stipules 1.8-2.8 mm; leaves in whorls of 4 or opposite</description>
      <determination>1 Polycarpon tetraphyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals 1-1.5 mm, flat or sometimes barely keeled; stipules 0.4-1.2 mm; leaves opposite, not whorled</description>
      <determination>2 Polycarpon depressum</determination>
    </key_statement>
  </key>
</bio:treatment>