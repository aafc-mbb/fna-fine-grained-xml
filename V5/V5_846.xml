<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">408</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Torrey in L. Sitgreaves" date="1853" rank="species">pharnaceoides</taxon_name>
    <taxon_name authority="Reveal" date="1974" rank="variety">cervinum</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>34: 246. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species pharnaceoides;variety cervinum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060460</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–3 dm.</text>
      <biological_entity id="o21653" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: petiole usually 0.1–0.3 (–0.5) cm;</text>
      <biological_entity constraint="basal" id="o21654" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o21655" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="0.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s1" to="0.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade 1–3 (–4) × 0.1–0.25 (–0.4) cm.</text>
      <biological_entity constraint="basal" id="o21656" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21657" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.25" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="0.4" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s2" to="0.25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 5–25 cm.</text>
      <biological_entity id="o21658" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Perianths yellow.</text>
      <biological_entity id="o21659" name="perianth" name_original="perianths" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly slopes, sagebrush and mountain mahogany communities, oak, pinyon-juniper and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="mountain mahogany communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1400-)1800-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1800" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2300" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>190b.</number>
  <other_name type="common_name">Deer Lodge wild buckwheat</other_name>
  <discussion>Variety cervinum is rare to infrequent in its limited range of eastern Lincoln County, Nevada, southwestern Iron and western Washington counties, Utah, and northeastern Mohave County, Arizona. It is totally disjunct from the typical variety.</discussion>
  
</bio:treatment>