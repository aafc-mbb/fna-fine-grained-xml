<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
    <other_info_on_meta type="mention_page">309</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="1836" rank="species">nudum</taxon_name>
    <taxon_name authority="(Jepson) Reveal in P. A. Munz" date="1968" rank="variety">indictum</taxon_name>
    <place_of_publication>
      <publication_title>in P. A. Munz, Suppl. Calif. Fl.,</publication_title>
      <place_in_publication>69. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species nudum;variety indictum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060418</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="species">indictum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Calif.</publication_title>
      <place_in_publication>1: 421. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species indictum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="species">latifolium</taxon_name>
    <taxon_name authority="(Jepson) S. Stokes" date="unknown" rank="variety">indictum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species latifolium;variety indictum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–8 (–10) dm.</text>
      <biological_entity id="o25376" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems strongly fistulose, 2.5–3.5 dm, glabrous.</text>
      <biological_entity id="o25377" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s1" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s1" to="3.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sheathing;</text>
      <biological_entity id="o25378" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 1–6 × 1–2 (–3) cm, densely tomentose abaxially, less so to floccose adaxially, margins strongly undulate-crisped.</text>
      <biological_entity id="o25379" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="less; adaxially" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o25380" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="undulate-crisped" value_original="undulate-crisped" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences cymose, 25–50 × 15–25 cm;</text>
      <biological_entity id="o25381" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s4" to="50" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s4" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches glabrous.</text>
      <biological_entity id="o25382" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 1 per node, 4–5 mm, glabrous.</text>
      <biological_entity id="o25383" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o25384" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25384" name="node" name_original="node" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 2.5–3 mm;</text>
      <biological_entity id="o25385" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth pale yellowish white to yellow or white, glabrous.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 80.</text>
      <biological_entity id="o25386" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale yellowish" value_original="pale yellowish" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="yellow or white" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25387" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clayey slopes, grassland communities, oak and conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey slopes" />
        <character name="habitat" value="grassland communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>91j.</number>
  <other_name type="common_name">Protruding wild buckwheat</other_name>
  <discussion>Variety indictum is found on the inner Coast Ranges of central California. For the most part, its range is east of that of var. auriculatum, occurring in Fresno, western Kern, Kings, Merced, eastern Monterey, San Benito, and eastern San Luis Obispo counties.</discussion>
  
</bio:treatment>