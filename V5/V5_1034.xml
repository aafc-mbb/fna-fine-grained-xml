<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">505</other_info_on_meta>
    <other_info_on_meta type="mention_page">491</other_info_on_meta>
    <other_info_on_meta type="illustration_page">506</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">venosus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 733. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species venosus;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060806</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous or nearly so, with creeping rhizomes.</text>
      <biological_entity id="o6385" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s0" value="nearly" value_original="nearly" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6386" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
      <relation from="o6385" id="r694" name="with" negation="false" src="d0_s0" to="o6386" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or, rarely, erect, usually producing axillary shoots near base, (10–) 15–30 (–40) cm.</text>
      <biological_entity id="o6387" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character name="orientation" src="d0_s1" value="," value_original="," />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s1" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o6388" name="shoot" name_original="shoots" src="d0_s1" type="structure" />
      <biological_entity id="o6389" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o6387" id="r695" modifier="usually" name="producing" negation="false" src="d0_s1" to="o6388" />
      <relation from="o6387" id="r696" modifier="usually" name="near" negation="false" src="d0_s1" to="o6389" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ovate-elliptic, obovate-elliptic, or ovatelanceolate, (2–) 4–12 (–15) × 1–5 (–6) cm, subcoriaceous, base narrowly to broadly cuneate, margins entire, flat or slightly undulate, apex acute or acuminate.</text>
      <biological_entity id="o6390" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate-elliptic" value_original="obovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate-elliptic" value_original="obovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s2" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o6391" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o6392" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o6393" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal and axillary, usually occupying distal 2/3 of stem/shoot, usually dense, or interrupted in proximal part, broadly paniculate.</text>
      <biological_entity id="o6394" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually; usually" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character constraint="in proximal part" constraintid="o6396" is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="broadly" name="arrangement" notes="" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o6395" name="shoot" name_original="stem/shoot" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s3" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6396" name="part" name_original="part" src="d0_s3" type="structure" />
      <relation from="o6394" id="r697" modifier="usually" name="occupying" negation="false" src="d0_s3" to="o6395" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels articulated near middle, filiform or slightly thickened, (8–) 10–16 mm, articulation distinct, slightly swollen.</text>
      <biological_entity id="o6397" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character constraint="near middle" constraintid="o6398" is_modifier="false" name="architecture" src="d0_s4" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="slightly" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6398" name="middle" name_original="middle" src="d0_s4" type="structure" />
      <biological_entity id="o6399" name="articulation" name_original="articulation" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 5–15 in whorls;</text>
      <biological_entity id="o6400" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o6401" from="5" name="quantity" src="d0_s5" to="15" />
      </biological_entity>
      <biological_entity id="o6401" name="whorl" name_original="whorls" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>inner tepals distinctly double-reticulately veined, orbiculate or reniform-orbiculate, 13–18 (–20) × (20–) 23–30 mm, base deeply emarginate or cordate, margins entire, apex rounded, obtuse, rarely subacute, with short, broadly triangular tip;</text>
      <biological_entity constraint="inner" id="o6402" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distinctly double-reticulately" name="architecture" src="d0_s6" value="veined" value_original="veined" />
        <character is_modifier="false" name="shape" src="d0_s6" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="reniform-orbiculate" value_original="reniform-orbiculate" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s6" to="18" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_width" src="d0_s6" to="23" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="23" from_unit="mm" name="width" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6403" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s6" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o6404" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o6405" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="subacute" value_original="subacute" />
      </biological_entity>
      <biological_entity id="o6406" name="tip" name_original="tip" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
      <relation from="o6405" id="r698" name="with" negation="false" src="d0_s6" to="o6406" />
    </statement>
    <statement id="d0_s7">
      <text>tubercles absent, occasionally very small.</text>
      <biological_entity id="o6407" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="occasionally very" name="size" src="d0_s7" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes brown or dark-brown, 5–7 × 4–6 mm. 2n = 40.</text>
      <biological_entity id="o6408" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6409" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand dunes, sandy and gravelly riverbanks and slopes, deserts, grasslands 200-1500 m</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly riverbanks" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="deserts" />
        <character name="habitat" value="200-1500 m" modifier="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Ariz., Calif., Colo., Idaho, Iowa, Kans., Minn., Mont., Nebr., Nev., N.Mex., N.Dak., Okla., Oreg., S.Dak., Tex., Utah, Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Veined or veiny dock</other_name>
  <other_name type="common_name">wild-begonia</other_name>
  <other_name type="common_name">rumex veine</other_name>
  <discussion>Rumex venosus is a distinctive species rarely confused with any other members of the genus. However, I have seen herbarium specimens of it misidentified as R. hymenosepalus, and vice versa.</discussion>
  
</bio:treatment>