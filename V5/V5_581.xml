<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">295</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="C. V. Morton" date="1935" rank="species">panamintense</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>25: 308. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species panamintense;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060450</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">racemosum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="variety">desertorum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species racemosum;variety desertorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="species">reliquum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species reliquum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect, 1.5–3 × 1–4 dm, tomentose, brownish.</text>
      <biological_entity id="o6786" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="length" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems slightly spreading to erect, without persistent leaf-bases, up to 1/5 height of plant;</text>
      <biological_entity id="o6787" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="slightly spreading" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
      <biological_entity id="o6788" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/5 height of plant" />
      </biological_entity>
      <relation from="o6787" id="r740" name="without" negation="false" src="d0_s1" to="o6788" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems absent;</text>
      <biological_entity constraint="caudex" id="o6789" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems erect, slender, solid, not fistulose, 0.5–1.5 dm, tomentose.</text>
      <biological_entity id="o6790" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s3" to="1.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal, 1 per node;</text>
      <biological_entity id="o6791" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="per node" constraintid="o6792" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6792" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 1–5 cm, tomentose;</text>
      <biological_entity id="o6793" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade elliptic to ovate or obovate, 1.5–4 × 1–2.5 cm, white-tomentose abaxially, less so and often greenish adaxially, margins plane.</text>
      <biological_entity id="o6794" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="ovate or obovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="less; often; adaxially" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o6795" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose with involucres racemosely disposed at tips, (10–) 12–20 × 1–5 cm;</text>
      <biological_entity id="o6796" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character constraint="with involucres" constraintid="o6797" is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_length" notes="" src="d0_s7" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" notes="" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s7" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6797" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character constraint="at tips" constraintid="o6798" is_modifier="false" modifier="racemosely" name="arrangement" src="d0_s7" value="disposed" value_original="disposed" />
      </biological_entity>
      <biological_entity id="o6798" name="tip" name_original="tips" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, tomentose;</text>
      <biological_entity id="o6799" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, narrowly triangular, and 1–4 mm, or leaflike, elliptic to oval, and 5–15 × 5–15 mm.</text>
      <biological_entity id="o6800" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="oval" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o6801" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node, turbinate, 3–5 × 2–4 mm, tomentose;</text>
      <biological_entity id="o6802" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o6803" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o6803" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.4–0.8 mm.</text>
      <biological_entity id="o6804" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 3.5–5 mm;</text>
      <biological_entity id="o6805" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white to whitish brown, glabrous;</text>
      <biological_entity id="o6806" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="whitish brown" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, monomorphic, oblanceolate;</text>
      <biological_entity id="o6807" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 3–7 mm;</text>
      <biological_entity id="o6808" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o6809" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 2.5–3 mm, glabrous.</text>
      <biological_entity id="o6810" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky to gravelly slopes and ridges, sagebrush and mountain mahogany communities, pinyon-juniper and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky to gravelly slopes" />
        <character name="habitat" value="rocky to ridges" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="mountain mahogany communities" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1600-)1800-2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1800" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2900" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>76.</number>
  <other_name type="common_name">Panamint Mountain wild buckwheat</other_name>
  <discussion>Eriogonum panamintense is widely scattered in the desert ranges from the southern tip of the White Mountains in Inyo County to the New York Mountains of San Bernardino County, California, eastward across Nevada in the Magruder and Silver Peak (Esmeralda County), Grapevine (Nye County), and Spring (Charleston) mountains (Clark County) of Nevada, on the Hualapai Mountains south of Kingman, and on Mt. Bangs south of Littlefield, Mohave County, Arizona. The Panamint Mountain wild buckwheat typically grows at elevations in California higher than does E. rupinum, but in Nevada their elevation ranges overlap, as in the Magruder Mountains and near Westgard Pass in the White Mountains.</discussion>
  
</bio:treatment>