<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">247</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">microthecum</taxon_name>
    <taxon_name authority="Reveal" date="2004" rank="variety">arceuthinum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>86: 135. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species microthecum;variety arceuthinum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060389</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 0.5–0.7 × 1–1.5 dm.</text>
      <biological_entity id="o20648" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="0.7" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="1.5" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex spreading;</text>
      <biological_entity id="o20649" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o20650" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems 0.1–0.3 dm, glabrous.</text>
      <biological_entity id="o20651" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o20652" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s2" to="0.3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade linear, 0.5–0.8 × 0.05–0.1 cm, densely white-tomentose adaxially, sparsely floccose or glabrous and green adaxially, margins revolute.</text>
      <biological_entity id="o20653" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o20654" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.05" from_unit="cm" name="width" src="d0_s3" to="0.1" to_unit="cm" />
        <character is_modifier="false" modifier="densely; adaxially" name="pubescence" src="d0_s3" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o20655" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 0.5–2.5 cm;</text>
      <biological_entity id="o20656" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches sparsely floccose or glabrous.</text>
      <biological_entity id="o20657" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 2–3 mm, glabrous.</text>
      <biological_entity id="o20658" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 1.5–2 mm;</text>
      <biological_entity id="o20659" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth white.</text>
      <biological_entity id="o20660" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes 1.5–2 mm.</text>
      <biological_entity id="o20661" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Volcanic slopes and cliffs, oak and pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="volcanic slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1j.</number>
  <other_name type="common_name">Juniper Mountain wild buckwheat</other_name>
  <discussion>Variety arceuthinum was gathered originally by Carl Purpus in the “Juniper Mountains” in 1898. It is uncertain whether his collection came from southwestern Iron County, Utah, or from southeastern Lincoln County, Nevada. A single, sterile collection (Shultz &amp; Shultz 7109, BRY, GH) with dubious label data supposedly was found in Nevada, and a second collection from that state, made by A. Jerry Tiehm and sent to L. M. Schulz, apparently has been lost.</discussion>
  
</bio:treatment>