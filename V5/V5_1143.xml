<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">560</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="mention_page">549</other_info_on_meta>
    <other_info_on_meta type="mention_page">552</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">polygonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Polygonum</taxon_name>
    <taxon_name authority="M. Bieberstein" date="1808" rank="species">patulum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Taur.-Caucas.</publication_title>
      <place_in_publication>1: 304. 1808</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonum;section polygonum;species patulum;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242100128</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green or bluish green, heterophyllous.</text>
      <biological_entity id="o25419" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="heterophyllous" value_original="heterophyllous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly ascending or erect, branched from base, not wiry, 20–80 cm.</text>
      <biological_entity id="o25420" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from base" constraintid="o25421" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s1" value="wiry" value_original="wiry" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25421" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: ocrea 7–9 mm, proximal part cylindic, distal part disintegrating into straight fibers;</text>
      <biological_entity id="o25422" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25423" name="ocreum" name_original="ocrea" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o25424" name="part" name_original="part" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o25425" name="part" name_original="part" src="d0_s2" type="structure">
        <character constraint="into fibers" constraintid="o25426" is_modifier="false" name="dehiscence" src="d0_s2" value="disintegrating" value_original="disintegrating" />
      </biological_entity>
      <biological_entity id="o25426" name="fiber" name_original="fibers" src="d0_s2" type="structure">
        <character is_modifier="true" name="course" src="d0_s2" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.2–1 mm;</text>
      <biological_entity id="o25427" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o25428" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade green or bluish green, linear to lanceolate, 25–40 × 4–8 mm, margins flat, apex acute;</text>
      <biological_entity id="o25429" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o25430" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="bluish green" value_original="bluish green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25431" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o25432" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stem-leaves 2–4 times as long as branch leaves;</text>
      <biological_entity id="o25433" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o25434" name="stem-leaf" name_original="stem-leaves" src="d0_s5" type="structure">
        <character constraint="leaf" constraintid="o25435" is_modifier="false" name="length" src="d0_s5" value="2-4 times as long as branch leaves" />
      </biological_entity>
      <biological_entity constraint="branch" id="o25435" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal leaves abruptly reduced and not overtopping flowers (shorter than or equaling flowers).</text>
      <biological_entity id="o25436" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o25437" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o25438" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <relation from="o25437" id="r2827" name="overtopping" negation="true" src="d0_s6" to="o25438" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary and terminal, spikelike;</text>
      <biological_entity id="o25439" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cymes aggregated at tips of stems and branches, 1–3-flowered.</text>
      <biological_entity id="o25440" name="cyme" name_original="cymes" src="d0_s8" type="structure">
        <character constraint="at tips" constraintid="o25441" is_modifier="false" name="arrangement" src="d0_s8" value="aggregated" value_original="aggregated" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
      <biological_entity id="o25441" name="tip" name_original="tips" src="d0_s8" type="structure" />
      <biological_entity id="o25442" name="stem" name_original="stems" src="d0_s8" type="structure" />
      <biological_entity id="o25443" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <relation from="o25441" id="r2828" name="part_of" negation="false" src="d0_s8" to="o25442" />
      <relation from="o25441" id="r2829" name="part_of" negation="false" src="d0_s8" to="o25443" />
    </statement>
    <statement id="d0_s9">
      <text>Pedicels enclosed in ocreae, 1.5–2 mm.</text>
      <biological_entity id="o25444" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25445" name="ocrea" name_original="ocreae" src="d0_s9" type="structure" />
      <relation from="o25444" id="r2830" name="enclosed in" negation="false" src="d0_s9" to="o25445" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers closed or semi-open;</text>
      <biological_entity id="o25446" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="condition" src="d0_s10" value="closed" value_original="closed" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="semi-open" value_original="semi-open" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perianth 2.2–3 mm;</text>
      <biological_entity id="o25447" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>tube 15–30% of perianth length;</text>
      <biological_entity id="o25448" name="tube" name_original="tube" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>tepals overlapping, green with white to pink margins, petaloid, not keeled, oblong, cucullate;</text>
      <biological_entity id="o25449" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s13" value="overlapping" value_original="overlapping" />
        <character constraint="with margins" constraintid="o25450" is_modifier="false" name="coloration" src="d0_s13" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cucullate" value_original="cucullate" />
      </biological_entity>
      <biological_entity id="o25450" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" is_modifier="true" name="coloration" src="d0_s13" to="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>veins branched;</text>
      <biological_entity id="o25451" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens 8.</text>
      <biological_entity id="o25452" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes slightly exserted from perianth, light-brown to dark-brown, ovate, 3-gonous, 2–2.3 (–2.8) mm, faces subequal, concave, apex not beaked, edges concave, dull, striate-tubercled;</text>
      <biological_entity id="o25453" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s16" to="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="2.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25454" name="perianth" name_original="perianth" src="d0_s16" type="structure" />
      <biological_entity id="o25455" name="face" name_original="faces" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="shape" src="d0_s16" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o25456" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s16" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o25457" name="edge" name_original="edges" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="concave" value_original="concave" />
        <character is_modifier="false" name="reflectance" src="d0_s16" value="dull" value_original="dull" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="striate-tubercled" value_original="striate-tubercled" />
      </biological_entity>
      <relation from="o25453" id="r2831" modifier="slightly" name="exserted from" negation="false" src="d0_s16" to="o25454" />
    </statement>
    <statement id="d0_s17">
      <text>late-season achenes uncommon, 2.5–4 mm.</text>
      <biological_entity constraint="late-season" id="o25458" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s17" value="uncommon" value_original="uncommon" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ill., Wash.; Eurasia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>The name Polygonum patulum has been misapplied to a distinctive form of P. ramosissimum in saline marshes in California (M. Costea and F. J. Tardif 2003b).</discussion>
  
</bio:treatment>