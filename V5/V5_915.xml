<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="mention_page">436</other_info_on_meta>
    <other_info_on_meta type="mention_page">438</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Small" date="1898" rank="genus">acanthoscyphus</taxon_name>
    <taxon_name authority="(Parry) Small" date="1898" rank="species">parishii</taxon_name>
    <taxon_name authority="(Ertter) Reveal" date="2004" rank="variety">goodmaniana</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>9: 144. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus acanthoscyphus;species parishii;variety goodmaniana;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060004</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxytheca</taxon_name>
    <taxon_name authority="Parry" date="unknown" rank="species">parishii</taxon_name>
    <taxon_name authority="Ertter" date="unknown" rank="variety">goodmaniana</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>32: 90, fig. 6H–J. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Oxytheca;species parishii;variety goodmaniana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect or spreading, 0.5–3 dm.</text>
      <biological_entity id="o25335" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems slender, (1.5–) 2–6 cm.</text>
      <biological_entity id="o25336" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 1–3 cm.</text>
      <biological_entity id="o25337" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: bracts 2 (–3), subulate, awns 0.5–1.5 mm.</text>
      <biological_entity id="o25338" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o25339" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="3" />
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o25340" name="awn" name_original="awns" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 0.3–1.5 (–2) cm.</text>
      <biological_entity id="o25341" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s4" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres: awns 4 (–5), ivory colored, 2–3 mm.</text>
      <biological_entity id="o25342" name="involucre" name_original="involucres" src="d0_s5" type="structure" />
      <biological_entity id="o25343" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="5" />
        <character name="quantity" src="d0_s5" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="ivory colored" value_original="ivory colored" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy calcareous slopes, chaparral communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy calcareous slopes" />
        <character name="habitat" value="chaparral communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1c.</number>
  <other_name type="common_name">Cushenbury flowery puncturebract</other_name>
  <discussion>Variety goodmaniana is known only from the northeastern slope of the San Bernardino Mountains in San Bernardino County. The name Oxytheca watsonii was misapplied to this taxon in the pre-1980 California literature.</discussion>
  
</bio:treatment>