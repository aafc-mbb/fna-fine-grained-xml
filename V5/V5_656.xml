<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">225</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="species">gilmanii</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>3: 16. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species gilmanii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060295</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, cespitose or pulvinate, occasionally scapose, 0.1–0.5 × 1–3 dm, tomentose.</text>
      <biological_entity id="o29394" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="shape" src="d0_s0" value="pulvinate" value_original="pulvinate" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="length" src="d0_s0" to="0.5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, with persistent leaf-bases, up to 1/5 height of plant;</text>
      <biological_entity id="o29395" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o29396" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/5 height of plant" />
      </biological_entity>
      <relation from="o29395" id="r3307" name="with" negation="false" src="d0_s1" to="o29396" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems matted;</text>
      <biological_entity constraint="caudex" id="o29397" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems scapelike, spreading to erect, slender, solid, not fistulose, 0.1–0.2 dm, tomentose.</text>
      <biological_entity id="o29398" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s3" to="0.2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal, 1 per node;</text>
      <biological_entity id="o29399" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="per node" constraintid="o29400" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o29400" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.2–0.5 cm, tomentose;</text>
      <biological_entity id="o29401" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s5" to="0.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade elliptic, 0.2–0.4 × 0.1–0.2 cm, tomentose, margins plane.</text>
      <biological_entity id="o29402" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="length" src="d0_s6" to="0.4" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s6" to="0.2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o29403" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose-umbellate or capitate, (0.4–) 0.8–1.5 × 0.5–1.5 cm;</text>
      <biological_entity id="o29404" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose-umbellate" value_original="cymose-umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_length" src="d0_s7" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s7" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches umbellate, tomentose;</text>
      <biological_entity id="o29405" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, triangular to linear-lanceolate, 1–5 mm.</text>
      <biological_entity id="o29406" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s9" to="linear-lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles spreading, 0.1–0.3 (–0.5) cm, tomentose.</text>
      <biological_entity id="o29407" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="0.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s10" to="0.3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node, turbinate, 1.5–2 × 1–1.5 mm, tomentose;</text>
      <biological_entity id="o29408" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o29409" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o29409" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.4–0.7 mm.</text>
      <biological_entity id="o29410" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 3.5–5 mm;</text>
      <biological_entity id="o29411" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth yellowish, with reddish spot, glabrous;</text>
      <biological_entity id="o29412" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="with reddish spot" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximally, dimorphic, those of outer whorl orbiculate and globose, 3–4 × 3–4 mm, those of inner whorl oblanceolate, 3.5–5 × 1–1.5 mm;</text>
      <biological_entity id="o29413" name="tepal" name_original="tepals" src="d0_s15" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s15" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o29414" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <biological_entity constraint="inner" id="o29415" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <relation from="o29413" id="r3308" name="part_of" negation="false" src="d0_s15" to="o29414" />
      <relation from="o29413" id="r3309" name="part_of" negation="false" src="d0_s15" to="o29415" />
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 3–4 mm;</text>
      <biological_entity id="o29416" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pubescent proximally.</text>
      <biological_entity id="o29417" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes brown, 2.5–3 mm, glabrous.</text>
      <biological_entity id="o29418" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone gravelly to rocky slopes and ridges, sagebrush communities, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone gravelly to rocky slopes" />
        <character name="habitat" value="limestone gravelly to ridges" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>99.</number>
  <other_name type="past_name">gilmani</other_name>
  <other_name type="common_name">Gilman’s wild buckwheat</other_name>
  <discussion>Eriogonum gilmanii is a rare, localized species. It is known from the Cottonwood, Last Chance, and Panamint mountains of Inyo County.</discussion>
  
</bio:treatment>