<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">145</other_info_on_meta>
    <other_info_on_meta type="mention_page">146</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sagina</taxon_name>
    <taxon_name authority="(Elliott) Torrey &amp; A. Gray" date="1838" rank="species">decumbens</taxon_name>
    <taxon_name authority="(S. Watson) G. E. Crow" date="1978" rank="subspecies">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>80: 68. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus sagina;species decumbens;subspecies occidentalis;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060811</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sagina</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 344. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sagina;species occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: basal rosette absent.</text>
      <biological_entity id="o6378" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o6379" name="rosette" name_original="rosette" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sepals ovate to orbiculate, body and hyaline margins frequently purple.</text>
      <biological_entity id="o6380" name="sepal" name_original="sepals" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="orbiculate" />
      </biological_entity>
      <biological_entity id="o6381" name="body" name_original="body" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="frequently" name="coloration_or_density" src="d0_s1" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o6382" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="frequently" name="coloration_or_density" src="d0_s1" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Seeds light-brown, seldom ridged, and then never forming reticulate pattern, smooth to slightly pebbled.</text>
      <biological_entity id="o6383" name="seed" name_original="seeds" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="seldom" name="shape" src="d0_s2" value="ridged" value_original="ridged" />
        <character char_type="range_value" from="smooth" modifier="never" name="relief" src="d0_s2" to="slightly pebbled" />
      </biological_entity>
      <biological_entity id="o6384" name="pattern" name_original="pattern" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_coloration_or_relief" src="d0_s2" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <relation from="o6383" id="r693" modifier="never" name="forming" negation="false" src="d0_s2" to="o6384" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dryish hillsides, margins of vernal pools, streams, open spots in redwood and pine woods, roadsides, dwellings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dryish hillsides" />
        <character name="habitat" value="margins" constraint="of vernal pools" />
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="open spots" constraint="in redwood and pine woods" />
        <character name="habitat" value="redwood" />
        <character name="habitat" value="pine woods" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7b.</number>
  <discussion>Except by geography, subsp. occidentalis is very difficult to distinguish from subsp. decumbens. In plants of subsp. occidentalis the sepals tend to be more orbiculate and the capsules, prior to dehiscence, tend to be more globose. Extremely variable, subsp. decumbens generally can be recognized on the basis of presence of tuberculate seeds (60% frequency) and 80% have a combination of tuberculate seeds and glandular-pubescent pedicels and calyx bases. But when seeds are smooth, seeing the reticulate ridge pattern requires high magnification, and while SEM readily clarifies the differences, its use is hardly practical. Subspecies decumbens has a greater tendency to possess purple sepal tips or sepal margins, and purplish coloration frequently at the nodes.</discussion>
  
</bio:treatment>