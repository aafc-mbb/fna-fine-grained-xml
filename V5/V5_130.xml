<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">63</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">59</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(Rydberg) Ikonnikov" date="unknown" rank="species">eastwoodiae</taxon_name>
    <place_of_publication>
      <publication_title>Novosti Syst. Vyssh. Rast.</publication_title>
      <place_in_publication>10: 139. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species eastwoodiae;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060140</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">eastwoodiae</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>31: 406. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species eastwoodiae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">fendleri</taxon_name>
    <taxon_name authority="(Rydberg) S. L. Welsh" date="unknown" rank="variety">eastwoodiae</taxon_name>
    <taxon_hierarchy>genus Arenaria;species fendleri;variety eastwoodiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely matted, green, not glaucous, with woody base.</text>
      <biological_entity id="o9941" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9942" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o9941" id="r1109" name="with" negation="false" src="d0_s0" to="o9942" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, (8–) 10–25 cm, glabrous or stipitate-glandular.</text>
      <biological_entity id="o9943" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves persistent;</text>
      <biological_entity id="o9944" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o9945" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves usually in 2–4 pairs, reduced distally;</text>
      <biological_entity id="o9946" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o9947" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o9948" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <relation from="o9947" id="r1110" name="in" negation="false" src="d0_s3" to="o9948" />
    </statement>
    <statement id="d0_s4">
      <text>basal blades spreading to recurved, needlelike, 1–3 (–3.5) cm × 0.5–0.7 mm, flexuous to rigid, herbaceous, apex spinose, glabrous to puberulent, not glaucous.</text>
      <biological_entity id="o9949" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o9950" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="recurved" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="needlelike" value_original="needlelike" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s4" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o9951" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinose" value_original="spinose" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="puberulent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (1–) 3–17-flowered, ± open cymes.</text>
      <biological_entity id="o9952" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(1-)3-17-flowered" value_original="(1-)3-17-flowered" />
      </biological_entity>
      <biological_entity id="o9953" name="cyme" name_original="cymes" src="d0_s5" type="structure" />
      <relation from="o9952" id="r1111" modifier="more or less" name="open" negation="false" src="d0_s5" to="o9953" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 3–30 mm, glabrous or stipitate-glandular.</text>
      <biological_entity id="o9954" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals green or purplish, 1–3-veined, lanceolate to ovatelanceolate, (3.5–) 4–6.5 mm, not enlarging in fruit, margins broad, apex narrowly acute to acuminate, glabrous or stipitate-glandular;</text>
      <biological_entity id="o9955" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o9956" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-3-veined" value_original="1-3-veined" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="ovatelanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6.5" to_unit="mm" />
        <character constraint="in fruit" constraintid="o9957" is_modifier="false" modifier="not" name="size" src="d0_s7" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o9957" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o9958" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o9959" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s7" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellowish white or sometimes brownish to reddish-pink, broadly oblongelliptic to oblanceolate, 4–6.5 mm, 0.9–1.1 times as long as sepals, apex rounded;</text>
      <biological_entity id="o9960" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9961" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish white" value_original="yellowish white" />
        <character name="coloration" src="d0_s8" value="sometimes" value_original="sometimes" />
        <character char_type="range_value" from="brownish" name="coloration" src="d0_s8" to="reddish-pink" />
        <character char_type="range_value" from="broadly oblongelliptic" name="shape" src="d0_s8" to="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6.5" to_unit="mm" />
        <character constraint="sepal" constraintid="o9962" is_modifier="false" name="length" src="d0_s8" value="0.9-1.1 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o9962" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity id="o9963" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectaries narrowly longitudinally rectangular, apically cleft or emarginate, adjacent to filaments opposite sepals, 1–2 mm.</text>
      <biological_entity id="o9964" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9965" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly longitudinally" name="shape" src="d0_s9" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s9" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="emarginate" value_original="emarginate" />
        <character constraint="to filaments" constraintid="o9966" is_modifier="false" name="arrangement" src="d0_s9" value="adjacent" value_original="adjacent" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9966" name="filament" name_original="filaments" src="d0_s9" type="structure" />
      <biological_entity id="o9967" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 4–6 mm, glabrous.</text>
      <biological_entity id="o9968" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds brown, ovoid to suborbicular with hilar notch, 1.2–1.7 mm, papillate, subechinate;</text>
      <biological_entity id="o9969" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character char_type="range_value" constraint="with hilar, notch" constraintid="o9970, o9971" from="ovoid" name="shape" src="d0_s11" to="suborbicular" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="subechinate" value_original="subechinate" />
      </biological_entity>
      <biological_entity id="o9970" name="hilar" name_original="hilar" src="d0_s11" type="structure" />
      <biological_entity id="o9971" name="notch" name_original="notch" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>tubercles conical.</text>
      <biological_entity id="o9972" name="tubercle" name_original="tubercles" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="conical" value_original="conical" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="past_name">eastwoodii</other_name>
  <other_name type="common_name">Eastwood’s sandwort</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The Hopi Indians may use Eremogone eastwoodiae as an emetic (B. Maguire 1960).</discussion>
  <discussion>The nectaries in Eremogone eastwoodiae are different from those of most other species of the genus in North America since they are a separate bilobed structure adjacent to, but not a direct enlargement of, the filament bases opposite the sepals.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and pedicels glabrous</description>
      <determination>5a Eremogone eastwoodiae var. eastwoodiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and pedicels stipitate-glandular</description>
      <determination>5b Eremogone eastwoodiae var. adenophora</determination>
    </key_statement>
  </key>
</bio:treatment>