<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">319</other_info_on_meta>
    <other_info_on_meta type="mention_page">322</other_info_on_meta>
    <other_info_on_meta type="illustration_page">305</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Bentham" date="1836" rank="species">strictum</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) C. L. Hitchcock in C. L. Hitchcock et al." date="1964" rank="variety">proliferum</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>2: 132. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species strictum;variety proliferum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060511</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">proliferum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 164. 1870</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species proliferum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="species">fulvum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species fulvum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">strictum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="variety">argenteum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species strictum;variety argenteum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">strictum</taxon_name>
    <taxon_name authority="(S. Stokes) S. Stokes" date="unknown" rank="subspecies">bellum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species strictum;subspecies bellum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">strictum</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) S. Stokes" date="unknown" rank="subspecies">proliferum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species strictum;subspecies proliferum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loose mats, 2–4 dm wide.</text>
      <biological_entity id="o4778" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="width" notes="" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4779" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades broadly elliptic to ovate, 1–3 cm, grayish-tomentose to floccose on both surfaces, sometimes greenish-tomentose to floccose and greenish adaxially.</text>
      <biological_entity id="o4780" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s1" to="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" constraint="on surfaces" constraintid="o4781" from="grayish-tomentose" name="pubescence" src="d0_s1" to="floccose" />
        <character char_type="range_value" from="greenish-tomentose" modifier="sometimes" name="pubescence" notes="" src="d0_s1" to="floccose" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s1" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o4781" name="surface" name_original="surfaces" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 5–15 cm;</text>
      <biological_entity id="o4782" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches tomentose to floccose.</text>
      <biological_entity id="o4783" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s3" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 4–6 × 2–4 mm, tomentose.</text>
      <biological_entity id="o4784" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 3–5 mm;</text>
      <biological_entity id="o4785" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth white to rose or purple.</text>
      <biological_entity id="o4786" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s6" to="rose or purple" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, sagebrush communities, montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="conifer woodlands" modifier="montane" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(100-)400-2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="400" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>96a.</number>
  <other_name type="common_name">Proliferous wild buckwheat</other_name>
  <discussion>Variety proliferum is widespread and often rather common throughout its range. The largest concentration is found in a gentle arc from northeastern Washington to southern Idaho and western Montana. The variety is widely distributed also in central and eastern Oregon, northern California, and Nevada. In portions of central Idaho and western Montana, some individuals clearly approach Eriogonum ovalifolium var. pansum.</discussion>
  
</bio:treatment>