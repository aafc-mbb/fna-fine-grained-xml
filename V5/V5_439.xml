<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John W. Thieret</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">214</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">AGROSTEMMA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 435. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 198. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus agrostemma;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek agros, field, and stemma, crown or wreath, alluding to the flowers’ use in garlands</other_info_on_name>
    <other_info_on_name type="fna_id">100851</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o13618" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots stout.</text>
      <biological_entity id="o13619" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple or branched, terete.</text>
      <biological_entity id="o13620" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves connate proximally into sheath, sessile;</text>
      <biological_entity id="o13621" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="into sheath" constraintid="o13622" is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o13622" name="sheath" name_original="sheath" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade 1-veined or obscurely 3-veined, linear to narrowly lanceolate, apex acute.</text>
      <biological_entity id="o13623" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o13624" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, lax cymes or of solitary, mostly axillary flowers;</text>
      <biological_entity id="o13625" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o13626" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o13627" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <relation from="o13626" id="r1489" name="part_of" negation="false" src="d0_s5" to="o13627" />
    </statement>
    <statement id="d0_s6">
      <text>bracts, when present, paired, foliaceous;</text>
      <biological_entity id="o13628" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="when present" name="arrangement" src="d0_s6" value="paired" value_original="paired" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>involucel bracteoles absent.</text>
      <biological_entity constraint="involucel" id="o13629" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect.</text>
      <biological_entity id="o13630" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual or rarely unisexual and pistillate;</text>
      <biological_entity id="o13631" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals connate proximally into tube, 25–62 mm;</text>
      <biological_entity id="o13632" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character constraint="into tube" constraintid="o13633" is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="62" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13633" name="tube" name_original="tube" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>tube green, 10-veined, cylindric to ovoid, terete, commissures between sepals 1-veined, herbaceous;</text>
      <biological_entity id="o13634" name="tube" name_original="tube" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="10-veined" value_original="10-veined" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s11" to="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity constraint="between sepals" constraintid="o13636" id="o13635" name="commissure" name_original="commissures" src="d0_s11" type="structure" constraint_original="between  sepals, ">
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s11" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o13636" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lobes green, 1-veined, linear-lanceolate, [shorter or] longer than tube, often equaling or longer than petals, margins green, herbaceous, apex acute;</text>
      <biological_entity id="o13637" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s12" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character constraint="than tube" constraintid="o13638" is_modifier="false" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="often" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
        <character constraint="than petals" constraintid="o13639" is_modifier="false" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o13638" name="tube" name_original="tube" src="d0_s12" type="structure" />
      <biological_entity id="o13639" name="petal" name_original="petals" src="d0_s12" type="structure" />
      <biological_entity id="o13640" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s12" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o13641" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 5, purplish red or white, clawed, auricles absent, coronal appendages absent, blade apex obtuse, entire or briefly emarginate;</text>
      <biological_entity id="o13642" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish red" value_original="purplish red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o13643" name="auricle" name_original="auricles" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="coronal" id="o13644" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="blade" id="o13645" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="briefly" name="architecture_or_shape" src="d0_s13" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>nectaries near bases of filaments opposite sepals;</text>
      <biological_entity id="o13646" name="nectary" name_original="nectaries" src="d0_s14" type="structure" />
      <biological_entity id="o13647" name="base" name_original="bases" src="d0_s14" type="structure" />
      <biological_entity id="o13648" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o13649" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o13646" id="r1490" name="near" negation="false" src="d0_s14" to="o13647" />
      <relation from="o13647" id="r1491" name="part_of" negation="false" src="d0_s14" to="o13648" />
      <relation from="o13647" id="r1492" name="part_of" negation="false" src="d0_s14" to="o13649" />
    </statement>
    <statement id="d0_s15">
      <text>stamens 10, 5 adnate to petals, 5 at base of gynoecium;</text>
      <biological_entity id="o13650" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character constraint="to petals" constraintid="o13651" is_modifier="false" name="fusion" src="d0_s15" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o13651" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character constraint="at base" constraintid="o13652" name="quantity" src="d0_s15" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o13652" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity id="o13653" name="gynoecium" name_original="gynoecium" src="d0_s15" type="structure" />
      <relation from="o13652" id="r1493" name="part_of" negation="false" src="d0_s15" to="o13653" />
    </statement>
    <statement id="d0_s16">
      <text>filaments distinct;</text>
      <biological_entity id="o13654" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>staminodes absent;</text>
      <biological_entity id="o13655" name="staminode" name_original="staminodes" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles (4–) 5, clavate, 10–12 mm, with dense, stiffly ascending hairs proximally;</text>
      <biological_entity id="o13656" name="style" name_original="styles" src="d0_s18" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s18" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s18" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s18" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s18" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13657" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character is_modifier="true" name="density" src="d0_s18" value="dense" value_original="dense" />
        <character is_modifier="true" modifier="stiffly" name="orientation" src="d0_s18" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o13656" id="r1494" name="with" negation="false" src="d0_s18" to="o13657" />
    </statement>
    <statement id="d0_s19">
      <text>stigmas (4–) 5, subterminal, papillate (30×).</text>
      <biological_entity id="o13658" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s19" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s19" value="5" value_original="5" />
        <character is_modifier="false" name="position" src="d0_s19" value="subterminal" value_original="subterminal" />
        <character is_modifier="false" name="relief" src="d0_s19" value="papillate" value_original="papillate" />
        <character name="quantity" src="d0_s19" value="[30" value_original="[30" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Capsules ovoid, opening by (4–) 5 ascending teeth;</text>
      <biological_entity id="o13659" name="capsule" name_original="capsules" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o13660" name="tooth" name_original="teeth" src="d0_s20" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="atypical_quantity" src="d0_s20" to="5" to_inclusive="false" />
        <character is_modifier="true" name="quantity" src="d0_s20" value="5" value_original="5" />
        <character is_modifier="true" name="orientation" src="d0_s20" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o13659" id="r1495" name="opening by" negation="false" src="d0_s20" to="o13660" />
    </statement>
    <statement id="d0_s21">
      <text>carpophore absent.</text>
      <biological_entity id="o13661" name="carpophore" name_original="carpophore" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds ca. 30–60, black, reniform, laterally compressed, tuberculate, marginal wing absent, appendage absent;</text>
      <biological_entity id="o13662" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s22" to="60" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s22" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s22" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="relief" src="d0_s22" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o13663" name="wing" name_original="wing" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13664" name="appendage" name_original="appendage" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>embryo peripheral, curved.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 12.</text>
      <biological_entity id="o13665" name="embryo" name_original="embryo" src="d0_s23" type="structure">
        <character is_modifier="false" name="position" src="d0_s23" value="peripheral" value_original="peripheral" />
        <character is_modifier="false" name="course" src="d0_s23" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="x" id="o13666" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia; introduced worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>37.</number>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Agrostemma brachyloba (Fenzl) Hammer (A. gracilis Boissier), a species native to Greece and Asia Minor, has been reported as a garden waif in Boulder, Colorado, where it persisted for at least four years (W. A. Weber et al. 1979). It differs from A. githago in that its calyx lobes are shorter (rather than longer) than the calyx tube, and its petals, spotted on the limb, are longer (rather than shorter) than the calyx lobes.</discussion>
  <references>
    <reference>Firbank, L. G. 1988. Biological flora of the British Isles. 165. Agrostemma githago L. (Lychnis githago (L.) Scop.). J. Ecol. 76: 1232–1246.</reference>
    <reference>Hammer, K., P. Hanelt, and H. Knupffer. 1982. Vorarbeiten zur monographischen Darstellung von Wildpflanzensortimenten: Agrostemma L. (Studies towards a monographic treatment of wild plant collections: Agrostemma L.) Kulturpflanze 30: 45–96.</reference>
  </references>
  
</bio:treatment>