<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">510</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">509</other_info_on_meta>
    <other_info_on_meta type="mention_page">513</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="Rechinger f." date="1936" rank="species">utahensis</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>40: 298. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species utahensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060805</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous, with vertical rootstock.</text>
      <biological_entity id="o1783" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1784" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o1783" id="r193" name="with" negation="false" src="d0_s0" to="o1784" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, occasionally ascending, usually producing axillary shoots below 1st-order inflorescence or at proximal nodes, 15–40 (–60) cm.</text>
      <biological_entity id="o1785" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o1786" name="shoot" name_original="shoots" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o1787" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
      </biological_entity>
      <relation from="o1785" id="r194" modifier="usually" name="producing" negation="false" src="d0_s1" to="o1786" />
      <relation from="o1785" id="r195" modifier="usually" name="below 1st-order inflorescence or at" negation="false" src="d0_s1" to="o1787" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades lanceolate or linear-lanceolate, 6–15 × 2–3 cm, usually ca. 4–5 times as long as wide, widest near middle or slightly towards base, usually thin, base cuneate, margin entire, flat, rarely indistinctly undulate, apex acute.</text>
      <biological_entity id="o1788" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="4-5" value_original="4-5" />
        <character constraint="near " constraintid="o1790" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o1789" name="middle" name_original="middle" src="d0_s2" type="structure" />
      <biological_entity id="o1790" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o1791" name="middle" name_original="middle" src="d0_s2" type="structure" />
      <biological_entity id="o1792" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="width" notes="" src="d0_s2" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o1793" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o1794" name="margin" name_original="margin" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="rarely indistinctly" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o1795" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o1790" id="r196" name="near" negation="false" src="d0_s2" to="o1791" />
      <relation from="o1790" id="r197" name="near" negation="false" src="d0_s2" to="o1792" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal and axillary, terminal usually occupying distal 1/5–1/3 of stem, dense or occasionally slightly interrupted at base, usually broadly paniculate (branches normally simple and crowded).</text>
      <biological_entity id="o1796" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character constraint="at base" constraintid="o1798" is_modifier="false" modifier="occasionally slightly" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="usually broadly" name="arrangement" notes="" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o1797" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/5" is_modifier="true" name="quantity" src="d0_s3" to="1/3" />
      </biological_entity>
      <biological_entity id="o1798" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o1796" id="r198" name="occupying" negation="false" src="d0_s3" to="o1797" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels articulated in proximal 1/3 or almost near base, filiform (but thickened distally), 4–7 mm, not more than 2–2.5 times as long as inner tepals, articulation indistinctly or evidently swollen.</text>
      <biological_entity id="o1799" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character constraint="in " constraintid="o1801" is_modifier="false" name="architecture" src="d0_s4" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character constraint="tepal" constraintid="o1804" is_modifier="false" name="length" src="d0_s4" value="2+-2.5 times as long as inner tepals" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1800" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o1801" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o1802" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o1803" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="inner" id="o1804" name="tepal" name_original="tepals" src="d0_s4" type="structure" />
      <biological_entity id="o1805" name="articulation" name_original="articulation" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="evidently" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o1801" id="r199" name="in" negation="false" src="d0_s4" to="o1802" />
      <relation from="o1801" id="r200" name="in" negation="false" src="d0_s4" to="o1803" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers 10–25 in whorls;</text>
      <biological_entity id="o1806" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o1807" from="10" name="quantity" src="d0_s5" to="25" />
      </biological_entity>
      <biological_entity id="o1807" name="whorl" name_original="whorls" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>inner tepals deltoid or broadly ovate-deltoid, 2.5–3 × 2.5–3 mm, base truncate or indistinctly cordate, margins entire, apex acute, rarely subacute;</text>
      <biological_entity constraint="inner" id="o1808" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="deltoid" value_original="deltoid" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate-deltoid" value_original="ovate-deltoid" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1809" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o1810" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o1811" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tubercles absent.</text>
      <biological_entity id="o1812" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes dark reddish-brown or almost black, 1.8–2 × 1–1.3 mm. 2n = 40.</text>
      <biological_entity id="o1813" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" modifier="almost" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s8" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1814" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shores of rivers and streams, wet meadows, rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shores" constraint="of rivers and streams" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.; Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Utah willow dock</other_name>
  <discussion>The names Rumex mexicanus and R. salicifolius in the broad sense often have been applied to R. utahensis.</discussion>
  <discussion>Records of “narrow-leaved forms” of Rumex utahensis from Yukon (E. Hultén 1968) probably refer to R. hultenii Tzvelev (see comments under 27. R. sibiricus) or R. sibiricus in the narrow sense.</discussion>
  
</bio:treatment>