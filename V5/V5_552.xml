<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="S. Watson in W. H. Brewer et al." date="1880" rank="species">ochrocephalum</taxon_name>
    <taxon_name authority="(S. Stokes) M. Peck" date="1945" rank="variety">calcareum</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>4: 178. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species ochrocephalum;variety calcareum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060433</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ochrocephalum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="subspecies">calcareum</taxon_name>
    <place_of_publication>
      <publication_title>Eriogonum,</publication_title>
      <place_in_publication>92. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species ochrocephalum;subspecies calcareum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ochrocephalum</taxon_name>
    <taxon_name authority="Reveal" date="unknown" rank="variety">sceptrum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species ochrocephalum;variety sceptrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades lanceolate to elliptic or oblong, (0.6–) 1–3.5 (–4) × 0.5–1.5 cm.</text>
      <biological_entity id="o801" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s0" to="elliptic or oblong" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_length" src="d0_s0" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s0" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scapes (0.6–) 1–4 (–5) dm, glabrous or, rarely, slightly floccose.</text>
      <biological_entity id="o802" name="scape" name_original="scapes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.6" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s1" value="," value_original="," />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres turbinate, 3–5.5 (–6) × (1.5–) 2.5–3 (–4) mm, sparsely floccose or glabrous.</text>
      <biological_entity id="o803" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s2" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 1.5–3 mm;</text>
      <biological_entity id="o804" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>perianth glabrous.</text>
      <biological_entity id="o805" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Achenes 1.5–3 mm.</text>
      <biological_entity id="o806" name="achene" name_original="achenes" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Volcanic, diatomaceous or gumbo flats, washes, and slopes, saltbush and sagebrush communities, juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="volcanic" />
        <character name="habitat" value="diatomaceous" />
        <character name="habitat" value="gumbo flats" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="woodlands" modifier="juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>62b.</number>
  <other_name type="common_name">Harper wild buckwheat</other_name>
  <discussion>Variety calcareum is known from southern Baker and Malheur counties, Oregon, and from Elmore, Owyhee, Payette, Twin Falls, and southern Washington counties, Idaho. It is considered “sensitive” in Idaho.</discussion>
  
</bio:treatment>