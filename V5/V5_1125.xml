<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">552</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="mention_page">550</other_info_on_meta>
    <other_info_on_meta type="mention_page">553</other_info_on_meta>
    <other_info_on_meta type="illustration_page">540</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">polygonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Polygonum</taxon_name>
    <taxon_name authority="T. R. Mertens &amp; P. H. Raven" date="1965" rank="species">marinense</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>18: 87. 1965</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonum;section polygonum;species marinense;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060743</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants often reddish tinged, heterophyllous, subsucculent.</text>
      <biological_entity id="o21612" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish tinged" value_original="reddish tinged" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="heterophyllous" value_original="heterophyllous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to ascending, branching from base, not wiry, 15–40 cm.</text>
      <biological_entity id="o21613" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="ascending" />
        <character constraint="from base" constraintid="o21614" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s1" value="wiry" value_original="wiry" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21614" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: ocrea 4–6 mm, proximal part funnelform, distal part silvery hyaline, soon disintegrating, leaving almost no fibrous remains;</text>
      <biological_entity id="o21615" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21616" name="ocreum" name_original="ocrea" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21617" name="part" name_original="part" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o21618" name="part" name_original="part" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="silvery hyaline" value_original="silvery hyaline" />
        <character is_modifier="false" modifier="soon" name="dehiscence" src="d0_s2" value="disintegrating" value_original="disintegrating" />
        <character is_modifier="false" modifier="almost" name="quantity" src="d0_s2" value="no" value_original="no" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–5 mm;</text>
      <biological_entity id="o21619" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21620" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade often reddish tinged, elliptic to obovate or oblanceolate;</text>
      <biological_entity id="o21621" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21622" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish tinged" value_original="reddish tinged" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="obovate or oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>20–35 × 9–16 mm, margins flat, apex rounded;</text>
      <biological_entity id="o21623" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21624" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o21625" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stem-leaves (1.3–) 2–2.6 (–3.5) times as long as branch leaves;</text>
      <biological_entity id="o21626" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o21627" name="stem-leaf" name_original="stem-leaves" src="d0_s6" type="structure">
        <character constraint="leaf" constraintid="o21628" is_modifier="false" name="length" src="d0_s6" value="(1.3-)2-2.6(-3.5) times as long as branch leaves" />
      </biological_entity>
      <biological_entity constraint="branch" id="o21628" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>distal leaves overtopping flowers in distal part of inflorescence.</text>
      <biological_entity id="o21629" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o21630" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o21631" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o21632" name="part" name_original="part" src="d0_s7" type="structure" />
      <biological_entity id="o21633" name="inflorescence" name_original="inflorescence" src="d0_s7" type="structure" />
      <relation from="o21630" id="r2411" name="overtopping" negation="false" src="d0_s7" to="o21631" />
      <relation from="o21630" id="r2412" name="in" negation="false" src="d0_s7" to="o21632" />
      <relation from="o21632" id="r2413" name="part_of" negation="false" src="d0_s7" to="o21633" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences axillary;</text>
      <biological_entity id="o21634" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>cymes in most leaf-axils, 1–4-flowered.</text>
      <biological_entity id="o21635" name="cyme" name_original="cymes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="1-4-flowered" value_original="1-4-flowered" />
      </biological_entity>
      <biological_entity id="o21636" name="leaf-axil" name_original="leaf-axils" src="d0_s9" type="structure" />
      <relation from="o21635" id="r2414" name="in" negation="false" src="d0_s9" to="o21636" />
    </statement>
    <statement id="d0_s10">
      <text>Pedicels mostly exserted from ocreae, 2–4 mm.</text>
      <biological_entity id="o21637" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21638" name="ocrea" name_original="ocreae" src="d0_s10" type="structure" />
      <relation from="o21637" id="r2415" modifier="mostly" name="exserted from" negation="false" src="d0_s10" to="o21638" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers semi-open;</text>
      <biological_entity id="o21639" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="semi-open" value_original="semi-open" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth 3–3.5 (–4) mm;</text>
      <biological_entity id="o21640" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 18–25% of perianth length;</text>
      <biological_entity id="o21641" name="tube" name_original="tube" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>tepals overlapping, green, margins white or pink, petaloid, not keeled, broadly rounded, cucullate;</text>
      <biological_entity id="o21642" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o21643" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s14" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>midveins unbranched;</text>
      <biological_entity id="o21644" name="midvein" name_original="midveins" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 8.</text>
      <biological_entity id="o21645" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes exserted from perianth, brown, ovate, 3-gonous, 2.8–3.4 (–4) mm, faces subequal or evidently unequal, apex not beaked, edges straight, shiny, minutely roughened;</text>
      <biological_entity id="o21646" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="3.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s17" to="3.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21647" name="perianth" name_original="perianth" src="d0_s17" type="structure" />
      <biological_entity id="o21648" name="face" name_original="faces" src="d0_s17" type="structure">
        <character is_modifier="false" name="size" src="d0_s17" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="evidently" name="size" src="d0_s17" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o21649" name="apex" name_original="apex" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s17" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o21650" name="edge" name_original="edges" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="minutely" name="relief_or_texture" src="d0_s17" value="roughened" value_original="roughened" />
      </biological_entity>
      <relation from="o21646" id="r2416" name="exserted from" negation="false" src="d0_s17" to="o21647" />
    </statement>
    <statement id="d0_s18">
      <text>late-season achenes uncommon, 4.5–5 mm. 2n = 60.</text>
      <biological_entity constraint="late-season" id="o21651" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s18" value="uncommon" value_original="uncommon" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s18" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21652" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal salt and brackish marshes, swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal salt" />
        <character name="habitat" value="brackish marshes" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Marin knotweed</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>The origin and taxonomic affinities of Polygonum marinense are uncertain. T. R. Mertens and P. H. Raven (1965) suggested a relationship with P. oxyspermum C. A. Meyer &amp; Bunge or the Mediterranean P. robertii Loiseleur-Deslongchamps. Polygonum marinense may be confused with P. ramosissimum. It can be distinguished by its subsucculent texture, funnelform ocreae, leaves rounded at the apices, and semi-open flowers. Marin knotweed is known from fewer than 15 locations in Marin, Napa, Solano, and Sonoma counties; it is threatened by coastal development.</discussion>
  
</bio:treatment>