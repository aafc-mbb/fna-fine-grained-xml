<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">587</other_info_on_meta>
    <other_info_on_meta type="mention_page">583</other_info_on_meta>
    <other_info_on_meta type="mention_page">588</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1754" rank="genus">persicaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Persicaria</taxon_name>
    <taxon_name authority="(Walter) Small" date="1903" rank="species">hirsuta</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>379. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus persicaria;section persicaria;species hirsuta;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060698</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">hirsutum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>132. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polygonum;species hirsutum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, 3–9 dm;</text>
      <biological_entity id="o115" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="9" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots also often arising from proximal nodes;</text>
      <biological_entity id="o116" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="from proximal nodes" constraintid="o117" is_modifier="false" modifier="often" name="orientation" src="d0_s1" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o117" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>rhizomes present.</text>
      <biological_entity id="o118" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems decumbent to ascending or erect, branched, without noticeable ribs, brownish-hirsute on internodes.</text>
      <biological_entity id="o119" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s3" to="ascending or erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character constraint="on internodes" constraintid="o121" is_modifier="false" name="pubescence" notes="" src="d0_s3" value="brownish-hirsute" value_original="brownish-hirsute" />
      </biological_entity>
      <biological_entity id="o120" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="noticeable" value_original="noticeable" />
      </biological_entity>
      <biological_entity id="o121" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <relation from="o119" id="r10" name="without" negation="false" src="d0_s3" to="o120" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: ocrea brown to reddish-brown, cylindric, 6–12 mm, chartaceous, base sometimes inflated, margins truncate, eciliate or ciliate with bristles 4–7.5 mm, surface hirsute, not glandular-punctate;</text>
      <biological_entity id="o122" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o123" name="ocreum" name_original="ocrea" src="d0_s4" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s4" to="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o124" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o125" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="eciliate" value_original="eciliate" />
        <character constraint="with bristles" constraintid="o126" is_modifier="false" name="pubescence" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o126" name="bristle" name_original="bristles" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o127" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s4" value="glandular-punctate" value_original="glandular-punctate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.1 (–0.3) cm, hirsute, leaves sometimes sessile;</text>
      <biological_entity id="o128" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o129" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="0.3" to_unit="cm" />
        <character name="some_measurement" src="d0_s5" unit="cm" value="0.1" value_original="0.1" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o130" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade without dark triangular or lunate blotch adaxially, ovate to lanceolate, (2–) 4–8 × (0.5–) 1–2.5 cm, base rounded to cordate, margins strigose to hirsute, apex acute to acuminate, faces sparingly hirsute abaxially and adaxially, midvein usually hirsute abaxially.</text>
      <biological_entity id="o131" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o132" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s6" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_width" src="d0_s6" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o133" name="blotch" name_original="blotch" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="dark" value_original="dark" />
        <character is_modifier="true" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="true" name="shape" src="d0_s6" value="lunate" value_original="lunate" />
      </biological_entity>
      <biological_entity id="o134" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s6" to="cordate" />
      </biological_entity>
      <biological_entity id="o135" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s6" to="hirsute" />
      </biological_entity>
      <biological_entity id="o136" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <biological_entity id="o137" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparingly; abaxially" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o138" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="adaxially; usually; abaxially" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <relation from="o132" id="r11" name="without" negation="false" src="d0_s6" to="o133" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences mostly terminal, erect, interrupted proximally, usually uninterrupted distally, 20–80 × 4–8 mm;</text>
      <biological_entity id="o139" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s7" value="uninterrupted" value_original="uninterrupted" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s7" to="80" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncle 30–60 mm, hirsute or, sometimes, nearly glabrous distally;</text>
      <biological_entity id="o140" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s8" to="60" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
        <character name="pubescence" src="d0_s8" value="," value_original="," />
        <character is_modifier="false" modifier="nearly; distally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ocreolae overlapping distally, usually not overlapping proximally, margins ciliate with bristles 0.4–1.5 (–2) mm.</text>
      <biological_entity id="o141" name="ocreola" name_original="ocreolae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="usually not; proximally" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o142" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character constraint="with bristles" constraintid="o143" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o143" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels ascending, 1–2 mm.</text>
      <biological_entity id="o144" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 1–3 per ocreate fascicle, homostylous;</text>
      <biological_entity id="o145" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per fascicle" constraintid="o146" from="1" name="quantity" src="d0_s11" to="3" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="homostylous" value_original="homostylous" />
      </biological_entity>
      <biological_entity id="o146" name="fascicle" name_original="fascicle" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>perianth white to pink, glabrous, not glandular-punctate, nonaccrescent;</text>
      <biological_entity id="o147" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="pink" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s12" value="glandular-punctate" value_original="glandular-punctate" />
        <character is_modifier="false" name="size" src="d0_s12" value="nonaccrescent" value_original="nonaccrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals 5, connate in proximally 1/3, obovate, 1.5–2 mm, veins not prominent, not anchor-shaped, margins entire, apex obtuse to rounded;</text>
      <biological_entity id="o148" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character modifier="proximally" name="quantity" src="d0_s13" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o149" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="anchor--shaped" value_original="anchor--shaped" />
      </biological_entity>
      <biological_entity id="o150" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o151" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s13" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 5, included;</text>
      <biological_entity id="o152" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers red, elliptic to ovate;</text>
      <biological_entity id="o153" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="red" value_original="red" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s15" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 3, connate proximally.</text>
      <biological_entity id="o154" name="style" name_original="styles" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes included or apex exserted, dark-brown to brownish black, 3-gonous, 2–2.5 × 1.3–1.8 mm, shiny, smooth.</text>
      <biological_entity id="o155" name="achene" name_original="achenes" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 20.</text>
      <biological_entity constraint="included" id="o156" name="apex" name_original="apex" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s17" to="brownish black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s17" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s17" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o157" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, open areas in savannahs, pond margins, ditches, often in shallow water</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="open areas" constraint="in savannahs" />
        <character name="habitat" value="savannahs" />
        <character name="habitat" value="pond margins" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="shallow water" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Hairy smartweed</other_name>
  <discussion>C. B. McDonald (1980) showed that Persicaria hirsuta is closely related to P. setacea and P. hydropiperoides. Hybrids between P. hirsuta and P. setacea have been produced experimentally but appear to be rare in the wild. Although geographically sympatric, the two species generally occupy different habitats. Experimental crosses between P. hirsuta and P. hydropiperoides were unsuccessful (McDonald).</discussion>
  
</bio:treatment>