<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">402</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">rotundifolium</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 21. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species rotundifolium;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060485</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">cernuum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="subspecies">glaucescens</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species cernuum;subspecies glaucescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cernuum</taxon_name>
    <taxon_name authority="(Bentham) S. Stokes" date="unknown" rank="subspecies">rotundifolium</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species cernuum;subspecies rotundifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rotundifolium</taxon_name>
    <taxon_name authority="Goodman" date="unknown" rank="variety">angustius</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species rotundifolium;variety angustius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading, annual, 0.5–4 dm, glabrous and often glaucous, greenish to grayish.</text>
      <biological_entity id="o28297" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s0" to="grayish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o28298" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o28299" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, solid, not fistulose, 0.1–0.5 (–0.7) dm, glabrous.</text>
      <biological_entity id="o28300" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o28301" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.7" to_unit="dm" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s2" to="0.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal;</text>
      <biological_entity id="o28302" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1.5–4 cm, floccose;</text>
      <biological_entity id="o28303" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade cordate to orbiculate, 1–2 (–3) × 1–2.5 (–3) cm, densely white-tomentose abaxially, floccose or subglabrous and greenish adaxially, margins plane.</text>
      <biological_entity id="o28304" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s5" to="orbiculate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s5" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o28305" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymose, open to diffuse, usually flat-topped, 5–35 × 5–35 cm;</text>
      <biological_entity id="o28306" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="density" src="d0_s6" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="flat-topped" value_original="flat-topped" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches glabrous;</text>
      <biological_entity id="o28307" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 1–2.5 × 0.5–2 mm.</text>
      <biological_entity id="o28308" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles erect, straight, stoutish, 0.3–1.5 cm, glabrous.</text>
      <biological_entity id="o28309" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="fragility" src="d0_s9" value="stoutish" value_original="stoutish" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s9" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres turbinate to campanulate, 1–2 × 1.5–2.5 mm, glabrous;</text>
      <biological_entity id="o28310" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s10" to="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, erect, 0.4–0.8 mm.</text>
      <biological_entity id="o28311" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1–2.5 mm;</text>
      <biological_entity id="o28312" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth white to pink with greenish to reddish midribs, becoming rose to red, glabrous;</text>
      <biological_entity id="o28313" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="with midribs" constraintid="o28314" from="white" name="coloration" src="d0_s13" to="pink" />
        <character char_type="range_value" from="rose" modifier="becoming" name="coloration" notes="" src="d0_s13" to="red" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28314" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character char_type="range_value" from="greenish" is_modifier="true" name="coloration" src="d0_s13" to="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals dimorphic, those of outer whorl flabellate, those of inner whorl lanceolate;</text>
      <biological_entity id="o28315" name="tepal" name_original="tepals" src="d0_s14" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flabellate" value_original="flabellate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o28316" name="whorl" name_original="whorl" src="d0_s14" type="structure" />
      <biological_entity constraint="inner" id="o28317" name="whorl" name_original="whorl" src="d0_s14" type="structure" />
      <relation from="o28315" id="r3162" name="part_of" negation="false" src="d0_s14" to="o28316" />
      <relation from="o28315" id="r3163" name="part_of" negation="false" src="d0_s14" to="o28317" />
    </statement>
    <statement id="d0_s15">
      <text>stamens included, 1.2–1.7 mm;</text>
      <biological_entity id="o28318" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o28319" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s16" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes dark-brown, 3-gonous, 1.5–2 mm, glabrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 40.</text>
      <biological_entity id="o28320" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28321" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, mixed grassland, saltbush, creosote bush, and mesquite communities, juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="creosote bush" />
        <character name="habitat" value="mesquite communities" />
        <character name="habitat" value="juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>178.</number>
  <other_name type="common_name">Round-leaf wild buckwheat</other_name>
  <discussion>Eriogonum rotundifolium is the southern counterpart to E. cernuum, being common to abundant and occasionally even weedy. Its overall range, however, is significantly smaller. It occurs in Arizona only in Cochise County, but is found more widely in New Mexico, and is common in the trans-Pecos region of western Texas, with scattered populations in Dimmit, Ector, Foard, and Knox counties outside that region.</discussion>
  <discussion>A sterile Edwin James specimen gathered in 1820 (NY) supposedly was collected near the Rocky Mountains and may be Eriogonum rotundifolium. Also seen at NY is an unattributed, redistributed collection of this species labeled only “Colorado.” Until better documented material from that state is seen, the species is considered not to be a member of the Colorado flora.</discussion>
  <discussion>F. A. Elmore (1943) reported that the round-leaf wild buckwheat was used by the Navajo (Diné) people as an emetic. My own consumption of a few seeds, as a self-experiment, produced no particular urge to vomit. Inasmuch as the treatment was taken after swallowing ants, it is difficult to know whether the ants or the seeds were the emetic. G. M. Hocking (1956) reported that the leaves were used for sore throats and the stems were eaten raw (the latter proving in the same self-experiment not to be particularly tasty, leaving a slightly sour aftertaste). Hocking also reported that the roots were used medicinally but mentioned no specific ailment.</discussion>
  
</bio:treatment>