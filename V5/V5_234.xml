<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">stellaria</taxon_name>
    <taxon_name authority="C. C. Chinnappa" date="1992" rank="species">porsildii</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>17: 29, fig. 1. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus stellaria;species porsildii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060939</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, erect-to-straggling, rarely clumped, never compact and cushion-forming, from slender rhizomes.</text>
      <biological_entity id="o24311" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="straggling" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s0" value="clumped" value_original="clumped" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, diffusely branched, rarely elongate and straggling, branched mainly at base, 4-sided, 9–20 cm, glabrous.</text>
      <biological_entity id="o24312" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="diffusely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="straggling" value_original="straggling" />
        <character constraint="at base" constraintid="o24313" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24313" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile;</text>
      <biological_entity id="o24314" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade green, never glaucous, linear to linear-lanceolate, widest at or near middle, 2.7–3.5 cm × 2–3 mm, not succulent, base cuneate, margins entire, apex gradually acuminate, acute, glabrous with few cilia at base.</text>
      <biological_entity id="o24315" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="never" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-lanceolate" />
        <character constraint="at middle" constraintid="o24316" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character char_type="range_value" from="2.7" from_unit="cm" name="length" notes="" src="d0_s3" to="3.5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o24316" name="middle" name_original="middle" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24317" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o24318" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24319" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character constraint="with cilia" constraintid="o24320" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24320" name="cilium" name_original="cilia" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o24321" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o24320" id="r2689" name="at" negation="false" src="d0_s3" to="o24321" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with flowers solitary, terminal or axillary in distal foliage leaves.</text>
      <biological_entity id="o24322" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="terminal" value_original="terminal" />
        <character constraint="in distal foliage leaves" constraintid="o24324" is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o24323" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="foliage" id="o24324" name="leaf" name_original="leaves" src="d0_s4" type="structure" constraint_original="distal foliage" />
      <relation from="o24322" id="r2690" name="with" negation="false" src="d0_s4" to="o24323" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels erect, 18–50 mm, glabrous.</text>
      <biological_entity id="o24325" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s5" to="50" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 7–10 mm diam.;</text>
      <biological_entity id="o24326" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals 5, midrib prominent, lateral-veins obscure, ovatelanceolate to lanceolate, 4–6 mm, margins narrow, membranous, apex acute, glabrous;</text>
      <biological_entity id="o24327" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o24328" name="midrib" name_original="midrib" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o24329" name="lateral-vein" name_original="lateral-veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24330" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o24331" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 5, 4–6 mm, equaling or slightly longer than sepals, blade apex deeply divided into 2 oblanceolate lobes;</text>
      <biological_entity id="o24332" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
        <character constraint="than sepals" constraintid="o24333" is_modifier="false" name="length_or_size" src="d0_s8" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o24333" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity constraint="blade" id="o24334" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character constraint="into lobes" constraintid="o24335" is_modifier="false" modifier="deeply" name="shape" src="d0_s8" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o24335" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 10;</text>
      <biological_entity id="o24336" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 3, ascending, curled at tip, 2–3 mm.</text>
      <biological_entity id="o24337" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character constraint="at tip" constraintid="o24338" is_modifier="false" name="shape" src="d0_s10" value="curled" value_original="curled" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24338" name="tip" name_original="tip" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules black, oblong, 6–8 mm, slightly longer than sepals, apex obtuse, opening by 6 valves;</text>
      <biological_entity id="o24339" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character constraint="than sepals" constraintid="o24340" is_modifier="false" name="length_or_size" src="d0_s11" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o24340" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o24341" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24342" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
      <relation from="o24341" id="r2691" name="opening by" negation="false" src="d0_s11" to="o24342" />
    </statement>
    <statement id="d0_s12">
      <text>carpophore absent.</text>
      <biological_entity id="o24343" name="carpophore" name_original="carpophore" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds dark-brown, broadly ovate, 0.8–1 mm diam., shallowly tuberculate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 26.</text>
      <biological_entity id="o24344" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="diameter" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="shallowly" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24345" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Willow thickets, open forests and woodlands on slopes of mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="willow thickets" />
        <character name="habitat" value="open forests" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="slopes" constraint="of mountains" />
        <character name="habitat" value="mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2400-3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="2400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Porsild’s starwort</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Stellaria porsildii is closely related to S. longipes and S. longifolia and tends to be intermediate between them, with somewhat larger, solitary flowers. Its leaves tend to be more like those of S. longifolia, but they lack the papillate-scabrid margins, and have a few long cilia at the base. It is postulated that the polyploid S. longipes complex arose through hybridization between S. porsildii and S. longifolia, both of which are diploid (C. C. Chinnappa 1992). The two species can be hybridized but the artificial hybrid is diploid.</discussion>
  <discussion>Stellaria porsildii can be very difficult to distinguish from forms of S. longipes, and a confirmatory chromosome count is desirable, at least for records from new locations. The total absence of minute papillae on the stems and leaf margins distinguishes both species from S. longifolia. The presence of a few long cilia at the base of the leaves is a useful indication of S. porsildii, but such cilia often are present in S. longipes. Confirmatory characters for S. porsildii are the open, erect to straggling habit of the plant (never compact and cushion-forming), and the leaves, which are green (never glaucous), soft (not stiff or coriaceous), always narrowly linear-lanceolate, and tending to be widest near the center of the lamina (not lanceolate and widest at the base).</discussion>
  
</bio:treatment>