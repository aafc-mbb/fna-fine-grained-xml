<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">248</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="species">effusum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 15. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species effusum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060264</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">microthecum</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="unknown" rank="variety">effusum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species microthecum;variety effusum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, spreading, not scapose, (1.5–) 2–5 (–7) × 5–15 dm, grayish to reddish-brown-tomentose to floccose and gray or, rarely, thinly floccose and greenish.</text>
      <biological_entity id="o31837" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="atypical_length" src="d0_s0" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="7" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="15" to_unit="dm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character char_type="range_value" from="reddish-brown-tomentose" name="pubescence" src="d0_s0" to="floccose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray" value_original="gray" />
        <character name="coloration" src="d0_s0" value="," value_original="," />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s0" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to erect, typically without persistent leaf-bases, up to 1/2 height of plant;</text>
      <biological_entity id="o31838" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
      <biological_entity id="o31839" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/2 height of plant" />
      </biological_entity>
      <relation from="o31838" id="r3596" modifier="typically" name="without" negation="false" src="d0_s1" to="o31839" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems absent;</text>
      <biological_entity constraint="caudex" id="o31840" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems erect to spreading, slender, solid, not fistulose, 0.3–0.8 dm, floccose or glabrous.</text>
      <biological_entity id="o31841" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="spreading" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s3" to="0.8" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline, 1 per node;</text>
      <biological_entity id="o31842" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character constraint="per node" constraintid="o31843" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o31843" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.2–0.7 cm, tomentose to floccose;</text>
      <biological_entity id="o31844" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s5" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s5" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblanceolate to oblong or obovate, (1–) 1.5–3 × (0.2–) 0.3–0.7 cm, densely white-tomentose abaxially, white-floccose to glabrate or green and glabrous adaxially, margins plane.</text>
      <biological_entity id="o31845" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="oblong or obovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s6" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s6" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="0.7" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character char_type="range_value" from="white-floccose" name="pubescence" src="d0_s6" to="glabrate" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o31846" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose, 10–30 (–40) × 10–40 cm;</text>
      <biological_entity id="o31847" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s7" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, white-floccose to glabrate or subglabrous;</text>
      <biological_entity id="o31848" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character char_type="range_value" from="white-floccose" name="pubescence" src="d0_s8" to="glabrate or subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, triangular, 0.5–2 (–5) mm.</text>
      <biological_entity id="o31849" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent or mostly erect, slender, 0.3–2.5 cm, floccose.</text>
      <biological_entity id="o31850" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s10" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node, turbinate, 1.5–2.5 (–3) × 1–2 mm, tomentose to floccose;</text>
      <biological_entity id="o31851" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o31852" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s11" to="floccose" />
      </biological_entity>
      <biological_entity id="o31852" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.3–0.6 mm.</text>
      <biological_entity id="o31853" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 2–4 mm;</text>
      <biological_entity id="o31854" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth yellow, glabrous;</text>
      <biological_entity id="o31855" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, essentially monomorphic, elliptic to obovate;</text>
      <biological_entity id="o31856" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" modifier="essentially" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s15" to="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens mostly exserted, 2–4.5 mm;</text>
      <biological_entity id="o31857" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments sparsely pilose proximally.</text>
      <biological_entity id="o31858" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes brown, 2–2.5 mm, glabrous.</text>
      <biological_entity id="o31859" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to rocky slopes and flats, mixed grassland and sagebrush communities, juniper and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to rocky slopes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Mont., Nebr., N.Mex., S.Dak., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Spreading wild buckwheat</other_name>
  <discussion>Eriogonum effusum is rather common on the northern Great Plains and along the eastern slope of the Rocky Mountains in central and eastern Colorado, southeastern Montana, western Nebraska, northern New Mexico, southwestern South Dakota, and southeastern Wyoming. Some specimens from Chaffee County, Colorado, are thinly floccose and greenish (Atwood &amp; Welsh 29689, BRY) and thus similar to E. leptocladon. A collection from Pyramid Lake, Washoe County, Nevada (Frandsen &amp; Brown 182, NESH) is clearly mislabeled. A roadside collection of E. effusum gathered near Little America, Sweetwater County, Wyoming, in 1961 (G. Mason 4025, ASU) was an introduction that has not persisted.</discussion>
  <discussion>The spreading wild buckwheat is occasionally merged with Eriogonum microthecum even though the two are morphologically distinct and their ranges do not overlap. Plants in New Mexico are sometimes difficult to distinguish from the related E. leptocladon var. ramosissimum. The species is the food plant for the Rita dotted-blue butterfly (Euphilotes rita). A hybrid between Eriogonum effusum and E. pauciflorum has been named E. ×nebraskense Rydberg [E. multiceps Nees subsp. nebraskense (Rydberg) S. Stokes; E. pauciflorum Pursh var. nebraskense (Rydberg) Reveal]. The hybrid is known from Weld County, Colorado; Cheyenne and Kimball counties, Nebraska; and Converse and Platte counties, Wyoming.</discussion>
  
</bio:treatment>