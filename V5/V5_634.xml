<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="1836" rank="species">elatum</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>17: 413. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species elatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060265</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect, not scapose, 4–8 (–15) × 1–4 dm, glabrous or villous.</text>
      <biological_entity id="o27762" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="15" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="length" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, without persistent leaf-bases, up to 1/5 height of plant;</text>
      <biological_entity id="o27763" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o27764" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/5 height of plant" />
      </biological_entity>
      <relation from="o27763" id="r3086" name="without" negation="false" src="d0_s1" to="o27764" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems absent;</text>
      <biological_entity constraint="caudex" id="o27765" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems erect, slender to stout, solid or hollow, infrequently fistulose, 1.5–4 (–8) dm, glabrous, tomentose, or villous.</text>
      <biological_entity id="o27766" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" from="slender" name="size" src="d0_s3" to="stout" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="hollow" value_original="hollow" />
        <character is_modifier="false" modifier="infrequently" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="8" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s3" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal;</text>
      <biological_entity id="o27767" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 5–25 cm, villous;</text>
      <biological_entity id="o27768" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="25" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade lanceolate to lanceovate, 4–15 (–25) × 1.5–6 cm, loosely villous and green on both surfaces or infrequently thinly tomentose abaxially and glabrate adaxially, margins plane.</text>
      <biological_entity id="o27769" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="lanceovate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="25" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character constraint="on " constraintid="o27773" is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o27770" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o27771" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o27772" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="infrequently thinly" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="true" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o27773" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o27774" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o27775" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o27776" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o27777" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
      <relation from="o27773" id="r3087" name="on" negation="false" src="d0_s6" to="o27774" />
      <relation from="o27773" id="r3088" modifier="on both surfaces or infrequently thinly tomentose and glabrate , margins" name="on" negation="false" src="d0_s6" to="o27775" />
      <relation from="o27775" id="r3089" name="on" negation="false" src="d0_s6" to="o27776" />
      <relation from="o27775" id="r3090" modifier="on both surfaces or infrequently thinly tomentose and glabrate , margins" name="on" negation="false" src="d0_s6" to="o27777" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose, 15–50 × 10–30 cm;</text>
      <biological_entity id="o27778" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s7" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s7" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches glabrous or villous;</text>
      <biological_entity id="o27779" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, semileaflike, linear, and 5–30 × 2–5 mm proximally, scalelike, triangular, and 1–4 mm distally.</text>
      <biological_entity id="o27780" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="semileaflike" value_original="semileaflike" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" modifier="proximally" name="width" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="distally" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent or slender, erect, 0.5–4 cm, glabrous or tomentose.</text>
      <biological_entity id="o27781" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s10" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node or 2–5 per cluster, turbinate, (2.5–) 3–4 × 2.5–3 mm, glabrous or slightly tomentose;</text>
      <biological_entity id="o27782" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o27783" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character char_type="range_value" constraint="per cluster" from="2" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s11" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o27783" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.4–0.9 mm.</text>
      <biological_entity id="o27784" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 2.5–4 mm, glabrous;</text>
      <biological_entity id="o27785" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white;</text>
      <biological_entity id="o27786" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, monomorphic, obovate;</text>
      <biological_entity id="o27787" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 2.5–4 mm;</text>
      <biological_entity id="o27788" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o27789" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 3.5–4 mm, glabrous.</text>
      <biological_entity id="o27790" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s18" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>94.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Eriogonum elatum is widely distributed but rather scattered throughout its range. The plants are occasionally seen in the garden. Inflorescence branches were chewed or made into an infusion and taken as a physic by some Native American people (D. E. Moerman 1986).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering stems and inflorescence branches gla- brous</description>
      <determination>94a Eriogonum elatum var. elatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering stems and inflorescence branches villous</description>
      <determination>94b Eriogonum elatum var. villosum</determination>
    </key_statement>
  </key>
</bio:treatment>