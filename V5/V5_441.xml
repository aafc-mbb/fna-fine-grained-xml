<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">215</other_info_on_meta>
    <other_info_on_meta type="illustration_page">203</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">agrostemma</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">githago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">githago</taxon_name>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus agrostemma;species githago;variety githago;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060015</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–100 cm, spreading-pilose, villous.</text>
      <biological_entity id="o20820" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="spreading-pilose" value_original="spreading-pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 4–15 cm.</text>
      <biological_entity id="o20821" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="distance" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences of solitary, axillary flowers.</text>
      <biological_entity id="o20822" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity constraint="axillary" id="o20823" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o20822" id="r2329" name="part_of" negation="false" src="d0_s2" to="o20823" />
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 5–20 cm.</text>
      <biological_entity id="o20824" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: calyx 2.5–6.2 cm;</text>
      <biological_entity id="o20825" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o20826" name="calyx" name_original="calyx" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="6.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>tube 1.2–1.7 cm, spreading-pilose;</text>
      <biological_entity id="o20827" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o20828" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s5" to="1.7" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="spreading-pilose" value_original="spreading-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lobes linear to linear-lanceolate, 1.2–4.5 cm;</text>
      <biological_entity id="o20829" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o20830" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="linear-lanceolate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s6" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals purplish red to pink (rarely white), unspotted, 1.5–4 cm, equaling or shorter than calyx lobes.</text>
      <biological_entity id="o20831" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o20832" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="purplish red" name="coloration" src="d0_s7" to="pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="unspotted" value_original="unspotted" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s7" to="4" to_unit="cm" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
        <character constraint="than calyx lobes" constraintid="o20833" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o20833" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Capsules 16–24 mm.</text>
      <biological_entity id="o20834" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s8" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 3–4 mm wide.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 48 (Europe).</text>
      <biological_entity id="o20835" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20836" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, roadsides, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Man., N.B., Ont., P.E.I., Que., Sask.; Ala., Alaska, Ark., Calif., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., Wash., W.Va., Wis., Wyo.; Eurasia; widely introduced worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="widely  worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <discussion>Historical collections are known from British Columbia (1915) and Newfoundland (1890).</discussion>
  
</bio:treatment>