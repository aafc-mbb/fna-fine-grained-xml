<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">33</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="subfamily">Paronychioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">paronychia</taxon_name>
    <taxon_name authority="(Nuttall) Fenzl ex Walpers" date="1842" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Bot. Syst.</publication_title>
      <place_in_publication>1: 262. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily paronychioideae;genus paronychia;species americana;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060669</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Herniaria</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts</publication_title>
      <place_in_publication>5: 291. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Herniaria;species americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paronychia</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="unknown" rank="species">americana</taxon_name>
    <taxon_name authority="(Small) Chaudhri" date="unknown" rank="subspecies">pauciflora</taxon_name>
    <taxon_hierarchy>genus Paronychia;species americana;subspecies pauciflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Siphonychia</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">americana</taxon_name>
    <taxon_hierarchy>genus Siphonychia;species americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Siphonychia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pauciflora</taxon_name>
    <taxon_hierarchy>genus Siphonychia;species pauciflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or biennial;</text>
      <biological_entity id="o8579" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot slender.</text>
      <biological_entity id="o8580" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending to prostrate, branched, 5–60 cm, often retrorsely pubescent on 1 side.</text>
      <biological_entity id="o8581" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character constraint="on side" constraintid="o8582" is_modifier="false" modifier="often retrorsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8582" name="side" name_original="side" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules ovatelanceolate, 1.5–7 mm, apex acuminate, fimbriate;</text>
      <biological_entity id="o8583" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o8584" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8585" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade spatulate to oblanceolate or linear-oblanceolate, 3–20 × 1–4 mm, herbaceous, apex acute to obtuse or rounded, glabrous to scaberulous.</text>
      <biological_entity id="o8586" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o8587" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate or linear-oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o8588" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse or rounded" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="scaberulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cymes terminal, 9–25-flowered, ± compact, forming spheroid glomerules 2–6 mm wide.</text>
      <biological_entity id="o8589" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="9-25-flowered" value_original="9-25-flowered" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_arrangement" src="d0_s5" value="compact" value_original="compact" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8590" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="spheroid" value_original="spheroid" />
      </biological_entity>
      <relation from="o8589" id="r958" name="forming" negation="false" src="d0_s5" to="o8590" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 5-merous, ± short-cylindric, with slightly enlarged hypanthium and calyx widening somewhat distally, 1–1.8 mm, sparsely to moderately pubescent proximally with hooked to straight hairs;</text>
      <biological_entity id="o8591" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-merous" value_original="5-merous" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="short-cylindric" value_original="short-cylindric" />
      </biological_entity>
      <biological_entity id="o8592" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="slightly" name="size" src="d0_s6" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="somewhat distally; distally" name="width" src="d0_s6" value="widening" value_original="widening" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.8" to_unit="mm" />
        <character constraint="with hairs" constraintid="o8594" is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8593" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="slightly" name="size" src="d0_s6" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="somewhat distally; distally" name="width" src="d0_s6" value="widening" value_original="widening" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.8" to_unit="mm" />
        <character constraint="with hairs" constraintid="o8594" is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8594" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="hooked" value_original="hooked" />
        <character is_modifier="true" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals redbrown (sometimes finely striped or mottled), white distally, midrib obscure, obovate, 0.4–0.8 mm, leathery to rigid, margins white, 0.03–0.1 mm wide, thinly herbaceous, apex broadly rounded, hood broadly rounded, awn or mucro usually absent or minute;</text>
      <biological_entity id="o8595" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o8596" name="midrib" name_original="midrib" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s7" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="leathery" name="texture" src="d0_s7" to="rigid" />
      </biological_entity>
      <biological_entity id="o8597" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="0.03" from_unit="mm" name="width" src="d0_s7" to="0.1" to_unit="mm" />
        <character is_modifier="false" modifier="thinly" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o8598" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o8599" name="hood" name_original="hood" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o8600" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s7" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o8601" name="mucro" name_original="mucro" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s7" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminodes narrowly triangular, 0.3–0.4 mm;</text>
      <biological_entity id="o8602" name="staminode" name_original="staminodes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 1, cleft in distal 1/6, 0.6–0.8 mm.</text>
      <biological_entity id="o8603" name="style" name_original="style" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character constraint="in distal 1/6" constraintid="o8604" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8604" name="1/6" name_original="1/6" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Utricles ovoid to ellipsoid, 0.6–0.8 mm, smooth, glabrous.</text>
      <biological_entity id="o8605" name="utricle" name_original="utricles" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="ellipsoid" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dunes, pine/oak woodland, fields, clearings, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dunes" />
        <character name="habitat" value="pine\/oak woodland" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">American nailwort</other_name>
  <discussion>Plants of Paronychia americana with fewer flowers in lax cymes from Florida and Georgia were named subsp. pauciflora, a distinction that we do not feel is worth recognition.</discussion>
  
</bio:treatment>