<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">592</other_info_on_meta>
    <other_info_on_meta type="mention_page">583</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1754" rank="genus">persicaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Persicaria</taxon_name>
    <taxon_name authority="(Bruijn) Kitagawa" date="1937" rank="species">longiseta</taxon_name>
    <place_of_publication>
      <publication_title>Rep. Inst. Sci. Res. Manchoukuo</publication_title>
      <place_in_publication>1: 322. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus persicaria;section persicaria;species longiseta;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242100101</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="Bruijn" date="unknown" rank="species">longisetum</taxon_name>
    <place_of_publication>
      <publication_title>in F. A. W. Miquel, Pl. Jungh.</publication_title>
      <place_in_publication>3: 307. 1854</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polygonum;species longisetum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Persicaria</taxon_name>
    <taxon_name authority="(Blume) Nakai" date="unknown" rank="species">caespitosa</taxon_name>
    <taxon_name authority="(Bruijn) C. F. Reed" date="unknown" rank="variety">longiseta</taxon_name>
    <taxon_hierarchy>genus Persicaria;species caespitosa;variety longiseta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="Blume" date="unknown" rank="species">caespitosum</taxon_name>
    <taxon_name authority="(Bruijn) Steward" date="unknown" rank="variety">longisetum</taxon_name>
    <taxon_hierarchy>genus Polygonum;species caespitosum;variety longisetum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, 3–8 dm;</text>
      <biological_entity id="o31204" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots also often arising from proximal nodes;</text>
      <biological_entity id="o31205" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="from proximal nodes" constraintid="o31206" is_modifier="false" modifier="often" name="orientation" src="d0_s1" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31206" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>rhizomes and stolons absent.</text>
      <biological_entity id="o31207" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o31208" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems decumbent to ascending, branched, without noticeable ribs, glabrous.</text>
      <biological_entity id="o31209" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s3" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o31210" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="noticeable" value_original="noticeable" />
      </biological_entity>
      <relation from="o31209" id="r3532" name="without" negation="false" src="d0_s3" to="o31210" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: ocrea hyaline to brownish, cylindric, 5–12 mm, chartaceous, base sometimes inflated, margins truncate, ciliate with bristles 4–12 mm, surface glabrous or strigose, not glandular-punctate;</text>
      <biological_entity id="o31211" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o31212" name="ocreum" name_original="ocrea" src="d0_s4" type="structure">
        <character char_type="range_value" from="hyaline" name="coloration" src="d0_s4" to="brownish" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o31213" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o31214" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character constraint="with bristles" constraintid="o31215" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o31215" name="bristle" name_original="bristles" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31216" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s4" value="glandular-punctate" value_original="glandular-punctate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.1–0.3 (–0.6) cm, glabrous, leaves sometimes sessile;</text>
      <biological_entity id="o31217" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o31218" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s5" to="0.3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o31219" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade without dark triangular or lunate blotch adaxially, ovatelanceolate to linear-lanceolate, 2–8 × 1–3 cm, base tapering to cuneate, margins antrorsely strigose, apex acute to acuminate, faces glabrous or sparingly strigose along veins abaxially, glabrous or strigose along midvein and margins adaxially, not glandular-punctate.</text>
      <biological_entity id="o31220" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o31221" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" notes="" src="d0_s6" to="linear-lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o31222" name="blotch" name_original="blotch" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="dark" value_original="dark" />
        <character is_modifier="true" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="true" name="shape" src="d0_s6" value="lunate" value_original="lunate" />
      </biological_entity>
      <biological_entity id="o31223" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="tapering" name="shape" src="d0_s6" to="cuneate" />
      </biological_entity>
      <biological_entity id="o31224" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o31225" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <biological_entity id="o31226" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="along veins" constraintid="o31227" is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="along margins" constraintid="o31229" is_modifier="false" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" notes="" src="d0_s6" value="glandular-punctate" value_original="glandular-punctate" />
      </biological_entity>
      <biological_entity id="o31227" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <biological_entity id="o31228" name="midvein" name_original="midvein" src="d0_s6" type="structure" />
      <biological_entity id="o31229" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <relation from="o31221" id="r3533" name="without" negation="false" src="d0_s6" to="o31222" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, sometimes also axillary, erect, uninterrupted, 10–40 (–80) × 3–7 mm;</text>
      <biological_entity id="o31230" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="uninterrupted" value_original="uninterrupted" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="80" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncle 10–50 mm, glabrous;</text>
      <biological_entity id="o31231" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="50" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ocreolae overlapping, margins ciliate with bristles (0.5–) 1–4 (–6) mm.</text>
      <biological_entity id="o31232" name="ocreola" name_original="ocreolae" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o31233" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character constraint="with bristles" constraintid="o31234" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o31234" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels ascending, 1–2 mm.</text>
      <biological_entity id="o31235" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 1–5 per ocreate fascicle, homostylous;</text>
      <biological_entity id="o31236" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per fascicle" constraintid="o31237" from="1" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="homostylous" value_original="homostylous" />
      </biological_entity>
      <biological_entity id="o31237" name="fascicle" name_original="fascicle" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>perianth pinkish green proximally, roseate distally, glabrous, not glandular-punctate, scarcely accrescent;</text>
      <biological_entity id="o31238" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s12" value="pinkish green" value_original="pinkish green" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s12" value="roseate" value_original="roseate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s12" value="glandular-punctate" value_original="glandular-punctate" />
        <character is_modifier="false" modifier="scarcely" name="size" src="d0_s12" value="accrescent" value_original="accrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals 5, connate ca. 1/3 their length, obovate, 2.2–2.8 mm, veins not prominent, not anchor-shaped, margins entire, apex obtuse to rounded;</text>
      <biological_entity id="o31239" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character name="length" src="d0_s13" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31240" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="anchor--shaped" value_original="anchor--shaped" />
      </biological_entity>
      <biological_entity id="o31241" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o31242" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s13" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 5, included;</text>
      <biological_entity id="o31243" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers yellow, elliptic to ovate;</text>
      <biological_entity id="o31244" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s15" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 3, connate proximally.</text>
      <biological_entity id="o31245" name="style" name_original="styles" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes included, dark-brown to black, 3-gonous, 1.6–2.3 × 1.1–1.6 mm, shiny, smooth.</text>
      <biological_entity id="o31246" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="included" value_original="included" />
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s17" to="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s17" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s17" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Floodplain forests and woodlands, shorelines of ponds, moist roadsides, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="floodplain forests" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="shorelines" constraint="of ponds , moist roadsides , waste places" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="moist roadsides" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., N.B., Ont.; Ala., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Pa., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.; e Asia; introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="e Asia" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <other_name type="common_name">Bristly lady’s-thumb</other_name>
  <discussion>Persicaria longiseta is morphologically similar to another Asian species, P. posumbu (Buchanan-Hamilton ex D. Don) H. Gross (= P. caespitosa). Its spread in the United States since its introduction near Philadelphia in 1910 was summarized by A. K. Paterson (2000).</discussion>
  
</bio:treatment>