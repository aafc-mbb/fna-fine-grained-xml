<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">510</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="Rechinger f." date="1936" rank="species">crassus</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>40: 295. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species crassus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060776</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous, with vertical rootstock.</text>
      <biological_entity id="o20417" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o20418" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o20417" id="r2277" name="with" negation="false" src="d0_s0" to="o20418" />
    </statement>
    <statement id="d0_s1">
      <text>Stems procumbent, occasionally ascending, usually producing axillary shoots below 1st-order inflorescence or at proximal nodes, 20–50 (–60) cm.</text>
      <biological_entity id="o20419" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o20420" name="shoot" name_original="shoots" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o20421" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
      </biological_entity>
      <relation from="o20419" id="r2278" modifier="usually" name="producing" negation="false" src="d0_s1" to="o20420" />
      <relation from="o20419" id="r2279" modifier="usually" name="below 1st-order inflorescence or at" negation="false" src="d0_s1" to="o20421" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ovatelanceolate, elliptic-lanceolate, or ovate-elliptic, 3–12 × 1–5 cm, usually not more than 2–3.5 times as long as wide, widest near middle or occasionally in proximal 1/2, thick, coriaceous, base broadly cuneate, occasionally almost truncate or cuneate, margins entire, flat, apex acute.</text>
      <biological_entity id="o20422" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="2+-3.5" value_original="2+-3.5" />
        <character constraint="near " constraintid="o20424" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
        <character is_modifier="false" name="texture" src="d0_s2" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o20423" name="middle" name_original="middle" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o20424" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o20425" name="middle" name_original="middle" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o20426" name="1/2" name_original="1/2" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" notes="" src="d0_s2" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o20427" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="occasionally almost; almost" name="shape" src="d0_s2" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o20428" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o20429" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o20424" id="r2280" name="near" negation="false" src="d0_s2" to="o20425" />
      <relation from="o20424" id="r2281" name="near" negation="false" src="d0_s2" to="o20426" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal and axillary, terminal usually occupying distal 1/3–1/2 of stem, dense or interrupted only near base, usually broadly paniculate (branches simple or nearly so).</text>
      <biological_entity id="o20430" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character constraint="near base" constraintid="o20432" is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="usually broadly" name="arrangement" notes="" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o20431" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s3" to="1/2" />
      </biological_entity>
      <biological_entity id="o20432" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o20430" id="r2282" name="occupying" negation="false" src="d0_s3" to="o20431" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels articulated in proximal 1/3 or almost near base, usually rather thick, 4–9 mm, not more than 2–2.5 times as long as inner tepals, articulation distinctly swollen.</text>
      <biological_entity id="o20433" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character constraint="in " constraintid="o20435" is_modifier="false" name="architecture" src="d0_s4" value="articulated" value_original="articulated" />
        <character is_modifier="false" modifier="usually rather" name="width" notes="" src="d0_s4" value="thick" value_original="thick" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
        <character constraint="tepal" constraintid="o20438" is_modifier="false" name="length" src="d0_s4" value="2+-2.5 times as long as inner tepals" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20434" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o20435" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o20436" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o20437" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="inner" id="o20438" name="tepal" name_original="tepals" src="d0_s4" type="structure" />
      <biological_entity id="o20439" name="articulation" name_original="articulation" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o20435" id="r2283" name="in" negation="false" src="d0_s4" to="o20436" />
      <relation from="o20435" id="r2284" name="in" negation="false" src="d0_s4" to="o20437" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers 10–25 in whorls;</text>
      <biological_entity id="o20440" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o20441" from="10" name="quantity" src="d0_s5" to="25" />
      </biological_entity>
      <biological_entity id="o20441" name="whorl" name_original="whorls" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>inner tepals broadly ovate, ovate, or lingulate-deltoid, (3–) 4–5 × (2.5–) 3–4 mm, base rounded or subtruncate, margins entire or indistinctly erose, apex acute or subacute;</text>
      <biological_entity constraint="inner" id="o20442" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lingulate-deltoid" value_original="lingulate-deltoid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lingulate-deltoid" value_original="lingulate-deltoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s6" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_width" src="d0_s6" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20443" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="subtruncate" value_original="subtruncate" />
      </biological_entity>
      <biological_entity id="o20444" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="indistinctly" name="architecture" src="d0_s6" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o20445" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tubercle 1, large, subequal to inner tepals, or slightly narrower than inner tepals (then free margins of inner tepal distinctly narrower than tubercle), subglabrous to verrucose.</text>
      <biological_entity id="o20446" name="tubercle" name_original="tubercle" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="size" src="d0_s7" value="large" value_original="large" />
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character constraint="than inner tepals" constraintid="o20448" is_modifier="false" name="width" notes="" src="d0_s7" value="slightly narrower" value_original="slightly narrower" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" name="relief" src="d0_s7" value="verrucose" value_original="verrucose" />
      </biological_entity>
      <biological_entity constraint="inner" id="o20447" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <biological_entity constraint="inner" id="o20448" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Achenes brown or dark reddish-brown, 2–2.5 × 1.7–2.1 mm. 2n = 20.</text>
      <biological_entity id="o20449" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s8" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20450" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes, sandy shores and marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
        <character name="habitat" value="sandy shores" />
        <character name="habitat" value="marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Fleshy willow dock</other_name>
  
</bio:treatment>