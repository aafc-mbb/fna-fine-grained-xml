<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">142</other_info_on_meta>
    <other_info_on_meta type="mention_page">141</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sagina</taxon_name>
    <taxon_name authority="(Linnaeus) H. Karsten" date="1882" rank="species">saginoides</taxon_name>
    <place_of_publication>
      <publication_title>Deut. Fl.,</publication_title>
      <place_in_publication>539. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus sagina;species saginoides;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242000714</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spergula</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">saginoides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 441. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spergula;species saginoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sagina</taxon_name>
    <taxon_name authority="C. Presl" date="unknown" rank="species">linnaei</taxon_name>
    <taxon_hierarchy>genus Sagina;species linnaei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sagina</taxon_name>
    <taxon_name authority="(Bunge) Fernald" date="unknown" rank="species">micrantha</taxon_name>
    <taxon_hierarchy>genus Sagina;species micrantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sagina</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">saginoides</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">hesperia</taxon_name>
    <taxon_hierarchy>genus Sagina;species saginoides;variety hesperia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, tufted or becoming cespitose in alpine habitats, glabrous.</text>
      <biological_entity id="o19820" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character constraint="in habitats" constraintid="o19821" is_modifier="false" modifier="becoming" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19821" name="habitat" name_original="habitats" src="d0_s0" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s0" value="alpine" value_original="alpine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or sometimes procumbent, few to many-branched, not filiform.</text>
      <biological_entity id="o19822" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="many-branched" value_original="many-branched" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: axillary fascicles absent;</text>
      <biological_entity id="o19823" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal frequently in primary and secondary rosettes 9–45 mm diam., blade linear, 10–20 mm, not succulent, apex apiculate, rarely aristate, glabrous;</text>
      <biological_entity id="o19824" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o19825" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="primary and secondary" id="o19826" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s3" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19827" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o19828" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="aristate" value_original="aristate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o19825" id="r2212" name="in" negation="false" src="d0_s3" to="o19826" />
    </statement>
    <statement id="d0_s4">
      <text>cauline not conspicuously connate basally, rarely forming inflated cup in cespitose, alpine plants, blade linear, sometimes linear-subulate in cespitose plants, 4–20 (–25) mm, not fleshy, apex apiculate, glabrous.</text>
      <biological_entity id="o19829" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o19830" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not conspicuously; basally" name="fusion" src="d0_s4" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o19831" name="cup" name_original="cup" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o19832" name="plant" name_original="plants" src="d0_s4" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s4" value="cespitose" value_original="cespitose" />
        <character is_modifier="true" name="habitat" src="d0_s4" value="alpine" value_original="alpine" />
      </biological_entity>
      <biological_entity id="o19833" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character constraint="in plants" constraintid="o19834" is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o19834" name="plant" name_original="plants" src="d0_s4" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s4" value="cespitose" value_original="cespitose" />
      </biological_entity>
      <biological_entity id="o19835" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o19830" id="r2213" modifier="rarely" name="forming" negation="false" src="d0_s4" to="o19831" />
      <relation from="o19830" id="r2214" modifier="rarely" name="in" negation="false" src="d0_s4" to="o19832" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels frequently recurved during capsular development, erect in fruit, filiform, glabrous.</text>
      <biological_entity id="o19836" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="frequently" name="development" src="d0_s5" value="recurved" value_original="recurved" />
        <character constraint="in fruit" constraintid="o19837" is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19837" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers axillary or terminal, 5-merous, very rarely some 4-merous;</text>
      <biological_entity id="o19838" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-merous" value_original="5-merous" />
        <character is_modifier="false" modifier="very rarely; rarely" name="architecture" src="d0_s6" value="4-merous" value_original="4-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>calyx base glabrous;</text>
      <biological_entity constraint="calyx" id="o19839" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals elliptic, 2–2.5 mm, hyaline margins white, rarely purple in alpine specimens, apex obtuse to rounded, remaining appressed following capsule dehiscence;</text>
      <biological_entity id="o19840" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19841" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character constraint="in specimens" constraintid="o19842" is_modifier="false" modifier="rarely" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o19842" name="specimen" name_original="specimens" src="d0_s8" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s8" value="alpine" value_original="alpine" />
      </biological_entity>
      <biological_entity id="o19843" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="rounded" />
      </biological_entity>
      <biological_entity id="o19844" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o19843" id="r2215" name="remaining" negation="false" src="d0_s8" to="o19844" />
    </statement>
    <statement id="d0_s9">
      <text>petals elliptic, (1–) 1.5–2 mm, shorter than or equaling sepals;</text>
      <biological_entity id="o19845" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character constraint="than or equaling sepals" constraintid="o19846" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o19846" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens (5 or) 10.</text>
      <biological_entity id="o19847" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 2.5–3 (–3.5) mm, 1.5–2 times sepals, dehiscing to base.</text>
      <biological_entity id="o19848" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character constraint="sepal" constraintid="o19849" is_modifier="false" name="size_or_quantity" src="d0_s11" value="1.5-2 times sepals" value_original="1.5-2 times sepals" />
        <character constraint="to base" constraintid="o19850" is_modifier="false" name="dehiscence" src="d0_s11" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o19849" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o19850" name="base" name_original="base" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds brown, obliquely triangular with distinct abaxial groove, 0.3–0.4 mm, smooth to slightly pebbled.</text>
      <biological_entity constraint="abaxial" id="o19852" name="groove" name_original="groove" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>2n = 22.</text>
      <biological_entity id="o19851" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character constraint="with abaxial groove" constraintid="o19852" is_modifier="false" modifier="obliquely" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s12" to="slightly pebbled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19853" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid-late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane sites, open or light shade, wet places on lake margins, along stream gravels and seepages in rock ledges and roadcuts, subalpine and alpine zones</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane sites" />
        <character name="habitat" value="open" />
        <character name="habitat" value="light shade" />
        <character name="habitat" value="wet places" constraint="on lake margins" />
        <character name="habitat" value="lake margins" />
        <character name="habitat" value="stream gravels" modifier="along" constraint="in rock ledges and roadcuts" />
        <character name="habitat" value="seepages" constraint="in rock ledges and roadcuts" />
        <character name="habitat" value="rock ledges" />
        <character name="habitat" value="roadcuts" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Nfld. and Labr. (Nfld.), N.W.T., Nunavut, Que., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wash., Wyo.; Mexico; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Sagine des alpes</other_name>
  <discussion>Some specimens from alpine habitats in Montana and Alberta are intermediate between Sagina saginoides and the typically arctic S. nivalis.</discussion>
  
</bio:treatment>