<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">345</other_info_on_meta>
    <other_info_on_meta type="mention_page">337</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="Reveal" date="1976" rank="variety">desereticum</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>35: 365. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety desereticum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060546</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading mats, 1–3.5 (–4) × 3–6 dm.</text>
      <biological_entity id="o20473" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o20474" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="4" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, 1–3 dm, thinly floccose or glabrous, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o20475" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20476" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o20475" id="r2285" name="without" negation="false" src="d0_s1" to="o20476" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in loose rosettes;</text>
      <biological_entity id="o20477" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o20478" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o20477" id="r2286" name="in" negation="false" src="d0_s2" to="o20478" />
    </statement>
    <statement id="d0_s3">
      <text>blade usually elliptic, 1–2 (–2.5) × 0.5–1.5 (–2) cm, glabrous on both surfaces at full anthesis, margins plane.</text>
      <biological_entity id="o20479" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character constraint="on surfaces" constraintid="o20480" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20480" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o20481" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="at full anthesis" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences umbellate;</text>
      <biological_entity id="o20482" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 2–4.5 (–5.5) cm, glabrous, without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o20483" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="4.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20484" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o20485" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o20483" id="r2287" name="without" negation="false" src="d0_s5" to="o20484" />
      <relation from="o20484" id="r2288" name="part_of" negation="false" src="d0_s5" to="o20485" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 2–3 mm, lobes 1–2.5 mm.</text>
      <biological_entity id="o20486" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20487" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 4–8 mm;</text>
      <biological_entity id="o20488" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth pale-yellow to cream.</text>
      <biological_entity id="o20489" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s8" to="cream" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly slopes and ridges, mixed grassland and sagebrush communities, oak, aspen, and montane to subalpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="sandy to gravelly slopes" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="aspen" />
        <character name="habitat" value="conifer" />
        <character name="habitat" value="montane to subalpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1500-)1900-3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="1900" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3300" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Nev., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107r.</number>
  <other_name type="common_name">Deseret sulphur flower</other_name>
  <discussion>Variety desereticum is widely scattered in southern Idaho (Bear Lake, Blaine, Custer, and Owyhee counties), southwestern Montana (Park County), and southwestern Wyoming (Carbon, Teton, and Uinta counties) south into northeastern Nevada (Elko County) and northern Utah (Box Elder, Carbon, Daggett, Duchesne, Emery, Juab, Millard, Piute, Rich, Salt Lake, Sanpete, Summit, Tooele, Utah, and Wasatch counties). It is common only in Utah and southeastern Idaho. Variety desereticum is related to var. dichrocephalum, but the two rarely occur together. Late in the season, high-elevation plants can have attractive, bright red leaves. Such plants approach var. porteri in aspect, especially in the Jarbidge, Ruby, and East Humboldt mountains of Nevada. The Deseret sulphur flower is worthy of cultivation.</discussion>
  
</bio:treatment>