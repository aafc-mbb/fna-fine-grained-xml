<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">246</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">microthecum</taxon_name>
    <taxon_name authority="Reveal" date="1971" rank="variety">lapidicola</taxon_name>
    <place_of_publication>
      <publication_title>Sci. Bull. Brigham Young Univ., Biol. Ser.</publication_title>
      <place_in_publication>13(1): 28, fig. 17. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species microthecum;variety lapidicola;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060393</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 0.5–1.5 × 1–3 dm.</text>
      <biological_entity id="o23112" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent or sometimes spreading;</text>
      <biological_entity id="o23113" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o23114" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems 0.2–0.6 dm, tomentose to floccose.</text>
      <biological_entity id="o23115" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o23116" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s2" to="0.6" to_unit="dm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s2" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade elliptic, 0.3–0.7 (–0.8) × 0.1–0.4 cm, densely reddish-brown-tomentose abaxially, tomentose to floccose adaxially, margins not revolute.</text>
      <biological_entity id="o23117" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23118" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s3" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="0.4" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="reddish-brown-tomentose" value_original="reddish-brown-tomentose" />
        <character char_type="range_value" from="tomentose" modifier="adaxially" name="pubescence" src="d0_s3" to="floccose" />
      </biological_entity>
      <biological_entity id="o23119" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 2–6 cm;</text>
      <biological_entity id="o23120" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches tomentose to floccose.</text>
      <biological_entity id="o23121" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s5" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 2.5–3.5 mm, floccose to subglabrous.</text>
      <biological_entity id="o23122" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s6" to="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers (1.5–) 2–3.5 mm;</text>
      <biological_entity id="o23123" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth whitish red to pink, rose, or orange.</text>
      <biological_entity id="o23124" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character char_type="range_value" from="whitish red" name="coloration" src="d0_s8" to="pink rose or orange" />
        <character char_type="range_value" from="whitish red" name="coloration" src="d0_s8" to="pink rose or orange" />
        <character char_type="range_value" from="whitish red" name="coloration" src="d0_s8" to="pink rose or orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes 2.5–3 mm.</text>
      <biological_entity id="o23125" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly to rocky volcanic or sandstone outcrops, sagebrush communities, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly to rocky volcanic" />
        <character name="habitat" value="gravelly to sandstone outcrops" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2600-3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1f.</number>
  <other_name type="common_name">Pahute Mesa wild buckwheat</other_name>
  <discussion>Variety lapidicola is found infrequently in the southern mountains of the Great Basin from Inyo County, California, eastward across central Nevada (Clark, Esmeralda, Lincoln, Nye, and White Pine counties) to western Utah (Juab and Washington counties). It approaches the more depauperate forms of var. simpsonii in habit and is difficult to separate from var. phoeniceum in western Utah. An expression possibly worthy of formal recognition with sparse, whitish tomentum and narrow, greenish adaxial leaf surfaces from White River Valley in central Nye County, Nevada (e.g., Harrison &amp; Harrison 13211, BRY; Holmgren &amp; Holmgren 8219, NY; Reveal 8331, NY; B. T. Welsh &amp; K. H. Thorne 402 and 567, BRY), tentatively here assigned to var. lapidicola, is somewhat similar to var. simpsonii.</discussion>
  
</bio:treatment>