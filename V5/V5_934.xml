<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">450</other_info_on_meta>
    <other_info_on_meta type="mention_page">447</other_info_on_meta>
    <other_info_on_meta type="mention_page">449</other_info_on_meta>
    <other_info_on_meta type="illustration_page">451</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="(Goodman) Reveal &amp; Hardham" date="1989" rank="subgenus">Eriogonella</taxon_name>
    <taxon_name authority="Bentham" date="1836" rank="species">membranacea</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>17: 419, plate 17, fig. 11. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus eriogonella;species membranacea;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060078</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonella</taxon_name>
    <taxon_name authority="(Bentham) Goodman" date="unknown" rank="species">membranacea</taxon_name>
    <taxon_hierarchy>genus Eriogonella;species membranacea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–6 (–10) × 0.5–3 (–5) dm, woolly-floccose.</text>
      <biological_entity id="o10122" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="woolly-floccose" value_original="woolly-floccose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o10123" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.1–0.5 (–0.8) cm;</text>
      <biological_entity id="o10124" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s2" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear to narrowly oblanceolate, (1–) 1.5–5 × 0.1–0.3 cm, thinly to densely floccose adaxially, densely tomentose abaxially.</text>
      <biological_entity id="o10125" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="narrowly oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s3" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="0.3" to_unit="cm" />
        <character is_modifier="false" modifier="thinly to densely; adaxially" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences strict, white to greenish, open;</text>
      <biological_entity id="o10126" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="strict" value_original="strict" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s4" to="greenish" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts usually 2, opposite, rarely in whorls of 3–5, short-petiolate, acerose, similar to proximal leaf-blades only reduced, 0.3–3 cm × 1–3 mm, awns straight, 0.5–1 mm.</text>
      <biological_entity id="o10127" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="of 3-5" name="architecture" notes="" src="d0_s5" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acerose" value_original="acerose" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" notes="" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10128" name="whorl" name_original="whorls" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o10129" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="only" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o10130" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o10127" id="r1134" modifier="rarely" name="in" negation="false" src="d0_s5" to="o10128" />
      <relation from="o10127" id="r1135" name="to" negation="false" src="d0_s5" to="o10129" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres usually congested in small terminal clusters of 1–3 at node of dichotomies, urceolate, ventricose basally, 3-angled, 6-ribbed, 3–4 mm, not corrugate, with conspicuous, white margins extending across sinuses, tomentose to floccose or glabrate with age, greenish to brownish;</text>
      <biological_entity id="o10131" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character constraint="in terminal clusters" constraintid="o10132" is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s6" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="6-ribbed" value_original="6-ribbed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="arrangement_or_relief" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character char_type="range_value" constraint="with age" constraintid="o10137" from="tomentose" name="pubescence" src="d0_s6" to="floccose or glabrate" />
        <character char_type="range_value" from="greenish" name="coloration" notes="" src="d0_s6" to="brownish" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o10132" name="cluster" name_original="clusters" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="small" value_original="small" />
        <character char_type="range_value" constraint="at node" constraintid="o10133" from="1" modifier="of" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o10133" name="node" name_original="node" src="d0_s6" type="structure" />
      <biological_entity id="o10134" name="dichotomy" name_original="dichotomies" src="d0_s6" type="structure" />
      <biological_entity id="o10135" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o10136" name="sinuse" name_original="sinuses" src="d0_s6" type="structure" />
      <biological_entity id="o10137" name="age" name_original="age" src="d0_s6" type="structure" />
      <relation from="o10133" id="r1136" name="part_of" negation="false" src="d0_s6" to="o10134" />
      <relation from="o10131" id="r1137" name="with" negation="false" src="d0_s6" to="o10135" />
      <relation from="o10135" id="r1138" name="extending across" negation="false" src="d0_s6" to="o10136" />
    </statement>
    <statement id="d0_s7">
      <text>teeth 6;</text>
      <biological_entity id="o10138" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>awns uncinate, 0.7–1.5 mm.</text>
      <biological_entity id="o10139" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="uncinate" value_original="uncinate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 1 (–2), slightly exserted;</text>
      <biological_entity id="o10140" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="2" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth white to rose, subcylindric, (1.5–) 2.5–3 mm, densely pubescent abaxially;</text>
      <biological_entity id="o10141" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="rose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals connate 2/3 their length, slightly dimorphic, entire and rounded apically, those of outer whorl obovate, those of inner whorl spatulate;</text>
      <biological_entity id="o10142" name="tepal" name_original="tepals" src="d0_s11" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="2/3" value_original="2/3" />
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o10143" name="whorl" name_original="whorl" src="d0_s11" type="structure" />
      <biological_entity constraint="inner" id="o10144" name="whorl" name_original="whorl" src="d0_s11" type="structure" />
      <relation from="o10142" id="r1139" name="part_of" negation="false" src="d0_s11" to="o10143" />
      <relation from="o10142" id="r1140" name="part_of" negation="false" src="d0_s11" to="o10144" />
    </statement>
    <statement id="d0_s12">
      <text>stamens slightly exserted;</text>
      <biological_entity id="o10145" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 1.5–2.5 mm, glabrous;</text>
      <biological_entity id="o10146" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers pink to red, oval, 0.2–0.3 mm.</text>
      <biological_entity id="o10147" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="red" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oval" value_original="oval" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s14" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 2.5–3 mm. 2n = 38, 40, (42), 80, 82, 84.</text>
      <biological_entity id="o10148" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10149" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="38" value_original="38" />
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
        <character name="atypical_quantity" src="d0_s15" value="42" value_original="42" />
        <character name="quantity" src="d0_s15" value="80" value_original="80" />
        <character name="quantity" src="d0_s15" value="82" value_original="82" />
        <character name="quantity" src="d0_s15" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly or rocky flats and slopes, mixed grassland and chaparral communities, oak-pine woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly" />
        <character name="habitat" value="sandy to rocky flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="oak-pine woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40-1400(-1600) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="40" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1600" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Pink spineflower</other_name>
  <discussion>Chorizanthe membranacea has long been considered an isolated element among the spineflowers. The strict, upright habit, numerous basal and cauline leaves, and broad, continuous, membranous margins of the involucre all reflect that isolation. Pink spineflower is widespread and often locally common in the Coast Ranges of southwestern Oregon and California and on the western foothills of the Sierra Nevada southward to the Transverse Ranges and the Tehachapi Mountains of Ventura and Kern counties, California.</discussion>
  
</bio:treatment>