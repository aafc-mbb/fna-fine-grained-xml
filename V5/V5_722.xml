<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">siskiyouense</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 44. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species siskiyouense;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060493</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">ursinum</taxon_name>
    <taxon_name authority="(Small) S. Stokes" date="unknown" rank="variety">siskiyouense</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species ursinum;variety siskiyouense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading, matted, 0.5–2 × 1–5 dm, glabrate or glabrous.</text>
      <biological_entity id="o28682" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="2" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex spreading;</text>
      <biological_entity id="o28683" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o28684" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, slender, solid, not fistulose, arising at nodes of caudex branches and at distal nodes of short, nonflowering aerial branches, 0.5–1.5 (–2) dm, usually glabrous, with a whorl of 2–4 leaflike bracts ca. midlength, similar to leaf-blade, 0.3–0.5 × 0.1–0.2 cm.</text>
      <biological_entity id="o28685" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o28686" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character constraint="at nodes of caudex branches and at distal nodes" constraintid="o28687" is_modifier="false" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s2" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s2" to="1.5" to_unit="dm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" notes="" src="d0_s2" to="0.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" notes="" src="d0_s2" to="0.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28687" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity constraint="nonflowering" id="o28688" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
      </biological_entity>
      <biological_entity id="o28689" name="whorl" name_original="whorl" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o28690" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="4" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o28691" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure" />
      <relation from="o28687" id="r3200" name="part_of" negation="false" src="d0_s2" to="o28688" />
      <relation from="o28686" id="r3201" name="with" negation="false" src="d0_s2" to="o28689" />
      <relation from="o28689" id="r3202" name="part_of" negation="false" src="d0_s2" to="o28690" />
      <relation from="o28686" id="r3203" name="to" negation="false" src="d0_s2" to="o28691" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in dense compact basal rosettes;</text>
      <biological_entity id="o28692" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o28693" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="true" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s3" value="compact" value_original="compact" />
      </biological_entity>
      <relation from="o28692" id="r3204" name="in" negation="false" src="d0_s3" to="o28693" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.2–0.6 cm, glabrate or glabrous;</text>
      <biological_entity id="o28694" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s4" to="0.6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade spatulate to round, (0.3–) 0.5–0.8 × (0.2–) 0.3–0.5 (–0.7) cm, densely white to thinly tomentose abaxially, sparsely floccose to glabrate and green or olive green adaxially, rarely glabrous on both surfaces, margins entire, plane.</text>
      <biological_entity id="o28695" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="round" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_length" src="d0_s5" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s5" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s5" to="0.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" modifier="thinly; abaxially" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character char_type="range_value" from="sparsely floccose" name="pubescence" src="d0_s5" to="glabrate" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="olive green" value_original="olive green" />
        <character constraint="on surfaces" constraintid="o28696" is_modifier="false" modifier="rarely" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28696" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o28697" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences capitate, rarely umbellate, 0.8–1.5 cm wide or 1–2 × 1–3 cm;</text>
      <biological_entity id="o28698" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s6" value="umbellate" value_original="umbellate" />
        <character name="width" src="d0_s6" value="1-2×1-3 cm" value_original="1-2×1-3 cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches usually glabrate;</text>
      <biological_entity id="o28699" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts absent immediately below involucre.</text>
      <biological_entity id="o28700" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character constraint="immediately below involucre" constraintid="o28701" is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o28701" name="involucre" name_original="involucre" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres 1 per node, turbinate-campanulate to campanulate, (3–) 3.5–4 × 4–6 mm, arachnoid-tomentose;</text>
      <biological_entity id="o28702" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character constraint="per node" constraintid="o28703" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character char_type="range_value" from="turbinate-campanulate" name="shape" notes="" src="d0_s9" to="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s9" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
      </biological_entity>
      <biological_entity id="o28703" name="node" name_original="node" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>teeth 6–10, lobelike, reflexed, (2–) 2.5–3.5 mm.</text>
      <biological_entity id="o28704" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="10" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="lobelike" value_original="lobelike" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers (4–) 4.5–6 mm, including 0.6–1 mm stipelike base;</text>
      <biological_entity id="o28705" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28706" name="base" name_original="base" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="true" name="shape" src="d0_s11" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <relation from="o28705" id="r3205" name="including" negation="false" src="d0_s11" to="o28706" />
    </statement>
    <statement id="d0_s12">
      <text>perianth sulphur yellow, glabrous;</text>
      <biological_entity id="o28707" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="sulphur yellow" value_original="sulphur yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals monomorphic, oblong;</text>
      <biological_entity id="o28708" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens exserted, 3.5–5 mm;</text>
      <biological_entity id="o28709" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o28710" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes light-brown, 4.5–5 mm, glabrous.</text>
      <biological_entity id="o28711" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly serpentine slopes and outcrops, manzanita communities, montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly serpentine slopes" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="manzanita communities" />
        <character name="habitat" value="conifer woodlands" modifier="montane" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>110.</number>
  <other_name type="past_name">siskiyouensis</other_name>
  <other_name type="common_name">Siskiyou wild buckwheat</other_name>
  <discussion>Eriogonum siskiyouense is restricted to the ridge system that extends from the Scott Mountain area to the Mt. Eddy region of Siskiyou and Trinity counties. The vast majority of individuals in a population have a single involucre atop each flowering stem, but at lower elevations and in somewhat more protected sites the inflorescence may be umbellate. The Siskiyou wild buckwheat does well in cultivation if its soil requirements are fulfilled.</discussion>
  
</bio:treatment>