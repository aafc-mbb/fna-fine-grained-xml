<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">604</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">plumbaginaceae</taxon_name>
    <taxon_name authority="Willdenow" date="1809" rank="genus">armeria</taxon_name>
    <taxon_name authority="(Miller) Willdenow" date="1809" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>1: 333. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plumbaginaceae;genus armeria;species maritima</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250033128</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Statice</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Statice no. 3. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Statice;species maritima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rootstock erect.</text>
      <biological_entity id="o15676" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 1–15 cm × 0.5–3 mm, base 1-veined or ± 3-veined, faces glabrous or hairy.</text>
      <biological_entity id="o15677" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15678" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o15679" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Scapes erect, 2–60 cm, glabrous or hairy.</text>
      <biological_entity id="o15680" name="scape" name_original="scapes" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: involucral sheath 5–32 mm;</text>
      <biological_entity id="o15681" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o15682" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s3" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="32" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>outermost involucral-bract ovate to triangular-lanceolate, 4–14 mm, shorter than, equaling, or exceeding head, mucronate or not;</text>
      <biological_entity id="o15683" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="outermost" id="o15684" name="involucral-bract" name_original="involucral-bract" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="triangular-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="14" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s4" value="equaling" value_original="equaling" />
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
        <character name="shape" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o15685" name="head" name_original="head" src="d0_s4" type="structure" />
      <relation from="o15684" id="r1735" name="exceeding" negation="false" src="d0_s4" to="o15685" />
    </statement>
    <statement id="d0_s5">
      <text>heads 13–28 mm diam.</text>
      <biological_entity id="o15686" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o15687" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="diameter" src="d0_s5" to="28" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers monomorphic, with all stigmas papillate and pollen reticulate, or dimorphic, with papillate stigmas and finely reticulate pollen or smooth stigmas and coarsely reticulate pollen;</text>
      <biological_entity id="o15688" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity id="o15689" name="stigma" name_original="stigmas" src="d0_s6" type="structure">
        <character is_modifier="false" name="relief" src="d0_s6" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o15690" name="pollen" name_original="pollen" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s6" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="growth_form" src="d0_s6" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o15691" name="stigma" name_original="stigmas" src="d0_s6" type="structure">
        <character is_modifier="true" name="relief" src="d0_s6" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o15693" name="stigma" name_original="stigmas" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="finely" name="architecture_or_coloration_or_relief" src="d0_s6" value="reticulate" value_original="reticulate" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o15694" name="pollen" name_original="pollen" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="finely" name="architecture_or_coloration_or_relief" src="d0_s6" value="reticulate" value_original="reticulate" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="true" modifier="coarsely" name="architecture_or_coloration_or_relief" src="d0_s6" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <relation from="o15688" id="r1736" name="with" negation="false" src="d0_s6" to="o15689" />
      <relation from="o15690" id="r1737" name="with" negation="false" src="d0_s6" to="o15691" />
    </statement>
    <statement id="d0_s7">
      <text>calyx-tube hairy on and between ribs (holotrichous), on ribs only (pleurotrichous), or glabrous (atrichous);</text>
      <biological_entity id="o15695" name="calyx-tube" name_original="calyx-tube" src="d0_s7" type="structure">
        <character constraint="on ribs" constraintid="o15696" is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15696" name="rib" name_original="ribs" src="d0_s7" type="structure" />
      <biological_entity id="o15697" name="rib" name_original="ribs" src="d0_s7" type="structure" />
      <relation from="o15695" id="r1738" name="on" negation="false" src="d0_s7" to="o15697" />
    </statement>
    <statement id="d0_s8">
      <text>teeth triangular to shallowly triangular, awned or not;</text>
      <biological_entity id="o15698" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s8" to="shallowly triangular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="awned" value_original="awned" />
        <character name="architecture_or_shape" src="d0_s8" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla pink to white;</text>
      <biological_entity id="o15699" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals showy and exceeding calyx or reduced and included in calyx.</text>
      <biological_entity id="o15701" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o15702" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <relation from="o15700" id="r1739" name="exceeding" negation="false" src="d0_s10" to="o15701" />
      <relation from="o15700" id="r1740" name="included in" negation="false" src="d0_s10" to="o15702" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 18.</text>
      <biological_entity id="o15700" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="showy" value_original="showy" />
        <character is_modifier="false" name="size" src="d0_s10" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15703" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Man., N.W.T., Nunavut, Ont., Sask., Yukon; Alaska, Calif., Colo., Oreg., Wash.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Subspecies 10 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers dimorphic, papillate stigmas associated with finely reticulate pollen and smooth stigmas associated with coarsely reticulate pollen</description>
      <determination>1a Armeria maritima subsp. maritima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers monomorphic, stigmas all papillate with coarsely reticulate pollen</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescence sheath lengths usually 0.75 times diam. of flower heads; outer involucral bracts almost equaling or exceeding flower head; scapes glabrous; calyces hairy on ribs</description>
      <determination>1b Armeria maritima subsp. californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescence sheath lengths usually 0.5-0.75 times diam. of flower heads; outer involucral bracts 0.5-0.75 times flower head; scapes glabrous or hairy; calyces hairy throughout, on ribs only, or glabrous</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Calyces hairy throughout or on ribs only; scapes glabrous or hairy; leaf blades hairy or glabrous</description>
      <determination>1c Armeria maritima subsp. sibirica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Calyces glabrous; scapes glabrous; leaf blades glabrous</description>
      <determination>1d Armeria maritima subsp. interior</determination>
    </key_statement>
  </key>
</bio:treatment>