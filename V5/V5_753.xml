<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">368</other_info_on_meta>
    <other_info_on_meta type="mention_page">369</other_info_on_meta>
    <other_info_on_meta type="illustration_page">366</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">jamesii</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 7. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species jamesii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250060342</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, compact or spreading, matted, 0.5–2.5 × 3–15 dm, tomentose to floccose.</text>
      <biological_entity id="o18570" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity id="o18572" name="whole-organism" name_original="" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s0" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent or spreading;</text>
      <biological_entity id="o18573" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o18574" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, slender, solid, not fistulose, usually arising directly from a taproot, 0.5–1.5 dm, tomentose to floccose.</text>
      <biological_entity id="o18575" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o18576" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character constraint="from taproot" constraintid="o18577" is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s2" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s2" to="floccose" />
      </biological_entity>
      <biological_entity id="o18577" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, typically not in rosettes;</text>
      <biological_entity id="o18578" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o18579" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <relation from="o18578" id="r2089" modifier="typically not" name="in" negation="true" src="d0_s3" to="o18579" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–6 cm, tomentose to floccose;</text>
      <biological_entity id="o18580" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s4" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually narrowly elliptic, 1–3 (–3.5) × (0.3–) 0.5–1 (–1.2) cm, densely tomentose abaxially, thinly tomentose, floccose or glabrous and grayish to greenish adaxially, margins entire, plane or undulate and crisped.</text>
      <biological_entity id="o18581" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_width" src="d0_s5" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="grayish" modifier="adaxially" name="coloration" src="d0_s5" to="greenish" />
      </biological_entity>
      <biological_entity id="o18582" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crisped" value_original="crisped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences umbellate or compound-umbellate, 10–30 × 10–25 cm;</text>
      <biological_entity id="o18583" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="compound-umbellate" value_original="compound-umbellate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s6" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches tomentose to floccose;</text>
      <biological_entity id="o18584" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s7" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3–9, semileaflike at proximal node, 0.5–2 × 0.2–1 cm, often scalelike distally.</text>
      <biological_entity id="o18585" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="9" />
        <character constraint="at proximal node" constraintid="o18586" is_modifier="false" name="architecture_or_shape" src="d0_s8" value="semileaflike" value_original="semileaflike" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" notes="" src="d0_s8" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" notes="" src="d0_s8" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="often; distally" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18586" name="node" name_original="node" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres 1 per node, turbinate, 1.5–7 × 2–5 mm, tomentose to floccose;</text>
      <biological_entity id="o18587" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character constraint="per node" constraintid="o18588" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s9" to="floccose" />
      </biological_entity>
      <biological_entity id="o18588" name="node" name_original="node" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>teeth 5–8, erect, 0.1–0.5 mm.</text>
      <biological_entity id="o18589" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="8" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 3–8 mm, including 0.7–2 mm stipelike base;</text>
      <biological_entity id="o18590" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18591" name="base" name_original="base" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="true" name="shape" src="d0_s11" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <relation from="o18590" id="r2090" name="including" negation="false" src="d0_s11" to="o18591" />
    </statement>
    <statement id="d0_s12">
      <text>perianth white to cream, densely pubescent abaxially;</text>
      <biological_entity id="o18592" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="cream" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals dimorphic, those of outer whorl lanceolate to elliptic, 2–5 × 1–3 mm, those of inner whorl lanceolate to fan-shaped, 1.5–6 × 2–4 mm;</text>
      <biological_entity id="o18593" name="tepal" name_original="tepals" src="d0_s13" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="fan-shaped" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18594" name="whorl" name_original="whorl" src="d0_s13" type="structure" />
      <biological_entity constraint="inner" id="o18595" name="whorl" name_original="whorl" src="d0_s13" type="structure" />
      <relation from="o18593" id="r2091" name="part_of" negation="false" src="d0_s13" to="o18594" />
      <relation from="o18593" id="r2092" name="part_of" negation="false" src="d0_s13" to="o18595" />
    </statement>
    <statement id="d0_s14">
      <text>stamens exserted, 2–4 mm;</text>
      <biological_entity id="o18596" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o18597" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes light-brown to brown, 4–5 mm, glabrous except for sparsely pubescent beak.</text>
      <biological_entity id="o18598" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s16" to="brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18599" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Kans., N.Mex., Okla., Tex.; including Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="including Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>128.</number>
  <other_name type="common_name">Antelope sage</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Eriogonum jamesii is a nectar source for the rare Spalding dotted-blue butterfly (Euphilotes spaldingi).</discussion>
  <discussion>Eriogonum jamesii and E. arcuatum (see below) are considered “life medicines” and used ceremonially by Native Americans (C. Arnold, pers. comm.; A. B. Reagan 1929; P. A. Vestal 1952).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf margins undulate, frequently crisped; flowers 3-5(-6) mm; se Arizona, s New Mexico, sw Texas</description>
      <determination>128c Eriogonum jamesii var. undulatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf margins plane, not crisped; flowers 4-8 mm; e Arizona, c and s Colorado, wc Kansas, New Mexico, w Oklahoma, n and w Texas</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences compound-umbellate; e Arizona, c and s Colorado, New Mexico, w Oklahoma, n and w Texas</description>
      <determination>128a Eriogonum jamesii var. jamesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences simple-umbellate; wc Kansas</description>
      <determination>128b Eriogonum jamesii var. simplex</determination>
    </key_statement>
  </key>
</bio:treatment>