<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard K. Rabeler,Ronald L. Hartman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Ehrhart" date="unknown" rank="genus">MOENCHIA</taxon_name>
    <place_of_publication>
      <publication_title>Neues Mag. Aerzte</publication_title>
      <place_in_publication>5: 203. 1783</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus moenchia;</taxon_hierarchy>
    <other_info_on_name type="etymology">for Conrad Moench, 1744–1805, professor at Marburg, Germany</other_info_on_name>
    <other_info_on_name type="fna_id">120904</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o1384" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots slender.</text>
      <biological_entity id="o1385" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, simple or sometimes branched proximally, terete.</text>
      <biological_entity id="o1386" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sometimes; proximally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: cauline leaves connate proximally, sessile or sometimes petiolate (basal leaves);</text>
      <biological_entity id="o1387" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o1388" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 1-veined, linear to linear-oblanceolate, not succulent, apex acute.</text>
      <biological_entity id="o1389" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1390" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-oblanceolate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o1391" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, 1–3-flowered, spreading cymes or flowers solitary;</text>
      <biological_entity id="o1392" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
      <biological_entity id="o1393" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o1394" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts paired, foliaceous, those of axillary flowers with scarious margins.</text>
      <biological_entity id="o1395" name="bract" name_original="bracts" src="d0_s6" type="structure" constraint="flower" constraint_original="flower; flower">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="paired" value_original="paired" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o1396" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1397" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o1395" id="r147" name="part_of" negation="false" src="d0_s6" to="o1396" />
      <relation from="o1395" id="r148" name="with" negation="false" src="d0_s6" to="o1397" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels erect.</text>
      <biological_entity id="o1398" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth and androecium hypogynous;</text>
      <biological_entity id="o1399" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1400" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o1401" name="androecium" name_original="androecium" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 4, distinct, green, lanceolate, 3.8–7 mm, herbaceous, margins white or silvery, scarious, apex acute;</text>
      <biological_entity id="o1402" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1403" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s9" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o1404" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="silvery" value_original="silvery" />
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o1405" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 4 or rarely absent, white, claw absent, blade apex entire;</text>
      <biological_entity id="o1406" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1407" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o1408" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="blade" id="o1409" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>nectaries at base of filaments opposite sepals;</text>
      <biological_entity id="o1410" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1411" name="nectary" name_original="nectaries" src="d0_s11" type="structure" />
      <biological_entity id="o1412" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o1413" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o1414" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o1411" id="r149" name="at" negation="false" src="d0_s11" to="o1412" />
      <relation from="o1412" id="r150" name="part_of" negation="false" src="d0_s11" to="o1413" />
      <relation from="o1412" id="r151" name="part_of" negation="false" src="d0_s11" to="o1414" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, inserted at base of ovary;</text>
      <biological_entity id="o1415" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1416" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o1417" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o1418" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <relation from="o1416" id="r152" name="inserted at" negation="false" src="d0_s12" to="o1417" />
      <relation from="o1416" id="r153" name="part_of" negation="false" src="d0_s12" to="o1418" />
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct nearly to base;</text>
      <biological_entity id="o1419" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1420" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character constraint="to base" constraintid="o1421" is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o1421" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>staminodes absent;</text>
      <biological_entity id="o1422" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o1423" name="staminode" name_original="staminodes" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 4, filiform, 0.7–1 mm, glabrous proximally;</text>
      <biological_entity id="o1424" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o1425" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s15" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigmas 4, linear along adaxial surface of styles, minutely papillate (50×).</text>
      <biological_entity id="o1426" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o1427" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="4" value_original="4" />
        <character constraint="along adaxial surface" constraintid="o1428" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="minutely" name="relief" notes="" src="d0_s16" value="papillate" value_original="papillate" />
        <character name="quantity" src="d0_s16" value="[50" value_original="[50" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1428" name="surface" name_original="surface" src="d0_s16" type="structure" />
      <biological_entity id="o1429" name="style" name_original="styles" src="d0_s16" type="structure" />
      <relation from="o1428" id="r154" name="part_of" negation="false" src="d0_s16" to="o1429" />
    </statement>
    <statement id="d0_s17">
      <text>Capsules cylindric, opening by 8 revolute teeth, shorter than or equaling sepals;</text>
      <biological_entity id="o1430" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="cylindric" value_original="cylindric" />
        <character constraint="than or equaling sepals" constraintid="o1432" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o1431" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="8" value_original="8" />
        <character is_modifier="true" name="shape_or_vernation" src="d0_s17" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o1432" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="true" name="variability" src="d0_s17" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o1430" id="r155" name="opening by" negation="false" src="d0_s17" to="o1431" />
    </statement>
    <statement id="d0_s18">
      <text>carpophore absent.</text>
      <biological_entity id="o1433" name="carpophore" name_original="carpophore" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 35–55, brown, reniform with deep abaxial groove, laterally compressed, papillate, marginal wing absent, appendage absent.</text>
      <biological_entity id="o1434" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="35" name="quantity" src="d0_s19" to="55" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="brown" value_original="brown" />
        <character constraint="with abaxial groove" constraintid="o1435" is_modifier="false" name="shape" src="d0_s19" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="laterally" name="shape" notes="" src="d0_s19" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="relief" src="d0_s19" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1435" name="groove" name_original="groove" src="d0_s19" type="structure">
        <character is_modifier="true" name="depth" src="d0_s19" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o1436" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>x = 19.</text>
      <biological_entity id="o1437" name="appendage" name_original="appendage" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o1438" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe (Mediterranean region); introduced in Africa (Republic of South Africa), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe (Mediterranean region)" establishment_means="introduced" />
        <character name="distribution" value="in Africa (Republic of South Africa)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">Upright chickweed</other_name>
  <discussion>Species 3 (1 in the flora).</discussion>
  <references>
    <reference>Ketzner, D. M. 1996. Crepis pulchra (Asteraceae) and Moenchia erecta (Caryophyllaceae) in Illinois. Trans. Illinois State Acad. Sci. 89: 21–23.</reference>
    <reference>Rabeler, R. K. 1991. Moenchia erecta (Caryophyllaceae) in eastern North America. Castanea 56: 150–151.</reference>
  </references>
  
</bio:treatment>