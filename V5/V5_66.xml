<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">37</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="illustration_page">35</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="subfamily">Paronychioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">paronychia</taxon_name>
    <taxon_name authority="(Chapman) Shinners" date="1962" rank="species">erecta</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>1: 102. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily paronychioideae;genus paronychia;species erecta;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060679</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Siphonychia</taxon_name>
    <taxon_name authority="Chapman" date="unknown" rank="species">erecta</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.,</publication_title>
      <place_in_publication>47. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Siphonychia;species erecta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Odontonychia</taxon_name>
    <taxon_name authority="(Small) Small" date="unknown" rank="species">corymbosa</taxon_name>
    <taxon_hierarchy>genus Odontonychia;species corymbosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Odontonychia</taxon_name>
    <taxon_name authority="(Chapman) Small" date="unknown" rank="species">erecta</taxon_name>
    <taxon_hierarchy>genus Odontonychia;species erecta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paronychia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">erecta</taxon_name>
    <taxon_name authority="(Small) Chaudhri" date="unknown" rank="variety">corymbosa</taxon_name>
    <taxon_hierarchy>genus Paronychia;species erecta;variety corymbosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial (occasionally biennial), often matted;</text>
      <biological_entity id="o28431" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot stout.</text>
      <biological_entity id="o28432" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate to ascending, branched especially distally, retrorsely to spreading-pubescent throughout (when pubescent);</text>
      <biological_entity id="o28433" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" modifier="especially distally; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="retrorsely-pubescent" modifier="retrorsely" name="pubescence" src="d0_s2" to="spreading-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>flowering-stems 8–48 cm;</text>
      <biological_entity id="o28434" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s3" to="48" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sterile stems 2–10 cm.</text>
      <biological_entity id="o28435" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves: stipules ovatelanceolate, 2–12 mm, apex acuminate, entire;</text>
      <biological_entity id="o28436" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o28437" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28438" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade linear to spatulate-oblanceolate, 4–40 × 1–4 mm, leathery, apex obtuse to acute, moderately antrorsely pubescent.</text>
      <biological_entity id="o28439" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o28440" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="spatulate-oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o28441" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
        <character is_modifier="false" modifier="moderately antrorsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cymes terminal, 15–200+-flowered, branched, densely to loosely grouped to form subcorymbose clusters 5–50 mm wide.</text>
      <biological_entity id="o28442" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="15-200+-flowered" value_original="15-200+-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="branched" value_original="branched" />
        <character constraint="to form" constraintid="o28443" is_modifier="false" modifier="densely to loosely" name="arrangement" src="d0_s7" value="grouped" value_original="grouped" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28443" name="form" name_original="form" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="subcorymbose" value_original="subcorymbose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 5-merous, narrowly ellipsoid, with slightly enlarged hypanthium and calyx tapering distally, 2.3–3.5 mm, glabrous to slightly puberulent proximally with straight to hooked hairs;</text>
      <biological_entity id="o28444" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-merous" value_original="5-merous" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o28445" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="slightly" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
        <character char_type="range_value" constraint="with hairs" constraintid="o28447" from="glabrous" name="pubescence" src="d0_s8" to="slightly puberulent" />
      </biological_entity>
      <biological_entity id="o28446" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="slightly" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
        <character char_type="range_value" constraint="with hairs" constraintid="o28447" from="glabrous" name="pubescence" src="d0_s8" to="slightly puberulent" />
      </biological_entity>
      <biological_entity id="o28447" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="true" name="shape" src="d0_s8" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals redbrown, white or whitish distally, veins absent, narrowly oblong to lanceolate-oblong, 1.4–2 mm, papery, margins white, ca. 0.1 mm wide, papery, apex rounded, hood formed from slight incurving, awn or mucro absent;</text>
      <biological_entity id="o28448" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o28449" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s9" to="lanceolate-oblong" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s9" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o28450" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character name="width" src="d0_s9" unit="mm" value="0.1" value_original="0.1" />
        <character is_modifier="false" name="texture" src="d0_s9" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o28451" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o28452" name="hood" name_original="hood" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o28453" name="awn" name_original="awn" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="slight" value_original="slight" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="incurving" value_original="incurving" />
      </biological_entity>
      <biological_entity id="o28454" name="mucro" name_original="mucro" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="slight" value_original="slight" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="incurving" value_original="incurving" />
      </biological_entity>
      <relation from="o28452" id="r3178" name="formed from" negation="false" src="d0_s9" to="o28453" />
      <relation from="o28452" id="r3179" name="formed from" negation="false" src="d0_s9" to="o28454" />
    </statement>
    <statement id="d0_s10">
      <text>staminodes narrowly oblong, 0.4 mm;</text>
      <biological_entity id="o28455" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 1, cleft in distal 1/10, 1.3–1.7 mm.</text>
      <biological_entity id="o28456" name="style" name_original="style" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character constraint="in distal 1/10" constraintid="o28457" is_modifier="false" name="architecture_or_shape" src="d0_s11" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28457" name="1/10" name_original="1/10" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Utricles ovoid to ellipsoid, 1–1.2 mm, rugulose, glabrous.</text>
      <biological_entity id="o28458" name="utricle" name_original="utricles" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s12" to="ellipsoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s12" value="rugulose" value_original="rugulose" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes, sandflats, pine/oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
        <character name="habitat" value="sandflats" />
        <character name="habitat" value="woodlands" modifier="pine\/oak" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., La., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Squareflower</other_name>
  <discussion>Plants with pubescent stems, strigose leaves, and pubescent receptacles have been recognized as var. corymbosa.</discussion>
  
</bio:treatment>