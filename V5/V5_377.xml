<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1838" rank="species">hookeri</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 193. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species hookeri;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060854</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial;</text>
      <biological_entity id="o2820" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex much-branched, thick and woody.</text>
      <biological_entity id="o2821" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems solitary or numerous, decumbent and rooting at base, becoming erect, 5–14 (–25) cm, with gray, soft, curly to retrorsely crispate pubescence, rarely glandular.</text>
      <biological_entity id="o2822" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at base" constraintid="o2823" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character is_modifier="false" modifier="becoming" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="14" to_unit="cm" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o2823" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade spatulate or narrowly lanceolate to oblanceolate, sometimes broadly so, 4–7 (–10) cm × 8–12 (–20) mm, reduced toward base, apex acute, pubescent on both surfaces, especially on midrib;</text>
      <biological_entity id="o2824" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2825" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s3" to="oblanceolate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
        <character constraint="toward base" constraintid="o2826" is_modifier="false" modifier="sometimes broadly; broadly" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o2826" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o2827" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character constraint="on surfaces" constraintid="o2828" is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2828" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o2829" name="midrib" name_original="midrib" src="d0_s3" type="structure" />
      <relation from="o2827" id="r316" modifier="especially" name="on" negation="false" src="d0_s3" to="o2829" />
    </statement>
    <statement id="d0_s4">
      <text>subterranean bractlike, papery.</text>
      <biological_entity id="o2830" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="location" src="d0_s4" value="subterranean" value_original="subterranean" />
        <character is_modifier="false" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" name="texture" src="d0_s4" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences reduced to single, terminal flower or open, (1–) 3–5 (–9) -flowered cyme, bracteate;</text>
      <biological_entity id="o2831" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="single" value_original="single" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2832" name="flower" name_original="flower" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o2833" name="cyme" name_original="cyme" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="(1-)3-5(-9)-flowered" value_original="(1-)3-5(-9)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts leaflike, reduced distally to ca. 1 cm.</text>
      <biological_entity id="o2834" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels ascending, straight, 1–6 cm, with a short canescence.</text>
      <biological_entity id="o2835" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2836" name="canescence" name_original="canescence" src="d0_s7" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
      </biological_entity>
      <relation from="o2835" id="r317" name="with" negation="false" src="d0_s7" to="o2836" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 10-veined, broadly tubular in flower, 12–25  5–8 mm, turbinate in fruit and swelling in middle to ca. 10 mm broad, canescent, rarely sparsely pubescent or glandular;</text>
      <biological_entity id="o2837" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2838" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="10-veined" value_original="10-veined" />
        <character constraint="in flower" constraintid="o2839" is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character constraint="in fruit" constraintid="o2840" is_modifier="false" name="shape" src="d0_s8" value="turbinate" value_original="turbinate" />
        <character constraint="in middle" constraintid="o2841" is_modifier="false" name="size" notes="" src="d0_s8" value="swelling" value_original="swelling" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="canescent" value_original="canescent" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o2839" name="flower" name_original="flower" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="25" />
      </biological_entity>
      <biological_entity id="o2840" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o2841" name="middle" name_original="middle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lobes lanceolate, 4–7 mm, with narrow, membranous margins, apex acute;</text>
      <biological_entity id="o2842" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2843" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2844" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o2845" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o2843" id="r318" name="with" negation="false" src="d0_s9" to="o2844" />
    </statement>
    <statement id="d0_s10">
      <text>corolla coral pink or white, clawed, claw equaling calyx;</text>
      <biological_entity id="o2846" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2847" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="coral pink" value_original="coral pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o2848" name="claw" name_original="claw" src="d0_s10" type="structure" />
      <biological_entity id="o2849" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>limb 4-lobed, usually deeply so, rarely 2-lobed with smaller lateral teeth, lobes 7–22 mm, appendages 2, linear, 1.5–3.5 mm (absent in subsp. bolanderi);</text>
      <biological_entity id="o2850" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2851" name="limb" name_original="limb" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="4-lobed" value_original="4-lobed" />
        <character constraint="with smaller lateral teeth" constraintid="o2852" is_modifier="false" modifier="usually deeply; deeply; rarely" name="shape" src="d0_s11" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity constraint="smaller lateral" id="o2852" name="tooth" name_original="teeth" src="d0_s11" type="structure" />
      <biological_entity id="o2853" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2854" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens slightly longer than corolla claw;</text>
      <biological_entity id="o2855" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2856" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character constraint="than corolla claw" constraintid="o2857" is_modifier="false" name="length_or_size" src="d0_s12" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o2857" name="claw" name_original="claw" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stigmas 3, slightly longer than corolla claw.</text>
      <biological_entity id="o2858" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o2859" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character constraint="than corolla claw" constraintid="o2860" is_modifier="false" name="length_or_size" src="d0_s13" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o2860" name="claw" name_original="claw" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid to oblong, equaling calyx, dehiscing by 6 teeth;</text>
      <biological_entity id="o2861" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="oblong" />
      </biological_entity>
      <biological_entity id="o2862" name="calyx" name_original="calyx" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character constraint="by teeth" constraintid="o2863" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o2863" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>carpophore 2–5 mm.</text>
      <biological_entity id="o2864" name="carpophore" name_original="carpophore" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark-brown to black, reniform, ca. 2 mm broad, with concentric rings of small papillae.</text>
      <biological_entity id="o2865" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s16" to="black" />
        <character is_modifier="false" name="shape" src="d0_s16" value="reniform" value_original="reniform" />
        <character name="width" src="d0_s16" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="concentric" id="o2866" name="ring" name_original="rings" src="d0_s16" type="structure" />
      <biological_entity id="o2867" name="papilla" name_original="papillae" src="d0_s16" type="structure">
        <character is_modifier="true" name="size" src="d0_s16" value="small" value_original="small" />
      </biological_entity>
      <relation from="o2865" id="r319" name="with" negation="false" src="d0_s16" to="o2866" />
      <relation from="o2866" id="r320" name="part_of" negation="false" src="d0_s16" to="o2867" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla lobes either 4 of unequal size or 2 with lateral teeth, lobes lanceolate to broadly oblong, 5-10 mm, corolla coral pink or white, appendages 2, 1.5-3.5 mm, linear</description>
      <determination>23a Silene hookeri subsp. hookeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla lobes 4, equal, linear, 15-25 mm, corolla white with greenish center, appendages absent</description>
      <determination>23b Silene hookeri subsp. bolanderi</determination>
    </key_statement>
  </key>
</bio:treatment>