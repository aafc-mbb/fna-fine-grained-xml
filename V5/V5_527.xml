<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Reveal" date="1985" rank="species">tiehmii</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>45: 277. 1985</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species tiehmii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060530</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, matted, scapose, 1–1.5 × 0.5–3 dm, floccose, grayish.</text>
      <biological_entity id="o6456" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, with persistent leaf-bases, up to 1/5 height of plant;</text>
      <biological_entity id="o6457" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o6458" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/5 height of plant" />
      </biological_entity>
      <relation from="o6457" id="r703" name="with" negation="false" src="d0_s1" to="o6458" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems matted;</text>
      <biological_entity constraint="caudex" id="o6459" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems scapelike, erect, slender, solid, not fistulose, (0.6–) 1–1.3 (–1.5) dm, floccose.</text>
      <biological_entity id="o6460" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="scapelike" value_original="scapelike" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="1.3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal, fasciculate in terminal tufts;</text>
      <biological_entity id="o6461" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="in terminal tufts" constraintid="o6462" is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o6462" name="tuft" name_original="tufts" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5–1.6 (–2) cm, tomentose;</text>
      <biological_entity id="o6463" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="1.6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade elliptic to oblong, (0.8–) 1–2.5 (–3) × 0.5–0.8 (–1) cm, densely white or grayish-tomentose on both surfaces, margins plane.</text>
      <biological_entity id="o6464" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="oblong" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_length" src="d0_s6" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s6" to="0.8" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character constraint="on surfaces" constraintid="o6465" is_modifier="false" name="pubescence" src="d0_s6" value="grayish-tomentose" value_original="grayish-tomentose" />
      </biological_entity>
      <biological_entity id="o6465" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o6466" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences capitate, 0.9–1.4 cm wide;</text>
      <biological_entity id="o6467" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="width" src="d0_s7" to="1.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches absent;</text>
      <biological_entity id="o6468" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, triangular, 1–1.5 mm.</text>
      <biological_entity id="o6469" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o6470" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 4–8 per cluster, turbinate-campanulate, 4–5 × 3–4 mm, rigid, floccose;</text>
      <biological_entity id="o6471" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per cluster" from="4" name="quantity" src="d0_s11" to="8" />
        <character is_modifier="false" name="shape" src="d0_s11" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>teeth 5–6, erect to slightly spreading, 1.5–2 mm.</text>
      <biological_entity id="o6472" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="6" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s12" to="slightly spreading" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 2.5–3.5 (–4) mm;</text>
      <biological_entity id="o6473" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth yellowish white or whitish to cream, sparsely glandular abaxially;</text>
      <biological_entity id="o6474" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s14" to="cream" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="architecture_or_function_or_pubescence" src="d0_s14" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, monomorphic, oblong;</text>
      <biological_entity id="o6475" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 3–4 (–4.5) mm;</text>
      <biological_entity id="o6476" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o6477" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 3–4 mm, glabrous.</text>
      <biological_entity id="o6478" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky clay slopes and washes, saltbush communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky clay slopes" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="saltbush communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45.</number>
  <other_name type="common_name">Tiehm’s wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum tiehmii is known only from the Silver Peak Range of Esmeralda County. It is considered a “sensitive” species in Nevada.</discussion>
  
</bio:treatment>