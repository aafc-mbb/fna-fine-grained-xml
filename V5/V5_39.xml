<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John W. Thieret,Richard K. Rabeler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Tanfani in F. Parlatore" date="unknown" rank="subfamily">Polycarpoideae</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="genus">POLYCARPAEA</taxon_name>
    <place_of_publication>
      <publication_title>J. Hist. Nat.</publication_title>
      <place_in_publication>2: 3, 5, plate 25. 1792</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily polycarpoideae;genus polycarpaea;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek poly-, numerous, and karpos, fruit, alluding to the numerous capsules</other_info_on_name>
    <other_info_on_name type="fna_id">126355</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [small shrubs], annual [perennial].</text>
      <biological_entity id="o25948" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots slender to stout.</text>
      <biological_entity id="o25949" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s1" to="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, branched, terete.</text>
      <biological_entity id="o25950" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite, sometimes appearing whorled, not connate, short-petiolate (basal leaves) or sessile (cauline leaves);</text>
      <biological_entity id="o25951" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules 2 per node, white, lanceolate, margins entire, apex hairlike;</text>
      <biological_entity id="o25952" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o25953" name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o25953" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity id="o25954" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25955" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hairlike" value_original="hairlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 1-veined, oblong-ovate to suborbiculate (basal leaves), subulate to linear (cauline leaves), not succulent, apex acute or hairlike.</text>
      <biological_entity id="o25956" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s5" to="suborbiculate subulate" />
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s5" to="suborbiculate subulate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o25957" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="hairlike" value_original="hairlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, compact to loose cymes;</text>
      <biological_entity id="o25958" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="compact" name="architecture" src="d0_s6" to="loose" />
      </biological_entity>
      <biological_entity id="o25959" name="cyme" name_original="cymes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts paired, scarious.</text>
      <biological_entity id="o25960" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect or spreading.</text>
      <biological_entity id="o25961" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth and androecium perigynous;</text>
      <biological_entity id="o25962" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25963" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o25964" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium cupshaped, not abruptly expanded distally;</text>
      <biological_entity id="o25965" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25966" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cup-shaped" value_original="cup-shaped" />
        <character is_modifier="false" modifier="not abruptly; distally" name="size" src="d0_s10" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals distinct, silvery with brown midrib, ovate to lanceolate, not keeled, 2.2–3.1 mm, scarious, margins scarious, apex acute;</text>
      <biological_entity id="o25967" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o25968" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character constraint="with midrib" constraintid="o25969" is_modifier="false" name="coloration" src="d0_s11" value="silvery" value_original="silvery" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s11" to="lanceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o25969" name="midrib" name_original="midrib" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o25970" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o25971" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5, pink, blade apex irregularly toothed or entire;</text>
      <biological_entity id="o25972" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o25973" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity constraint="blade" id="o25974" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s12" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectaries between filament bases;</text>
      <biological_entity id="o25975" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="between bases" constraintid="o25977" id="o25976" name="nectary" name_original="nectaries" src="d0_s13" type="structure" constraint_original="between  bases, " />
      <biological_entity constraint="filament" id="o25977" name="base" name_original="bases" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 5;</text>
      <biological_entity id="o25978" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o25979" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments distinct;</text>
      <biological_entity id="o25980" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o25981" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style 1, filiform, minute, less than 0.1 mm, glabrous proximally;</text>
      <biological_entity id="o25982" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o25983" name="style" name_original="style" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s16" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="size" src="d0_s16" value="minute" value_original="minute" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.1" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas 3, subcapitate, smooth to obscurely papillate (50×).</text>
      <biological_entity id="o25984" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o25985" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="subcapitate" value_original="subcapitate" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s17" to="obscurely papillate" />
        <character name="quantity" src="d0_s17" value="[50" value_original="[50" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules ellipsoid, opening by 3 recurved valves;</text>
      <biological_entity id="o25986" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o25987" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s18" value="3" value_original="3" />
        <character is_modifier="true" name="orientation" src="d0_s18" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o25986" id="r2880" name="opening by" negation="false" src="d0_s18" to="o25987" />
    </statement>
    <statement id="d0_s19">
      <text>carpophore present.</text>
      <biological_entity id="o25988" name="carpophore" name_original="carpophore" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 5–8, tan, translucent, pyriform, not compressed, rugulose, marginal wing absent, appendage absent.</text>
      <biological_entity id="o25989" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s20" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s20" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="shape" src="d0_s20" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s20" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="relief" src="d0_s20" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o25990" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>x = 9.</text>
      <biological_entity id="o25991" name="appendage" name_original="appendage" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o25992" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., tropics and subtropics, chiefly Old World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="tropics and subtropics" establishment_means="introduced" />
        <character name="distribution" value="chiefly Old World" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="past_name">Polycarpea</other_name>
  <discussion>Species ca. 50 (1 in the flora).</discussion>
  <references>
    <reference>Bakker, K. 1957. Revision of the genus Polycarpaea (Caryoph.) in Malaysia. Acta Bot. Neerl. 6: 48–53.</reference>
    <reference>Gagnepain, F. 1908. Contribution à la connaissance du genre Polycarpaea Lamk. J. Bot. (Morot), sér. 2, 1: 275–280.</reference>
    <reference>Lakela, O. 1962. Occurrence of species of Polycarpaea Lam. (Caryophyllaceae) in North America. Rhodora 64: 179–182.</reference>
    <reference>Lakela, O. 1963. Annotation of North American Polycarpaea. Rhodora 65: 35–44.</reference>
    <reference>Sundaramoorthy, S. and D. N. Sen. 1988. Ecology of Indian arid zone weeds. XI. Polycarpaea corymbosa (Linn.) Lamk. Geobios (Jodhpur) 15: 235–237.</reference>
  </references>
  
</bio:treatment>