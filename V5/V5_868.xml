<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">419</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Oregonium</taxon_name>
    <taxon_name authority="Eastwood" date="1931" rank="species">covilleanum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>4, 20: 138. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oregonium;species covilleanum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060242</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="unknown" rank="species">vimineum</taxon_name>
    <taxon_name authority="(Eastwood) S. Stokes" date="unknown" rank="variety">covilleanum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species vimineum;variety covilleanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect, 1–4 dm, glabrous, green to reddish.</text>
      <biological_entity id="o1863" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="reddish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: aerial flowering-stems erect, 0.3–1 dm, glabrous.</text>
      <biological_entity id="o1864" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o1865" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s1" to="1" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal;</text>
      <biological_entity id="o1866" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–4 (–5) cm, tomentose;</text>
      <biological_entity id="o1867" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade suborbiculate to reniform, (0.3–) 0.5–1.5 × 0.5–1.5 (–1.8) cm, densely white-tomentose abaxially, glabrous and bright green adaxially.</text>
      <biological_entity id="o1868" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="suborbiculate" name="shape" src="d0_s4" to="reniform" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_length" src="d0_s4" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s4" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="bright green" value_original="bright green" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymose, infrequently distally uniparous due to suppression of secondary branches, open, 5–35 × 5–30 cm;</text>
      <biological_entity id="o1869" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymose" value_original="cymose" />
        <character constraint="of secondary branches" constraintid="o1870" is_modifier="false" modifier="infrequently distally" name="architecture" src="d0_s5" value="uniparous" value_original="uniparous" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="open" value_original="open" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s5" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o1870" name="branch" name_original="branches" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches glabrous;</text>
      <biological_entity id="o1871" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 1–2 × 0.5–1 mm.</text>
      <biological_entity id="o1872" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles absent.</text>
      <biological_entity id="o1873" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres terminal at tips of slender branchlets proximally, not appressed to branches, turbinate, 2–2.5 × 1–1.5 mm, glabrous;</text>
      <biological_entity id="o1874" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character constraint="at tips" constraintid="o1875" is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character constraint="to branches" constraintid="o1877" is_modifier="false" modifier="not" name="fixation_or_orientation" notes="" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1875" name="tip" name_original="tips" src="d0_s9" type="structure" />
      <biological_entity constraint="slender" id="o1876" name="branchlet" name_original="branchlets" src="d0_s9" type="structure" />
      <biological_entity id="o1877" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <relation from="o1875" id="r210" name="part_of" negation="false" src="d0_s9" to="o1876" />
    </statement>
    <statement id="d0_s10">
      <text>teeth 5, erect, 0.4–0.8 mm.</text>
      <biological_entity id="o1878" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 2–2.5 (–3) mm;</text>
      <biological_entity id="o1879" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth pink at early anthesis, becoming white to rose or yellow, minutely puberulent;</text>
      <biological_entity id="o1880" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character constraint="at early anthesis" is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character char_type="range_value" from="white" modifier="becoming" name="coloration" src="d0_s12" to="rose or yellow" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals monomorphic, narrowly elliptic;</text>
      <biological_entity id="o1881" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s13" value="elliptic" value_original="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens included, 1.7–2 mm;</text>
      <biological_entity id="o1882" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments glabrous.</text>
      <biological_entity id="o1883" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes light-brown, 3-gonous, 1.8–2 mm. 2n = 34.</text>
      <biological_entity id="o1884" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1885" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shale or serpentine outcrops and slopes, chaparral communities, oak and pine woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shale" />
        <character name="habitat" value="serpentine outcrops" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>206.</number>
  <other_name type="common_name">Coville wild buckwheat</other_name>
  <discussion>Eriogonum covilleanum is encountered infrequently in the Inner Coast Ranges of central and southwestern California (Fresno, Kern, Merced, Monterey, San Benito, San Luis Obispo, Santa Barbara, Santa Clara, Stanislaus, and Ventura counties). On occasion it can be locally common but is never abundant or weedy. It is often found growing with other annual wild buckwheats, and care must be taken to sort the species.</discussion>
  
</bio:treatment>