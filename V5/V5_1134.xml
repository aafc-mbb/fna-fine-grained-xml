<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">555</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="mention_page">556</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">polygonum</taxon_name>
    <taxon_name authority="C. Merck ex K. Koch" date="1849" rank="species">humifusum</taxon_name>
    <taxon_name authority="(B. L. Robinson) Costea &amp; Tardif" date="2003" rank="subspecies">caurianum</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>20: 995. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonum;species humifusum;subspecies caurianum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060741</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="species">caurianum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Boston Soc. Nat. Hist.</publication_title>
      <place_in_publication>31: 264. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polygonum;species caurianum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants frequently reddish or purplish tinged;</text>
    </statement>
    <statement id="d0_s1">
      <text>homophyllous or, rarely, heterophyllous.</text>
      <biological_entity id="o1640" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="frequently" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="purplish tinged" value_original="purplish tinged" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="homophyllous" value_original="homophyllous" />
        <character name="architecture" src="d0_s1" value="," value_original="," />
        <character is_modifier="false" name="architecture" src="d0_s1" value="heterophyllous" value_original="heterophyllous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate, often zigzagged, branched mostly from base, wiry, 2–20 (–40) cm.</text>
      <biological_entity id="o1641" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character constraint="from base" constraintid="o1642" is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="wiry" value_original="wiry" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1642" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves often opposite at proximal nodes;</text>
      <biological_entity id="o1643" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at proximal nodes" constraintid="o1644" is_modifier="false" modifier="often" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1644" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ocrea 2–3 (–4) mm, proximal part funnelform, distal part soon lacerate, nearly completely deciduous;</text>
      <biological_entity id="o1645" name="ocreum" name_original="ocrea" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1646" name="part" name_original="part" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o1647" name="part" name_original="part" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="soon" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" modifier="nearly completely" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1–3.4 mm;</text>
      <biological_entity id="o1648" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade usually reddish or purple-tinged, obovate to oblanceolate, 3–12 (–25) × (1.5–) 2.5–4.5 (–8) mm, margins flat, apex rounded to obtuse;</text>
      <biological_entity id="o1649" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple-tinged" value_original="purple-tinged" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s6" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1650" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o1651" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stem-leaves 1–1.5 (–2) times as long as adjacent branch leaves, distal leaves overtopping flowers.</text>
      <biological_entity id="o1652" name="stem-leaf" name_original="stem-leaves" src="d0_s7" type="structure">
        <character constraint="leaf" constraintid="o1653" is_modifier="false" name="length" src="d0_s7" value="1-1.5(-2) times as long as adjacent branch leaves" />
      </biological_entity>
      <biological_entity constraint="branch" id="o1653" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o1654" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o1655" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <relation from="o1654" id="r175" name="overtopping" negation="false" src="d0_s7" to="o1655" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences axillary;</text>
      <biological_entity id="o1656" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>cymes ± uniformly distributed, 2–6-flowered.</text>
      <biological_entity id="o1657" name="cyme" name_original="cymes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less uniformly" name="arrangement" src="d0_s9" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="2-6-flowered" value_original="2-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels enclosed in ocreae, 0.5–1.5 mm.</text>
      <biological_entity id="o1658" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1659" name="ocrea" name_original="ocreae" src="d0_s10" type="structure" />
      <relation from="o1658" id="r176" name="enclosed in" negation="false" src="d0_s10" to="o1659" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers closed or semi-open;</text>
      <biological_entity id="o1660" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="condition" src="d0_s11" value="closed" value_original="closed" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="semi-open" value_original="semi-open" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth 1.5–2.3 (–3) mm;</text>
      <biological_entity id="o1661" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 28–49% of perianth length;</text>
      <biological_entity id="o1662" name="tube" name_original="tube" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>tepals partially overlapping, green, margins pink, rarely white, petaloid, not keeled, oblong, ± outcurved, usually not cucullate;</text>
      <biological_entity id="o1663" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="partially" name="arrangement" src="d0_s14" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o1664" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="outcurved" value_original="outcurved" />
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s14" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>midveins unbranched or branched;</text>
      <biological_entity id="o1665" name="midvein" name_original="midveins" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 5.</text>
      <biological_entity id="o1666" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes exserted from perianth, dark-brown to purple, ovatelanceolate, 2–3-gonous, 1.4–1.6 (–2.2) mm, faces unequal, apex not beaked or obscurely beaked, edges straight or concave, shiny or dull, smooth to roughened;</text>
      <biological_entity id="o1667" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s17" to="purple" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="2-3-gonous" value_original="2-3-gonous" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s17" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1668" name="perianth" name_original="perianth" src="d0_s17" type="structure" />
      <biological_entity id="o1669" name="face" name_original="faces" src="d0_s17" type="structure">
        <character is_modifier="false" name="size" src="d0_s17" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o1670" name="apex" name_original="apex" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s17" value="beaked" value_original="beaked" />
        <character is_modifier="false" modifier="obscurely" name="architecture_or_shape" src="d0_s17" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o1671" name="edge" name_original="edges" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s17" value="concave" value_original="concave" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="dull" value_original="dull" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s17" to="roughened" />
      </biological_entity>
      <relation from="o1667" id="r177" name="exserted from" negation="false" src="d0_s17" to="o1668" />
    </statement>
    <statement id="d0_s18">
      <text>late-season achenes common, 2–3.5 mm.</text>
      <biological_entity constraint="late-season" id="o1672" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s18" value="common" value_original="common" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravel bars, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravel bars" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20-700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Nunavut, Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9a.</number>
  <other_name type="common_name">Alaska knotweed</other_name>
  <other_name type="common_name">renouée du Nord-Ouest</other_name>
  <discussion>Polygonum humifusum often has opposite proximal leaves. In North America, the closest relative of subsp. caurianum is P. fowleri, rare individuals of which produce opposite leaves.</discussion>
  <discussion>Three specimens of the Asian subsp. humifusum were collected near Nanaimo, Vancouver Island, by J. Macoun in 1883 and 1887 (J. F. Brenckle 1941); the subspecies has not been recollected there. It has stems and leaves that are green, and achenes 2.1–2.7 mm, ± beaked, and exserted 0.9–1.3 mm from the perianth at maturity.</discussion>
  
</bio:treatment>