<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">39</other_info_on_meta>
    <other_info_on_meta type="mention_page">32</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="subfamily">Paronychioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">paronychia</taxon_name>
    <taxon_name authority="M. C. Johnston" date="1963" rank="species">jonesii</taxon_name>
    <place_of_publication>
      <publication_title>Wrightia</publication_title>
      <place_in_publication>2: 250. 1963</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily paronychioideae;genus paronychia;species jonesii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060686</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
      <biological_entity id="o22380" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot slender.</text>
      <biological_entity id="o22381" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate, sprawling-spreading, much-branched, 10–35 cm, short-pubescent.</text>
      <biological_entity id="o22382" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="sprawling-spreading" value_original="sprawling-spreading" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="much-branched" value_original="much-branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="35" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="short-pubescent" value_original="short-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules ovate, 3–7.5 mm, apex acuminate, entire;</text>
      <biological_entity id="o22383" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22384" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22385" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate to spatulate, 5–18 × 1.5–3.5 mm, leathery, apex obtuse to very short-cuspidate, densely appressed-pubescent.</text>
      <biological_entity id="o22386" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o22387" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="18" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o22388" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse to very" value_original="obtuse to very" />
        <character is_modifier="false" modifier="very" name="architecture_or_shape" src="d0_s4" value="short-cuspidate" value_original="short-cuspidate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cymes terminal and subterminal, occasionally axillary, 3–7-flowered, congested, clusters 3–8 mm wide.</text>
      <biological_entity id="o22389" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="subterminal" value_original="subterminal" />
        <character is_modifier="false" modifier="occasionally" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-7-flowered" value_original="3-7-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 5-merous, ± extended-urceolate, with enlarged hypanthium and calyx constricted proximally, flaring distally, 1.8–2 mm, pubescent with short, hooked hairs proximally;</text>
      <biological_entity id="o22390" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-merous" value_original="5-merous" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="extended-urceolate" value_original="extended-urceolate" />
      </biological_entity>
      <biological_entity id="o22391" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o22392" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="proximally" name="size" src="d0_s6" value="constricted" value_original="constricted" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s6" value="flaring" value_original="flaring" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character constraint="with hairs" constraintid="o22393" is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22393" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s6" value="hooked" value_original="hooked" />
      </biological_entity>
      <relation from="o22390" id="r2490" name="with" negation="false" src="d0_s6" to="o22391" />
    </statement>
    <statement id="d0_s7">
      <text>sepals redbrown, often finely striped or mottled, veins absent, spatulate-obovate to suboblong, 0.8–1.3 mm, leathery to rigid, margins white, 0.1–0.2 mm wide, papery, apex terminated by awn, broadly rounded, awn divergent, 0.3–0.4 mm, broadly conic in proximal 1/3–1/2 with white, scabrous spine;</text>
      <biological_entity id="o22394" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="often finely" name="coloration" src="d0_s7" value="striped" value_original="striped" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="mottled" value_original="mottled" />
      </biological_entity>
      <biological_entity id="o22395" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character char_type="range_value" from="spatulate-obovate" name="shape" src="d0_s7" to="suboblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s7" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="leathery" name="texture" src="d0_s7" to="rigid" />
      </biological_entity>
      <biological_entity id="o22396" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o22397" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o22398" name="awn" name_original="awn" src="d0_s7" type="structure" />
      <biological_entity id="o22399" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.4" to_unit="mm" />
        <character constraint="in proximal 1/3-1/2" constraintid="o22400" is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o22400" name="1/3-1/2" name_original="1/3-1/2" src="d0_s7" type="structure" />
      <biological_entity id="o22401" name="spine" name_original="spine" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <relation from="o22397" id="r2491" name="terminated by" negation="false" src="d0_s7" to="o22398" />
      <relation from="o22400" id="r2492" name="with" negation="false" src="d0_s7" to="o22401" />
    </statement>
    <statement id="d0_s8">
      <text>staminodes filiform, 0.5–0.6 mm;</text>
      <biological_entity id="o22402" name="staminode" name_original="staminodes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 1, 0.7–0.8 mm, cleft in distal 1/5.</text>
      <biological_entity id="o22403" name="style" name_original="style" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
        <character constraint="in distal 1/5" constraintid="o22404" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity constraint="distal" id="o22404" name="1/5" name_original="1/5" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Utricles globose, 0.8–1 mm, smooth, glabrous.</text>
      <biological_entity id="o22405" name="utricle" name_original="utricles" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, open grounds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open grounds" modifier="sandy" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-40 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="40" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Jones’ nailwort</other_name>
  <discussion>Paronychia jonesii most closely resembles P. drummondii and is known only from southeastern coastal Texas.</discussion>
  
</bio:treatment>