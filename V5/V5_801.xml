<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">391</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="mention_page">392</other_info_on_meta>
    <other_info_on_meta type="illustration_page">388</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="unknown" rank="species">gordonii</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 20. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species gordonii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060297</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect to spreading, annual, (0.5–) 1–5 (–7) dm, glabrous or sparsely hispid, grayish.</text>
      <biological_entity id="o13808" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="7" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o13809" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o13810" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, solid, not fistulose, 0.5–1.5 (–3) dm, glabrous or sparsely hispid.</text>
      <biological_entity id="o13811" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o13812" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="3" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s2" to="1.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal;</text>
      <biological_entity id="o13813" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–5 cm, glabrous or sparsely hirsute;</text>
      <biological_entity id="o13814" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade obovate to round or reniform, 1–5 × 1–5 cm, sparsely villous to hirsute and green on both surfaces, becoming glabrous with age, margins plane.</text>
      <biological_entity id="o13815" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="round or reniform" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="sparsely villous" name="pubescence" src="d0_s5" to="hirsute" />
        <character constraint="on surfaces" constraintid="o13816" is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character constraint="with age" constraintid="o13817" is_modifier="false" modifier="becoming" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13816" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o13817" name="age" name_original="age" src="d0_s5" type="structure" />
      <biological_entity id="o13818" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymose, open to dense, 5–40 (–50) × 5–50 cm;</text>
      <biological_entity id="o13819" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="50" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="40" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches not fistulose, glabrous or sparsely hispid;</text>
      <biological_entity id="o13820" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="fistulose" value_original="fistulose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 0.5–2 (–3) × 0.5–2.5 mm.</text>
      <biological_entity id="o13821" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s8" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles erect, straight, slender, 0.5–2.5 (–4) cm, glabrous.</text>
      <biological_entity id="o13822" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres campanulate, 0.6–1.5 × 0.8–1.5 mm, glabrous;</text>
      <biological_entity id="o13823" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s10" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, erect, 0.2–0.4 mm.</text>
      <biological_entity id="o13824" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1–2.5 mm;</text>
      <biological_entity id="o13825" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth white with greenish or reddish midribs, rarely yellowish, becoming pink to rose, glabrous;</text>
      <biological_entity id="o13826" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character constraint="with midribs" constraintid="o13827" is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" notes="" src="d0_s13" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="pink" modifier="becoming" name="coloration" src="d0_s13" to="rose" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13827" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals monomorphic, oblong to narrowly ovate;</text>
      <biological_entity id="o13828" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s14" to="narrowly ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens included to exserted, 1–1.8 mm;</text>
      <biological_entity id="o13829" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character char_type="range_value" from="included" name="position" src="d0_s15" to="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments glabrous.</text>
      <biological_entity id="o13830" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes shiny light-brown to brown, 3-gonous, 2–2.5 mm, glabrous.</text>
      <biological_entity id="o13831" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="shiny" value_original="shiny" />
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s17" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to clayey flats and slopes, saltbush, greasewood, and sagebrush communities, pinyon and/or juniper or montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="greasewood" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="sandy to clayey flats" />
        <character name="habitat" value="pinyon" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Nebr., N.Mex., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>159.</number>
  <other_name type="past_name">gordoni</other_name>
  <other_name type="common_name">Gordon’s wild buckwheat</other_name>
  <discussion>Eriogonum gordonii is common to even locally abundant throughout a range that extends in an arc from the Colorado Plateau of extreme northern Coconino and Apache counties, Arizona, and San Juan County, New Mexico, northward through the desert regions of eastern Utah and western Colorado into southwestern Wyoming, then northeastward mainly in short-grass prairie through east-central Wyoming and extreme northwestern Nebraska to South Dakota. A series of disjunct populations occurs in Fremont and Las Animas counties in Colorado. The species can be weedy in appearance but usually is absent from areas of significant disturbance, unlike other annual wild buckwheats.</discussion>
  
</bio:treatment>