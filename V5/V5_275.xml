<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="mention_page">129</other_info_on_meta>
    <other_info_on_meta type="mention_page">130</other_info_on_meta>
    <other_info_on_meta type="mention_page">133</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Loefling in C. Linnaeus" date="1754" rank="genus">minuartia</taxon_name>
    <taxon_name authority="T. W. Nelson &amp; J. P. Nelson" date="1991" rank="species">stolonifera</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>43: 17, fig. 1. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus minuartia;species stolonifera;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060654</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, mat-forming.</text>
      <biological_entity id="o21843" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots moderately stout, woody.</text>
      <biological_entity id="o21844" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ± erect, gray-green, 10–20 cm, glabrous or often stipitate-glandular, especially distally, internodes of stems 1–6 times as long as leaves (proximal leaves often shorter than internodes), 2–3 stolons radiating from crown, 6–20 cm.</text>
      <biological_entity id="o21845" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o21846" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o21848" is_modifier="false" name="length" src="d0_s2" value="1-6 times as long as leaves" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <biological_entity id="o21847" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o21848" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21849" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character constraint="from crown" constraintid="o21850" is_modifier="false" name="arrangement" src="d0_s2" value="radiating" value_original="radiating" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21850" name="crown" name_original="crown" src="d0_s2" type="structure" />
      <relation from="o21846" id="r2442" modifier="especially distally; distally" name="part_of" negation="false" src="d0_s2" to="o21847" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves overlapping, loosely proximally, evenly spaced, connate proximally, with tight, scarious sheath 0.3–0.8 mm;</text>
      <biological_entity id="o21851" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="loosely proximally; proximally; evenly" name="arrangement" src="d0_s3" value="spaced" value_original="spaced" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o21852" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s3" value="tight" value_original="tight" />
        <character is_modifier="true" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s3" to="0.8" to_unit="mm" />
      </biological_entity>
      <relation from="o21851" id="r2443" name="with" negation="false" src="d0_s3" to="o21852" />
    </statement>
    <statement id="d0_s4">
      <text>blade ± straight to outwardly curved, gray-green, shallowly concave, 3-veined, often prominently so abaxially, needlelike, 5–11 × 0.5–0.9 mm, rigid, margins not thickened, scarious in proximal 1/2, stipitate-glandular, apex green to purple, acute to obtuse, navicular, dull, stipitate-glandular throughout;</text>
      <biological_entity id="o21853" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="less straight" name="course" src="d0_s4" to="outwardly curved" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="concave" value_original="concave" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="often prominently; prominently; abaxially" name="architecture_or_shape" src="d0_s4" value="needlelike" value_original="needlelike" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o21854" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character constraint="in proximal 1/2" constraintid="o21855" is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21855" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <biological_entity id="o21856" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="purple" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="navicular" value_original="navicular" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>axillary leaves weakly developed among proximal cauline leaves.</text>
      <biological_entity constraint="axillary" id="o21857" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal cauline" id="o21858" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o21857" id="r2444" modifier="weakly" name="developed among" negation="false" src="d0_s5" to="o21858" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 7–25-flowered, open cymes;</text>
      <biological_entity id="o21859" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="7-25-flowered" value_original="7-25-flowered" />
      </biological_entity>
      <biological_entity id="o21860" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts lanceolate to subulate, herbaceous, margins scarious.</text>
      <biological_entity id="o21861" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="subulate" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o21862" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.3–1.5 cm, often stipitate-glandular.</text>
      <biological_entity id="o21863" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium disc-shaped;</text>
      <biological_entity id="o21864" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o21865" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="disc--shaped" value_original="disc--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 1–3-veined (weakly in flower), ovate to lanceolate, (herbaceous portion narrowly lanceolate to linear-oblong), 3.5–4.8 mm, not enlarging in fruit, apex green to purple, narrowly acute to acuminate, not hooded, stipitate-glandular;</text>
      <biological_entity id="o21866" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21867" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-3-veined" value_original="1-3-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.8" to_unit="mm" />
        <character constraint="in fruit" constraintid="o21868" is_modifier="false" modifier="not" name="size" src="d0_s10" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o21868" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity id="o21869" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="purple" />
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s10" to="acuminate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals broadly oblanceolate, 1.6–1.8 times as long as sepals, apex rounded, entire.</text>
      <biological_entity id="o21870" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o21871" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character constraint="sepal" constraintid="o21872" is_modifier="false" name="length" src="d0_s11" value="1.6-1.8 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o21872" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o21873" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules sessile, ovoid, 3.5–5 mm, equaling sepals.</text>
      <biological_entity id="o21874" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21875" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds reddish-brown to brown, oblongelliptic, 2–2.4 mm, tuberculate.</text>
      <biological_entity id="o21876" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s13" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Jeffrey pine woodlands, serpentine soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="jeffrey pine woodlands" />
        <character name="habitat" value="serpentine soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <other_name type="common_name">Stolon or Scott Mountain sandwort</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Minuartia stolonifera, like M. decumbens and M. rosei, is restricted to serpentine soils of northwestern California, specifically to Scott Mountain in Siskiyou County. The three species are most closely related to the polymorphic M. nuttallii.</discussion>
  
</bio:treatment>