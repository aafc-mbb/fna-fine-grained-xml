<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">150</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">scleranthus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">annuus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">annuus</taxon_name>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus scleranthus;species annuus;subspecies annuus;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250060820</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or biennial.</text>
      <biological_entity id="o1849" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to prostrate, 2–25 cm.</text>
      <biological_entity id="o1850" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="prostrate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 3–24 mm.</text>
      <biological_entity id="o1851" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="distance" src="d0_s2" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers usually equaling or exceeded by bracts;</text>
      <biological_entity id="o1852" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s3" value="exceeded by bracts" value_original="exceeded by bracts" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>hypanthium becoming strongly 10-ribbed in fruit, 1.2–2 mm;</text>
      <biological_entity id="o1853" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character constraint="in fruit" constraintid="o1854" is_modifier="false" modifier="becoming strongly" name="architecture_or_shape" src="d0_s4" value="10-ribbed" value_original="10-ribbed" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1854" name="fruit" name_original="fruit" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>sepals not overlapping, spreading to erect in fruit, 1.5–4 mm, margins 0.1 mm or less wide, apex acute;</text>
      <biological_entity id="o1855" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" constraint="in fruit" constraintid="o1856" from="spreading" name="orientation" src="d0_s5" to="erect" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1856" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
      <biological_entity id="o1857" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="0.1" value_original="0.1" />
        <character is_modifier="false" modifier="less" name="width" src="d0_s5" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o1858" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens ca. 1/2 length of sepals.</text>
    </statement>
    <statement id="d0_s7">
      <text>“Fruits” 3.2–5 mm including sepals.</text>
      <biological_entity id="o1859" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" value="1/2 length of sepals" value_original="1/2 length of sepals" />
      </biological_entity>
      <biological_entity id="o1861" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <relation from="o1860" id="r209" name="including" negation="false" src="d0_s7" to="o1861" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 44 (Europe).</text>
      <biological_entity id="o1860" name="fruit" name_original="fruits" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1862" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy fields, roadsides, weedy areas, lawns</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="weedy areas" />
        <character name="habitat" value="lawns" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Ala., Ark., Calif., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., N.H., N.J., N.Y., N.C., Ohio, Okla., Oreg., Pa., R.I., S.C., Tenn., Vt., Va., Wash., W.Va., Wis.; Europe; w Asia; n Africa; widely naturalized elsewhere, including Mexico, Central America (Costa Rica), South America (Ecuador), e Asia (South Korea), Africa (Kenya, Republic of South Africa), Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="w Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="widely naturalized elsewhere" establishment_means="native" />
        <character name="distribution" value="including Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Costa Rica)" establishment_means="native" />
        <character name="distribution" value="South America (Ecuador)" establishment_means="native" />
        <character name="distribution" value="e Asia (South Korea)" establishment_means="native" />
        <character name="distribution" value="Africa (Kenya)" establishment_means="native" />
        <character name="distribution" value="Africa (Republic of South Africa)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  
</bio:treatment>