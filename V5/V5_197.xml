<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard K. Rabeler,Ronald L. Hartman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">50</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HOLOSTEUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 88. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 39. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus holosteum;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek holos, whole or all, and osteon, bone, humorous allusion to frailty of the plant</other_info_on_name>
    <other_info_on_name type="fna_id">115639</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or winter-annual.</text>
      <biological_entity id="o512" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" value_original="winter-annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots slender.</text>
      <biological_entity id="o513" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, simple or branched, terete.</text>
      <biological_entity id="o514" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves forming basal rosette, connate proximally into sheath, petiolate (proximal leaves) or sessile (cauline leaves);</text>
      <biological_entity id="o515" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="into sheath" constraintid="o517" is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o516" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity id="o517" name="sheath" name_original="sheath" src="d0_s3" type="structure" />
      <relation from="o515" id="r48" name="forming" negation="false" src="d0_s3" to="o516" />
    </statement>
    <statement id="d0_s4">
      <text>blade 1-veined, oblanceolate to spatulate (proximal leaves) or elliptic to ovate (cauline leaves), somewhat succulent, apex acute.</text>
      <biological_entity id="o518" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="spatulate or elliptic" />
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s4" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o519" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, umbellate cymes, sometimes not yet developed in young individuals, then with 2–4 bracts and often buds at base of pedicel;</text>
      <biological_entity id="o520" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o521" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
      </biological_entity>
      <biological_entity id="o522" name="individual" name_original="individuals" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o523" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o524" name="bud" name_original="buds" src="d0_s5" type="structure" />
      <biological_entity id="o525" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o526" name="pedicel" name_original="pedicel" src="d0_s5" type="structure" />
      <relation from="o521" id="r49" modifier="sometimes not yet" name="developed in" negation="false" src="d0_s5" to="o522" />
      <relation from="o521" id="r50" name="with" negation="false" src="d0_s5" to="o523" />
      <relation from="o524" id="r51" name="at" negation="false" src="d0_s5" to="o525" />
      <relation from="o525" id="r52" name="part_of" negation="false" src="d0_s5" to="o526" />
    </statement>
    <statement id="d0_s6">
      <text>bracts clustered, foliaceous with scarious margins to entirely scarious.</text>
      <biological_entity id="o527" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s6" value="clustered" value_original="clustered" />
        <character constraint="with margins" constraintid="o528" is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o528" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="entirely" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels reflexed after flowering, erect in fruit.</text>
      <biological_entity id="o529" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character constraint="after flowering" is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character constraint="in fruit" constraintid="o530" is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o530" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual or occasionally unisexual and pistillate;</text>
      <biological_entity id="o531" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="occasionally" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o532" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o533" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, distinct, green, lanceolate to ovate, 2.5–4.5 mm, herbaceous, margins white, scarious, apex acute to obtuse, not hooded;</text>
      <biological_entity id="o534" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s10" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o535" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o536" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="obtuse" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="hooded" value_original="hooded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, white to pink, clawed, blade apex jagged but not 2-fid;</text>
      <biological_entity id="o537" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity constraint="blade" id="o538" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="jagged" value_original="jagged" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectaries not apparent;</text>
      <biological_entity id="o539" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="apparent" value_original="apparent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 3–5, arising from base of ovary;</text>
      <biological_entity id="o540" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="5" />
        <character constraint="from base" constraintid="o541" is_modifier="false" name="orientation" src="d0_s13" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o541" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o542" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
      <relation from="o541" id="r53" name="part_of" negation="false" src="d0_s13" to="o542" />
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct nearly to base;</text>
      <biological_entity id="o543" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character constraint="to base" constraintid="o544" is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o544" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>staminodes absent;</text>
      <biological_entity id="o545" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 3 (–5), clavate to filiform, 0.5–1.5 mm, glabrous proximally;</text>
      <biological_entity id="o546" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="5" />
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character char_type="range_value" from="clavate" name="shape" src="d0_s16" to="filiform" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas 3 (–5), subterminal to linear along adaxial surface of styles, minutely papillate (50×).</text>
      <biological_entity id="o547" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="5" />
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s17" value="subterminal" value_original="subterminal" />
        <character constraint="along adaxial surface" constraintid="o548" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="minutely" name="relief" notes="" src="d0_s17" value="papillate" value_original="papillate" />
        <character name="quantity" src="d0_s17" value="[50" value_original="[50" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o548" name="surface" name_original="surface" src="d0_s17" type="structure" />
      <biological_entity id="o549" name="style" name_original="styles" src="d0_s17" type="structure" />
      <relation from="o548" id="r54" name="part_of" negation="false" src="d0_s17" to="o549" />
    </statement>
    <statement id="d0_s18">
      <text>Capsules ovoid to cylindric, opening by 6 (rarely 8 or 10) revolute teeth;</text>
      <biological_entity id="o550" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s18" to="cylindric" />
      </biological_entity>
      <biological_entity id="o551" name="tooth" name_original="teeth" src="d0_s18" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s18" value="6" value_original="6" />
        <character is_modifier="true" name="shape_or_vernation" src="d0_s18" value="revolute" value_original="revolute" />
      </biological_entity>
      <relation from="o550" id="r55" name="opening by" negation="false" src="d0_s18" to="o551" />
    </statement>
    <statement id="d0_s19">
      <text>carpophore absent.</text>
      <biological_entity id="o552" name="carpophore" name_original="carpophore" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 35–60, orange to brown, oblong, shield-shaped, dorsiventrally compressed, papillate, marginal wing absent, appendage absent;</text>
      <biological_entity id="o553" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="35" name="quantity" src="d0_s20" to="60" />
        <character char_type="range_value" from="orange" name="coloration" src="d0_s20" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s20" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s20" value="shield--shaped" value_original="shield--shaped" />
        <character is_modifier="false" modifier="dorsiventrally" name="shape" src="d0_s20" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="relief" src="d0_s20" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o554" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o555" name="appendage" name_original="appendage" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>embryo central, straight.</text>
    </statement>
    <statement id="d0_s22">
      <text>x = 10.</text>
      <biological_entity id="o556" name="embryo" name_original="embryo" src="d0_s21" type="structure">
        <character is_modifier="false" name="position" src="d0_s21" value="central" value_original="central" />
        <character is_modifier="false" name="course" src="d0_s21" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="x" id="o557" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe (e Mediterranean region), c, sw Asia, Africa (Mediterranean region, s to Ethiopia); introduced in South America (Argentina), w Europe, Africa (Republic of South Africa).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe (e Mediterranean region)" establishment_means="introduced" />
        <character name="distribution" value="c" establishment_means="introduced" />
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa (Mediterranean region)" establishment_means="introduced" />
        <character name="distribution" value="Africa (s to Ethiopia)" establishment_means="introduced" />
        <character name="distribution" value="in South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="w Europe" establishment_means="introduced" />
        <character name="distribution" value="Africa (Republic of South Africa)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Jagged chickweed</other_name>
  <discussion>Species 3–4 (1 in the flora).</discussion>
  <references>
    <reference>Shinners, L. H. 1965. Holosteum umbellatum (Caryophyllaceae) in the United States: Population explosion and fractionated suicide. Sida 2: 119–128.</reference>
  </references>
  
</bio:treatment>