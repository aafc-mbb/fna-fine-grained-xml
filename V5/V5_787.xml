<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">380</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(H. Gross) Reveal" date="1967" rank="subgenus">Pterogonum</taxon_name>
    <taxon_name authority="Torrey in L. Sitgreaves" date="1853" rank="species">alatum</taxon_name>
    <taxon_name authority="Torrey in War Department [U.S.]" date="1857" rank="variety">glabriusculum</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 131. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus pterogonum;species alatum;variety glabriusculum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060164</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–20 (–25) dm.</text>
      <biological_entity id="o20565" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems glabrous or nearly so.</text>
      <biological_entity id="o20566" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s1" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o20567" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal: petiole 2–5, strigose, blade linear-lanceolate to lanceolate, 8–20 × 0.5–1.5 cm, slightly strigose or glabrous adaxially, glabrous abaxially except for strigose margins and midveins;</text>
      <biological_entity constraint="basal" id="o20568" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o20569" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o20570" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="except-for midveins" constraintid="o20572" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20571" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o20572" name="midvein" name_original="midveins" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>cauline: blade 1–9 cm.</text>
      <biological_entity constraint="cauline" id="o20573" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o20574" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 2–6.5 dm;</text>
      <biological_entity id="o20575" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s5" to="6.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches glabrous.</text>
      <biological_entity id="o20576" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles glabrous or occasionally slightly strigose.</text>
      <biological_entity id="o20577" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally slightly" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres glabrous.</text>
      <biological_entity id="o20578" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Perianths yellow to yellowish green or maroon.</text>
      <biological_entity id="o20579" name="perianth" name_original="perianths" src="d0_s9" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s9" to="yellowish green or maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Achenes 5.5–9 × 3–5.5 mm. 2n = 40.</text>
      <biological_entity id="o20580" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20581" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and gentle slopes, mixed grassland, saltbush, and mesquite communities, oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="gentle slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="mesquite communities" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>146b.</number>
  <other_name type="common_name">Canadian River wild buckwheat</other_name>
  <discussion>Variety glabriusculum is a distinctive taxon of the southern Great Plains, often being the tallest plants on the low, rolling hills. It is geographically isolated from var. alatum, being found near Ruth in Curry County, New Mexico, in western Oklahoma, and in northern Texas.</discussion>
  
</bio:treatment>