<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">microthecum</taxon_name>
    <taxon_name authority="Reveal" date="1971" rank="variety">johnstonii</taxon_name>
    <place_of_publication>
      <publication_title>Sci. Bull. Brigham Young Univ., Biol. Ser.</publication_title>
      <place_in_publication>13(1): 28, fig. 15. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species microthecum;variety johnstonii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060391</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 0.6–1.3 × 2–5 dm.</text>
      <biological_entity id="o3289" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.6" from_unit="dm" name="length" src="d0_s0" to="1.3" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="width" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex spreading;</text>
      <biological_entity id="o3290" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o3291" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems 0.3–0.6 dm, floccose to subglabrous.</text>
      <biological_entity id="o3292" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o3293" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s2" to="0.6" to_unit="dm" />
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s2" to="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade elliptic to ovate, 0.5–1 × (0.2–) 0.3–0.5 (–0.6) cm, densely whitish-brown-tomentose abaxially, floccose to subglabrous adaxially, margins not revolute.</text>
      <biological_entity id="o3294" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3295" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s3" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="0.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="whitish-brown-tomentose" value_original="whitish-brown-tomentose" />
        <character char_type="range_value" from="floccose" modifier="adaxially" name="pubescence" src="d0_s3" to="subglabrous" />
      </biological_entity>
      <biological_entity id="o3296" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 0.5–3 cm;</text>
      <biological_entity id="o3297" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches floccose to subglabrous.</text>
      <biological_entity id="o3298" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s5" to="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres (2–) 2.5–3 mm, usually glabrous.</text>
      <biological_entity id="o3299" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers (2.5–) 3–3.5 (–4) mm;</text>
      <biological_entity id="o3300" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth white to reddish.</text>
      <biological_entity id="o3301" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes 2.5–3 mm.</text>
      <biological_entity id="o3302" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Granitic slopes, montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granitic slopes" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2600-2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1e.</number>
  <other_name type="common_name">Johnston’s wild buckwheat</other_name>
  <discussion>Variety johnstonii is rare and known only from the upper reaches of the San Gabriel Mountains near the San Bernardino–Los Angeles county line.</discussion>
  
</bio:treatment>