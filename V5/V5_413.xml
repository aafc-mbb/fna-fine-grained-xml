<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="B. L. Robinson" date="1893" rank="species">scaposa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>28: 145. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species scaposa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060884</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silene</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">scaposa</taxon_name>
    <taxon_name authority="C. L. Hitchcock &amp; Maguire" date="unknown" rank="variety">lobata</taxon_name>
    <taxon_hierarchy>genus Silene;species scaposa;variety lobata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, subscapose, cespitose;</text>
      <biological_entity id="o3776" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="subscapose" value_original="subscapose" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot stout;</text>
      <biological_entity id="o3777" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched, woody.</text>
      <biological_entity id="o3778" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems several, erect, simple, 15–50 cm, puberulent, viscid-glandular distally.</text>
      <biological_entity id="o3779" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s3" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="distally" name="architecture_or_function_or_pubescence" src="d0_s3" value="viscid-glandular" value_original="viscid-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o3780" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3781" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal marcescent, long-petiolate, densely tufted, blade 1-veined, narrowly oblanceolate, 2–10 (–20) cm × 2–12 (–20) mm, not fleshy, base tapering to petiole, apex acute to obtuse, finely puberulent on both surfaces;</text>
      <biological_entity constraint="basal" id="o3782" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="condition" src="d0_s5" value="marcescent" value_original="marcescent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="long-petiolate" value_original="long-petiolate" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s5" value="tufted" value_original="tufted" />
      </biological_entity>
      <biological_entity id="o3783" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o3784" name="base" name_original="base" src="d0_s5" type="structure">
        <character constraint="to petiole" constraintid="o3785" is_modifier="false" name="shape" src="d0_s5" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o3785" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
      <biological_entity id="o3786" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character constraint="on surfaces" constraintid="o3787" is_modifier="false" modifier="finely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o3787" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>cauline in 1–3 pairs, sessile, much reduced, blade linear-lanceolate, not fleshy.</text>
      <biological_entity constraint="cauline" id="o3788" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o3789" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o3790" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o3788" id="r416" name="in" negation="false" src="d0_s6" to="o3789" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 1–5 (–7) -flowered, with terminal flower and lateral, open, pedunculate cymes often reduced to single flowers, bracteate;</text>
      <biological_entity id="o3791" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-5(-7)-flowered" value_original="1-5(-7)-flowered" />
        <character is_modifier="false" name="position" notes="" src="d0_s7" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o3792" name="flower" name_original="flower" src="d0_s7" type="structure" />
      <biological_entity id="o3793" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o3794" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="single" value_original="single" />
      </biological_entity>
      <relation from="o3791" id="r417" name="with" negation="false" src="d0_s7" to="o3792" />
    </statement>
    <statement id="d0_s8">
      <text>bracts narrowly lanceolate, 3–10 (–20) mm.</text>
      <biological_entity id="o3795" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, elongate, 0.5–4.5 cm, glandular-puberulent.</text>
      <biological_entity id="o3796" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s9" to="4.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx prominently 10-veined, those to lobes lance-shaped broadened and thickened distally, commissural veins slender, not forked distally, campanulate, 10–12 × 3.5–5 mm in flower, enlarging to 15 × 10 mm in fruit, not contracted around carpophore, papery, margins dentate, glandular-pubescent, viscid, veins parallel, with pale commissures, lobes patent, ovate, 1.5–4 mm, rigid, margins broad, membranous;</text>
      <biological_entity id="o3797" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3798" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s10" value="10-veined" value_original="10-veined" />
        <character is_modifier="false" name="width" src="d0_s10" value="broadened" value_original="broadened" />
        <character is_modifier="false" modifier="distally" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o3799" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lance--shaped" value_original="lance--shaped" />
      </biological_entity>
      <biological_entity constraint="commissural" id="o3800" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="not; distally" name="shape" src="d0_s10" value="forked" value_original="forked" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" constraint="in flower" constraintid="o3801" from="3.5" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
        <character name="length" src="d0_s10" unit="mm" value="15" value_original="15" />
        <character constraint="in fruit" constraintid="o3802" name="width" src="d0_s10" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o3801" name="flower" name_original="flower" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s10" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o3802" name="fruit" name_original="fruit" src="d0_s10" type="structure">
        <character constraint="around carpophore" constraintid="o3803" is_modifier="false" modifier="not" name="condition_or_size" notes="" src="d0_s10" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o3803" name="carpophore" name_original="carpophore" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" notes="" src="d0_s10" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o3804" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="coating" src="d0_s10" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o3805" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o3806" name="commissure" name_original="commissures" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o3807" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="patent" value_original="patent" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o3808" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o3798" id="r418" name="to" negation="false" src="d0_s10" to="o3799" />
      <relation from="o3805" id="r419" name="with" negation="false" src="d0_s10" to="o3806" />
    </statement>
    <statement id="d0_s11">
      <text>corolla off-white to dingy purple-red, clawed, claw exceeding calyx, ciliate proximally, broadened distally, limbs erect, 2–4-lobed, less than 1/2 length of calyx, lobes 2–5 mm, appendages 2–4, 0.5–1 mm;</text>
      <biological_entity id="o3809" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3810" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="off-white" name="coloration" src="d0_s11" to="dingy purple-red" />
        <character is_modifier="false" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o3811" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="distally" name="width" src="d0_s11" value="broadened" value_original="broadened" />
      </biological_entity>
      <biological_entity id="o3812" name="calyx" name_original="calyx" src="d0_s11" type="structure" />
      <biological_entity id="o3813" name="limb" name_original="limbs" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s11" value="2-4-lobed" value_original="2-4-lobed" />
        <character char_type="range_value" from="0 length of calyx" name="length" src="d0_s11" to="1/2 length of calyx" />
      </biological_entity>
      <biological_entity id="o3814" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3815" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="4" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o3811" id="r420" name="exceeding" negation="false" src="d0_s11" to="o3812" />
    </statement>
    <statement id="d0_s12">
      <text>stamens slightly exserted;</text>
      <biological_entity id="o3816" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3817" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments lanate, expanded at base;</text>
      <biological_entity id="o3818" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o3819" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="lanate" value_original="lanate" />
        <character constraint="at base" constraintid="o3820" is_modifier="false" name="size" src="d0_s13" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o3820" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>styles 3–5, ± equaling calyx.</text>
      <biological_entity id="o3821" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o3822" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
      <biological_entity id="o3823" name="calyx" name_original="calyx" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules slightly longer than calyx, opening by 3–5 teeth;</text>
      <biological_entity id="o3824" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character constraint="than calyx" constraintid="o3825" is_modifier="false" name="length_or_size" src="d0_s15" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o3825" name="calyx" name_original="calyx" src="d0_s15" type="structure" />
      <biological_entity id="o3826" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s15" to="5" />
      </biological_entity>
      <relation from="o3824" id="r421" name="opening by" negation="false" src="d0_s15" to="o3826" />
    </statement>
    <statement id="d0_s16">
      <text>carpophore 1.5–2.5 mm.</text>
      <biological_entity id="o3827" name="carpophore" name_original="carpophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds brown, reniform, 1.2–2 mm, margins with large, inflated papillae, rugose on sides.</text>
      <biological_entity id="o3828" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3830" name="papilla" name_original="papillae" src="d0_s17" type="structure">
        <character is_modifier="true" name="size" src="d0_s17" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s17" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o3831" name="side" name_original="sides" src="d0_s17" type="structure" />
      <relation from="o3829" id="r422" name="with" negation="false" src="d0_s17" to="o3830" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 48.</text>
      <biological_entity id="o3829" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character constraint="on sides" constraintid="o3831" is_modifier="false" name="relief" notes="" src="d0_s17" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3832" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine grassy, gravelly, or rocky slopes, ponderosa pine forests, juniper scrub, sagebrush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" modifier="subalpine grassy gravelly or" />
        <character name="habitat" value="ponderosa pine forests" />
        <character name="habitat" value="juniper scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>52.</number>
  <other_name type="common_name">Robinson’s catchfly</other_name>
  <discussion>Silene scaposa is a very distinct species with its subscapose inflorescence, coronalike ring of short petals, and distended fruiting calyx in which the veins to the lobes are markedly broadened and lanceolate. Variation in lobing of the corolla has been the basis for recognizing two varieties: var. scaposa (var. typica C. L. Hitchcock &amp; Maguire), which has two-lobed petals, and var. lobata, which has four-lobed petals. However, these differences appear to be of little significance.</discussion>
  
</bio:treatment>