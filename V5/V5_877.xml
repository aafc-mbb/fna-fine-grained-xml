<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">422</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Oregonium</taxon_name>
    <taxon_name authority="S. Watson" date="1882" rank="species">molestum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 379. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oregonium;species molestum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060402</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="unknown" rank="species">vimineum</taxon_name>
    <taxon_name authority="(S. Watson) S. Stokes" date="unknown" rank="subspecies">molestum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species vimineum;subspecies molestum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect, 4–10 dm, glabrous, greenish to grayish.</text>
      <biological_entity id="o20147" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s0" to="grayish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: aerial flowering-stems erect, 1–4 dm, glabrous.</text>
      <biological_entity id="o20148" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o20149" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal;</text>
      <biological_entity id="o20150" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–6 cm, floccose;</text>
      <biological_entity id="o20151" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade round to reniform, (0.5–) 1–4 × (0.5–) 1–4 cm, densely white-tomentose abaxially, floccose to glabrate and mostly greenish adaxially.</text>
      <biological_entity id="o20152" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s4" to="reniform" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_width" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s4" value="white-tomentose" value_original="white-tomentose" />
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s4" to="glabrate" />
        <character is_modifier="false" modifier="mostly; adaxially" name="coloration" src="d0_s4" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymose, occasionally distally uniparous due to suppression of secondary branches, open, 30–80 × 10–50 cm;</text>
      <biological_entity id="o20153" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymose" value_original="cymose" />
        <character constraint="of secondary branches" constraintid="o20154" is_modifier="false" modifier="occasionally distally" name="architecture" src="d0_s5" value="uniparous" value_original="uniparous" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="open" value_original="open" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s5" to="80" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s5" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o20154" name="branch" name_original="branches" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches straight, not inwardly curved distally, glabrous;</text>
      <biological_entity id="o20155" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not inwardly; distally" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 1–3 × 1–3 mm.</text>
      <biological_entity id="o20156" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles absent.</text>
      <biological_entity id="o20157" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres appressed to branches, cylindric-turbinate, (3.5–) 4–5 (–7) × 2.5–3 (3.5) mm, glabrous;</text>
      <biological_entity id="o20158" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character constraint="to branches" constraintid="o20159" is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="cylindric-turbinate" value_original="cylindric-turbinate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character name="atypical_width" src="d0_s9" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20159" name="branch" name_original="branches" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>teeth 5, erect, 0.2–0.4 mm.</text>
      <biological_entity id="o20160" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 1.5–3 mm;</text>
      <biological_entity id="o20161" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth white to pink, rarely pale-yellow, glabrous;</text>
      <biological_entity id="o20162" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals monomorphic, oblong-obovate;</text>
      <biological_entity id="o20163" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-obovate" value_original="oblong-obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens included, 1–1.5 mm;</text>
      <biological_entity id="o20164" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o20165" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes brown, 3-gonous, 2–2.5 mm, glabrous.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 40.</text>
      <biological_entity id="o20166" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20167" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy flats and slopes, grassland and chaparral communities, oak and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="grassland" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>211.</number>
  <other_name type="common_name">Pineland wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum molestum is infrequent to occasionally common in the San Bernardino (San Bernardino County) and San Jacinto (Riverside County) mountains, and in scattered mountain ranges of San Diego County, California. It is easily confused with the perennial E. nudum var. pauciflorum.</discussion>
  
</bio:treatment>