<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">18</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Tanfani in F. Parlatore" date="unknown" rank="subfamily">Polycarpoideae</taxon_name>
    <taxon_name authority="(Persoon) J. Presl &amp; C. Presl" date="unknown" rank="genus">spergularia</taxon_name>
    <taxon_name authority="(Hornemann ex Chamisso &amp; Schlechtendal) Heynhold" date="1846" rank="species">macrotheca</taxon_name>
    <place_of_publication>
      <publication_title>Alph. Aufz. Gew.</publication_title>
      <place_in_publication>689. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily polycarpoideae;genus spergularia;species macrotheca;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060915</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Hornemann ex Chamisso &amp; Schlechtendal" date="unknown" rank="species">macrotheca</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>1: 53. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species macrotheca;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants strongly perennial with branched, woody base, stout, 5–40 cm, densely stipitate-glandular in inflorescence or throughout.</text>
      <biological_entity id="o19324" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="strongly" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots becoming stout, woody.</text>
      <biological_entity id="o19325" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="becoming" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending to prostrate, usually branched proximally;</text>
      <biological_entity id="o19326" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="prostrate" />
        <character is_modifier="false" modifier="usually; proximally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>main-stem 0.8–3 mm diam. proximally.</text>
      <biological_entity id="o19327" name="main-stem" name_original="main-stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" modifier="proximally" name="diameter" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules conspicuous, dull white to tan, narrowly triangular, 4.5–11 mm, apex long-acuminate;</text>
      <biological_entity id="o19328" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o19329" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dull" value_original="dull" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s4" to="tan" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19330" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear, (0.6–) 1–5.5 cm, fleshy, apex apiculate to spine-tipped;</text>
      <biological_entity id="o19331" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o19332" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="5.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o19333" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="apiculate" name="architecture_or_shape" src="d0_s5" to="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>axillary leaves 1–2+ per cluster.</text>
      <biological_entity id="o19334" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="axillary" id="o19335" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per cluster" from="1" name="quantity" src="d0_s6" to="2" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cymes simple to 3-compound or flowers solitary and axillary.</text>
      <biological_entity id="o19336" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character char_type="range_value" from="simple" name="architecture" src="d0_s7" to="3-compound" />
      </biological_entity>
      <biological_entity id="o19337" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect, divergent, or reflexed in fruit.</text>
      <biological_entity id="o19338" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character constraint="in fruit" constraintid="o19339" is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o19339" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals connate 0.5–1.8 mm proximally, lobes often 3-veined, ovate to lanceolate, 4.5–7 mm, to 8 mm in fruit, margins 0.3–0.7 mm wide, apex blunt to rounded;</text>
      <biological_entity id="o19340" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19341" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character char_type="range_value" from="0.5" from_unit="mm" modifier="proximally" name="some_measurement" src="d0_s9" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19342" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s9" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o19343" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19343" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o19344" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s9" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19345" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s9" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white or pink to rosy, elliptic to obovate, 0.9–1.3 times as long as sepals in flower;</text>
      <biological_entity id="o19346" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19347" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="rosy" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="obovate" />
        <character constraint="in flower" constraintid="o19349" is_modifier="false" name="length" src="d0_s10" value="0.9-1.3 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o19348" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
      <biological_entity id="o19349" name="flower" name_original="flower" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 9–10;</text>
      <biological_entity id="o19350" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19351" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 0.6–3 mm.</text>
      <biological_entity id="o19352" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o19353" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules tan, 4.6–10 mm, 0.8–1.4 times as long as sepals.</text>
      <biological_entity id="o19354" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character char_type="range_value" from="4.6" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character constraint="sepal" constraintid="o19355" is_modifier="false" name="length" src="d0_s13" value="0.8-1.4 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o19355" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds ± redbrown, often with submarginal groove or depression, suborbiculate to pyriform, compressed, (0.6–) 0.7–1.2 mm, smooth to faintly or prominently tuberculate or sculpturing of parallel, wavy lines or of low, rounded mounds, not papillate;</text>
      <biological_entity id="o19356" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s14" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="suborbiculate" name="shape" notes="" src="d0_s14" to="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s14" to="faintly or prominently tuberculate" />
        <character name="relief" src="d0_s14" value="sculpturing of parallel , wavy lines" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s14" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="submarginal" id="o19357" name="groove" name_original="groove" src="d0_s14" type="structure" />
      <biological_entity id="o19358" name="depression" name_original="depression" src="d0_s14" type="structure" />
      <biological_entity id="o19359" name="mound" name_original="mounds" src="d0_s14" type="structure">
        <character is_modifier="true" name="position" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="true" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o19356" id="r2160" modifier="often" name="with" negation="false" src="d0_s14" to="o19357" />
      <relation from="o19356" id="r2161" modifier="often" name="with" negation="false" src="d0_s14" to="o19358" />
      <relation from="o19356" id="r2162" name="part_of" negation="false" src="d0_s14" to="o19359" />
    </statement>
    <statement id="d0_s15">
      <text>wing absent or white to reddish-brown proximally, 0.1–0.3 mm wide, margins irregular.</text>
      <biological_entity id="o19360" name="wing" name_original="wing" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s15" value="white to reddish-brown" value_original="white to reddish-brown" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s15" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19361" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_course" src="d0_s15" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.; including nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="including nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name"> Sticky sand-spurrey</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals pink to rosy; styles 0.6-1.2 mm</description>
      <determination>1a Spergularia macrotheca var. macrotheca</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals white; styles 1.2-3 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Capsules 1.2-1.4 times as long as sepals; styles 1.2-1.9 mm</description>
      <determination>1b Spergularia macrotheca var. leucantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Capsules 0.8-1 times as long as sepals; styles 2-3 mm</description>
      <determination>1c Spergularia macrotheca var. longistyla</determination>
    </key_statement>
  </key>
</bio:treatment>