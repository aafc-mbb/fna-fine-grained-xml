<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">93</other_info_on_meta>
    <other_info_on_meta type="illustration_page">94</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Ehrhart" date="unknown" rank="genus">moenchia</taxon_name>
    <taxon_name authority="(Linnaeus) P. Gaertner" date="1799" rank="species">erecta</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">erecta</taxon_name>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus moenchia;species erecta;subspecies erecta;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060661</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 2.5–19 cm, glabrous, glaucous.</text>
      <biological_entity id="o1305" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s0" to="19" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 5–18 mm.</text>
      <biological_entity id="o1306" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="distance" src="d0_s1" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pedicels 1–7.6 cm.</text>
      <biological_entity id="o1307" name="pedicel" name_original="pedicels" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="7.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petals ca. 1/2–2/3 times as long as sepals.</text>
      <biological_entity id="o1308" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character constraint="sepal" constraintid="o1309" is_modifier="false" name="length" src="d0_s3" value="1/2-2/3 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o1309" name="sepal" name_original="sepals" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Capsules 5–6.5 mm, usually 4/5–1 times as long as sepals.</text>
      <biological_entity id="o1311" name="sepal" name_original="sepals" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>2n = 38 (Europe).</text>
      <biological_entity id="o1310" name="capsule" name_original="capsules" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="6.5" to_unit="mm" />
        <character constraint="sepal" constraintid="o1311" is_modifier="false" name="length" src="d0_s4" value="4/5-1 times as long as sepals" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1312" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lawns, disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lawns" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C.; Calif., Ill., Oreg., S.C., Wash.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <discussion>Moenchia erecta is known in widely scattered areas of western North America; its occurrence in the east is limited. Historical records from the 1830s exist as specimens collected at Baltimore, Maryland, and Philadelphia, Pennsylvania; the species has not been recollected in either place since. The Illinois population, first documented in 1993, is scattered over an acre of a mowed field in a city park.</discussion>
  
</bio:treatment>