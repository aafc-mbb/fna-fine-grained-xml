<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">394</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="mention_page">395</other_info_on_meta>
    <other_info_on_meta type="mention_page">397</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="illustration_page">388</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Torrey in J. C. Ives" date="1861" rank="species">deflexum</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Ives, Rep. Colorado R.</publication_title>
      <place_in_publication>4: 24. 1861</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species deflexum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250060250</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect to spreading, annual, (0.5–) 1–10 (–20) dm, glabrous, occasionally glaucous, greenish to grayish.</text>
      <biological_entity id="o22789" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s0" to="grayish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o22790" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o22791" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, solid or occasionally hollow and fistulose, 0.3–3 (–4) dm, glabrous, occasionally glaucous.</text>
      <biological_entity id="o22792" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o22793" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="4" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal;</text>
      <biological_entity id="o22794" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–7 cm, usually floccose;</text>
      <biological_entity id="o22795" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade cordate to reniform or nearly orbiculate, 1–2.5 (–4) × 2–4 (–5) cm, densely white-tomentose abaxially, less so to floccose or subglabrous and grayish to greenish adaxially, margins entire.</text>
      <biological_entity id="o22796" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s5" to="reniform or nearly orbiculate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s5" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="less" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="subglabrous" value_original="subglabrous" />
        <character char_type="range_value" from="grayish" modifier="adaxially" name="coloration" src="d0_s5" to="greenish" />
      </biological_entity>
      <biological_entity id="o22797" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymose, open to diffuse, flat-topped, spreading, hemispheric or narrowly erect and strict with whiplike branches, 10–90 (–180) × 5–50 cm;</text>
      <biological_entity id="o22798" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="density" src="d0_s6" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" modifier="narrowly" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character constraint="with branches" constraintid="o22799" is_modifier="false" name="course" src="d0_s6" value="strict" value_original="strict" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s6" to="180" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" notes="" src="d0_s6" to="90" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" notes="" src="d0_s6" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22799" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="whip-like" value_original="whiplike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches glabrous, occasionally glaucous;</text>
      <biological_entity id="o22800" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 1–3 × 0.5–1.5 mm.</text>
      <biological_entity id="o22801" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles absent or deflexed, rarely some ± erect distally, straight, slender to stout, 0.1–1.5 cm, glabrous.</text>
      <biological_entity id="o22802" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" modifier="rarely; more or less; distally" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="slender" name="size" src="d0_s9" to="stout" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s9" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres narrowly turbinate to turbinate, 1.5–2.5 (–3) × 1–2.5 mm, glabrous;</text>
      <biological_entity id="o22803" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly turbinate" name="shape" src="d0_s10" to="turbinate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, erect, (0.2–) 0.5–1 mm.</text>
      <biological_entity id="o22804" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1–2.5 mm;</text>
      <biological_entity id="o22805" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth white to pink, with greenish to reddish midribs, becoming pinkish to reddish, glabrous;</text>
      <biological_entity id="o22806" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="pink" />
        <character char_type="range_value" from="pinkish" modifier="becoming" name="coloration" notes="" src="d0_s13" to="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22807" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character char_type="range_value" from="greenish" is_modifier="true" name="coloration" src="d0_s13" to="reddish" />
      </biological_entity>
      <relation from="o22806" id="r2538" name="with" negation="false" src="d0_s13" to="o22807" />
    </statement>
    <statement id="d0_s14">
      <text>tepals dimorphic, those of outer whorl oblong or cordate to ovate, those of inner whorl lanceolate to narrowly ovate;</text>
      <biological_entity id="o22808" name="tepal" name_original="tepals" src="d0_s14" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="cordate" name="shape" src="d0_s14" to="ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s14" to="narrowly ovate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o22809" name="whorl" name_original="whorl" src="d0_s14" type="structure" />
      <biological_entity constraint="inner" id="o22810" name="whorl" name_original="whorl" src="d0_s14" type="structure" />
      <relation from="o22808" id="r2539" name="part_of" negation="false" src="d0_s14" to="o22809" />
      <relation from="o22808" id="r2540" name="part_of" negation="false" src="d0_s14" to="o22810" />
    </statement>
    <statement id="d0_s15">
      <text>stamens included, 1–1.5 mm;</text>
      <biological_entity id="o22811" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments glabrous or sparsely pilose proximally.</text>
      <biological_entity id="o22812" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s16" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes brown to dark-brown, 3-gonous, (1.5–) 2–3 mm, glabrous.</text>
      <biological_entity id="o22813" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s17" to="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Nev., Utah; including Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="including Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>166.</number>
  <other_name type="common_name">Skeleton weed</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Eriogonum deflexum is common and often weedy throughout most of its range. It is an important source of small seed for birds. The reported use of the stem (M. L. Zigmond 1981, as E. insigne) by the Kawaiisu people as a smoking pipe is incorrect; the taxon in point was actually E. deflexum var. baratum. The desert metalmark butterfly (Apodemia mormo deserti) is found in association with E. deflexum.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres (2-)2.5-3 mm, narrowly turbinate; peduncles (0.3-)0.5-1.5 cm; stems fistulose; s and ec California, s Nevada</description>
      <determination>166c Eriogonum deflexum var. baratum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 1.5-2.5 mm, turbinate; peduncles absent or 0.1-0.5 cm; stems not fistulose; Arizona, s and ec California, Nevada, sw New Mexico, w Utah</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Tepals cordate to ovate; Arizona, s Califonia, s Nevada, sw New Mexico, w Utah</description>
      <determination>166a Eriogonum deflexum var. deflexum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Tepals oblong; ec California, Nevada</description>
      <determination>166b Eriogonum deflexum var. nevadense</determination>
    </key_statement>
  </key>
</bio:treatment>