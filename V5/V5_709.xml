<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="mention_page">342</other_info_on_meta>
    <other_info_on_meta type="mention_page">343</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="(Bentham) M. E. Jones" date="1903" rank="variety">polyanthum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>11: 5. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety polyanthum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060566</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">polyanthum</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 12. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species polyanthum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">umbellatum</taxon_name>
    <taxon_name authority="(Bentham) S. Stokes" date="unknown" rank="subspecies">polyanthum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species umbellatum;subspecies polyanthum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, round, rather open, 4–10 × 5–10 dm.</text>
      <biological_entity id="o3833" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="round" value_original="round" />
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character char_type="range_value" from="4" from_unit="dm" name="length" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, 1–2 dm, floccose, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o3834" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o3835" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o3834" id="r423" name="without" negation="false" src="d0_s1" to="o3835" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in rather open, terminal rosettes;</text>
      <biological_entity id="o3836" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="terminal" id="o3837" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="rather" name="architecture" src="d0_s2" value="open" value_original="open" />
      </biological_entity>
      <relation from="o3836" id="r424" name="in" negation="false" src="d0_s2" to="o3837" />
    </statement>
    <statement id="d0_s3">
      <text>blade oblanceolate to narrowly elliptic, 1–3 × 0.3–1 (–1.3) cm, densely white-tomentose abaxially, thinly floccose or glabrous and light green adaxially, margins plane.</text>
      <biological_entity id="o3838" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="narrowly elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1.3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="light green" value_original="light green" />
      </biological_entity>
      <biological_entity id="o3839" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences umbellate or compound-umbellate, branched 1–2 (–3) times;</text>
      <biological_entity id="o3840" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound-umbellate" value_original="compound-umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="" src="d0_s4" value="1-2(-3) times" value_original="1-2(-3) times " />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches thinly floccose or glabrous, occasionally central branch seemingly with a whorl of bracts ca. midlength;</text>
      <biological_entity id="o3841" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3842" name="branch" name_original="branch" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="occasionally" name="position" src="d0_s5" value="central" value_original="central" />
      </biological_entity>
      <biological_entity id="o3843" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o3844" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o3842" id="r425" name="with" negation="false" src="d0_s5" to="o3843" />
      <relation from="o3843" id="r426" name="part_of" negation="false" src="d0_s5" to="o3844" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 2.5–4 mm, floccose, lobes 2–3.5 mm.</text>
      <biological_entity id="o3845" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o3846" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 4–7 mm;</text>
      <biological_entity id="o3847" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth bright-yellow.</text>
      <biological_entity id="o3848" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine flats and slopes, oak and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107jj.</number>
  <other_name type="common_name">American River sulphur flower</other_name>
  <discussion>The inflorescences of var. polyanthum are commonly compound-umbellate, but plants with reduced yet bracteated inflorescences do occur. Those with a reduced inflorescence technically consist of a long (6–10 cm), central, bractless peduncle and two lateral branches (3–4 cm), with each of the latter bearing a peduncle (3–6 cm). Such branches seem to have a whorl of leaflike bracts, but actually the bracts are positioned between the branch and involucres (technically at the base of the peduncle) and thus are like other members of the genus.</discussion>
  <discussion>The name var. polyanthum has been misapplied in California to plants here attributed to var. modocense and var. dumosum.</discussion>
  
</bio:treatment>