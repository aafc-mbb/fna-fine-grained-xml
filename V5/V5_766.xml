<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">371</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="1813" rank="species">flavum</taxon_name>
    <taxon_name authority="Reveal" date="1968" rank="variety">aquilinum</taxon_name>
    <place_of_publication>
      <publication_title>Ark. Bot., n. s.</publication_title>
      <place_in_publication>7: 46. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;species flavum;variety aquilinum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060287</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tight, compact mats, 3–10 dm wide.</text>
      <biological_entity id="o23710" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement_or_density" src="d0_s0" value="tight" value_original="tight" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o23711" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems mostly erect, 0.2–0.8 dm.</text>
      <biological_entity id="o23712" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s1" to="0.8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades narrowly elliptic, 1–2 (–3.5) × 0.4–1 cm, densely tomentose abaxially, floccose and greenish adaxially.</text>
      <biological_entity id="o23713" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s2" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences umbellate;</text>
      <biological_entity id="o23714" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="umbellate" value_original="umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches 0.5–2 cm.</text>
      <biological_entity id="o23715" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres usually turbinate-campanulate, (4–) 5–7 (–8) mm.</text>
      <biological_entity id="o23716" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 3–5 (–6) mm, including 0.2–0.3 mm stipelike base;</text>
      <biological_entity id="o23717" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23718" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s6" to="0.3" to_unit="mm" />
        <character is_modifier="true" name="shape" src="d0_s6" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <relation from="o23717" id="r2631" name="including" negation="false" src="d0_s6" to="o23718" />
    </statement>
    <statement id="d0_s7">
      <text>perianth bright-yellow.</text>
      <biological_entity id="o23719" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed bluffs, mixed grassland communities, willow thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="exposed bluffs" />
        <character name="habitat" value="mixed grassland communities" />
        <character name="habitat" value="willow thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>133b.</number>
  <other_name type="common_name">Yukon wild buckwheat</other_name>
  <discussion>Variety aquilinum is rare and isolated, known only from the Eagle area of extreme east-central Alaska (including the Kathul Mountains, Southeast Fairbanks County) and from the Aishihik Lake area in the Yukon Territory. It is closely related to and marginally distinct from var. flavum, its isolation the result of Pleistocene glaciation.</discussion>
  
</bio:treatment>