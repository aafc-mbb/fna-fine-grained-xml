<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="illustration_page">65</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(Nuttall) W. A. Weber" date="1981" rank="species">hookeri</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>33: 326. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species hookeri;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060148</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">hookeri</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 178. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species hookeri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely or loosely matted, green, not glaucous, somewhat woody at base.</text>
      <biological_entity id="o188" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character constraint="at base" constraintid="o189" is_modifier="false" modifier="somewhat" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o189" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 1–15 (–20) cm, scabrid-puberulent.</text>
      <biological_entity id="o190" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="scabrid-puberulent" value_original="scabrid-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves persistent;</text>
      <biological_entity id="o191" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o192" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves in 1–4 pairs, usually little overlapping, often larger than basal leaves;</text>
      <biological_entity id="o193" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o194" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" notes="" src="d0_s3" value="overlapping" value_original="overlapping" />
        <character constraint="than basal leaves" constraintid="o196" is_modifier="false" name="size" src="d0_s3" value="often larger" value_original="often larger" />
      </biological_entity>
      <biological_entity id="o195" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity constraint="basal" id="o196" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o194" id="r20" name="in" negation="false" src="d0_s3" to="o195" />
    </statement>
    <statement id="d0_s4">
      <text>basal blades straight to arcuate-spreading, subulate to needlelike, 0.3–4 cm × 0.5–1.5 mm, flexible or rigid, herbaceous, apex spinose, glabrous, often glaucous.</text>
      <biological_entity id="o197" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o198" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="arcuate-spreading" value_original="arcuate-spreading" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s4" to="needlelike" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s4" value="pliable" value_original="flexible" />
        <character is_modifier="false" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o199" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 3–30+-flowered, congested, capitate or subcapitate cymes.</text>
      <biological_entity id="o200" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-30+-flowered" value_original="3-30+-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="subcapitate" value_original="subcapitate" />
      </biological_entity>
      <biological_entity id="o201" name="cyme" name_original="cymes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.2–2 mm, scabrid-puberulent.</text>
      <biological_entity id="o202" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrid-puberulent" value_original="scabrid-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 1–3-veined, often obscurely so, linear-lanceolate to lanceolate, (5–) 6–10 mm, not enlarging in fruit, margins narrow, apex narrowly acute or acuminate, glabrous or pubescent;</text>
      <biological_entity id="o203" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o204" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-3-veined" value_original="1-3-veined" />
        <character char_type="range_value" from="linear-lanceolate" modifier="often obscurely; obscurely" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character constraint="in fruit" constraintid="o205" is_modifier="false" modifier="not" name="size" src="d0_s7" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o205" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o206" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o207" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, oblanceolate, 4.5–8.5 mm, ± equaling sepals, apex rounded to obtuse;</text>
      <biological_entity id="o208" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o209" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o210" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o211" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectaries as lateral and abaxial mounds with transverse groove at base of filaments opposite sepals, 0.2–0.3 mm.</text>
      <biological_entity id="o212" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o213" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral and abaxial" id="o214" name="mound" name_original="mounds" src="d0_s9" type="structure" />
      <biological_entity id="o215" name="groove" name_original="groove" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s9" value="transverse" value_original="transverse" />
      </biological_entity>
      <biological_entity id="o216" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o217" name="filament" name_original="filaments" src="d0_s9" type="structure" />
      <biological_entity id="o218" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o213" id="r21" name="as" negation="false" src="d0_s9" to="o214" />
      <relation from="o214" id="r22" name="with" negation="false" src="d0_s9" to="o215" />
      <relation from="o215" id="r23" name="at" negation="false" src="d0_s9" to="o216" />
      <relation from="o216" id="r24" name="part_of" negation="false" src="d0_s9" to="o217" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules to 4 mm, glabrous.</text>
      <biological_entity id="o219" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds black, ellipsoid-oblong to pyriform with hilar notch, 1.8–2 mm, tuberculate;</text>
      <biological_entity id="o220" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character char_type="range_value" constraint="with hilar, notch" constraintid="o221, o222" from="ellipsoid-oblong" name="shape" src="d0_s11" to="pyriform" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o221" name="hilar" name_original="hilar" src="d0_s11" type="structure" />
      <biological_entity id="o222" name="notch" name_original="notch" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>tubercles rounded, elongate.</text>
      <biological_entity id="o223" name="tubercle" name_original="tubercles" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., Mont., N.Mex., Nebr., Nev., Okla., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Hooker’s sandwort</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaf blades 0.3-1.5 cm, straight or recurved, rigid; sepals 5-8(-9) mm</description>
      <determination>9a Eremogone hookeri var. hookeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaf blades 2-4 cm, straight, rigid or flexible; sepals (7-)8-10 mm</description>
      <determination>9b Eremogone hookeri var. pinetorum</determination>
    </key_statement>
  </key>
</bio:treatment>