<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">526</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="mention_page">527</other_info_on_meta>
    <other_info_on_meta type="mention_page">528</other_info_on_meta>
    <other_info_on_meta type="mention_page">530</other_info_on_meta>
    <other_info_on_meta type="illustration_page">525</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rumex Linnaeus Sect.. Rumex" date="unknown" rank="section">Rumex</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">pulcher</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 336. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section rumex;species pulcher;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242417185</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous or distinctly papillose especially on veins of leaf-blades abaxially, with fusiform, vertical rootstock.</text>
      <biological_entity id="o16590" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="on veins" constraintid="o16591" is_modifier="false" modifier="distinctly" name="relief" src="d0_s0" value="papillose" value_original="papillose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16591" name="vein" name_original="veins" src="d0_s0" type="structure" />
      <biological_entity id="o16592" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure" />
      <biological_entity id="o16593" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="fusiform" value_original="fusiform" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o16591" id="r1842" name="part_of" negation="false" src="d0_s0" to="o16592" />
      <relation from="o16590" id="r1843" name="with" negation="false" src="d0_s0" to="o16593" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, often flexuous in distal part, branched in distal 2/3, occasionally almost from base, 20–60 (–70) cm.</text>
      <biological_entity id="o16594" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="in distal part" constraintid="o16595" is_modifier="false" modifier="often" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
        <character constraint="in distal 2/3" constraintid="o16596" is_modifier="false" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s1" to="70" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16595" name="part" name_original="part" src="d0_s1" type="structure" />
      <biological_entity constraint="distal" id="o16596" name="2/3" name_original="2/3" src="d0_s1" type="structure" />
      <biological_entity id="o16597" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o16594" id="r1844" modifier="occasionally almost; almost" name="from" negation="false" src="d0_s1" to="o16597" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: ocrea deciduous or partially persistent at maturity;</text>
      <biological_entity id="o16598" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16599" name="ocreum" name_original="ocrea" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character constraint="at maturity" is_modifier="false" modifier="partially" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblong to ovate-oblong, sometimes broadly lanceolate or panduriform, contracted near middle or proximally, 4–10 (–15) × (2–) 3–5 cm, less than 4 times as long as wide, base normally truncate or weakly cordate, occasionally rounded, margins entire, flat or undulate, rarely slightly crisped, apex obtuse or subacute.</text>
      <biological_entity id="o16600" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16601" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="ovate-oblong" />
        <character is_modifier="false" modifier="sometimes broadly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="panduriform" value_original="panduriform" />
        <character constraint="near " constraintid="o16603" is_modifier="false" name="condition_or_size" src="d0_s3" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="occasionally" name="shape" notes="" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="middle" id="o16602" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16603" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="middle" id="o16604" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16605" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_width" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" constraint="near " constraintid="o16607" from="3" from_unit="cm" name="width" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="middle" id="o16606" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16607" name="base" name_original="base" src="d0_s3" type="structure">
        <character constraint="near " constraintid="o16609" is_modifier="false" name="l_w_ratio" src="d0_s3" value="0-4" value_original="0-4" />
      </biological_entity>
      <biological_entity constraint="middle" id="o16608" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16609" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="normally" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o16610" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="rarely slightly" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o16611" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subacute" value_original="subacute" />
      </biological_entity>
      <relation from="o16603" id="r1845" name="near" negation="false" src="d0_s3" to="o16604" />
      <relation from="o16603" id="r1846" name="near" negation="false" src="d0_s3" to="o16605" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, occupying distal 2/3 of stem or more, usually lax and interrupted, broadly paniculate, branches usually divaricately spreading, forming angle of 60–90° with 1st-order stem.</text>
      <biological_entity id="o16612" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s4" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="broadly" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o16613" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o16614" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually divaricately" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o16615" name="angle" name_original="angle" src="d0_s4" type="structure" />
      <biological_entity id="o16616" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <relation from="o16612" id="r1847" name="occupying" negation="false" src="d0_s4" to="o16613" />
      <relation from="o16614" id="r1848" name="forming" negation="false" src="d0_s4" to="o16615" />
      <relation from="o16614" id="r1849" modifier="60-90°" name="with" negation="false" src="d0_s4" to="o16616" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels articulated in proximal 1/3 or occasionally near middle, thickened, not filiform, 2–5 (–6) mm, articulation distinctly swollen.</text>
      <biological_entity id="o16617" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="in " constraintid="o16619" is_modifier="false" name="architecture" src="d0_s5" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s5" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16618" name="1/3" name_original="1/3" src="d0_s5" type="structure" />
      <biological_entity id="o16619" name="middle" name_original="middle" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o16620" name="1/3" name_original="1/3" src="d0_s5" type="structure" />
      <biological_entity id="o16621" name="middle" name_original="middle" src="d0_s5" type="structure" />
      <biological_entity id="o16622" name="articulation" name_original="articulation" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s5" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o16619" id="r1850" name="in" negation="false" src="d0_s5" to="o16620" />
      <relation from="o16619" id="r1851" name="in" negation="false" src="d0_s5" to="o16621" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 10–20 in rather dense whorls;</text>
      <biological_entity id="o16623" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o16624" from="10" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o16624" name="whorl" name_original="whorls" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="rather" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner tepals ovate-triangular, deltoid, or oblong-deltoid, 3–6 × 2–3 mm (excluding teeth), normally ca. 1.5 times as long as wide, base truncate, margins usually distinctly dentate, rarely subentire, apex obtuse to subacute, straight, teeth 2–5 (–9), normally on margins at each side, narrowly triangular, 0.3–2.5 mm, longer or shorter than width of inner tepals;</text>
      <biological_entity constraint="inner" id="o16625" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="deltoid" value_original="deltoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong-deltoid" value_original="oblong-deltoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="deltoid" value_original="deltoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong-deltoid" value_original="oblong-deltoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s7" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o16626" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o16627" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually distinctly" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o16628" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="subacute" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o16629" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="9" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s7" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="than width of inner tepals" constraintid="o16632" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="longer or shorter" value_original="longer or shorter" />
      </biological_entity>
      <biological_entity id="o16630" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o16631" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity constraint="inner" id="o16632" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="true" name="character" src="d0_s7" value="width" value_original="width" />
      </biological_entity>
      <relation from="o16629" id="r1852" modifier="normally" name="on" negation="false" src="d0_s7" to="o16630" />
      <relation from="o16630" id="r1853" name="at" negation="false" src="d0_s7" to="o16631" />
    </statement>
    <statement id="d0_s8">
      <text>tubercles (1–) 3, equal or unequal, usually verrucose (warty).</text>
      <biological_entity id="o16633" name="tubercle" name_original="tubercles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s8" value="verrucose" value_original="verrucose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes dark reddish-brown to almost black, 2–2.8 × 1.3–2 mm. 2n = 20.</text>
      <biological_entity id="o16634" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark reddish-brown to almost" value_original="dark reddish-brown to almost" />
        <character is_modifier="false" modifier="almost" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16635" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, roadsides, shores, fields, meadows, moist to dry habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="moist to dry habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ariz., Ark., Calif., Fla., Ga., Ky., La., Md., Mass., Miss., Mo., Nev., N.J., N.Y., N.C., Okla., Oreg., Pa., R.I., S.C., Tenn., Tex., Va., W.Va.; s, w Europe; sw Asia; n Africa; introduced elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="s" establishment_means="native" />
        <character name="distribution" value="w Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="elsewhere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>53.</number>
  <other_name type="common_name">Fiddle dock</other_name>
  <discussion>Rumex pulcher is an extremely polymorphic species consisting of five or six more or less distinct subspecies (K. H. Rechinger 1949, 1964). Three of these were reported by Rechinger (1937) from North America: subsp. pulcher; subsp. woodsii (De Not.) Arcangeli [= Rumex divaricatus Linnaeus]; and subsp. anodontus (Haussknecht) Rechinger f. Judging from herbarium specimens, subsp. woodsii seems to be the most common. However, J. E. Dawson (1979) noted that many North American specimens cannot easily be assigned to any subspecies.</discussion>
  <discussion>Some records require confirmation, especially from the midwestern states, since Rumex pulcher often is confused with other species with dentate inner tepals. Records from Colorado (H. D. Harrington 1954) belong to R. stenophyllus (W. A. Weber and R. C. Wittmann 1992).</discussion>
  
</bio:treatment>