<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">581</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1754" rank="genus">persicaria</taxon_name>
    <taxon_name authority="(M. Král) S. P. Hong" date="1993" rank="section">Rubrivena</taxon_name>
    <taxon_name authority="Greuter &amp; Burdet" date="1989" rank="species">wallichii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">wallichii</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus persicaria;section rubrivena;species wallichii;variety wallichii;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250060704</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, sometimes suffrutescent, 7–12 (–25) dm;</text>
      <biological_entity id="o15406" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="suffrutescent" value_original="suffrutescent" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="dm" />
        <character char_type="range_value" from="7" from_unit="dm" name="some_measurement" src="d0_s0" to="12" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots not also arising from proximal nodes;</text>
      <biological_entity id="o15407" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="from proximal nodes" constraintid="o15408" is_modifier="false" name="orientation" src="d0_s1" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15408" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>rhizomes present.</text>
      <biological_entity id="o15409" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems ribbed, glabrous or densely pubescent.</text>
      <biological_entity id="o15410" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: ocrea reddish-brown, cylindric, 10–40 mm, base inflated, surface glabrous or densely pubescent;</text>
      <biological_entity id="o15411" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o15412" name="ocreum" name_original="ocrea" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15413" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o15414" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.3–2 (–3) cm, glabrous or densely pubescent;</text>
      <biological_entity id="o15415" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o15416" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade (7.5–) 9–22 (–27) × 2.8–7.8 cm, base cordate to truncate, margins ciliate, apex acuminate to caudate-acuminate, faces pubescent abaxially, glabrous or pubescent adaxially.</text>
      <biological_entity id="o15417" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o15418" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="7.5" from_unit="cm" name="atypical_length" src="d0_s6" to="9" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="27" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s6" to="22" to_unit="cm" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="width" src="d0_s6" to="7.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15419" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate to truncate" value_original="cordate to truncate" />
      </biological_entity>
      <biological_entity id="o15420" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15421" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s6" to="caudate-acuminate" />
      </biological_entity>
      <biological_entity id="o15422" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 40–110 × 10–55 mm;</text>
      <biological_entity id="o15423" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s7" to="110" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s7" to="55" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncle 10–80 mm, glabrous or pubescent;</text>
      <biological_entity id="o15424" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="80" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ocreolae not overlapping, margins eciliate.</text>
      <biological_entity id="o15425" name="ocreola" name_original="ocreolae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o15426" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="eciliate" value_original="eciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels ascending to spreading, 1–4 mm, glabrous.</text>
      <biological_entity id="o15427" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 3–9 per ocreate fascicle;</text>
      <biological_entity id="o15428" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per fascicle" constraintid="o15429" from="3" name="quantity" src="d0_s11" to="9" />
      </biological_entity>
      <biological_entity id="o15429" name="fascicle" name_original="fascicle" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>perianth white or pinkish, often with scattered reddish glands, glabrous, nonaccrescent;</text>
      <biological_entity id="o15430" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="size" src="d0_s12" value="nonaccrescent" value_original="nonaccrescent" />
      </biological_entity>
      <biological_entity id="o15431" name="gland" name_original="glands" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
      </biological_entity>
      <relation from="o15430" id="r1699" modifier="often" name="with" negation="false" src="d0_s12" to="o15431" />
    </statement>
    <statement id="d0_s13">
      <text>tepals oblong to obovate, dimorphic, outer 3 larger than inner 2, 2.2–3.8 mm, margins entire, apex obtuse to rounded;</text>
      <biological_entity id="o15432" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s13" to="obovate" />
        <character is_modifier="false" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="outer" id="o15433" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character constraint="than inner tepals" constraintid="o15434" is_modifier="false" name="size" src="d0_s13" value="larger" value_original="larger" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o15434" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o15435" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15436" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s13" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments inserted at bases of tepals, distinct, glabrous;</text>
      <biological_entity id="o15437" name="filament" name_original="filaments" src="d0_s14" type="structure" constraint="tepal" constraint_original="tepal; tepal">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15438" name="base" name_original="bases" src="d0_s14" type="structure" />
      <biological_entity id="o15439" name="tepal" name_original="tepals" src="d0_s14" type="structure" />
      <relation from="o15437" id="r1700" name="inserted at" negation="false" src="d0_s14" to="o15438" />
      <relation from="o15437" id="r1701" name="part_of" negation="false" src="d0_s14" to="o15439" />
    </statement>
    <statement id="d0_s15">
      <text>anthers red to purple, ovate;</text>
      <biological_entity id="o15440" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s15" to="purple" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles distinct.</text>
      <biological_entity id="o15441" name="style" name_original="styles" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes included, brown, 3-gonous, 2.1–2.5 × 1.3–1.8 mm, dull, minutely roughened.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 22.</text>
      <biological_entity id="o15442" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="included" value_original="included" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="length" src="d0_s17" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s17" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="minutely" name="relief_or_texture" src="d0_s17" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15443" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist disturbed places, marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist disturbed places" />
        <character name="habitat" value="marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; St. Pierre and Miquelon; B.C.; Calif., Mass., Oreg.; Asia (Bhutan, China, India, Nepal, Tibet).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="Asia (Bhutan)" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (India)" establishment_means="native" />
        <character name="distribution" value="Asia (Nepal)" establishment_means="native" />
        <character name="distribution" value="Asia (Tibet)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10a.</number>
  
</bio:treatment>