<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Bentham" date="1836" rank="species">strictum</taxon_name>
    <taxon_name authority="(A. Gray) Reveal" date="1978" rank="variety">greenei</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>40: 467. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species strictum;variety greenei;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060510</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">greenei</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>12: 83. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species greenei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="unknown" rank="species">niveum</taxon_name>
    <taxon_name authority="(A. Gray) S. Stokes" date="unknown" rank="variety">greenei</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species niveum;variety greenei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants dense mats, 2–10 dm wide.</text>
      <biological_entity id="o18486" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="width" notes="" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18487" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades ovate, 0.5–1 cm, densely white-lanate to tomentose on both surfaces.</text>
      <biological_entity id="o18488" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="distance" src="d0_s1" to="1" to_unit="cm" />
        <character char_type="range_value" constraint="on surfaces" constraintid="o18489" from="densely white-lanate" name="pubescence" src="d0_s1" to="tomentose" />
      </biological_entity>
      <biological_entity id="o18489" name="surface" name_original="surfaces" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 3–5 cm;</text>
      <biological_entity id="o18490" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches tomentose.</text>
      <biological_entity id="o18491" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 4–6 × 2–3 mm, tomentose.</text>
      <biological_entity id="o18492" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 4–5 mm;</text>
      <biological_entity id="o18493" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth white.</text>
      <biological_entity id="o18494" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, typically serpentine flats, slopes, and ridges, sagebrush communities, montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="serpentine flats" modifier="typically" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="conifer woodlands" modifier="montane" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>96b.</number>
  <other_name type="common_name">Greene’s wild buckwheat</other_name>
  <discussion>Variety greenei is restricted to the North Coast Ranges of northwestern California (Humboldt, Mendocino, Siskiyou, Tehama, and Trinity counties). In the herbarium, it is occasionally difficult to distinguish from nonserpentine populations of var. proliferum. The variety is worthy of cultivation.</discussion>
  
</bio:treatment>