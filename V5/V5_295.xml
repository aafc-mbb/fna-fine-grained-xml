<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">144</other_info_on_meta>
    <other_info_on_meta type="mention_page">141</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sagina</taxon_name>
    <taxon_name authority="Lange in H. J. Rink" date="1857" rank="species">caespitosa</taxon_name>
    <place_of_publication>
      <publication_title>in H. J. Rink, Grønland</publication_title>
      <place_in_publication>2: 133. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus sagina;species caespitosa;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060809</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sagina</taxon_name>
    <taxon_name authority="(Lindblom) Fries" date="unknown" rank="species">nivalis</taxon_name>
    <taxon_name authority="(Lange) B. Boivin" date="unknown" rank="variety">caespitosa</taxon_name>
    <taxon_hierarchy>genus Sagina;species nivalis;variety caespitosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spergella</taxon_name>
    <taxon_name authority="(Lange) Á. Löve &amp; D. Löve" date="unknown" rank="species">caespitosa</taxon_name>
    <taxon_hierarchy>genus Spergella;species caespitosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose, forming small mats or cushions, glandular-pubescent or glabrous.</text>
      <biological_entity id="o19968" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19969" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o19970" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
      </biological_entity>
      <relation from="o19968" id="r2227" name="forming" negation="false" src="d0_s0" to="o19969" />
      <relation from="o19968" id="r2228" name="forming" negation="false" src="d0_s0" to="o19970" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to spreading, frequently purple-tinged, many-branched, not filiform.</text>
      <biological_entity id="o19971" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" modifier="frequently" name="coloration" src="d0_s1" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="many-branched" value_original="many-branched" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: axillary fascicles absent;</text>
      <biological_entity id="o19972" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal not in primary rosettes, secondary rosettes usually present, blade linear to linear-subulate, 2–13 mm, not fleshy, apex apiculate to acute, glabrous;</text>
      <biological_entity id="o19973" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o19974" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="primary" id="o19975" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <biological_entity constraint="secondary" id="o19976" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o19977" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o19978" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="apiculate" name="shape" src="d0_s3" to="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o19974" id="r2229" name="in" negation="false" src="d0_s3" to="o19975" />
    </statement>
    <statement id="d0_s4">
      <text>cauline connate basally, forming shallow, often purplish, scarious cup, blade with frequently conspicuous midvein, subulate, 3–9 (–12) mm, becoming shorter toward apex, not fleshy, apex apiculate to acute, glabrous.</text>
      <biological_entity id="o19979" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o19980" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s4" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o19981" name="cup" name_original="cup" src="d0_s4" type="structure">
        <character is_modifier="true" name="depth" src="d0_s4" value="shallow" value_original="shallow" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s4" value="purplish" value_original="purplish" />
        <character is_modifier="true" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o19982" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
        <character constraint="toward apex" constraintid="o19984" is_modifier="false" modifier="becoming" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="not" name="texture" notes="" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o19983" name="midvein" name_original="midvein" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="frequently" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o19984" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o19985" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="apiculate" name="shape" src="d0_s4" to="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o19980" id="r2230" name="forming" negation="false" src="d0_s4" to="o19981" />
      <relation from="o19982" id="r2231" name="with" negation="false" src="d0_s4" to="o19983" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels filiform, glandular-pubescent, rarely glabrous.</text>
      <biological_entity id="o19986" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers terminal, 5-merous or 4-merous and 5-merous;</text>
      <biological_entity id="o19987" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-merous" value_original="5-merous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="4-merous" value_original="4-merous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>calyx base glandular-pubescent or glabrous;</text>
      <biological_entity constraint="calyx" id="o19988" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals broadly ovate to lanceolate, 2–2.5 mm, hyaline margins usually purple-tinged, at least at apex, apex obtuse to somewhat acute, glandular-pubescent or glabrous, remaining appressed after capsule dehiscence;</text>
      <biological_entity id="o19989" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19990" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity id="o19991" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o19992" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse to somewhat" value_original="obtuse to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19993" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o19990" id="r2232" modifier="at-least" name="at" negation="false" src="d0_s8" to="o19991" />
      <relation from="o19992" id="r2233" name="remaining" negation="false" src="d0_s8" to="o19993" />
    </statement>
    <statement id="d0_s9">
      <text>petals elliptic to narrowly obovate, 2.5–3 mm, longer than or seldom equaling sepals;</text>
      <biological_entity id="o19994" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="narrowly obovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character constraint="than or seldom equaling sepals" constraintid="o19995" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o19995" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="seldom" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 8 or 10.</text>
      <biological_entity id="o19996" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="8" value_original="8" />
        <character name="quantity" src="d0_s10" unit="or" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 3–3.5 mm, exceeding sepals, dehiscing to base.</text>
      <biological_entity id="o19997" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o19998" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o19999" name="base" name_original="base" src="d0_s11" type="structure" />
      <relation from="o19997" id="r2234" name="exceeding" negation="false" src="d0_s11" to="o19998" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds brown, obliquely triangular with abaxial groove, 0.5 mm, lateral surfaces frequently with elongate ridges, abaxial surface appearing smooth to pebbled.</text>
      <biological_entity id="o20000" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character constraint="with abaxial groove" constraintid="o20001" is_modifier="false" modifier="obliquely" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
        <character name="some_measurement" notes="" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20001" name="groove" name_original="groove" src="d0_s12" type="structure" />
      <biological_entity constraint="lateral" id="o20002" name="surface" name_original="surfaces" src="d0_s12" type="structure" />
      <biological_entity id="o20003" name="ridge" name_original="ridges" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20004" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s12" to="pebbled" />
      </biological_entity>
      <relation from="o20002" id="r2235" name="with" negation="false" src="d0_s12" to="o20003" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid-late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet sands and gravels of shorelines and stream margins, wet mossy places, dry rocky barrens, gravelly hillocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet sands" constraint="of shorelines and stream margins , wet mossy places ," />
        <character name="habitat" value="gravels" constraint="of shorelines and stream margins , wet mossy places ," />
        <character name="habitat" value="shorelines" />
        <character name="habitat" value="stream margins" />
        <character name="habitat" value="wet mossy places" />
        <character name="habitat" value="dry rocky barrens" />
        <character name="habitat" value="gravelly hillocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Man., Nfld. and Labr., Nunavut, Que.; arctic Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" value="arctic Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Sagine cespiteuse</other_name>
  
</bio:treatment>