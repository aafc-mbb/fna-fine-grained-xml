<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">378</other_info_on_meta>
    <other_info_on_meta type="mention_page">376</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(H. Gross) Reveal" date="1967" rank="subgenus">Pterogonum</taxon_name>
    <taxon_name authority="J. M. Coulter" date="1890" rank="species">nealleyi</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>1: 48. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus pterogonum;species nealleyi;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060408</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, polycarpic, 5–12 dm, glabrous;</text>
      <biological_entity id="o2960" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polycarpic" value_original="polycarpic" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="12" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot not chambered.</text>
      <biological_entity id="o2961" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="chambered" value_original="chambered" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems: caudex compact;</text>
      <biological_entity id="o2962" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o2963" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems usually 1, not fistulose, 2–4.5 (–5) dm, glabrous.</text>
      <biological_entity id="o2964" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o2965" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s3" to="4.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o2966" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal petiolate, petiole 1–2.5 cm, strigose, blade oblanceolate to spatulate, 4–8 × 0.5–1.6 cm, strigose and grayish on both surfaces;</text>
      <biological_entity constraint="basal" id="o2967" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o2968" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o2969" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="spatulate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1.6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character constraint="on surfaces" constraintid="o2970" is_modifier="false" name="coloration" src="d0_s5" value="grayish" value_original="grayish" />
      </biological_entity>
      <biological_entity id="o2970" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>cauline blade absent or sessile, oblanceolate, (0.5–) 1–4 × (0.2–) 0.3–0.8 cm, similar to basal blade.</text>
      <biological_entity constraint="cauline" id="o2971" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s6" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s6" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2972" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <relation from="o2971" id="r337" name="to" negation="false" src="d0_s6" to="o2972" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 2–7 dm;</text>
      <biological_entity id="o2973" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s7" to="7" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches glabrous;</text>
      <biological_entity id="o2974" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts scalelike, 0.7–2.5 × 0.5–2 mm.</text>
      <biological_entity id="o2975" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s9" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles erect, straight or slightly curving upward, 1–8 cm, glabrous.</text>
      <biological_entity id="o2976" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s10" value="curving" value_original="curving" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="8" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres turbinate-campanulate to campanulate, 2–3 × 2–4 mm, glabrous;</text>
      <biological_entity id="o2977" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character char_type="range_value" from="turbinate-campanulate" name="shape" src="d0_s11" to="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>teeth 0.5–1 mm.</text>
      <biological_entity id="o2978" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 1.5–2 mm at anthesis, 2.5–3 mm in fruit;</text>
      <biological_entity id="o2979" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o2980" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2980" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>perianth white to greenish white at anthesis, reddish in fruit, glabrous or sparsely strigose abaxially;</text>
      <biological_entity id="o2981" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="white" name="coloration" src="d0_s14" to="greenish white" />
        <character constraint="in fruit" constraintid="o2982" is_modifier="false" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="pubescence" src="d0_s14" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o2982" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>tepals narrowly elliptic to oblong;</text>
      <biological_entity id="o2983" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s15" to="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 2–3 mm;</text>
      <biological_entity id="o2984" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o2985" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes greenish brown to reddish-brown, 4–6 mm, 3-winged along distal 1/3, nearly beakless, strigose.</text>
      <biological_entity constraint="distal" id="o2987" name="1/3" name_original="1/3" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>2n = 40.</text>
      <biological_entity id="o2986" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character char_type="range_value" from="greenish brown" name="coloration" src="d0_s18" to="reddish-brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s18" to="6" to_unit="mm" />
        <character constraint="along distal 1/3" constraintid="o2987" is_modifier="false" name="architecture" src="d0_s18" value="3-winged" value_original="3-winged" />
        <character is_modifier="false" modifier="nearly" name="architecture" notes="" src="d0_s18" value="beakless" value_original="beakless" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2988" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous flats and slopes, mixed grassland, saltbush, and creosote bush communities, oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="flats" modifier="calcareous" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="creosote bush communities" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>144.</number>
  <other_name type="common_name">Irion County wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Historically, Eriogonum nealleyi occurred in Coke, Howard, Irion, and Sterling counties of southwestern Texas. Recent collections are from Irion and Sterling counties.</discussion>
  
</bio:treatment>