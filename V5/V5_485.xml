<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">259</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">corymbosum</taxon_name>
    <taxon_name authority="(M. E. Jones) Reveal" date="1983" rank="variety">aureum</taxon_name>
    <place_of_publication>
      <publication_title>Taxon</publication_title>
      <place_in_publication>32: 293. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species corymbosum;variety aureum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060234</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">aureum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 5: 718. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species aureum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">microthecum</taxon_name>
    <taxon_name authority="(M. E. Jones) S. Stokes" date="unknown" rank="subspecies">aureum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species microthecum;subspecies aureum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, 2–6 (–8) × 3–8 (–12) dm.</text>
      <biological_entity id="o27850" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="12" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="12" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline 1/2 or more length of flowering-stem;</text>
      <biological_entity id="o27852" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character name="quantity" src="d0_s1" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o27853" name="flowering-stem" name_original="flowering-stem" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.5–0.8 cm;</text>
      <biological_entity id="o27854" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually elliptic, 2–4 × 1–1.5 cm, densely tomentose abaxially, usually glabrous and green adaxially.</text>
      <biological_entity id="o27855" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 3–10 cm;</text>
      <biological_entity id="o27856" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches glabrous or nearly so.</text>
      <biological_entity id="o27857" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s5" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 1–2 × 1–1.5 (–2) mm.</text>
      <biological_entity id="o27858" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 1.5–2.5 mm;</text>
      <biological_entity id="o27859" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth yellow to pale-yellow, rarely nearly white, glabrous.</text>
      <biological_entity id="o27860" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="pale-yellow" />
        <character is_modifier="false" modifier="rarely nearly" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly slopes, creosote and blackbrush communities, juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="creosote" />
        <character name="habitat" value="blackbrush communities" />
        <character name="habitat" value="woodlands" modifier="juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16h.</number>
  <other_name type="common_name">Golden wild buckwheat</other_name>
  <discussion>Variety aureum is known only from Shivwits Hill near Castle Cliffs in Washington County. This rare expression is probably the result of past introgression with the no longer sympatric herbaceous perennial Eriogonum thompsoniae.</discussion>
  
</bio:treatment>