<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">106</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">stellaria</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">holostea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 422. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus stellaria;species holostea;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">220012926</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alsine</taxon_name>
    <taxon_name authority="(Linnaeus) Britton" date="unknown" rank="species">holostea</taxon_name>
    <taxon_hierarchy>genus Alsine;species holostea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, scrambling to ascending, from slender, creeping rhizomes.</text>
      <biological_entity id="o2122" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="scrambling" value_original="scrambling" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2123" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
      <relation from="o2122" id="r236" name="from" negation="false" src="d0_s0" to="o2123" />
    </statement>
    <statement id="d0_s1">
      <text>Stems branched distally, 4-angled, 15–60 cm, glabrous or hispid-puberulent distally.</text>
      <biological_entity id="o2124" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="hispid-puberulent" value_original="hispid-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile;</text>
      <biological_entity id="o2125" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly lanceolate, widest near base, 4–8 cm × 2–10 mm, somewhat coriaceous, base round and clasping, margins and abaxial midrib very rough, apex narrowly and sharply acuminate, scabrid, otherwise glabrous, slightly glaucous.</text>
      <biological_entity id="o2126" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character constraint="near base" constraintid="o2127" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" notes="" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o2127" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2128" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="round" value_original="round" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o2129" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="very" name="pubescence_or_relief" src="d0_s3" value="rough" value_original="rough" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2130" name="midrib" name_original="midrib" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="very" name="pubescence_or_relief" src="d0_s3" value="rough" value_original="rough" />
      </biological_entity>
      <biological_entity id="o2131" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrid" value_original="scabrid" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, loose, 3–31-flowered cymes;</text>
      <biological_entity id="o2132" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o2133" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="3-31-flowered" value_original="3-31-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts foliaceous, 5–50 mm, margins and abaxial midrib scabrid.</text>
      <biological_entity id="o2134" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2135" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrid" value_original="scabrid" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2136" name="midrib" name_original="midrib" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrid" value_original="scabrid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels ascending, 1–60 mm, slender, pubescent.</text>
      <biological_entity id="o2137" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="60" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 20–30 mm diam.;</text>
      <biological_entity id="o2138" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 5, inconspicuously 3-veined, ovatelanceolate, 6–8 mm, margins narrow, scarious, apex acute, glabrous;</text>
      <biological_entity id="o2139" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" modifier="inconspicuously" name="architecture" src="d0_s8" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2140" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o2141" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 5 (rarely absent), 8–14 mm, longer than sepals, blade apex 2-fid to middle;</text>
      <biological_entity id="o2142" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
        <character constraint="than sepals" constraintid="o2143" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o2143" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity constraint="blade" id="o2144" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="position" src="d0_s9" value="middle" value_original="middle" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 10, sometimes fewer by degeneration;</text>
      <biological_entity id="o2145" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
        <character constraint="by degeneration" constraintid="o2146" is_modifier="false" modifier="sometimes" name="quantity" src="d0_s10" value="fewer" value_original="fewer" />
      </biological_entity>
      <biological_entity id="o2146" name="degeneration" name_original="degeneration" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>styles 3, ascending, ca. 4 mm.</text>
      <biological_entity id="o2147" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules green, subglobose, 5–6 mm, ± equaling sepals, apex obtuse, opening by 3 valves, tardily splitting into 6;</text>
      <biological_entity id="o2148" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2149" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o2150" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character constraint="into 6" is_modifier="false" modifier="tardily" name="architecture_or_dehiscence" src="d0_s12" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o2151" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
      <relation from="o2150" id="r237" name="opening by" negation="false" src="d0_s12" to="o2151" />
    </statement>
    <statement id="d0_s13">
      <text>carpophore absent.</text>
      <biological_entity id="o2152" name="carpophore" name_original="carpophore" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds reddish-brown, reniform, 2–3 mm diam., papillose.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 26 (Europe).</text>
      <biological_entity id="o2153" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2154" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodlands, hedgerows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="hedgerows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Conn., Mass., N.J., N.Y., N.C., Ohio, Pa.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">Greater stitchwort</other_name>
  <other_name type="common_name">Easter-bell</other_name>
  <discussion>Stellaria holostea is sometimes cultivated and occasionally naturalizes.</discussion>
  
</bio:treatment>