<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">341</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="(Piper) C. L. Hitchcock in C. L. Hitchcock et al." date="1964" rank="variety">hypoleium</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>2: 135. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety hypoleium;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060556</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">umbellatum</taxon_name>
    <taxon_name authority="Piper" date="unknown" rank="subspecies">hypoleium</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>11: 238. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species umbellatum;subspecies hypoleium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, mostly prostrate, compact mats, 1–2.5 × 2–5 dm.</text>
      <biological_entity id="o24889" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="mostly" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o24890" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="width" src="d0_s0" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, 0.7–1.5 (–2) dm, thinly floccose or glabrous, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o24891" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24892" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o24891" id="r2768" name="without" negation="false" src="d0_s1" to="o24892" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in loose rosettes;</text>
      <biological_entity id="o24893" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24894" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o24893" id="r2769" name="in" negation="false" src="d0_s2" to="o24894" />
    </statement>
    <statement id="d0_s3">
      <text>blade usually elliptic, (0.5–) 1–2 × (0.4–) 0.5–1.5 cm, glabrous on both surfaces except for fringed, pubescent margins and veins, margins plane, pubescent.</text>
      <biological_entity id="o24895" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_width" src="d0_s3" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character constraint="on surfaces" constraintid="o24896" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24896" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o24897" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24898" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24899" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences umbellate;</text>
      <biological_entity id="o24900" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 1–2 (–3) cm, without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o24901" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24902" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o24903" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o24901" id="r2770" name="without" negation="false" src="d0_s5" to="o24902" />
      <relation from="o24902" id="r2771" name="part_of" negation="false" src="d0_s5" to="o24903" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 2–4 mm, lobes 2–4 mm.</text>
      <biological_entity id="o24904" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24905" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 4–6 mm;</text>
      <biological_entity id="o24906" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth bright-yellow.</text>
      <biological_entity id="o24907" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly to rocky slopes and ridges, high-elevation sagebrush communities, montane to subalpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="high-elevation sagebrush communities" />
        <character name="habitat" value="montane to subalpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(900-)1200-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2100" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107f.</number>
  <other_name type="common_name">Kittitas sulphur flower</other_name>
  <discussion>Variety hypoleium is restricted to Chelan and Kittitas counties, Washington, extending from the Mt. Stuart Range south to the Bald Mountain area west of Ellensburg. It is doubtfully distinct from var. aureum, although geographically well isolated</discussion>
  
</bio:treatment>