<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">567</other_info_on_meta>
    <other_info_on_meta type="mention_page">565</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">polygonum</taxon_name>
    <taxon_name authority="S. Watson" date="1873" rank="section">Duravia</taxon_name>
    <taxon_name authority="Meisner in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">polygaloides</taxon_name>
    <taxon_name authority="(L. C. Wheeler) J. C. Hickman" date="1984" rank="subspecies">esotericum</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>31: 251. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonum;section duravia;species polygaloides;subspecies esotericum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060753</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="L. C. Wheeler" date="unknown" rank="species">esotericum</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>40: 310, fig. 1. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polygonum;species esotericum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–12 cm.</text>
      <biological_entity id="o19153" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences continuous from bases of branches or stems, narrowly cylindric, 2–10 × 5–7 mm;</text>
      <biological_entity id="o19154" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character constraint="from bases" constraintid="o19155" is_modifier="false" name="architecture" src="d0_s1" value="continuous" value_original="continuous" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s1" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19155" name="base" name_original="bases" src="d0_s1" type="structure" />
      <biological_entity id="o19156" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o19157" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <relation from="o19155" id="r2140" name="part_of" negation="false" src="d0_s1" to="o19156" />
      <relation from="o19155" id="r2141" name="part_of" negation="false" src="d0_s1" to="o19157" />
    </statement>
    <statement id="d0_s2">
      <text>bracts appressed, elliptic-lanceolate, 4–8 mm, rigid, margins flat, green, if white then scarious portion 0.1–0.2 mm wide;</text>
      <biological_entity id="o19158" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o19159" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o19160" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="true" name="texture" src="d0_s2" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s2" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>lateral and midveins prominent adaxially.</text>
      <biological_entity constraint="lateral" id="o19161" name="midvein" name_original="midveins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="adaxially" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: perianth 2.1–2.9 mm;</text>
      <biological_entity id="o19162" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o19163" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s4" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>tube 29–40% of perianth length;</text>
      <biological_entity id="o19164" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o19165" name="tube" name_original="tube" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>tepals white or pink;</text>
      <biological_entity id="o19166" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19167" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth-tube and base of tepals smooth;</text>
      <biological_entity id="o19168" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19169" name="perianth-tube" name_original="perianth-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o19170" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o19171" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <relation from="o19169" id="r2142" name="part_of" negation="false" src="d0_s7" to="o19171" />
      <relation from="o19170" id="r2143" name="part_of" negation="false" src="d0_s7" to="o19171" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 5–8.</text>
      <biological_entity id="o19172" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o19173" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes dark-brown, lanceolate, 2–2.5 mm, reticulately ridged to almost smooth, dull.</text>
      <biological_entity id="o19174" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="reticulately" name="shape" src="d0_s9" value="ridged" value_original="ridged" />
        <character is_modifier="false" modifier="almost" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vernal pools, seasonally wet places, desert scrub, pinyon and juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="wet places" modifier="seasonally" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="pinyon" />
        <character name="habitat" value="juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22c.</number>
  <other_name type="common_name">Modoc County knotweed</other_name>
  
</bio:treatment>