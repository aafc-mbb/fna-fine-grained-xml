<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">375</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Greene" date="1885" rank="species">robustum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 126. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species robustum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060480</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">lobbii</taxon_name>
    <taxon_name authority="(Greene) M. E. Jones" date="unknown" rank="variety">robustum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species lobbii;variety robustum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect, matted, 0.5–3 × 1–2 dm, tomentose to floccose.</text>
      <biological_entity id="o13867" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="2" to_unit="dm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s0" to="floccose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o13868" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o13869" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect or nearly so, stout, solid, not fistulose, usually arising directly from a taproot, 0.5–1.2 (–1.6) dm, tomentose to floccose.</text>
      <biological_entity id="o13870" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o13871" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character name="orientation" src="d0_s2" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character constraint="from taproot" constraintid="o13872" is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s2" to="1.6" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s2" to="1.2" to_unit="dm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s2" to="floccose" />
      </biological_entity>
      <biological_entity id="o13872" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, in well-defined rosettes;</text>
      <biological_entity id="o13873" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o13874" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="well-defined" value_original="well-defined" />
      </biological_entity>
      <relation from="o13873" id="r1513" name="in" negation="false" src="d0_s3" to="o13874" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–4 (–5.5) cm, tomentose to floccose;</text>
      <biological_entity id="o13875" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s4" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to obovate, 2.5–4 (–5) × 1.6–2.5 (–3.5) cm, densely white to grayish or reddish-tomentose abaxially, less so to floccose or glabrous and greenish adaxially, margins entire, plane.</text>
      <biological_entity id="o13876" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="width" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="densely white" name="coloration" src="d0_s5" to="grayish" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="reddish-tomentose" value_original="reddish-tomentose" />
        <character is_modifier="false" modifier="less" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o13877" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2-umbellate, 5–10 × 5–10 cm;</text>
      <biological_entity id="o13878" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-umbellate" value_original="2-umbellate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches tomentose to floccose;</text>
      <biological_entity id="o13879" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s7" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3–5 at proximal node, leaflike, 1.5–2.5 (–3.5) × 0.3–0,8 (–1) cm, sometimes absent immediately below involucre.</text>
      <biological_entity id="o13880" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="at proximal node" constraintid="o13881" from="3" name="quantity" src="d0_s8" to="5" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s8" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="2.5" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="3.5" />
        <character char_type="range_value" from="1.5" name="quantity" src="d0_s8" to="2.5" />
        <character char_type="range_value" from="0.3" name="quantity" src="d0_s8" to="0" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="1" to_unit="cm" />
        <character name="some_measurement" src="d0_s8" unit="cm" value="8" value_original="8" />
        <character constraint="immediately below involucre" constraintid="o13882" is_modifier="false" modifier="sometimes" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o13881" name="node" name_original="node" src="d0_s8" type="structure" />
      <biological_entity id="o13882" name="involucre" name_original="involucre" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres 1 per node, campanulate, 8–11 (–13) × 8–12 mm, thinly tomentose to lanate;</text>
      <biological_entity id="o13883" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character constraint="per node" constraintid="o13884" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="thinly tomentose" name="pubescence" src="d0_s9" to="lanate" />
      </biological_entity>
      <biological_entity id="o13884" name="node" name_original="node" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>teeth 6–10, usually lobelike, mostly reflexed, 2–6 mm.</text>
      <biological_entity id="o13885" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="10" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s10" value="lobelike" value_original="lobelike" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 7–9 mm, including 0.1–0.4 mm stipelike base;</text>
      <biological_entity id="o13886" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13887" name="base" name_original="base" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
        <character is_modifier="true" name="shape" src="d0_s11" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <relation from="o13886" id="r1514" name="including" negation="false" src="d0_s11" to="o13887" />
    </statement>
    <statement id="d0_s12">
      <text>perianth creamy yellow to pale yellowish, glabrous;</text>
      <biological_entity id="o13888" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="creamy yellow" name="coloration" src="d0_s12" to="pale yellowish" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals monomorphic, oblong-obovate;</text>
      <biological_entity id="o13889" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-obovate" value_original="oblong-obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens exserted, 7–9 mm;</text>
      <biological_entity id="o13890" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o13891" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes light-brown to brown, 6–8 mm, glabrous.</text>
      <biological_entity id="o13892" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s16" to="brown" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Heavy clayey slopes, montane and subalpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="heavy clayey slopes" />
        <character name="habitat" value="montane" />
        <character name="habitat" value="subalpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300-2000(-2500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2500" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>139.</number>
  <other_name type="common_name">Altered andesite wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum robustum is restricted primarily to altered andesite soils in west-central Nevada, essentially at the confluence of Carson City, Lyon, Storey, and Washoe counties. There, due to the unusual soil, the plants typically occur in areas without sagebrush but among conifer species usually found at higher elevations. The species is cultivated infrequently and is a food plant for the intermediate dotted-blue butterfly (Euphilotes intermedia).</discussion>
  <references>
    <reference>Kuyper, K. F., U. Yandell, and R. S. Nowak. 1997. On the taxonomic status of Eriogonum robustum (Polygonaceae), a rare endemic in western Nevada. Great Basin Naturalist 57: 1–10.</reference>
  </references>
  
</bio:treatment>