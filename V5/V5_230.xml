<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">stellaria</taxon_name>
    <taxon_name authority="Engelmann" date="1882" rank="species">obtusa</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>7: 5. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus stellaria;species obtusa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060938</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alsine</taxon_name>
    <taxon_name authority="(Engelmann) Rose" date="unknown" rank="species">obtusa</taxon_name>
    <taxon_hierarchy>genus Alsine;species obtusa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alsine</taxon_name>
    <taxon_name authority="Piper" date="unknown" rank="species">viridula</taxon_name>
    <taxon_hierarchy>genus Alsine;species viridula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alsine</taxon_name>
    <taxon_name authority="(B. L. Robinson) A. Heller" date="unknown" rank="species">washingtoniana</taxon_name>
    <taxon_hierarchy>genus Alsine;species washingtoniana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stellaria</taxon_name>
    <taxon_name authority="(Piper) St. John" date="unknown" rank="species">viridula</taxon_name>
    <taxon_hierarchy>genus Stellaria;species viridula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stellaria</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="species">washingtoniana</taxon_name>
    <taxon_hierarchy>genus Stellaria;species washingtoniana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, creeping, often matted but not forming cushions, rhizomatous.</text>
      <biological_entity id="o31348" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o31349" name="cushion" name_original="cushions" src="d0_s0" type="structure" />
      <relation from="o31348" id="r3545" name="forming" negation="true" src="d0_s0" to="o31349" />
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, branched, 4-sided, 3–23 cm, internodes equaling or longer than leaves, glabrous, rarely pilose.</text>
      <biological_entity id="o31350" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="23" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o31351" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="variability" src="d0_s1" value="equaling" value_original="equaling" />
        <character constraint="than leaves" constraintid="o31352" is_modifier="false" name="length_or_size" src="d0_s1" value="longer" value_original="longer" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o31352" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile or short-petiolate;</text>
      <biological_entity id="o31353" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-petiolate" value_original="short-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly ovate to elliptic, 0.2–1.2 cm × 0.9–7 mm, base round or cuneate, margins entire, apex acute, shiny, glabrous or ciliate near base.</text>
      <biological_entity id="o31354" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="length" src="d0_s3" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31355" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o31356" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o31357" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="near base" constraintid="o31358" is_modifier="false" name="pubescence" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o31358" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with flowers solitary, axillary;</text>
      <biological_entity id="o31359" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o31360" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o31359" id="r3546" name="with" negation="false" src="d0_s4" to="o31360" />
    </statement>
    <statement id="d0_s5">
      <text>bracts absent.</text>
      <biological_entity id="o31361" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels spreading, 3–12 mm, glabrous.</text>
      <biological_entity id="o31362" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 1.5–2 mm diam.;</text>
      <biological_entity id="o31363" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 4–5, veins obscure, midrib sometimes apparent, ± ovate, 1.5–3.5 mm, margins narrow, scarious, apex ± obtuse, glabrous;</text>
      <biological_entity id="o31364" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o31365" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o31366" name="midrib" name_original="midrib" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s8" value="apparent" value_original="apparent" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31367" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o31368" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals absent;</text>
      <biological_entity id="o31369" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 10 or fewer;</text>
      <biological_entity id="o31370" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or fewer" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3 (–4), curled, shorter than 0.5 mm.</text>
      <biological_entity id="o31371" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="4" />
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s11" value="curled" value_original="curled" />
        <character modifier="shorter than" name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules green to pale straw colored, translucent, globose to broadly ovoid, 2.3–3.5 mm, 1.9–2 times as long as sepals, apex obtuse, opening by 6 valves;</text>
      <biological_entity id="o31372" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s12" to="pale straw colored" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s12" value="translucent" value_original="translucent" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s12" to="broadly ovoid" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character constraint="sepal" constraintid="o31373" is_modifier="false" name="length" src="d0_s12" value="1.9-2 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o31373" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o31374" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o31375" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="6" value_original="6" />
      </biological_entity>
      <relation from="o31374" id="r3547" name="opening by" negation="false" src="d0_s12" to="o31375" />
    </statement>
    <statement id="d0_s13">
      <text>carpophore absent.</text>
      <biological_entity id="o31376" name="carpophore" name_original="carpophore" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds grayish black, broadly elliptic, 0.5–0.7 mm diam., finely reticulate.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 26, 52, ca. 65, ca. 78.</text>
      <biological_entity id="o31377" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="grayish black" value_original="grayish black" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s14" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s14" to="0.7" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="architecture_or_coloration_or_relief" src="d0_s14" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31378" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="26" value_original="26" />
        <character name="quantity" src="d0_s15" value="52" value_original="52" />
        <character name="quantity" src="d0_s15" value="65" value_original="65" />
        <character name="quantity" src="d0_s15" value="78" value_original="78" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist areas in woods, shaded edges of creeks, talus slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist areas" constraint="in woods , shaded edges of creeks , talus slopes" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="shaded edges" constraint="of creeks , talus slopes" />
        <character name="habitat" value="creeks" />
        <character name="habitat" value="talus slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Calif., Colo., Idaho, Mont., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Blunt-sepaled starwort</other_name>
  <other_name type="common_name">Rocky Mountain starwort</other_name>
  
</bio:treatment>