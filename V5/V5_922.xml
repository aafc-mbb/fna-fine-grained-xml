<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">441</other_info_on_meta>
    <other_info_on_meta type="illustration_page">442</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="genus">nemacaulis</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="species">denudata</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia, n. s.</publication_title>
      <place_in_publication>1: 168. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus nemacaulis;species denudata;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220009066</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="species">nemacaulis</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species nemacaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.4–2.5 (–4) × 0.2–8 dm.</text>
      <biological_entity id="o23126" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="4" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="length" src="d0_s0" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="width" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 1–8 × 0.1–1.5 cm.</text>
      <biological_entity id="o23127" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s1" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 0.3–3 × 2.8 dm;</text>
      <biological_entity id="o23128" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.3" from_unit="dm" name="length" src="d0_s2" to="3" to_unit="dm" />
        <character name="width" src="d0_s2" unit="dm" value="2.8" value_original="2.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches (2–) 4–10 at first node, 1–2 (–3) thereafter;</text>
      <biological_entity id="o23129" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s3" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <biological_entity id="o23130" name="node" name_original="node" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="3" />
        <character char_type="range_value" from="1" modifier="thereafter" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts 1–5 × 0.5–2 mm.</text>
      <biological_entity id="o23131" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0.1–3 mm or absent.</text>
      <biological_entity id="o23132" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucral-bracts (1.5–) 2–4 × 0.5–2 mm.</text>
      <biological_entity id="o23133" name="involucral-bract" name_original="involucral-bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s6" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth 0.5–1.5 mm;</text>
      <biological_entity id="o23134" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o23135" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 0.5–1 mm;</text>
      <biological_entity id="o23136" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o23137" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 0.2 mm.</text>
      <biological_entity id="o23138" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o23139" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Achenes 1 mm.</text>
      <biological_entity id="o23140" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers (5-)12-30 per involucre; peduncles usually absent; involucral bracts dark red, white-tomentose; stems prostrate to decumbent; leaf blades usually spatulate, sometimes linear; outer tepals broadly obovate to ovate; coastal beaches</description>
      <determination>1a Nemacaulis denudata var. denudata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers 5(-12) per involucre; peduncules usually present; involucral bracts light brown to yellowish green, tawny-tomentose; stems ascending to erect; leaf blades usually linear or narrowly spatulate; outer tepals linear to oblong; coastal and inland deserts</description>
      <determination>1b Nemacaulis denudata var. gracilis</determination>
    </key_statement>
  </key>
</bio:treatment>