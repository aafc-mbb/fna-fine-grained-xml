<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">330</other_info_on_meta>
    <other_info_on_meta type="mention_page">329</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="1835" rank="species">longifolium</taxon_name>
    <taxon_name authority="(Goodman) Reveal" date="1968" rank="variety">harperi</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>3: 197. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eriogonum;species longifolium;variety harperi;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060373</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Goodman" date="unknown" rank="species">harperi</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>74: 329, figs. 1–3. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species harperi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–18 dm, thinly tomentose.</text>
      <biological_entity id="o7730" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="18" to_unit="dm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal leaf-blade 10–15 × 1.5–2.5 cm, tomentose abaxially, sparsely floccose or glabrous adaxially.</text>
      <biological_entity id="o7731" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o7732" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s1" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 0.3–0.8 cm.</text>
      <biological_entity id="o7733" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s2" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres turbinate, 3–3.5 × 1.5–2.5 mm.</text>
      <biological_entity id="o7734" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 5–7 mm, including 0.5–1 (–1.2) mm stipelike base.</text>
      <biological_entity id="o7735" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7736" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_some_measurement" src="d0_s4" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="true" name="shape" src="d0_s4" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <relation from="o7735" id="r844" name="including" negation="false" src="d0_s4" to="o7736" />
    </statement>
    <statement id="d0_s5">
      <text>Achenes 4–4.5 mm. 2n = 40.</text>
      <biological_entity id="o7737" name="achene" name_original="achenes" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7738" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly, often calcareous flats, bluffs, outcrops, and slopes, oak and conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly" />
        <character name="habitat" value="sandy to calcareous flats" modifier="often" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ky., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>105b.</number>
  <other_name type="common_name">Harper’s wild buckwheat</other_name>
  <discussion>Variety harperi is known only from scattered populations in Alabama (Colbert and Franklin counties), Kentucky (Christian County), and Tennessee (DeKalb, Putnam, Smith, and Wilson counties). The Kentucky record is based on a report indicating that the plant had been extirpated there.</discussion>
  
</bio:treatment>