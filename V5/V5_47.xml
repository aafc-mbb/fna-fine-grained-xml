<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ann Swanson,Richard K. Rabeler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Tanfani in F. Parlatore" date="unknown" rank="subfamily">Polycarpoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">STIPULICIDA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 26, plate 6. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily polycarpoideae;genus stipulicida;</taxon_hierarchy>
    <other_info_on_name type="fna_id">131601</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual (or short-lived perennial?).</text>
      <biological_entity id="o19639" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots filiform to stout.</text>
      <biological_entity id="o19640" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems diffuse to erect, repeatedly dichotomous, terete.</text>
      <biological_entity id="o19641" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="density" src="d0_s2" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="repeatedly" name="architecture" src="d0_s2" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite (cauline) or rosulate (basal), connate (distally) or not (proximally), petiolate (basal) or sessile (cauline);</text>
      <biological_entity id="o19642" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character name="fusion" src="d0_s3" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules 2 per node (cauline leaves) or forming tuft of to 14+ per node (basal leaves), white to tan, filiform, forming incised or notched nodal fringe;</text>
      <biological_entity id="o19643" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character constraint="per node; per node or forming tuft" constraintid="o19645, o19646" name="quantity" src="d0_s4" value="2" value_original="2" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s4" to="tan" />
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o19644" name="node" name_original="node" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per node" constraintid="o19647" from="0" modifier="of" name="quantity" src="d0_s4" to="14" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o19645" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity id="o19646" name="tuft" name_original="tuft" src="d0_s4" type="structure" />
      <biological_entity id="o19647" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity constraint="nodal" id="o19648" name="fringe" name_original="fringe" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="incised" value_original="incised" />
        <character is_modifier="true" name="shape" src="d0_s4" value="notched" value_original="notched" />
      </biological_entity>
      <relation from="o19643" id="r2191" name="forming" negation="false" src="d0_s4" to="o19648" />
    </statement>
    <statement id="d0_s5">
      <text>blade 1-veined, spatulate to suborbiculate (basal) or scalelike, subulate to triangular (cauline), not succulent, apex obtuse.</text>
      <biological_entity id="o19649" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="suborbiculate or scalelike subulate" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="suborbiculate or scalelike subulate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o19650" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, compact, few-flowered cymes;</text>
      <biological_entity id="o19651" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o19652" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="few-flowered" value_original="few-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts paired, scalelike.</text>
      <biological_entity id="o19653" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
        <character is_modifier="false" name="shape" src="d0_s7" value="scale-like" value_original="scalelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect.</text>
      <biological_entity id="o19654" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth and androecium hypogynous;</text>
      <biological_entity id="o19655" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19656" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o19657" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals distinct, reddish-brown, elliptic to obovate, 0.8–2 mm, scarious, margins scarious, apex acute to obtuse or mucronate;</text>
      <biological_entity id="o19658" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19659" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o19660" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o19661" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="obtuse or mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, white, blade apex entire to erose;</text>
      <biological_entity id="o19662" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19663" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="blade" id="o19664" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="entire" name="architecture" src="d0_s11" to="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectaries as minute, rounded lobes flanking filament bases;</text>
      <biological_entity id="o19665" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o19666" name="nectary" name_original="nectaries" src="d0_s12" type="structure" />
      <biological_entity id="o19667" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="minute" value_original="minute" />
        <character is_modifier="true" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="filament" id="o19668" name="base" name_original="bases" src="d0_s12" type="structure" />
      <relation from="o19666" id="r2192" name="as" negation="false" src="d0_s12" to="o19667" />
      <relation from="o19667" id="r2193" name="flanking" negation="false" src="d0_s12" to="o19668" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 3–5;</text>
      <biological_entity id="o19669" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19670" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct;</text>
      <biological_entity id="o19671" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o19672" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3, distinct or nearly so, capitate, ca. 0.2 mm, glabrous proximally;</text>
      <biological_entity id="o19673" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o19674" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character name="fusion" src="d0_s15" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.2" value_original="0.2" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigmas 3, terminal, obscurely papillate (30×).</text>
      <biological_entity id="o19675" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o19676" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s16" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s16" value="papillate" value_original="papillate" />
        <character name="quantity" src="d0_s16" value="[30" value_original="[30" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules ellipsoid to globose, opening by 3 recurved valves;</text>
      <biological_entity id="o19677" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s17" to="globose" />
      </biological_entity>
      <biological_entity id="o19678" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="true" name="orientation" src="d0_s17" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o19677" id="r2194" name="opening by" negation="false" src="d0_s17" to="o19678" />
    </statement>
    <statement id="d0_s18">
      <text>carpophore absent.</text>
      <biological_entity id="o19679" name="carpophore" name_original="carpophore" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds ca. 20, golden chestnut to reddish-brown, ± triangular, laterally compressed, lustrous, reticulate, marginal wing absent, appendage absent.</text>
      <biological_entity id="o19680" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="20" value_original="20" />
        <character char_type="range_value" from="golden chestnut" name="coloration" src="d0_s19" to="reddish-brown" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s19" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s19" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="reflectance" src="d0_s19" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s19" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o19681" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o19682" name="appendage" name_original="appendage" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States, West Indies (Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>