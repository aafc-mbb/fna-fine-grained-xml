<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">60</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(Nuttall) Ikonnikov" date="1973" rank="species">congesta</taxon_name>
    <place_of_publication>
      <publication_title>Novosti Syst. Vyssh. Rast.</publication_title>
      <place_in_publication>10: 139. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species congesta;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060130</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">congesta</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 178. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species congesta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tufted or sometimes matted, green, not glaucous, with woody base.</text>
      <biological_entity id="o27049" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o27050" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o27049" id="r3005" name="with" negation="false" src="d0_s0" to="o27050" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect, 3–40 (–50) cm, glabrous or often stipitate-glandular.</text>
      <biological_entity id="o27051" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves persistent or not;</text>
      <biological_entity id="o27052" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o27053" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character name="duration" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves in 3–5 pairs, similar, but reduced distally;</text>
      <biological_entity id="o27054" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o27055" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o27056" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <relation from="o27055" id="r3006" name="in" negation="false" src="d0_s3" to="o27056" />
    </statement>
    <statement id="d0_s4">
      <text>basal blades erect-ascending to arcuate-spreading, subulate or needlelike to filiform, (0.8–) 2–11 (–14) cm × 0.4–2 mm, flexuous or rigid, herbaceous to ± fleshy, apex obtuse to sharply acute or spinose, glabrous, sometimes glaucous.</text>
      <biological_entity id="o27057" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o27058" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="erect-ascending" name="orientation" src="d0_s4" to="arcuate-spreading" />
        <character char_type="range_value" from="needlelike" name="shape" src="d0_s4" to="filiform" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_length" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="11" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s4" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s4" to="more or less fleshy" />
      </biological_entity>
      <biological_entity id="o27059" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="sharply acute or spinose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 3–50+-flowered, congested and capitate or sometimes open, umbellate cymes.</text>
      <biological_entity id="o27060" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-50+-flowered" value_original="3-50+-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o27061" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.1–7 (–15) mm or ± absent, usually glabrous, rarely stipitate-glandular.</text>
      <biological_entity id="o27062" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 1–3-veined, sometimes obscurely so, ovate to lanceolate, 3–6.5 mm, not expanding in fruit, margins narrow, apex obtuse or acute to acuminate, rarely spinose, glabrous (or glandular in var. prolifera);</text>
      <biological_entity id="o27063" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o27064" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-3-veined" value_original="1-3-veined" />
        <character char_type="range_value" from="ovate" modifier="sometimes obscurely; obscurely" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6.5" to_unit="mm" />
        <character constraint="in fruit" constraintid="o27065" is_modifier="false" modifier="not" name="size" src="d0_s7" value="expanding" value_original="expanding" />
      </biological_entity>
      <biological_entity id="o27065" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o27066" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o27067" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s7" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, oblong, 5–8 (–10) mm, 1.5–2 times as long as sepals, apex entire to slightly emarginate;</text>
      <biological_entity id="o27068" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o27069" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character constraint="sepal" constraintid="o27070" is_modifier="false" name="length" src="d0_s8" value="1.5-2 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o27070" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity id="o27071" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s8" to="slightly emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectaries as lateral and abaxial mound with crescent-shaped groove at base of filaments opposite sepals, 0.3 × 0.15–0.2 mm.</text>
      <biological_entity id="o27072" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o27073" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character name="length" notes="" src="d0_s9" unit="mm" value="0.3" value_original="0.3" />
        <character char_type="range_value" from="0.15" from_unit="mm" name="width" notes="" src="d0_s9" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral and abaxial" id="o27074" name="mound" name_original="mound" src="d0_s9" type="structure" />
      <biological_entity id="o27075" name="groove" name_original="groove" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="crescent--shaped" value_original="crescent--shaped" />
      </biological_entity>
      <biological_entity id="o27076" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o27077" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o27078" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o27073" id="r3007" name="as" negation="false" src="d0_s9" to="o27074" />
      <relation from="o27074" id="r3008" name="with" negation="false" src="d0_s9" to="o27075" />
      <relation from="o27075" id="r3009" name="at" negation="false" src="d0_s9" to="o27076" />
      <relation from="o27076" id="r3010" name="part_of" negation="false" src="d0_s9" to="o27077" />
      <relation from="o27076" id="r3011" name="part_of" negation="false" src="d0_s9" to="o27078" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules 3.5–6 mm, glabrous.</text>
      <biological_entity id="o27079" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds reddish-brown to black, broadly ellipsoid to ovoid, 1.4–3 mm, tuberculate;</text>
      <biological_entity id="o27080" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s11" to="black" />
        <character char_type="range_value" from="broadly ellipsoid" name="shape" src="d0_s11" to="ovoid" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>tubercles low, rounded, often elongate.</text>
      <biological_entity id="o27081" name="tubercle" name_original="tubercles" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Sask.; Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Ballhead sandwort</other_name>
  <discussion>Varieties 9 (9 in the flora).</discussion>
  <discussion>Eremogone congesta is highly polymorphic; it has been been divided into 11 varieties (nine recognized here), most of which are distinctive and locally distributed. M. F. Baad (1969) noted two patterns of variation of different origin within E. congesta, but he did not present a revised classification.</discussion>
  <discussion>While most specimens of the four varieties with dense inflorescences do not exhibit evident pedicels, the occasional plant does bear one or more pedicels to 1–2 mm, sometimes in secondary inflorescences.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels usually 0.1-0.2 mm or ± absent; inflorescences dense, tight cymes</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels 1-6(-15) mm; inflorescences somewhat to markedly open cymes or umbellate cymes</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal leaf blades filiform, 3-14 cm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal leaf blades needlelike, 1-3.5 cm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences capitate, rounded cymes; sepal apices obtuse to rounded</description>
      <determination>4a Eremogone congesta var. congesta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences capitate, pyramidal cymes; sepal apices narrowly acute to acuminate</description>
      <determination>4b Eremogone congesta var. cephaloidea</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals 5-6.5 mm, apex acute to acuminate; basal leaf blades herbaceous; California, Nevada</description>
      <determination>4g Eremogone congesta var. simulans</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals 3.5-4 mm, apex obtuse; basal leaf blades ± succulent; California, Oregon</description>
      <determination>4d Eremogone congesta var. crassula</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepal apices obtuse</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepal apices acute to acuminate or spinose</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Inflorescences proliferating, ± loose cymes; bracts often not closely enveloping sepals; Alberta, Saskatchewan, Colorado, Idaho, Montana, Utah, Wyoming</description>
      <determination>4e Eremogone congesta var. lithophila</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Inflorescences umbels, bracts clustered at umbel base; California</description>
      <determination>4i Eremogone congesta var. suffrutescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Basal leaf blades (2-)3-8 cm, filiform; pedicels glabrous or sometimes stipitate-glandular (possible throughout range, true for Nevada populations); Idaho, Nevada, Oregon, Washington</description>
      <determination>4f Eremogone congesta var. prolifera</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Basal leaf blades 0.5-2.5(-3.5) cm, needlelike or filiform; pedicels glabrous; California, Nevada, Utah</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences proliferating, ± loose cymes; sepals 3.5-4.5 mm</description>
      <determination>4h Eremogone congesta var. subcongesta</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences capitate and often rounded, to subcongested, proliferating or open cymes; sepals 4.5-6.5 mm</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Sepals 4.5-5.5 mm, weakly to conspicuously 1-3-veined, apex spinose</description>
      <determination>4c Eremogone congesta var. charlestonensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Sepals usually 5.5-6.5 mm, conspicuously 3-veined, apex acute to acuminate</description>
      <determination>4g Eremogone congesta var. simulans</determination>
    </key_statement>
  </key>
</bio:treatment>