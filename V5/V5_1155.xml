<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">566</other_info_on_meta>
    <other_info_on_meta type="mention_page">565</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">polygonum</taxon_name>
    <taxon_name authority="S. Watson" date="1873" rank="section">Duravia</taxon_name>
    <taxon_name authority="Meisner in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">polygaloides</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">polygaloides</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonum;section duravia;species polygaloides;subspecies polygaloides;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060755</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">polygaloides</taxon_name>
    <taxon_name authority="Brenckle" date="unknown" rank="variety">montanum</taxon_name>
    <taxon_hierarchy>genus Polygonum;species polygaloides;variety montanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–25 cm.</text>
      <biological_entity id="o1673" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences mostly confined to tips of branches, globose to ovoid, 7–14 × 8–15 mm;</text>
      <biological_entity id="o1674" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s1" to="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s1" to="14" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1675" name="tip" name_original="tips" src="d0_s1" type="structure" />
      <biological_entity id="o1676" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <relation from="o1674" id="r178" name="confined to" negation="false" src="d0_s1" to="o1675" />
      <relation from="o1674" id="r179" name="part_of" negation="false" src="d0_s1" to="o1676" />
    </statement>
    <statement id="d0_s2">
      <text>bracts ascending or appressed, oblong to broadly ovate or elliptic, 4–9 mm, ± rigid, margins flat, undulate, or sometimes revolute, white, scarious portion 0.5–1 mm wide;</text>
      <biological_entity id="o1677" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="broadly ovate or elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o1678" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o1679" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="true" name="texture" src="d0_s2" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>midveins thin, lateral-veins thickened and prominent adaxially.</text>
      <biological_entity id="o1680" name="midvein" name_original="midveins" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o1681" name="lateral-vein" name_original="lateral-veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="adaxially" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: perianth 2.5–3 mm;</text>
      <biological_entity id="o1682" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o1683" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>tube 19–30% of perianth length;</text>
      <biological_entity id="o1684" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1685" name="tube" name_original="tube" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>tepals white or pink;</text>
      <biological_entity id="o1686" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1687" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth-tube and base of tepals often ± crisped or undulate;</text>
      <biological_entity id="o1688" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1689" name="perianth-tube" name_original="perianth-tube" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often more or less" name="shape" src="d0_s7" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="shape" src="d0_s7" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o1690" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often more or less" name="shape" src="d0_s7" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="shape" src="d0_s7" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o1691" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <relation from="o1689" id="r180" name="part_of" negation="false" src="d0_s7" to="o1691" />
      <relation from="o1690" id="r181" name="part_of" negation="false" src="d0_s7" to="o1691" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 8.</text>
      <biological_entity id="o1692" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1693" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes dark-brown, ovatelanceolate, 1.3–2.1 mm, reticulate with longitudinal ridges, dull.</text>
      <biological_entity id="o1694" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s9" to="2.1" to_unit="mm" />
        <character constraint="with ridges" constraintid="o1695" is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s9" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s9" value="dull" value_original="dull" />
      </biological_entity>
      <biological_entity id="o1695" name="ridge" name_original="ridges" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s9" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry beds of ponds and watercourses, roadsides, drainage pits</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry beds" constraint="of ponds and watercourses" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="watercourses" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="drainage pits" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.; Idaho, Mont., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22a.</number>
  
</bio:treatment>