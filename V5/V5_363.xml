<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">181</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="Ehrhart" date="1792" rank="species">dichotoma</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">dichotoma</taxon_name>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species dichotoma;subspecies dichotoma;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060842</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, robust;</text>
      <biological_entity id="o22723" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot slender.</text>
      <biological_entity id="o22724" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, branched distally, usually reddish, (20–) 50–100 cm, coarsely hispid.</text>
      <biological_entity id="o22725" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 2 per node;</text>
      <biological_entity id="o22726" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="per node" constraintid="o22727" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o22727" name="node" name_original="node" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal petiolate, blade lanceolate-spatulate, 3–10 cm × 6–30 mm (including petiole), base tapering into petiole as long as lamina;</text>
      <biological_entity constraint="proximal" id="o22728" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o22729" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate-spatulate" value_original="lanceolate-spatulate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22730" name="base" name_original="base" src="d0_s4" type="structure">
        <character constraint="into petiole" constraintid="o22731" is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o22731" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o22732" name="lamina" name_original="lamina" src="d0_s4" type="structure" />
      <relation from="o22731" id="r2525" name="as long as" negation="false" src="d0_s4" to="o22732" />
    </statement>
    <statement id="d0_s5">
      <text>cauline sessile or nearly so, smaller distally, blade lanceolate-cuneate, 1.5–5 cm × 3–15 mm, apex acute, hispid on both surfaces.</text>
      <biological_entity constraint="cauline" id="o22733" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s5" value="nearly" value_original="nearly" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o22734" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate-cuneate" value_original="lanceolate-cuneate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22735" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character constraint="on surfaces" constraintid="o22736" is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o22736" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences with many ascending, elongate branches, many-flowered, open, setose, glandular or not but not viscid;</text>
      <biological_entity id="o22737" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="many-flowered" value_original="many-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="setose" value_original="setose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
        <character name="architecture_or_function_or_pubescence" src="d0_s6" value="not" value_original="not" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o22738" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="many" value_original="many" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="true" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o22737" id="r2526" name="with" negation="false" src="d0_s6" to="o22738" />
    </statement>
    <statement id="d0_s7">
      <text>flowers 1 per node;</text>
      <biological_entity id="o22739" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character constraint="per node" constraintid="o22740" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o22740" name="node" name_original="node" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracts ranging from resembling leaves to narrowly lanceolate, 5–10 mm, apex caudate-acuminate.</text>
      <biological_entity id="o22741" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o22742" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o22743" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="caudate-acuminate" value_original="caudate-acuminate" />
      </biological_entity>
      <relation from="o22741" id="r2527" name="ranging from" negation="false" src="d0_s8" to="o22742" />
      <relation from="o22741" id="r2528" name="to" negation="false" src="d0_s8" to="o22743" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers subsessile;</text>
      <biological_entity id="o22744" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calyx prominently 10-veined, not inflated, setose, lobed, tubular in flower, ovoid and (7–) 10–15 × 2.5–4 mm in fruit, veins parallel, with pale commissures;</text>
      <biological_entity id="o22745" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s10" value="10-veined" value_original="10-veined" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="setose" value_original="setose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
        <character constraint="in flower" constraintid="o22746" is_modifier="false" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s10" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o22747" from="2.5" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22746" name="flower" name_original="flower" src="d0_s10" type="structure" />
      <biological_entity id="o22747" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity id="o22748" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o22749" name="commissure" name_original="commissures" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="pale" value_original="pale" />
      </biological_entity>
      <relation from="o22748" id="r2529" name="with" negation="false" src="d0_s10" to="o22749" />
    </statement>
    <statement id="d0_s11">
      <text>lobes 5, spreading to recurved, narrowly lanceolate, 2–3 mm, apex acuminate;</text>
      <biological_entity id="o22750" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s11" to="recurved" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22751" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white, rarely pink, clawed, claw equaling calyx, limb obovate but deeply 2-lobed, 5–9 mm, apex truncate, appendages 0.2 mm;</text>
      <biological_entity id="o22752" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s12" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o22753" name="claw" name_original="claw" src="d0_s12" type="structure" />
      <biological_entity id="o22754" name="calyx" name_original="calyx" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o22755" name="limb" name_original="limb" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s12" value="2-lobed" value_original="2-lobed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22756" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o22757" name="appendage" name_original="appendages" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens exceeding petals, reduced to staminodes in some flowers;</text>
      <biological_entity id="o22758" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character constraint="to staminodes" constraintid="o22760" is_modifier="false" name="size" src="d0_s13" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o22759" name="petal" name_original="petals" src="d0_s13" type="structure" />
      <biological_entity id="o22760" name="staminode" name_original="staminodes" src="d0_s13" type="structure" />
      <biological_entity id="o22761" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <relation from="o22758" id="r2530" name="exceeding" negation="false" src="d0_s13" to="o22759" />
      <relation from="o22760" id="r2531" name="in" negation="false" src="d0_s13" to="o22761" />
    </statement>
    <statement id="d0_s14">
      <text>filaments white;</text>
      <biological_entity id="o22762" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3, white, ca. 2 times calyx.</text>
      <biological_entity id="o22763" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character constraint="calyx" constraintid="o22764" is_modifier="false" name="size_or_quantity" src="d0_s15" value="2 times calyx" value_original="2 times calyx" />
      </biological_entity>
      <biological_entity id="o22764" name="calyx" name_original="calyx" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules enclosed in calyx, ellipsoid, opening by 6 spreading, lanceolate teeth;</text>
      <biological_entity id="o22765" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o22766" name="calyx" name_original="calyx" src="d0_s16" type="structure" />
      <biological_entity id="o22767" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="6" value_original="6" />
        <character is_modifier="true" name="orientation" src="d0_s16" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="shape" src="d0_s16" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <relation from="o22765" id="r2532" name="enclosed in" negation="false" src="d0_s16" to="o22766" />
      <relation from="o22765" id="r2533" name="opening by" negation="false" src="d0_s16" to="o22767" />
    </statement>
    <statement id="d0_s17">
      <text>carpophore stout, 1.5–4 mm, glabrous.</text>
      <biological_entity id="o22768" name="carpophore" name_original="carpophore" src="d0_s17" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s17" value="stout" value_original="stout" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds grayish brown to black, broadly reniform with concave faces, ca. 1 mm diam., rugose.</text>
      <biological_entity id="o22770" name="face" name_original="faces" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>2n = 24.</text>
      <biological_entity id="o22769" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="grayish brown" name="coloration" src="d0_s18" to="black" />
        <character constraint="with faces" constraintid="o22770" is_modifier="false" modifier="broadly" name="shape" src="d0_s18" value="reniform" value_original="reniform" />
        <character name="diameter" notes="" src="d0_s18" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="relief" src="d0_s18" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22771" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cultivated land, roadsides, burnt clearings in forests, disturbed prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cultivated land" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="burnt clearings" constraint="in forests , disturbed prairies" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="disturbed prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Nfld. and Labr. (Nfld.), Ont., Que., Sask.; Calif., Colo., Conn., Ga., Idaho, Ill., Ind., Iowa, Ky., Maine, Mass., Mich., Minn., Mo., Mont., Nebr., N.H., N.J., N.Y., N.C., Ohio, Oreg., Pa., Tenn., Tex., Vt., Va., Wash., W.Va., Wis., Wyo.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15a.</number>
  <discussion>Subspecies dichotoma is an occasional adventive weed.</discussion>
  
</bio:treatment>