<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">462</other_info_on_meta>
    <other_info_on_meta type="mention_page">448</other_info_on_meta>
    <other_info_on_meta type="mention_page">467</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="section">Ptelosepala</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="species">procumbens</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 17. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section ptelosepala;species procumbens;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060088</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chorizanthe</taxon_name>
    <taxon_name authority="Goodman" date="unknown" rank="species">chaetophora</taxon_name>
    <taxon_hierarchy>genus Chorizanthe;species chaetophora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chorizanthe</taxon_name>
    <taxon_name authority="Goodman" date="unknown" rank="species">jonesiana</taxon_name>
    <taxon_hierarchy>genus Chorizanthe;species jonesiana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chorizanthe</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">procumbens</taxon_name>
    <taxon_name authority="Goodman" date="unknown" rank="variety">albiflora</taxon_name>
    <taxon_hierarchy>genus Chorizanthe;species procumbens;variety albiflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chorizanthe</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">procumbens</taxon_name>
    <taxon_name authority="Goodman" date="unknown" rank="variety">mexicana</taxon_name>
    <taxon_hierarchy>genus Chorizanthe;species procumbens;variety mexicana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chorizanthe</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">uncinata</taxon_name>
    <taxon_hierarchy>genus Chorizanthe;species uncinata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants prostrate to decumbent, 0.2–0.8 × 0.5–4 (–5) dm, thinly pubescent.</text>
      <biological_entity id="o23333" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="prostrate" name="growth_form_or_orientation" src="d0_s0" to="decumbent" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="length" src="d0_s0" to="0.8" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o23334" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.5–2 (–3) cm;</text>
      <biological_entity id="o23335" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblanceolate, (0.5–) 1–3 (–4) × 0.1–0.7 (–1.2) cm, thinly pubescent.</text>
      <biological_entity id="o23336" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="0.7" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with involucres in small, open clusters 0.3–1 cm diam., greenish yellow to green or reddish green;</text>
      <biological_entity id="o23337" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="greenish yellow" name="diam" notes="" src="d0_s4" to="green or reddish green" />
      </biological_entity>
      <biological_entity id="o23338" name="involucre" name_original="involucres" src="d0_s4" type="structure" />
      <relation from="o23337" id="r2600" name="with" negation="false" src="d0_s4" to="o23338" />
    </statement>
    <statement id="d0_s5">
      <text>bracts 2, sessile, leaflike and similar to proximal leaf-blades only reduced, linear-oblanceolate to elliptic, 0.3–1 (–1.5) cm × 1.5–5 (–8) mm, rapidly reduced and scalelike at distal nodes, linear, acicular, often acerose, 0.1–0.5 cm × (0.3–) 0.5–3 mm, awns straight, 0.2–1 mm.</text>
      <biological_entity id="o23339" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="linear-oblanceolate" name="shape" notes="" src="d0_s5" to="elliptic" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="rapidly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character constraint="at distal nodes" constraintid="o23341" is_modifier="false" name="shape" src="d0_s5" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" notes="" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acicular" value_original="acicular" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="acerose" value_original="acerose" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="length" src="d0_s5" to="0.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_width" src="d0_s5" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23340" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="only" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23341" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o23342" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o23339" id="r2601" name="to" negation="false" src="d0_s5" to="o23340" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3–10, rarely more, greenish yellow to reddish green, cylindric or narrowly to broadly campanulate, not ventricose,1.5–3 mm, faintly corrugate, without scarious or membranous margins, thinly pubescent with spreading hairs, longest hairs on ribs and at base;</text>
      <biological_entity id="o23343" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="10" />
        <character char_type="range_value" from="greenish yellow" modifier="rarely" name="coloration" src="d0_s6" to="reddish green" />
        <character char_type="range_value" from="cylindric or" name="shape" src="d0_s6" to="narrowly broadly campanulate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="faintly" name="arrangement_or_relief" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character constraint="with hairs" constraintid="o23345" is_modifier="false" modifier="thinly" name="pubescence" notes="" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o23344" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="true" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o23345" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="longest" id="o23346" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <biological_entity id="o23347" name="base" name_original="base" src="d0_s6" type="structure" />
      <relation from="o23343" id="r2602" name="without" negation="false" src="d0_s6" to="o23344" />
      <relation from="o23346" id="r2603" name="on ribs and at" negation="false" src="d0_s6" to="o23347" />
    </statement>
    <statement id="d0_s7">
      <text>teeth spreading, equal, 1–2.5 mm, or divergent, thickened basally, unequal, 1–2 mm or 2.5–5 mm with hyaline margins between teeth;</text>
      <biological_entity id="o23348" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="basally" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character name="some_measurement" src="d0_s7" value="2.5-5 mm" value_original="2.5-5 mm" />
      </biological_entity>
      <biological_entity constraint="between teeth" constraintid="o23350" id="o23349" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="between  teeth, ">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o23350" name="tooth" name_original="teeth" src="d0_s7" type="structure" />
      <relation from="o23348" id="r2604" name="with" negation="false" src="d0_s7" to="o23349" />
    </statement>
    <statement id="d0_s8">
      <text>awns uncinate, 0.2–0.5 mm.</text>
      <biological_entity id="o23351" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="uncinate" value_original="uncinate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers exserted;</text>
      <biological_entity id="o23352" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth yellow or sometimes white, cylindric, (1.7–) 2–3 mm, pubescent;</text>
      <biological_entity id="o23353" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals connate ca. 2/3 their length, essentially monomorphic, narrowly oblong to narrowly obovate, occasionally with outer lobes slightly broader and longer than inner ones, entire apically;</text>
      <biological_entity id="o23354" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="2/3" value_original="2/3" />
        <character is_modifier="false" modifier="essentially" name="architecture" src="d0_s11" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s11" to="narrowly obovate" />
        <character constraint="than inner lobes" constraintid="o23356" is_modifier="false" name="length_or_size" src="d0_s11" value="slightly broader and longer" value_original="slightly broader and longer" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" notes="" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="outer" id="o23355" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="width" src="d0_s11" value="broader" value_original="broader" />
      </biological_entity>
      <biological_entity constraint="inner" id="o23356" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <relation from="o23354" id="r2605" modifier="occasionally" name="with" negation="false" src="d0_s11" to="o23355" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 9, exserted;</text>
      <biological_entity id="o23357" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="9" value_original="9" />
        <character is_modifier="false" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments connate basally into 0.2–1 mm tube, (0.3–) 0.5–2.5 mm, pilose-ciliate;</text>
      <biological_entity id="o23358" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character constraint="into tube" constraintid="o23359" is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s13" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s13" value="pilose-ciliate" value_original="pilose-ciliate" />
      </biological_entity>
      <biological_entity id="o23359" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers cream to pale-yellow, oblong, (0.2–) 0.5–0.7 mm.</text>
      <biological_entity id="o23360" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s14" to="pale-yellow" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes brown, lenticular, 1.5–2.5 mm. 2n = (38), 40, (42, 44, 46).</text>
      <biological_entity id="o23361" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23362" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="atypical_quantity" src="d0_s15" value="38" value_original="38" />
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
        <character name="quantity" src="d0_s15" value="[42" value_original="[42" />
        <character name="quantity" src="d0_s15" value="44" value_original="44" />
        <character name="quantity" src="d0_s15" value="46]" value_original="46]" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, coastal grassland, coastal sage, chaparral, and desert scrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="coastal grassland" />
        <character name="habitat" value="coastal sage" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="desert scrub communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(0-)10-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="10" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Prostrate spineflower</other_name>
  <discussion>Chorizanthe procumbens is a variable complex of widely scattered, locally infrequent to common populations that occur from the Santa Monica, San Gabriel, and San Bernardino mountains southward through western Riverside and Orange counties to San Diego County. In a strict sense, prostrate plants from San Diego southward belong to C. procumbens (including C. jonesiana) while decumbent plants to the north are C. uncinata (C. procumbens sensu G. J. Goodman 1934), if such a distinction is considered taxonomically useful. Plants with a grayish hue south of our range in Baja California have been described as C. chaetophora. All of our plants have a greenish yellow cast.</discussion>
  
</bio:treatment>