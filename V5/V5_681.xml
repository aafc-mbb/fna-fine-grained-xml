<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">342</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="(Dammer) M. E. Jones" date="unknown" rank="variety">haussknechtii</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>11: 6. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety haussknechtii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060554</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Dammer" date="unknown" rank="species">haussknechtii</taxon_name>
    <place_of_publication>
      <publication_title>Gartenflora</publication_title>
      <place_in_publication>40: 493, fig. 92. 1891 (as hausknecktii)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species haussknechtii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">umbellatum</taxon_name>
    <taxon_name authority="(Dammer) S. Stokes" date="unknown" rank="subspecies">haussknechtii</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species umbellatum;subspecies haussknechtii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, typically prostrate, sprawling mats, 0.5–1.5 × 1–4 dm.</text>
      <biological_entity id="o26350" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="typically" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o26351" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="sprawling" value_original="sprawling" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems spreading to erect, 0.3–0.6 (–1.5) dm, thinly tomentose, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o26352" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s1" to="0.6" to_unit="dm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o26353" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o26352" id="r2920" name="without" negation="false" src="d0_s1" to="o26353" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves typically in tight rosettes;</text>
      <biological_entity id="o26354" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26355" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s2" value="tight" value_original="tight" />
      </biological_entity>
      <relation from="o26354" id="r2921" name="in" negation="false" src="d0_s2" to="o26355" />
    </statement>
    <statement id="d0_s3">
      <text>blade usually broadly elliptic, 0.5–1.5 (–2.5) × 0.5–1.2 (–1.5) cm, tannish-tomentose abaxially, thinly tomentose or glabrous and olive green adaxially, margins plane.</text>
      <biological_entity id="o26356" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually broadly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.2" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="tannish-tomentose" value_original="tannish-tomentose" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="olive green" value_original="olive green" />
      </biological_entity>
      <biological_entity id="o26357" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences compact-umbellate;</text>
      <biological_entity id="o26358" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="compact-umbellate" value_original="compact-umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 0.1–1.5 (–2) cm, thinly tomentose, without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o26359" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s5" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o26360" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o26361" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o26359" id="r2922" name="without" negation="false" src="d0_s5" to="o26360" />
      <relation from="o26360" id="r2923" name="part_of" negation="false" src="d0_s5" to="o26361" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 1.5–3.5 mm, lobes 1–4 mm.</text>
      <biological_entity id="o26362" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26363" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 2–6 mm;</text>
      <biological_entity id="o26364" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth bright-yellow.</text>
      <biological_entity id="o26365" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Volcanic, sandy to gravelly slopes and ridges, mixed grassland and sagebrush communities, montane to subalpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="volcanic" />
        <character name="habitat" value="sandy to gravelly slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="montane to subalpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-2500(-3100) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1000" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3100" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107h.</number>
  <other_name type="past_name">hausknechtii</other_name>
  <other_name type="common_name">Haussknecht’s sulphur flower</other_name>
  <discussion>Variety haussknechtii, as here circumscribed, is a high-elevation taxon found mainly on volcanic peaks in north-central Oregon (Benton, Clackamas, Hood River, and Wasco counties) and south-central Washington (Kittitas and Yakima counties). It is common on Mt. Hood and Mt. Adams. It typically grows with E. marifolium, and mixed collections often are found in herbaria; the two taxa have in common a distinctive olive green color of the adaxial leaf surfaces. Haussknecht’s sulphur flower is not always clearly distinct from var. modocense. It is occasionally seen in cultivation, especially in European gardens.</discussion>
  
</bio:treatment>