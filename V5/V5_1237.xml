<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">608</other_info_on_meta>
    <other_info_on_meta type="mention_page">606</other_info_on_meta>
    <other_info_on_meta type="mention_page">607</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">plumbaginaceae</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="genus">limonium</taxon_name>
    <taxon_name authority="(Stapf) F. T. Hubbard" date="1916" rank="species">perezii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>18: 158. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plumbaginaceae;genus limonium;species perezii</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250060622</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Statice</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="species">perezii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. (Oxford)</publication_title>
      <place_in_publication>22: 116. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Statice;species perezii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves all in basal rosettes, living at anthesis, to 30 cm;</text>
      <biological_entity id="o4211" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o4212" name="rosette" name_original="rosettes" src="d0_s0" type="structure" />
      <relation from="o4211" id="r464" name="in" negation="false" src="d0_s0" to="o4212" />
    </statement>
    <statement id="d0_s1">
      <text>petiole winged distally, to 18 cm, usually exceeding blade;</text>
      <biological_entity id="o4213" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="winged" value_original="winged" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4214" name="blade" name_original="blade" src="d0_s1" type="structure" />
      <relation from="o4213" id="r465" modifier="usually" name="exceeding" negation="false" src="d0_s1" to="o4214" />
    </statement>
    <statement id="d0_s2">
      <text>blade round to broadly ovate or subcordate, to 15 × 9 cm, leathery, base subtruncate (abruptly narrowed) and then decurrent, margins entire, apex cuspidate, cusp to 5 mm, soon falling;</text>
      <biological_entity id="o4215" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s2" to="broadly ovate or subcordate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="9" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o4216" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="subtruncate" value_original="subtruncate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o4217" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4218" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o4219" name="cusp" name_original="cusp" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="soon" name="life_cycle" src="d0_s2" value="falling" value_original="falling" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>main lateral-veins pinnate.</text>
      <biological_entity constraint="main" id="o4220" name="lateral-vein" name_original="lateral-veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: axes not winged, to 100 cm × 7 mm, glabrous to puberulent (hairs ca. 0.1 mm);</text>
      <biological_entity id="o4221" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o4222" name="axis" name_original="axes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s4" to="100" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>nonflowering branches absent;</text>
      <biological_entity id="o4223" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="nonflowering" id="o4224" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spikelets moderately to densely aggregated at tips of branches, internodes mostly 2–4 mm;</text>
      <biological_entity id="o4225" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o4226" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character constraint="at tips" constraintid="o4227" is_modifier="false" modifier="moderately to densely" name="arrangement" src="d0_s6" value="aggregated" value_original="aggregated" />
      </biological_entity>
      <biological_entity id="o4227" name="tip" name_original="tips" src="d0_s6" type="structure" />
      <biological_entity id="o4228" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <biological_entity id="o4229" name="internode" name_original="internodes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o4227" id="r466" name="part_of" negation="false" src="d0_s6" to="o4228" />
    </statement>
    <statement id="d0_s7">
      <text>subtending bracts 3–6 mm, acute or aristate (outer) to truncate (inner), ciliate or fimbriate at margins, surfaces glabrous or minutely appressed-pubescent;</text>
      <biological_entity id="o4230" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="aristate" value_original="aristate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ciliate" value_original="ciliate" />
        <character constraint="at margins" constraintid="o4232" is_modifier="false" name="shape" src="d0_s7" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
      <biological_entity id="o4231" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o4232" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o4233" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s7" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <relation from="o4230" id="r467" name="subtending" negation="false" src="d0_s7" to="o4231" />
    </statement>
    <statement id="d0_s8">
      <text>flowers 1–2 per spikelet.</text>
      <biological_entity id="o4234" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o4235" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per spikelet" constraintid="o4236" from="1" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <biological_entity id="o4236" name="spikelet" name_original="spikelet" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx blue-purple in distal 1/2, with reddish-brown, glabrous ribs, funnelform;</text>
      <biological_entity id="o4237" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4238" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character constraint="in distal 1/2" constraintid="o4239" is_modifier="false" name="coloration" src="d0_s9" value="blue-purple" value_original="blue-purple" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4239" name="1/2" name_original="1/2" src="d0_s9" type="structure" />
      <biological_entity id="o4240" name="rib" name_original="ribs" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="true" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o4238" id="r468" name="with" negation="false" src="d0_s9" to="o4240" />
    </statement>
    <statement id="d0_s10">
      <text>tube ca. 5 mm, minutely pubescent along proximal end of ribs (hairs less than 0.1 mm);</text>
      <biological_entity id="o4241" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4242" name="tube" name_original="tube" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="5" value_original="5" />
        <character constraint="along proximal end" constraintid="o4243" is_modifier="false" modifier="minutely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4243" name="end" name_original="end" src="d0_s10" type="structure" />
      <biological_entity id="o4244" name="rib" name_original="ribs" src="d0_s10" type="structure" />
      <relation from="o4243" id="r469" name="part_of" negation="false" src="d0_s10" to="o4244" />
    </statement>
    <statement id="d0_s11">
      <text>lobes spreading, ca. 5 mm (5 main lobes with shallower lobes between larger lobes), or lobes indistinct and calyx appearing erose or irregularly lobed at mouth;</text>
      <biological_entity id="o4245" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4246" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o4247" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o4248" name="calyx" name_original="calyx" src="d0_s11" type="structure" />
      <biological_entity id="o4249" name="mouth" name_original="mouth" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
        <character is_modifier="true" modifier="irregularly" name="shape" src="d0_s11" value="lobed" value_original="lobed" />
      </biological_entity>
      <relation from="o4248" id="r470" name="appearing" negation="false" src="d0_s11" to="o4249" />
    </statement>
    <statement id="d0_s12">
      <text>petals whitish, barely exceeding calyx.</text>
      <biological_entity id="o4250" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o4251" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o4252" name="calyx" name_original="calyx" src="d0_s12" type="structure" />
      <relation from="o4251" id="r471" modifier="barely" name="exceeding" negation="false" src="d0_s12" to="o4252" />
    </statement>
    <statement id="d0_s13">
      <text>Utricles 4–5 mm. 2n = 14.</text>
      <biological_entity id="o4253" name="utricle" name_original="utricles" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4254" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed coastal areas, cliffs, sand dunes, roadsides (where it is sometimes planted)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed coastal areas" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Atlantic Islands (Canary Islands).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands (Canary Islands)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  
</bio:treatment>