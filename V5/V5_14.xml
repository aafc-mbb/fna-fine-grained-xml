<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Tanfani in F. Parlatore" date="unknown" rank="subfamily">Polycarpoideae</taxon_name>
    <taxon_name authority="Willdenow ex Schultes in J. J. Roemer et al." date="1819" rank="genus">drymaria</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="1913" rank="species">pachyphylla</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 121. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily polycarpoideae;genus drymaria;species pachyphylla;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060123</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, succulent, glabrous, glaucous.</text>
      <biological_entity id="o3880" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems nearly prostrate, radiating pseudo-verticillately from base, 10–20 cm.</text>
      <biological_entity id="o3881" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="nearly" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character constraint="from base" constraintid="o3882" is_modifier="false" name="arrangement" src="d0_s1" value="radiating" value_original="radiating" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3882" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves appearing whorled;</text>
    </statement>
    <statement id="d0_s3">
      <text>not stipulate;</text>
      <biological_entity id="o3883" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="stipulate" value_original="stipulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 2–8 mm;</text>
      <biological_entity id="o3884" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to suborbiculate, (0.2–) 0.5–1.3 cm × 4–10 mm, base obtuse to rounded, apex ± obtuse.</text>
      <biological_entity id="o3885" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="suborbiculate" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_length" src="d0_s5" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.3" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3886" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflores axillary, congested, 3–12-flowered umbelliform clusters.</text>
      <biological_entity id="o3887" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-12-flowered" value_original="3-12-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="umbelliform" value_original="umbelliform" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels shorter to longer than subtending bracts at maturity.</text>
      <biological_entity id="o3888" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character constraint="than subtending bracts" constraintid="o3889" is_modifier="false" name="size_or_length" src="d0_s7" value="shorter to longer" value_original="shorter to longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o3889" name="bract" name_original="bracts" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals with 3 or 5 obscure veins usually not confluent apically, oblong to broadly elliptic (herbaceous portion similar), 2–3.5 mm, subequal, apex obtuse (herbaceous portion generally acute), not hooded, glabrous;</text>
      <biological_entity id="o3890" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3891" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s8" to="broadly elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o3892" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" modifier="usually not; apically" name="arrangement" src="d0_s8" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o3893" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o3891" id="r435" name="with" negation="false" src="d0_s8" to="o3892" />
    </statement>
    <statement id="d0_s9">
      <text>petals 4-fid for 1/2 or less their length, 2.5–3 mm, 2/3–1 times as long as sepals, lobes 1-veined, vein unbranched, linear, outer pair 1/2 length of petal, each with narrower, slightly shorter lobe on inner flank, trunk laterally denticulate, base abruptly tapered, apex ± rounded.</text>
      <biological_entity id="o3894" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3895" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character constraint="for " constraintid="o3896" is_modifier="false" name="shape" src="d0_s9" value="4-fid" value_original="4-fid" />
      </biological_entity>
      <biological_entity id="o3896" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o3897" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o3898" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o3899" name="vein" name_original="vein" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="outer" id="o3900" name="flank" name_original="flank" src="d0_s9" type="structure">
        <character name="length" src="d0_s9" value="1/2 length of petal" value_original="1/2 length of petal" />
      </biological_entity>
      <biological_entity id="o3901" name="lobe" name_original="lobe" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="narrower" value_original="narrower" />
        <character is_modifier="true" modifier="slightly" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3902" name="flank" name_original="flank" src="d0_s9" type="structure" />
      <biological_entity id="o3903" name="trunk" name_original="trunk" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o3904" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o3905" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o3896" id="r436" name="for" negation="false" src="d0_s9" to="o3897" />
      <relation from="o3900" id="r437" name="with" negation="false" src="d0_s9" to="o3901" />
      <relation from="o3901" id="r438" name="on" negation="false" src="d0_s9" to="o3902" />
    </statement>
    <statement id="d0_s10">
      <text>Seeds olive green to black, teardrop-shaped (with elongate or crescent-shaped lateral thickening), 1.1–1.3 mm;</text>
      <biological_entity id="o3906" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="olive green" name="coloration" src="d0_s10" to="black" />
        <character is_modifier="false" name="shape" src="d0_s10" value="teardrop--shaped" value_original="teardrop--shaped" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tubercles marginal, minute, elongate.</text>
      <biological_entity id="o3907" name="tubercle" name_original="tubercles" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="marginal" value_original="marginal" />
        <character is_modifier="false" name="size" src="d0_s11" value="minute" value_original="minute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Heavy, saline soils, desert flats, river bottoms, playa margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saline soils" modifier="heavy" />
        <character name="habitat" value="desert flats" />
        <character name="habitat" value="river bottoms" />
        <character name="habitat" value="playa margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Durango, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Thickleaf drymary</other_name>
  <other_name type="common_name">inkweed</other_name>
  <discussion>Drymaria pachyphylla is highly toxic to livestock.</discussion>
  
</bio:treatment>