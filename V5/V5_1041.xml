<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">508</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="mention_page">507</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="Moris" date="unknown" rank="species">chrysocarpus</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Reale Accad. Sci. Torino</publication_title>
      <place_in_publication>38: 46. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species chrysocarpus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060775</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="species">berlandieri</taxon_name>
    <taxon_hierarchy>genus Rumex;species berlandieri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">langloisii</taxon_name>
    <taxon_hierarchy>genus Rumex;species langloisii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous, with vertical rootstock.</text>
      <biological_entity id="o3191" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3192" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o3191" id="r354" name="with" negation="false" src="d0_s0" to="o3192" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or erect, usually producing axillary shoots below 1st-order inflorescence or at proximal nodes, 40–60 (–80) cm.</text>
      <biological_entity id="o3193" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o3194" name="shoot" name_original="shoots" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o3195" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
      <relation from="o3193" id="r355" modifier="usually" name="producing" negation="false" src="d0_s1" to="o3194" />
      <relation from="o3193" id="r356" modifier="usually" name="below 1st-order inflorescence or at" negation="false" src="d0_s1" to="o3195" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades deep olive green, with strongly prominent veins abaxially, linear-lanceolate, occasionally lanceolate, 5–12 × 1.5–4 cm, usually ca. 3.5–5 times as long as wide, widest near or below middle, thick, coriaceous, base cuneate or rounded-cuneate, margins entire to crenulate, undulate or crisped, flat, apex subobtuse or broadly acute.</text>
      <biological_entity id="o3196" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="depth" src="d0_s2" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="olive green" value_original="olive green" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="3.5-5" value_original="3.5-5" />
        <character constraint="near or below middle veins" constraintid="o3198" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
        <character is_modifier="false" name="texture" src="d0_s2" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o3197" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="strongly" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="middle" id="o3198" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" notes="" src="d0_s2" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o3199" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded-cuneate" value_original="rounded-cuneate" />
      </biological_entity>
      <biological_entity id="o3200" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s2" to="crenulate undulate or crisped" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s2" to="crenulate undulate or crisped" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o3201" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="subobtuse" value_original="subobtuse" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o3196" id="r357" name="with" negation="false" src="d0_s2" to="o3197" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal and axillary, terminal usually occupying distal 1/3 of stem, lax, interrupted almost to top, usually broadly paniculate (branches simple or nearly so).</text>
      <biological_entity id="o3202" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character constraint="to top" constraintid="o3204" is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="usually broadly" name="arrangement" notes="" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o3203" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s3" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o3204" name="top" name_original="top" src="d0_s3" type="structure" />
      <relation from="o3202" id="r358" name="occupying" negation="false" src="d0_s3" to="o3203" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels articulated in proximal 1/3 or almost near middle, filiform or slightly thickened, especially distally 3–6 (–7) mm, not more than 2–2.5 times as long as inner tepals, articulation indistinctly swollen.</text>
      <biological_entity id="o3205" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character constraint="in " constraintid="o3207" is_modifier="false" name="architecture" src="d0_s4" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="slightly" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" modifier="especially distally; distally" name="atypical_some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" modifier="especially distally; distally" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
        <character constraint="tepal" constraintid="o3210" is_modifier="false" name="length" src="d0_s4" value="2+-2.5 times as long as inner tepals" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3206" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o3207" name="middle" name_original="middle" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o3208" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o3209" name="middle" name_original="middle" src="d0_s4" type="structure" />
      <biological_entity constraint="inner" id="o3210" name="tepal" name_original="tepals" src="d0_s4" type="structure" />
      <biological_entity id="o3211" name="articulation" name_original="articulation" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o3207" id="r359" name="in" negation="false" src="d0_s4" to="o3208" />
      <relation from="o3207" id="r360" name="in" negation="false" src="d0_s4" to="o3209" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers 5–15 in whorls;</text>
      <biological_entity id="o3212" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o3213" from="5" name="quantity" src="d0_s5" to="15" />
      </biological_entity>
      <biological_entity id="o3213" name="whorl" name_original="whorls" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>inner tepals orbiculate, ovate-deltoid, or ovate-triangular, 3.5–4.5 (–5) × 3–4 (–4.5) mm, base truncate or rarely indistinctly cordate, margins entire, apex subacute;</text>
      <biological_entity constraint="inner" id="o3214" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-deltoid" value_original="ovate-deltoid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-deltoid" value_original="ovate-deltoid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-triangular" value_original="ovate-triangular" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s6" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3215" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="rarely indistinctly" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o3216" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3217" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tubercles 3, equal or subequal, much narrower than inner tepals, free margins of inner tepals wider than or at least as wide as tubercle, verrucose to subglabrous.</text>
      <biological_entity id="o3218" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character constraint="than inner tepals" constraintid="o3219" is_modifier="false" name="width" src="d0_s7" value="much narrower" value_original="much narrower" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3219" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <biological_entity id="o3220" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" value_original="free" />
        <character constraint="as-wide-as tubercle" constraintid="o3222" is_modifier="false" name="width" src="d0_s7" value="wider" value_original="wider" />
        <character is_modifier="false" name="relief" notes="" src="d0_s7" value="verrucose" value_original="verrucose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="subglabrous" value_original="subglabrous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3221" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <biological_entity id="o3222" name="tubercle" name_original="tubercle" src="d0_s7" type="structure" />
      <relation from="o3220" id="r361" name="part_of" negation="false" src="d0_s7" to="o3221" />
    </statement>
    <statement id="d0_s8">
      <text>Achenes brown or dark reddish-brown, 2.5–3 × 1.5–2 mm. 2n = 20.</text>
      <biological_entity id="o3223" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3224" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swamps, marshes, shores, wet alluvial forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swamps" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="wet alluvial forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Tex.; ne Mexico (Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="ne Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="past_name">chrysocarpos</other_name>
  <other_name type="common_name">Amamastla dock</other_name>
  <discussion>The name Rumex floridanus was misapplied (in part) to this species by W. D. Trelease (1892) and other North American authors. Rumex chrysocarpus is distinctive and rarely confused with other species of the R. salicifolius aggregate.</discussion>
  
</bio:treatment>