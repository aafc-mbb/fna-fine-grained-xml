<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">515</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rumex Linnaeus Sect.. Rumex" date="unknown" rank="section">Rumex</taxon_name>
    <taxon_name authority="Osterhout" date="1898" rank="species">densiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>6: 13. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section rumex;species densiflorus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060777</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">polyrrhizus</taxon_name>
    <taxon_hierarchy>genus Rumex;species polyrrhizus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous or indistinctly papillose-pubescent, with creeping horizontal rhizome.</text>
      <biological_entity id="o19175" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="indistinctly" name="pubescence" src="d0_s0" value="papillose-pubescent" value_original="papillose-pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19176" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <relation from="o19175" id="r2144" name="with" negation="false" src="d0_s0" to="o19176" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched above middle (only in inflorescence), 50–100 cm.</text>
      <biological_entity id="o19177" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="above middle" constraintid="o19178" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19178" name="middle" name_original="middle" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: ocrea deciduous or partially persistent at maturity;</text>
      <biological_entity id="o19179" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19180" name="ocreum" name_original="ocrea" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character constraint="at maturity" is_modifier="false" modifier="partially" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade with large lateral-veins alternating with short ones, oblong or oblong-lanceolate, 30–40 (–50) × 10–12 cm, more than 3 times as long as wide, base broadly cuneate, truncate, or weakly cordate, margins entire or indistinctly repand, flat, apex obtuse or broadly acute.</text>
      <biological_entity id="o19181" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19182" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="50" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s3" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s3" to="12" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="3+" value_original="3+" />
      </biological_entity>
      <biological_entity id="o19183" name="lateral-vein" name_original="lateral-veins" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="large" value_original="large" />
        <character constraint="with ones" constraintid="o19184" is_modifier="false" name="arrangement" src="d0_s3" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o19184" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o19185" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o19186" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s3" value="repand" value_original="repand" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o19187" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o19182" id="r2145" name="with" negation="false" src="d0_s3" to="o19183" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, occupying distal 1/2 of stem, usually dense, narrowly paniculate.</text>
      <biological_entity id="o19188" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="usually" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="narrowly" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o19189" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <relation from="o19188" id="r2146" name="occupying" negation="false" src="d0_s4" to="o19189" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels articulated in proximal 1/3, filiform, 6–16 mm, articulation indistinct.</text>
      <biological_entity id="o19190" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="in proximal 1/3" constraintid="o19191" is_modifier="false" name="architecture" src="d0_s5" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o19191" name="1/3" name_original="1/3" src="d0_s5" type="structure" />
      <biological_entity id="o19192" name="articulation" name_original="articulation" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 10–20 in whorls;</text>
      <biological_entity id="o19193" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o19194" from="10" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o19194" name="whorl" name_original="whorls" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>inner tepals ovate-triangular or subcordate, 5–6 × 4.5–6 mm, widest at or near middle, base weakly emarginate, margins entire, erose, or indistinctly denticulate mostly at base, apex abruptly narrowed, acute or subacute;</text>
      <biological_entity constraint="inner" id="o19195" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subcordate" value_original="subcordate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
        <character constraint="at or near base" constraintid="o19196" is_modifier="false" name="width" src="d0_s7" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o19196" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="true" name="position" src="d0_s7" value="middle" value_original="middle" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s7" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o19197" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
        <character constraint="at base" constraintid="o19198" is_modifier="false" modifier="indistinctly" name="shape" src="d0_s7" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o19198" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o19199" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s7" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tubercles absent.</text>
      <biological_entity id="o19200" name="tubercle" name_original="tubercles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes deep brown to reddish-brown, 2.5–4 (–4.5) × 1.8–2.5 mm. 2n = 120.</text>
      <biological_entity id="o19201" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="deep" value_original="deep" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s9" to="reddish-brown" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19202" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along streams and rivers in montane, subalpine, and alpine zones</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" modifier="along" constraint="in montane , subalpine , and alpine zones" />
        <character name="habitat" value="rivers" constraint="in montane , subalpine , and alpine zones" />
        <character name="habitat" value="montane" />
        <character name="habitat" value="subalpine" />
        <character name="habitat" value="alpine zones" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-3000(-3500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <other_name type="common_name">Dense-flowered dock</other_name>
  <discussion>The following three species are closely related to Rumex densiflorus, all belonging to subsect. Densiflori Rechinger f., and possibly form one polymorphic “macrospecies” (K. H. Rechinger 1937). Á. Löve (1986) treated R. orthoneurus and R. pycnanthus as subspecies of R. densiflorus. However, the variability of this aggregate is insufficiently known, and I prefer to treat it as consisting of four “microspecies.”</discussion>
  <discussion>Rumex densiflorus is reported from northwestern New Mexico (W. C. Martin and C. R. Hutchins 1980), where it most probably occurs; records for southern Idaho (R. J. Davis 1952) and Arizona (J. H. Lehr 1978) need confirmation.</discussion>
  
</bio:treatment>