<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">500</other_info_on_meta>
    <other_info_on_meta type="mention_page">490</other_info_on_meta>
    <other_info_on_meta type="mention_page">501</other_info_on_meta>
    <other_info_on_meta type="illustration_page">499</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="(Meisner) Rechinger f." date="1937" rank="subgenus">Acetosella</taxon_name>
    <taxon_name authority="Jurtzev &amp; V. V. Petrovsky" date="1973" rank="species">beringensis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zhurn. (Moscow &amp; Leningrad)</publication_title>
      <place_in_publication>58: 1745. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus acetosella;species beringensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250060770</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acetosella</taxon_name>
    <taxon_name authority="(Jurtzev &amp; V. V. Petrovsky) Á. Löve &amp; D. Löve" date="unknown" rank="species">beringensis</taxon_name>
    <taxon_hierarchy>genus Acetosella;species beringensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous, with thick, densely tufted underground stolons.</text>
      <biological_entity id="o30763" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o30764" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="true" modifier="densely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="true" name="location" src="d0_s0" value="underground" value_original="underground" />
      </biological_entity>
      <relation from="o30763" id="r3474" name="with" negation="false" src="d0_s0" to="o30764" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, rarely ascending, several from base, branched in inflorescence, 5–15 (–20) cm;</text>
      <biological_entity id="o30765" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="from base" constraintid="o30766" is_modifier="false" name="quantity" src="d0_s1" value="several" value_original="several" />
        <character constraint="in inflorescence" constraintid="o30767" is_modifier="false" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30766" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o30767" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>shoots usually densely crowded, not elongated.</text>
      <biological_entity id="o30768" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually densely" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="not" name="length" src="d0_s2" value="elongated" value_original="elongated" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: ocrea brownish or reddish-brown, membranous;</text>
      <biological_entity id="o30769" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o30770" name="ocreum" name_original="ocrea" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear or spatulate-lanceolate, not hastate (without basal lobes), 1.5–5 × 0.1–0.3 cm, base narrowly cuneate (gradually narrowing into petiole), margins entire, flat or slightly convolute, apex obtuse or subacute.</text>
      <biological_entity id="o30771" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o30772" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate-lanceolate" value_original="spatulate-lanceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="hastate" value_original="hastate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s4" to="0.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30773" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o30774" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="convolute" value_original="convolute" />
      </biological_entity>
      <biological_entity id="o30775" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, occupying more than distal 1/2 of stem, ± dense, usually interrupted at least near base, narrowly paniculate with branches directed upward.</text>
      <biological_entity id="o30776" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="more or less" name="density" notes="" src="d0_s5" value="dense" value_original="dense" />
        <character constraint="near base" constraintid="o30779" is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character constraint="with branches" constraintid="o30780" is_modifier="false" modifier="narrowly" name="arrangement" notes="" src="d0_s5" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o30777" name="1/2" name_original="1/2" src="d0_s5" type="structure" />
      <biological_entity id="o30778" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <biological_entity id="o30779" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o30780" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="directed" value_original="directed" />
      </biological_entity>
      <relation from="o30777" id="r3475" name="part_of" negation="false" src="d0_s5" to="o30778" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–4 mm.</text>
      <biological_entity id="o30781" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers (3–) 4–7 in whorls;</text>
      <biological_entity id="o30782" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s7" to="4" to_inclusive="false" />
        <character char_type="range_value" constraint="in whorls" constraintid="o30783" from="4" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
      <biological_entity id="o30783" name="whorl" name_original="whorls" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>inner tepals distinctly enlarged, 1.6–2.3 × 1.8–2.5 mm (free wing 0.3–0.5 mm wide), base cuneate, apex obtuse or subacute.</text>
      <biological_entity constraint="inner" id="o30784" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distinctly" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s8" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30785" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o30786" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes brown or reddish-brown, 1–1.5 × 0.8–1.2 mm. 2n = 14.</text>
      <biological_entity id="o30787" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s9" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30788" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy and gravelly soil, shores, limestone outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soil" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="limestone outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska; ne Asia (ne Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="ne Asia (ne Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Beringia or Bering Sea sorrel</other_name>
  <discussion>The name Rumex graminifolius was commonly misapplied to this species in both northwestern North America and northeastern Eurasia.</discussion>
  
</bio:treatment>