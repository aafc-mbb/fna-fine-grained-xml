<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">282</other_info_on_meta>
    <other_info_on_meta type="mention_page">225</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="S. Watson" date="1889" rank="species">gracilipes</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>24: 85. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species gracilipes;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060302</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Porter ex S. Watson" date="unknown" rank="species">kennedyi</taxon_name>
    <taxon_name authority="(S. Watson) S. Stokes" date="unknown" rank="subspecies">gracilipes</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species kennedyi;subspecies gracilipes;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">ochrocephalum</taxon_name>
    <taxon_name authority="(S. Watson) J. T. Howell" date="unknown" rank="variety">gracilipes</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species ochrocephalum;variety gracilipes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, matted, scapose, 0.5–1 × 0.5–2 dm, glandular-hairy, greenish.</text>
      <biological_entity id="o14175" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="1" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems matted, with persistent leaf-bases, up to 1/5 height of plant;</text>
      <biological_entity id="o14176" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="matted" value_original="matted" />
      </biological_entity>
      <biological_entity id="o14177" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/5 height of plant" />
      </biological_entity>
      <relation from="o14176" id="r1540" name="with" negation="false" src="d0_s1" to="o14177" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems matted;</text>
      <biological_entity constraint="caudex" id="o14178" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems scapelike, erect, slender, solid, not fistulose, (0.2–) 0.3–1 (–1.2) dm, glandular-hairy.</text>
      <biological_entity id="o14179" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="scapelike" value_original="scapelike" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="0.3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1.2" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s3" to="1" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal, fasciculate in terminal tufts;</text>
      <biological_entity id="o14180" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="in terminal tufts" constraintid="o14181" is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o14181" name="tuft" name_original="tufts" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole (0.5–) 0.8–1.7 (–2) cm, thinly tomentose, glandular;</text>
      <biological_entity id="o14182" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s5" to="1.7" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblanceolate to elliptic, (0.3–) 1–1.5 (–2) × (0.2–) 0.3–0.6 cm, densely white-tomentose and glandular abaxially, less so and greenish adaxially, margins plane.</text>
      <biological_entity id="o14183" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="elliptic" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_length" src="d0_s6" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s6" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="0.6" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="less; adaxially" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o14184" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences capitate, 1–2 cm;</text>
      <biological_entity id="o14185" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches absent;</text>
      <biological_entity id="o14186" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3–5, lanceolate, scalelike, 1.5–3 mm.</text>
      <biological_entity id="o14187" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="5" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o14188" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 5–7 per cluster, turbinate to campanulate, 2–4 × 2–3 mm, membranous, sparsely glandular to glandular-puberulent or pubescent;</text>
      <biological_entity id="o14189" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per cluster" from="5" name="quantity" src="d0_s11" to="7" />
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s11" to="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="membranous" value_original="membranous" />
        <character char_type="range_value" from="sparsely glandular" name="pubescence" src="d0_s11" to="glandular-puberulent or pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect to spreading, 1–1.5 mm.</text>
      <biological_entity id="o14190" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s12" to="spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 2–3 mm;</text>
      <biological_entity id="o14191" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white to rose, glandular;</text>
      <biological_entity id="o14192" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="rose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s14" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, monomorphic, obovate;</text>
      <biological_entity id="o14193" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 2–3.5 (–4) mm;</text>
      <biological_entity id="o14194" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o14195" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 2–2.3 (–2.5) mm, glabrous.</text>
      <biological_entity id="o14196" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="2.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Granitic, sandstone or limestone sandy to gravelly outcrops, slopes, and ridge tops, high-elevation sagebrush communities, subalpine and alpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly outcrops" modifier="granitic" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="ridge tops" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="limestone sandy to gravelly outcrops" />
        <character name="habitat" value="high-elevation sagebrush communities" />
        <character name="habitat" value="subalpine" />
        <character name="habitat" value="alpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2900-3900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3900" to_unit="m" from="2900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>59.</number>
  <other_name type="common_name">White Mountains wild buckwheat</other_name>
  <discussion>Eriogonum gracilipes is generally found with Great Basin bristlecone pine (Pinus longaeva D. K. Bailey) and foxtail pine (Pinus balfouriana S. Watson) in the higher elevations of the White Mountains and on the eastern edge of the Sierra Nevada immediately to the west. The White Mountains wild buckwheat is restricted to Mono and Inyo counties, California, and just enters Esmeralda County in Nevada. It is occasionally seen in cultivation.</discussion>
  
</bio:treatment>