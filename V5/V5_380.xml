<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="mention_page">194</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="C. L. Hitchcock &amp; Maguire" date="1947" rank="species">invisa</taxon_name>
    <place_of_publication>
      <publication_title>Revis. N. Amer. Silene,</publication_title>
      <place_in_publication>31, plate 4, fig. 25. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species invisa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060857</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o9567" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot stout;</text>
      <biological_entity id="o9568" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex becoming branched, bearing tufts of leaves.</text>
      <biological_entity id="o9569" name="caudex" name_original="caudex" src="d0_s2" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" modifier="becoming" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o9570" name="tuft" name_original="tufts" src="d0_s2" type="structure" />
      <biological_entity id="o9571" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o9569" id="r1063" name="bearing" negation="false" src="d0_s2" to="o9570" />
      <relation from="o9569" id="r1064" name="part_of" negation="false" src="d0_s2" to="o9571" />
    </statement>
    <statement id="d0_s3">
      <text>Stems several, erect, unbranched proximal to inflorescence, 10–40 cm, puberulent.</text>
      <biological_entity id="o9572" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character constraint="to inflorescence" constraintid="o9573" is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o9573" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly basal, petiolate, blade oblanceolate or spatulate, 1.5–5 cm × 2–6 mm, apex acute, glabrous except for a few cilia on petiole;</text>
      <biological_entity id="o9574" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o9575" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o9576" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9577" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character constraint="except-for cilia" constraintid="o9578" is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9578" name="cilium" name_original="cilia" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o9579" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <relation from="o9578" id="r1065" name="on" negation="false" src="d0_s4" to="o9579" />
    </statement>
    <statement id="d0_s5">
      <text>cauline leaves in 2–4 pairs, reduced distally, blade linear to narrowly lanceolate or oblanceolate, 2–7 cm × 2–6 mm.</text>
      <biological_entity constraint="cauline" id="o9580" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o9581" name="pair" name_original="pairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o9582" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate or oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o9580" id="r1066" name="in" negation="false" src="d0_s5" to="o9581" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymose, 1–3-flowered, open, bracteate;</text>
      <biological_entity id="o9583" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-3-flowered" value_original="1-3-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cyme 1, terminal, often with 1 flower at next node;</text>
      <biological_entity id="o9584" name="cyme" name_original="cyme" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o9585" name="flower" name_original="flower" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="next" id="o9586" name="node" name_original="node" src="d0_s7" type="structure" />
      <relation from="o9584" id="r1067" modifier="often" name="with" negation="false" src="d0_s7" to="o9585" />
      <relation from="o9585" id="r1068" name="at" negation="false" src="d0_s7" to="o9586" />
    </statement>
    <statement id="d0_s8">
      <text>bracts linear-lanceolate, 5–20 mm.</text>
      <biological_entity id="o9587" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, from 0.5 cm, lengthening to 3 cm in fruit, gray and somewhat retrorse-puberulent with stipitate-glandular hairs.</text>
      <biological_entity id="o9588" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="from 0.5 cm" name="length" src="d0_s9" value="lengthening" value_original="lengthening" />
        <character char_type="range_value" constraint="in fruit" constraintid="o9589" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="3" to_unit="cm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="gray" value_original="gray" />
        <character constraint="with hairs" constraintid="o9590" is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s9" value="retrorse-puberulent" value_original="retrorse-puberulent" />
      </biological_entity>
      <biological_entity id="o9589" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o9590" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx prominently 10-veined, veins parallel, green, with pale commissures, broadened in lobes, narrowly campanulate and 7–11 × 3–4 mm in flower, campanulate and 8–12 × 4–5 mm in fruit, tending to broaden proximally, glandular-puberulent, lobes 5, erect, lanceolate, 1–2 mm, apex blunt;</text>
      <biological_entity id="o9591" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9592" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s10" value="10-veined" value_original="10-veined" />
      </biological_entity>
      <biological_entity id="o9593" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" constraint="in flower" constraintid="o9596" from="3" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o9597" from="4" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9594" name="commissure" name_original="commissures" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="pale" value_original="pale" />
        <character constraint="in lobes" constraintid="o9595" is_modifier="false" name="width" notes="" src="d0_s10" value="broadened" value_original="broadened" />
      </biological_entity>
      <biological_entity id="o9595" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o9596" name="flower" name_original="flower" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o9597" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity id="o9598" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="proximally" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o9599" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="blunt" value_original="blunt" />
      </biological_entity>
      <relation from="o9593" id="r1069" name="with" negation="false" src="d0_s10" to="o9594" />
      <relation from="o9593" id="r1070" name="tending to broaden" negation="false" src="d0_s10" to="o9598" />
    </statement>
    <statement id="d0_s11">
      <text>petals cream to pink, often tinged with dusky purple, slightly longer than calyx, limb 1–2 mm, unlobed or apex notched;</text>
      <biological_entity id="o9600" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9601" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="tinged with dusky purple" value_original="tinged with dusky purple" />
        <character constraint="than calyx" constraintid="o9602" is_modifier="false" name="length_or_size" src="d0_s11" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o9602" name="calyx" name_original="calyx" src="d0_s11" type="structure" />
      <biological_entity id="o9603" name="limb" name_original="limb" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o9604" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens equaling calyx;</text>
      <biological_entity id="o9605" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9606" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o9607" name="calyx" name_original="calyx" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas 3, equaling calyx.</text>
      <biological_entity id="o9608" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o9609" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o9610" name="calyx" name_original="calyx" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules narrowly ovoid, 10–13 mm, slightly longer than calyx, opening with 6 outwardly curved teeth;</text>
      <biological_entity id="o9611" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="13" to_unit="mm" />
        <character constraint="than calyx" constraintid="o9612" is_modifier="false" name="length_or_size" src="d0_s14" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o9612" name="calyx" name_original="calyx" src="d0_s14" type="structure" />
      <biological_entity id="o9613" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="true" modifier="outwardly" name="course" src="d0_s14" value="curved" value_original="curved" />
      </biological_entity>
      <relation from="o9611" id="r1071" name="opening with" negation="false" src="d0_s14" to="o9613" />
    </statement>
    <statement id="d0_s15">
      <text>carpophore shorter than 1 mm.</text>
      <biological_entity id="o9614" name="carpophore" name_original="carpophore" src="d0_s15" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s15" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds brown, reniform, angular, 0.7–1 mm, margins with large, balloonlike papillae, sides rugose.</text>
      <biological_entity id="o9615" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="angular" value_original="angular" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9616" name="margin" name_original="margins" src="d0_s16" type="structure" />
      <biological_entity id="o9617" name="papilla" name_original="papillae" src="d0_s16" type="structure">
        <character is_modifier="true" name="size" src="d0_s16" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s16" value="balloonlike" value_original="balloonlike" />
      </biological_entity>
      <relation from="o9616" id="r1072" name="with" negation="false" src="d0_s16" to="o9617" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 48.</text>
      <biological_entity id="o9618" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9619" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist openings in coniferous forests on mountain slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" modifier="moist" constraint="in coniferous forests" />
        <character name="habitat" value="coniferous forests" modifier="in" />
        <character name="habitat" value="mountain slopes" modifier="on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <other_name type="common_name">Short-petalled campion</other_name>
  <discussion>Silene invisa is found in the Cascades and Sierra Nevada. It is a rare species very similar to S. drummondii, from which it can usually be distinguished by its smaller size, glabrous leaves, and the large, inflated papillae of the seeds. Plants with intermediate characters occur in Nevada and Arizona.</discussion>
  
</bio:treatment>