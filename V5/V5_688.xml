<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">344</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="Reveal" date="1968" rank="variety">vernum</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>28: 157. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety vernum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060576</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, dome-shaped, 3–6 (–9) × 3–9 (–13) dm.</text>
      <biological_entity id="o7523" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="dome--shaped" value_original="dome--shaped" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="9" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="length" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="13" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="9" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, 0.5–1.5 dm, floccose or glabrous, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o7524" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7525" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o7524" id="r803" name="without" negation="false" src="d0_s1" to="o7525" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in rather open rosettes;</text>
      <biological_entity id="o7526" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7527" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="rather" name="architecture" src="d0_s2" value="open" value_original="open" />
      </biological_entity>
      <relation from="o7526" id="r804" name="in" negation="false" src="d0_s2" to="o7527" />
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic, 0.5–2.5 × 0.3–1 cm, thinly floccose abaxially, less so to thinly floccose or glabrous and green adaxially, with some blades glabrous on both surfaces, margins plane.</text>
      <biological_entity id="o7528" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="thinly; abaxially" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
        <character is_modifier="false" modifier="less; thinly" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o7529" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character constraint="on surfaces" constraintid="o7530" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7530" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o7531" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
      <relation from="o7528" id="r805" name="with" negation="false" src="d0_s3" to="o7529" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences umbellate;</text>
      <biological_entity id="o7532" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 3–8 cm, mostly glabrous, without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o7533" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7534" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o7535" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o7533" id="r806" name="without" negation="false" src="d0_s5" to="o7534" />
      <relation from="o7534" id="r807" name="part_of" negation="false" src="d0_s5" to="o7535" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 1.5–2.5 mm, lobes 2–3 mm.</text>
      <biological_entity id="o7536" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7537" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers (5–) 6–9 (–10) mm;</text>
      <biological_entity id="o7538" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth pale to bright-yellow.</text>
      <biological_entity id="o7539" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly, often volcanic flats and slopes, saltbush and sagebrush communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly" />
        <character name="habitat" value="volcanic flats" modifier="often" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="sagebrush communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2000(-2200) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1400" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2200" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107o.</number>
  <other_name type="common_name">Spring-flowering sulphur flower</other_name>
  <discussion>Variety vernum is known from several scattered populations in northern Nye County. It flowers mainly in May and early June, with some fruit-bearing flowers persisting into early July. Flower color can vary from bright to pale yellow in a single population. The taxon clearly is related to var. nevadense, which in central Nevada occurs at higher elevations (mainly in pinyon-juniper communities). The shrubs certainly are worthy of cultivation.</discussion>
  
</bio:treatment>