<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">22</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="illustration_page">21</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Tanfani in F. Parlatore" date="unknown" rank="subfamily">Polycarpoideae</taxon_name>
    <taxon_name authority="(Persoon) J. Presl &amp; C. Presl" date="unknown" rank="genus">spergularia</taxon_name>
    <taxon_name authority="(Persoon) Cambessèdes in A. St.-Hilaire et al." date="1830" rank="species">villosa</taxon_name>
    <taxon_hierarchy>family caryophyllaceae;subfamily polycarpoideae;genus spergularia;species villosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250012420</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spergula</taxon_name>
    <taxon_name authority="Persoon" date="unknown" rank="species">villosa</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>1: 522. 1805</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>in A. St.-Hilaire et al.,Fl. Bras. Merid.</publication_title>
      <place_in_publication>2: 129. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spergula;species villosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants strongly perennial with branched, woody base, stout, 11–30 cm, stipitate-glandular in inflorescence or throughout.</text>
      <biological_entity id="o14524" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="strongly" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots becoming stout, woody.</text>
      <biological_entity id="o14525" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="becoming" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending, often arcuately so, much-branched proximally;</text>
      <biological_entity id="o14526" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" modifier="often arcuately; arcuately; proximally" name="architecture" src="d0_s2" value="much-branched" value_original="much-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>main-stem 0.4–1.3 mm diam. proximally.</text>
      <biological_entity id="o14527" name="main-stem" name_original="main-stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" modifier="proximally" name="diameter" src="d0_s3" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules ± conspicuous, dull white, broadly lance-acuminate, 3–8 mm, apex mucronate;</text>
      <biological_entity id="o14528" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14529" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="lance-acuminate" value_original="lance-acuminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14530" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade filiform to linear, 1–4.2 cm, somewhat fleshy, apex apiculate or spine-tipped;</text>
      <biological_entity id="o14531" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o14532" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="4.2" to_unit="cm" />
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o14533" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>axillary leaves 2–4 per cluster.</text>
      <biological_entity id="o14534" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="axillary" id="o14535" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per cluster" from="2" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cymes simple to 3-compound.</text>
      <biological_entity id="o14536" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character char_type="range_value" from="simple" name="architecture" src="d0_s7" to="3-compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels spreading to reflexed in fruit.</text>
      <biological_entity id="o14537" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o14538" from="spreading" name="orientation" src="d0_s8" to="reflexed" />
      </biological_entity>
      <biological_entity id="o14538" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals connate 0.5–0.7 mm proximally, lobes 1-veined or 3-veined, lanceovate to lanceolate, 2.5–4 mm, to 5 mm in fruit, margins 0.1–0.6 mm wide, apex acute to acuminate but often briefly rounded at tip;</text>
      <biological_entity id="o14539" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14540" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character char_type="range_value" from="0.5" from_unit="mm" modifier="proximally" name="some_measurement" src="d0_s9" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14541" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s9" to="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o14542" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14542" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o14543" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s9" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14544" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
        <character constraint="at tip" constraintid="o14545" is_modifier="false" modifier="often briefly" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o14545" name="tip" name_original="tip" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals white, ± elliptic, 0.7–0.8 times as long as sepal;</text>
      <biological_entity id="o14546" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14547" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character constraint="sepal" constraintid="o14548" is_modifier="false" name="length" src="d0_s10" value="0.7-0.8 times as long as sepal" />
      </biological_entity>
      <biological_entity id="o14548" name="sepal" name_original="sepal" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 7–10;</text>
      <biological_entity id="o14549" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14550" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 0.4–0.6 mm.</text>
      <biological_entity id="o14551" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14552" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules greenish to tan, (4–) 5–6.5 mm, 1.1–1.3 times as long as sepals.</text>
      <biological_entity id="o14553" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s13" to="tan" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="6.5" to_unit="mm" />
        <character constraint="sepal" constraintid="o14554" is_modifier="false" name="length" src="d0_s13" value="1.1-1.3 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o14554" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds reddish-brown to dark-brown, often with submarginal groove, broadly ovate, plump, 0.4–0.5 mm, smooth, often sculptured with parallel, wavy lines, papillae often present;</text>
      <biological_entity id="o14555" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s14" to="dark-brown" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="size" src="d0_s14" value="plump" value_original="plump" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character constraint="with lines" constraintid="o14557" is_modifier="false" modifier="often" name="relief" src="d0_s14" value="sculptured" value_original="sculptured" />
      </biological_entity>
      <biological_entity constraint="submarginal" id="o14556" name="groove" name_original="groove" src="d0_s14" type="structure" />
      <biological_entity id="o14557" name="line" name_original="lines" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
        <character is_modifier="true" name="shape" src="d0_s14" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o14558" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o14555" id="r1586" modifier="often" name="with" negation="false" src="d0_s14" to="o14556" />
    </statement>
    <statement id="d0_s15">
      <text>wing often present, white, 0.1–0.2 mm wide, margins irregular.</text>
      <biological_entity id="o14559" name="wing" name_original="wing" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s15" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14560" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_course" src="d0_s15" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy slopes and bluffs, clay ridges and plains, disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy slopes" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="clay ridges" />
        <character name="habitat" value="plains" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Oreg.; Mexico (Baja California); South America (Chile?).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="South America (Chile?)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Hairy sand-spurrey</other_name>
  
</bio:treatment>