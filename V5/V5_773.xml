<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">pyrolifolium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">pyrolifolium</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species pyrolifolium;variety pyrolifolium;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060475</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial flowering-stems mostly ascending, 0.3–1 dm.</text>
      <biological_entity id="o10117" name="flowering-stem" name_original="flowering-stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s0" to="1" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades glabrous on both surfaces.</text>
      <biological_entity id="o10118" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character constraint="on surfaces" constraintid="o10119" is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10119" name="surface" name_original="surfaces" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Perianths sparsely pilose, with glandular-hairs evident.</text>
      <biological_entity id="o10120" name="perianth" name_original="perianths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o10121" name="glandular-hair" name_original="glandular-hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o10120" id="r1133" name="with" negation="false" src="d0_s2" to="o10121" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly, usually volcanic flats, slopes, and ridges, mixed grassland, sagebrush, and mountain meadow communities, oak, montane, and subalpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly" />
        <character name="habitat" value="volcanic flats" modifier="usually" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="ridges" modifier="and" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="mountain meadow communities" modifier="and" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="montane" />
        <character name="habitat" value="subalpine conifer woodlands" modifier="and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(800-)1600-3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="1600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3300" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>137b.</number>
  <other_name type="common_name">Shasta wild buckwheat</other_name>
  <discussion>Variety pyrolifolium is widely scattered; its range overlaps to a considerable degree with that of var. coryphaeum. In the Cascade Range, var. pyrolifolium occurs only in Kittitas County, Washington, and on Little Mt. Hoffman, Mt. Lassen, and Mt. Shasta in Siskiyou and Shasta counties, California. Only in Blaine, Boise, Custer, Elmore, Lemhi, and Valley counties of central Idaho are the plants relatively common, and then they nearly always occur with var. coryphaeum.</discussion>
  
</bio:treatment>