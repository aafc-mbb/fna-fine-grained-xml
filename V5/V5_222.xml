<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">stellaria</taxon_name>
    <taxon_name authority="Torrey in War Department [U.S.]" date="1857" rank="species">littoralis</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 69. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus stellaria;species littoralis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060933</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, straggling to scandent, from elongate rhizomes.</text>
      <biological_entity id="o24855" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="straggling" name="growth_form" src="d0_s0" to="scandent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24856" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o24855" id="r2765" name="from" negation="false" src="d0_s0" to="o24856" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, often decumbent at base, branched, 4-sided, 10–60 cm, uniformly and softly pubescent.</text>
      <biological_entity id="o24857" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="at base" constraintid="o24858" is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="uniformly; softly" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24858" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile;</text>
      <biological_entity id="o24859" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to ovatelanceolate, widest proximal to middle, 1–4.5 cm × 4–20 mm, base round, margins densely ciliate, apex shortly acuminate, pubescent on both surfaces.</text>
      <biological_entity id="o24860" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="ovatelanceolate" />
        <character is_modifier="false" name="position" src="d0_s3" value="widest" value_original="widest" />
        <character char_type="range_value" from="proximal" name="position" src="d0_s3" to="middle" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24861" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o24862" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o24863" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character constraint="on surfaces" constraintid="o24864" is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24864" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, 5–many-flowered, leafy cymes;</text>
      <biological_entity id="o24865" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-many-flowered" value_original="5-many-flowered" />
      </biological_entity>
      <biological_entity id="o24866" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts foliaceous, 4–40 mm, margins ciliate, not scarious.</text>
      <biological_entity id="o24867" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24868" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels ascending to erect, straight, spreading to reflexed at base in fruit, 5–20 mm.</text>
      <biological_entity id="o24869" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character char_type="range_value" constraint="at base" constraintid="o24870" from="spreading" name="orientation" src="d0_s6" to="reflexed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24870" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o24871" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <relation from="o24870" id="r2766" name="in" negation="false" src="d0_s6" to="o24871" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 9–10 mm diam.;</text>
      <biological_entity id="o24872" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals (4–) 5, 3-veined, lanceolate, 2.8–5 mm, margins narrow, scarious, apex acuminate, ciliate-pubescent mainly on margins and veins;</text>
      <biological_entity id="o24873" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24874" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o24875" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character constraint="on veins" constraintid="o24877" is_modifier="false" name="pubescence" src="d0_s8" value="ciliate-pubescent" value_original="ciliate-pubescent" />
      </biological_entity>
      <biological_entity id="o24876" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <biological_entity id="o24877" name="vein" name_original="veins" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>petals 5, 4–6 mm, equaling or slightly longer than sepals, blade apex deeply 2-fid;</text>
      <biological_entity id="o24878" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
        <character constraint="than sepals" constraintid="o24879" is_modifier="false" name="length_or_size" src="d0_s9" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o24879" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity constraint="blade" id="o24880" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s9" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 10;</text>
      <biological_entity id="o24881" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3, ascending, ca. 1.5 mm.</text>
      <biological_entity id="o24882" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules green to straw colored, lanceoloid-ovoid, 5–6 mm, slightly longer than sepals, apex obtuse, opening by 3, tardily 6, ascending valves;</text>
      <biological_entity id="o24883" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s12" to="straw colored" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceoloid-ovoid" value_original="lanceoloid-ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character constraint="than sepals" constraintid="o24884" is_modifier="false" name="length_or_size" src="d0_s12" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o24884" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o24885" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24886" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="true" modifier="tardily" name="quantity" src="d0_s12" value="6" value_original="6" />
        <character is_modifier="true" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o24885" id="r2767" name="opening by" negation="false" src="d0_s12" to="o24886" />
    </statement>
    <statement id="d0_s13">
      <text>carpophore absent.</text>
      <biological_entity id="o24887" name="carpophore" name_original="carpophore" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds reddish-brown, broadly and obliquely ovate, ± 1 mm diam., minutely rugose.</text>
      <biological_entity id="o24888" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="broadly; obliquely" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character modifier="more or less" name="diameter" src="d0_s14" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshy fields, marshes, coastal bluffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshy fields" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="coastal bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>less than 100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="100" from_unit="m" constraint="less than " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Beach starwort or chickweed</other_name>
  <discussion>Stellaria littoralis is very similar to S. dichotoma Linnaeus from China, the Russian Far East, and Siberia. It may be conspecific with the latter and may have been introduced into the San Francisco area in the early days of exploration of the Pacific coast. A more detailed study is warranted.</discussion>
  
</bio:treatment>