<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">430</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="mention_page">323</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Reveal &amp; J. T. Howell" date="1976" rank="genus">DEDECKERA</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>28: 245, fig. 1. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus dedeckera;</taxon_hierarchy>
    <other_info_on_name type="etymology">for Mary Caroline DeDecker, 1909–2000, noted California conservationist</other_info_on_name>
    <other_info_on_name type="fna_id">109462</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs;</text>
      <biological_entity id="o23259" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot stout.</text>
      <biological_entity id="o23260" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems spreading, woody, hirsutulous;</text>
      <biological_entity id="o23261" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex absent;</text>
      <biological_entity id="o23262" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>aerial flowering-stems spreading, herbaceous, hirsutulous.</text>
      <biological_entity id="o23263" name="flowering-stem" name_original="flowering-stems" src="d0_s4" type="structure">
        <character is_modifier="true" name="location" src="d0_s4" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves usually persistent through anthesis, 1 per node, cauline, alternate;</text>
      <biological_entity id="o23264" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="through anthesis node, leaves" constraintid="o23265, o23266" is_modifier="false" modifier="usually" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s5" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o23265" name="node" name_original="node" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="through" name="life_cycle" src="d0_s5" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o23266" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="through" name="life_cycle" src="d0_s5" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o23267" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>petiole present, blade narrow to broadly elliptic, margins entire.</text>
      <biological_entity id="o23268" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23269" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o23270" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, cymose;</text>
      <biological_entity id="o23271" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, not brittle or disarticulating into segments, round, hirsutulous, each node with 1 short-pedicellate flower and 4–12 sessile or subsessile flowers;</text>
      <biological_entity id="o23272" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s8" value="brittle" value_original="brittle" />
        <character constraint="into segments" constraintid="o23273" is_modifier="false" name="architecture" src="d0_s8" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
      <biological_entity id="o23273" name="segment" name_original="segments" src="d0_s8" type="structure" />
      <biological_entity id="o23274" name="node" name_original="node" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o23275" name="flower" name_original="flower" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="short-pedicellate" value_original="short-pedicellate" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
      <biological_entity id="o23276" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <relation from="o23274" id="r2591" name="with" negation="false" src="d0_s8" to="o23275" />
    </statement>
    <statement id="d0_s9">
      <text>bracts 3–4 at proximal nodes and leaflike, 2–3 at distal nodes and scalelike, distinct, hirsutulous.</text>
      <biological_entity id="o23277" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="at proximal nodes" constraintid="o23278" from="3" name="quantity" src="d0_s9" to="4" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s9" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" constraint="at distal nodes" constraintid="o23279" from="2" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23278" name="node" name_original="nodes" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o23279" name="node" name_original="nodes" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Peduncles erect, slender.</text>
      <biological_entity id="o23280" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucral-bracts obscure, in 2 whorls of 3 plus 2 lobes at proximal nodes, distinct, reduced to 2 lobes at distal nodes, not awn-tipped.</text>
      <biological_entity id="o23281" name="involucral-bract" name_original="involucral-bracts" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s11" value="distinct" value_original="distinct" />
        <character constraint="to lobes" constraintid="o23285" is_modifier="false" name="size" src="d0_s11" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s11" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity id="o23282" name="whorl" name_original="whorls" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o23283" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23284" name="node" name_original="nodes" src="d0_s11" type="structure" />
      <biological_entity id="o23285" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23286" name="node" name_original="nodes" src="d0_s11" type="structure" />
      <relation from="o23281" id="r2592" name="in" negation="false" src="d0_s11" to="o23282" />
      <relation from="o23282" id="r2593" name="part_of" negation="false" src="d0_s11" to="o23283" />
      <relation from="o23282" id="r2594" name="at" negation="false" src="d0_s11" to="o23284" />
      <relation from="o23285" id="r2595" name="at" negation="false" src="d0_s11" to="o23286" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers 4–6 per involucral cluster, sessile or pedicellate;</text>
      <biological_entity id="o23287" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per involucral cluster" from="4" name="quantity" src="d0_s12" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth yellow to reddish yellow, broadly campanulate when open, narrowly urceolate when closed, hispidulous abaxially;</text>
      <biological_entity id="o23288" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="reddish yellow" />
        <character is_modifier="false" modifier="when open" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="when closed" name="shape" src="d0_s13" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s13" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals 6, slightly dimorphic, those of outer whorl broader and longer than those of inner whorl, entire apically;</text>
      <biological_entity id="o23289" name="tepal" name_original="tepals" src="d0_s14" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" modifier="of inner whorl; apically" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="outer" id="o23290" name="whorl" name_original="whorl" src="d0_s14" type="structure" />
      <relation from="o23289" id="r2596" name="part_of" negation="false" src="d0_s14" to="o23290" />
    </statement>
    <statement id="d0_s15">
      <text>stamens 9;</text>
      <biological_entity id="o23291" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments basally adnate, pilose basally;</text>
      <biological_entity id="o23292" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s16" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s16" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers yellow, oblong.</text>
      <biological_entity id="o23293" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes, light reddish-brown, not winged, 3-gonous, pubescent apically.</text>
      <biological_entity id="o23294" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light reddish-brown" value_original="light reddish-brown" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s18" value="3-gonous" value_original="3-gonous" />
        <character is_modifier="false" modifier="apically" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds: embryo curved.</text>
      <biological_entity id="o23295" name="seed" name_original="seeds" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>x = 20.</text>
      <biological_entity id="o23296" name="embryo" name_original="embryo" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="x" id="o23297" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Eureka or July gold</other_name>
  <discussion>Species 1.</discussion>
  <discussion>Dedeckera is rare but not endangered or threatened, primarily because most populations are in remote areas along sides of steep canyon walls. Most populations occur in areas managed by the Bureau of Land Management, the United States Forest Service, or the National Park Service. The plants rarely produce mature achenes (D. Wiens et al. 1989). The genus is allied to Eriogonum subg. Eucycla.</discussion>
  <references>
    <reference>Nickrent, D. L. and D. Wiens. 1989. Genetic diversity in the rare California shrub Dedeckera eurekensis (Polygonaceae). Syst. Bot. 14: 245–253.</reference>
    <reference>Wiens, D. et al. 1989. Developmental failure and loss of reproductive capacity in the rare palaeoendemic shrub Dedeckera eurekensis. Nature 338: 65–67.</reference>
    <reference>Wiens, D., L. Allphin, D. H. Mansfield, and G. Thackray. 2004. Developmental failure and loss of reproductive capacity as a factor in extinction: A nine-year study of Dedeckera eurekensis (Polygonaceae). Aliso 21: 55–63.</reference>
    <reference>Wiens, D., C. I. Davern, and C. L. Calvin. 1988. Exceptionally low seed set in Dedeckera eurekensis: Is there a genetic component of extinction? In: C. A. Hall and V. Doyle-Jones, eds. 1988. Plant Biology of Eastern California. Los Angeles. Pp. 19–29.</reference>
    <reference>Wiens, D., M. DeDecker, and C. D. Wiens. 1986. Observations on the pollination of Dedeckera eurekensis (Polygonaceae). Madroño 33: 302–305.</reference>
  </references>
  
</bio:treatment>