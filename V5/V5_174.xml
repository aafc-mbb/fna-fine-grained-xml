<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">85</other_info_on_meta>
    <other_info_on_meta type="mention_page">75</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cerastium</taxon_name>
    <taxon_name authority="Greene" date="1901" rank="species">fastigiatum</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 303. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus cerastium;species fastigiatum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060045</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, with slender taproot.</text>
      <biological_entity id="o5025" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="slender" id="o5026" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o5025" id="r565" name="with" negation="false" src="d0_s0" to="o5026" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched from base, branches ascending (fastigiate), 10–50 cm, pubescent with stiff, gland-tipped, patent or slightly reflexed hairs with broadened base, pubescence shorter than diam. of stem, soft wooly hairs absent;</text>
      <biological_entity id="o5027" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from base" constraintid="o5028" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o5028" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o5029" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character constraint="with stiff" is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="patent" value_original="patent" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s1" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o5030" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character constraint="than diam of stem" constraintid="o5032" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s1" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5031" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="broadened" value_original="broadened" />
      </biological_entity>
      <biological_entity id="o5032" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s1" value="diam" value_original="diam" />
      </biological_entity>
      <biological_entity id="o5033" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s1" value="soft" value_original="soft" />
        <character is_modifier="true" name="pubescence" src="d0_s1" value="wooly" value_original="wooly" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o5030" id="r566" name="with" negation="false" src="d0_s1" to="o5031" />
    </statement>
    <statement id="d0_s2">
      <text>small axillary tufts of leaves absent.</text>
      <biological_entity constraint="axillary" id="o5034" name="tuft" name_original="tufts" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o5035" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o5034" id="r567" name="consist_of" negation="false" src="d0_s2" to="o5035" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile;</text>
      <biological_entity id="o5036" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal and midstem blades linear-lanceolate, 20–70 × 1.5–6 mm, apex acute to acuminate;</text>
      <biological_entity constraint="midstem" id="o5037" name="blade" name_original="blades" src="d0_s4" type="structure" constraint_original="distal and midstem">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5038" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal leaves shortly connate basally, blade narrowly oblanceolate, tending to spatulate, pubescence short, stiff, patent, glandular.</text>
      <biological_entity constraint="proximal" id="o5039" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly; basally" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o5040" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="patent" value_original="patent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences very lax, making up at least 1/2 height of plant, 3–45-flowered cymes;</text>
      <biological_entity id="o5041" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="very" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o5042" name="plant" name_original="plant" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="at-least" name="height" src="d0_s6" value="at-least" value_original="at-least" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o5043" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="3-45-flowered" value_original="3-45-flowered" />
      </biological_entity>
      <relation from="o5041" id="r568" name="making up" negation="false" src="d0_s6" to="o5042" />
    </statement>
    <statement id="d0_s7">
      <text>bracts, linear-lanceolate, 2–22 mm, herbaceous, glandular-pubescent.</text>
      <biological_entity id="o5044" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="22" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect to spreading, bent distally, slender, 5–20 mm, 1–5 times as long as sepals, usually longer than capsules, glandular-pubescent.</text>
      <biological_entity id="o5045" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="spreading" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s8" value="bent" value_original="bent" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
        <character constraint="sepal" constraintid="o5046" is_modifier="false" name="length" src="d0_s8" value="1-5 times as long as sepals" />
        <character constraint="than capsules" constraintid="o5047" is_modifier="false" name="length_or_size" src="d0_s8" value="usually longer" value_original="usually longer" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o5046" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity id="o5047" name="capsule" name_original="capsules" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals narrowly lanceolate, 4–5 mm, margins narrow (narrower than herbaceous center), apex sharply acute to acuminate, glandular-hispid, hairs shorter than sepal tips;</text>
      <biological_entity id="o5048" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5049" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5050" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o5051" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="sharply acute" name="shape" src="d0_s9" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-hispid" value_original="glandular-hispid" />
      </biological_entity>
      <biological_entity id="o5052" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character constraint="than sepal tips" constraintid="o5053" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="sepal" id="o5053" name="tip" name_original="tips" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals oblanceolate, 4–5 mm, ± equaling sepals, apex 2-fid;</text>
      <biological_entity id="o5054" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5055" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5056" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o5057" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 10;</text>
      <biological_entity id="o5058" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5059" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 5.</text>
      <biological_entity id="o5060" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o5061" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cylindric, curved, (5–) 7–10 (–11) mm, ca. 2 times as long as sepals;</text>
      <biological_entity id="o5062" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="11" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character constraint="sepal" constraintid="o5063" is_modifier="false" name="length" src="d0_s13" value="2 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o5063" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>teeth 10, erect to slightly spreading, margins convolute.</text>
      <biological_entity id="o5064" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s14" to="slightly spreading" />
      </biological_entity>
      <biological_entity id="o5065" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s14" value="convolute" value_original="convolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds golden brown, 0.5–0.8 mm diam., coarsely tuberculate.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 36.</text>
      <biological_entity id="o5066" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="golden brown" value_original="golden brown" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s15" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s15" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5067" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy canyons and washes, open rocky and sandy places and dry pine woods in arid mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy canyons" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="open rocky" />
        <character name="habitat" value="sandy places" />
        <character name="habitat" value="dry pine woods" constraint="in arid mountains" />
        <character name="habitat" value="arid mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Fastigiate mouse-ear chickweed</other_name>
  <discussion>Until recently Cerastium fastigiatum was included in C. nutans but it is readily separable by its bushy, ascending habit, shorter pubescence, long, narrow leaves, and smaller capsule. It can be very similar to forms of C. brachypodum but differs from that species in its longer pedicels, narrowly acute leaves, glandular pubescence on the stems, and much more branched (fastigiate) habit. From C. nutans var. obtectum it differs in its very narrow sepals, narrowly lanceolate leaves, and smaller capsule.</discussion>
  
</bio:treatment>