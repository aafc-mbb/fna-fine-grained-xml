<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">260</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Reveal" date="1968" rank="species">smithii</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>27: 202, fig. 6. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species smithii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060494</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">corymbosum</taxon_name>
    <taxon_name authority="(Reveal) S. L. Welsh" date="unknown" rank="variety">smithii</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species corymbosum;variety smithii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, erect to spreading, not scapose, (3–) 4–10 × 5–20 dm, glabrous, bright green.</text>
      <biological_entity id="o9169" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="3" from_unit="dm" name="atypical_length" src="d0_s0" to="4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="length" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, without persistent leaf-bases, up to 1/3 height of plant;</text>
      <biological_entity id="o9170" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o9171" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/3 height of plant" />
      </biological_entity>
      <relation from="o9170" id="r1016" name="without" negation="false" src="d0_s1" to="o9171" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems absent;</text>
      <biological_entity constraint="caudex" id="o9172" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems erect to spreading, slender, solid, not fistulose, 0.2–2 dm, glabrous.</text>
      <biological_entity id="o9173" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="spreading" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline on proximal 2/3 of stem, 1 per node;</text>
      <biological_entity id="o9174" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="on proximal 2/3" constraintid="o9175" is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9175" name="2/3" name_original="2/3" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o9177" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9176" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity id="o9177" name="node" name_original="node" src="d0_s4" type="structure" />
      <relation from="o9175" id="r1017" name="part_of" negation="false" src="d0_s4" to="o9176" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.3–0.5 cm, floccose or glabrous;</text>
      <biological_entity id="o9178" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s5" to="0.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade narrowly elliptic, 2.5–4.5 × (0.3–) 0.6–1 cm, glabrous and green on both surfaces, rarely thinly floccose adaxially, margins occasionally slightly revolute.</text>
      <biological_entity id="o9179" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_width" src="d0_s6" to="0.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="on surfaces" constraintid="o9180" is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="rarely thinly; adaxially" name="pubescence" notes="" src="d0_s6" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o9180" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o9181" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="occasionally slightly" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose, open to compact, 2–25 × 3–35 cm;</text>
      <biological_entity id="o9182" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="open" name="architecture" src="d0_s7" to="compact" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="25" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s7" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches often with involucres racemosely arranged at tips of inflorescence, glabrous;</text>
      <biological_entity id="o9183" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9184" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character constraint="at tips" constraintid="o9185" is_modifier="false" modifier="racemosely" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o9185" name="tip" name_original="tips" src="d0_s8" type="structure" />
      <biological_entity id="o9186" name="inflorescence" name_original="inflorescence" src="d0_s8" type="structure" />
      <relation from="o9183" id="r1018" name="with" negation="false" src="d0_s8" to="o9184" />
      <relation from="o9185" id="r1019" name="part_of" negation="false" src="d0_s8" to="o9186" />
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, linear to triangular, 1.5–4.5 mm.</text>
      <biological_entity id="o9187" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="triangular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o9188" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node, turbinate, (2.5–) 3–3.5 × 2–2.5 mm, glabrous;</text>
      <biological_entity id="o9189" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o9190" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s11" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9190" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.3–0.5 mm.</text>
      <biological_entity id="o9191" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 3–4 mm;</text>
      <biological_entity id="o9192" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth yellow, glabrous;</text>
      <biological_entity id="o9193" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, slightly dimorphic, those of outer whorl obovate, 1.5–2 mm wide, those of inner whorl oblanceolate, 1–1.5 mm wide;</text>
      <biological_entity id="o9194" name="tepal" name_original="tepals" src="d0_s15" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o9195" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <biological_entity constraint="inner" id="o9196" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <relation from="o9194" id="r1020" name="part_of" negation="false" src="d0_s15" to="o9195" />
      <relation from="o9194" id="r1021" name="part_of" negation="false" src="d0_s15" to="o9196" />
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 2–5 (–7) mm;</text>
      <biological_entity id="o9197" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o9198" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes brown, 3–3.5 mm, glabrous except for minutely papillate beak.</text>
      <biological_entity id="o9200" name="beak" name_original="beak" src="d0_s18" type="structure">
        <character is_modifier="true" modifier="minutely" name="relief" src="d0_s18" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>2n = 40.</text>
      <biological_entity id="o9199" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9201" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep, moving, red blow sand, mixed grassland and scrub oak communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="red blow sand" modifier="deep moving" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="scrub" />
        <character name="habitat" value="oak communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Smith’s wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum smithii is restricted to the San Rafael Desert of Emery and Wayne counties. Plants from Little Gilson Butte (J. L. Anderson 86-228, BRY) have leaf blades thinly floccose on the abaxial surface but are otherwise similar to the shrubs found at Flat Top to the east. Smith’s wild buckwheat occurs in selenium-rich sands, which may limit its ability to be cultivated. The bright green, brilliantly yellow-flowered plants would make an elegant addition to the garden.</discussion>
  
</bio:treatment>