<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">86</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cerastium</taxon_name>
    <taxon_name authority="Baumgarten" date="1816" rank="species">fontanum</taxon_name>
    <taxon_name authority="(Hartman) Greuter &amp; Burdet" date="1982" rank="subspecies">vulgare</taxon_name>
    <place_of_publication>
      <publication_title>Willdenowia</publication_title>
      <place_in_publication>12: 37. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus cerastium;species fontanum;subspecies vulgare;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242000276</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="Hartman" date="unknown" rank="species">vulgare</taxon_name>
    <place_of_publication>
      <publication_title>Handb. Skand. Fl.,</publication_title>
      <place_in_publication>182. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cerastium;species vulgare;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="Gilibert" date="unknown" rank="species">caespitosum</taxon_name>
    <taxon_hierarchy>genus Cerastium;species caespitosum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="species">fontanum</taxon_name>
    <taxon_name authority="(Link) Jalas" date="unknown" rank="subspecies">triviale</taxon_name>
    <taxon_hierarchy>genus Cerastium;species fontanum;subspecies triviale;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">triviale</taxon_name>
    <taxon_hierarchy>genus Cerastium;species triviale;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vulgatum</taxon_name>
    <taxon_name authority="Fries" date="unknown" rank="variety">hirsutum</taxon_name>
    <taxon_hierarchy>genus Cerastium;species vulgatum;variety hirsutum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial (very rarely annual).</text>
      <biological_entity id="o6490" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences usually eglandular, occasionally viscid and glandular.</text>
      <biological_entity id="o6491" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="occasionally" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petals equaling sepals.</text>
      <biological_entity id="o6492" name="petal" name_original="petals" src="d0_s2" type="structure" />
      <biological_entity id="o6493" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character is_modifier="true" name="variability" src="d0_s2" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Capsules 9–13 mm.</text>
      <biological_entity id="o6494" name="capsule" name_original="capsules" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s3" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Seeds 0.4–0.9 mm;</text>
      <biological_entity id="o6495" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s4" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>tubercles small.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = ca. 122–152, usually 144.</text>
      <biological_entity id="o6496" name="tubercle" name_original="tubercles" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6497" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="122" name="quantity" src="d0_s6" to="152" unit=",usually" />
        <character name="quantity" src="d0_s6" unit=",usually" value="144" value_original="144" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering throughout growing season.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>A common weed in grassy places: lawns, roadsides, pastures, open woodlands, wastelands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="a common weed" constraint="in grassy places" />
        <character name="habitat" value="grassy places" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="wastelands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; introduced worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>16b</number>
  <discussion>Small annual forms of subsp. vulgare can be difficult to separate from Cerastium pumilum. The latter has smaller capsules, narrower and sharply acute sepals, and short, glandular hairs on the sepals, bracts, and inflorescence.</discussion>
  
</bio:treatment>