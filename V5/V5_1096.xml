<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">537</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">polygonella</taxon_name>
    <taxon_name authority="Meisner in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">ciliata</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 81. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonella;species ciliata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060711</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, gynodioecious, 2.5–11 dm.</text>
      <biological_entity id="o7217" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynodioecious" value_original="gynodioecious" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s0" to="11" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually simple, sometimes branched distally, glabrous or minutely pubescent.</text>
      <biological_entity id="o7218" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sometimes; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves fugaceous;</text>
      <biological_entity id="o7219" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="fugaceous" value_original="fugaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ocrea margins ciliate;</text>
      <biological_entity constraint="ocrea" id="o7220" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, larger ones falcate, (3–) 11–33 (–51) × 0.3–1.3 (–3) mm, base barely tapered, margins not hyaline, apex acuminate, glabrous.</text>
      <biological_entity id="o7221" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="size" src="d0_s4" value="larger" value_original="larger" />
        <character is_modifier="false" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s4" to="11" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="33" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="51" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s4" to="33" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s4" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7222" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="barely" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o7223" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o7224" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (10–) 20–35 (–45) mm;</text>
      <biological_entity id="o7225" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s5" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ocreola encircling rachis, only the base adnate to rachis, apex acuminate.</text>
      <biological_entity id="o7226" name="ocreolum" name_original="ocreola" src="d0_s6" type="structure" />
      <biological_entity id="o7227" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity id="o7228" name="base" name_original="base" src="d0_s6" type="structure">
        <character constraint="to rachis" constraintid="o7229" is_modifier="false" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o7229" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity id="o7230" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o7226" id="r776" name="encircling" negation="false" src="d0_s6" to="o7227" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels spreading to slightly reflexed in anthesis, sharply reflexed in fruit, 0.3–1.1 mm, as long or much longer than subtending ocreola.</text>
      <biological_entity id="o7231" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in anthesis" from="spreading" name="orientation" src="d0_s7" to="slightly reflexed" />
        <character constraint="in fruit" constraintid="o7232" is_modifier="false" modifier="sharply" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7232" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity constraint="subtending" id="o7234" name="ocreolum" name_original="ocreola" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character is_modifier="true" modifier="much" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o7233" name="ocreolum" name_original="ocreola" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character is_modifier="true" modifier="much" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o7231" id="r777" name="as" negation="false" src="d0_s7" to="o7234" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual or pistillate;</text>
      <biological_entity id="o7235" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer tepals appressed in anthesis and fruit, white, distal portion of midrib often inconspicuously greenish, narrowly oblong, 0.9–1.5 mm in anthesis, margins entire;</text>
      <biological_entity constraint="outer" id="o7236" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o7237" is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o7237" name="fruit" name_original="fruit" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7238" name="portion" name_original="portion" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often inconspicuously" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" constraint="in anthesis" from="0.9" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7239" name="midrib" name_original="midrib" src="d0_s9" type="structure" />
      <biological_entity id="o7240" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o7238" id="r778" name="part_of" negation="false" src="d0_s9" to="o7239" />
    </statement>
    <statement id="d0_s10">
      <text>inner tepals appressed in anthesis and fruit, white, narrowly oblong, 0.8–1.5 mm in anthesis, margins entire;</text>
      <biological_entity constraint="inner" id="o7241" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o7242" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" constraint="in anthesis" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7242" name="fruit" name_original="fruit" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity id="o7243" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments dimorphic;</text>
      <biological_entity id="o7244" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers deep red;</text>
      <biological_entity id="o7245" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="depth" src="d0_s12" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles and stigmas ca. 0.1 mm in anthesis.</text>
      <biological_entity id="o7246" name="style" name_original="styles" src="d0_s13" type="structure">
        <character constraint="in anthesis" name="some_measurement" src="d0_s13" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
      <biological_entity id="o7247" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character constraint="in anthesis" name="some_measurement" src="d0_s13" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes exserted, reddish-brown to yellowbrown, 3-gonous, 1.7–3.4 × 0.6–0.9 mm, shiny, smooth.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 22.</text>
      <biological_entity id="o7248" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s14" to="yellowbrown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s14" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s14" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7249" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy pinelands and pine barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy pinelands" />
        <character name="habitat" value="pine barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Fringed jointweed</other_name>
  
</bio:treatment>