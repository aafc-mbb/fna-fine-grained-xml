<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">brevicaule</taxon_name>
    <taxon_name authority="(S. Stokes) Dorn" date="1988" rank="variety">canum</taxon_name>
    <place_of_publication>
      <publication_title>Vasc. Pl. Wyoming,</publication_title>
      <place_in_publication>299. 1988</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species brevicaule;variety canum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060205</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="species">multiceps</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="subspecies">canum</taxon_name>
    <place_of_publication>
      <publication_title>Eriogonum,</publication_title>
      <place_in_publication>94. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species multiceps;subspecies canum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">lagopus</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species lagopus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">pauciflorum</taxon_name>
    <taxon_name authority="(S. Stokes) Reveal" date="unknown" rank="variety">canum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species pauciflorum;variety canum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, 1.5–3 × 1.5–3.5 dm.</text>
      <biological_entity id="o1042" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="length" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="width" src="d0_s0" to="3.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, 0.5–1 dm, tomentose.</text>
      <biological_entity id="o1043" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="1" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade narrowly oblanceolate to oblanceolate or spatulate, (1–) 1.5–3.5 (–4) × 0.2–0.7 (–0.9) cm, margins plane.</text>
      <biological_entity id="o1044" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1045" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s2" to="oblanceolate or spatulate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s2" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="0.9" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s2" to="0.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1046" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences subumbellate to cymose, usually divided 3 or more times, 5–10 cm;</text>
      <biological_entity id="o1047" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character char_type="range_value" from="subumbellate" name="architecture" src="d0_s3" to="cymose" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character name="quantity" src="d0_s3" unit="or moretimes" value="3" value_original="3" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches tomentose.</text>
      <biological_entity id="o1048" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 1 per node, turbinate, (2–) 3–3.5 (–5) × 2–2.5 (–3) mm, tomentose.</text>
      <biological_entity id="o1049" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character constraint="per node" constraintid="o1050" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s5" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o1050" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers (1–) 1.5–2.5 (–3) mm;</text>
      <biological_entity id="o1051" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth yellow, sparsely pubescent.</text>
      <biological_entity id="o1052" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clayey flats and slopes, mixed grassland and sagebrush communities, juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27b.</number>
  <other_name type="common_name">Parasol wild buckwheat</other_name>
  <discussion>Variety canum is rare throughout its widely scattered range in Carbon, Park, and Yellowstone counties, Montana, and in Big Horn and Sheridan counties, Wyoming.</discussion>
  
</bio:treatment>