<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">279</other_info_on_meta>
    <other_info_on_meta type="mention_page">225</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="A. Nelson &amp; P. B. Kennedy" date="unknown" rank="species">rosense</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>19: 36. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species rosense;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060481</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, matted, scapose, 0.2–1 (–1.3) × 0.5–5 dm, glandular-hairy, greenish.</text>
      <biological_entity id="o30136" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="1.3" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="length" src="d0_s0" to="1" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems matted, with persistent leaf-bases, up to 1/5 height of plant;</text>
      <biological_entity id="o30137" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="matted" value_original="matted" />
      </biological_entity>
      <biological_entity id="o30138" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/5 height of plant" />
      </biological_entity>
      <relation from="o30137" id="r3391" name="with" negation="false" src="d0_s1" to="o30138" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems matted;</text>
      <biological_entity constraint="caudex" id="o30139" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems scapelike, weakly erect to erect, slender, solid, not fistulose, 0.1–0.9 (–1.1) dm, densely glandular-hairy.</text>
      <biological_entity id="o30140" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="weakly erect" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1.1" to_unit="dm" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s3" to="0.9" to_unit="dm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal, fasciculate in terminal tufts;</text>
      <biological_entity id="o30141" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="in terminal tufts" constraintid="o30142" is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o30142" name="tuft" name_original="tufts" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.4–2 cm, tomentose, sometimes also glandular;</text>
      <biological_entity id="o30143" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblanceolate or broadly elliptic to oval, 0.4–2.5 × (0.15–) 0.25–1.6 cm, densely white-tomentose and glandular on both surfaces or densely white-tomentose abaxially and greenish-tomentose adaxially, margins plane.</text>
      <biological_entity id="o30144" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s6" to="oval" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.15" from_unit="cm" name="atypical_width" src="d0_s6" to="0.25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.25" from_unit="cm" name="width" src="d0_s6" to="1.6" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character constraint="on " constraintid="o30148" is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o30145" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o30146" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o30147" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="densely" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="true" name="pubescence" src="d0_s6" value="greenish-tomentose" value_original="greenish-tomentose" />
      </biological_entity>
      <biological_entity id="o30148" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="greenish-tomentose" value_original="greenish-tomentose" />
      </biological_entity>
      <biological_entity id="o30149" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o30150" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="greenish-tomentose" value_original="greenish-tomentose" />
      </biological_entity>
      <biological_entity id="o30151" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o30152" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="greenish-tomentose" value_original="greenish-tomentose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
      <relation from="o30148" id="r3392" name="on" negation="false" src="d0_s6" to="o30149" />
      <relation from="o30148" id="r3393" modifier="on both surfaces or densely white-tomentose and greenish-tomentose , margins" name="on" negation="false" src="d0_s6" to="o30150" />
      <relation from="o30150" id="r3394" name="on" negation="false" src="d0_s6" to="o30151" />
      <relation from="o30150" id="r3395" modifier="on both surfaces or densely white-tomentose and greenish-tomentose , margins" name="on" negation="false" src="d0_s6" to="o30152" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences capitate, 0.6–1.5 cm;</text>
      <biological_entity id="o30153" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches absent;</text>
      <biological_entity id="o30154" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, triangular, 1–3.5 mm.</text>
      <biological_entity id="o30155" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o30156" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 3–6 per cluster, turbinate to campanulate, (2.5–) 3–5 (–6) × 2.5–4 (–6) mm, rigid, glandular and sparsely hairy;</text>
      <biological_entity id="o30157" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per cluster" from="3" name="quantity" src="d0_s11" to="6" />
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s11" to="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s11" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s11" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>teeth 5–8, erect, 0.7–1.4 mm.</text>
      <biological_entity id="o30158" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="8" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 2–4 mm;</text>
      <biological_entity id="o30159" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth bright-yellow to reddish yellow, occasionally cream, glabrous or glandular;</text>
      <biological_entity id="o30160" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="bright-yellow" name="coloration" src="d0_s14" to="reddish yellow" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s14" value="cream" value_original="cream" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximally, monomorphic, obovate or oblong;</text>
      <biological_entity id="o30161" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 3–5.5 mm;</text>
      <biological_entity id="o30162" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments glabrous or sparsely pilose proximally.</text>
      <biological_entity id="o30163" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 1.5–3.5 mm, glabrous.</text>
      <biological_entity id="o30164" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s18" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.; Calif. and Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Calif. and Nev" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>53.</number>
  <other_name type="past_name">rosensis</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades oblanceolate to elliptic, (0.15-)0.25-0.6(-1) cm wide; involucres (2.5-)3-4(-4.5) × 2.5-3.5 mm; flowers 2-3 mm; tepals obovate; achenes 1.5-2.5 mm; Sierra Nevada of California and adjacent desert ranges of wc Nevada</description>
      <determination>53a Eriogonum rosense var. rosense</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades broadly elliptic to oval, 0.5-1.6 cm wide; involucres (2.5-)3-5(-6) × 3-5(-6) mm; flowers (2.5-)3-4 mm; tepals oblong; achenes (2.5-)3-3.5 mm; desert ranges, Nevada</description>
      <determination>53b Eriogonum rosense var. beatleyae</determination>
    </key_statement>
  </key>
</bio:treatment>