<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">124</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">129</other_info_on_meta>
    <other_info_on_meta type="mention_page">130</other_info_on_meta>
    <other_info_on_meta type="mention_page">133</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Loefling in C. Linnaeus" date="1754" rank="genus">minuartia</taxon_name>
    <taxon_name authority="T. W. Nelson &amp; J. P. Nelson" date="1981" rank="species">decumbens</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>33: 162, fig. 1. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus minuartia;species decumbens;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060631</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, mat-forming.</text>
      <biological_entity id="o6700" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots stout, woody.</text>
      <biological_entity id="o6701" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, green, 4–15 cm, trailing stems to 30 cm, glabrous, internodes of flowering-stems ca. as long as leaves.</text>
      <biological_entity id="o6702" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6703" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="trailing" value_original="trailing" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6704" name="internode" name_original="internodes" src="d0_s2" type="structure" />
      <biological_entity id="o6705" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure" />
      <biological_entity id="o6706" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o6704" id="r731" name="part_of" negation="false" src="d0_s2" to="o6705" />
      <relation from="o6704" id="r732" name="as long as" negation="false" src="d0_s2" to="o6706" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves overlapping proximally, all evenly spaced, connate proximally, with tight, scarious sheath 0.5–0.7 mm;</text>
      <biological_entity id="o6707" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s3" value="spaced" value_original="spaced" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o6708" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s3" value="tight" value_original="tight" />
        <character is_modifier="true" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="0.7" to_unit="mm" />
      </biological_entity>
      <relation from="o6707" id="r733" name="with" negation="false" src="d0_s3" to="o6708" />
    </statement>
    <statement id="d0_s4">
      <text>blade arcuate, green, flat, 3-veined, needlelike to subulate, 3–6 (–9) × 0.7–2 mm, ± rigid, margins scarious proximally, apex green, blunt to ± acute, dull, glabrous;</text>
      <biological_entity id="o6709" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="course_or_shape" src="d0_s4" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="needlelike" name="shape" src="d0_s4" to="subulate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o6710" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o6711" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="blunt" name="shape" src="d0_s4" to="more or less acute" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>axillary leaves present among vegetative leaves.</text>
      <biological_entity constraint="axillary" id="o6712" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o6713" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s5" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o6712" id="r734" name="present among" negation="false" src="d0_s5" to="o6713" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 5–20-flowered, open cymes;</text>
      <biological_entity id="o6714" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-20-flowered" value_original="5-20-flowered" />
      </biological_entity>
      <biological_entity id="o6715" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts narrowly lanceolate, herbaceous, thinly scarious-margined.</text>
      <biological_entity id="o6716" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="thinly" name="architecture" src="d0_s7" value="scarious-margined" value_original="scarious-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.5–2.5 cm, sparsely stipitate-glandular.</text>
      <biological_entity id="o6717" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium dish-shaped;</text>
      <biological_entity id="o6718" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6719" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="dish--shaped" value_original="dish--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals (1-veined or) 3-veined, narrowly lanceolate (herbaceous portion narrowly lanceolate), 5–6 mm, not enlarging in fruit, apex often purple, acute to acuminate, not hooded, sparsely stipitate-glandular;</text>
      <biological_entity id="o6720" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6721" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character constraint="in fruit" constraintid="o6722" is_modifier="false" modifier="not" name="size" src="d0_s10" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o6722" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity id="o6723" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="hooded" value_original="hooded" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals broadly linear to oblongelliptic, 0.7–0.9 times as long as sepals, apex rounded, entire to slightly emarginate.</text>
      <biological_entity id="o6724" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6725" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="broadly linear" name="shape" src="d0_s11" to="oblongelliptic" />
        <character constraint="sepal" constraintid="o6726" is_modifier="false" name="length" src="d0_s11" value="0.7-0.9 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o6726" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o6727" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s11" to="slightly emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ellipsoid, 4–4.8 mm, shorter than sepals.</text>
      <biological_entity id="o6728" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="4.8" to_unit="mm" />
        <character constraint="than sepals" constraintid="o6729" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o6729" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds purplish brown, elliptic-oblong, ± compressed, 1.8–2.2 mm, tuberculate;</text>
      <biological_entity id="o6730" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s13" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tubercles low, rounded.</text>
      <biological_entity id="o6731" name="tubercle" name_original="tubercles" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Jeffrey pine woodlands, serpentine soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="jeffrey pine woodlands" />
        <character name="habitat" value="serpentine soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Lassicus stitchwort</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Minuartia decumbens, like M. rosei and M. stolonifera, is restricted to serpentine soils of northwestern California, specifically to Mule Ridge in Trinity County. The three species are most closely related to the polymorphic M. nuttallii.</discussion>
  
</bio:treatment>