<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">303</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="S. Watson" date="1888" rank="species">pendulum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>23: 265. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species pendulum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060458</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pendulum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="variety">confertum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species pendulum;variety confertum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, erect or slightly spreading, not scapose, 2–5 × 2–8 dm, densely tomentose, tannish.</text>
      <biological_entity id="o23598" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="width" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="tannish" value_original="tannish" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly erect, without persistent leaf-bases, up to 1/2 height or more of plant;</text>
      <biological_entity id="o23599" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o23600" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="of plant" constraintid="o23601" from="0" name="height" src="d0_s1" to="1/2" />
      </biological_entity>
      <biological_entity id="o23601" name="plant" name_original="plant" src="d0_s1" type="structure" />
      <relation from="o23599" id="r2620" name="without" negation="false" src="d0_s1" to="o23600" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems absent;</text>
      <biological_entity constraint="caudex" id="o23602" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems erect, stout, solid, not fistulose, 2–4 dm, tomentose.</text>
      <biological_entity id="o23603" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s3" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves 1 per node and sheathing up stems or fasciculate at tips of basal branches;</text>
      <biological_entity id="o23604" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o23605" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="up " constraintid="o23607" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o23605" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity id="o23606" name="stem" name_original="stems" src="d0_s4" type="structure" />
      <biological_entity id="o23607" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s4" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o23608" name="branch" name_original="branches" src="d0_s4" type="structure" />
      <relation from="o23607" id="r2621" name="part_of" negation="false" src="d0_s4" to="o23608" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.1–0.5 (–1) cm, tomentose;</text>
      <biological_entity id="o23609" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s5" to="0.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblanceolate to narrowly oblong, (1.5–) 2–4 (–5) × 1–2.5 (–3) cm, densely white-tomentose abaxially, floccose adaxially, margins plane.</text>
      <biological_entity id="o23610" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="narrowly oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s6" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o23611" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose, 15–30 (–40) × 15–40 cm;</text>
      <biological_entity id="o23612" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="40" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s7" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, tomentose;</text>
      <biological_entity id="o23613" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, semileaflike, narrowly oblanceolate, 3–7 × 1–3 mm.</text>
      <biological_entity id="o23614" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="semileaflike" value_original="semileaflike" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent or erect, slender, (1–) 3–10 cm, tomentose.</text>
      <biological_entity id="o23615" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="10" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node, turbinate-campanulate, 3.5–5 × 2.5–4 mm, tomentose;</text>
      <biological_entity id="o23616" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o23617" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o23617" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5–8, erect, 0.4–0.8 mm.</text>
      <biological_entity id="o23618" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="8" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 3–6 (–7) mm;</text>
      <biological_entity id="o23619" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white, densely villous abaxially;</text>
      <biological_entity id="o23620" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s14" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/3, monomorphic, narrowly oblong;</text>
      <biological_entity id="o23621" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 3–7 mm;</text>
      <biological_entity id="o23622" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o23623" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes brown, 3–5 mm, villous.</text>
      <biological_entity id="o23624" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, sagebrush communities, oak and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>86.</number>
  <other_name type="common_name">Waldo wild buckwheat</other_name>
  <discussion>Eriogonum pendulum is a rare to locally uncommon shrub in the O’Brien-Waldo area of Josephine County, Oregon, and more common in the Siskiyou Mountains of Del Norte County, California, as far south as Gasquet.</discussion>
  
</bio:treatment>