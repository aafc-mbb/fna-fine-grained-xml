<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">470</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Bentham" date="1836" rank="genus">MUCRONEA</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>17: 405, 419, plate 20. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus mucronea;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin mucronis, sharp point, alluding to awns of bracts and involucres</other_info_on_name>
    <other_info_on_name type="fna_id">121274</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="R. Brown ex Bentham" date="unknown" rank="genus">Chorizanthe</taxon_name>
    <taxon_name authority="(Bentham) A. Gray" date="unknown" rank="section">Mucronea</taxon_name>
    <taxon_hierarchy>genus Chorizanthe;section Mucronea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
      <biological_entity id="o15209" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot slender.</text>
      <biological_entity id="o15210" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems arising directly from the root, erect to spreading, solid, not fistulose or disarticulating into ringlike segments, sparsely glandular-pubescent.</text>
      <biological_entity id="o15211" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from root" constraintid="o15212" is_modifier="false" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s2" to="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character constraint="into segments" constraintid="o15213" is_modifier="false" name="architecture" src="d0_s2" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o15212" name="root" name_original="root" src="d0_s2" type="structure" />
      <biological_entity id="o15213" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="ringlike" value_original="ringlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually quickly deciduous, basal, rosulate;</text>
      <biological_entity id="o15214" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually quickly" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15215" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o15216" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade spatulate to obovate, margins entire.</text>
      <biological_entity id="o15217" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="obovate" />
      </biological_entity>
      <biological_entity id="o15218" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, cymose, uniparous due to suppression of secondaries;</text>
      <biological_entity id="o15219" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character constraint="of secondaries" constraintid="o15220" is_modifier="false" name="architecture" src="d0_s6" value="uniparous" value_original="uniparous" />
      </biological_entity>
      <biological_entity id="o15220" name="secondary" name_original="secondaries" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>branches dichotomous, not brittle or disarticulating into segments, round, sparsely glandular-pubescent;</text>
      <biological_entity id="o15221" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s7" value="brittle" value_original="brittle" />
        <character constraint="into segments" constraintid="o15222" is_modifier="false" name="architecture" src="d0_s7" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="round" value_original="round" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o15222" name="segment" name_original="segments" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracts 3 (–5), positioned to side of node or perfoliate and completely but unequally surrounding node, connate nearly completely, triangular to ovate or oblong, awned, sparsely glandular-pubescent.</text>
      <biological_entity id="o15223" name="bract" name_original="bracts" src="d0_s8" type="structure" constraint="node" constraint_original="node; node">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" modifier="nearly completely; completely" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s8" to="ovate or oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="awned" value_original="awned" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o15224" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o15225" name="node" name_original="node" src="d0_s8" type="structure" />
      <biological_entity id="o15226" name="node" name_original="node" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="unequally" name="position_relational" src="d0_s8" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity id="o15227" name="node" name_original="node" src="d0_s8" type="structure" />
      <biological_entity id="o15228" name="node" name_original="node" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="unequally" name="position_relational" src="d0_s8" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <relation from="o15223" id="r1673" name="positioned to" negation="false" src="d0_s8" to="o15224" />
      <relation from="o15223" id="r1674" name="part_of" negation="false" src="d0_s8" to="o15225" />
      <relation from="o15223" id="r1675" name="part_of" negation="false" src="d0_s8" to="o15226" />
      <relation from="o15223" id="r1676" name="part_of" negation="false" src="d0_s8" to="o15227" />
      <relation from="o15223" id="r1677" modifier="of node or perfoliate and but unequally surrounding node" name="part_of" negation="false" src="d0_s8" to="o15228" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles absent.</text>
      <biological_entity id="o15229" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres 1–3 per node, tubular, cylindric, (2–) 3–4-angled, slightly ventricose on the angles in some;</text>
      <biological_entity id="o15230" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="per node" constraintid="o15231" from="1" name="quantity" src="d0_s10" to="3" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="(2-)3-4-angled" value_original="(2-)3-4-angled" />
        <character constraint="on angles" constraintid="o15232" is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="ventricose" value_original="ventricose" />
      </biological_entity>
      <biological_entity id="o15231" name="node" name_original="node" src="d0_s10" type="structure" />
      <biological_entity id="o15232" name="angle" name_original="angles" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>teeth (2–) 3–4, awn-tipped.</text>
      <biological_entity id="o15233" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s11" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1 (–2) per involucre;</text>
      <biological_entity id="o15234" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="2" />
        <character constraint="per involucre" constraintid="o15235" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15235" name="involucre" name_original="involucre" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>perianth white to pink, campanulate when open, cylindric when closed, pubescent abaxially;</text>
      <biological_entity id="o15236" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="pink" />
        <character is_modifier="false" modifier="when open" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="when closed" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals 6, connate ca. 1/3 their length, monomorphic, entire or erose to fimbriate apically;</text>
      <biological_entity id="o15237" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character name="length" src="d0_s14" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s14" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens 6–9;</text>
      <biological_entity id="o15238" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s15" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments free, glabrous;</text>
      <biological_entity id="o15239" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers pink to red, oblong.</text>
      <biological_entity id="o15240" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s17" to="red" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes mostly included, brown to black, not winged, globose-lenticular, glabrous.</text>
      <biological_entity id="o15241" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s18" value="included" value_original="included" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s18" to="black" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose-lenticular" value_original="globose-lenticular" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds: embryo straight.</text>
      <biological_entity id="o15242" name="seed" name_original="seeds" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>x = 19.</text>
      <biological_entity id="o15243" name="embryo" name_original="embryo" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="x" id="o15244" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">California spineflower</other_name>
  <discussion>Species 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence bracts positioned to side of node, involucres 1-3 per node, 2.5-5(-7) mm,bracts (2-)3(-4), awns (0.5-)1-2.5 (3) mm; flowers 1-2; tepals entire apically</description>
      <determination>1 Mucronea californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence bracts perfoliate; involucres 1 per node, 3-5(-6) mm, bracts 4, awns (0.3-)0.5-1.2 mm; flowers 1; tepals entire to fimbriate apically</description>
      <determination>2 Mucronea perfoliata</determination>
    </key_statement>
  </key>
</bio:treatment>