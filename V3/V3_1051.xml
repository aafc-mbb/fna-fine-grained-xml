<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="de Candolle" date="1824" rank="section">Hecatonia</taxon_name>
    <taxon_name authority="Rafinesque" date="1818" rank="species">flabellaris</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Monthly Mag. &amp; Crit. Rev.</publication_title>
      <place_in_publication>2: 344. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section hecatonia;species flabellaris;</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501142</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">delphinifolius</taxon_name>
    <taxon_hierarchy>genus Ranunculus;species delphinifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems floating or prostrate, glabrous, rooting at proximal nodes.</text>
      <biological_entity id="o5946" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="floating" value_original="floating" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="at proximal nodes" constraintid="o5947" is_modifier="false" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5947" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal leaves seldom present, cauline leaf-blades semicircular to reniform, 1-6×-lobed, parted, or dissected 1.2-7.3 × 1.9-10.8 cm, base truncate or cordate, segment margins entire or crenate, apex rounded to filiform.</text>
      <biological_entity id="o5948" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o5949" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="seldom" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o5950" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s1" to="reniform 1-6×-lobed parted or dissected" />
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s1" to="reniform 1-6×-lobed parted or dissected" />
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s1" to="reniform 1-6×-lobed parted or dissected" />
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s1" to="reniform 1-6×-lobed parted or dissected" />
      </biological_entity>
      <biological_entity id="o5951" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity constraint="segment" id="o5952" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o5953" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: receptacle sparsely hispid;</text>
      <biological_entity id="o5954" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o5955" name="receptacle" name_original="receptacle" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals 5, spreading or weakly reflexed, 5-7 × 3-6 mm, glabrous;</text>
      <biological_entity id="o5956" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o5957" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s3" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals 5-6 (-14), 7-12 × 5-9 mm;</text>
      <biological_entity id="o5958" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o5959" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="14" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>nectary scale variable, crescent-shaped, funnel-shaped, or flaplike;</text>
      <biological_entity id="o5960" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="nectary" id="o5961" name="scale" name_original="scale" src="d0_s5" type="structure">
        <character is_modifier="false" name="variability" src="d0_s5" value="variable" value_original="variable" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crescent--shaped" value_original="crescent--shaped" />
        <character is_modifier="false" name="shape" src="d0_s5" value="funnel--shaped" value_original="funnel--shaped" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flaplike" value_original="flaplike" />
        <character is_modifier="false" name="shape" src="d0_s5" value="funnel--shaped" value_original="funnel--shaped" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flaplike" value_original="flaplike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>style 0.8-1.2 mm.</text>
      <biological_entity id="o5962" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o5963" name="style" name_original="style" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads of achenes ovoid, 8-10 × 7-8 mm;</text>
      <biological_entity id="o5964" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5965" name="achene" name_original="achenes" src="d0_s7" type="structure" />
      <relation from="o5964" id="r839" name="part_of" negation="false" src="d0_s7" to="o5965" />
    </statement>
    <statement id="d0_s8">
      <text>achenes 1.8-2.2 × 1.6-2.2 mm, glabrous;</text>
      <biological_entity id="o5966" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s8" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s8" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak lanceolate, straight, 1-1.8 mm. 2n = 32.</text>
      <biological_entity id="o5967" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5968" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (May–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow water or drying mud</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mud" modifier="shallow water or drying" />
        <character name="habitat" value="water" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Ont., Que.; Ala., Ark., Calif., Conn., Del., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., Nev., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>63.</number>
  <other_name type="common_name">Renoncule &amp;agrave; évantails</other_name>
  <discussion>The Fox tribes used Ranunculus flabellaris as a cold remedy and a respiratory aid (D. E. Moerman 1986).</discussion>
  
</bio:treatment>