<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey &amp; A. Gray" date="1838" rank="species">occidentalis</taxon_name>
    <taxon_name authority="L. D. Benson" date="1941" rank="variety">hexasepalus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>68: 167. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species occidentalis;variety hexasepalus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501182</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="(L. D. Benson) L. D. Benson" date="unknown" rank="species">hexasepalus</taxon_name>
    <taxon_hierarchy>genus Ranunculus;species hexasepalus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or spreading, 2-6 mm thick, hirsute or strigose, at least distally.</text>
      <biological_entity id="o16448" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s0" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaf-blades 3-parted, ultimate segments elliptic or narrowly elliptic, margins dentate.</text>
      <biological_entity constraint="basal" id="o16449" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="3-parted" value_original="3-parted" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o16450" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s1" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o16451" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals 5-6, 4-5 mm;</text>
      <biological_entity id="o16452" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o16453" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="6" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petals 8-14, 9-13 × 3-7 mm.</text>
      <biological_entity id="o16454" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o16455" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s3" to="14" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s3" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Achenes 2.6-3 × 2.2-3 mm, glabrous;</text>
      <biological_entity id="o16456" name="achene" name_original="achenes" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="length" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>beak lanceolate, curved, at least distally, 1.2-1.6 mm.</text>
      <biological_entity id="o16457" name="beak" name_original="beak" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="1.2" from_unit="mm" modifier="at-least distally; distally" name="some_measurement" src="d0_s5" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas near coast</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" constraint="near coast" />
        <character name="habitat" value="coast" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9f.</number>
  <discussion>Ranunculus occidentalis var. hexasepalus is endemic to the Queen Charlotte Islands.</discussion>
  
</bio:treatment>