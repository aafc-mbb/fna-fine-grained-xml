<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(N. I. Malyutin) M. J. Warnock" date="1995" rank="subsection">grumosa</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">tricorne</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.,</publication_title>
      <place_in_publication>314. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection grumosa;species tricorne;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500554</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 20-60 cm;</text>
      <biological_entity id="o10130" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base often reddish, nearly glabrous.</text>
      <biological_entity id="o10131" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly on proximal 1/3 of stem;</text>
      <biological_entity id="o10132" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o10133" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <biological_entity id="o10134" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o10132" id="r1411" name="on" negation="false" src="d0_s2" to="o10133" />
      <relation from="o10133" id="r1412" name="part_of" negation="false" src="d0_s2" to="o10134" />
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 0-4 at anthesis;</text>
      <biological_entity constraint="basal" id="o10135" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 2-8 at anthesis;</text>
      <biological_entity constraint="cauline" id="o10136" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="2" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 4-12 cm.</text>
      <biological_entity id="o10137" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round, 2-8 × 4-12 cm, nearly glabrous;</text>
      <biological_entity id="o10138" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="round" value_original="round" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s6" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-18, 5 or more extending more than 3/5 distance to petiole, width 2-10 mm (basal), 4-10 mm (cauline), widest at middle or in proximal 1/2.</text>
      <biological_entity constraint="ultimate" id="o10139" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="18" />
        <character name="quantity" src="d0_s7" unit="or moreextending" value="5" value_original="5" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character constraint="to petiole" constraintid="o10140" name="quantity" src="d0_s7" value="/5" value_original="/5" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character constraint="at " constraintid="o10142" is_modifier="false" name="width" src="d0_s7" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o10140" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10141" name="middle" name_original="middle" src="d0_s7" type="structure" />
      <biological_entity constraint="proximal" id="o10142" name="1/2" name_original="1/2" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 5-15 (-30) -flowered, less than 3 times longer than wide;</text>
      <biological_entity id="o10143" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-15(-30)-flowered" value_original="5-15(-30)-flowered" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s8" value="0-3" value_original="0-3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel 1-2.5 cm, puberulent;</text>
      <biological_entity id="o10144" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 1-4 (-6) mm from flowers, green, linear, 3-5 mm, puberulent.</text>
      <biological_entity id="o10145" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o10146" from="1" from_unit="mm" name="location" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o10146" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals deep bluish purple to pink or white, puberulent, lateral sepals spreading, 11-19 × 4-7 mm, spurs straight, within 30° of horizontal, 13-16 mm;</text>
      <biological_entity id="o10147" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10148" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="deep" value_original="deep" />
        <character char_type="range_value" from="bluish purple" name="coloration" src="d0_s11" to="pink or white" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o10149" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s11" to="19" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10150" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character constraint="of horizontal" name="degree" src="d0_s11" value="30°" value_original="30°" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s11" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades ± covering stamens, blue, except sometimes in white-flowered plants, 6-10 mm, clefts 0.5-2 mm;</text>
      <biological_entity id="o10151" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o10152" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" name="coloration" src="d0_s12" value="blue" value_original="blue" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10153" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o10154" name="plant" name_original="plants" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="white-flowered" value_original="white-flowered" />
      </biological_entity>
      <biological_entity id="o10155" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o10152" id="r1413" modifier="more or less" name="covering" negation="false" src="d0_s12" to="o10153" />
      <relation from="o10152" id="r1414" modifier="sometimes" name="in" negation="false" src="d0_s12" to="o10154" />
    </statement>
    <statement id="d0_s13">
      <text>hairs sparse, mostly centered near junction of blade and claw, white.</text>
      <biological_entity id="o10156" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o10157" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s13" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o10158" name="junction" name_original="junction" src="d0_s13" type="structure" />
      <biological_entity id="o10159" name="blade" name_original="blade" src="d0_s13" type="structure" />
      <biological_entity id="o10160" name="claw" name_original="claw" src="d0_s13" type="structure" />
      <relation from="o10157" id="r1415" modifier="mostly" name="centered near" negation="false" src="d0_s13" to="o10158" />
      <relation from="o10157" id="r1416" name="part_of" negation="false" src="d0_s13" to="o10159" />
      <relation from="o10157" id="r1417" name="part_of" negation="false" src="d0_s13" to="o10160" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 14-22 mm, 4-4.5 times longer than wide, nearly glabrous.</text>
      <biological_entity id="o10161" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s14" to="22" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="4-4.5" value_original="4-4.5" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds unwinged;</text>
      <biological_entity id="o10162" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="unwinged" value_original="unwinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>surface of each seed-coat cell with 1-5 small, swollen, elongate, blunt, hairlike structures, barely visible at 20×, otherwise smooth.</text>
      <biological_entity constraint="seed-coat" id="o10164" name="cell" name_original="cell" src="d0_s16" type="structure" />
      <biological_entity id="o10165" name="structure" name_original="structures" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s16" to="5" />
        <character is_modifier="true" name="size" src="d0_s16" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s16" value="swollen" value_original="swollen" />
        <character is_modifier="true" name="shape" src="d0_s16" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="shape" src="d0_s16" value="blunt" value_original="blunt" />
        <character is_modifier="true" name="shape" src="d0_s16" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <relation from="o10163" id="r1418" name="part_of" negation="false" src="d0_s16" to="o10164" />
      <relation from="o10163" id="r1419" name="with" negation="false" src="d0_s16" to="o10165" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 16.</text>
      <biological_entity id="o10163" name="surface" name_original="surface" src="d0_s16" type="structure" constraint="cell" constraint_original="cell; cell">
        <character constraint="at 20×" is_modifier="false" modifier="barely" name="prominence" notes="" src="d0_s16" value="visible" value_original="visible" />
        <character is_modifier="false" modifier="otherwise" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10166" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Slopes in deciduous forests, thicket edges, moist prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous forests" modifier="slopes in" />
        <character name="habitat" value="thicket edges" />
        <character name="habitat" value="moist prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., Md., Miss., Mo., Nebr., N.C., Ohio, Okla., Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>55.</number>
  <other_name type="common_name">Dwarf larkspur</other_name>
  <discussion>Delphinium tricorne is the most commonly encountered larkspur east of the Great Plains.</discussion>
  <discussion>The Cherokee prepared infusions of Delphinium tricorne to ingest for heart problems, although they believed the roots of the plant made cows drunk and killed them (D. E. Moerman 1986).</discussion>
  
</bio:treatment>