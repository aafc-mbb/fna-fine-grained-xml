<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="Linneaus" date="unknown" rank="section">quercus</taxon_name>
    <taxon_name authority="Schlechtendal &amp; Chamisso" date="1830" rank="species">polymorpha</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>5: 78. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section quercus;species polymorpha</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501074</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, subevergreen, to 20 m.</text>
      <biological_entity id="o12202" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="subevergreen" value_original="subevergreen" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark gray to brown, scaly.</text>
      <biological_entity id="o12203" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s1" to="brown" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs reddish-brown, 2-3 mm diam., tomentose, soon glabrate.</text>
      <biological_entity id="o12204" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Buds reddish-brown, ovoid, 3-10 mm, apex acute, pubescent or glabrate.</text>
      <biological_entity id="o12205" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12206" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole (6-) 15-25 mm.</text>
      <biological_entity id="o12207" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12208" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade elliptic or ovate or lanceovate, sometimes obovate, 50-100 (-150) × 30-60 (-80) mm, base rounded or cordate, margins entire or obscurely or prominently serrate-toothed in distal 1/3 blade, revolute, secondary-veins moderately curved, 10-12 (-14) on each side, apex rounded, acuminate or retuse, sometimes with prominent drip-tip;</text>
      <biological_entity id="o12209" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceovate" value_original="lanceovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s5" to="100" to_unit="mm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s5" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12210" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o12211" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s5" value="or" value_original="or" />
        <character constraint="in blade" constraintid="o12212" is_modifier="false" modifier="prominently" name="shape" src="d0_s5" value="serrate-toothed" value_original="serrate-toothed" />
        <character is_modifier="false" name="shape_or_vernation" notes="" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o12212" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o12213" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="14" />
        <character char_type="range_value" constraint="on side" constraintid="o12214" from="10" name="quantity" src="d0_s5" to="12" />
      </biological_entity>
      <biological_entity id="o12214" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o12215" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="retuse" value_original="retuse" />
      </biological_entity>
      <biological_entity id="o12216" name="drip-tip" name_original="drip-tip" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o12215" id="r1697" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o12216" />
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially light green, sometimes rather glaucous, veinlets raised, forming raised reticulum, floccose or tomentose with erect, golden hairs, soon glabrate, adaxially dark or light green, glossy, floccose or tomentose when immature, soon glabrate, secondary and tertiary-veins impressed.</text>
      <biological_entity id="o12217" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="sometimes rather" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o12218" name="veinlet" name_original="veinlets" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="raised" value_original="raised" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="floccose" value_original="floccose" />
        <character constraint="with hairs" constraintid="o12220" is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="soon" name="pubescence" notes="" src="d0_s6" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="dark" value_original="dark" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="light green" value_original="light green" />
        <character is_modifier="false" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="when immature" name="pubescence" src="d0_s6" value="floccose" value_original="floccose" />
        <character is_modifier="false" modifier="when immature" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o12219" name="reticulum" name_original="reticulum" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o12220" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="golden" value_original="golden" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o12221" name="tertiary-vein" name_original="tertiary-veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="impressed" value_original="impressed" />
      </biological_entity>
      <relation from="o12218" id="r1698" name="forming" negation="false" src="d0_s6" to="o12219" />
    </statement>
    <statement id="d0_s7">
      <text>Acorns 1-2 on peduncle 5-30 mm;</text>
      <biological_entity id="o12222" name="acorn" name_original="acorns" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="on peduncle" constraintid="o12223" from="1" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <biological_entity id="o12223" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cup hemispheric or funnel-shaped, 10-13 mm deep × 12-20 mm wide, including ca. 1/2 nut, scales appressed, thickened basally, gray-canescent;</text>
      <biological_entity id="o12224" name="cup" name_original="cup" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="funnel--shaped" value_original="funnel--shaped" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s8" to="13" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="×12-20" value_original="×12-20" />
      </biological_entity>
      <biological_entity id="o12225" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o12226" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="basally" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="gray-canescent" value_original="gray-canescent" />
      </biological_entity>
      <relation from="o12224" id="r1699" name="including" negation="false" src="d0_s8" to="o12225" />
    </statement>
    <statement id="d0_s9">
      <text>nut light-brown, ovoid-ellipsoid or barrel-shaped, 14-20 (-25) × 8-13 mm, glabrous.</text>
      <biological_entity id="o12227" name="nut" name_original="nut" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid-ellipsoid" value_original="ovoid-ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="barrel--shaped" value_original="barrel--shaped" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="25" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s9" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cotyledons distinct.</text>
      <biological_entity id="o12228" name="cotyledon" name_original="cotyledons" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering in spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Riparian forest gallery, margins of thorn scrub, dry tropical forest, lower margins of oak-pine woodland, and cloud forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riparian forest gallery" />
        <character name="habitat" value="margins" constraint="of thorn scrub , dry tropical forest , lower margins of oak-pine woodland , and cloud forest" />
        <character name="habitat" value="thorn scrub" />
        <character name="habitat" value="dry tropical forest" />
        <character name="habitat" value="lower margins" constraint="of oak-pine woodland" />
        <character name="habitat" value="oak-pine woodland" />
        <character name="habitat" value="cloud forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chiapas, Hidalgo, Nuevo León, Oaxaca, Tamaulipas, San Luis Potosí, and Veracruz); Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chiapas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Hidalgo)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Oaxaca)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (and Veracruz)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>41.</number>
  <other_name type="common_name">Net-leaf white oak</other_name>
  <discussion>This widespread species of Mexico and Central America has only recently been discovered in the United States as a small grove of trees about 30 km from the international border in Texas (B. J. Simpson et al. 1992). Quercus polymorpha is becoming available in the nursery trade in Texas and the southeastern United States. It is distinct from the superficially similar Q. splendens Née (= Q. sororia Liebmann) of western Mexico, with which it is sometimes placed in synonymy, in that Q. splendens has connate cotyledons instead of distinct cotyledons, as in Q. polymorpha.</discussion>
  
</bio:treatment>