<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">betula</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">nana</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">nana</taxon_name>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus betula;species nana;subspecies nana;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500256</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, procumbent, widely spreading, or upright, to 1 m.</text>
      <biological_entity id="o4280" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="upright" value_original="upright" />
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="upright" value_original="upright" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark dark-brown.</text>
      <biological_entity id="o4281" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs sparsely to densely pubescent, dull, without heavy resinous coating, without large, warty, resinous glands.</text>
      <biological_entity id="o4282" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="reflectance" src="d0_s2" value="dull" value_original="dull" />
      </biological_entity>
      <biological_entity id="o4283" name="heavy" name_original="heavy" src="d0_s2" type="structure">
        <character is_modifier="false" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o4284" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="large" value_original="large" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s2" value="warty" value_original="warty" />
        <character is_modifier="true" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o4282" id="r608" name="without" negation="false" src="d0_s2" to="o4283" />
      <relation from="o4282" id="r609" name="without" negation="false" src="d0_s2" to="o4284" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade broadly orbiculate or obovate-orbiculate to reniform, 0.8–1.5 × 0.8–2 cm, base rounded or cuneate to nearly cordate;</text>
      <biological_entity id="o4285" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="obovate-orbiculate" name="shape" src="d0_s3" to="reniform" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4286" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="nearly cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially sparsely pubescent to glabrous.</text>
      <biological_entity id="o4287" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="abaxially sparsely pubescent" name="pubescence" src="d0_s4" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Infructescences 0.5–1.5 × 0.4–1 cm;</text>
      <biological_entity id="o4288" name="infructescence" name_original="infructescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scales glabrous, lobes diverging distal to middle, ascending, about equal in length.</text>
      <biological_entity id="o4289" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>2n = 28.</text>
      <biological_entity id="o4290" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="diverging" value_original="diverging" />
        <character char_type="range_value" from="distal" name="position" src="d0_s6" to="middle" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="length" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4291" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tundra, open rocky barrens, and scrubs, in Europe subalpine mountain moors</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tundra" />
        <character name="habitat" value="open rocky barrens" />
        <character name="habitat" value="scrubs" />
        <character name="habitat" value="europe subalpine mountain moors" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; N.W.T.; n Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17a.</number>
  <other_name type="common_name">Arctic dwarf birch</other_name>
  <discussion>Betula nana subsp. nana occurs in both eastern and western Greenland at latitudes north of 63 degrees. In Europe, the range of this subspecies extends across the subarctic zone and southward in the Alps and other ranges.</discussion>
  
</bio:treatment>