<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="L. D. Benson" date="1954" rank="species">austro-oreganus</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>52: 341. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species austro-oreganus;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501122</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or ascending, never rooting nodally, crisped-pilose, base not bulbous.</text>
      <biological_entity id="o28479" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="never; nodally" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="crisped-pilose" value_original="crisped-pilose" />
      </biological_entity>
      <biological_entity id="o28480" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots never tuberous.</text>
      <biological_entity id="o28481" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaf-blades broadly rhombic to semicircular in outline, 3-parted, 2.8-4.3 × 3-5.5 cm, segments 3-lobed, ultimate segments lanceolate, margins entire or toothed, apex narrowly acute or acuminate.</text>
      <biological_entity constraint="basal" id="o28482" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="in outline" constraintid="o28483" from="broadly rhombic" name="shape" src="d0_s2" to="semicircular" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="3-parted" value_original="3-parted" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="length" src="d0_s2" to="4.3" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s2" to="5.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28483" name="outline" name_original="outline" src="d0_s2" type="structure" />
      <biological_entity id="o28484" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o28485" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o28486" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o28487" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: receptacle glabrous;</text>
      <biological_entity id="o28488" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o28489" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals reflexed 1 mm above base, 4-6 × 1.5-3 mm, densely pilose;</text>
      <biological_entity id="o28490" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o28491" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character constraint="above base" constraintid="o28492" name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o28492" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals 5, abaxially red, adaxially yellow, 10-12 × 4-6 mm.</text>
      <biological_entity id="o28493" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o28494" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes hemispheric, 4-7 × 7-10 mm;</text>
      <biological_entity id="o28495" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28496" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o28495" id="r3811" name="part_of" negation="false" src="d0_s6" to="o28496" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 3.4-4.2 × 2.8-3.2 mm, sometimes basally pilose, margin forming narrow rib 0.1-0.2 mm wide;</text>
      <biological_entity id="o28497" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.4" from_unit="mm" name="length" src="d0_s7" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" src="d0_s7" to="3.2" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes basally" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o28498" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28499" name="rib" name_original="rib" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o28498" id="r3812" name="forming" negation="false" src="d0_s7" to="o28499" />
    </statement>
    <statement id="d0_s8">
      <text>beak persistent, lance-subulate, straight or somewhat curved distally, 1.6-2.6 mm.</text>
      <biological_entity id="o28500" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lance-subulate" value_original="lance-subulate" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="somewhat; distally" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s8" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="flowering time" char_type="atypical_range" to="May" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ranunculus austro-oreganus is doubtfully distinct from R. occidentalis var. howellii. L. D. Benson (1954) described the stem as bulbous-based and similar to that of R. bulbosus, but a differentiated base is not evident in material I have seen (some of which was cited by Benson).</discussion>
  
</bio:treatment>