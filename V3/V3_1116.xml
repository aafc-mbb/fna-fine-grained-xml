<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Henk van der Werff</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">lauraceae</taxon_name>
    <taxon_name authority="Lamarck" date="1792" rank="genus">LITSEA</taxon_name>
    <place_of_publication>
      <publication_title>Encycl.</publication_title>
      <place_in_publication>3: 574. 1792, name conserved</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lauraceae;genus LITSEA</taxon_hierarchy>
    <other_info_on_name type="etymology">Litsé, the Chinese name for the plant</other_info_on_name>
    <other_info_on_name type="fna_id">118754</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [or trees], deciduous [or evergreen].</text>
      <biological_entity id="o17395" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate, not aromatic.</text>
      <biological_entity id="o17396" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="not" name="odor" src="d0_s1" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade pinnately veined, rarely with 3 primary-veins, leathery;</text>
      <biological_entity id="o17397" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s2" value="veined" value_original="veined" />
        <character is_modifier="false" name="texture" notes="" src="d0_s2" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o17398" name="primary-vein" name_original="primary-veins" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
      <relation from="o17397" id="r2383" modifier="rarely" name="with" negation="false" src="d0_s2" to="o17398" />
    </statement>
    <statement id="d0_s3">
      <text>surfaces glabrous or variously pubescent;</text>
      <biological_entity id="o17399" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="variously" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>domatia absent.</text>
      <biological_entity id="o17400" name="domatium" name_original="domatia" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences appearing with or before new leaves, axillary, pseudoumbels, subtended by decussate bracts.</text>
      <biological_entity id="o17401" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o17402" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="new" value_original="new" />
      </biological_entity>
      <biological_entity id="o17403" name="pseudoumbel" name_original="pseudoumbels" src="d0_s5" type="structure" />
      <biological_entity id="o17404" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="decussate" value_original="decussate" />
      </biological_entity>
      <relation from="o17401" id="r2384" name="appearing with" negation="false" src="d0_s5" to="o17402" />
      <relation from="o17403" id="r2385" name="subtended by" negation="false" src="d0_s5" to="o17404" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers unisexual, staminate and pistillate on different plants;</text>
      <biological_entity id="o17405" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o17406" is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17406" name="plant" name_original="plants" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>tepals deciduous, yellow, green, or white, equal, glabrous.</text>
      <biological_entity id="o17407" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: stamens 9 (or 12);</text>
      <biological_entity id="o17408" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17409" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="9" value_original="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 4-locular, 4-valved, introrse.</text>
      <biological_entity id="o17410" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17411" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="4-locular" value_original="4-locular" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="4-valved" value_original="4-valved" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers: staminodes 9 (or 12);</text>
      <biological_entity id="o17412" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17413" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="9" value_original="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary globose.</text>
      <biological_entity id="o17414" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17415" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Drupe red, globose, seated in small, single-rimmed cupule.</text>
      <biological_entity id="o17416" name="drupe" name_original="drupe" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character constraint="in cupule" constraintid="o17417" is_modifier="false" name="location" src="d0_s12" value="seated" value_original="seated" />
      </biological_entity>
      <biological_entity id="o17417" name="cupule" name_original="cupule" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="small" value_original="small" />
        <character is_modifier="true" name="relief" src="d0_s12" value="single-rimmed" value_original="single-rimmed" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, mostly in Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="mostly in Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Species ca. 400 (1 in the flora).</discussion>
  <discussion>Mexico has four species of Litsea, one of which extends to Costa Rica in Central America. Litsea merits revision and, as accepted here, it is probably polyphyletic. It is very similar to Lindera and best recognized by its 4-locular anthers (2-locular in Lindera).</discussion>
  
</bio:treatment>