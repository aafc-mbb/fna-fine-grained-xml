<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Wernischeck" date="1763" rank="genus">cimicifuga</taxon_name>
    <taxon_name authority="Kearney" date="1897" rank="species">rubifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>24: 561. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus cimicifuga;species rubifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500380</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 30-140 cm, glabrous.</text>
      <biological_entity id="o19851" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="140" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole angled, 20-50 cm, proximally deeply grooved adaxially, densely pubescent in groove, otherwise glabrous or sparsely pubescent.</text>
      <biological_entity id="o19852" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o19853" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="angled" value_original="angled" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character is_modifier="false" modifier="proximally deeply; adaxially" name="architecture" src="d0_s1" value="grooved" value_original="grooved" />
        <character constraint="in groove" constraintid="o19854" is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o19854" name="groove" name_original="groove" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade 1-2-ternately compound;</text>
      <biological_entity id="o19855" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="1-2-ternately" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 3-9 (-17);</text>
      <biological_entity id="o19856" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="17" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>terminal leaflet of central segments broadly obovate, deeply 3-5-lobed, 9-30 × 9-25 cm, with 5-9 prominent veins arising basally, base deeply cordate, margins coarsely and irregularly dentate, apex sharply acuminate, surfaces abaxially with long, appressed, delicate hairs, adaxially glabrous;</text>
      <biological_entity constraint="segment" id="o19857" name="leaflet" name_original="leaflet" src="d0_s4" type="structure" constraint_original="segment terminal; segment">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="3-5-lobed" value_original="3-5-lobed" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="width" src="d0_s4" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o19858" name="segment" name_original="segments" src="d0_s4" type="structure" />
      <biological_entity id="o19859" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="9" />
        <character is_modifier="true" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="basally" name="orientation" src="d0_s4" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o19860" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o19861" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o19862" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o19864" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="fragility" src="d0_s4" value="delicate" value_original="delicate" />
      </biological_entity>
      <relation from="o19857" id="r2682" name="part_of" negation="false" src="d0_s4" to="o19858" />
      <relation from="o19857" id="r2683" name="with" negation="false" src="d0_s4" to="o19859" />
      <relation from="o19863" id="r2684" name="with" negation="false" src="d0_s4" to="o19864" />
    </statement>
    <statement id="d0_s5">
      <text>other leaflets 8-24 × 6-22 cm.</text>
      <biological_entity id="o19863" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="adaxially" name="pubescence" notes="" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19865" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s5" to="24" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s5" to="22" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences erect panicles of 2-6 racemelike branches, terminal raceme 15-30 cm, puberulent to short-pubescent;</text>
      <biological_entity id="o19866" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o19867" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o19868" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="6" />
        <character is_modifier="true" name="shape" src="d0_s6" value="racemelike" value_original="racemelike" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o19869" name="raceme" name_original="raceme" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s6" to="short-pubescent" />
      </biological_entity>
      <relation from="o19867" id="r2685" name="consist_of" negation="false" src="d0_s6" to="o19868" />
    </statement>
    <statement id="d0_s7">
      <text>bracts 3, subtending pedicel, central bract larger, lanceolate, 2 mm, lateral bracts ovate-deltate;</text>
      <biological_entity id="o19870" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o19871" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <biological_entity constraint="central" id="o19872" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="larger" value_original="larger" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19873" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-deltate" value_original="ovate-deltate" />
      </biological_entity>
      <relation from="o19870" id="r2686" name="subtending" negation="false" src="d0_s7" to="o19871" />
    </statement>
    <statement id="d0_s8">
      <text>pedicel to 5 mm, short-pubescent, bracteoles absent.</text>
      <biological_entity id="o19874" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="short-pubescent" value_original="short-pubescent" />
      </biological_entity>
      <biological_entity id="o19875" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 5, white;</text>
      <biological_entity id="o19876" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19877" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals absent;</text>
      <biological_entity id="o19878" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19879" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 35-65;</text>
      <biological_entity id="o19880" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19881" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="35" name="quantity" src="d0_s11" to="65" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 3-5 mm;</text>
      <biological_entity id="o19882" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o19883" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistils 1 (-2), sessile, sparsely glandular;</text>
      <biological_entity id="o19884" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19885" name="pistil" name_original="pistils" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="2" />
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s13" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style short, straight or slightly recurved;</text>
      <biological_entity id="o19886" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o19887" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma minute, 0.2-0.3 mm wide.</text>
      <biological_entity id="o19888" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o19889" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="minute" value_original="minute" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s15" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Follicles 1 (-2), sessile, oblong, strongly compressed laterally, 8-20 mm, thin walled.</text>
      <biological_entity id="o19890" name="follicle" name_original="follicles" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="2" />
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="strongly; laterally" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s16" to="20" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s16" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="walled" value_original="walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds reddish-brown, lenticular, covering of scales giving cylindric appearance, ca. 3 mm, covered with reddish-brown, membranous scales.</text>
      <biological_entity id="o19892" name="scale" name_original="scales" src="d0_s17" type="structure" />
      <biological_entity id="o19893" name="appearance" name_original="appearance" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o19894" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s17" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="true" name="texture" src="d0_s17" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o19891" id="r2687" name="covering of" negation="false" src="d0_s17" to="o19892" />
      <relation from="o19892" id="r2688" name="giving" negation="false" src="d0_s17" to="o19893" />
      <relation from="o19891" id="r2689" name="covered with" negation="false" src="d0_s17" to="o19894" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 16.</text>
      <biological_entity id="o19891" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lenticular" value_original="lenticular" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19895" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (Aug–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>North-facing, limestone talus slopes and river bluffs, ravines, and coves, and along rivers and creeks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone talus slopes" modifier="north-facing" />
        <character name="habitat" value="river bluffs" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="coves" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="creeks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ill., Ind., Ky., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Appalachian bugbane</other_name>
  <discussion>In addition to being found in the Ridge and Valley Province of Tennessee and Virginia, Cimicifuga rubifolia has disjunct populations in Alabama, the lower Ohio River Valley, and in the middle Tennessee counties of Davidson, Montgomery, and Stewart.</discussion>
  <discussion>The name Cimicifuga racemosa var. cordifolia has been misapplied to C. rubifolia. See discussion of C. cordifolia under C. americana.</discussion>
  
</bio:treatment>