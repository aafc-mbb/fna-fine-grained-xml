<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Susan L. Sherman-Broyles, William T. Barker, Leila M. Schulz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">368</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">ULMACEAE</taxon_name>
    <taxon_hierarchy>family ULMACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10928</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, deciduous (sometimes tardily deciduous in Ulmus).</text>
      <biological_entity id="o2143" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark smooth to deeply fissured or scaly and flaky;</text>
      <biological_entity id="o2145" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s1" to="deeply fissured" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="flaky" value_original="flaky" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>sap watery.</text>
      <biological_entity id="o2146" name="sap" name_original="sap" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="watery" value_original="watery" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate [opposite], distichous [or not], simple;</text>
      <biological_entity id="o2147" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules present;</text>
      <biological_entity id="o2148" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present.</text>
      <biological_entity id="o2149" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade: base often oblique, margins entire or serrate, crenate, or toothed;</text>
      <biological_entity id="o2150" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure" />
      <biological_entity id="o2151" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="orientation_or_shape" src="d0_s6" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity id="o2152" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>venation pinnate to palmate-pinnate.</text>
      <biological_entity id="o2153" name="leaf-blade" name_original="leaf-blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="pinnate" name="architecture_or_shape" src="d0_s7" to="palmate-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences axillary, cymes, racemes, fascicles, or flowers solitary, arising from branchlets of previous season (e.g., Ulmus) or of current season (e.g., Celtis).</text>
      <biological_entity id="o2154" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o2155" name="cyme" name_original="cymes" src="d0_s8" type="structure" />
      <biological_entity id="o2156" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o2157" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
        <character constraint="from branchlets" constraintid="o2158" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o2158" name="branchlet" name_original="branchlets" src="d0_s8" type="structure" />
      <biological_entity constraint="previous" id="o2159" name="season" name_original="season" src="d0_s8" type="structure" />
      <biological_entity constraint="current" id="o2160" name="season" name_original="season" src="d0_s8" type="structure" />
      <relation from="o2158" id="r267" name="part_of" negation="false" src="d0_s8" to="o2159" />
      <relation from="o2158" id="r268" name="part_of" negation="false" src="d0_s8" to="o2160" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual or unisexual, staminate and pistillate on same [different] plants;</text>
      <biological_entity id="o2161" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o2162" is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2162" name="plant" name_original="plants" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>sepals persistent, (1-) 5 (-9), connate [distinct], imbricate or valvate;</text>
      <biological_entity id="o2163" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="9" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="valvate" value_original="valvate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals absent;</text>
      <biological_entity id="o2164" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens usually as many as calyx lobes, hypogynous, opposite calyx lobes, erect in bud;</text>
      <biological_entity id="o2165" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s12" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o2166" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
      <biological_entity constraint="calyx" id="o2167" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="opposite" value_original="opposite" />
        <character constraint="in bud" constraintid="o2168" is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o2168" name="bud" name_original="bud" src="d0_s12" type="structure" />
      <relation from="o2165" id="r269" name="as many as" negation="false" src="d0_s12" to="o2166" />
    </statement>
    <statement id="d0_s13">
      <text>filaments free or arising from calyx-tube, distinct, curved or sigmoid in bud;</text>
      <biological_entity id="o2169" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="free" value_original="free" />
        <character constraint="from calyx-tube" constraintid="o2170" is_modifier="false" name="orientation" src="d0_s13" value="arising" value_original="arising" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character constraint="in bud" constraintid="o2171" is_modifier="false" name="course" src="d0_s13" value="sigmoid" value_original="sigmoid" />
      </biological_entity>
      <biological_entity id="o2170" name="calyx-tube" name_original="calyx-tube" src="d0_s13" type="structure" />
      <biological_entity id="o2171" name="bud" name_original="bud" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 2-locular, dehiscence longitudinal;</text>
      <biological_entity id="o2172" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistils 1, 2 (-3) -carpellate;</text>
      <biological_entity id="o2173" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2(-3)-carpellate" value_original="2(-3)-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary 1 (-2) -locular;</text>
      <biological_entity id="o2174" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="1(-2)-locular" value_original="1(-2)-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 1 per locule, pendulous from apex of locule, anatropous or amphitropous;</text>
      <biological_entity id="o2175" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character constraint="per locule" constraintid="o2176" name="quantity" src="d0_s17" value="1" value_original="1" />
        <character constraint="from apex" constraintid="o2177" is_modifier="false" name="orientation" notes="" src="d0_s17" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="amphitropous" value_original="amphitropous" />
      </biological_entity>
      <biological_entity id="o2176" name="locule" name_original="locule" src="d0_s17" type="structure" />
      <biological_entity id="o2177" name="apex" name_original="apex" src="d0_s17" type="structure" />
      <biological_entity id="o2178" name="locule" name_original="locule" src="d0_s17" type="structure" />
      <relation from="o2177" id="r270" name="part_of" negation="false" src="d0_s17" to="o2178" />
    </statement>
    <statement id="d0_s18">
      <text>styles (1-) 2, distinct, receptive stigmatic area decurrent on distal inner margin of style-branch.</text>
      <biological_entity id="o2179" name="style" name_original="styles" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s18" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s18" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o2180" name="receptive" name_original="receptive" src="d0_s18" type="structure" />
      <biological_entity constraint="stigmatic" id="o2181" name="area" name_original="area" src="d0_s18" type="structure">
        <character constraint="on distal inner margin" constraintid="o2182" is_modifier="false" name="shape" src="d0_s18" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity constraint="distal inner" id="o2182" name="margin" name_original="margin" src="d0_s18" type="structure" />
      <biological_entity id="o2183" name="style-branch" name_original="style-branch" src="d0_s18" type="structure" />
      <relation from="o2182" id="r271" name="part_of" negation="false" src="d0_s18" to="o2183" />
    </statement>
    <statement id="d0_s19">
      <text>Fruits fleshy drupes, samaras, or nutlike.</text>
      <biological_entity id="o2184" name="fruit" name_original="fruits" src="d0_s19" type="structure" />
      <biological_entity id="o2185" name="drupe" name_original="drupes" src="d0_s19" type="structure">
        <character is_modifier="true" name="texture" src="d0_s19" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o2186" name="samara" name_original="samaras" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s19" value="nutlike" value_original="nutlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 1;</text>
      <biological_entity id="o2187" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>arils absent;</text>
      <biological_entity id="o2188" name="aril" name_original="arils" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>endosperm absent to scanty, consisting of 1 layer of thick-walled cells;</text>
      <biological_entity id="o2189" name="endosperm" name_original="endosperm" src="d0_s22" type="structure">
        <character char_type="range_value" from="absent" name="quantity" src="d0_s22" to="scanty" />
      </biological_entity>
      <biological_entity id="o2190" name="layer" name_original="layer" src="d0_s22" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s22" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2191" name="cell" name_original="cells" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s22" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <relation from="o2189" id="r272" name="consisting of" negation="false" src="d0_s22" to="o2190" />
      <relation from="o2190" id="r273" name="part_of" negation="false" src="d0_s22" to="o2191" />
    </statement>
    <statement id="d0_s23">
      <text>embryo straight or curved.</text>
      <biological_entity id="o2192" name="embryo" name_original="embryo" src="d0_s23" type="structure">
        <character is_modifier="false" name="course" src="d0_s23" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s23" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical and north temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical and north temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Elm Family</other_name>
  <discussion>Genera ca. 18, species ca. 150 (4 genera, 19 species in the flora).</discussion>
  <discussion>Plants of this family are wind-pollinated (anemophilous).</discussion>
  <discussion>Ulmaceae are frequently divided into two subfamilies, Ulmoideae and Celtoideae; they are sometimes separated into two families, Ulmaceae and Celtidaceae (I. A. Grudzinskaya 1965). These subfamilial or familial distinctions are supported by flavonoid chemistry (D. E. Giannasi and K. J. Niklas 1977; D. E. Giannasi 1978), pollen morphology (M. Zavada 1983), and some anatomic structures (E. M. Sweitzer 1971). Typically the Ulmoideae have flavonols, strictly pinnately veined leaves, and dry fruits; the Celtoideae have glycoflavones, pinnipalmately veined leaves, and drupaceous fruits. Some genera (e.g., Zelkova, with pinnately veined leaves and drupaceous fruits) are intermediate, and various authors place them in different subfamilies.</discussion>
  <discussion>In this treatment Ulmus and Planera are considered part of the subfamily Ulmoideae; Celtis and Trema are in subfamily Celtoideae. Zelkova serrata is widely cultivated as an ornamental tree in North America, but it is not known to be naturalized in the flora. Chemical similarities between subfamilies include the presence of proanthocyanins with some tannins and scattered mucilaginous cells or canals. Additionally, members of the family share a strong tendency toward mineralization of the cell walls with calcium carbonate or silica and possess solitary or clustered crystals of calcium oxalate.</discussion>
  <discussion>Ulmaceae include trees and shrubs of horticultural importance.</discussion>
  <references>
    <reference>Barker, W. T. 1986. Ulmaceae. In: Great Plains Flora Association. 1986. Flora of the Great Plains. Lawrence, Kans. Pp. 119-123.</reference>
    <reference>Elias, T. S. 1970. The genera of Ulmaceae in the southeastern United States. J. Arnold Arbor. 51: 18-40.</reference>
    <reference>Giannasi, D. E. 1978. Generic relationships in the Ulmaceae based on flavonoid chemistry. Taxon 27: 331-344.</reference>
    <reference>Giannasi, D. E. and K. J. Niklas. 1977. Flavonoids and other constituents of fossil Miocene Celtis and Ulmus (Succor Creek Flora). Science 197: 765-767.</reference>
    <reference>Grudzinskaya, I. A. 1965. The Ulmaceae and reasons for distinguishing the Celtidoideae as a separate family Celtidaceae Link. Bot. Zhurn. (Moscow &amp; Leningrad) 52: 1723-1749.</reference>
    <reference>Sweitzer, E. M. 1971. The comparative anatomy of Ulmaceae. J. Arnold Arbor. 52: 523-585.</reference>
    <reference>Zavada, M. 1983. Pollen morphology of Ulmaceae. Grana 22: 23-30.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade pinnately veined; fruits dry, nutlike or samaras.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade palmately veined at base, pinnately veined over remainder of blade; fruits drupes.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers bisexual; fruits samaras.</description>
      <determination>1 Ulmus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers normally unisexual, inflorescences usually with a few bisexual flowers; fruits nutlike.</description>
      <determination>2 Planera</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade entire or serrate to ca. 3/4 length; flowers solitary or in few-flowered clusters; drupes 1.</description>
      <determination>3 Celtis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade crenate to serrate for entire length; flowers 12-20, in cymes.</description>
      <determination>4 Trema</determination>
    </key_statement>
  </key>
</bio:treatment>