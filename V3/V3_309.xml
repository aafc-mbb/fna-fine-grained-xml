<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Linnaeus" date="unknown" rank="family">fumariaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="1805" rank="genus">corydalis</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) A. Gray" date="1886" rank="species">micrantha</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>11: 189. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fumariaceae;genus corydalis;species micrantha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500441</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corydalis</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">aurea</taxon_name>
    <taxon_name authority="Engelmann ex A. Gray" date="unknown" rank="variety">micrantha</taxon_name>
    <place_of_publication>
      <publication_title>Manual ed.</publication_title>
      <place_in_publication>5, 62. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Corydalis;species aurea;variety micrantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Capnoides</taxon_name>
    <taxon_name authority="(Engelmann ex A. Gray) Britton" date="unknown" rank="species">micranthum</taxon_name>
    <taxon_hierarchy>genus Capnoides;species micranthum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants winter-annual, glaucous to nearly green, from somewhat succulent roots.</text>
      <biological_entity id="o4452" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" value_original="winter-annual" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4453" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="somewhat" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
      </biological_entity>
      <relation from="o4452" id="r638" name="from" negation="false" src="d0_s0" to="o4453" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1-several, erect to prostrate-ascending, (1.5-) 2-4 (-6) dm.</text>
      <biological_entity id="o4454" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="prostrate-ascending" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="6" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves crowded, compound;</text>
      <biological_entity id="o4455" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade with 2 orders of leaflets and lobes;</text>
      <biological_entity id="o4456" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o4457" name="order" name_original="orders" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4458" name="leaflet" name_original="leaflets" src="d0_s3" type="structure" />
      <biological_entity id="o4459" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <relation from="o4456" id="r639" name="with" negation="false" src="d0_s3" to="o4457" />
      <relation from="o4457" id="r640" name="part_of" negation="false" src="d0_s3" to="o4458" />
      <relation from="o4457" id="r641" name="part_of" negation="false" src="d0_s3" to="o4459" />
    </statement>
    <statement id="d0_s4">
      <text>ultimate lobes ovate, oblongelliptic, or obovate, margins incised, apex subapiculate.</text>
      <biological_entity constraint="ultimate" id="o4460" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o4461" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o4462" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="subapiculate" value_original="subapiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, (6-) 10-16 (-20) -flowered, primary racemes slightly to conspicuously exceeding leaves, secondary racemes fewer flowered, exceeded by leaves, cleistogamous-flowered racemes frequently present, 1-5-flowered, inconspicuous;</text>
      <biological_entity id="o4463" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="(6-)10-16(-20)-flowered" value_original="(6-)10-16(-20)-flowered" />
      </biological_entity>
      <biological_entity constraint="primary" id="o4464" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
      <biological_entity id="o4465" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="secondary" id="o4466" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="fewer" value_original="fewer" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="flowered" value_original="flowered" />
      </biological_entity>
      <biological_entity id="o4467" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o4468" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="cleistogamous-flowered" value_original="cleistogamous-flowered" />
        <character is_modifier="false" modifier="frequently" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-5-flowered" value_original="1-5-flowered" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <relation from="o4464" id="r642" modifier="slightly to conspicuously" name="exceeding" negation="false" src="d0_s5" to="o4465" />
      <relation from="o4466" id="r643" name="exceeded by" negation="false" src="d0_s5" to="o4467" />
    </statement>
    <statement id="d0_s6">
      <text>bracts elliptic to attenuate-ovate, 5-8 × 2-4 mm, margins denticulate, distal bracts usually much reduced, those of cleistogamous racemes minute.</text>
      <biological_entity id="o4469" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="attenuate-ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4470" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4471" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually much" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="size" src="d0_s6" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o4472" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <relation from="o4471" id="r644" name="part_of" negation="false" src="d0_s6" to="o4472" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect or spreading;</text>
      <biological_entity id="o4473" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 2-6 mm;</text>
      <biological_entity id="o4474" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals ovate, to 1.5 mm, margins often sinuate or dentate;</text>
      <biological_entity id="o4475" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4476" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals pale to medium yellow;</text>
      <biological_entity id="o4477" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s10" to="medium yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>spurred petal slightly to strongly curved, (11-) 12-14 (-15) mm, spur straight, 4-7 mm, apex obtuse or ± globose, crest low, wrinkled, rarely obsolescent, marginal wing well developed, sometimes revolute, unspurred outer petal slightly bent, 9-11 mm, crest low;</text>
      <biological_entity id="o4478" name="petal" name_original="petal" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="slightly to strongly" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character char_type="range_value" from="11" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4479" name="spur" name_original="spur" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4480" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o4481" name="crest" name_original="crest" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="low" value_original="low" />
        <character is_modifier="false" name="relief" src="d0_s11" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" modifier="rarely" name="prominence" src="d0_s11" value="obsolescent" value_original="obsolescent" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o4482" name="wing" name_original="wing" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s11" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="outer" id="o4483" name="petal" name_original="petal" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="unspurred" value_original="unspurred" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="bent" value_original="bent" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4484" name="crest" name_original="crest" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="low" value_original="low" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>inner petals oblanceolate, 7-10 mm, blade apex 2 times or more wider than base, basal lobes obscure, claw 3-4 mm;</text>
      <biological_entity constraint="inner" id="o4485" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="blade" id="o4486" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character constraint="base" constraintid="o4487" is_modifier="false" name="width" src="d0_s12" value="2 times or more wider than base" />
      </biological_entity>
      <biological_entity id="o4487" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity constraint="basal" id="o4488" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o4489" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectariferous spur straight or curved, sometimes clavate, ca. 3/5 length of petal spur;</text>
      <biological_entity constraint="nectariferous" id="o4490" name="spur" name_original="spur" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
        <character name="length" src="d0_s13" value="3/5 length of petal spur" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style ca. 4 mm;</text>
      <biological_entity id="o4491" name="style" name_original="style" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma rectangular, 2-lobed, 1/2 as long as wide, with 8 papillae.</text>
      <biological_entity id="o4492" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s15" value="2-lobed" value_original="2-lobed" />
        <character name="quantity" src="d0_s15" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="width" src="d0_s15" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o4493" name="papilla" name_original="papillae" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="8" value_original="8" />
      </biological_entity>
      <relation from="o4492" id="r645" name="with" negation="false" src="d0_s15" to="o4493" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules erect, linear, slender, straight to slightly incurved, 10-35 mm, essentially glabrous, usually shorter in cleistogamous-flowered racemes.</text>
      <biological_entity id="o4494" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" name="size" src="d0_s16" value="slender" value_original="slender" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s16" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character constraint="in racemes" constraintid="o4495" is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4495" name="raceme" name_original="racemes" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="cleistogamous-flowered" value_original="cleistogamous-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds ca. 1.5 mm diam., concentrically and minutely decorated, marginal ring absent.</text>
      <biological_entity id="o4496" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character name="diameter" src="d0_s17" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" modifier="concentrically; minutely" name="relief" src="d0_s17" value="decorated" value_original="decorated" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o4497" name="ring" name_original="ring" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ill., Iowa, Kans., La., Minn., Miss., Mo., N.C., Nebr., Okla., S.C., S.Dak., Tenn., Tex., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <discussion>Corydalis micrantha can be distinguished readily from other yellow-flowered North American species by its very small seeds. Cleistogamy is encountered regularly in C. micrantha. A single plant from any part of the range may have only cleistogamous flowers, only chasmogamous flowers, or both types. Plants having only cleistogamous flowers usually are much more profusely and delicately branched. In C. micrantha, at least, shade and age appear to play roles in the initiation of cleistogamy.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Racemes of chasmogamous flowers not greatly exceeding leaves, often short; petal spur ± globose at apex; capsules often stout, commonly 10–15 mm.</description>
      <determination>8a Corydalis micrantha subsp. micrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Racemes of chasmogamous flowers often greatly exceeding leaves, elongate; petal spur blunt, not globose at apex; capsules slender, commonly 15–30 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems usually weak; capsules commonly 15–20 mm.</description>
      <determination>8b Corydalis micrantha subsp. australis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems usually stout; capsules commonly 21–35 mm.</description>
      <determination>8c Corydalis micrantha subsp. texensis</determination>
    </key_statement>
  </key>
</bio:treatment>