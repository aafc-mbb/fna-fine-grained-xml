<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">CHELIDONIUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 505. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 224. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus CHELIDONIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek cheilidon, swallow (bird), perhaps from lore reported by Aristotle and others that mother swallows bathe eyes of their young with the sap</other_info_on_name>
    <other_info_on_name type="fna_id">106616</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, biennial or perennial, caulescent, from stout rhizomes or taproots;</text>
      <biological_entity id="o10879" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o10880" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o10881" name="taproot" name_original="taproots" src="d0_s0" type="structure" />
      <relation from="o10879" id="r1518" name="from" negation="false" src="d0_s0" to="o10880" />
      <relation from="o10879" id="r1519" name="from" negation="false" src="d0_s0" to="o10881" />
    </statement>
    <statement id="d0_s1">
      <text>sap yellow to orange.</text>
      <biological_entity id="o10882" name="sap" name_original="sap" src="d0_s1" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s1" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems leafy.</text>
      <biological_entity id="o10883" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves petiolate;</text>
      <biological_entity id="o10884" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal rosulate, cauline alternate;</text>
      <biological_entity constraint="basal" id="o10885" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o10886" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 1-2× pinnately lobed.</text>
      <biological_entity id="o10887" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="1-2×pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary or terminal, umbelliform, few flowered;</text>
      <biological_entity id="o10888" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="umbelliform" value_original="umbelliform" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o10889" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 2, distinct;</text>
      <biological_entity id="o10890" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10891" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 4;</text>
      <biological_entity id="o10892" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10893" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens ca. 12-many;</text>
      <biological_entity id="o10894" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10895" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" is_modifier="false" name="quantity" src="d0_s10" to="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistil 2-carpellate;</text>
      <biological_entity id="o10896" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10897" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-carpellate" value_original="2-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary 1-locular;</text>
      <biological_entity id="o10898" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10899" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style ± distinct;</text>
      <biological_entity id="o10900" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o10901" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma 2-lobed.</text>
      <biological_entity id="o10902" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o10903" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules erect, 2-valved, dehiscing from base.</text>
      <biological_entity id="o10904" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-valved" value_original="2-valved" />
        <character constraint="from base" constraintid="o10905" is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o10905" name="base" name_original="base" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds few-to-many, arillate.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 6.</text>
      <biological_entity id="o10906" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s16" to="many" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o10907" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Celandine</other_name>
  <other_name type="common_name">greater celandine</other_name>
  <other_name type="common_name">rock-poppy</other_name>
  <other_name type="common_name">swallowwort</other_name>
  <other_name type="common_name">chélidoine</other_name>
  <discussion>Species 1</discussion>
  
</bio:treatment>