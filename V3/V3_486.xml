<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">aconitum</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey &amp; A. Gray" date="1838" rank="species">columbianum</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey &amp; A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 34. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus aconitum;species columbianum</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500014</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots tuberous, tuber distally not obviously bulblike, to 60 × 15 mm, parent tuber producing 1 (rarely 2) daughter tubers with connecting rhizome very short, i. e., tubers ±contiguous.</text>
      <biological_entity id="o8573" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <biological_entity id="o8574" name="tuber" name_original="tuber" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="distally not obviously" name="architecture_or_shape" src="d0_s0" value="bulblike" value_original="bulblike" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s0" to="60" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s0" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="parent" id="o8575" name="tuber" name_original="tuber" src="d0_s0" type="structure" />
      <biological_entity constraint="daughter" id="o8576" name="tuber" name_original="tubers" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8577" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="fusion_or_position_relational" src="d0_s0" value="connecting" value_original="connecting" />
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o8578" name="tuber" name_original="tubers" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s0" value="contiguous" value_original="contiguous" />
      </biological_entity>
      <relation from="o8575" id="r1194" name="producing" negation="false" src="d0_s0" to="o8576" />
      <relation from="o8575" id="r1195" name="with" negation="false" src="d0_s0" to="o8577" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect and stout to twining and reclining, 2-30 dm.</text>
      <biological_entity id="o8579" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="twining" value_original="twining" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="reclining" value_original="reclining" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="30" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: blade deeply 3-5 (-7) -divided, usually with more than 2 mm leaf tissue between deepest sinus and base of blade, 5-15 cm wide, segment margins variously cleft and toothed.</text>
      <biological_entity constraint="cauline" id="o8580" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8581" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="3-5(-7)-divided" value_original="3-5(-7)-divided" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" notes="" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="between sinus and leaf" constraintid="o8583-o8584" id="o8582" name="tissue" name_original="tissue" src="d0_s2" type="structure" constraint_original="between  sinus and  leaf, ">
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s2" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="deepest" id="o8583" name="sinus" name_original="sinus" src="d0_s2" type="structure" />
      <biological_entity constraint="deepest" id="o8584" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o8585" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity constraint="segment" id="o8586" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="variously" name="architecture_or_shape" src="d0_s2" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <relation from="o8581" id="r1196" modifier="usually" name="with" negation="false" src="d0_s2" to="o8582" />
      <relation from="o8582" id="r1197" name="part_of" negation="false" src="d0_s2" to="o8585" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences open racemes or panicles.</text>
      <biological_entity id="o8587" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o8588" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o8589" name="panicle" name_original="panicles" src="d0_s3" type="structure" />
      <relation from="o8587" id="r1198" name="open" negation="false" src="d0_s3" to="o8588" />
      <relation from="o8587" id="r1199" name="open" negation="false" src="d0_s3" to="o8589" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers commonly blue, sometimes white, cream colored, or blue tinged at sepal margins, 18-50 mm from tips of pendent sepals to top of hood;</text>
      <biological_entity id="o8590" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="commonly" name="coloration" src="d0_s4" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="cream colored" value_original="cream colored" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="blue tinged" value_original="blue tinged" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="cream colored" value_original="cream colored" />
        <character constraint="at sepal margins" constraintid="o8591" is_modifier="false" name="coloration" src="d0_s4" value="blue tinged" value_original="blue tinged" />
        <character char_type="range_value" constraint="from tips" constraintid="o8592" from="18" from_unit="mm" name="location" notes="" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="sepal" id="o8591" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o8592" name="tip" name_original="tips" src="d0_s4" type="structure" />
      <biological_entity id="o8593" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o8594" name="top" name_original="top" src="d0_s4" type="structure" />
      <biological_entity id="o8595" name="hood" name_original="hood" src="d0_s4" type="structure" />
      <relation from="o8592" id="r1200" name="part_of" negation="false" src="d0_s4" to="o8593" />
      <relation from="o8592" id="r1201" name="to" negation="false" src="d0_s4" to="o8594" />
      <relation from="o8594" id="r1202" name="top of" negation="false" src="d0_s4" to="o8595" />
    </statement>
    <statement id="d0_s5">
      <text>pendent sepals 6-16 mm;</text>
      <biological_entity id="o8596" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="pendent" value_original="pendent" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>hood conic-hemispheric, hemispheric, or crescent-shaped, 11-34mm from receptacle to top of hood, 6-26 mm wide from receptacle to beak apex.</text>
      <biological_entity id="o8597" name="hood" name_original="hood" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="conic-hemispheric" value_original="conic-hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crescent--shaped" value_original="crescent--shaped" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crescent--shaped" value_original="crescent--shaped" />
        <character char_type="range_value" constraint="from receptacle" constraintid="o8598" from="11" from_unit="mm" name="location" src="d0_s6" to="34" to_unit="mm" />
        <character char_type="range_value" constraint="from receptacle" constraintid="o8601" from="6" from_unit="mm" name="width" notes="" src="d0_s6" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8598" name="receptacle" name_original="receptacle" src="d0_s6" type="structure" />
      <biological_entity id="o8599" name="top" name_original="top" src="d0_s6" type="structure" />
      <biological_entity id="o8600" name="hood" name_original="hood" src="d0_s6" type="structure" />
      <biological_entity id="o8601" name="receptacle" name_original="receptacle" src="d0_s6" type="structure" />
      <biological_entity constraint="beak" id="o8602" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <relation from="o8598" id="r1203" name="to" negation="false" src="d0_s6" to="o8599" />
      <relation from="o8599" id="r1204" name="top of" negation="false" src="d0_s6" to="o8600" />
      <relation from="o8601" id="r1205" name="to" negation="false" src="d0_s6" to="o8602" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Colo., Idaho, Iowa, Mont., N.Mex., N.Y., Nev., Ohio, Oreg., S.Dak., Utah, Wash., Wis., Wyo.; Moist areas, primarily in w North America, sporadic in e U.S.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Moist areas" establishment_means="native" />
        <character name="distribution" value="primarily in w North America" establishment_means="native" />
        <character name="distribution" value="sporadic in e U.S" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Available information suggests that Aconitum columbianum is probably not one of the extremely toxic aconites (D. E. Brink 1982; J. D. Olsen et al. 1990).</discussion>
  <references>
    <reference>Brink, D. E. 1980. Reproduction and variation in Aconitum columbianum (Ranunculaceae), with emphasis on California populations. Amer. J.Bot. 67: 263-273.</reference>
    <reference>Brink, D. E. and J. M. J. de Wet. 1980. Interpopulation variation in nectar production in Aconitum columbianum (Ranunculaceae). Oecologia 47: 160-163.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf axils and inflorescence without bulbils</description>
      <determination>5a Aconitum columbianum subsp. columbianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf axils and/or inflorescence with conspicuous bulbils</description>
      <determination>5b Aconitum columbianum subsp. viviparum</determination>
    </key_statement>
  </key>
</bio:treatment>