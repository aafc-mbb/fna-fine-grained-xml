<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">133</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="(Prantl) L. D. Benson" date="1940" rank="subgenus">Coptidium</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>27: 807. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus Coptidium</taxon_hierarchy>
    <other_info_on_name type="fna_id">310011</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="b. Coptidium Prantl" date="1887" rank="subgenus">Ranunculus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>9: 266. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>subgenus Ranunculus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glabrous.</text>
      <biological_entity id="o1650" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, not bulbous-based, without bulbils.</text>
      <biological_entity id="o1651" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="bulbous-based" value_original="bulbous-based" />
      </biological_entity>
      <biological_entity id="o1652" name="bulbil" name_original="bulbils" src="d0_s1" type="structure" />
      <relation from="o1651" id="r201" name="without" negation="false" src="d0_s1" to="o1652" />
    </statement>
    <statement id="d0_s2">
      <text>Roots nodal, never tuberous.</text>
      <biological_entity id="o1653" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="nodal" value_original="nodal" />
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s2" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, simple, petiolate;</text>
      <biological_entity id="o1654" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade deeply 3-parted to base, as wide as long, segments toothed or shallowly lobed.</text>
      <biological_entity id="o1655" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character constraint="to base" constraintid="o1656" is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="3-parted" value_original="3-parted" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s4" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o1656" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o1657" name="segment" name_original="segments" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, solitary flowers.</text>
      <biological_entity id="o1658" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o1659" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers pedicellate (pedicels naked, rarely leafy);</text>
      <biological_entity id="o1660" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals deciduous soon after anthesis, 3;</text>
      <biological_entity id="o1661" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="after anthesis" is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 5-8, yellow;</text>
      <biological_entity id="o1662" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary scale attached on 3 sides, forming pocket, glabrous, free margin entire;</text>
      <biological_entity constraint="nectary" id="o1663" name="scale" name_original="scale" src="d0_s9" type="structure">
        <character constraint="on sides" constraintid="o1664" is_modifier="false" name="fixation" src="d0_s9" value="attached" value_original="attached" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1664" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o1665" name="pocket" name_original="pocket" src="d0_s9" type="structure" />
      <biological_entity id="o1666" name="margin" name_original="margin" src="d0_s9" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o1663" id="r202" name="forming" negation="false" src="d0_s9" to="o1665" />
    </statement>
    <statement id="d0_s10">
      <text>style present.</text>
      <biological_entity id="o1667" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits achenes, 1-locular;</text>
      <biological_entity constraint="fruits" id="o1668" name="achene" name_original="achenes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>achene body oblong-lenticular, 1.9-2.6 times as wide as thick, prolonged beyond seed as corky distal appendage;</text>
      <biological_entity constraint="achene" id="o1669" name="body" name_original="body" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong-lenticular" value_original="oblong-lenticular" />
        <character is_modifier="false" name="width_or_width" src="d0_s12" value="1.9-2.6 times as wide as thick" />
        <character constraint="beyond seed" constraintid="o1670" is_modifier="false" name="length" src="d0_s12" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o1670" name="seed" name_original="seed" src="d0_s12" type="structure" />
      <biological_entity constraint="distal" id="o1671" name="appendage" name_original="appendage" src="d0_s12" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s12" value="corky" value_original="corky" />
      </biological_entity>
      <relation from="o1670" id="r203" name="as" negation="false" src="d0_s12" to="o1671" />
    </statement>
    <statement id="d0_s13">
      <text>wall thick, smooth;</text>
      <biological_entity id="o1672" name="wall" name_original="wall" src="d0_s13" type="structure">
        <character is_modifier="false" name="width" src="d0_s13" value="thick" value_original="thick" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>margin low corky band;</text>
      <biological_entity id="o1673" name="margin" name_original="margin" src="d0_s14" type="structure" />
      <biological_entity id="o1674" name="band" name_original="band" src="d0_s14" type="structure">
        <character is_modifier="true" name="position" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s14" value="corky" value_original="corky" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak much shorter than achene body.</text>
      <biological_entity id="o1675" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character constraint="than achene body" constraintid="o1676" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity constraint="achene" id="o1676" name="body" name_original="body" src="d0_s15" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2g.</number>
  <discussion>Species 1 (1 in the flora).</discussion>
  
</bio:treatment>