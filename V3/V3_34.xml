<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="M. J. Warnock" date="1995" rank="subsection">virescens</taxon_name>
    <taxon_name authority="S. Watson" date="1890" rank="species">madrense</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 141. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection virescens;species madrense;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500517</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 30-80 (-100) cm;</text>
      <biological_entity id="o23478" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base often reddish, puberulent.</text>
      <biological_entity id="o23479" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline and basal;</text>
      <biological_entity id="o23480" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 0-8 at anthesis;</text>
      <biological_entity constraint="basal" id="o23481" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 3-11 at anthesis;</text>
      <biological_entity constraint="cauline" id="o23482" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="3" name="quantity" src="d0_s4" to="11" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1-15 (-25) cm.</text>
      <biological_entity id="o23483" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade semicircular to cordate, 2-8 × 2-10 cm, nearly glabrous;</text>
      <biological_entity id="o23484" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s6" to="cordate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-12, width 3-10 mm (basal), 2-6 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o23485" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="12" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 5-75 (-98) -flowered;</text>
      <biological_entity id="o23486" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-75(-98)-flowered" value_original="5-75(-98)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel 1-2.5 (-5) cm, puberulent;</text>
      <biological_entity id="o23487" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 2-4 (-8) mm from flowers, green, lanceolate-linear, 3-5 mm, puberulent.</text>
      <biological_entity id="o23488" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o23489" from="2" from_unit="mm" name="location" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate-linear" value_original="lanceolate-linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o23489" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals dark blue to light blue, puberulent, lateral sepals spreading, 9-15 × 5-7 mm, spurs straight, ascending ca. 45 (-90) °, 10-15 (-19) mm;</text>
      <biological_entity id="o23490" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o23491" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="dark blue" name="coloration" src="d0_s11" to="light blue" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o23492" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23493" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="45(-90)°" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="19" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades elevated, exposing stamens, 4-7 mm, clefts 2-4 mm;</text>
      <biological_entity id="o23494" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o23495" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23496" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o23497" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o23495" id="r3211" name="exposing" negation="false" src="d0_s12" to="o23496" />
    </statement>
    <statement id="d0_s13">
      <text>hairs centered, densest on inner lobes near base of cleft, white.</text>
      <biological_entity id="o23498" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o23499" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="centered" value_original="centered" />
        <character constraint="on inner lobes" constraintid="o23500" is_modifier="false" name="density" src="d0_s13" value="densest" value_original="densest" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="inner" id="o23500" name="lobe" name_original="lobes" src="d0_s13" type="structure" />
      <biological_entity id="o23501" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o23500" id="r3212" name="near" negation="false" src="d0_s13" to="o23501" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 15-21 mm, 3.5-4.5 times longer than wide, nearly glabrous.</text>
      <biological_entity id="o23502" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s14" to="21" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="3.5-4.5" value_original="3.5-4.5" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds: seed-coat cells with surfaces pustulate.</text>
      <biological_entity id="o23503" name="seed" name_original="seeds" src="d0_s15" type="structure" />
      <biological_entity constraint="seed-coat" id="o23504" name="cell" name_original="cells" src="d0_s15" type="structure" />
      <biological_entity id="o23505" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief" src="d0_s15" value="pustulate" value_original="pustulate" />
      </biological_entity>
      <relation from="o23504" id="r3213" name="with" negation="false" src="d0_s15" to="o23505" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous slopes, oak woods or desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous slopes" />
        <character name="habitat" value="oak woods" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, and Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (and Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>38.</number>
  <other_name type="common_name">Sierra Madre larkspur</other_name>
  <other_name type="common_name">Edwards' Plateau larkspur</other_name>
  <discussion>Delphinium madrense hybridizes with D. carolinianum subspp. vimineum and virescens.</discussion>
  
</bio:treatment>