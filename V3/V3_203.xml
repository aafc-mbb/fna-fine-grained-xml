<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="Ewan" date="1936" rank="subsection">Subscaposa</taxon_name>
    <taxon_name authority="A. Gray" date="1887" rank="species">parishii</taxon_name>
    <taxon_name authority="(Wiggins) H. F. Lewis &amp; Epling" date="1954" rank="subspecies">subglobosum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>8: 15. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection subscaposa;species parishii;subspecies subglobosum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500532</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Wiggins" date="unknown" rank="species">subglobosum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Dudley Herb.</publication_title>
      <place_in_publication>1: 99. 1929</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species subglobosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (19-) 30-50 (-78) cm.</text>
      <biological_entity id="o90" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="19" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="78" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o91" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ultimate lobes 7-12, lobes of basal leaves nearly as narrow as lobes of cauline.</text>
      <biological_entity constraint="ultimate" id="o92" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s2" to="12" />
      </biological_entity>
      <biological_entity id="o93" name="lobe" name_original="lobes" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o94" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o95" name="lobe" name_original="lobes" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o96" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o93" id="r13" name="part_of" negation="false" src="d0_s2" to="o94" />
      <relation from="o95" id="r14" name="part_of" negation="false" src="d0_s2" to="o96" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: pedicel 3-20 mm, 8-17 mm apart.</text>
      <biological_entity id="o97" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o98" name="pedicel" name_original="pedicel" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="17" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals dark blue, usually spreading, lateral sepals 9-13 × 5-7 mm, spurs 12-14 mm;</text>
      <biological_entity id="o99" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o100" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark blue" value_original="dark blue" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o101" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s4" to="13" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o102" name="spur" name_original="spurs" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lower petal blades 4-6 mm.</text>
      <biological_entity id="o103" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="petal" id="o104" name="blade" name_original="blades" src="d0_s5" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruits 8-11 mm.</text>
      <biological_entity id="o105" name="fruit" name_original="fruits" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter-mid spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry chaparral and desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry chaparral" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25b.</number>
  <discussion>Delphinium parishii subsp. subglobosum occurs only on the eastern side of peninsular ranges.</discussion>
  <discussion>Delphinium parishii subsp. subglobosum hybridizes with D. parryi subsp. parryi. Likely to be confused only with D. parryi, D. parishii subsp. subglobosum may be differentiated from that species by its lack of arched hairs on stems.</discussion>
  
</bio:treatment>