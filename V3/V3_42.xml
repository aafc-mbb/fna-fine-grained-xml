<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="Ewan" date="1936" rank="subsection">Subscaposa</taxon_name>
    <taxon_name authority="A. Gray" date="1887" rank="species">parryi</taxon_name>
    <taxon_name authority="(H. F. Lewis &amp; Epling) M. J. Warnock" date="1990" rank="subspecies">purpureum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>68: 2. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection subscaposa;species parryi;subspecies purpureum;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500538</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">parishii</taxon_name>
    <taxon_name authority="H. F. Lewis &amp; Epling" date="unknown" rank="subspecies">purpureum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>8: 15. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species parishii;subspecies purpureum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots usually more than 10 cm.</text>
      <biological_entity id="o21396" name="root" name_original="roots" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 30-90 cm.</text>
      <biological_entity id="o21397" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly on proximal 1/3 of stem;</text>
      <biological_entity id="o21398" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o21399" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <biological_entity id="o21400" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o21398" id="r2903" name="on" negation="false" src="d0_s2" to="o21399" />
      <relation from="o21399" id="r2904" name="part_of" negation="false" src="d0_s2" to="o21400" />
    </statement>
    <statement id="d0_s3">
      <text>basal leaves usually present at anthesis;</text>
      <biological_entity constraint="basal" id="o21401" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade with ultimate lobes 3-20, width less than 4 mm.</text>
      <biological_entity id="o21402" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" notes="" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o21403" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="20" />
      </biological_entity>
      <relation from="o21402" id="r2905" name="with" negation="false" src="d0_s4" to="o21403" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: bracteoles 3-6 mm.</text>
      <biological_entity id="o21404" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o21405" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals usually reflexed, lateral sepals 7-11 mm, spurs 10-13 mm;</text>
      <biological_entity id="o21406" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o21407" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21408" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21409" name="spur" name_original="spurs" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lower petal blades 3-5 mm. 2n = 16.</text>
      <biological_entity id="o21410" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="petal" id="o21411" name="blade" name_original="blades" src="d0_s7" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21412" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry chaparral, sage scrub and lower montane woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry chaparral" />
        <character name="habitat" value="sage scrub" />
        <character name="habitat" value="lower montane woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-1600(-2400) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1000" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2400" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23c.</number>
  <other_name type="common_name">Mount Pinos larkspur</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Delphinium parryi subsp. purpureum may be imperiled by human encroachment.</discussion>
  <discussion>This taxon may be confused with Delphinium parishii, but its abundant arched hairs and lack of wider-lobed basal leaves will distinguish D. parryi subsp. purpureum. It hybridizes with D. parishii subsp. pallidum.</discussion>
  
</bio:treatment>