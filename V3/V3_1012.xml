<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="Barton" date="1793" rank="genus">jeffersonia</taxon_name>
    <taxon_name authority="(Linnaeus) Persoon" date="1805" rank="species">diphylla</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>1: 418. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus jeffersonia;species diphylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500714</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Podophyllum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">diphyllum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 505. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Podophyllum;species diphyllum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: petiole slender, erect, 9-25 cm at anthesis, maturing to 18-43 cm.</text>
      <biological_entity id="o6900" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o6901" name="petiole" name_original="petiole" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="at anthesis , maturing to 18-43 cm" from="9" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade 2-foliolate, often with minute apiculation between leaflets;</text>
      <biological_entity id="o6902" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="2-foliolate" value_original="2-foliolate" />
      </biological_entity>
      <biological_entity constraint="between leaflets" constraintid="o6904" id="o6903" name="apiculation" name_original="apiculation" src="d0_s1" type="structure" constraint_original="between  leaflets, ">
        <character is_modifier="true" name="size" src="d0_s1" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o6904" name="leaflet" name_original="leaflets" src="d0_s1" type="structure" />
      <relation from="o6902" id="r962" modifier="often" name="with" negation="false" src="d0_s1" to="o6903" />
    </statement>
    <statement id="d0_s2">
      <text>leaflets 1.2-4 × 0.6-2.5 cm at anthesis, maturing to 6-13 × 3-7 cm, lobes rounded to acute.</text>
      <biological_entity id="o6905" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" constraint="at lobes" constraintid="o6906" from="0.6" from_unit="cm" name="width" src="d0_s2" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6906" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="maturing" value_original="maturing" />
        <character char_type="range_value" from="6" from_unit="cm" is_modifier="true" name="length" src="d0_s2" to="13" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" is_modifier="true" name="width" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scapes 9-33 cm, frequently taller than petioles at anthesis.</text>
      <biological_entity id="o6907" name="scape" name_original="scapes" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s3" to="33" to_unit="cm" />
        <character constraint="than petioles" constraintid="o6908" is_modifier="false" name="height" src="d0_s3" value="frequently taller" value_original="frequently taller" />
      </biological_entity>
      <biological_entity id="o6908" name="petiole" name_original="petioles" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals elliptic to obovate, 7-15 × 4-6 mm;</text>
      <biological_entity id="o6909" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o6910" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals white, elliptic to obovate, 11-22 × 9-12 mm;</text>
      <biological_entity id="o6911" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6912" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s5" to="22" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens 6-12 mm;</text>
      <biological_entity id="o6913" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6914" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 2-3 mm;</text>
      <biological_entity id="o6915" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6916" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 4-9 × 1-1.5 mm;</text>
      <biological_entity id="o6917" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6918" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovaries 6-11 × 3-7 mm.</text>
      <biological_entity id="o6919" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6920" name="ovary" name_original="ovaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits 18-38 × 8-17 mm, leathery, opening transversely, apical quarter resembling lid, becoming reflexed.</text>
      <biological_entity id="o6921" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s10" to="38" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s10" to="17" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="becoming" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o6922" name="lid" name_original="lid" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="transversely" name="position" src="d0_s10" value="apical" value_original="apical" />
      </biological_entity>
      <relation from="o6921" id="r963" name="opening" negation="false" src="d0_s10" to="o6922" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds oblong, 4-7 × 2 mm;</text>
      <biological_entity id="o6923" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="7" to_unit="mm" />
        <character name="width" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>aril laciniate, attached at adaxial side of hilum.</text>
      <biological_entity constraint="hilum" id="o6925" name="side" name_original="side" src="d0_s12" type="structure" constraint_original="hilum adaxial; hilum" />
      <biological_entity id="o6926" name="hilum" name_original="hilum" src="d0_s12" type="structure" />
      <relation from="o6925" id="r964" name="part_of" negation="false" src="d0_s12" to="o6926" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 12.</text>
      <biological_entity id="o6924" name="aril" name_original="aril" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="laciniate" value_original="laciniate" />
        <character constraint="at adaxial side" constraintid="o6925" is_modifier="false" name="fixation" src="d0_s12" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6927" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring; fruiting spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" />
        <character name="fruiting time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich moist woods to semiopen rocky slopes and outcrops, usually over limestone or other calcareous rocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich moist woods" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="other calcareous" />
        <character name="habitat" value="rocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ga., Ill., Ind., Iowa, Ky., Md., Mich., Minn., N.Y., N.C., Ohio, Pa., Tenn., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Twinleaf</other_name>
  <other_name type="common_name">rheumatism-root</other_name>
  <discussion>Plants of Jeffersonia diphylla were used medicinally by Native Americans for treatment of dropsy, gravel and urinary ailments, and for gall and diarrhea, and in poultices for sores and ulcers (D. E. Moermann 1986).</discussion>
  
</bio:treatment>