<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="Linneaus" date="unknown" rank="section">quercus</taxon_name>
    <taxon_name authority="Sargent" date="1895" rank="species">toumeyi</taxon_name>
    <place_of_publication>
      <publication_title>Gard. &amp; Forest</publication_title>
      <place_in_publication>8: 92. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section quercus;species toumeyi</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501091</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Trelease" date="unknown" rank="species">hartmanii</taxon_name>
    <taxon_hierarchy>genus Quercus;species hartmanii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, deciduous or subevergreen.</text>
      <biological_entity id="o16045" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="subevergreen" value_original="subevergreen" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="subevergreen" value_original="subevergreen" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark dark gray to almost black, scaly.</text>
      <biological_entity id="o16047" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark gray to almost" value_original="dark gray to almost" />
        <character is_modifier="false" modifier="almost" name="coloration" src="d0_s1" value="black" value_original="black" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs brownish, 1-2 mm, usually persistently pubescent.</text>
      <biological_entity id="o16048" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="usually persistently" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Buds reddish-brown, ovoid, ca. 1 mm.</text>
      <biological_entity id="o16049" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character name="some_measurement" src="d0_s3" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 2-3.5 mm.</text>
      <biological_entity id="o16050" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16051" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade oblongelliptic or lanceolate, 15-25 (-30) × (6-) 8-12 (-15) mm, base obtuse or cuneate, rarely subcordate, margins strongly cartilaginous, entire, sometimes sparsely mucronate-dentate toward apex, secondary-veins 7-8 on each side, apex acute, sometimes rounded;</text>
      <biological_entity id="o16052" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_width" src="d0_s5" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16053" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o16054" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly" name="pubescence_or_texture" src="d0_s5" value="cartilaginous" value_original="cartilaginous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="toward apex" constraintid="o16055" is_modifier="false" modifier="sometimes sparsely" name="architecture_or_shape" src="d0_s5" value="mucronate-dentate" value_original="mucronate-dentate" />
      </biological_entity>
      <biological_entity id="o16055" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity id="o16056" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o16057" from="7" name="quantity" src="d0_s5" to="8" />
      </biological_entity>
      <biological_entity id="o16057" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o16058" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially dull gray, microscopically pubescent with long, soft, white or yellow hairs concentrated in tufts along midvein and base, adaxially glossy green, sparsely minutely stellate-pubescent or glabrate.</text>
      <biological_entity id="o16059" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="reflectance" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="gray" value_original="gray" />
        <character constraint="with hairs" constraintid="o16060" is_modifier="false" modifier="microscopically" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" notes="" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="sparsely minutely" name="pubescence" src="d0_s6" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o16060" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s6" value="long" value_original="long" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s6" value="soft" value_original="soft" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character constraint="in tufts" constraintid="o16061" is_modifier="false" name="arrangement_or_density" src="d0_s6" value="concentrated" value_original="concentrated" />
      </biological_entity>
      <biological_entity id="o16061" name="tuft" name_original="tufts" src="d0_s6" type="structure" />
      <biological_entity id="o16062" name="midvein" name_original="midvein" src="d0_s6" type="structure" />
      <biological_entity id="o16063" name="base" name_original="base" src="d0_s6" type="structure" />
      <relation from="o16061" id="r2215" name="along" negation="false" src="d0_s6" to="o16062" />
      <relation from="o16061" id="r2216" name="along" negation="false" src="d0_s6" to="o16063" />
    </statement>
    <statement id="d0_s7">
      <text>Acorns solitary or paired, subsessile or on peduncle 2 mm;</text>
      <biological_entity id="o16064" name="acorn" name_original="acorns" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="on peduncle" value_original="on peduncle" />
      </biological_entity>
      <biological_entity id="o16065" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <relation from="o16064" id="r2217" name="on" negation="false" src="d0_s7" to="o16065" />
    </statement>
    <statement id="d0_s8">
      <text>cup cupshaped, 6 mm deep × ca. 8-9 mm wide, enclosing ca. 1/3 nut, scales moderately tuberculate;</text>
      <biological_entity id="o16066" name="cup" name_original="cup" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cup-shaped" value_original="cup-shaped" />
        <character name="width" src="d0_s8" unit="mm" value="6" value_original="6" />
        <character name="width" src="d0_s8" unit="mm" value="×8-9" value_original="×8-9" />
      </biological_entity>
      <biological_entity id="o16067" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o16068" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="moderately" name="relief" src="d0_s8" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <relation from="o16066" id="r2218" name="enclosing" negation="false" src="d0_s8" to="o16067" />
    </statement>
    <statement id="d0_s9">
      <text>nut light-brown, narrowly ovoid or elliptic, 8-15 × 6-8 mm.</text>
      <biological_entity id="o16069" name="nut" name_original="nut" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cotyledons distinct.</text>
      <biological_entity id="o16070" name="cotyledon" name_original="cotyledons" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, oak woodlands, and open chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="open chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua and Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua and Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>71.</number>
  <other_name type="common_name">Toumey oak</other_name>
  <discussion>Quercus toumeyi, particularly the more spinescent-leaved form, is often confused with Q. turbinella. The latter species has acorns on peduncles greater than 10 mm, and more or less evenly distributed minute, flat, stellate trichomes on the abaxial leaf surface, in contrast to the subsessile acorns and longer straight hairs along the midvein of the abaxial leaf surface in Q. toumeyi.</discussion>
  
</bio:treatment>