<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">nymphaeaceae</taxon_name>
    <taxon_name authority="Smith in J. Sibthorp &amp; J. E. Smith" date="1809" rank="genus">nuphar</taxon_name>
    <taxon_name authority="(Persoon) Fernald" date="1917" rank="species">microphylla</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>19: 111. 1917 (as microphyllum)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nymphaeaceae;genus nuphar;species microphylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500815</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nymphaea</taxon_name>
    <taxon_name authority="Persoon" date="unknown" rank="species">microphylla</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>2: 63. 1807</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Nymphaea;species microphylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nuphar</taxon_name>
    <taxon_name authority="(Michaux) W. T. Aiton" date="unknown" rank="species">kalmiana</taxon_name>
    <taxon_hierarchy>genus Nuphar;species kalmiana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nuphar</taxon_name>
    <taxon_name authority="(Willdenow) Smith" date="unknown" rank="species">minima</taxon_name>
    <taxon_hierarchy>genus Nuphar;species minima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes 1-2 cm diam.</text>
      <biological_entity id="o22616" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly floating, occasionally submersed;</text>
      <biological_entity id="o22617" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
        <character is_modifier="false" modifier="occasionally" name="location" src="d0_s1" value="submersed" value_original="submersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole flattened to filiform.</text>
      <biological_entity id="o22618" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="flattened" name="shape" src="d0_s2" to="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade abaxially often purple, adaxially green to greenish purple, broadly elliptic to ovate, 3.5-10 (-13) × 3.5-7.5 (-8.5) cm, 1-1.5 times as long as wide, sinus 2/3 or more length of midrib, lobes divergent and forming V-shaped angle;</text>
      <biological_entity id="o22619" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="purple" value_original="purple" />
        <character char_type="range_value" from="adaxially green" name="coloration" src="d0_s3" to="greenish purple" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="13" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s3" to="7.5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="1-1.5" value_original="1-1.5" />
      </biological_entity>
      <biological_entity id="o22620" name="sinus" name_original="sinus" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o22621" name="midrib" name_original="midrib" src="d0_s3" type="structure" />
      <biological_entity id="o22622" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o22623" name="angle" name_original="angle" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <relation from="o22622" id="r3088" name="forming" negation="false" src="d0_s3" to="o22623" />
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially glabrous to densely pubescent.</text>
      <biological_entity id="o22624" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="abaxially glabrous" name="pubescence" src="d0_s4" to="densely pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 1-2 cm diam.;</text>
      <biological_entity id="o22625" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals 5 (-10), abaxially green to adaxially yellow toward base;</text>
      <biological_entity id="o22626" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="10" />
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" constraint="toward base" constraintid="o22627" from="abaxially green" name="coloration" src="d0_s6" to="adaxially yellow" />
      </biological_entity>
      <biological_entity id="o22627" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>petals broadly spatulate and thin, or notched and thickened;</text>
      <biological_entity id="o22628" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character is_modifier="false" name="shape" src="d0_s7" value="notched" value_original="notched" />
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 1-3 mm, shorter than filaments.</text>
      <biological_entity id="o22629" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character constraint="than filaments" constraintid="o22630" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o22630" name="filament" name_original="filaments" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruit yellow, green, brown, or rarely purple, mostly globose-ovoid, occasionally flask-shaped, 1-2 cm, smooth basally, faintly ribbed toward apex, deeply constricted below stigmatic disk, constriction 1.5-5 mm diam.;</text>
      <biological_entity id="o22631" name="fruit" name_original="fruit" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s9" value="globose-ovoid" value_original="globose-ovoid" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s9" value="flask--shaped" value_original="flask--shaped" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character constraint="toward apex" constraintid="o22632" is_modifier="false" modifier="faintly" name="architecture_or_shape" src="d0_s9" value="ribbed" value_original="ribbed" />
        <character constraint="below stigmatic, disk" constraintid="o22633, o22634" is_modifier="false" modifier="deeply" name="size" notes="" src="d0_s9" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o22632" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o22633" name="stigmatic" name_original="stigmatic" src="d0_s9" type="structure" />
      <biological_entity id="o22634" name="disk" name_original="disk" src="d0_s9" type="structure" />
      <biological_entity id="o22635" name="constriction" name_original="constriction" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmatic disk red, 2.5-7 mm diam., with 6-10 deep crenations;</text>
      <biological_entity constraint="stigmatic" id="o22636" name="disk" name_original="disk" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22637" name="crenation" name_original="crenations" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s10" to="10" />
        <character is_modifier="true" name="depth" src="d0_s10" value="deep" value_original="deep" />
      </biological_entity>
      <relation from="o22636" id="r3089" name="with" negation="false" src="d0_s10" to="o22637" />
    </statement>
    <statement id="d0_s11">
      <text>stigmatic rays 6-11, linear, terminating 0-0.2 mm from margin of disk.</text>
      <biological_entity constraint="stigmatic" id="o22638" name="ray" name_original="rays" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="11" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds ca. 3 mm. 2n = 34.</text>
      <biological_entity id="o22639" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22640" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ponds, lakes, sluggish streams, sloughs, ditches, and occasionally tidal waters</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="sluggish streams" />
        <character name="habitat" value="sloughs" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="tidal waters" modifier="occasionally" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., N.S., Ont., Que.; Conn., Maine, Mass., Mich., Minn., N.H., N.J., N.Y., Pa., Vt., Wis.; Europe; n Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Petit nénuphar jaune</other_name>
  <discussion>Intermediates between Nuphar microphylla and N. variegata, probably of hybrid origin, are treated as N. rubrodisca. A form with ten sepals (Nuphar microphyllum forma multisepalum Lakela) occurs in northeastern Minnesota. Recent observations of the Eurasian N. pumila (Timm) de Candolle by C. B. Hellquist in Siberia suggest that Beal's lumping of N microphylla under N. lutea subsp. pumila (Timm) E. O. Beal should be further studied.</discussion>
  
</bio:treatment>