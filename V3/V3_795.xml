<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John W. Thieret, John T. Kartesz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Decaisne" date="unknown" rank="family">LARDIZABALACEAE</taxon_name>
    <taxon_hierarchy>family LARDIZABALACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10477</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines [small trees or shrubs], woody, evergreen or deciduous.</text>
      <biological_entity id="o22401" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems without spines.</text>
      <biological_entity id="o22402" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o22403" name="spine" name_original="spines" src="d0_s1" type="structure" />
      <relation from="o22402" id="r3063" name="without" negation="false" src="d0_s1" to="o22403" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o22404" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules absent [except in Lardizabala];</text>
      <biological_entity id="o22405" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles present.</text>
      <biological_entity id="o22406" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade palmately compound [pinnately compound or 1-3-ternate].</text>
      <biological_entity id="o22407" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s5" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, racemes, occasionally solitary flowers, flowers pedicellate.</text>
      <biological_entity id="o22408" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o22409" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
      <biological_entity id="o22410" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="occasionally" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o22411" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, staminate and pistillate on same plants in same or different inflorescences [staminate and pistillate on different plants or flowers bisexual, staminate, and pistillate], showy, 3-merous;</text>
      <biological_entity id="o22412" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o22413" is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s7" value="showy" value_original="showy" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-merous" value_original="3-merous" />
      </biological_entity>
      <biological_entity id="o22413" name="plant" name_original="plants" src="d0_s7" type="structure" />
      <biological_entity id="o22414" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <relation from="o22413" id="r3064" name="in" negation="false" src="d0_s7" to="o22414" />
    </statement>
    <statement id="d0_s8">
      <text>sepaloid bracteoles absent;</text>
      <biological_entity id="o22415" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="sepaloid" value_original="sepaloid" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 3-6, distinct, usually petaloid, not spurred;</text>
      <biological_entity id="o22416" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="6" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s9" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s9" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals absent [6; nectary present].</text>
      <biological_entity id="o22417" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: stamens 6 (-8), distinct [proximally connate];</text>
      <biological_entity id="o22418" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o22419" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="8" />
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers with 2 pollen-sacs, extrorse, dehiscing longitudinally;</text>
      <biological_entity id="o22420" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o22421" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="dehiscence_or_orientation" notes="" src="d0_s12" value="extrorse" value_original="extrorse" />
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s12" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o22422" name="pollen-sac" name_original="pollen-sacs" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <relation from="o22421" id="r3065" name="with" negation="false" src="d0_s12" to="o22422" />
    </statement>
    <statement id="d0_s13">
      <text>pistillodes sometimes present.</text>
      <biological_entity id="o22423" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o22424" name="pistillode" name_original="pistillodes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: staminodes sometimes present;</text>
      <biological_entity id="o22425" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o22426" name="staminode" name_original="staminodes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistils 3-15 in 1-5 whorls of 3 each, 1-carpellate, 1-locular;</text>
      <biological_entity id="o22427" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o22428" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o22429" from="3" name="quantity" src="d0_s15" to="15" />
      </biological_entity>
      <biological_entity id="o22429" name="whorl" name_original="whorls" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s15" to="5" />
      </biological_entity>
      <biological_entity id="o22430" name="1-locular" name_original="1-locular" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="3" value_original="3" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="1-carpellate" value_original="1-carpellate" />
      </biological_entity>
      <relation from="o22429" id="r3066" name="part_of" negation="false" src="d0_s15" to="o22430" />
    </statement>
    <statement id="d0_s16">
      <text>placentation submarginal or laminar;</text>
      <biological_entity id="o22431" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="position" src="d0_s16" value="placentation" value_original="placentation" />
        <character is_modifier="false" name="position" src="d0_s16" value="submarginal" value_original="submarginal" />
        <character is_modifier="false" name="position" src="d0_s16" value="laminar" value_original="laminar" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 100-several hundred [1-20];</text>
      <biological_entity id="o22432" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o22433" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" from="100" is_modifier="false" name="quantity" src="d0_s17" to="several" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s17" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma 1, sessile to nearly sessile.</text>
      <biological_entity id="o22434" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o22435" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s18" to="nearly sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits aggregates of berries or of fleshy follicles (sometimes only 1 pistil maturing).</text>
      <biological_entity constraint="fruits" id="o22436" name="aggregate" name_original="aggregates" src="d0_s19" type="structure" />
      <biological_entity id="o22437" name="follicle" name_original="follicles" src="d0_s19" type="structure">
        <character is_modifier="true" name="texture" src="d0_s19" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o22436" id="r3067" name="of berries or of" negation="false" src="d0_s19" to="o22437" />
    </statement>
    <statement id="d0_s20">
      <text>Seeds [1-] 100-several hundred, never stalked;</text>
      <biological_entity id="o22438" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s20" value="[1-]100-several" value_original="[1-]100-several" />
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s20" value="stalked" value_original="stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>aril absent;</text>
      <biological_entity id="o22439" name="aril" name_original="aril" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>endosperm abundant;</text>
      <biological_entity id="o22440" name="endosperm" name_original="endosperm" src="d0_s22" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s22" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>embryo small, straight.</text>
      <biological_entity id="o22441" name="embryo" name_original="embryo" src="d0_s23" type="structure">
        <character is_modifier="false" name="size" src="d0_s23" value="small" value_original="small" />
        <character is_modifier="false" name="course" src="d0_s23" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Lardizabala Family</other_name>
  <discussion>Genera ca. 8, species ca. 30 (1 genus, 1 species in the flora).</discussion>
  <discussion>Lardizabalaceae have a disjunct distribution. Two of the genera–Decaisnea and Lardizabala–are found in Chile; the others are Asiatic (Himalayas to Vietnam, Japan, and Korea).</discussion>
  <references>
    <reference>Ernst, W. R. 1964. The genera of Berberidaceae, Lardizabalaceae, and Menispermaceae in the southeastern United States. J. Arnold Arbor. 45: 1-35.</reference>
    <reference>Matthews, V. A. 1989. Lardizabalaceae. In: S. M. Walters et al., eds. 1984+. The European Garden Flora. 3+ vols. Cambridge etc. Vol. 3, pp. 396-398.</reference>
    <reference>Prantl, K. 1888. Lardizabalaceae. In: H. G. A. Engler and K. Prantl, eds. 1887-1915. Die natürlichen Pflanzenfamilien.... 254 fasc. Leipzig. Fasc. 19[III,2], pp. 67-70.</reference>
    <reference>Spongberg, S. A. and I. H. Burch. 1979. Lardizabalaceae hardy in temperate North America. J. Arnold Arbor. 60: 302-315.</reference>
  </references>
  
</bio:treatment>