<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">270</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(Greene) B. Boivin" date="1944" rank="section">Leucocoma</taxon_name>
    <taxon_name authority="H. E. Ahles" date="1959" rank="species">cooleyi</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>11: 68. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section leucocoma;species cooleyi;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501262</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect to reclining, slender, 60-200 cm.</text>
      <biological_entity id="o14840" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="reclining" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: proximal cauline petiolate, distal cauline sessile to nearly sessile;</text>
      <biological_entity id="o14841" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal cauline" id="o14842" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal cauline" id="o14843" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s1" to="nearly sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles and rachises glabrous, neither pubescent nor glandular.</text>
      <biological_entity id="o14844" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14845" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o14846" name="rachis" name_original="rachises" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade: proximal cauline mostly 2×-ternately compound, distal cauline usually ternately compound;</text>
      <biological_entity id="o14847" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly 2×-ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="usually ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets linear to narrowly lanceolate or oblanceolate, apically occasionally 2-3-lobed, 12-68 × 1-12 mm, length (2.6-) 4-26 times width, membranous to leathery, margins sometimes revolute, lobe margins entire;</text>
      <biological_entity id="o14848" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure" />
      <biological_entity id="o14849" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate or oblanceolate" />
        <character is_modifier="false" modifier="apically occasionally" name="shape" src="d0_s4" value="2-3-lobed" value_original="2-3-lobed" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s4" to="68" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="(2.6-)4-26" value_original="(2.6-)4-26" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s4" to="leathery" />
      </biological_entity>
      <biological_entity id="o14850" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o14851" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>surfaces abaxially glabrous.</text>
      <biological_entity id="o14852" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure" />
      <biological_entity id="o14853" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences racemes to panicles, elongate, few flowered;</text>
      <biological_entity constraint="inflorescences" id="o14854" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
      </biological_entity>
      <biological_entity id="o14855" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <relation from="o14854" id="r2068" name="to" negation="false" src="d0_s6" to="o14855" />
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels neither pubescent nor glandular.</text>
      <biological_entity id="o14856" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o14857" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers usually unisexual, staminate and pistillate on different plants;</text>
      <biological_entity id="o14858" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o14859" is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o14859" name="plant" name_original="plants" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 4-5, white to yellowish in staminate flowers, greenish in pistillate flowers, obovate, 1.5 mm;</text>
      <biological_entity id="o14860" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
        <character char_type="range_value" constraint="in flowers" constraintid="o14861" from="white" name="coloration" src="d0_s9" to="yellowish" />
        <character constraint="in flowers" constraintid="o14862" is_modifier="false" name="coloration" notes="" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="obovate" value_original="obovate" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o14861" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14862" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments white to purple, 2.5-6 mm;</text>
      <biological_entity id="o14863" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="purple" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 0.9-2.5 mm.</text>
      <biological_entity id="o14864" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes 5-6, sessile or nearly sessile;</text>
      <biological_entity id="o14865" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stipe 0-0.4 mm;</text>
      <biological_entity id="o14866" name="stipe" name_original="stipe" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>body ellipsoid, 4.5-6 mm, prominently veined, glabrous;</text>
      <biological_entity id="o14867" name="body" name_original="body" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s14" value="veined" value_original="veined" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak 1.3-2.4 mm. 2n = 210.</text>
      <biological_entity id="o14868" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s15" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14869" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="210" value_original="210" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (mid Jun-mid Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="mid Jun-mid Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Boggy, savannahlike borders of low woodlands, and disturbed areas such as roadside ditches, clearings, and edges of frequently burned savannahs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="savannahlike borders" modifier="boggy" constraint="of low woodlands , and disturbed areas such as roadside ditches , clearings , and edges of frequently burned savannahs" />
        <character name="habitat" value="low woodlands" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadside ditches" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="edges" constraint="of frequently burned savannahs" />
        <character name="habitat" value="burned savannahs" modifier="frequently" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Cooley's meadow-rue</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., N.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Thalictrum cooleyi occurs commonly on Grifton soil and is associated with some sort of disturbance, including clearings, edges of frequently burned savannahs, roadsides, and powerline rights-of-way that are maintained by fire or mowing. Silvicultural and agricultural practices and their associated suppression of fire have seriously affected populations of T. cooleyi. Furthermore, fruit production appears to be quite low in the species (S. W. Leonard 1987).</discussion>
  <discussion>Leaves of Thalictrum cooleyi have fewer leaflets than other species of Thalictrum sect. Leucocoma.</discussion>
  
</bio:treatment>