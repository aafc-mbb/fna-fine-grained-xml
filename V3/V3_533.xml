<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">caltha</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">palustris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 558. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus caltha;species palustris</taxon_hierarchy>
    <other_info_on_name type="fna_id">200007551</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Caltha</taxon_name>
    <taxon_name authority="R.Brown" date="unknown" rank="species">arctica</taxon_name>
    <taxon_hierarchy>genus Caltha;species arctica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Caltha</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">asarifolia</taxon_name>
    <taxon_hierarchy>genus Caltha;species asarifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Caltha</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palustris</taxon_name>
    <taxon_name authority="(R. Brown) Hultén" date="unknown" rank="subspecies">arctica</taxon_name>
    <taxon_hierarchy>genus Caltha;species palustris;subspecies arctica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Caltha</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palustris</taxon_name>
    <taxon_name authority="(de Candolle) Hultén" date="unknown" rank="subspecies">asarifolia</taxon_name>
    <taxon_hierarchy>genus Caltha;species palustris;subspecies asarifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Caltha</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palustris</taxon_name>
    <taxon_name authority="(R. Brown) Huth" date="unknown" rank="variety">arctica</taxon_name>
    <taxon_hierarchy>genus Caltha;species palustris;variety arctica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Caltha</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palustris</taxon_name>
    <taxon_name authority="(de Candolle) Huth" date="unknown" rank="variety">asarifolia</taxon_name>
    <taxon_hierarchy>genus Caltha;species palustris;variety asarifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Caltha</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palustris</taxon_name>
    <taxon_name authority="(Pursh) Torrey &amp; A. Gray" date="unknown" rank="variety">flabellifolia</taxon_name>
    <taxon_hierarchy>genus Caltha;species palustris;variety flabellifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems leafy, permanently erect, or sprawling with age and producing roots and shoots at nodes.</text>
      <biological_entity id="o25804" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="permanently" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" modifier="permanently" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character constraint="with age" constraintid="o25805" is_modifier="false" name="orientation" src="d0_s0" value="sprawling" value_original="sprawling" />
      </biological_entity>
      <biological_entity id="o25805" name="age" name_original="age" src="d0_s0" type="structure" />
      <biological_entity id="o25806" name="root" name_original="roots" src="d0_s0" type="structure" />
      <biological_entity id="o25807" name="shoot" name_original="shoots" src="d0_s0" type="structure" />
      <biological_entity id="o25808" name="node" name_original="nodes" src="d0_s0" type="structure" />
      <relation from="o25804" id="r3497" name="producing" negation="false" src="d0_s0" to="o25806" />
      <relation from="o25804" id="r3498" name="producing" negation="false" src="d0_s0" to="o25807" />
      <relation from="o25804" id="r3499" name="at" negation="false" src="d0_s0" to="o25808" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: blade rounded to ovate, reniform, or cordate, largest (0.5-) 2-12.5 × (1-) 2-19 cm, margins entire or crenate to dentate.</text>
      <biological_entity constraint="basal" id="o25809" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o25810" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="ovate reniform or cordate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="ovate reniform or cordate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="ovate reniform or cordate" />
        <character is_modifier="false" name="size" src="d0_s1" value="largest" value_original="largest" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="12.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_width" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s1" to="19" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25811" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s1" to="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 1-7-flowered.</text>
      <biological_entity id="o25812" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-7-flowered" value_original="1-7-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 10-45 mm diam.;</text>
      <biological_entity id="o25813" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s3" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals yellow or orange, (6-) 10-25 mm.</text>
      <biological_entity id="o25814" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="orange" value_original="orange" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Follicles 5-15 (-25), spreading, sessile, ellipsoid;</text>
      <biological_entity id="o25815" name="follicle" name_original="follicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="25" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="15" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bodies 8-15 × 3-4.5 mm;</text>
      <biological_entity id="o25816" name="body" name_original="bodies" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style and stigma straight or curved, 0.5-2 mm.</text>
      <biological_entity id="o25817" name="style" name_original="style" src="d0_s7" type="structure">
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25818" name="stigma" name_original="stigma" src="d0_s7" type="structure">
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds elliptic, 1.5-2.5 mm. 2n=32, 56, 60.</text>
      <biological_entity id="o25819" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25820" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="32" value_original="32" />
        <character name="quantity" src="d0_s8" value="56" value_original="56" />
        <character name="quantity" src="d0_s8" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Apr–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshes, fens, ditches, wet woods and swamps, thriving best in open or only partly shaded sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshes" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="wet woods" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="open" modifier="thriving best" />
        <character name="habitat" value="shaded sites" modifier="only partly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Conn., Del., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Mo., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Tenn., Vt., Va., Wash., W.Va., Wis.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Cowslip</other_name>
  <other_name type="common_name">cowflock</other_name>
  <other_name type="common_name">kingcup</other_name>
  <other_name type="common_name">buttercup</other_name>
  <other_name type="common_name">populage des marais</other_name>
  <other_name type="common_name">soucis d'eau</other_name>
  <discussion>Caltha palustris has been divided into different taxa, although plants have been most commonly assigned to two varieties in North America. Typical C. palustris var. palustris is characterized by permanently erect, stout stems that do not produce roots and shoots at the nodes after anthesis. The basal leaves are broadly cordate to reniform with coarsely crenate-dentate margins and overlapping basal lobes. Generally more than three flowers occur on a stem. In contrast, C. palustris var. flabellifolia [= var. arctica, var. radicans (T. F. Forster) Beck] is characterized by stems that sprawl with age and produce roots and shoots at the nodes after anthesis. The basal leaves are ± reniform with denticulate margins, and the basal lobes are widely divergent and do not overlap. Often fewer than three flowers occur on a stem. Caltha palustris var. flabellifolia is distributed locally throughout the range of C. palustris var. palustris; it often grows in places with more extreme environmental conditions, such as shorelines, tidal areas, swiftly running streams and rivers, and areas with an arctic climate. Many arctic specimens can be assigned to this variety.</discussion>
  <discussion>While Caltha palustris var. palustris and var. flabellifolia are distinctive in their extremes, they appear to represent elements along a morphologic continuum rather than recognizable taxonomic entities. For example, P.G. Smit (1973) found plants from Point Barrow, Alaska, to be dwarfed, few flowered, and prostrate, while specimens from southern Alaska were robust, many flowered, and erect. Between these two extremes a complete series of intermediates occurs. Based on that evidence, and considering the phenotypic plasticity known to exist in this species, the various specific and infraspecific segregates of C. palustris in North America are not recognized.</discussion>
  <discussion>Native Americans used various preparations of the roots of Caltha palustris medicinally to treat colds and sores, as an aid in childbirth and to induce vomiting, and as a protection against love charms; infusions of leaves were taken for constipation (D. E. Moerman 1986).</discussion>
  <references>
    <reference>Smit, P. G. 1967. Taxonomical and ecological studies in Caltha palustris L. (preliminary report). Proc. Kon. Ned. Akad. Wetensch. C. 70: 500-510.</reference>
    <reference>Smit, P. G. 1968. Taxonomical and ecological studies in Caltha palustris L. II. Proc. Kon. Ned. Akad. Wetensch. C. 71: 280-292.</reference>
    <reference>Woodell, S. R. J. and M. Kootin-Sanwu. 1971. Intraspecific variation in Caltha palustris. New Phytol. 70: 173-186.</reference>
  </references>
  
</bio:treatment>