<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gwynn W. Ramsey</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Wernischeck" date="1763" rank="genus">CIMICIFUGA</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Pl.,</publication_title>
      <place_in_publication>298. 1763</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus CIMICIFUGA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin cimex, bug, and fugare, to drive away</other_info_on_name>
    <other_info_on_name type="fna_id">107092</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from hard, knotted, long-lived rhizomes.</text>
      <biological_entity id="o24105" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o24106" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="hard" value_original="hard" />
        <character is_modifier="true" name="shape" src="d0_s0" value="knotted" value_original="knotted" />
        <character is_modifier="true" name="duration" src="d0_s0" value="long-lived" value_original="long-lived" />
      </biological_entity>
      <relation from="o24105" id="r3294" name="from" negation="false" src="d0_s0" to="o24106" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, compound, petiolate with basal wings clasping stem;</text>
      <biological_entity id="o24107" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="compound" value_original="compound" />
        <character constraint="with basal wings" constraintid="o24108" is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o24108" name="wing" name_original="wings" src="d0_s1" type="structure" />
      <biological_entity id="o24109" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s1" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves alternate.</text>
      <biological_entity constraint="cauline" id="o24110" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade 1-3-ternately compound;</text>
      <biological_entity id="o24111" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="1-3-ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets ovatelanceolate to broadly obovate or orbiculate, 2-5-lobed, lobe margins toothed or shallow to deeply incised.</text>
      <biological_entity id="o24112" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s4" to="broadly obovate or orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-5-lobed" value_original="2-5-lobed" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o24113" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="depth" src="d0_s4" value="shallow" value_original="shallow" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, many-flowered panicles of racemelike branches [spikes in Asian spp.], 7-60 cm;</text>
      <biological_entity id="o24114" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o24115" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="many-flowered" value_original="many-flowered" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s5" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24116" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="racemelike" value_original="racemelike" />
      </biological_entity>
      <relation from="o24115" id="r3295" name="part_of" negation="false" src="d0_s5" to="o24116" />
    </statement>
    <statement id="d0_s6">
      <text>bracts 1 or 3, alternate, subtending pedicel (pedicels bracteolate in C. americana), not forming involucre.</text>
      <biological_entity id="o24117" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o24118" name="pedicel" name_original="pedicel" src="d0_s6" type="structure" />
      <biological_entity id="o24119" name="involucre" name_original="involucre" src="d0_s6" type="structure" />
      <relation from="o24117" id="r3296" name="subtending" negation="false" src="d0_s6" to="o24118" />
      <relation from="o24117" id="r3297" name="forming" negation="true" src="d0_s6" to="o24119" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual [unisexual], radially symmetric;</text>
      <biological_entity id="o24120" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals not persistent in fruit, (2-) 4-5 (-6), greenish white or cream to greenish yellow, sometimes pinkish or tinged with red, plane or ± concave, ovate to obovate, 3-6 mm;</text>
      <biological_entity id="o24121" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o24122" is_modifier="false" modifier="not" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="cream" name="coloration" notes="" src="d0_s8" to="greenish yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="tinged with red" value_original="tinged with red" />
        <character char_type="range_value" from="more or less concave ovate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="more or less concave ovate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24122" name="fruit" name_original="fruit" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s8" to="4" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 0-8, distinct, white or yellowish, plane, apex 2-cleft [entire], sometimes clawed, 3-6 mm;</text>
      <biological_entity id="o24123" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="8" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o24124" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="2-cleft" value_original="2-cleft" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s9" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nectariferous area sometimes present;</text>
      <biological_entity constraint="nectariferous" id="o24125" name="area" name_original="area" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 20-110;</text>
      <biological_entity id="o24126" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s11" to="110" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments filiform [flattened];</text>
      <biological_entity id="o24127" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminodes absent between stamens and pistils;</text>
      <biological_entity id="o24128" name="staminode" name_original="staminodes" src="d0_s13" type="structure">
        <character constraint="between stamens, pistils" constraintid="o24129, o24130" is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o24129" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o24130" name="pistil" name_original="pistils" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pistils 1-8, simple;</text>
      <biological_entity id="o24131" name="pistil" name_original="pistils" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s14" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovules 4-15 per pistil;</text>
      <biological_entity id="o24132" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per pistil" constraintid="o24133" from="4" name="quantity" src="d0_s15" to="15" />
      </biological_entity>
      <biological_entity id="o24133" name="pistil" name_original="pistil" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style present.</text>
      <biological_entity id="o24134" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits follicles, usually aggregate, sessile or stipitate, ovoid to obovoid, weakly to strongly compressed, sides not prominently veined;</text>
      <biological_entity constraint="fruits" id="o24135" name="follicle" name_original="follicles" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s17" value="aggregate" value_original="aggregate" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s17" to="obovoid" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s17" to="obovoid" />
      </biological_entity>
      <biological_entity id="o24136" name="side" name_original="sides" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not prominently" name="architecture" src="d0_s17" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>beak terminal, straight or hooked at tip, 0.5-2.5 mm.</text>
      <biological_entity id="o24137" name="beak" name_original="beak" src="d0_s18" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s18" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="course" src="d0_s18" value="straight" value_original="straight" />
        <character constraint="at tip" constraintid="o24138" is_modifier="false" name="shape" src="d0_s18" value="hooked" value_original="hooked" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s18" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24138" name="tip" name_original="tip" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>Seeds pale-brown to reddish or purplish brown, angled or laterally compressed, hemispheric, lenticular, or cylindric, smooth, slightly ridged, verrucose, or densely scaly.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = 8.</text>
      <biological_entity id="o24139" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="pale-brown" name="coloration" src="d0_s19" to="reddish or purplish brown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="angled" value_original="angled" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s19" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s19" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s19" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="angled" value_original="angled" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s19" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s19" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s19" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="angled" value_original="angled" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s19" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s19" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s19" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="angled" value_original="angled" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s19" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s19" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s19" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s19" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="relief" src="d0_s19" value="verrucose" value_original="verrucose" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence" src="d0_s19" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity constraint="x" id="o24140" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America and Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America and Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Bugbane</other_name>
  <other_name type="common_name">snakeroot</other_name>
  <other_name type="common_name">cohosh</other_name>
  <discussion>Species 12 (6 in the flora).</discussion>
  <discussion>Cimicifuga may be divided into two natural groups: those with seeds scaly and those with seeds lacking scales or nearly so. Cimicifuga racemosa and C. elata of North America, with scaleless seeds, are most closely related to C. biternata (Siebold &amp; Zuccarini) Miquel and C. japonica (Thunberg) Sprengel of Asia.</discussion>
  <discussion>Four or five species of Cimicifuga are cultivated as ornamentals, and at least five named cultivars have been developed.</discussion>
  <references>
    <reference>Compton, J. 1992. Cimicifuga L.: Ranunculaceae. Plantsman 14(2): 99-115.</reference>
    <reference>Ramsey, G. W. 1965. A Biosystematic Study of the Genus Cimicifuga (Ranunculaceae). Ph.D. thesis. University of Tennessee.</reference>
    <reference>Ramsey, G. W. 1987. Morphological considerations in North American Cimicifuga. Castanea 52: 129-141.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seeds without scales or scales very short; stigma 0.5 mm wide.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seeds scaly; stigma minute, 0.2–0.3 mm wide.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Follicles ovoid; seeds hemispheric, smooth or ±rough-ridged, brown; pedicel subtended by 1 bract; base of terminal leaflet with 3 prominent veins; petals (1–)4(–8), oblong; e North America.</description>
      <determination>1 Cimicifuga racemosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Follicles oblong; seeds lenticular, usually verrucose, rarely with very short scales, reddish to purplish brown; pedicel subtended by 3 bracts; base of terminal leaflet with 5–7 prominent veins; petals absent; w North America.</description>
      <determination>2 Cimicifuga elata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pistils sessile, 1–2(–4 in C. arizonica); seeds lenticular, covering of scales giving cylindric appearance.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pistils short-stipitate, (2–)3–8; seeds lenticular, scales not causing cylindric appearance.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals absent; filaments 3–5 mm; pistils 1(–2), sparsely glandular; style short, straight or slightly recurved; terminal leaflet deeply cordate with 5–9 prominent veins at base; e North America.</description>
      <determination>3 Cimicifuga rubifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals (0–)2; filaments 5–8 mm; pistils 1–3(–4), glandular; style long, hooked; terminal leaflet somewhat cordate with 3 prominent veins at base; w North America.</description>
      <determination>4 Cimicifuga arizonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Pistils glabrous; pedicel bracteolate, granular; petiole glabrous; seeds ca. 3.5 mm, covered with broad, lacerate scales; stamens 40–70; filaments 6–10 mm; e North America.</description>
      <determination>5 Cimicifuga americana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Pistils densely pubescent; pedicel not bracteolate; petiole densely pubescent; seeds ca. 2.5–3 mm, loosely covered with narrow, lacerate scales; stamens 20–25; filaments 4–5 mm; w North America.</description>
      <determination>6 Cimicifuga laciniata</determination>
    </key_statement>
  </key>
</bio:treatment>