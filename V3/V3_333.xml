<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="Ewan" date="1936" rank="subsection">Subscaposa</taxon_name>
    <taxon_name authority="Ewan" date="1945" rank="species">gypsophilum</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Colorado Stud., Ser. D, Phys. Sci.</publication_title>
      <place_in_publication>2: 189. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection subscaposa;species gypsophilum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500502</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (30-) 60-100 (-150) cm;</text>
      <biological_entity id="o25988" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base usually reddish, glabrous, glaucous.</text>
      <biological_entity id="o25989" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o25990" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25991" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves absent at anthesis;</text>
      <biological_entity constraint="basal" id="o25992" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 3-7 at anthesis;</text>
      <biological_entity constraint="cauline" id="o25993" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="3" name="quantity" src="d0_s4" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 2-10 cm.</text>
      <biological_entity id="o25994" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round to pentagonal, 1.5-6 × 2-12 cm, nearly glabrous;</text>
      <biological_entity id="o25995" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s6" to="pentagonal" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-12, width 3-24 mm (basal), 1-8 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o25996" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="12" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="24" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 15-30 (-64) -flowered, cylindric;</text>
      <biological_entity id="o25997" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="15-30(-64)-flowered" value_original="15-30(-64)-flowered" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel spreading, (0.5-) 1.5-3.5 cm, glabrous;</text>
      <biological_entity id="o25998" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s9" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 2-6 mm from flowers, green, linear, 2-5 mm, glabrous.</text>
      <biological_entity id="o25999" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o26000" from="2" from_unit="mm" name="location" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26000" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals rarely reflexed, white to pink, nearly glabrous, lateral sepals spreading, 7-19 × 3-10 mm, spurs straight to upcurved, ascending 30-45° above horizontal, 7-15 mm;</text>
      <biological_entity id="o26001" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26002" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o26003" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s11" to="19" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26004" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="upcurved" value_original="upcurved" />
        <character constraint="above horizontal" is_modifier="false" modifier="30-45°" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades elevated, exposing stamens, 3-8 mm, clefts 1-4 mm;</text>
      <biological_entity id="o26005" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o26006" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26007" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o26008" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o26006" id="r3525" name="exposing" negation="false" src="d0_s12" to="o26007" />
    </statement>
    <statement id="d0_s13">
      <text>hairs centered near base of cleft, ± evenly distributed, white.</text>
      <biological_entity id="o26009" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o26010" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less evenly" name="arrangement" src="d0_s13" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o26011" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o26010" id="r3526" name="centered near" negation="false" src="d0_s13" to="o26011" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 9-18 mm, 2.5-3.2 times longer than wide, puberulent.</text>
      <biological_entity id="o26012" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="18" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="2.5-3.2" value_original="2.5-3.2" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds: seed-coat cells brick-shaped, cell margins undulate, surfaces roughened.</text>
      <biological_entity id="o26013" name="seed" name_original="seeds" src="d0_s15" type="structure" />
      <biological_entity constraint="seed-coat" id="o26014" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="brick--shaped" value_original="brick--shaped" />
      </biological_entity>
      <biological_entity constraint="cell" id="o26015" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o26016" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief_or_texture" src="d0_s15" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Delphinium gypsophilum is sometimes confused with D. hesperium subsp. pallescens, D. recurvatum, and the white-flowered phases of D. hansenii subsp. kernense. The echinate seeds and long-haired petioles of D. hansenii immediately distinguish it from D. gypsophilum, which has neither.</discussion>
  <discussion>Delphinium gypsophilum is related, and similar in many respects, to D. recurvatum. The two may be distinguished morphologically by their sepals. Delphinium recurvatum has reflexed, blue sepals; those of D. gypsophilum are spreading and white, although they may change to light blue when dry. Plants of D. recurvatum normally are less than 60 cm; those of D. gypsophilum are usually more than 60 cm. Ecologically, D. recurvatum occupies level ground among shrubs, typically in alkaline valley bottoms; D. gypsophilum is found on well-drained hillsides among grasses and in chaparral and oak woodland.</discussion>
  <discussion>From Delphinium hesperium subsp. pallescens, specimens of D. gypsophilum may be separated by their much more finely dissected leaves, with less surface area, stem base usually reddish, stems frequently glaucous proximally, undulate margins of seed coat cells, and absence of striations in stem base of dried specimens. In contrast, D. hesperium subsp. pallescens has leaves less dissected, with greater surface area, stem base rarely reddish, stem not glaucous proximally, seed coat cells with straight margins, and striations present on the proximal stem of dried specimens.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lower petals 5–8 mm; lateral sepals 10 mm or more; pedicel usually more than 1 cm apart.</description>
      <determination>29a Delphinium gypsophilum subsp. gypsophilum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lower petals 3–5 mm; lateral sepals 10 mm or less; pedicel usually less than 1 cm apart.</description>
      <determination>29b Delphinium gypsophilum subsp. parviflorum</determination>
    </key_statement>
  </key>
</bio:treatment>