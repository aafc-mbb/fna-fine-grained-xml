<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">nymphaeaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">nymphaea</taxon_name>
    <taxon_name authority="Zuccarini" date="1832" rank="species">mexicana</taxon_name>
    <place_of_publication>
      <publication_title>Abh. Math.-Phys. Cl. Königl. Bayer. Akad. Wiss.</publication_title>
      <place_in_publication>1: 365. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nymphaeaceae;genus nymphaea;species mexicana</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500827</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castalia</taxon_name>
    <taxon_name authority="(Leitner) Greene" date="unknown" rank="species">flava</taxon_name>
    <taxon_hierarchy>genus Castalia;species flava;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nymphaea</taxon_name>
    <taxon_name authority="Leitner" date="unknown" rank="species">flava</taxon_name>
    <taxon_hierarchy>genus Nymphaea;species flava;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes unbranched, erect, cylindric;</text>
      <biological_entity id="o22731" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s0" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stolons elongate, spongy, developing clusters of curved, fleshy, overwintering roots resembling tiny bananas at terminal nodes.</text>
      <biological_entity id="o22732" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="texture" src="d0_s1" value="spongy" value_original="spongy" />
        <character is_modifier="false" name="development" src="d0_s1" value="developing" value_original="developing" />
        <character constraint="of roots" constraintid="o22733" is_modifier="false" name="arrangement" src="d0_s1" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o22733" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="course" src="d0_s1" value="curved" value_original="curved" />
        <character is_modifier="true" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="duration" src="d0_s1" value="overwintering" value_original="overwintering" />
      </biological_entity>
      <biological_entity id="o22734" name="banana" name_original="bananas" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="tiny" value_original="tiny" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o22735" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o22733" id="r3098" name="resembling" negation="false" src="d0_s1" to="o22734" />
      <relation from="o22732" id="r3099" name="at" negation="false" src="d0_s1" to="o22735" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole glabrous.</text>
      <biological_entity id="o22736" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22737" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade abaxially purplish with dark flecks, adaxially green, often with brown mottling, ovate to elliptic or nearly orbiculate, 7-18 (-27) × 7-14 (-18) cm, margins entire or sinuate;</text>
      <biological_entity id="o22739" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="dark flecks" value_original="dark flecks" />
        <character is_modifier="true" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s3" value="brown mottling" value_original="brown mottling" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s3" to="elliptic or nearly orbiculate" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_length" src="d0_s3" to="27" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="18" to_unit="cm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s3" to="18" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="14" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="sinuate" value_original="sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>venation radiate and impressed centrally, without weblike pattern, principal veins 11-22;</text>
      <biological_entity id="o22738" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character constraint="with margins" constraintid="o22739" is_modifier="false" modifier="abaxially" name="coloration" src="d0_s3" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o22740" name="pattern" name_original="pattern" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="centrally" name="prominence" src="d0_s4" value="impressed" value_original="impressed" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="weblike" value_original="weblike" />
      </biological_entity>
      <biological_entity constraint="principal" id="o22741" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s4" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>surfaces glabrous.</text>
      <biological_entity id="o22742" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers floating or emersed, 6-11 cm diam., opening and closing diurnally, only sepals and outermost petals in distinct whorls of 4;</text>
      <biological_entity id="o22743" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="location" src="d0_s6" value="floating" value_original="floating" />
        <character is_modifier="false" name="location" src="d0_s6" value="emersed" value_original="emersed" />
        <character char_type="range_value" from="6" from_unit="cm" name="diameter" src="d0_s6" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22744" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="true" name="condition" src="d0_s6" value="closing" value_original="closing" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o22745" name="petal" name_original="petals" src="d0_s6" type="structure" />
      <biological_entity id="o22746" name="whorl" name_original="whorls" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <relation from="o22743" id="r3100" name="opening" negation="false" src="d0_s6" to="o22744" />
      <relation from="o22745" id="r3101" name="in" negation="false" src="d0_s6" to="o22746" />
    </statement>
    <statement id="d0_s7">
      <text>sepals uniformly yellowish green, often red-tinted, evidently veined, lines of insertion on receptacle often slightly prominent;</text>
      <biological_entity id="o22747" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="red-tinted" value_original="red-tinted" />
        <character is_modifier="false" modifier="evidently" name="architecture" src="d0_s7" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o22748" name="line" name_original="lines" src="d0_s7" type="structure" />
      <biological_entity id="o22749" name="receptacle" name_original="receptacle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often slightly" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o22748" id="r3102" name="on" negation="false" src="d0_s7" to="o22749" />
    </statement>
    <statement id="d0_s8">
      <text>petals 12-30, yellow;</text>
      <biological_entity id="o22750" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="30" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens ca. 50-60, yellow, connective appendage minute or absent;</text>
      <biological_entity id="o22751" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s9" to="60" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="connective" id="o22752" name="appendage" name_original="appendage" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="minute" value_original="minute" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments widest below middle, longer than anthers;</text>
      <biological_entity id="o22753" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character constraint="than anthers" constraintid="o22755" is_modifier="false" name="length_or_size" notes="" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o22754" name="middle" name_original="middle" src="d0_s10" type="structure" />
      <biological_entity id="o22755" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o22753" id="r3103" name="below middle" negation="false" src="d0_s10" to="o22754" />
    </statement>
    <statement id="d0_s11">
      <text>pistil 7-10-locular, appendages at margin of stigmatic disk oblong-tapered, to 4.5 mm.</text>
      <biological_entity id="o22756" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="7-10-locular" value_original="7-10-locular" />
      </biological_entity>
      <biological_entity id="o22757" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22758" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong-tapered" value_original="oblong-tapered" />
      </biological_entity>
      <biological_entity id="o22759" name="stigmatic" name_original="stigmatic" src="d0_s11" type="structure" />
      <biological_entity id="o22760" name="disk" name_original="disk" src="d0_s11" type="structure" />
      <relation from="o22757" id="r3104" name="at" negation="false" src="d0_s11" to="o22758" />
      <relation from="o22758" id="r3105" name="part_of" negation="false" src="d0_s11" to="o22759" />
      <relation from="o22758" id="r3106" name="part_of" negation="false" src="d0_s11" to="o22760" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds globose, ca. 5 × 5 mm, uniformly covered with hairlike papillae 100-220 µm. 2n = 56.</text>
      <biological_entity id="o22761" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character name="length" src="d0_s12" unit="mm" value="5" value_original="5" />
        <character name="width" src="d0_s12" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="100" from_unit="um" name="some_measurement" src="d0_s12" to="220" to_unit="um" />
      </biological_entity>
      <biological_entity id="o22762" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22763" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="56" value_original="56" />
      </biological_entity>
      <relation from="o22761" id="r3107" modifier="uniformly" name="covered with" negation="false" src="d0_s12" to="o22762" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall, mainly summer farther north.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="flowering time" char_type="range_value" modifier="mainly;  farther north" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Outer coastal plain in alkaline lakes, ponds, warm springs, pools in marshes, sloughs, sluggish streams, ditches, and canals</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="outer coastal plain" constraint="in alkaline lakes , ponds , warm springs ," />
        <character name="habitat" value="alkaline lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="warm springs" />
        <character name="habitat" value="pools" constraint="in marshes , sloughs ," />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="sloughs" />
        <character name="habitat" value="sluggish streams" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="canals" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Calif., Fla., Ga., La., Miss., N.C., Okla., S.C., Tex.; ne, c Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="ne" establishment_means="native" />
        <character name="distribution" value="c Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Banana water-lily</other_name>
  <other_name type="common_name">yellow water-lily</other_name>
  <other_name type="common_name">herbe au coeur</other_name>
  <discussion>Nymphaea mexicana is probably introduced in most inland sites and in California, where it is considered a problematic weed in waterways; it is not common in most states except Florida. The distribution of this species is similar to that of the winter distribution of canvasback ducks, for which the bananalike tubers are an important food (J. E. Cely 1979). This species forms natural hybrids with N. odorata; the hybrids have been named N. ×thiona D. B. Ward (D. B. Ward 1977). Except for stem characteristics, which resemble one or the other parent, and their added vigor, the hybrids are generally intermediate in morphology. They are completely sterile; however, hybrids with the stolon-bearing habit of N. mexicana can form extensive clones and, although somewhat larger in stature than N. mexicana, they closely resemble that less agressive parent and could easily be mistaken for it. Some of the introductions, such as in southeastern Nevada and north-central Kentucky, are clearly this hybrid.</discussion>
  <references>
    <reference>Capperino, M. E. and E. L. Schneider. 1985. Floral biology of Nymphaea mexicana Zucc. (Nymphaeaceae). Aquatic Bot. 23: 83-93.</reference>
    <reference>Cely, J. E. 1979. The ecology and distribution of banana waterlily and its utilization by canvasback ducks. Proc. Annual Conf. SouthE. Assoc. Fish Wildlife Agencies 33: 43-47.</reference>
  </references>
  
</bio:treatment>