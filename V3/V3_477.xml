<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">ceratophyllaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ceratophyllum</taxon_name>
    <taxon_name authority="Chamisso" date="1829" rank="species">muricatum</taxon_name>
    <taxon_name authority="(Grisebach) Les" date="1988" rank="subspecies">australe</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>13: 85. 1988</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ceratophyllaceae;genus ceratophyllum;species muricatum;subspecies australe</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500340</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceratophyllum</taxon_name>
    <taxon_name authority="Grisebach" date="unknown" rank="species">australe</taxon_name>
    <place_of_publication>
      <publication_title>Symb. Fl. Argent.,</publication_title>
      <place_in_publication>14. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ceratophyllum;species australe;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceratophyllum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">demersum</taxon_name>
    <taxon_name authority="K. Schumann" date="unknown" rank="variety">cristatum</taxon_name>
    <taxon_hierarchy>genus Ceratophyllum;species demersum;variety cristatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceratophyllum</taxon_name>
    <taxon_name authority="Fassett" date="unknown" rank="species">floridanum</taxon_name>
    <taxon_hierarchy>genus Ceratophyllum;species floridanum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceratophyllum</taxon_name>
    <taxon_name authority="Fassett" date="unknown" rank="species">llerenae</taxon_name>
    <taxon_hierarchy>genus Ceratophyllum;species llerenae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 1 m;</text>
      <biological_entity id="o26206" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>apical leaf whorls not densely crowded.</text>
      <biological_entity constraint="leaf" id="o26207" name="whorl" name_original="whorls" src="d0_s1" type="structure" constraint_original="apical leaf">
        <character is_modifier="false" modifier="not densely" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves light green to yellow-green, fine-textured.</text>
      <biological_entity id="o26208" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s2" to="yellow-green" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fine-textured" value_original="fine-textured" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade simple or forked into 2-10 ultimate segments (forking of largest leaves 3d or 4th order), segments sometimes conspicuously inflated, mature leaf whorls 2.5-6 cm diam., marginal denticles weak and inconspicuous, not raised on broad base of green tissue, sometimes nearly absent;</text>
      <biological_entity id="o26209" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character constraint="into ultimate segments" constraintid="o26210" is_modifier="false" name="shape" src="d0_s3" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o26210" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <biological_entity id="o26211" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes conspicuously" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o26212" name="whorl" name_original="whorls" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="mature" value_original="mature" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="diameter" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26214" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o26215" name="tissue" name_original="tissue" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="true" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o26213" id="r3543" name="raised on" negation="true" src="d0_s3" to="o26214" />
      <relation from="o26213" id="r3544" name="raised on" negation="false" src="d0_s3" to="o26215" />
    </statement>
    <statement id="d0_s4">
      <text>1st leaves of plumule simple.</text>
      <biological_entity constraint="marginal" id="o26213" name="denticle" name_original="denticles" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s3" value="weak" value_original="weak" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" modifier="sometimes nearly" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o26216" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o26217" name="plumule" name_original="plumule" src="d0_s4" type="structure" />
      <relation from="o26216" id="r3545" name="part_of" negation="false" src="d0_s4" to="o26217" />
    </statement>
    <statement id="d0_s5">
      <text>Achene green or dark-brown, body (excluding spines) 3-4.5 × 2-3 × 1-2 mm, basal spines 2 (rarely absent), straight or curved, 0.3-4.5 mm, marginal spines (0-) 2-20, 0.1-4 mm, terminal spine straight or curved, 1.5-7.5 mm, margins slightly winged.</text>
      <biological_entity id="o26218" name="achene" name_original="achene" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
      <biological_entity id="o26219" name="body" name_original="body" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="height" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o26220" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o26221" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s5" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="20" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o26222" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26223" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s5" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh water of ponds and lakes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fresh water" constraint="of ponds and lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., N.C.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.</number>
  <discussion>Principally of tropical distribution, Ceratophyllum muricatum is known in North America north of Mexico only from ephemeral habitats in coastal southeastern United States. The wide range of variation of spines on the fruits in C. muricatum has led to the describing of several variants (typically when spine lengths have been reduced) as different species. As in C. demersum, spineless phenotypes from North America have been called C. submersum Linnaeus, which does not occur in the New World. The affinity of C. muricatum for shallow, ephemeral habitats results in its sporadic and nonpersistent occurrence in present North American localities. Fossil records of C. muricatum from the lower and middle Eocene document its occurrence in more inland sites, presumably when the climate of the interior stations was more similar to tropical conditions. Of the three North American species of Ceratophyllum, this species is most likely to be collected (in season) with fruit present.</discussion>
  
</bio:treatment>