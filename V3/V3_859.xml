<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Henk var der Werff</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">lauraceae</taxon_name>
    <taxon_name authority="Schaeffer" date="1760" rank="genus">CINNAMOMUM</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Exped.,</publication_title>
      <place_in_publication>74. 1760, name conserved</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lauraceae;genus CINNAMOMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kinnamomon, cinnamon</other_info_on_name>
    <other_info_on_name type="fna_id">107109</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, evergreen.</text>
      <biological_entity id="o7539" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark gray [or brown], furrowed [or smooth];</text>
      <biological_entity id="o7541" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark and leaves often aromatic.</text>
      <biological_entity id="o7542" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="odor" src="d0_s2" value="aromatic" value_original="aromatic" />
      </biological_entity>
      <biological_entity id="o7543" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="odor" src="d0_s2" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate, infrequently opposite.</text>
      <biological_entity id="o7544" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="infrequently" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade with (1-) 3 primary-veins [or infrequently pinnately veined], papery to leathery;</text>
      <biological_entity id="o7545" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="papery" name="texture" notes="" src="d0_s4" to="leathery" />
      </biological_entity>
      <biological_entity id="o7546" name="primary-vein" name_original="primary-veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <relation from="o7545" id="r1049" name="with" negation="false" src="d0_s4" to="o7546" />
    </statement>
    <statement id="d0_s5">
      <text>surfaces glabrous or variously pubescent;</text>
      <biological_entity id="o7547" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="variously" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>domatia frequently present.</text>
      <biological_entity id="o7548" name="domatium" name_original="domatia" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="frequently" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences appearing when mature leaves present, axillary, panicles.</text>
      <biological_entity id="o7549" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o7550" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o7551" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <relation from="o7549" id="r1050" name="appearing when" negation="false" src="d0_s7" to="o7550" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o7552" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals deciduous or persistent, white, green, or yellow, equal;</text>
      <biological_entity id="o7553" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 9, anthers 4-locular, 4-valved (rarely with anthers of inner 3 stamens 2-locular), extrorse;</text>
      <biological_entity id="o7554" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="9" value_original="9" />
      </biological_entity>
      <biological_entity id="o7555" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="4-locular" value_original="4-locular" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="4-valved" value_original="4-valved" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s10" value="extrorse" value_original="extrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes 3, apex sagittate or cordate;</text>
      <biological_entity id="o7556" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o7557" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cordate" value_original="cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary ovoid-ellipsoid.</text>
      <biological_entity id="o7558" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid-ellipsoid" value_original="ovoid-ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Drupe bluish black, nearly globose, seated in small cupule with entire single rim or tepals persistent.</text>
      <biological_entity id="o7559" name="drupe" name_original="drupe" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="bluish black" value_original="bluish black" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character constraint="in cupule" constraintid="o7560" is_modifier="false" name="location" src="d0_s13" value="seated" value_original="seated" />
      </biological_entity>
      <biological_entity id="o7560" name="cupule" name_original="cupule" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o7561" name="rim" name_original="rim" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="true" name="quantity" src="d0_s13" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o7562" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o7560" id="r1051" name="with" negation="false" src="d0_s13" to="o7561" />
      <relation from="o7560" id="r1052" name="with" negation="false" src="d0_s13" to="o7562" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical and subtropical regions, North America, Central America, South America, Asia, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical and subtropical regions" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Species 300 or more (1 in the flora).</discussion>
  <discussion>The neotropical species were formerly included in Phoebe, but they are better placed in Cinnamomum.</discussion>
  
</bio:treatment>