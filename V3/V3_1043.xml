<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(Greene) B. Boivin" date="1944" rank="section">Leucocoma</taxon_name>
    <taxon_name authority="Greene" date="1909" rank="species">amphibolum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>1: 173. 1817, not T. revolutum Le Lièvre 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section leucocoma;species amphibolum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501274</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">amphibolum</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species amphibolum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">hepaticum</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species hepaticum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">moseleyi</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species moseleyi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">revolutum</taxon_name>
    <taxon_name authority="B. Boivin" date="unknown" rank="variety">glandulosior</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species revolutum;variety glandulosior;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, coarse, 50-150 cm.</text>
      <biological_entity id="o26904" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="relief" src="d0_s0" value="coarse" value_original="coarse" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline, proximal leaves petiolate, distal sessile;</text>
      <biological_entity id="o26905" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o26906" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o26907" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles and rachises stipitate-glandular to glabrous.</text>
      <biological_entity id="o26908" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="stipitate-glandular" name="pubescence" src="d0_s2" to="glabrous" />
      </biological_entity>
      <biological_entity id="o26909" name="rachis" name_original="rachises" src="d0_s2" type="structure">
        <character char_type="range_value" from="stipitate-glandular" name="pubescence" src="d0_s2" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade 1-4×-ternately compound;</text>
      <biological_entity id="o26910" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="1-4×-ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets grayish or brownish green or dark to bright green, lanceolate, elliptic, ovate, reniform to obovate, apically undivided or 2-3 (-5) -lobed, 9-60 × 5-50 mm, length 0.9-2.7 (-5.25) times width, usually leathery, margins often revolute, lobe margins entire;</text>
      <biological_entity id="o26911" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="brownish green or dark" name="coloration" src="d0_s4" to="bright green" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="reniform" name="shape" src="d0_s4" to="obovate apically undivided or 2-3(-5)-lobed" />
        <character char_type="range_value" from="reniform" name="shape" src="d0_s4" to="obovate apically undivided or 2-3(-5)-lobed" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="50" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="0.9-2.7(-5.25)" value_original="0.9-2.7(-5.25)" />
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o26912" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o26913" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>surfaces abaxially with sessile to stalked glands or muriculate to whitish papillose.</text>
      <biological_entity id="o26914" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character char_type="range_value" from="muriculate" name="relief" src="d0_s5" to="whitish papillose" />
      </biological_entity>
      <biological_entity id="o26915" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character char_type="range_value" from="sessile" is_modifier="true" name="architecture" src="d0_s5" to="stalked" />
      </biological_entity>
      <relation from="o26914" id="r3623" name="with" negation="false" src="d0_s5" to="o26915" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences racemes to panicles, elongate, many flowered;</text>
      <biological_entity constraint="inflorescences" id="o26916" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
      </biological_entity>
      <biological_entity id="o26917" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <relation from="o26916" id="r3624" name="to" negation="false" src="d0_s6" to="o26917" />
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels sometimes stipitate-glandular.</text>
      <biological_entity id="o26918" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o26919" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers usually unisexual, staminate and pistillate on different plants;</text>
      <biological_entity id="o26920" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o26921" is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26921" name="plant" name_original="plants" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 4 (-6), whitish, ovate to oblanceolate, (2-) 3-4 mm;</text>
      <biological_entity id="o26922" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="6" />
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments white, slightly clavate, 2.5-7.8 mm, ± flexible;</text>
      <biological_entity id="o26923" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7.8" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s10" value="pliable" value_original="flexible" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers (0.7-) 1.2-2.7 (-3) mm, blunt to apiculate.</text>
      <biological_entity id="o26924" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="1.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="blunt" name="shape" src="d0_s11" to="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes 8-16, sessile or slightly stipitate;</text>
      <biological_entity id="o26925" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s12" to="16" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s12" value="stipitate" value_original="stipitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stipe 0.2-1.7 mm;</text>
      <biological_entity id="o26926" name="stipe" name_original="stipe" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>body lanceolate to ellipsoid, 3.5-5 mm, prominently veined, usually stipitate-glandular;</text>
      <biological_entity id="o26927" name="body" name_original="body" src="d0_s14" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s14" to="ellipsoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s14" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak ± persistent, linear-filiform, (1-) 1.5-3.3 (-5) mm, ± equal to length of achene body.</text>
      <biological_entity constraint="achene" id="o26929" name="body" name_original="body" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 140.</text>
      <biological_entity id="o26928" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="linear-filiform" value_original="linear-filiform" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3.3" to_unit="mm" />
        <character constraint="to " constraintid="o26929" is_modifier="false" modifier="more or less" name="variability" src="d0_s15" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26930" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="140" value_original="140" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Mar–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry open woods, brushy banks, thickets, barrens, and prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry open woods" />
        <character name="habitat" value="brushy banks" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="barrens" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Ont., Que.; Ala., Ariz., Ark., Colo., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Ky., La., Md., Mass., Mich., Minn., Miss., Mo., Nev., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">Skunk meadow-rue</other_name>
  <other_name type="common_name">wax-leaved meadow-rue</other_name>
  <other_name type="common_name">purple meadow-rue</other_name>
  <discussion>Glandular individuals of Thalictrum revolutum have been called var. glandulosior. They are seen throughout the range of the species and do not represent a distinct lineage. Occasional glandular plants with unusually short anthers are often misidentified as T. pubescens.</discussion>
  <discussion>Material of this species from the western United States has been incorrectly assumed by previous authors to be T. dasycarpum, because T. revolutum is not included in floras of that region.</discussion>
  
</bio:treatment>