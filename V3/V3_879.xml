<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="(de Candolle) G. Lawson" date="1884" rank="species">acutiloba</taxon_name>
    <place_of_publication>
      <publication_title>Proc. &amp; Trans. Roy. Soc. Canada</publication_title>
      <place_in_publication>2(4): 30. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species acutiloba</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500048</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">acutiloba</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>1: 22. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hepatica;species acutiloba;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_name authority="(Pursh) Britton" date="unknown" rank="species">acuta</taxon_name>
    <taxon_hierarchy>genus Hepatica;species acuta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">nobilis</taxon_name>
    <taxon_name authority="(Pursh) Steyermark" date="unknown" rank="variety">acuta</taxon_name>
    <taxon_hierarchy>genus Hepatica;species nobilis;variety acuta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_name authority="Chaix" date="unknown" rank="species">triloba</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="variety">acuta</taxon_name>
    <taxon_hierarchy>genus Hepatica;species triloba;variety acuta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">triloba</taxon_name>
    <taxon_name authority="(de Candolle) Warner" date="unknown" rank="variety">acutiloba</taxon_name>
    <taxon_hierarchy>genus Hepatica;species triloba;variety acutiloba;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 5-19 cm, from rhizomes, rhizomes ascending to horizontal.</text>
      <biological_entity id="o5711" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="19" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5712" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o5713" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="horizontal" />
      </biological_entity>
      <relation from="o5711" id="r802" name="from" negation="false" src="d0_s0" to="o5712" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 3-15, often purplish abaxially, simple, deeply divided;</text>
      <biological_entity constraint="basal" id="o5714" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="15" />
        <character is_modifier="false" modifier="often; abaxially" name="coloration" src="d0_s1" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s1" value="divided" value_original="divided" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 3-19 cm;</text>
      <biological_entity id="o5715" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="19" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaf-blade widely orbiculate, 1.3-8 × 1.8- -11.5 cm, base cordate, margins entire, apex acute or acuminate, surfaces strongly villous to glabrescent;</text>
      <biological_entity id="o5716" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s3" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="length" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s3" to="11.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5717" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o5718" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5719" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o5720" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="strongly villous" name="pubescence" src="d0_s3" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lobes 3 (-5), deltate, 0.7-4 cm wide;</text>
      <biological_entity id="o5721" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="5" />
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s4" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>middle lobe 70-90% of total blade length.</text>
      <biological_entity constraint="middle" id="o5722" name="lobe" name_original="lobe" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1-flowered, villous to pilose;</text>
      <biological_entity id="o5723" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-flowered" value_original="1-flowered" />
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s6" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>involucral-bracts 3, 1-tiered, simple, dissimilar to basal leaves, lanceolate to ovate, 0.53-1.8 × 0.27-0.95 cm, sessile, calyxlike, closely subtending flowers, bases distinct, cuneate, margins entire, apex acute, strongly villous to glabrescent.</text>
      <biological_entity id="o5724" name="involucral-bract" name_original="involucral-bracts" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="1-tiered" value_original="1-tiered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s7" to="ovate" />
        <character char_type="range_value" from="0.53" from_unit="cm" name="length" src="d0_s7" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.27" from_unit="cm" name="width" src="d0_s7" to="0.95" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="calyxlike" value_original="calyxlike" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5725" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o5726" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5727" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o5728" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5729" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character char_type="range_value" from="strongly villous" name="pubescence" src="d0_s7" to="glabrescent" />
      </biological_entity>
      <relation from="o5724" id="r803" name="to" negation="false" src="d0_s7" to="o5725" />
      <relation from="o5724" id="r804" name="closely subtending" negation="false" src="d0_s7" to="o5726" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 5-12, white to pink or bluish, ovate to obovate, 6-14.6 × 2.2-5.8 mm, glabrous;</text>
      <biological_entity id="o5730" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5731" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="12" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="pink or bluish" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s8" to="14.6" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s8" to="5.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals absent;</text>
      <biological_entity id="o5732" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5733" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 10-30.</text>
      <biological_entity id="o5734" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5735" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Heads of achenes spheric;</text>
      <biological_entity id="o5736" name="head" name_original="heads" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity id="o5737" name="achene" name_original="achenes" src="d0_s11" type="structure" />
      <relation from="o5736" id="r805" name="part_of" negation="false" src="d0_s11" to="o5737" />
    </statement>
    <statement id="d0_s12">
      <text>pedicel 0.1-0.4 cm.</text>
      <biological_entity id="o5738" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s12" to="0.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes: body narrowly ovoid, 3.5-4.7 × 1.3-1.9 mm, slightly winged, hispid, gradually tapering;</text>
      <biological_entity id="o5739" name="achene" name_original="achenes" src="d0_s13" type="structure" />
      <biological_entity id="o5740" name="body" name_original="body" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s13" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s13" to="1.9" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak indistinct.</text>
      <biological_entity id="o5741" name="achene" name_original="achenes" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n=14.</text>
      <biological_entity id="o5742" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5743" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous woods, often in calcareous soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous woods" />
        <character name="habitat" value="calcareous soils" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ark., Conn., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Mo., N.H., N.Y., N.C., Ohio, Pa., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <other_name type="common_name">Sharp-lobed hepatica</other_name>
  <other_name type="common_name">anémone &amp;agrave; lobes aigus</other_name>
  <other_name type="common_name">hépatique &amp;agrave; lobes aigus</other_name>
  <discussion>In North America, Anemone acutiloba and A. americana are sufficiently well differentiated to enable the distinction of the two species. Some intermediates do occur but it is uncertain as to whether thes intermediates or hybrids. The fact that the two species are highly sympatric and still maintain their differences implies that they should still be recognized as distinctive species (see G.L. Stebbins 1993).</discussion>
  <discussion>The two North American species formerly placed in Hepatica are closely allied to the Eurasian Anemone hepatica Linnaeus [=Hepatica nobilis Miller, Hepatica hepatica (Linnaeus) Karst]. Among European collections, plants approach either A. acutiloba or A. americana in leaf morphology, but some intermediates are found (J. A. Steyermark and C. S. Steyermark 1960). North American plants differ from A. hepatica in having narrower sepals, larger involucral bracts, and shorter and less pubescent scapes. Further research, including a comparative study of breeding systems, is needed to clarify the relationship between Anemone hepatica, A. acutiloba, and A. americana. Pending such work, the eastern North American hepaticas are here recognized as distinct species.</discussion>
  <discussion>D. E. Moerman (1986) lists Hepatica acutiloba as one of the plants used medicinally by Native Americans in the treatment of abdominal pains, poor digestion, and constipation, as a wash for "twisted mouth or crossed eyes," and as a gynecological aid.</discussion>
  
</bio:treatment>