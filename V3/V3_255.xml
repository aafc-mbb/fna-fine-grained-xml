<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">clematis</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1895" rank="subgenus">viorna</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">reticulata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>156. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus clematis;subgenus viorna;species reticulata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500410</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viorna</taxon_name>
    <taxon_name authority="(Walter) Small" date="unknown" rank="species">reticulata</taxon_name>
    <taxon_hierarchy>genus Viorna;species reticulata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Harbison ex Small" date="unknown" rank="species">V.subreticulata</taxon_name>
    <taxon_hierarchy>species V.subreticulata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems viny, to 4 m, glabrous or sparsely pilose-pubescent, sometimes more densely pubescent near nodes.</text>
      <biological_entity id="o27178" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="viny" value_original="viny" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="pilose-pubescent" value_original="pilose-pubescent" />
        <character constraint="near nodes" constraintid="o27179" is_modifier="false" modifier="sometimes; densely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o27179" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade 1-pinnate;</text>
      <biological_entity id="o27180" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="1-pinnate" value_original="1-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets 6-8 plus additional tendril-like terminal leaflet, elliptic to ovate, unlobed, 1-3-lobed, or proximal 3-foliolate, 1-9 × 0.5-5 (-7.5) cm, leathery, prominently and finely reticulate abaxially and adaxially;</text>
      <biological_entity id="o27181" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="8" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s2" to="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-3-lobed" value_original="1-3-lobed" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o27182" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="additional" value_original="additional" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="tendril-like" value_original="tendril-like" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27183" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="prominently; finely; abaxially; adaxially" name="architecture_or_coloration_or_relief" src="d0_s2" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>surfaces abaxially silky-pubescent, not glaucous.</text>
      <biological_entity id="o27184" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="silky-pubescent" value_original="silky-pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, 1-3-flowered;</text>
      <biological_entity id="o27185" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts about 1/3 distance from base of peduncle.</text>
      <biological_entity id="o27186" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character constraint="from base" constraintid="o27187" name="quantity" src="d0_s5" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o27187" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o27188" name="peduncle" name_original="peduncle" src="d0_s5" type="structure" />
      <relation from="o27187" id="r3652" name="part_of" negation="false" src="d0_s5" to="o27188" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers urn-shaped;</text>
      <biological_entity id="o27189" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="urn--shaped" value_original="urn--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals pale lavender to purple, greenish toward tip, ovatelanceolate, 1.2-3 cm, margins not expanded, ± thick, not crispate, densely tomentose, tips acute, recurved, abaxially usually ± densely yellowish pubescent, occasionally nearly glabrous.</text>
      <biological_entity id="o27190" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale lavender" name="coloration" src="d0_s7" to="purple" />
        <character constraint="toward tip" constraintid="o27191" is_modifier="false" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27191" name="tip" name_original="tip" src="d0_s7" type="structure" />
      <biological_entity id="o27192" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s7" value="expanded" value_original="expanded" />
        <character is_modifier="false" modifier="more or less" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="crispate" value_original="crispate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o27193" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="abaxially usually more or less densely" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="occasionally nearly" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes: bodies appressed-pubescent;</text>
      <biological_entity id="o27194" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <biological_entity id="o27195" name="body" name_original="bodies" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak 4-6 cm, plumose.</text>
      <biological_entity id="o27196" name="achene" name_original="achenes" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 16.</text>
      <biological_entity id="o27197" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s9" to="6" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27198" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry woods and thickets in sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry woods" constraint="in sandy soils" />
        <character name="habitat" value="thickets" constraint="in sandy soils" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-150 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., Okla., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <discussion>In immature fruit, especially, the vestiture of the beaks of Clematis reticulata might not consistently suffice to distinguish it from C. pitcheri, which has appressed-pubescent beaks. Clematis reticulata is distinguished from C. pitcheri by the very fine reticulation of the leaves, with the smallest areoles completely enclosed by veinlets generally less than 1 mm long and even the quaternary veins prominently raised on the adaxial surface.</discussion>
  
</bio:treatment>