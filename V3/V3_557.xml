<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="de Candolle" date="1824" rank="section">Echinella</taxon_name>
    <taxon_name authority="A. Sprengel in K. Sprengel" date="1828" rank="species">platensis</taxon_name>
    <place_of_publication>
      <publication_title>in K. Sprengel,  Syst. Veg.</publication_title>
      <place_in_publication>5: 586. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section echinella;species platensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">233501197</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems decumbent, sparsely pilose.</text>
      <biological_entity id="o593" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal and lower cauline leaf-blades reniform, 3-parted, 0.8-1.7 × 1.2-2 cm, segment base cordate, margins dentate or lobulate, apex rounded-obtuse.</text>
      <biological_entity constraint="basal and lower cauline" id="o594" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s1" value="3-parted" value_original="3-parted" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s1" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="segment" id="o595" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o596" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobulate" value_original="lobulate" />
      </biological_entity>
      <biological_entity id="o597" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded-obtuse" value_original="rounded-obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers sessile;</text>
      <biological_entity id="o598" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>receptacle glabrous or pilose;</text>
      <biological_entity id="o599" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals 3, spreading, 1-2 × 0.5-1 mm, hirsute;</text>
      <biological_entity id="o600" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 3, 1-2 × 0.5-0.8 mm.</text>
      <biological_entity id="o601" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes hemispheric, 2-2.5 × 3-3.5 mm;</text>
      <biological_entity id="o602" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o603" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o602" id="r67" name="part_of" negation="false" src="d0_s6" to="o603" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 10-15 per head, 1.3-1.7 × 1-1.3 mm, faces and proximal margin finely papillate, each papilla crowned with hooked bristle, otherwise glabrous;</text>
      <biological_entity id="o604" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per head" constraintid="o605" from="10" name="quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" notes="" src="d0_s7" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s7" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o605" name="head" name_original="head" src="d0_s7" type="structure" />
      <biological_entity id="o606" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s7" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o607" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s7" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o608" name="papilla" name_original="papilla" src="d0_s7" type="structure">
        <character constraint="with bristle" constraintid="o609" is_modifier="false" name="architecture" src="d0_s7" value="crowned" value_original="crowned" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o609" name="bristle" name_original="bristle" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>beak semicircular to deltate, curved, 0.2-0.5 mm.</text>
      <biological_entity id="o610" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s8" to="deltate" />
        <character is_modifier="false" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–spring (Feb–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="winter" />
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Weed in disturbed areas, usually near coast</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="weed" constraint="in disturbed areas" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="coast" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Fla., Ga., La., Miss., N.C., Tex.; native to South America (s Brazil, Uruguay, and Argentina).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="native to South America (s Brazil)" establishment_means="native" />
        <character name="distribution" value="native to South America (Uruguay)" establishment_means="native" />
        <character name="distribution" value="native to South America (and Argentina)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  
</bio:treatment>