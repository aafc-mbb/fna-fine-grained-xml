<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Blume" date="unknown" rank="family">myricaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">myrica</taxon_name>
    <taxon_name authority="Mirbel in H. Duhamel du Monceau et al." date="1804" rank="species">pensylvanica</taxon_name>
    <place_of_publication>
      <publication_title>in H. Duhamel du Monceau et al.,  Traité Arbr. Arbust. Nouv. ed.</publication_title>
      <place_in_publication>2: 190. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myricaceae;genus myrica;species pensylvanica</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">233500794</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerothamnus</taxon_name>
    <taxon_name authority="(Mirbel) Moldenke" date="unknown" rank="species">pensylvanica</taxon_name>
    <taxon_hierarchy>genus Cerothamnus;species pensylvanica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myrica</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">cerifera</taxon_name>
    <taxon_name authority="Castiglioni" date="unknown" rank="variety">frutescens</taxon_name>
    <taxon_hierarchy>genus Myrica;species cerifera;variety frutescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myrica</taxon_name>
    <taxon_name authority="Youngken" date="unknown" rank="species">macfarlanei</taxon_name>
    <taxon_hierarchy>genus Myrica;species macfarlanei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or rarely small trees, deciduous, rhizomatous, colonial, to 2 (-4.5) m.</text>
      <biological_entity id="o5249" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="4.5" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" modifier="rarely" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="4.5" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branchlets reddish-brown and gland-dotted when young, becoming whitish gray in age, otherwise densely pilose;</text>
      <biological_entity id="o5251" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="reddish-brown and gland-dotted" value_original="reddish-brown and gland-dotted" />
        <character constraint="in age" constraintid="o5252" is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="whitish gray" value_original="whitish gray" />
        <character is_modifier="false" modifier="otherwise densely" name="pubescence" notes="" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o5252" name="age" name_original="age" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>glands yellow.</text>
      <biological_entity id="o5253" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade aromatic when crushed, oblanceolate to elliptic, occasionally obovate, 2.5-6.5 (-7.8) × 1.5-2.7 cm, usually membranous, less often leathery, base cuneate to attenuate, margins sometimes entire, usually serrate distal to middle, apex obtuse to rounded, sometimes acute, short-apiculate;</text>
      <biological_entity id="o5254" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when crushed" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="elliptic" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="7.8" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s3" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="2.7" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="less often" name="texture" src="d0_s3" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o5255" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="attenuate" />
      </biological_entity>
      <biological_entity id="o5256" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s3" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="distal" name="position" src="d0_s3" to="middle" />
      </biological_entity>
      <biological_entity id="o5257" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="short-apiculate" value_original="short-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially pale green, pilose on veins, moderately to densely glandular, adaxially dark green, pilose (especially along midrib), glandless or sparsely glandular;</text>
      <biological_entity id="o5258" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale green" value_original="pale green" />
        <character constraint="on veins" constraintid="o5259" is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="moderately to densely" name="architecture_or_function_or_pubescence" notes="" src="d0_s4" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="glandless" value_original="glandless" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o5259" name="vein" name_original="veins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>glands yellowbrown.</text>
      <biological_entity id="o5260" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowbrown" value_original="yellowbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: staminate 0.4-1.8 cm;</text>
      <biological_entity id="o5261" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s6" to="1.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 0.3-1.4 cm.</text>
      <biological_entity id="o5262" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s7" to="1.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers unisexual, staminate and pistillate on different plants.</text>
      <biological_entity id="o5263" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o5264" is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5264" name="plant" name_original="plants" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Staminate flowers: bract of flower shorter than staminal column, margins opaque, apically ciliate or completely glabrous, usually abaxially glabrous, occasionally densely pilose;</text>
      <biological_entity id="o5265" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5266" name="bract" name_original="bract" src="d0_s9" type="structure">
        <character constraint="than staminal column" constraintid="o5268" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5267" name="flower" name_original="flower" src="d0_s9" type="structure" />
      <biological_entity constraint="staminal" id="o5268" name="column" name_original="column" src="d0_s9" type="structure" />
      <biological_entity id="o5269" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="opaque" value_original="opaque" />
        <character is_modifier="false" modifier="apically" name="pubescence" src="d0_s9" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="completely" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually abaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally densely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <relation from="o5266" id="r754" name="part_of" negation="false" src="d0_s9" to="o5267" />
    </statement>
    <statement id="d0_s10">
      <text>stamens mostly 3-4.</text>
      <biological_entity id="o5270" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5271" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: bracteoles persistent in fruit, 4, not accrescent or adnate to fruit wall, margins slightly ciliate or glabrous, abaxially usually densely gland-dotted;</text>
      <biological_entity id="o5272" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5273" name="bracteole" name_original="bracteoles" src="d0_s11" type="structure">
        <character constraint="in fruit" constraintid="o5274" is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="size" notes="" src="d0_s11" value="accrescent" value_original="accrescent" />
        <character constraint="to fruit wall" constraintid="o5275" is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o5274" name="fruit" name_original="fruit" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="fruit" id="o5275" name="wall" name_original="wall" src="d0_s11" type="structure" />
      <biological_entity id="o5276" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s11" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="abaxially usually densely" name="coloration" src="d0_s11" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary wall densely hirsute near apex, otherwise glabrous.</text>
      <biological_entity id="o5277" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="ovary" id="o5278" name="wall" name_original="wall" src="d0_s12" type="structure">
        <character constraint="near apex" constraintid="o5279" is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5279" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits globose-ellipsoid, 3.5-5.5 mm;</text>
      <biological_entity id="o5280" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="globose-ellipsoid" value_original="globose-ellipsoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>fruit wall and warty protuberances hirsute, at least when young, hairs usually obscured by thick coat of white wax.</text>
      <biological_entity constraint="fruit" id="o5281" name="wall" name_original="wall" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity constraint="fruit" id="o5282" name="protuberance" name_original="protuberances" src="d0_s14" type="structure">
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s14" value="warty" value_original="warty" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o5283" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character constraint="by coat" constraintid="o5284" is_modifier="false" modifier="at-least; when young; usually" name="prominence" src="d0_s14" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o5284" name="coat" name_original="coat" src="d0_s14" type="structure">
        <character is_modifier="true" name="width" src="d0_s14" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o5285" name="wax" name_original="wax" src="d0_s14" type="substance">
        <character is_modifier="true" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <relation from="o5284" id="r755" name="part_of" negation="false" src="d0_s14" to="o5285" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer, fruiting late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes, pine barrens, pine-oak forests, old fields, bogs, edges of streams, ponds, and swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
        <character name="habitat" value="pine barrens" />
        <character name="habitat" value="pine-oak forests" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="edges" constraint="of streams , ponds , and swamps" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-325 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="325" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Conn., Del., Maine, Md., Mass., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Northern bayberry</other_name>
  <other_name type="common_name">waxberry</other_name>
  <other_name type="common_name">tallow bayberry</other_name>
  <other_name type="common_name">small waxberry</other_name>
  <other_name type="common_name">tallowshrub</other_name>
  <other_name type="common_name">swamp candleberry</other_name>
  <other_name type="common_name">candlewood</other_name>
  <other_name type="common_name">candletree</other_name>
  <other_name type="common_name">tallowtree</other_name>
  <other_name type="common_name">myrique de Pennsylvanie</other_name>
  <discussion>Where their ranges overlap, Myrica pensylvanica hybridizes quite readily with both M. cerifera and M. heterophylla. This ease of hybridization obviously contributes to an already complicated taxonomic situation; it is a matter for further field-based investigation.</discussion>
  
</bio:treatment>