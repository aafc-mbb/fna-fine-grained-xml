<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">calycanthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">calycanthus</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="1841" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>340, plate 84. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family calycanthaceae;genus calycanthus;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500303</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Butneria</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) Greene" date="unknown" rank="species">occidentalis</taxon_name>
    <taxon_hierarchy>genus Butneria;species occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, to 4 m.</text>
      <biological_entity id="o27517" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Lateral bud exposed.</text>
      <biological_entity constraint="lateral" id="o27518" name="bud" name_original="bud" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="exposed" value_original="exposed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petiole 5-10 mm, pubescent to glabrous.</text>
      <biological_entity id="o27519" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s2" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade ovatelanceolate to oblong-lanceolate or ovate-elliptic, 5-15 × 2-8 cm, base rounded to nearly cordate, apex acute to obtuse;</text>
      <biological_entity id="o27520" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s3" to="oblong-lanceolate or ovate-elliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27521" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="nearly cordate" />
      </biological_entity>
      <biological_entity id="o27522" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially green, pubescent to glabrous.</text>
      <biological_entity id="o27523" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s4" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: hypanthium campanulate or ovoid-campanulate at maturity, 2-4 × 1-2 cm;</text>
      <biological_entity id="o27524" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o27525" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character constraint="at maturity" is_modifier="false" name="shape" src="d0_s5" value="ovoid-campanulate" value_original="ovoid-campanulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals linear to linear-spatulate or ovate-elliptic, 2-6 × 0.5-1 cm, apex rounded;</text>
      <biological_entity id="o27526" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o27527" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="linear-spatulate or ovate-elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27528" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 10-15, linear to oblong-linear.</text>
      <biological_entity id="o27529" name="flower" name_original="flowers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 22.</text>
      <biological_entity id="o27530" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="linear" name="arrangement_or_course_or_shape" src="d0_s7" to="oblong-linear" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27531" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early fall, fruiting mid fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late spring" />
        <character name="fruiting time" char_type="range_value" to="mid fall" from="mid fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along streams and on moist canyon slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="moist canyon slopes" modifier="and on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">California spicebush</other_name>
  <other_name type="common_name">sweetshrub</other_name>
  <discussion>Calycanthus occidentalis grows in the northern Coast Range, the southern Cascades Range, and the western Sierra Nevada. It is ecologically similar to C. floridus; it consistently differs from that taxon in a number of vegetative and floral characteristics. Because of an apparent lack of hardiness, C. occidentalis is cultivated less often than C. floridus.</discussion>
  <discussion>Some American Indians used scraped bark of Calycanthus occidentalis medicinally in treating severe colds (D. E. Moerman 1986).</discussion>
  
</bio:treatment>