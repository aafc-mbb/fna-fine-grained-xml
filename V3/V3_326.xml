<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(Rydberg) N. I. Malyutin" date="1987" rank="subsection">bicoloria</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="species">glareosum</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 257. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection bicoloria;species glareosum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500498</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Ewan" date="unknown" rank="species">caprorum</taxon_name>
    <taxon_hierarchy>genus Delphinium;species caprorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 20-40 cm;</text>
      <biological_entity id="o8500" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base reddish or not, glabrous, glaucous.</text>
      <biological_entity id="o8501" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character name="coloration" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: distribution variable;</text>
      <biological_entity id="o8502" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="variability" src="d0_s2" value="variable" value_original="variable" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 0-4 at anthesis;</text>
      <biological_entity id="o8503" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o8504" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 3-6 at anthesis;</text>
      <biological_entity id="o8505" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o8506" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="3" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1-17 cm.</text>
      <biological_entity id="o8507" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o8508" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round, 2-4 × 3-7 cm, not succulent, glabrous;</text>
      <biological_entity id="o8509" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="round" value_original="round" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s6" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s6" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 7-15, width 3-10 mm (basal), 2-7 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o8510" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 5-12-flowered;</text>
      <biological_entity id="o8511" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-12-flowered" value_original="5-12-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel 1-5 (-10) cm, nearly glabrous to glandular-puberulent;</text>
      <biological_entity id="o8512" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm" />
        <character char_type="range_value" from="nearly glabrous" name="pubescence" src="d0_s9" to="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 2-10 (-20) mm from flowers, green, linear, 3-8 mm, puberulent.</text>
      <biological_entity id="o8513" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o8514" from="2" from_unit="mm" name="location" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o8514" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals dark blue, nearly glabrous, lateral sepals spreading, 14-22 × 6-10 mm, spurs ± straight, orientation varies from 30° above to 30° below horizontal, 16-20 mm;</text>
      <biological_entity id="o8515" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8516" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark blue" value_original="dark blue" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o8517" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s11" to="22" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8518" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character constraint="below horizontal" modifier="0-30°" name="degree" src="d0_s11" value="30°" value_original="30°" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades slightly elevated, ± covering stamens, 6-9 mm, clefts 2-4 mm;</text>
      <biological_entity id="o8519" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o8520" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8521" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o8522" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o8520" id="r1185" modifier="more or less" name="covering" negation="false" src="d0_s12" to="o8521" />
    </statement>
    <statement id="d0_s13">
      <text>hairs local, densest on inner lobes near base of cleft, white to light yellow.</text>
      <biological_entity id="o8523" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8524" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="local" value_original="local" />
        <character constraint="on inner lobes" constraintid="o8525" is_modifier="false" name="density" src="d0_s13" value="densest" value_original="densest" />
        <character char_type="range_value" from="white" modifier="of cleft" name="coloration" notes="" src="d0_s13" to="light yellow" />
      </biological_entity>
      <biological_entity constraint="inner" id="o8525" name="lobe" name_original="lobes" src="d0_s13" type="structure" />
      <biological_entity id="o8526" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o8525" id="r1186" name="near" negation="false" src="d0_s13" to="o8526" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 12-17 mm, 2.5-3 times longer than wide, glabrous.</text>
      <biological_entity id="o8527" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s14" to="17" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="2.5-3" value_original="2.5-3" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds ± wing-margined;</text>
      <biological_entity id="o8528" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s15" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>seed-coat cells with surfaces smooth.</text>
      <biological_entity constraint="seed-coat" id="o8529" name="cell" name_original="cells" src="d0_s16" type="structure" />
      <biological_entity id="o8530" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o8529" id="r1187" name="with" negation="false" src="d0_s16" to="o8530" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steep rocky subalpine to alpine slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="steep rocky subalpine" />
        <character name="habitat" value="alpine slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44.</number>
  <other_name type="common_name">Olympic Mountain larkspur</other_name>
  <discussion>See discussion under Delphinium bicolor.</discussion>
  
</bio:treatment>