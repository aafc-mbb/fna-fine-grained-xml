<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John. H. Wiersema</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">64</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">NELUMBONACEAE</taxon_name>
    <taxon_hierarchy>family NELUMBONACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10608</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, aquatic, rhizomatous;</text>
      <biological_entity id="o16082" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots adventitious;</text>
    </statement>
    <statement id="d0_s2">
      <text>air chambers conspicuous in vegetative portions of plant.</text>
      <biological_entity id="o16083" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="derivation" src="d0_s1" value="adventitious" value_original="adventitious" />
      </biological_entity>
      <biological_entity id="o16084" name="chamber" name_original="chambers" src="d0_s2" type="structure">
        <character constraint="in portions" constraintid="o16085" is_modifier="false" name="prominence" src="d0_s2" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o16085" name="portion" name_original="portions" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o16086" name="plant" name_original="plant" src="d0_s2" type="structure" />
      <relation from="o16085" id="r2219" name="part_of" negation="false" src="d0_s2" to="o16086" />
    </statement>
    <statement id="d0_s3">
      <text>Rhizomes branched, repent, slender, terminal portions becoming tuberous-thickened late in growing season.</text>
      <biological_entity id="o16087" name="rhizome" name_original="rhizomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="growth_form" src="d0_s3" value="repent" value_original="repent" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o16088" name="portion" name_original="portions" src="d0_s3" type="structure">
        <character constraint="in " constraintid="o16089" is_modifier="false" modifier="becoming" name="size_or_width" src="d0_s3" value="tuberous-thickened" value_original="tuberous-thickened" />
      </biological_entity>
      <biological_entity id="o16089" name="season" name_original="season" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves arising directly from rhizome, alternate, floating or emersed;</text>
      <biological_entity id="o16090" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="from rhizome" constraintid="o16091" is_modifier="false" name="orientation" src="d0_s4" value="arising" value_original="arising" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s4" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="location" src="d0_s4" value="floating" value_original="floating" />
        <character is_modifier="false" name="location" src="d0_s4" value="emersed" value_original="emersed" />
      </biological_entity>
      <biological_entity id="o16091" name="rhizome" name_original="rhizome" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole long.</text>
      <biological_entity id="o16092" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade peltate, orbiculate, margins entire;</text>
      <biological_entity id="o16093" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="peltate" value_original="peltate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="orbiculate" value_original="orbiculate" />
      </biological_entity>
      <biological_entity id="o16094" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laticifers present.</text>
      <biological_entity id="o16095" name="laticifer" name_original="laticifers" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences axillary, solitary flowers.</text>
      <biological_entity id="o16096" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o16097" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual, protogynous, diurnal, borne above water surface;</text>
      <biological_entity id="o16098" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="protogynous" value_original="protogynous" />
        <character is_modifier="false" name="duration" src="d0_s9" value="diurnal" value_original="diurnal" />
      </biological_entity>
      <biological_entity id="o16099" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s9" value="water" value_original="water" />
      </biological_entity>
      <relation from="o16098" id="r2220" name="borne above" negation="false" src="d0_s9" to="o16099" />
    </statement>
    <statement id="d0_s10">
      <text>peduncle long;</text>
      <biological_entity id="o16100" name="peduncle" name_original="peduncle" src="d0_s10" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>involucre absent;</text>
      <biological_entity id="o16101" name="involucre" name_original="involucre" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth not persistent in fruit (outermost tepals of N. lutea normally persistent), hypogynous;</text>
      <biological_entity id="o16102" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character constraint="in fruit" constraintid="o16103" is_modifier="false" modifier="not" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="position" notes="" src="d0_s12" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o16103" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>tepals numerous, distinct, outermost reduced, inner tepals becoming larger and more petaloid;</text>
      <biological_entity id="o16104" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o16105" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="inner" id="o16106" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="becoming" name="size" src="d0_s13" value="larger" value_original="larger" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="petaloid" value_original="petaloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens numerous;</text>
      <biological_entity id="o16107" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="numerous" value_original="numerous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments slender;</text>
      <biological_entity id="o16108" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers dehiscing by longitudinal slits, connective appendage incurved;</text>
      <biological_entity id="o16109" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character constraint="by slits" constraintid="o16110" is_modifier="false" name="dehiscence" src="d0_s16" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o16110" name="slit" name_original="slits" src="d0_s16" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s16" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity constraint="connective" id="o16111" name="appendage" name_original="appendage" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pistils numerous, 1-carpellate, separately embedded in flattened top of turbinate receptacle;</text>
      <biological_entity id="o16112" name="pistil" name_original="pistils" src="d0_s17" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s17" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="1-carpellate" value_original="1-carpellate" />
      </biological_entity>
      <biological_entity id="o16113" name="top" name_original="top" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o16114" name="receptacle" name_original="receptacle" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="turbinate" value_original="turbinate" />
        <character is_modifier="true" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o16112" id="r2221" modifier="separately" name="embedded in" negation="false" src="d0_s17" to="o16113" />
      <relation from="o16112" id="r2222" modifier="separately" name="embedded in" negation="false" src="d0_s17" to="o16114" />
    </statement>
    <statement id="d0_s18">
      <text>ovary 1-locular;</text>
    </statement>
    <statement id="d0_s19">
      <text>placentation apical;</text>
      <biological_entity id="o16115" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s18" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="placentation" src="d0_s19" value="apical" value_original="apical" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovule 1;</text>
      <biological_entity id="o16116" name="ovule" name_original="ovule" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>style short;</text>
      <biological_entity id="o16117" name="style" name_original="style" src="d0_s21" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s21" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma nearly sessile, capitate.</text>
      <biological_entity id="o16118" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s22" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Fruits nutlike, indehiscent, loose in cavities of strongly accrescent receptacle.</text>
      <biological_entity id="o16119" name="fruit" name_original="fruits" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="nutlike" value_original="nutlike" />
        <character is_modifier="false" name="dehiscence" src="d0_s23" value="indehiscent" value_original="indehiscent" />
        <character constraint="in cavities" constraintid="o16120" is_modifier="false" name="architecture_or_fragility" src="d0_s23" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o16120" name="cavity" name_original="cavities" src="d0_s23" type="structure" />
      <biological_entity id="o16121" name="receptacle" name_original="receptacle" src="d0_s23" type="structure">
        <character is_modifier="true" modifier="strongly" name="size" src="d0_s23" value="accrescent" value_original="accrescent" />
      </biological_entity>
      <relation from="o16120" id="r2223" name="part_of" negation="false" src="d0_s23" to="o16121" />
    </statement>
    <statement id="d0_s24">
      <text>Seed 1, aril absent;</text>
      <biological_entity id="o16122" name="seed" name_original="seed" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16123" name="aril" name_original="aril" src="d0_s24" type="structure">
        <character is_modifier="false" name="presence" src="d0_s24" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>endosperm and perisperm absent;</text>
      <biological_entity id="o16124" name="endosperm" name_original="endosperm" src="d0_s25" type="structure">
        <character is_modifier="false" name="presence" src="d0_s25" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o16125" name="perisperm" name_original="perisperm" src="d0_s25" type="structure">
        <character is_modifier="false" name="presence" src="d0_s25" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>embryo completely filling seed;</text>
      <biological_entity id="o16126" name="embryo" name_original="embryo" src="d0_s26" type="structure" />
      <biological_entity id="o16127" name="seed" name_original="seed" src="d0_s26" type="structure" />
      <relation from="o16126" id="r2224" name="filling" negation="false" src="d0_s26" to="o16127" />
    </statement>
    <statement id="d0_s27">
      <text>cotyledons 2, fleshy.</text>
      <biological_entity id="o16128" name="cotyledon" name_original="cotyledons" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s27" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate and tropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate and tropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Lotus-lily family</other_name>
  <discussion>Genus 1, species 2 (2 in the flora).</discussion>
  <discussion>Nelumbonaceae are pollinated by insects, often by beetles.</discussion>
  <discussion>Formerly Nelumbonaceae frequently have been included in Nymphaeaceae in the broad sense.</discussion>
  <references>
    <reference>Wood, C. E. Jr. 1959. The genera of the Nymphaeaceae and Ceratophyllaceae in the southeastern United States. J. Arnold Arbor. 40: 94-112.</reference>
  </references>
  
</bio:treatment>