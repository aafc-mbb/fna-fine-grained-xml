<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">nymphaeaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">nymphaea</taxon_name>
    <taxon_name authority="(Salisbury) de Candolle" date="1821" rank="species">ampla</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 54. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nymphaeaceae;genus nymphaea;species ampla</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="fna_id">233500822</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castalia</taxon_name>
    <taxon_name authority="Salisbury" date="unknown" rank="species">ampla</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. (König &amp; Sims)</publication_title>
      <place_in_publication>2: 73. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Castalia;species ampla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes unbranched, erect, ovoid;</text>
      <biological_entity id="o9161" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stolons absent.</text>
      <biological_entity id="o9162" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole glabrous.</text>
      <biological_entity id="o9163" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o9164" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade abaxially purple, often spotted, adaxially green, ovate to nearly orbiculate, 15-45 × 15-45 cm, margins dentate to spinose-dentate;</text>
      <biological_entity id="o9165" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration_or_density" src="d0_s3" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s3" value="spotted" value_original="spotted" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="nearly orbiculate" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s3" to="45" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s3" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>venation radiate and prominent centrally, without weblike pattern, principal veins 13-29;</text>
      <biological_entity id="o9166" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="dentate" name="architecture_or_shape" src="d0_s3" to="spinose-dentate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o9167" name="pattern" name_original="pattern" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="centrally" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="weblike" value_original="weblike" />
      </biological_entity>
      <biological_entity constraint="principal" id="o9168" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s4" to="29" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>surfaces glabrous.</text>
      <biological_entity id="o9169" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers emersed, 7-18 cm diam., opening and closing diurnally, only sepals and outermost petals in distinct whorls of 4;</text>
      <biological_entity id="o9170" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="location" src="d0_s6" value="emersed" value_original="emersed" />
        <character char_type="range_value" from="7" from_unit="cm" name="diameter" src="d0_s6" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9171" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="true" name="condition" src="d0_s6" value="closing" value_original="closing" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o9172" name="petal" name_original="petals" src="d0_s6" type="structure" />
      <biological_entity id="o9173" name="whorl" name_original="whorls" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <relation from="o9170" id="r1291" name="opening" negation="false" src="d0_s6" to="o9171" />
      <relation from="o9172" id="r1292" name="in" negation="false" src="d0_s6" to="o9173" />
    </statement>
    <statement id="d0_s7">
      <text>sepals green, abaxially flecked with short dark streaks, faintly veined, lines of insertion on receptacle not prominent;</text>
      <biological_entity id="o9174" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character constraint="with streaks" constraintid="o9175" is_modifier="false" modifier="abaxially" name="coloration" src="d0_s7" value="flecked" value_original="flecked" />
        <character is_modifier="false" modifier="faintly" name="architecture" notes="" src="d0_s7" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o9175" name="streak" name_original="streaks" src="d0_s7" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o9176" name="line" name_original="lines" src="d0_s7" type="structure" />
      <biological_entity id="o9177" name="receptacle" name_original="receptacle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o9176" id="r1293" name="on" negation="false" src="d0_s7" to="o9177" />
    </statement>
    <statement id="d0_s8">
      <text>petals 12-21, white;</text>
      <biological_entity id="o9178" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="21" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 50-190, yellow, outer with connective appendage projecting 3-10 mm beyond anther;</text>
      <biological_entity id="o9179" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s9" to="190" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="outer" id="o9180" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="beyond anther" constraintid="o9182" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="connective" id="o9181" name="appendage" name_original="appendage" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o9182" name="anther" name_original="anther" src="d0_s9" type="structure" />
      <relation from="o9180" id="r1294" name="with" negation="false" src="d0_s9" to="o9181" />
    </statement>
    <statement id="d0_s10">
      <text>filaments widest at or below middle, mostly equal to or shorter than anthers;</text>
      <biological_entity id="o9183" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character constraint="at or below middle" is_modifier="false" name="width" src="d0_s10" value="widest" value_original="widest" />
        <character is_modifier="false" modifier="mostly" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character constraint="than anthers" constraintid="o9184" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9184" name="anther" name_original="anthers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pistil 14-25-locular, appendages at margin of stigmatic disk short-triangular, to 3 mm.</text>
      <biological_entity id="o9185" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="14-25-locular" value_original="14-25-locular" />
      </biological_entity>
      <biological_entity id="o9186" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9187" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="short-triangular" value_original="short-triangular" />
      </biological_entity>
      <biological_entity id="o9188" name="stigmatic" name_original="stigmatic" src="d0_s11" type="structure" />
      <biological_entity id="o9189" name="disk" name_original="disk" src="d0_s11" type="structure" />
      <relation from="o9186" id="r1295" name="at" negation="false" src="d0_s11" to="o9187" />
      <relation from="o9187" id="r1296" name="part_of" negation="false" src="d0_s11" to="o9188" />
      <relation from="o9187" id="r1297" name="part_of" negation="false" src="d0_s11" to="o9189" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds nearly globose to ellipsoid, 1.2-1.6 × 0.9-1.3 mm, 1.2-1.5 times as long as broad, with longitudinal rows of hairlike papillae 40-180 µm.</text>
      <biological_entity id="o9190" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="nearly globose" name="shape" src="d0_s12" to="ellipsoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s12" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s12" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="length_or_width" src="d0_s12" value="1.2-1.5 times as long as broad" />
      </biological_entity>
      <biological_entity id="o9191" name="row" name_original="rows" src="d0_s12" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s12" value="longitudinal" value_original="longitudinal" />
        <character char_type="range_value" from="40" from_unit="um" name="some_measurement" src="d0_s12" to="180" to_unit="um" />
      </biological_entity>
      <biological_entity id="o9192" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <relation from="o9190" id="r1298" name="with" negation="false" src="d0_s12" to="o9191" />
      <relation from="o9191" id="r1299" name="part_of" negation="false" src="d0_s12" to="o9192" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering all year.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ditches, canals, ponds, and freshwater tidal margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ditches" />
        <character name="habitat" value="canals" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="freshwater tidal margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-350 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="350" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Tex.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Reports of Nymphaea ampla in southern Texas by H. S. Conard (1905) are confirmed by specimens although no recent collections have been seen.</discussion>
  <references>
    <reference>Wunderlin, R. P. and D. H. Les. 1980. Nymphaea ampla (Nymphaeaceae), a waterlily new to Florida. Phytologia 45: 82-84.</reference>
  </references>
  
</bio:treatment>