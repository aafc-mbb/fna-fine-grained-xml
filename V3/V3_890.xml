<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>D. E. Brink, J. A. Woods</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ACONITUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 532. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 236. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ACONITUM</taxon_hierarchy>
    <other_info_on_name type="etymology">according to Pliny, the name "aconite" is taken from the ancient Black Sea port Aconis</other_info_on_name>
    <other_info_on_name type="fna_id">100300</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from tubers or elongate, fascicled roots.</text>
      <biological_entity id="o11612" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o11613" name="tuber" name_original="tubers" src="d0_s0" type="structure" />
      <biological_entity id="o11614" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="fascicled" value_original="fascicled" />
      </biological_entity>
      <relation from="o11612" id="r1618" name="from" negation="false" src="d0_s0" to="o11613" />
      <relation from="o11612" id="r1619" name="from" negation="false" src="d0_s0" to="o11614" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, proximal leaves petiolate, distal leaves sessile or nearly so;</text>
      <biological_entity id="o11615" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11616" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11617" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s1" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves alternate.</text>
      <biological_entity constraint="cauline" id="o11618" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade palmately divided into 3-7 segments, ultimate segments narrowly elliptic or lanceolate to linear, margins incised and toothed.</text>
      <biological_entity id="o11619" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character constraint="into segments" constraintid="o11620" is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o11620" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o11621" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o11622" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="incised" value_original="incised" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, sometimes also axillary, 1-32 (-more) racemes or panicles, to 28 cm;</text>
      <biological_entity id="o11623" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="32" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="28" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11624" name="raceme" name_original="racemes" src="d0_s4" type="structure" />
      <biological_entity id="o11625" name="panicle" name_original="panicles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracts leaflike, not forming involucre.</text>
      <biological_entity id="o11626" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o11627" name="involucre" name_original="involucre" src="d0_s5" type="structure" />
      <relation from="o11626" id="r1620" name="forming" negation="true" src="d0_s5" to="o11627" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual, bilaterally symmetric;</text>
      <biological_entity id="o11628" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals not persistent in fruit;</text>
      <biological_entity id="o11629" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o11630" is_modifier="false" modifier="not" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o11630" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>lower sepals (pendents) 2, plane, 6-20 mm;</text>
      <biological_entity constraint="lower" id="o11631" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral sepals 2, round-reniform;</text>
      <biological_entity constraint="lateral" id="o11632" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s9" value="round-reniform" value_original="round-reniform" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper sepal (hood) 1, saccate, arched, crescent-shaped or hemispheric to rounded-conic or tall and cylindric, usually beaked, 10-50 mm;</text>
      <biological_entity constraint="upper" id="o11633" name="sepal" name_original="sepal" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s10" value="saccate" value_original="saccate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="arched" value_original="arched" />
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s10" to="rounded-conic" />
        <character is_modifier="false" name="height" src="d0_s10" value="tall" value_original="tall" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s10" value="beaked" value_original="beaked" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 2, distinct, bearing near apex a capitate to coiled spur, concealed in hood, long-clawed;</text>
      <biological_entity id="o11634" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="capitate" name="architecture" src="d0_s11" to="coiled" />
        <character constraint="in hood" constraintid="o11637" is_modifier="false" name="prominence" notes="" src="d0_s11" value="concealed" value_original="concealed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="long-clawed" value_original="long-clawed" />
      </biological_entity>
      <biological_entity id="o11635" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity id="o11636" name="spur" name_original="spur" src="d0_s11" type="structure" />
      <biological_entity id="o11637" name="hood" name_original="hood" src="d0_s11" type="structure" />
      <relation from="o11634" id="r1621" name="bearing near" negation="false" src="d0_s11" to="o11635" />
    </statement>
    <statement id="d0_s12">
      <text>nectary present, on spur;</text>
      <biological_entity id="o11638" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11639" name="spur" name_original="spur" src="d0_s12" type="structure" />
      <relation from="o11638" id="r1622" name="on" negation="false" src="d0_s12" to="o11639" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 25-50;</text>
      <biological_entity id="o11640" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s13" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments with base expanded;</text>
      <biological_entity id="o11641" name="filament" name_original="filaments" src="d0_s14" type="structure" />
      <biological_entity id="o11642" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="expanded" value_original="expanded" />
      </biological_entity>
      <relation from="o11641" id="r1623" name="with" negation="false" src="d0_s14" to="o11642" />
    </statement>
    <statement id="d0_s15">
      <text>staminodes absent between stamens and pistils;</text>
      <biological_entity id="o11643" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character constraint="between stamens, pistils" constraintid="o11644, o11645" is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11644" name="stamen" name_original="stamens" src="d0_s15" type="structure" />
      <biological_entity id="o11645" name="pistil" name_original="pistils" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>pistils 3 (-5), simple;</text>
      <biological_entity id="o11646" name="pistil" name_original="pistils" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="5" />
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 10-20 per pistil;</text>
      <biological_entity id="o11647" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per pistil" constraintid="o11648" from="10" name="quantity" src="d0_s17" to="20" />
      </biological_entity>
      <biological_entity id="o11648" name="pistil" name_original="pistil" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style present.</text>
      <biological_entity id="o11649" name="style" name_original="style" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits follicles, aggregate, sessile, oblong, sides prominently transversely veined;</text>
      <biological_entity constraint="fruits" id="o11650" name="follicle" name_original="follicles" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="aggregate" value_original="aggregate" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o11651" name="side" name_original="sides" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="prominently transversely" name="architecture" src="d0_s19" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>beak terminal, straight, 2-3 mm.</text>
      <biological_entity id="o11652" name="beak" name_original="beak" src="d0_s20" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s20" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="course" src="d0_s20" value="straight" value_original="straight" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s20" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds deltoid, usually with small, transverse, membranous lamellae.</text>
      <biological_entity id="o11654" name="lamella" name_original="lamellae" src="d0_s21" type="structure">
        <character is_modifier="true" name="size" src="d0_s21" value="small" value_original="small" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s21" value="transverse" value_original="transverse" />
        <character is_modifier="true" name="texture" src="d0_s21" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o11653" id="r1624" modifier="usually" name="with" negation="false" src="d0_s21" to="o11654" />
    </statement>
    <statement id="d0_s22">
      <text>x=8.</text>
      <biological_entity id="o11653" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="deltoid" value_original="deltoid" />
      </biological_entity>
      <biological_entity constraint="x" id="o11655" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Circumboreal, southward into n Mexico and n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Circumboreal" establishment_means="native" />
        <character name="distribution" value="southward into n Mexico and n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Monkshood</other_name>
  <other_name type="common_name">aconite</other_name>
  <other_name type="common_name">wolfsbane</other_name>
  <other_name type="common_name">aconit</other_name>
  <discussion>Species ca. 100 (5 in the flora).</discussion>
  <discussion>The greatest concentration of species of Aconitum is in Asia, with a smaller group in Europe.</discussion>
  <discussion>Aconitum is phylogenetically most closely related to Delphinium Linnaeus as evidenced by similarities in karyotype, production of diterpene alkaloids, and similarities in floral morphology. Distinctive and unique floral morphology clearly distinguishes Aconitum from all other genera.</discussion>
  <discussion>The aconites have been of interest since ancient times because they contain diterpene alkaloids that range from relatively nontoxic to deadly poisonous. In various parts of the world they have been used medicinally and as a source of poisons throughout history (D. E. Brink 1982). Use of Aconitum alkaloids in modern medicine was largely discontinued by the late 1930s and early 1940s (E. E. Swanson et al. 1938; H. C. Wood and A. Osol 1943; A. Osol et al. 1960).</discussion>
  <discussion>Aconitum is a circumboreal arctic and alpine genus that extends into lower latitudes where there is suitable mesic habitat at high elevations along the north-south chains of mountains in eastern and western North America, and also in outlying, scattered, mesic, interglacial refugia, occasionally at low elevations.</discussion>
  <discussion>The genus Aconitum worldwide is notorious for complex patterns of morphologic intergradation that blur the lines between taxa. Aconites from different regions may be morphologically distinct but connected by a series of intermediate races. Aconitum columbianum exemplifies this in North America, and A. delphiniifolium may extend this complex of variation into Asia. Intergradation between A. columbianum and A. delphiniifolium should be more fully investigated.</discussion>
  <discussion>Cultivated aconites with origins outside North America sometimes persist in old gardens or are encountered as garden escapes, especially in eastern Canada (New Brunswick, Newfoundland, Nova Scotia, Ontario, and Quebec). These may include Aconitum lycoctonum Linnaeus, A. napellus Linnaeus, A. variegatum Linnaeus, and A.bicolor Schultes. Aconitum lycoctonum is similar to A. reclinatum of the southeastern United States in having the tall, conic-cylindric hood that is characterisitc of species in Aconitum sect. Lycoctonum de Candolle. Aconitum reclinatum has white flowers whereas A. lycoctonum has lilac-purple flowers.</discussion>
  <discussion>Aconitum napellus and A. variegatum are European introductions with leaves divided to the base as in A. delphiniifolium, which is native to Canada and Alaska. The introduced species have taller hoods and relatively short-petiolate leaves compared to A. delphiniifolium; in A. delphiniifolium petioles are longer, i.e., mostly as long as blades. Aconitum napellus has smooth seeds that lack the undulating membranous lamellae present in A. delpinifolium and A. variegatum.</discussion>
  <discussion>Aconitum bicolor is a reputed hybrid between A. napellus and A. variegatum, having leaves like the former and flowers similar to the latter. It is always sterile; seeds are not viable. Flowers of A. bicolor are frequently white with purple margins.</discussion>
  <discussion>A more complete treatment of the cultivated aconites likely to be encountered in North America can be found in H. J. Scoggan (1978-1979, part 3, pp. 718-720) and P. A. Munz (1945).</discussion>
  <references>
    <reference>Brink, D. E. 1982. Tuberous Aconitum (Ranunculaceae) of the continental United States: Morphological variation, taxonomy and disjunction. Bull. Torrey Bot. Club 109: 13-23.</reference>
    <reference>Brink, D. E., J. A. Woods, and K. R. Stern. 1994. Bulbiferous Aconitum (Ranunculaceae) of the western United States. Sida 16: 9-15.</reference>
    <reference>Hardin, J. W. 1964. Variation in Aconitum of eastern United States. Brittonia 16: 80-94.</reference>
    <reference>Kadota, Y. 1987. A Revision of Aconitum subgenus Aconitum (Ranunculaceae) of East Asia. Utsunomiya.</reference>
    <reference>Shteinberg, E. I. 1970. AconitumL. In: V. L. Komarov et al., eds. 1963+. Flora of the U.S.S.R. (Flora SSSR). Translated from Russian. 22+ vols. Jerusalem. Vol. 7, pp. 143-184.</reference>
    <reference>Munz, P. A. 1945. The cultivated aconites. Gentes Herb. 6: 462-505.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Aerial stem arising from elongate, fascicled roots; hoods very tall, conic or nearly cylindric, flowers white to cream colored; e United States.</description>
      <determination>1 Aconitum reclinatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Aerial stem arising from tuber; hoods low, conic-hemispheric to crescent-shaped, flowers commonly blue to purple, occasionally white or yellowish; w Canada, e,w United States (including Alaska).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaf blades deeply divided to very base or with at most 2(–4) mm tissue between base and deepest sinus; hood low, crescent-shaped to conic-hemispheric; w Canada, Alaska.</description>
      <determination>2 Aconitum delphiniifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaf blades usually not divided to the very base, usually more than 2 mm between base and deepest sinus; hood conic-hemispheric, occasionally crescent-shaped; widespread.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Tuber distally bulblike; terminal inflorescence often contracted, capitate; Alaska (Alaska Peninsula, and Aleutian Islands).</description>
      <determination>3 Aconitum maximum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Tuber distally not obviously bulblike; inflorescence elongate, not capitate; sw Canada, United States except Alaska.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Parent and daughter tubers separated by connecting rhizome 5–30 mm; eUnited States excluding Iowa, New York, and Wisconsin.</description>
      <determination>4 Aconitum uncinatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Parent and daughter tubers contiguous or nearly so; sw Canada, w United States, and Iowa, New York, Ohio, and Wisconsin.</description>
      <determination>5 Aconitum columbianum</determination>
    </key_statement>
  </key>
</bio:treatment>