<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">betula</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">lenta</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 983. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus betula;species lenta;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500250</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 20 m;</text>
      <biological_entity id="o21571" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks tall, straight, crowns narrow.</text>
      <biological_entity id="o21572" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="false" name="height" src="d0_s1" value="tall" value_original="tall" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o21573" name="crown" name_original="crowns" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bark of mature trunks and branches light grayish brown to dark-brown or nearly black, smooth, close, furrowed and broken into shallow scales with age.</text>
      <biological_entity id="o21574" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="light grayish" value_original="light grayish" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s2" to="dark-brown or nearly black" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="close" value_original="close" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="furrowed" value_original="furrowed" />
        <character constraint="into scales" constraintid="o21577" is_modifier="false" name="condition_or_fragility" src="d0_s2" value="broken" value_original="broken" />
      </biological_entity>
      <biological_entity id="o21575" name="trunk" name_original="trunks" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o21576" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o21577" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="true" name="depth" src="d0_s2" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o21578" name="age" name_original="age" src="d0_s2" type="structure" />
      <relation from="o21574" id="r2931" name="part_of" negation="false" src="d0_s2" to="o21575" />
      <relation from="o21574" id="r2932" name="part_of" negation="false" src="d0_s2" to="o21576" />
      <relation from="o21577" id="r2933" name="with" negation="false" src="d0_s2" to="o21578" />
    </statement>
    <statement id="d0_s3">
      <text>Twigs with taste and odor of wintergreen when crushed, glabrous to sparsely pubescent, usually covered with small resinous glands.</text>
      <biological_entity id="o21579" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrous" modifier="when crushed" name="odor" src="d0_s3" to="sparsely pubescent" />
      </biological_entity>
      <biological_entity id="o21580" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
        <character is_modifier="true" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o21579" id="r2934" modifier="usually" name="covered with" negation="false" src="d0_s3" to="o21580" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade ovate to oblong-ovate with 12–18 pairs of lateral-veins, 5–10 × 3–6 cm, base rounded to cordate, margins finely and sharply serrate or obscurely doubly serrate, teeth fine, sharp, apex acuminate;</text>
      <biological_entity id="o21581" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="with pairs" constraintid="o21582" from="ovate" name="shape" src="d0_s4" to="oblong-ovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" notes="" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21582" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s4" to="18" />
      </biological_entity>
      <biological_entity id="o21583" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure" />
      <biological_entity id="o21584" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="cordate" />
      </biological_entity>
      <biological_entity id="o21585" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="obscurely doubly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o21586" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="fine" value_original="fine" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sharp" value_original="sharp" />
      </biological_entity>
      <biological_entity id="o21587" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o21582" id="r2935" name="part_of" negation="false" src="d0_s4" to="o21583" />
    </statement>
    <statement id="d0_s5">
      <text>surfaces abaxially mostly glabrous, except sparsely pubescent along major veins and in vein-axils, often with scattered, minute, resinous glands.</text>
      <biological_entity id="o21588" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially mostly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21589" name="vein-axil" name_original="vein-axils" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21590" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="size" src="d0_s5" value="minute" value_original="minute" />
        <character is_modifier="true" name="coating" src="d0_s5" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o21588" id="r2936" name="except" negation="false" src="d0_s5" to="o21589" />
      <relation from="o21588" id="r2937" modifier="often" name="with" negation="false" src="d0_s5" to="o21590" />
    </statement>
    <statement id="d0_s6">
      <text>Infructescences erect, ovoid to nearly globose, 1.5–4 × 1.5–2.5 cm, usually remaining intact for a period after release of fruits in fall;</text>
      <biological_entity id="o21591" name="infructescence" name_original="infructescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="nearly globose" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
        <character constraint="for period" constraintid="o21592" is_modifier="false" modifier="usually" name="condition" src="d0_s6" value="intact" value_original="intact" />
      </biological_entity>
      <biological_entity id="o21592" name="period" name_original="period" src="d0_s6" type="structure" />
      <biological_entity id="o21593" name="release" name_original="release" src="d0_s6" type="structure" />
      <biological_entity id="o21594" name="fruit" name_original="fruits" src="d0_s6" type="structure" />
      <biological_entity id="o21595" name="fall" name_original="fall" src="d0_s6" type="structure" />
      <relation from="o21592" id="r2938" name="after" negation="false" src="d0_s6" to="o21593" />
      <relation from="o21593" id="r2939" name="part_of" negation="false" src="d0_s6" to="o21594" />
      <relation from="o21593" id="r2940" name="in" negation="false" src="d0_s6" to="o21595" />
    </statement>
    <statement id="d0_s7">
      <text>scales mostly glabrous, lobes diverging at or proximal to middle, central lobe short, cuneate, lateral lobes extended to slightly ascending, longer and broader than central lobe.</text>
      <biological_entity id="o21596" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21597" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
        <character char_type="range_value" from="proximal" name="position" src="d0_s7" to="middle" />
      </biological_entity>
      <biological_entity constraint="central" id="o21598" name="lobe" name_original="lobe" src="d0_s7" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21599" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="extended" value_original="extended" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="than central lobe" constraintid="o21600" is_modifier="false" name="width" src="d0_s7" value="longer and broader" value_original="longer and broader" />
      </biological_entity>
      <biological_entity constraint="central" id="o21600" name="lobe" name_original="lobe" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Samaras with wings narrower than body, broadest near center, not extended beyond body apically.</text>
      <biological_entity id="o21602" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character constraint="than body" constraintid="o21603" is_modifier="false" name="width" src="d0_s8" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o21603" name="body" name_original="body" src="d0_s8" type="structure" />
      <biological_entity id="o21604" name="center" name_original="center" src="d0_s8" type="structure" />
      <biological_entity id="o21605" name="body" name_original="body" src="d0_s8" type="structure" />
      <relation from="o21601" id="r2941" name="with" negation="false" src="d0_s8" to="o21602" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 28.</text>
      <biological_entity id="o21601" name="samara" name_original="samaras" src="d0_s8" type="structure">
        <character constraint="than body" constraintid="o21603" is_modifier="false" name="width" src="d0_s8" value="narrower" value_original="narrower" />
        <character constraint="near center" constraintid="o21604" is_modifier="false" name="width" src="d0_s8" value="broadest" value_original="broadest" />
        <character constraint="beyond body" constraintid="o21605" is_modifier="false" modifier="not" name="size" notes="" src="d0_s8" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21606" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich, moist, cool forests, especially on protected slopes, to rockier, more exposed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cool forests" modifier="rich moist" />
        <character name="habitat" value="protected slopes" modifier="especially" />
        <character name="habitat" value="rockier" />
        <character name="habitat" value="exposed sites" modifier="more" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Conn., Ga., Ky., Maine, Md., Mass., Miss., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Sweet birch</other_name>
  <other_name type="common_name">cherry birch</other_name>
  <discussion>Betula lenta is a dominant tree in the northern hardwood forests of the northern Appalachians and a valuable source of timber. It was formerly the chief commercial source of wintergreen oil (methyl salicylate), which is distilled from its wood. Betula lenta is most easily separated from B. alleghaniensis by its close bark and the glabrous scales of infructescences.</discussion>
  <discussion>Native Americans used Betula lenta medicinally to treat dysentery, colds, diarrhea, fevers, soreness, and milky urine, and as a spring tonic.</discussion>
  
</bio:treatment>