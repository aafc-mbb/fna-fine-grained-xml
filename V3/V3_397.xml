<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="Linneaus" date="unknown" rank="section">quercus</taxon_name>
    <taxon_name authority="Michaux" date="1801" rank="species">macrocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Chênes Amér., plates</publication_title>
      <place_in_publication>2, 3. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section quercus;species macrocarpa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501058</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">macrocarpa</taxon_name>
    <taxon_name authority="(Nuttall) Engelmann" date="unknown" rank="variety">depressa</taxon_name>
    <taxon_hierarchy>genus Quercus;species macrocarpa;variety depressa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">mandanensis</taxon_name>
    <taxon_hierarchy>genus Quercus;species mandanensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, deciduous, to 30 (-50) m.</text>
      <biological_entity id="o6439" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="30" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark dark gray, scaly or flat-ridged.</text>
      <biological_entity id="o6440" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark gray" value_original="dark gray" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="shape" src="d0_s1" value="flat-ridged" value_original="flat-ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs grayish or reddish, 2-4 mm diam., often forming extensive flat, radiating, corky wings, finely pubescent.</text>
      <biological_entity id="o6441" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="often; finely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o6442" name="wing" name_original="wings" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="extensive" value_original="extensive" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="true" name="arrangement" src="d0_s2" value="radiating" value_original="radiating" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s2" value="corky" value_original="corky" />
      </biological_entity>
      <relation from="o6441" id="r890" modifier="often" name="forming" negation="false" src="d0_s2" to="o6442" />
    </statement>
    <statement id="d0_s3">
      <text>Buds 2-5 (-6) mm, glabrous.</text>
      <biological_entity id="o6443" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole (6-) 15-25 (-30) mm.</text>
      <biological_entity id="o6444" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o6445" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade obovate to narrowly elliptic or narrowly obovate, often fiddle-shaped, (50-) 70-150 (-310) × (40-) 50-130 (-160) mm, base rounded to cuneate, margins moderately to deeply lobed, toothed, deepest sinuses near midleaf (at least in proximal 2/3), sinuses reaching nearly to midrib, longer lobes grading into shallow lobes or merely simple teeth distally, shallower, compound lobes proximally, secondary-veins arched, divergent, 4-5 (-10) on each side, apex broadly rounded or ovate;</text>
      <biological_entity id="o6446" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="narrowly elliptic or narrowly obovate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="fiddle--shaped" value_original="fiddle--shaped" />
        <character char_type="range_value" from="50" from_unit="mm" name="atypical_length" src="d0_s5" to="70" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="310" to_unit="mm" />
        <character char_type="range_value" from="70" from_unit="mm" name="length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="atypical_width" src="d0_s5" to="50" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="130" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="160" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="width" src="d0_s5" to="130" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6447" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o6448" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately to deeply" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="deepest" id="o6449" name="sinuse" name_original="sinuses" src="d0_s5" type="structure" />
      <biological_entity id="o6450" name="midleaf" name_original="midleaf" src="d0_s5" type="structure" />
      <biological_entity id="o6451" name="sinuse" name_original="sinuses" src="d0_s5" type="structure">
        <character constraint="to midrib" constraintid="o6452" is_modifier="false" name="position_relational" src="d0_s5" value="reaching" value_original="reaching" />
      </biological_entity>
      <biological_entity id="o6452" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity constraint="longer" id="o6453" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s5" value="shallower" value_original="shallower" />
      </biological_entity>
      <biological_entity id="o6454" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="true" name="depth" src="d0_s5" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o6455" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="merely" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o6456" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o6457" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s5" value="arched" value_original="arched" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="10" />
        <character char_type="range_value" constraint="on side" constraintid="o6458" from="4" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o6458" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o6459" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <relation from="o6449" id="r891" name="near" negation="false" src="d0_s5" to="o6450" />
      <relation from="o6453" id="r892" name="into" negation="false" src="d0_s5" to="o6454" />
      <relation from="o6453" id="r893" name="into" negation="false" src="d0_s5" to="o6455" />
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially light green or whitish, with minute appressed-stellate hairs forming dense, rarely sparse, tomentum, erect felty hairs absent, adaxially dark green or dull gray, sparsely puberulent to glabrate.</text>
      <biological_entity id="o6460" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s6" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o6461" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="minute" value_original="minute" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s6" value="appressed-stellate" value_original="appressed-stellate" />
      </biological_entity>
      <biological_entity id="o6462" name="tomentum" name_original="tomentum" src="d0_s6" type="structure">
        <character is_modifier="true" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character is_modifier="true" modifier="rarely" name="count_or_density" src="d0_s6" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o6463" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="true" name="pubescence" src="d0_s6" value="felty" value_original="felty" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="gray" value_original="gray" />
        <character char_type="range_value" from="sparsely puberulent" name="pubescence" src="d0_s6" to="glabrate" />
      </biological_entity>
      <relation from="o6460" id="r894" name="with" negation="false" src="d0_s6" to="o6461" />
      <relation from="o6461" id="r895" name="forming" negation="false" src="d0_s6" to="o6462" />
    </statement>
    <statement id="d0_s7">
      <text>Acorns 1-3 on stout peduncle (0-) 6-20 (-25) mm;</text>
      <biological_entity id="o6464" name="acorn" name_original="acorns" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="on peduncle" constraintid="o6465" from="1" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o6465" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s7" value="stout" value_original="stout" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cup hemispheric or turbinate, (8-) 15-50 mm deep × (10-) 20-60 mm wide, enclosing 1/2-7/8 nut or more, scales closely appressed, laterally connate, broadly triangular, keeled, tuberculate, finely grayish tomentose, those near margins often with soft awns to 5-10 mm or more, forming fringe around nut;</text>
      <biological_entity id="o6466" name="cup" name_original="cup" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s8" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s8" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s8" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s8" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6467" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character char_type="range_value" from="1/2" is_modifier="true" name="quantity" src="d0_s8" to="7/8" />
      </biological_entity>
      <biological_entity id="o6468" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="laterally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="relief" src="d0_s8" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="finely" name="coloration" src="d0_s8" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o6469" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <biological_entity id="o6470" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s8" value="soft" value_original="soft" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6471" name="fringe" name_original="fringe" src="d0_s8" type="structure" />
      <biological_entity id="o6472" name="nut" name_original="nut" src="d0_s8" type="structure" />
      <relation from="o6466" id="r896" name="enclosing" negation="false" src="d0_s8" to="o6467" />
      <relation from="o6468" id="r897" name="near" negation="false" src="d0_s8" to="o6469" />
      <relation from="o6469" id="r898" name="with" negation="false" src="d0_s8" to="o6470" />
      <relation from="o6468" id="r899" name="forming" negation="false" src="d0_s8" to="o6471" />
      <relation from="o6468" id="r900" name="around" negation="false" src="d0_s8" to="o6472" />
    </statement>
    <statement id="d0_s9">
      <text>nut light-brown or grayish, ovoid-ellipsoid or oblong, (15-) 25-50 × (10-) 20-40 mm, finely puberulent or floccose.</text>
      <biological_entity id="o6473" name="nut" name_original="nut" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid-ellipsoid" value_original="ovoid-ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s9" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s9" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s9" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s9" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cotyledons distinct.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 24.</text>
      <biological_entity id="o6474" name="cotyledon" name_original="cotyledons" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6475" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering in spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bottomlands, riparian slopes, poorly drained areas, prairies, usually on limestone or calcareous clays (in nw part of range on dry slopes and ridges, prairies)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bottomlands" />
        <character name="habitat" value="riparian slopes" />
        <character name="habitat" value="drained areas" modifier="poorly" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="limestone" modifier="usually on" />
        <character name="habitat" value="calcareous clays" />
        <character name="habitat" value="nw part" constraint="of range" />
        <character name="habitat" value="range" />
        <character name="habitat" value="dry slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Sask., Ont., Que.; Ala., Ark., Conn., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., N.J., N.Y., N.Dak., Ohio, Okla., Pa., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>47.</number>
  <other_name type="common_name">Burr oak</other_name>
  <other_name type="common_name">mossy-cup oak</other_name>
  <other_name type="common_name">chêne &amp;agrave; gros fruits</other_name>
  <discussion>Quercus macrocarpa is one of our most cold-tolerant oak species; it also endures a wide variety of other harsh conditions including poor dry soils and wet, poorly drained, and inundated locations. Putative hybrids with Q. bicolor are common in the northeastern part of its range, where the two species often occur together in wet, poorly drained habitats. The effect of this contact may be partially responsible for morphologic differences across the range of Q. macrocarpa. The large acorns are best developed in the southern part of the range, and a clinal decrease in acorn size and extent of the mossy fringe on the acorn cup seems to occur as one travels from south to north. In the northwest part of its range, Q. macrocarpa varies clinally to smaller, shrubbier forms on bluffs and hillsides, with smaller, less fringed cups, that are the basis of Q. macrocarpa var. depressa (Nuttall) Engelmann and Q. mandanensis Rydberg. These scrubby forms may merit formal recognition after more thorough study; they are treated here as clinal variants of the species. Quercus macrocarpa forms putative hybrids also with Q. alba in the savannah-type regions of the midwest. Putative hybrids with Q. gambelii occur out of the range of Q. macrocarpa.</discussion>
  <discussion>Quercus macrocarpa is the only oak species native to Montana (in the southeast corner). Wood of Q. macrocarpa is similar to that of Q. alba and produces one of the best and most durable oak lumbers.</discussion>
  <discussion>Native Americans used Quercus macrocarpa medicinally to treat heart troubles, cramps, diarrhea, Italian itch, and broken bones, to expel pinworms, and as an astringent (D. E. Moerman 1986).</discussion>
  
</bio:treatment>