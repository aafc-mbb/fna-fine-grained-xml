<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(N. I. Malyutin) M. J. Warnock" date="1995" rank="subsection">Grumosa</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>78: 92. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection grumosa;</taxon_hierarchy>
    <other_info_on_name type="fna_id">302015</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="N. I. Malyutin" date="unknown" rank="section">Grumosa</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zhurn. (Leningrad &amp; Moscow)</publication_title>
      <place_in_publication>72: 689. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;section Grumosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots 1-5 (-9) -branched from within 1 cm of stem attachment, 2-7 (-16) cm, cormlike to fascicled or fibrous, ± fleshy, ± succulent, usually brittle;</text>
      <biological_entity id="o12542" name="root" name_original="roots" src="d0_s0" type="structure">
        <character constraint="of stem attachment" constraintid="o12543" is_modifier="false" name="architecture" src="d0_s0" value="1-5(-9)-branched" value_original="1-5(-9)-branched" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="16" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="7" to_unit="cm" />
        <character char_type="range_value" from="cormlike" name="architecture" src="d0_s0" to="fascicled" />
        <character is_modifier="false" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s0" value="brittle" value_original="brittle" />
      </biological_entity>
      <biological_entity constraint="stem" id="o12543" name="attachment" name_original="attachment" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>buds minute.</text>
      <biological_entity id="o12544" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 (-2) per root, usually unbranched, elongation delayed 2-10 weeks after leaf initiation;</text>
      <biological_entity id="o12545" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="2" />
        <character constraint="per root" constraintid="o12546" name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o12546" name="root" name_original="root" src="d0_s2" type="structure" />
      <biological_entity id="o12547" name="elongation" name_original="elongation" src="d0_s2" type="structure" />
      <biological_entity id="o12548" name="week" name_original="weeks" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="10" />
      </biological_entity>
      <biological_entity id="o12549" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <relation from="o12547" id="r1748" name="delayed" negation="false" src="d0_s2" to="o12548" />
      <relation from="o12547" id="r1749" name="after" negation="false" src="d0_s2" to="o12549" />
    </statement>
    <statement id="d0_s3">
      <text>base narrowed, tenuously attached to root;</text>
      <biological_entity id="o12550" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character constraint="to root" constraintid="o12551" is_modifier="false" modifier="tenuously" name="fixation" src="d0_s3" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o12551" name="root" name_original="root" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal internodes much shorter than those of midstem.</text>
      <biological_entity constraint="proximal" id="o12552" name="internode" name_original="internodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline, largest leaves near base of stem, others usually gradually smaller on distal portion of stem;</text>
      <biological_entity id="o12553" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="largest" id="o12554" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o12555" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o12556" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <biological_entity id="o12557" name="other" name_original="others" src="d0_s5" type="structure">
        <character constraint="on distal portion" constraintid="o12558" is_modifier="false" modifier="usually gradually" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12558" name="portion" name_original="portion" src="d0_s5" type="structure" />
      <biological_entity id="o12559" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o12554" id="r1750" name="near" negation="false" src="d0_s5" to="o12555" />
      <relation from="o12555" id="r1751" name="part_of" negation="false" src="d0_s5" to="o12556" />
      <relation from="o12558" id="r1752" name="part_of" negation="false" src="d0_s5" to="o12559" />
    </statement>
    <statement id="d0_s6">
      <text>basal petioles spreading, cauline petioles ascending;</text>
      <biological_entity constraint="basal" id="o12560" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o12561" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade shape and lobing similar throughout.</text>
      <biological_entity id="o12562" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="throughout" name="shape" src="d0_s7" value="lobing" value_original="lobing" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences usually with 2-6 flowers per 5 cm, open, ± pyramidal, usually at least 3 times longer than wide, spurs rarely intersecting rachis;</text>
      <biological_entity id="o12563" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="per 5 cm" name="architecture" notes="" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o12564" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o12565" name="spur" name_original="spurs" src="d0_s8" type="structure" />
      <biological_entity id="o12566" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <relation from="o12563" id="r1753" name="with" negation="false" src="d0_s8" to="o12564" />
      <relation from="o12565" id="r1754" modifier="rarely" name="intersecting" negation="false" src="d0_s8" to="o12566" />
    </statement>
    <statement id="d0_s9">
      <text>pedicel spreading, usually more than 1.5 cm, rachis to midpedicel angle more than 30°;</text>
      <biological_entity id="o12567" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1.5" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s9" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12568" name="rachis" name_original="rachis" src="d0_s9" type="structure" />
      <biological_entity constraint="midpedicel" id="o12569" name="angle" name_original="angle" src="d0_s9" type="structure">
        <character name="degree" src="d0_s9" value="30+°" value_original="30+°" />
      </biological_entity>
      <relation from="o12568" id="r1755" name="to" negation="false" src="d0_s9" to="o12569" />
    </statement>
    <statement id="d0_s10">
      <text>bracts markedly smaller and fewer lobed than leaves.</text>
      <biological_entity id="o12570" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="markedly" name="size" src="d0_s10" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="fewer" value_original="fewer" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o12571" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruits ± spreading.</text>
      <biological_entity id="o12572" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds obpyramidal, 1.5-2.7 × 0.7-2 mm, ringed at proximal end, wing-margined or not;</text>
      <biological_entity id="o12573" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
        <character constraint="at proximal end" constraintid="o12574" is_modifier="false" name="relief" src="d0_s12" value="ringed" value_original="ringed" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="wing-margined" value_original="wing-margined" />
        <character name="architecture" src="d0_s12" value="not" value_original="not" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o12574" name="end" name_original="end" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>seed-coats usually lacking wavy ridges, cells usually elongate (short and narrow in D. alabamicum, D. newtonianum, D. nuttallianum, and D. treleasei), smooth or roughened, cell margins straight.</text>
      <biological_entity id="o12575" name="seed-coat" name_original="seed-coats" src="d0_s13" type="structure" />
      <biological_entity id="o12576" name="ridge" name_original="ridges" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="usually" name="quantity" src="d0_s13" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="shape" src="d0_s13" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o12577" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s13" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s13" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity constraint="cell" id="o12578" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, possibly others in Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="possibly others in Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16b.9.</number>
  <discussion>Species 13 or more (13 in the flora).</discussion>
  <discussion>Delphinium subsect. Grumosa is an extremely difficult complex, with many variations in a number of morphologic traits. Two divergent views of the complex were represented in the work of D. M. Sutherland (1967), in which the species recognized were large agglomerations, and that of P. A. Rydberg (1917), in which species rank was used for some edaphic variants. The complex has been and continues to be a major source of confusion for identification of Delphinium in North America. Most of the low larkspurs in poisonous-plant literature are members of this subsection.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals yellowish, white, or pink; if white then white in most members of the population.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals bluish, maroon, or purple, not yellowish; if white then only as sporadic individuals in a population.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers with no trace of pink or lavender.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers (especially sepal spur) with some element of pink or lavender.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Spurs less than 12 mm; sepals spreading to erect.</description>
      <determination>54 Delphinium nuttallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Spurs more than 11 mm; sepals widely spreading; lower petals usually yellow.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems less than 60 cm; widest leaf lobe less than 1 cm wide.</description>
      <determination>52 Delphinium menziesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems more than 60 cm; widest leaf lobe more than 1 cm wide.</description>
      <determination>52 Delphinium menziesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems less than 50(-70) cm; plants with sepal color similar to that of most other individuals in the population.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems more than (40-)50 cm; plants with sepal color dissimilar from that of most other individuals in the population.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Seeds with elongate blunt hairs on surface.</description>
      <determination>55 Delphinium tricorne</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Seeds lacking elongate blunt hairs.</description>
      <determination>61 Delphinium nuttallianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plants in populations with red flowers.</description>
      <determination>47 Delphinium nudicaule</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plants in populations with blue flowers.</description>
      <determination>49 Delphinium trolliifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Sepals maroon; plants usually distinctly different individuals within populations.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Sepals bluish or purple; plants usually similar to other individuals in populations.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stems puberulent proximally.</description>
      <determination>47 Delphinium nudicaule</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stems glabrous proximally.</description>
      <determination>47 Delphinium nudicaule</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves mostly borne on distal 2/3 of stems at anthesis; flowers usually more than 15 per main inflorescence axis.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves mostly borne on proximal 1/3 of stems; flowers often fewer than 20 per main inflorescence axis.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Inflorescences as wide as long or nearly so.</description>
      <determination>56 Delphinium newtonianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Inflorescences at least 2 times longer than wide.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Sepals light blue.</description>
      <determination>49 Delphinium trolliifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Sepals dark blue to bluish purple.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Lobes of midcauline leaves more than 6 mm wide; stems more than (40-)60 cm.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Lobes of midcauline leaves less than 6 mm wide; stems less than 60(-90) cm.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf margins crenate.</description>
      <determination>50 Delphinium bakeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf margins ±incised.</description>
      <determination>49 Delphinium trolliifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Sepals bluish purple (±drab) and often partly fading upon drying (especially veins, giving sepals of dried specimens mottled appearance).</description>
      <determination>54 Delphinium nuttallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Sepals dark deep blue and retaining color upon drying.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Lower petals white, yellowish, or tan; lower petal blade clefts no more than 1/3 blade length.</description>
      <determination>53 Delphinium sutherlandii</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Lower petals blue to purple; lower petal blade clefts at least 1/3 blade length.</description>
      <determination>61 Delphinium nuttallianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Sepals dark blue-purple (± drab), often partly fading upon drying (especially veins, giving sepals of dried specimens mottled appearance), distinctly puberulent externally, usually not reflexed; lower stem pubescent.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Sepals bright blue or purple (not drab blue-purple), usually retaining color upon drying, usually glabrous, often reflexed; if sepals drab blue-purple, puberulent, and not reflexed, then proximal portion of stems nearly glabrous to glabrous.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Seeds with swollen, blunt, hairlike protuberances; at least 2/3 of leaves in lower 1/4 of stems; stems more than (45-)60 cm.</description>
      <determination>58 Delphinium alabamicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Seeds without swollen, blunt, hairlike protuberances; at least 1/3 of leaves above lower 1/4 of stems; stems less than 60(-85) cm.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Lower petal blade clefts at least 1/3 blade length.</description>
      <determination>51 Delphinium decorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Lower petal blade clefts no more than 1/4 blade length.</description>
      <determination>52 Delphinium menziesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Lateral sepals strongly reflexed; leaf blade with usually 5 or fewer lobes extending 3/5 distance to petiole (if more than 5, then pedicel puberulent), lobes often more than 7 mm wide.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Lateral sepals not reflexed or only weakly so; leaf blade with more than 5 lobes extending more than 3/5 distance to petiole, lobes less than 7 mm wide.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Pedicels spreading from rachis at nearly 90°; leaf lobes distinctly wedge-shaped, widest in distal 1/2.</description>
      <determination>59 Delphinium gracilentum</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Pedicels spreading from rachis at usually less than 70°; leaf lobes less often wedge-shaped, widest near midpoint.</description>
      <determination>60 Delphinium patens</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Cauline leaves 2 or fewer, less than 1/2 size of basal leaves; stems glaucous.</description>
      <determination>57 Delphinium treleasei</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Cauline leaves 3 or more, similar in size to basal leaves; stems not glaucous.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Inflorescences at least 3 times longer than wide; lower petals tan or yellowish, at least 8 mm.</description>
      <determination>53 Delphinium sutherlandii</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Inflorescences less than 3 times longer than wide; lower petals blue (except sometimes in white-flowered plants), 3-8(-11) mm.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Seeds with swollen, blunt, hairlike protuberances on surface.</description>
      <determination>55 Delphinium tricorne</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Seeds without swollen, blunt, hairlike protuberances.</description>
      <determination>61 Delphinium nuttallianum</determination>
    </key_statement>
  </key>
</bio:treatment>