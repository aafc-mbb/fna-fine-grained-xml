<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard P. Wunderlin</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">388</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Link" date="unknown" rank="family">MORACEAE</taxon_name>
    <taxon_hierarchy>family MORACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10583</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, shrubs, herbs, or vines, deciduous or evergreen, frequently with milky sap.</text>
      <biological_entity id="o12966" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character name="growth_form" value="herb" />
        <character name="growth_form" value="vine" />
      </biological_entity>
      <biological_entity id="o12970" name="sap" name_original="sap" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="milky" value_original="milky" />
      </biological_entity>
      <relation from="o12966" id="r1806" modifier="frequently" name="with" negation="false" src="d0_s0" to="o12970" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate (rarely opposite or whorled), simple;</text>
      <biological_entity id="o12971" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules present, persistent or caducous;</text>
      <biological_entity id="o12972" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole adaxially grooved.</text>
      <biological_entity id="o12973" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade: margins entire, toothed, or lobed;</text>
      <biological_entity id="o12974" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure" />
      <biological_entity id="o12975" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>venation pinnate or with 3-5 basal palmate veins;</text>
      <biological_entity id="o12976" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="with 3-5 basal palmate veins" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12977" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="palmate" value_original="palmate" />
      </biological_entity>
      <relation from="o12976" id="r1807" name="with" negation="false" src="d0_s5" to="o12977" />
    </statement>
    <statement id="d0_s6">
      <text>cystoliths often present in epidermal-cells.</text>
      <biological_entity id="o12978" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure" />
      <biological_entity id="o12979" name="cystolith" name_original="cystoliths" src="d0_s6" type="structure">
        <character constraint="in epidermal-cells" constraintid="o12980" is_modifier="false" modifier="often" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12980" name="epidermal-cell" name_original="epidermal-cells" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences racemes, cymes, or capitula.</text>
      <biological_entity id="o12981" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o12982" name="raceme" name_original="racemes" src="d0_s7" type="structure" />
      <biological_entity id="o12983" name="cyme" name_original="cymes" src="d0_s7" type="structure" />
      <biological_entity id="o12984" name="capitulum" name_original="capitula" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers unisexual, staminate and pistillate on same or different plants, small, occasionally on flattened torus, more often enclosed within fleshy, flask-shaped receptacle (syconium);</text>
      <biological_entity id="o12985" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o12986" is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="size" notes="" src="d0_s8" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o12986" name="plant" name_original="plants" src="d0_s8" type="structure" />
      <biological_entity id="o12987" name="torus" name_original="torus" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o12988" name="receptacle" name_original="receptacle" src="d0_s8" type="structure">
        <character is_modifier="true" name="texture" src="d0_s8" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="shape" src="d0_s8" value="flask--shaped" value_original="flask--shaped" />
      </biological_entity>
      <relation from="o12985" id="r1808" modifier="occasionally" name="on" negation="false" src="d0_s8" to="o12987" />
      <relation from="o12985" id="r1809" modifier="often" name="enclosed within" negation="false" src="d0_s8" to="o12988" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 2-6, distinct or partly connate (vestigial in Brosimum).</text>
      <biological_entity id="o12989" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="6" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="partly" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers: stamens equal in number to sepals or calyx lobes and opposite them, straight or inflexed;</text>
      <biological_entity id="o12990" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o12991" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character constraint="in number" constraintid="o12992" is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="opposite them" name="course" notes="" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="inflexed" value_original="inflexed" />
      </biological_entity>
      <biological_entity id="o12992" name="number" name_original="number" src="d0_s10" type="structure" />
      <biological_entity id="o12993" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
      <biological_entity constraint="calyx" id="o12994" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <relation from="o12992" id="r1810" name="to" negation="false" src="d0_s10" to="o12993" />
      <relation from="o12992" id="r1811" name="to" negation="false" src="d0_s10" to="o12994" />
    </statement>
    <statement id="d0_s11">
      <text>anthers 1-2-locular.</text>
      <biological_entity id="o12995" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o12996" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="1-2-locular" value_original="1-2-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: sepals or calyx lobes 4, ± connate;</text>
      <biological_entity id="o12997" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o12998" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o12999" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistils 1, 1-2-carpellate;</text>
      <biological_entity id="o13000" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13001" name="pistil" name_original="pistils" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-2-carpellate" value_original="1-2-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 1, superior or inferior, 1 (-2) -locular;</text>
      <biological_entity id="o13002" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13003" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s14" value="superior" value_original="superior" />
        <character is_modifier="false" name="position" src="d0_s14" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="1(-2)-locular" value_original="1(-2)-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovules 1 per locule;</text>
      <biological_entity id="o13004" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13005" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character constraint="per locule" constraintid="o13006" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o13006" name="locule" name_original="locule" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>styles or style-branches 1-2;</text>
      <biological_entity id="o13007" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13008" name="style" name_original="styles" src="d0_s16" type="structure" />
      <biological_entity id="o13009" name="style-branch" name_original="style-branches" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s16" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas 1-2, entire.</text>
      <biological_entity id="o13010" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13011" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s17" to="2" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits multiple (syncarps);</text>
      <biological_entity id="o13012" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s18" value="multiple" value_original="multiple" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>individual achenes or drupelets partly or completely enclosed by enlarged common receptacle or by individual calyces.</text>
      <biological_entity constraint="individual" id="o13013" name="achene" name_original="achenes" src="d0_s19" type="structure" />
      <biological_entity constraint="individual" id="o13014" name="drupelet" name_original="drupelets" src="d0_s19" type="structure" />
      <biological_entity constraint="individual" id="o13015" name="calyx" name_original="calyces" src="d0_s19" type="structure" />
      <relation from="o13013" id="r1812" modifier="completely" name="enclosed by enlarged common receptacle or by" negation="false" src="d0_s19" to="o13015" />
      <relation from="o13014" id="r1813" modifier="completely" name="enclosed by enlarged common receptacle or by" negation="false" src="d0_s19" to="o13015" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Widespread in tropical and subtropical regions, less common in temperate areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Widespread in tropical and subtropical regions" establishment_means="native" />
        <character name="distribution" value="less common in temperate areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Mulberry Family</other_name>
  <discussion>Genera ca. 40, species nearly 1100 (7 genera, 18 species in the flora).</discussion>
  <discussion>Members of the large and diverse mulberry family are mainly woody and tropical; they are most abundant in Asia. The largest genera are Ficus, with approximately 750 species, and Dorstenia, with about 170 species. The family includes important timber trees, e.g., Chlorophora excelsa (Welwitsch) Bentham, iroko, from tropical Africa; Brosimum guianense (Aublet) Huber, letterwood, snakewood; and Ficus spp. Genera with species bearing edible fruits include the mulberries, Morus spp.; breadfruit and jackfruit, e.g., Artocarpus altilis (Parkinson) Fosberg and A. heterophyllus Lamarck; and figs, Ficus spp. Several species of Ficus are commonly cultivated in subtropical regions of the United States. These include F. carica Linnaeus; F. elastica Roxburgh ex Hornemann, India rubber plant; F. benghalensis Linnaeus, banyan; F. benjamina Linnaeus, weeping fig; F. pumila Linnaeus, creeping fig; and F. microcarpa Linnaeus f., Indian-laurel.</discussion>
  <discussion>Rubber plants and weeping figs are commonly sold as houseplants. Economically, the most important species are those associated with the silk trade. Morus alba Linnaeus, M. indica Linnaeus, M. laevigata Wallis, and M. serrata Roxburgh, cultivated in many temperate and tropical countries, provide the natural food source for the silkworm, Bombyx mori Linnaeus.</discussion>
  <discussion>Cudrania tricuspidata (Carrière) Bureau ex Lavallée, used as a food source for silkworms when Morus spp. are in short supply, is cultivated in North America as a hedge plant. The fruit is edible. Native to Korea and China, C. tricuspidata is known from a collection made in 1956 in McIntosh County, Georgia (S. B. Jones Jr. and N. C. Coile 1988), and it is naturalized in Orange County, North Carolina (R. D. Whetstone, pers. comm.).</discussion>
  <references>
    <reference>Engler, H. G. A. 1888b. Moraceae. In: H. G. A. Engler and K. Prantl, eds. 1887-1915. Die natürlichen Pflanzenfamilien.... 254 fasc. Leipzig. Fasc. 18[III,1], pp. 66-96.</reference>
    <reference>Rohwer, J. G. 1993b. Moraceae. In: K. Kubutzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 2+ vols. Berlin etc. Vol. 2, pp. 438-453.</reference>
    <reference>Tomlinson, P. B. 1980. The Biology of Trees Native to Tropical Florida. Allston, Mass.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Trees, shrubs, or vines.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants lacking evident aerial stems, rhizomatous, perennial; inflorescences axillary, long-pedunculate.</description>
      <determination>6 Dorstenia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants caulescent, taprooted, annual; inflorescences axillary, short-pedunculate.</description>
      <determination>1 Fatoua</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers all borne on inside of syconium; terminal vegetative bud surrounded by pair of stipules.</description>
      <determination>7 Ficus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers not borne on inside of syconium or only a solitary female flower immersed in receptacle; terminal vegetative bud scaly, not surrounded by pair of stipules.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Margins of leaf blade toothed, often lobed; venation appearing palmate, or weakly 3-veined from base.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Margins of leaf blade entire, never lobed; venation pinnate.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Pistillate inflorescences globose; styles unbranched.</description>
      <determination>3 Broussonetia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Pistillate inflorescences cylindric; styles 2-branched.</description>
      <determination>2 Morus</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blade ovate to lanceolate, not leathery; trees deciduous; syncarps 8-12 cm diam.</description>
      <determination>4 Maclura</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blade oblong, leathery; trees evergreen; syncarps 1.5 cm diam.</description>
      <determination>5 Brosimum</determination>
    </key_statement>
  </key>
</bio:treatment>