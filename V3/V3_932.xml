<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="N. I. Malyutin" date="1987" rank="subsection">exaltata</taxon_name>
    <taxon_name authority="Rydberg" date="1901" rank="species">ramosum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>28: 276. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection exaltata;species ramosum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500545</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (45-) 70-100 cm;</text>
      <biological_entity id="o23568" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base sometimes reddish, puberulent.</text>
      <biological_entity id="o23569" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, 8-24, absent from or on proximal 1/5 of stem at anthesis;</text>
      <biological_entity id="o23570" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s2" to="24" />
        <character constraint="of stem" constraintid="o23571" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23571" name="stem" name_original="stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1-12 cm.</text>
      <biological_entity id="o23572" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade round to pentagonal, 2-8 × 4-14 cm, nearly glabrous;</text>
      <biological_entity id="o23573" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s4" to="pentagonal" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s4" to="14" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 5-21, width 1-5 mm;</text>
      <biological_entity constraint="ultimate" id="o23574" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="21" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>midcauline leaf lobes more than 3 times longer than wide.</text>
      <biological_entity constraint="leaf" id="o23575" name="lobe" name_original="lobes" src="d0_s6" type="structure" constraint_original="midcauline leaf">
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="3+" value_original="3+" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences (10-) 15-40 (-120) -flowered;</text>
      <biological_entity id="o23576" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="(10-)15-40(-120)-flowered" value_original="(10-)15-40(-120)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 1-2.5 (-4) cm, puberulent;</text>
      <biological_entity id="o23577" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles 1-3 mm from flowers, green, sometimes margins white, linear or lanceolate, 3-5 (-8) mm, puberulent.</text>
      <biological_entity id="o23578" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o23579" from="1" from_unit="mm" name="location" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o23579" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o23580" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals bright dark blue, apex rounded, puberulent, lateral sepals forward pointing to spreading, 11-13 × 4-6 mm, spurs straight, ascending ca. 30° above horizontal, 9-13 mm;</text>
      <biological_entity id="o23581" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o23582" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="bright dark" value_original="bright dark" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blue" value_original="blue" />
      </biological_entity>
      <biological_entity id="o23583" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o23584" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pointing" name="orientation" src="d0_s10" to="spreading" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23585" name="spur" name_original="spurs" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character constraint="above horizontal" is_modifier="false" modifier="30°" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower petal blades ± covering stamens, 5-7 mm, clefts 2-3 mm;</text>
      <biological_entity id="o23586" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="petal" id="o23587" name="blade" name_original="blades" src="d0_s11" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23588" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o23589" name="cleft" name_original="clefts" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o23587" id="r3220" modifier="more or less" name="covering" negation="false" src="d0_s11" to="o23588" />
    </statement>
    <statement id="d0_s12">
      <text>hairs centered above base of cleft, white.</text>
      <biological_entity id="o23590" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o23591" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o23592" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o23591" id="r3221" name="centered above" negation="false" src="d0_s12" to="o23592" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits 11-17 mm, 3-4 times longer than wide, puberulent.</text>
      <biological_entity id="o23593" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s13" to="17" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s13" value="3-4" value_original="3-4" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds wing-margined;</text>
      <biological_entity id="o23594" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>seed-coat cells narrow, short, surfaces roughened.</text>
      <biological_entity constraint="seed-coat" id="o23595" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 16.</text>
      <biological_entity id="o23596" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief_or_texture" src="d0_s15" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23597" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, aspen woodlands, Artemisia scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="aspen woodlands" />
        <character name="habitat" value="artemisia scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100-3200(-3400) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="2100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3400" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Delphinium ramosum hybridizes with D. barbeyi and D. glaucum.</discussion>
  
</bio:treatment>