<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan T. Whittemore</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">MYOSURUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 284. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 137. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus MYOSURUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek mus, mouse, and oura, tail, from shape and texture of the fruiting head of M. minimus.</other_info_on_name>
    <other_info_on_name type="fna_id">121434</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o15109" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal, simple, tapering to filiform base.</text>
      <biological_entity id="o15110" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="tapering" name="shape" src="d0_s1" to="filiform" />
      </biological_entity>
      <biological_entity id="o15111" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade linear or very narrowly oblanceolate, margins entire.</text>
      <biological_entity id="o15112" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="very narrowly" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o15113" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal, solitary flowers;</text>
      <biological_entity id="o15114" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o15115" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts absent.</text>
      <biological_entity id="o15116" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o15117" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals not persistent in fruit, (3-) 5 (-8), green or with scarious margins, spurred, oblong to elliptic, lanceolate, or oblanceolate, 1.5-4 mm;</text>
      <biological_entity id="o15118" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o15119" is_modifier="false" modifier="not" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="with scarious margins" value_original="with scarious margins" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="spurred" value_original="spurred" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="elliptic lanceolate or oblanceolate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="elliptic lanceolate or oblanceolate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="elliptic lanceolate or oblanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15119" name="fruit" name_original="fruit" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s6" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="8" />
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o15120" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o15118" id="r2108" name="with" negation="false" src="d0_s6" to="o15120" />
    </statement>
    <statement id="d0_s7">
      <text>petals 0-5, distinct, white, plane, linear to very narrowly spatulate, long-clawed, 1-2.5 mm;</text>
      <biological_entity id="o15121" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear to very" value_original="linear to very" />
        <character is_modifier="false" modifier="very; narrowly" name="shape" src="d0_s7" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="long-clawed" value_original="long-clawed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s7" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear to very" value_original="linear to very" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nectary present;</text>
      <biological_entity id="o15122" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 5-25;</text>
      <biological_entity id="o15123" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments filiform;</text>
      <biological_entity id="o15124" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes absent between stamens and pistils;</text>
      <biological_entity id="o15125" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character constraint="between stamens, pistils" constraintid="o15126, o15127" is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15126" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o15127" name="pistil" name_original="pistils" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pistils 10-400, simple;</text>
      <biological_entity id="o15128" name="pistil" name_original="pistils" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="400" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovule 1 per pistil;</text>
      <biological_entity id="o15129" name="ovule" name_original="ovule" src="d0_s13" type="structure">
        <character constraint="per pistil" constraintid="o15130" name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15130" name="pistil" name_original="pistil" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style persistent.</text>
      <biological_entity id="o15131" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits achenes, aggregate, sessile, prismatic, exposed face forming plane outer surface, sides faceted or curved by compression against adjacent achenes;</text>
      <biological_entity constraint="fruits" id="o15132" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="aggregate" value_original="aggregate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s15" value="prismatic" value_original="prismatic" />
      </biological_entity>
      <biological_entity id="o15133" name="face" name_original="face" src="d0_s15" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s15" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity constraint="outer" id="o15134" name="surface" name_original="surface" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o15135" name="side" name_original="sides" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="faceted" value_original="faceted" />
        <character constraint="by compression" constraintid="o15136" is_modifier="false" name="course" src="d0_s15" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o15136" name="compression" name_original="compression" src="d0_s15" type="structure" />
      <biological_entity id="o15137" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s15" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o15133" id="r2109" name="forming" negation="false" src="d0_s15" to="o15134" />
      <relation from="o15136" id="r2110" name="against" negation="false" src="d0_s15" to="o15137" />
    </statement>
    <statement id="d0_s16">
      <text>sides not veined;</text>
      <biological_entity id="o15138" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak terminal, straight, 0.05-1.8 mm. x=8.</text>
      <biological_entity id="o15139" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s17" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="x" id="o15140" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate regions worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate regions worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Mousetail</other_name>
  <discussion>Species 15 (5 in the flora).</discussion>
  <discussion>Mature fruits are crucial to accurate identification of most North American species of Myosurus.</discussion>
  <discussion>Flowers of Myosurus are unique in the family in that the receptacle continues to elongate and, in some species, to initiate new ovaries after the flowers open and pollen is shed (D.E. Stone 1959).</discussion>
  <references>
    <reference>Mason, H. L. and D. E. Stone. 1957. Myosurus. In: Mason, H. L. 1957. A Flora of the Marshes of California. Berkeley. Pp. 497-505</reference>
    <reference>Stone, D. E. 1959. A unique balanced breeding system in the vernal pool mouse-tails. Evolution 13: 151-174.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Outer face of achene orbiculate to square or broadly rhombic, 0.8–1.3 times as high as wide.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Outer face of achene narrowly rhombic to elliptic or oblong to linear, 1.5–5 times as high as wide.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Outer face of achene bordered by prominent ridge.</description>
      <determination>4 Myosurus cupulatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Outer face of achene not bordered.</description>
      <determination>5 Myosurus nitidus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Beak of achene 0.05–0.4 mm, 0.05–0.3 times as long as body of achene, parallel to outer face of achene, heads of achenes thus appearing smooth.</description>
      <determination>1 Myosurus minimus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Beak of achene 0.6–1.8 mm, 0.4–1.1 times as long as body of achene, divergent, heads of achenes thus strongly roughened by projecting achene beaks.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Scapes 0–0.2 cm; heads of achenes immersed in leaves.</description>
      <determination>2 Myosurus sessilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Scapes 0.9–10.5 cm; heads of achenes projecting beyond leaves.</description>
      <determination>3 Myosurus apetalus</determination>
    </key_statement>
  </key>
</bio:treatment>