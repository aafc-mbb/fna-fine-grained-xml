<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">nymphaeaceae</taxon_name>
    <taxon_name authority="Smith in J. Sibthorp &amp; J. E. Smith" date="1809" rank="genus">nuphar</taxon_name>
    <taxon_name authority="(Walter) Pursh" date="1814" rank="species">sagittifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 370. 1814 (as sagittaefolia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nymphaeaceae;genus nuphar;species sagittifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500819</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nymphaea</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">sagittifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>155. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Nymphaea;species sagittifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nuphar</taxon_name>
    <taxon_name authority="(Linnaeus) Smith" date="unknown" rank="species">lutea</taxon_name>
    <taxon_name authority="(Walter) E. O. Beal" date="unknown" rank="subspecies">sagittifolia</taxon_name>
    <taxon_hierarchy>genus Nuphar;species lutea;subspecies sagittifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes 2-2.5 cm diam.</text>
      <biological_entity id="o7983" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s0" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves floating or submersed;</text>
      <biological_entity id="o7984" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="location" src="d0_s1" value="floating" value_original="floating" />
        <character is_modifier="false" name="location" src="d0_s1" value="submersed" value_original="submersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole terete.</text>
      <biological_entity id="o7985" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade abaxially and adaxially green, linear to lanceolate, 15-30 (-50) × 5-10 (-11.5) cm, 3-5 times as long as wide, sinus less than 1/3 length of midrib, lobes usually divergent and forming V-shaped angle;</text>
      <biological_entity id="o7986" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="50" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="11.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s3" to="10" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="3-5" value_original="3-5" />
      </biological_entity>
      <biological_entity id="o7987" name="sinus" name_original="sinus" src="d0_s3" type="structure">
        <character char_type="range_value" from="0 length of midrib" name="length" src="d0_s3" to="1/3 length of midrib" />
      </biological_entity>
      <biological_entity id="o7988" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o7989" name="angle" name_original="angle" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <relation from="o7988" id="r1120" name="forming" negation="false" src="d0_s3" to="o7989" />
    </statement>
    <statement id="d0_s4">
      <text>surfaces glabrous.</text>
      <biological_entity id="o7990" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 2-3 cm diam.;</text>
      <biological_entity id="o7991" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals 6, abaxially green to adaxially yellow toward base;</text>
      <biological_entity id="o7992" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
        <character char_type="range_value" constraint="toward base" constraintid="o7993" from="abaxially green" name="coloration" src="d0_s6" to="adaxially yellow" />
      </biological_entity>
      <biological_entity id="o7993" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>petals oblong, thick;</text>
      <biological_entity id="o7994" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 3-5 mm, barely or not at all longer than filaments.</text>
      <biological_entity id="o7995" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7997" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o7996" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o7996" id="r1121" modifier="barely; barely; barely; not; barely; not" name="at" negation="true" src="d0_s8" to="o7997" />
    </statement>
    <statement id="d0_s9">
      <text>Fruit green, ovoid, 3-3.5 × 2-3 cm, smooth basally, strongly ribbed toward apex, slightly constricted below stigmatic disk;</text>
      <biological_entity id="o7998" name="fruit" name_original="fruit" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s9" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s9" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character constraint="toward apex" constraintid="o7999" is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s9" value="ribbed" value_original="ribbed" />
        <character constraint="below stigmatic, disk" constraintid="o8000, o8001" is_modifier="false" modifier="slightly" name="size" notes="" src="d0_s9" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o7999" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o8000" name="stigmatic" name_original="stigmatic" src="d0_s9" type="structure" />
      <biological_entity id="o8001" name="disk" name_original="disk" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigmatic disk green, 14-18 mm diam., nearly entire;</text>
      <biological_entity constraint="stigmatic" id="o8002" name="disk" name_original="disk" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="14" from_unit="mm" name="diameter" src="d0_s10" to="18" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmatic rays 10-14, linear, mostly terminating 1-2 mm from margin of disk.</text>
      <biological_entity constraint="stigmatic" id="o8003" name="ray" name_original="rays" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="14" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 4-5 mm.</text>
      <biological_entity id="o8004" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="mid spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Freshwater streams, rivers, ponds, and lakes of coastal plain, extending to freshwater tidal areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="freshwater streams" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" constraint="of coastal plain , extending to freshwater tidal areas" />
        <character name="habitat" value="coastal plain" />
        <character name="habitat" value="tidal areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Nuphar sagittifolia is probably best treated as a subspecies. Plants intermediate between it and N. advena are treated under N. advena. The clinal variation pattern between the two taxa is apparently maintained via selection by vernalization (C. E. DePoe and E. O. Beal 1969; E. O. Beal and R. M. Southall 1977).</discussion>
  <discussion>This taxon is the Cape Fear spatterdock of the aquarium trade.</discussion>
  
</bio:treatment>