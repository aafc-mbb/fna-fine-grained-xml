<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">urticaceae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="genus">PILEA</taxon_name>
    <place_of_publication>
      <publication_title>Coll. Bot., plate</publication_title>
      <place_in_publication>4 and text on facing page. 1821, name conserved</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family urticaceae;genus PILEA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin pileus, felt cap, because of the calyx covering the achene</other_info_on_name>
    <other_info_on_name type="fna_id">125428</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque ex Britton &amp; A. Brown" date="unknown" rank="genus">Adicea</taxon_name>
    <taxon_hierarchy>genus Adicea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, shrubs, or subshrubs, annual or perennial, glabrous.</text>
      <biological_entity id="o8064" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or branched, erect, ascending, or repent.</text>
      <biological_entity id="o8067" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="repent" value_original="repent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o8068" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules present.</text>
      <biological_entity id="o8069" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades paired, equal or unequal, ovate, margins dentate or entire;</text>
      <biological_entity id="o8070" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
        <character is_modifier="false" name="variability" src="d0_s4" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s4" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o8071" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cystoliths linear, ± conspicuous.</text>
      <biological_entity id="o8072" name="cystolith" name_original="cystoliths" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="more or less" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, compact to lax cymes.</text>
      <biological_entity id="o8073" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="compact" name="architecture_or_arrangement" src="d0_s6" to="lax" />
      </biological_entity>
      <biological_entity id="o8074" name="cyme" name_original="cymes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, staminate and pistillate flowers in same cyme;</text>
      <biological_entity id="o8075" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8076" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8077" name="cyme" name_original="cyme" src="d0_s7" type="structure" />
      <relation from="o8076" id="r1126" name="in" negation="false" src="d0_s7" to="o8077" />
    </statement>
    <statement id="d0_s8">
      <text>bracts deltate to linear.</text>
      <biological_entity id="o8078" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s8" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate flowers: tepals 4;</text>
      <biological_entity id="o8079" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8080" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 4;</text>
      <biological_entity id="o8081" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8082" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillode conic.</text>
      <biological_entity id="o8083" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8084" name="pistillode" name_original="pistillode" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: tepals 3, equal or sometimes 1 tepal enlarged and hoodlike;</text>
      <biological_entity id="o8085" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8086" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character name="variability" src="d0_s12" value="sometimes" value_original="sometimes" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8087" name="tepal" name_original="tepal" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="shape" src="d0_s12" value="hoodlike" value_original="hoodlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminodes 3, opposite tepals, under tension and ejecting mature achene;</text>
      <biological_entity id="o8088" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8089" name="staminode" name_original="staminodes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o8090" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o8091" name="achene" name_original="achene" src="d0_s13" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s13" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o8090" id="r1127" name="under" negation="false" src="d0_s13" to="o8091" />
    </statement>
    <statement id="d0_s14">
      <text>style and tufted stigma deciduous.</text>
      <biological_entity id="o8092" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8093" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o8094" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement_or_pubescence" src="d0_s14" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes sessile, laterally compressed, ovoid to teardrop-shaped, free from perianth at maturity, partly covered by hoodlike tepal.</text>
      <biological_entity id="o8096" name="perianth" name_original="perianth" src="d0_s15" type="structure" />
      <biological_entity id="o8097" name="tepal" name_original="tepal" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="hoodlike" value_original="hoodlike" />
      </biological_entity>
      <relation from="o8095" id="r1128" modifier="partly; at maturity" name="covered by" negation="false" src="d0_s15" to="o8097" />
    </statement>
    <statement id="d0_s16">
      <text>x = 12, 13.</text>
      <biological_entity id="o8095" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s15" to="teardrop-shaped" />
        <character constraint="from perianth" constraintid="o8096" is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
      </biological_entity>
      <biological_entity constraint="x" id="o8098" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
        <character name="quantity" src="d0_s16" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly tropical and subtropical regions worldwide except Australia and New Zealand.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly tropical and subtropical regions worldwide except Australia and New Zealand" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Species ca. 400 (5 in the flora).</discussion>
  <discussion>Pilea should be further revised.</discussion>
  <references>
    <reference>Chen, C. J. 1982. A monograph of Pilea (Urticaceae) in China. Bull. Bot. Res., Harbin 2: 1-132.</reference>
    <reference>Fernald, M. L. 1936. Pilea in eastern North America. Contr. Gray Herb. 113: 169-170.</reference>
    <reference>Hermann, F. J. 1940. The geographic distribution of Pilea fontana. Torreya 40: 118-120.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf margins dentate; paired blades equal.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf margins entire; paired blades unequal.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Achenes uniformly black except for very narrow, pale, marginal band, surface conspicuously pebbled with raised bosses.</description>
      <determination>4 Pilea fontana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Achenes uniformly light colored or streaked with purple, surface smooth or purple striations sometimes slightly raised.</description>
      <determination>5 Pilea pumila</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems repent or prostrate.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems erect or diffusely ascending.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs; leaf blades spatulate to obovate.</description>
      <determination>2 Pilea microphylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Shrubs or subshrubs; leaf blades of 2 types, obovate and orbiculate to orbiculate-cordate.</description>
      <determination>3 Pilea trianthemoides</determination>
    </key_statement>
  </key>
</bio:treatment>