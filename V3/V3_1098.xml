<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="Ewan" date="1936" rank="subsection">Subscaposa</taxon_name>
    <taxon_name authority="Piper" date="1906" rank="species">xantholeucum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>11: 280. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection subscaposa;species xantholeucum;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500564</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 40-60 (-100) cm;</text>
      <biological_entity id="o20179" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base often reddish, glabrous, ± glaucous.</text>
      <biological_entity id="o20180" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o20181" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves usually absent at anthesis;</text>
      <biological_entity constraint="basal" id="o20182" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 4-6 at anthesis;</text>
      <biological_entity constraint="cauline" id="o20183" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="4" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.8-16 cm.</text>
      <biological_entity id="o20184" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s5" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round, 2-6 × 4-10 cm, nearly glabrous;</text>
      <biological_entity id="o20185" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="round" value_original="round" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s6" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-15, width 3-8 mm (basal), 1-5 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o20186" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 10-20 (-60) -flowered, narrowly pyramidal;</text>
      <biological_entity id="o20187" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="10-20(-60)-flowered" value_original="10-20(-60)-flowered" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel spreading, yellowish, 1.5-3 cm, ± glandular-puberulent;</text>
      <biological_entity id="o20188" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s9" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 6-12 mm from flowers, green to light-brown, linear to lanceolate, 4-7 mm, nearly glabrous.</text>
      <biological_entity id="o20189" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o20190" from="6" from_unit="mm" name="location" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s10" to="light-brown" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s10" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20190" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals yellow, glabrous, lateral sepals reflexed, 9-12 × 3-5 mm, spurs straight, ascending ca. 45° above horizontal, 11-15 mm;</text>
      <biological_entity id="o20191" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o20192" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o20193" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20194" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character constraint="above horizontal" is_modifier="false" modifier="45°" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades elevated, exposing stamens, 3-5 mm, clefts 1-2 mm;</text>
      <biological_entity id="o20195" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o20196" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20197" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o20198" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o20196" id="r2727" name="exposing" negation="false" src="d0_s12" to="o20197" />
    </statement>
    <statement id="d0_s13">
      <text>hairs centered mostly on inner lobes near base of cleft, white.</text>
      <biological_entity id="o20199" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o20200" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character constraint="on inner lobes" constraintid="o20201" is_modifier="false" name="position" src="d0_s13" value="centered" value_original="centered" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="inner" id="o20201" name="lobe" name_original="lobes" src="d0_s13" type="structure" />
      <biological_entity id="o20202" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o20201" id="r2728" name="near" negation="false" src="d0_s13" to="o20202" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 15-22 mm, 3-4 times longer than wide, glabrous to glandular-puberulent.</text>
      <biological_entity id="o20203" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s14" to="22" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="3-4" value_original="3-4" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s14" to="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds: seed-coat cells narrow, short, cell margins straight, surfaces smooth.</text>
      <biological_entity id="o20204" name="seed" name_original="seeds" src="d0_s15" type="structure" />
      <biological_entity constraint="seed-coat" id="o20205" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="cell" id="o20206" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 16.</text>
      <biological_entity id="o20207" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20208" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Slopes in open yellow pine forests, grasslands, sage scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open yellow pine forests" modifier="slopes in" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="sage scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Delphinium xantholeucum is very local; much of the habitat of this species has been converted to orchards.</discussion>
  
</bio:treatment>