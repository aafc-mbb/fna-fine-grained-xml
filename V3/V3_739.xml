<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(de Candolle) B. Boivin" date="1944" rank="section">Heterogamia</taxon_name>
    <taxon_name authority="Buckley" date="1843" rank="species">debile</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts</publication_title>
      <place_in_publication>45: 175. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section heterogamia;species debile;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501265</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots brownish, fusiform-tuberous with dried ribs.</text>
      <biological_entity id="o24540" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character constraint="with ribs" constraintid="o24541" is_modifier="false" name="architecture" src="d0_s0" value="fusiform-tuberous" value_original="fusiform-tuberous" />
      </biological_entity>
      <biological_entity id="o24541" name="rib" name_original="ribs" src="d0_s0" type="structure">
        <character is_modifier="true" name="condition" src="d0_s0" value="dried" value_original="dried" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems reclining, branched and flexible proximally, 10-40 cm, glabrous.</text>
      <biological_entity id="o24542" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="reclining" value_original="reclining" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="proximally" name="fragility" src="d0_s1" value="pliable" value_original="flexible" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal cauline leaves petiolate, 1-3×-ternately compound;</text>
      <biological_entity id="o24543" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal cauline" id="o24544" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="1-3×-ternately" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal cauline leaves petiolate or sessile, 1-2×-ternately compound or simple.</text>
      <biological_entity id="o24545" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal cauline" id="o24546" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="1-2×-ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade: leaflets ovate or obovate to reniform or orbiculate, apically shallowly to deeply 3-7-lobed, rarely undivided, 4-15 mm wide, surfaces glabrous.</text>
      <biological_entity id="o24547" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure" />
      <biological_entity id="o24548" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="reniform or orbiculate" />
        <character is_modifier="false" modifier="apically shallowly; shallowly to deeply" name="shape" src="d0_s4" value="3-7-lobed" value_original="3-7-lobed" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="undivided" value_original="undivided" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24549" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal and axillary, panicles, elongate, few flowered.</text>
      <biological_entity id="o24550" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o24551" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals whitish, lanceolate to obovate, 1.5-2.7 mm;</text>
      <biological_entity id="o24552" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o24553" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments colored, not white, 1.5-2 mm;</text>
      <biological_entity id="o24554" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24555" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="colored" value_original="colored" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 1.7-2.5 mm, mucronate;</text>
      <biological_entity id="o24556" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24557" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigma color unknown.</text>
      <biological_entity id="o24558" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24559" name="stigma" name_original="stigma" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Achenes 1-6, not reflexed, nearly sessile;</text>
      <biological_entity id="o24560" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="6" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stipe 0.1-0.3 mm;</text>
      <biological_entity id="o24561" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>body oblong to elliptic-lanceolate, not compressed, 3-3.7 × 0.7-1.2 mm, glabrous, prominently 6-8-veined, veins not anastomosing;</text>
      <biological_entity id="o24562" name="body" name_original="body" src="d0_s12" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="elliptic-lanceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s12" value="6-8-veined" value_original="6-8-veined" />
      </biological_entity>
      <biological_entity id="o24563" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>beak 1.3-2 mm.</text>
      <biological_entity id="o24564" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering in early spring (Mar–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" constraint="Mar-Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich, rocky, limestone woods, often in wet, alluvial soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone woods" modifier="rich rocky" />
        <character name="habitat" value="wet" />
        <character name="habitat" value="alluvial soil" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Thalictrum debile is closely related to T. arkansanum and T. texanum. The distinctions among the three species should be further studied.</discussion>
  
</bio:treatment>