<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="M. J. Warnock" date="1995" rank="subsection">multiplex</taxon_name>
    <taxon_name authority="Eastwood" date="1901" rank="species">polycladon</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>28: 669. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection multiplex;species polycladon;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500543</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">scopulorum</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="unknown" rank="variety">luporum</taxon_name>
    <taxon_hierarchy>genus Delphinium;species scopulorum;variety luporum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (20-) 60-100 (-160) cm;</text>
      <biological_entity id="o10993" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="160" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base reddish or not, glabrous.</text>
      <biological_entity id="o10994" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character name="coloration" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly on proximal 1/3 of stem, on proximal 1/5 at anthesis;</text>
      <biological_entity id="o10995" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o10996" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <biological_entity id="o10997" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity id="o10998" name="anthesis" name_original="anthesis" src="d0_s2" type="structure">
        <character is_modifier="true" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="true" name="quantity" src="d0_s2" value="1/5" value_original="1/5" />
      </biological_entity>
      <relation from="o10995" id="r1538" name="on" negation="false" src="d0_s2" to="o10996" />
      <relation from="o10996" id="r1539" name="part_of" negation="false" src="d0_s2" to="o10997" />
      <relation from="o10995" id="r1540" name="on" negation="false" src="d0_s2" to="o10998" />
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 0-3 at anthesis;</text>
      <biological_entity constraint="basal" id="o10999" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 4-7 at anthesis;</text>
      <biological_entity constraint="cauline" id="o11000" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="4" name="quantity" src="d0_s4" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1.5-17 cm.</text>
      <biological_entity id="o11001" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round to pentagonal, 1.5-7 × 2-14 cm, glabrous;</text>
      <biological_entity id="o11002" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s6" to="pentagonal" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="14" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-12, width 4-30 (-45) mm (basal), 3-30 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o11003" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="12" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="45" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 3-15 (-35) -flowered, open, often ± secund;</text>
      <biological_entity id="o11004" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-15(-35)-flowered" value_original="3-15(-35)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" modifier="often more or less" name="architecture" src="d0_s8" value="secund" value_original="secund" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel 1-4 (-15) cm, glabrous to puberulent;</text>
      <biological_entity id="o11005" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s9" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 2-8 (-37) mm from flowers, green, linear, 4-7 (-11) mm, nearly glabrous.</text>
      <biological_entity id="o11006" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="37" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o11007" from="2" from_unit="mm" name="location" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11007" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals bluish purple, nearly glabrous, lateral sepals spreading, (10-) 12-18 × 7-10 mm, spurs usually downcurved, ca. 30° below horizontal, 11-22 mm;</text>
      <biological_entity id="o11008" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11009" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="bluish purple" value_original="bluish purple" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o11010" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s11" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s11" to="18" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11011" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s11" value="downcurved" value_original="downcurved" />
        <character constraint="below horizontal" name="degree" src="d0_s11" value="30°" value_original="30°" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades slightly elevated, ± exposing stamens, 4-6 mm, clefts 1-2 mm;</text>
      <biological_entity id="o11012" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o11013" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="4" from_unit="mm" modifier="more or less" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11014" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o11015" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o11013" id="r1541" modifier="more or less" name="exposing" negation="false" src="d0_s12" to="o11014" />
    </statement>
    <statement id="d0_s13">
      <text>hairs mostly near base of cleft on inner lobes, yellow, sometimes white.</text>
      <biological_entity id="o11016" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o11017" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o11018" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity constraint="inner" id="o11019" name="lobe" name_original="lobes" src="d0_s13" type="structure" />
      <relation from="o11017" id="r1542" name="near" negation="false" src="d0_s13" to="o11018" />
      <relation from="o11018" id="r1543" modifier="of cleft" name="on" negation="false" src="d0_s13" to="o11019" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 13-20 mm, 3.5-4 times longer than wide, puberulent.</text>
      <biological_entity id="o11020" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="3.5-4" value_original="3.5-4" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds unwinged;</text>
      <biological_entity id="o11021" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="unwinged" value_original="unwinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>seed-coat cells with surfaces roughened.</text>
      <biological_entity id="o11023" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief_or_texture" src="d0_s16" value="roughened" value_original="roughened" />
      </biological_entity>
      <relation from="o11022" id="r1544" name="with" negation="false" src="d0_s16" to="o11023" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 16.</text>
      <biological_entity constraint="seed-coat" id="o11022" name="cell" name_original="cells" src="d0_s16" type="structure" />
      <biological_entity constraint="2n" id="o11024" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early autumn.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early autumn" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet sites near springs, streamsides, bogs, and wet talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet sites" constraint="near springs , streamsides , bogs , and wet talus" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="wet talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200-3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">High mountain larkspur</other_name>
  <discussion>Delphinium polycladon hybridizes with D. depauperatum and D. glaucum. Plants of D. polycladon are extremely variable. Individuals from very rocky, thin-soiled, sunny sites at higher elevations tend to be quite compact; they show the features of the species in a dwarfed state. Proximal internodes are especially shortened. Plants from areas of deeper soil (high or low elevations), especially those growing among shrubs, usually are much taller, with elongate proximal internodes, and other vegetative parts proportionally larger. Shorter plants may be confused with D. depauperatum or D. nuttallianum; see discussion under those species for distinguishing features. Taller plants may be confused with D. glaucum; they can be distinguished by their leaves predominately on proximal part of stem, sigmoid pedicel, and fewer flowers.</discussion>
  
</bio:treatment>