<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">berberis</taxon_name>
    <taxon_name authority="Fortune" date="1850" rank="species">bealei</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Chron.</publication_title>
      <place_in_publication>1850: 212. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus berberis;species bealei</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">233500224</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mahonia</taxon_name>
    <taxon_name authority="(Fortune) Carrière" date="unknown" rank="species">bealei</taxon_name>
    <taxon_hierarchy>genus Mahonia;species bealei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, evergreen, 1-2 m.</text>
      <biological_entity id="o16242" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems monomorphic, without short axillary shoots.</text>
      <biological_entity id="o16243" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o16244" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <relation from="o16243" id="r2239" name="without" negation="false" src="d0_s1" to="o16244" />
    </statement>
    <statement id="d0_s2">
      <text>Bark of 2d-year stems tan, glabrous.</text>
      <biological_entity id="o16245" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16246" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <relation from="o16245" id="r2240" name="part_of" negation="false" src="d0_s2" to="o16246" />
    </statement>
    <statement id="d0_s3">
      <text>Bud-scales 11-13 mm, persistent.</text>
      <biological_entity id="o16247" name="bud-scale" name_original="bud-scales" src="d0_s3" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="distance" src="d0_s3" to="13" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines absent.</text>
      <biological_entity id="o16248" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves 5-9-foliolate;</text>
      <biological_entity id="o16249" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-9-foliolate" value_original="5-9-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petioles 2-8 cm.</text>
      <biological_entity id="o16250" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaflet blades thick and rigid;</text>
      <biological_entity constraint="leaflet" id="o16251" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>surfaces abaxially smooth, shiny, adaxially dull, gray-green;</text>
      <biological_entity id="o16252" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abaxially" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s8" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="gray-green" value_original="gray-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>terminal leaflet stalked, blade 6.5-9.3 × 4-7 cm, 1.3-2.3 times as long as wide;</text>
      <biological_entity constraint="terminal" id="o16253" name="leaflet" name_original="leaflet" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o16254" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="6.5" from_unit="cm" name="length" src="d0_s9" to="9.3" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s9" to="7" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s9" value="1.3-2.3" value_original="1.3-2.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflet blades ovate or lanceovate, 4-6-veined from base, base truncate or weakly cordate, margins plane, toothed, with 2-7 teeth 3-8 mm tipped with spines to 1.4-4 × 0.3-0.6 mm, apex acuminate.</text>
      <biological_entity constraint="leaflet" id="o16255" name="blade" name_original="blades" src="d0_s10" type="structure" constraint_original="lateral leaflet">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceovate" value_original="lanceovate" />
        <character constraint="from base" constraintid="o16256" is_modifier="false" name="architecture" src="d0_s10" value="4-6-veined" value_original="4-6-veined" />
      </biological_entity>
      <biological_entity id="o16256" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o16257" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s10" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o16258" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s10" value="toothed" value_original="toothed" />
        <character constraint="with spines" constraintid="o16260" is_modifier="false" name="architecture" src="d0_s10" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o16259" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="7" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16260" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16261" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o16258" id="r2241" name="with" negation="false" src="d0_s10" to="o16259" />
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences racemose, dense, 70-150-flowered, 5-17 cm;</text>
      <biological_entity id="o16262" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="density" src="d0_s11" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="70-150-flowered" value_original="70-150-flowered" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s11" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracteoles ± corky, apex rounded to acute.</text>
      <biological_entity id="o16263" name="bracteole" name_original="bracteoles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence_or_texture" src="d0_s12" value="corky" value_original="corky" />
      </biological_entity>
      <biological_entity id="o16264" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s12" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Berries dark blue, glaucous, oblong-ovoid, 9-12 mm, juicy, solid.</text>
      <biological_entity id="o16265" name="berry" name_original="berries" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark blue" value_original="dark blue" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s13" value="juicy" value_original="juicy" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall–winter (Dec–Mar).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="winter" from="fall" />
        <character name="flowering time" char_type="range_value" to="Mar" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woodlands and shrublands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands" modifier="open" />
        <character name="habitat" value="shrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ga., N.C., Va.; native, Asia (China).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="native" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <discussion>Berberis bealei is commonly cultivated; although it rarely escapes, it is locally naturalized in the southeastern United States. It is resistant to infection by Puccinia graminis.</discussion>
  
</bio:treatment>