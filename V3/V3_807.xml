<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(N. I. Malyutin) M. J. Warnock" date="1995" rank="subsection">grumosa</taxon_name>
    <taxon_name authority="D. M. Moore" date="1939" rank="species">newtonianum</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>41: 196. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection grumosa;species newtonianum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500522</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 40-90 cm;</text>
      <biological_entity id="o13373" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base often reddish, puberulent.</text>
      <biological_entity id="o13374" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o13375" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13376" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves usually absent at anthesis;</text>
      <biological_entity constraint="basal" id="o13377" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 2-8 at anthesis;</text>
      <biological_entity constraint="cauline" id="o13378" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="2" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5-10 cm.</text>
      <biological_entity id="o13379" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round to pentagonal, 4-7 × 5-15 cm, nearly glabrous;</text>
      <biological_entity id="o13380" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s6" to="pentagonal" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-7, width 8-20 mm (basal), 5-15 mm (cauline), widest at middle or in proximal 1/2.</text>
      <biological_entity constraint="ultimate" id="o13381" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="7" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character constraint="at middle or in proximal 1/2" is_modifier="false" name="width" src="d0_s7" value="widest" value_original="widest" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 8-20 (-40) -flowered, as wide as long or nearly so;</text>
      <biological_entity id="o13382" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="8-20(-40)-flowered" value_original="8-20(-40)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel 1-4 (-6) cm, pubescent;</text>
      <biological_entity id="o13383" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 6-15 mm from flowers, green, linear, 1.5-5 mm, puberulent.</text>
      <biological_entity id="o13384" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o13385" from="6" from_unit="mm" name="location" src="d0_s10" to="15" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o13385" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals dark to light blue, rarely white, glabrous, lateral sepals spreading, 12-14 × 6-7 mm, spurs straight to decurved, within 30° of horizontal, 10-15 mm;</text>
      <biological_entity id="o13386" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13387" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="dark" name="coloration" src="d0_s11" to="light blue" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o13388" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s11" to="14" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13389" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="decurved" value_original="decurved" />
        <character constraint="of horizontal" name="degree" src="d0_s11" value="30°" value_original="30°" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades slightly elevated, ± exposing stamens, 4-5 mm, clefts 2-3 mm;</text>
      <biological_entity id="o13390" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o13391" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="4" from_unit="mm" modifier="more or less" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13392" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o13393" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o13391" id="r1863" modifier="more or less" name="exposing" negation="false" src="d0_s12" to="o13392" />
    </statement>
    <statement id="d0_s13">
      <text>hairs mostly centered near base of cleft, yellow.</text>
      <biological_entity id="o13394" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o13395" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o13396" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o13395" id="r1864" modifier="mostly" name="centered near" negation="false" src="d0_s13" to="o13396" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 8-12 mm, 3-3.5 times longer than wide, nearly glabrous.</text>
      <biological_entity id="o13397" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="3-3.5" value_original="3-3.5" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds unwinged;</text>
      <biological_entity id="o13398" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="unwinged" value_original="unwinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>surface of each seed-coat cell with swollen, blunt, hairlike structures, barely visible at 20×, otherwise smooth.</text>
      <biological_entity id="o13399" name="surface" name_original="surface" src="d0_s16" type="structure" constraint="cell" constraint_original="cell; cell">
        <character constraint="at 20×" is_modifier="false" modifier="barely" name="prominence" notes="" src="d0_s16" value="visible" value_original="visible" />
        <character is_modifier="false" modifier="otherwise" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="seed-coat" id="o13400" name="cell" name_original="cell" src="d0_s16" type="structure" />
      <biological_entity id="o13401" name="structure" name_original="structures" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="swollen" value_original="swollen" />
        <character is_modifier="true" name="shape" src="d0_s16" value="blunt" value_original="blunt" />
        <character is_modifier="true" name="shape" src="d0_s16" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <relation from="o13399" id="r1865" name="part_of" negation="false" src="d0_s16" to="o13400" />
      <relation from="o13399" id="r1866" name="with" negation="false" src="d0_s16" to="o13401" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Slopes in deciduous forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" constraint="in deciduous forest" />
        <character name="habitat" value="deciduous forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>56.</number>
  <other_name type="common_name">Ozark larkspur</other_name>
  <discussion>No cases of hybridization are known. Delphinium newtonianum often occurs in mixed populations with D. tricorne. It normally does not begin flowering until 4-6 weeks after D. tricorne has finished.</discussion>
  
</bio:treatment>