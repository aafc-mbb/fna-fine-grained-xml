<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="S. Watson" date="1876" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>11: 121. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500073</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="(S. Watson) Freyn" date="unknown" rank="species">occidentalis</taxon_name>
    <taxon_name authority="Hardin" date="unknown" rank="variety">subpilosa</taxon_name>
    <taxon_hierarchy>genus Anemone;species occidentalis;variety subpilosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pulsatilla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">occidentalis</taxon_name>
    <taxon_hierarchy>genus Pulsatilla;species occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 10-60 (-75) cm, from caudices, caudices ascending to vertical.</text>
      <biological_entity id="o287" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o288" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <biological_entity id="o289" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="vertical" />
      </biological_entity>
      <relation from="o287" id="r36" name="from" negation="false" src="d0_s0" to="o288" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves (2-) 3-6 (-8), primarily 3-foliolate with each leaflet pinnatifid to dissected;</text>
      <biological_entity constraint="basal" id="o290" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s1" to="3" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="8" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="6" />
        <character constraint="with leaflet" constraintid="o291" is_modifier="false" modifier="primarily" name="architecture" src="d0_s1" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
      <biological_entity id="o291" name="leaflet" name_original="leaflet" src="d0_s1" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s1" to="dissected" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 6-8 (-12) cm;</text>
      <biological_entity id="o292" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet petiolulate, ovate in outline, (2.5-) 3-6 (-8) cm, base cuneate, margins pinnatifid to dissected throughout, apex narrowly acute, surfaces villous;</text>
      <biological_entity constraint="terminal" id="o293" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolulate" value_original="petiolulate" />
        <character constraint="in outline" constraintid="o294" is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o294" name="outline" name_original="outline" src="d0_s3" type="structure" />
      <biological_entity id="o295" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o296" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="pinnatifid" modifier="throughout" name="shape" src="d0_s3" to="dissected" />
      </biological_entity>
      <biological_entity id="o297" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o298" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lateral leaflets 2×-parted, pinnatifid;</text>
      <biological_entity constraint="lateral" id="o299" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="2×-parted" value_original="2×-parted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate segments 2-3 mm wide.</text>
      <biological_entity constraint="ultimate" id="o300" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1-flowered;</text>
      <biological_entity id="o301" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle woolly or densely villous, glabrate;</text>
      <biological_entity id="o302" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts 3, occasionally more, 1-tiered, ±similar to basal leaves, 3-foliolate, ovate in outline, bases distinct;</text>
      <biological_entity id="o303" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" modifier="occasionally" name="arrangement" src="d0_s8" value="1-tiered" value_original="1-tiered" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="3-foliolate" value_original="3-foliolate" />
        <character constraint="in outline" constraintid="o305" is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o304" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o305" name="outline" name_original="outline" src="d0_s8" type="structure" />
      <biological_entity id="o306" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o303" id="r37" modifier="more or less" name="to" negation="false" src="d0_s8" to="o304" />
    </statement>
    <statement id="d0_s9">
      <text>terminal leaflet petiolulate, 2.5-7 cm (2.5 cm in flower, 7 cm or less in fruit), margins pinnatifid throughout, apex narrowly acute, surfaces villous;</text>
      <biological_entity constraint="terminal" id="o307" name="leaflet" name_original="leaflet" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="petiolulate" value_original="petiolulate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s9" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o308" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="throughout" name="shape" src="d0_s9" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o309" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o310" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflets 2×-parted, pinnatifid;</text>
      <biological_entity constraint="lateral" id="o311" name="leaflet" name_original="leaflets" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="2×-parted" value_original="2×-parted" />
        <character is_modifier="false" name="shape" src="d0_s10" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ultimate segments 2-3 mm wide.</text>
      <biological_entity constraint="ultimate" id="o312" name="segment" name_original="segments" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals 5-7, white, tinged purple (rarely abaxially blue proximally, white distally, and adaxially white), ovate to obovate, rarely elliptic, 15-30 × 10-17 (-19) mm, abaxially hairy, adaxially glabrous;</text>
      <biological_entity id="o313" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o314" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="7" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="tinged purple" value_original="tinged purple" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="obovate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s12" to="30" to_unit="mm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="19" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s12" to="17" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 150-200.</text>
      <biological_entity id="o315" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o316" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="150" name="quantity" src="d0_s13" to="200" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Heads of achenes spheric, rarely cylindric;</text>
      <biological_entity id="o317" name="head" name_original="heads" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="spheric" value_original="spheric" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o318" name="achene" name_original="achenes" src="d0_s14" type="structure" />
      <relation from="o317" id="r38" name="part_of" negation="false" src="d0_s14" to="o318" />
    </statement>
    <statement id="d0_s15">
      <text>pedicel 15-20 (-22) cm.</text>
      <biological_entity id="o319" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s15" to="22" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s15" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes: body ellipsoid, 3-4 × ca. 1.5 mm, not winged, villous;</text>
      <biological_entity id="o320" name="achene" name_original="achenes" src="d0_s16" type="structure" />
      <biological_entity id="o321" name="body" name_original="body" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s16" to="4" to_unit="mm" />
        <character name="width" src="d0_s16" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak curved or recurved, reflexed with age, (18-) 20-40 (-50) mm, long-villous, plumose.</text>
      <biological_entity id="o322" name="achene" name_original="achenes" src="d0_s17" type="structure" />
      <biological_entity id="o324" name="age" name_original="age" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n=16.</text>
      <biological_entity id="o323" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="recurved" value_original="recurved" />
        <character constraint="with age" constraintid="o324" is_modifier="false" name="orientation" src="d0_s17" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="18" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s17" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s17" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="40" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="long-villous" value_original="long-villous" />
        <character is_modifier="false" name="shape" src="d0_s17" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o325" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Aug/Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" constraint="May-Aug/Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly, rocky slopes, moist meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="moist meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Western pasqueflower</other_name>
  <other_name type="common_name">mountain pasqueflower</other_name>
  <other_name type="common_name">pulsatille</other_name>
  <discussion>W. J. Hooker (1829) included Anemone occidentalis in his concept of Anemone alpina Linnaeus.</discussion>
  <discussion>The Thompson Indians and the Okanagan used decoctions prepared from the roots of Anemone occidentalis to treat stomach and bowel troubles (D. E. Moerman 1986).</discussion>
  
</bio:treatment>