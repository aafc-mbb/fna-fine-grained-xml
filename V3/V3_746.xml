<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">castanea</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1768" rank="species">pumila</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Castanea no. 2. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus castanea;species pumila</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500327</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castanea</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">alnifolia</taxon_name>
    <taxon_hierarchy>genus Castanea;species alnifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castanea</taxon_name>
    <taxon_name authority="(Sargent) Ashe" date="unknown" rank="species">alnifolia</taxon_name>
    <taxon_name authority="Sargent" date="unknown" rank="variety">floridana</taxon_name>
    <taxon_hierarchy>genus Castanea;species alnifolia;variety floridana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castanea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">floridana</taxon_name>
    <taxon_hierarchy>genus Castanea;species floridana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castanea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pumila</taxon_name>
    <taxon_name authority="Sudworth" date="unknown" rank="variety">ashei</taxon_name>
    <taxon_hierarchy>genus Castanea;species pumila;variety ashei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, to 15 m, often rhizomatous.</text>
      <biological_entity id="o15508" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark gray to brown, smooth to slightly fissured.</text>
      <biological_entity id="o15510" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s1" to="brown" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s1" to="slightly fissured" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs puberulent with spreading hairs, occasionally glabrate with age.</text>
      <biological_entity id="o15511" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character constraint="with hairs" constraintid="o15512" is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character constraint="with age" constraintid="o15513" is_modifier="false" modifier="occasionally" name="pubescence" notes="" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o15512" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o15513" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 3-7 (-10) mm.</text>
      <biological_entity id="o15514" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o15515" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade narrowly elliptic to narrowly obovate or oblanceolate, 40-210 × 20-80 mm, base rounded to cordate, margins obscurely to sharply serrate, each abruptly acuminate tooth with awn usually less than 2 mm;</text>
      <biological_entity id="o15516" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s4" to="narrowly obovate or oblanceolate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s4" to="210" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="80" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15517" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="cordate" />
      </biological_entity>
      <biological_entity id="o15518" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="obscurely to sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15519" name="tooth" name_original="tooth" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="abruptly" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o15520" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o15519" id="r2151" name="with" negation="false" src="d0_s4" to="o15520" />
    </statement>
    <statement id="d0_s5">
      <text>surfaces abaxially typically densely covered with appressed stellate or erect-woolly, whitish to brown trichomes, sometimes essentially glabrate especially on shade leaves, veins often minutely puberulent.</text>
      <biological_entity id="o15521" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character constraint="with " constraintid="o15522" is_modifier="false" modifier="abaxially typically densely" name="position_relational" src="d0_s5" value="covered" value_original="covered" />
        <character constraint="on leaves" constraintid="o15524" is_modifier="false" modifier="sometimes essentially" name="pubescence" notes="" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o15522" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s5" value="stellate" value_original="stellate" />
        <character is_modifier="true" name="pubescence" src="d0_s5" value="erect-woolly" value_original="erect-woolly" />
        <character char_type="range_value" constraint="with " constraintid="o15523" from="whitish" name="coloration" src="d0_s5" to="brown" />
      </biological_entity>
      <biological_entity id="o15523" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s5" value="stellate" value_original="stellate" />
        <character is_modifier="true" name="pubescence" src="d0_s5" value="erect-woolly" value_original="erect-woolly" />
      </biological_entity>
      <biological_entity id="o15524" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration_or_habitat" src="d0_s5" value="shade" value_original="shade" />
      </biological_entity>
      <biological_entity id="o15525" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often minutely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pistillate flower 1 per cupule.</text>
      <biological_entity id="o15526" name="flower" name_original="flower" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character constraint="per cupule" constraintid="o15527" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15527" name="cupule" name_original="cupule" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Fruits: cupule 2-valved, enclosing 1 flower, valves irregularly dehiscing along 2 sutures, longest spines usually less than 10 mm;</text>
      <biological_entity id="o15528" name="fruit" name_original="fruits" src="d0_s7" type="structure" />
      <biological_entity id="o15529" name="cupule" name_original="cupule" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-valved" value_original="2-valved" />
      </biological_entity>
      <biological_entity id="o15530" name="flower" name_original="flower" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15531" name="valve" name_original="valves" src="d0_s7" type="structure">
        <character constraint="along sutures" constraintid="o15532" is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s7" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o15532" name="suture" name_original="sutures" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="longest" id="o15533" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <relation from="o15529" id="r2152" name="enclosing" negation="false" src="d0_s7" to="o15530" />
    </statement>
    <statement id="d0_s8">
      <text>nut 1 per cupule, ovate-conic, 7-21 × 7-19 mm, round in cross-section, not flattened, beak less than 3mm excluding styles.</text>
      <biological_entity id="o15534" name="fruit" name_original="fruits" src="d0_s8" type="structure" />
      <biological_entity id="o15535" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character constraint="per cupule" constraintid="o15536" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ovate-conic" value_original="ovate-conic" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s8" to="21" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="19" to_unit="mm" />
        <character constraint="in cross-section" constraintid="o15537" is_modifier="false" name="shape" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="false" modifier="not" name="shape" notes="" src="d0_s8" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o15536" name="cupule" name_original="cupule" src="d0_s8" type="structure" />
      <biological_entity id="o15537" name="cross-section" name_original="cross-section" src="d0_s8" type="structure" />
      <biological_entity id="o15538" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15539" name="style" name_original="styles" src="d0_s8" type="structure" />
      <relation from="o15538" id="r2153" name="excluding" negation="false" src="d0_s8" to="o15539" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (May–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="May-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forest, open woods, forest understory, dry sandy and wet sandy barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forest" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="forest understory" />
        <character name="habitat" value="dry sandy" />
        <character name="habitat" value="wet sandy barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., Ind., Ky., La., Md., Mass., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Chinquapin</other_name>
  <other_name type="common_name">chinkapin</other_name>
  <other_name type="common_name">dwarf chestnut</other_name>
  <discussion>Castanea pumila was thought to be extirpated from Long Island, New York, but it was recently recollected in Suffolk County.</discussion>
  <discussion>Numerous names have been applied to populations included here under Castanea pumila (see also C. ozarkensis). In general, the pattern of morphologic variation suggests three forms that have some geographic and ecologic continuity, but among which sufficient clinal variation occurs to prevent easy recognition. Plants of higher elevation and northern populations tend to be trees or large shrubs with densely tomentose vestiture of the abaxial leaf surface, while southern coastal populations typically have flat-stellate vestiture, often very sparse or even glabrate (e.g., C. floridana). These coastal populations may be separated into forms that are trees or large shrubs with leaves sparsely to not at all glandular versus low rhizomatous shrubs with prominent globose glands on twigs and leaves (centered in northern Florida and adjacent Georgia). The latter form is probably the same as C. alnifolia.</discussion>
  <discussion>Various preparations of the leaves of Castanea alnifolia were used by Native Americans medicinally to relieve headaches and as a wash for chills and cold sweats; preparations from unspecified parts of the plants were used to treat fever blisters (D. E. Moerman 1986).</discussion>
  
</bio:treatment>