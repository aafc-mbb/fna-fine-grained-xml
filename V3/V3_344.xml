<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="unknown" rank="section">Elatopsis</taxon_name>
    <taxon_name authority="W. T. Wang" date="1962" rank="subsection">Elata</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">elatum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 531. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section elatopsis;subsection elata;species elatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200007828</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 40-200 cm;</text>
      <biological_entity id="o24978" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base green, pubescent or glabrous.</text>
      <biological_entity id="o24979" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, 7-26 at anthesis;</text>
      <biological_entity id="o24980" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" constraint="at anthesis" from="7" name="quantity" src="d0_s2" to="26" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1-18 cm.</text>
      <biological_entity id="o24981" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade round to pentagonal, 3-15 × 6-22 cm, ± puberulent;</text>
      <biological_entity id="o24982" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s4" to="pentagonal" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s4" to="22" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 3-9, width 8-30 mm.</text>
      <biological_entity constraint="ultimate" id="o24983" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="9" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 25-100 (-more) -flowered;</text>
      <biological_entity id="o24984" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="25-100-flowered" value_original="25-100-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicel 1-3 (-5) cm, glabrous to pubescent;</text>
      <biological_entity id="o24985" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s7" to="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 2-5 (-9) mm from flowers, green, linear, 5-9 mm, ± puberulent.</text>
      <biological_entity id="o24986" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o24987" from="2" from_unit="mm" name="location" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o24987" name="flower" name_original="flowers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals blue, white, or purple, ± puberulent, lateral sepals spreading, 12-23 × 4-12 mm, spurs straight, ascending ca. 45° above horizontal, 15-22 mm;</text>
      <biological_entity id="o24988" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24989" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o24990" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s9" to="23" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24991" name="spur" name_original="spurs" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character constraint="above horizontal" is_modifier="false" modifier="45°" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower petal blades elevated, exposing stamens, 3-5 mm, clefts 0.2-1 mm;</text>
      <biological_entity id="o24992" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="petal" id="o24993" name="blade" name_original="blades" src="d0_s10" type="structure" constraint_original="lower petal">
        <character is_modifier="false" name="prominence" src="d0_s10" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24994" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o24995" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o24993" id="r3391" name="exposing" negation="false" src="d0_s10" to="o24994" />
    </statement>
    <statement id="d0_s11">
      <text>hairs sparse or dense, mostly near center of blade, yellow or white.</text>
      <biological_entity id="o24996" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o24997" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="density" src="d0_s11" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="density" src="d0_s11" value="dense" value_original="dense" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o24998" name="center" name_original="center" src="d0_s11" type="structure" />
      <biological_entity id="o24999" name="blade" name_original="blade" src="d0_s11" type="structure" />
      <relation from="o24997" id="r3392" modifier="mostly" name="near" negation="false" src="d0_s11" to="o24998" />
      <relation from="o24998" id="r3393" name="part_of" negation="false" src="d0_s11" to="o24999" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits 13-20 mm, 3.5-4.5 times longer than wide, ± puberulent.</text>
      <biological_entity id="o25000" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="3.5-4.5" value_original="3.5-4.5" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds winged;</text>
      <biological_entity id="o25001" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coats ± with small wavy ridges, cells elongate, surface roughened.</text>
      <biological_entity id="o25002" name="seed-coat" name_original="seed-coats" src="d0_s14" type="structure" />
      <biological_entity id="o25003" name="ridge" name_original="ridges" src="d0_s14" type="structure">
        <character is_modifier="true" name="size" src="d0_s14" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s14" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o25004" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o25005" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief_or_texture" src="d0_s14" value="roughened" value_original="roughened" />
      </biological_entity>
      <relation from="o25002" id="r3394" name="with" negation="false" src="d0_s14" to="o25003" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer, more than 8 weeks after snowmelt.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="more than 8 weeks after snowmelt" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Old homesites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="old homesites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Man., Sask., and probably elsewhere; native to Europe and w Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" value="and probably elsewhere" establishment_means="introduced" />
        <character name="distribution" value="native to Europe and w Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Delphinium elatum is cultivated as a garden plant or for cut flowers. It is not known to be naturalized extensively in North America; it may persist long after cultivation in cooler parts of the region.</discussion>
  
</bio:treatment>