<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="N. I. Malyutin" date="1987" rank="subsection">exaltata</taxon_name>
    <taxon_name authority="Ewan" date="1939" rank="species">andesicola</taxon_name>
    <place_of_publication>
      <publication_title>J. Washington Acad. Sci.</publication_title>
      <place_in_publication>29: 476. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection exaltata;species andesicola;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500472</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Ewan" date="unknown" rank="species">andesicola</taxon_name>
    <taxon_name authority="(Ewan) Ewan" date="unknown" rank="subspecies">amplum</taxon_name>
    <taxon_hierarchy>genus Delphinium;species andesicola;subspecies amplum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 60-200 +cm;</text>
      <biological_entity id="o25681" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base reddish or not, glabrous, glaucous.</text>
      <biological_entity id="o25682" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character name="coloration" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, 10-30, absent from proximal 1/5 of stem at anthesis;</text>
      <biological_entity id="o25683" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="30" />
        <character constraint="from proximal 1/5" constraintid="o25684" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o25684" name="1/5" name_original="1/5" src="d0_s2" type="structure" />
      <biological_entity id="o25685" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o25684" id="r3461" name="part_of" negation="false" src="d0_s2" to="o25685" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1-15 cm.</text>
      <biological_entity id="o25686" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade cordate to semicircular, 5-8 × 5-12 cm, nearly glabrous;</text>
      <biological_entity id="o25687" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s4" to="semicircular" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s4" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 5-16, width 3-20 mm, tips gradually tapered to mucronate apex;</text>
      <biological_entity constraint="ultimate" id="o25688" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="16" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25689" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character char_type="range_value" from="gradually tapered" name="shape" src="d0_s5" to="mucronate" />
      </biological_entity>
      <biological_entity id="o25690" name="apex" name_original="apex" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>midcauline leaf lobes more than 3 times longer than wide.</text>
      <biological_entity constraint="leaf" id="o25691" name="lobe" name_original="lobes" src="d0_s6" type="structure" constraint_original="midcauline leaf">
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="3+" value_original="3+" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 20-80-flowered;</text>
      <biological_entity id="o25692" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="20-80-flowered" value_original="20-80-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 1-2 (-3) cm, puberulent;</text>
      <biological_entity id="o25693" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles 1-3 mm from flowers, green to brown, linear-lanceolate, 3-6 mm, puberulent.</text>
      <biological_entity id="o25694" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o25695" from="1" from_unit="mm" name="location" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s9" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o25695" name="flower" name_original="flowers" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals purple, puberulent, lateral sepals spreading, 9-12 × 5-7 mm, spurs ascending ca. 45°, curved downward apically, purple, 10-13 mm, blunt-tipped;</text>
      <biological_entity id="o25696" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25697" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o25698" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25699" name="spur" name_original="spurs" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="45°" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="apically" name="orientation" src="d0_s10" value="downward" value_original="downward" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="blunt-tipped" value_original="blunt-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower petal blades ± covering stamens, 4-6 mm, clefts 1.5-2.5 mm;</text>
      <biological_entity id="o25700" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="petal" id="o25701" name="blade" name_original="blades" src="d0_s11" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25702" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o25703" name="cleft" name_original="clefts" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o25701" id="r3462" modifier="more or less" name="covering" negation="false" src="d0_s11" to="o25702" />
    </statement>
    <statement id="d0_s12">
      <text>hairs centered, densest on inner lobes near base of cleft, white.</text>
      <biological_entity id="o25704" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o25705" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="centered" value_original="centered" />
        <character constraint="on inner lobes" constraintid="o25706" is_modifier="false" name="density" src="d0_s12" value="densest" value_original="densest" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="inner" id="o25706" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
      <biological_entity id="o25707" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o25706" id="r3463" name="near" negation="false" src="d0_s12" to="o25707" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits 12-15 mm, 3.5-4 times longer than wide, sparsely puberulent.</text>
      <biological_entity id="o25708" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s13" to="15" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s13" value="3.5-4" value_original="3.5-4" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds unwinged;</text>
      <biological_entity id="o25709" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="unwinged" value_original="unwinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>seed-coat cells elongate, surfaces pustulate.</text>
      <biological_entity constraint="seed-coat" id="o25710" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 16.</text>
      <biological_entity id="o25711" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief" src="d0_s15" value="pustulate" value_original="pustulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25712" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows and coniferous woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="coniferous woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200-3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion>Delphinium andesicola, the westernmost representative of the southern Cordilleran complex, is found in the Chiricahua, Huachuca, Graham, and White mountains. Hybrids with Delphinium scopulorum are known.</discussion>
  
</bio:treatment>