<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Henry Loconte</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">CAULOPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 204. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus CAULOPHYLLUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek caulos, stem, and phyllos, leaf</other_info_on_name>
    <other_info_on_name type="fna_id">105942</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, deciduous, to 2-9 dm, glabrous.</text>
      <biological_entity id="o5969" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="9" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes nodose, producing 2 leaves per year.</text>
      <biological_entity id="o5970" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="nodose" value_original="nodose" />
      </biological_entity>
      <biological_entity id="o5971" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5972" name="year" name_original="year" src="d0_s1" type="structure" />
      <relation from="o5970" id="r840" name="producing" negation="false" src="d0_s1" to="o5971" />
      <relation from="o5970" id="r841" name="per" negation="false" src="d0_s1" to="o5972" />
    </statement>
    <statement id="d0_s2">
      <text>Aerial stems present.</text>
      <biological_entity id="o5973" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves caducous, cauline, 2-ranked, 1st leaf larger than 2d leaf, 2-4-ternately compound;</text>
      <biological_entity id="o5974" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="caducous" value_original="caducous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o5975" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
      <biological_entity id="o5976" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character constraint="than 2d leaf" constraintid="o5977" is_modifier="false" name="size" src="d0_s3" value="larger" value_original="larger" />
        <character is_modifier="false" modifier="2-4-ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o5977" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petioles short or absent.</text>
      <biological_entity id="o5978" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade broadly obovate in overall outline;</text>
      <biological_entity id="o5979" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character constraint="in outline" constraintid="o5980" is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o5980" name="outline" name_original="outline" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>rachis pulvinate;</text>
      <biological_entity id="o5981" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pulvinate" value_original="pulvinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflet blades broadly obovate, entire or lobed, margins not toothed;</text>
      <biological_entity constraint="leaflet" id="o5982" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>venation pinnate to palmate.</text>
      <biological_entity id="o5983" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character char_type="range_value" from="pinnate" name="architecture" src="d0_s8" to="palmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences terminal, compound cymes.</text>
      <biological_entity id="o5984" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o5985" name="cyme" name_original="cymes" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers 3-merous, 6-20 mm;</text>
      <biological_entity id="o5986" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-merous" value_original="3-merous" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracteoles caducous, 3-4, sepaloid;</text>
      <biological_entity id="o5987" name="bracteole" name_original="bracteoles" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="caducous" value_original="caducous" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sepaloid" value_original="sepaloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals 6, yellow, purple, red, brown, or green, petaloid;</text>
      <biological_entity id="o5988" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="petaloid" value_original="petaloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 6, fan-shaped, bearing nectar;</text>
      <biological_entity id="o5989" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
        <character is_modifier="false" name="shape" src="d0_s13" value="fan--shaped" value_original="fan--shaped" />
      </biological_entity>
      <biological_entity id="o5990" name="nectar" name_original="nectar" src="d0_s13" type="structure" />
      <relation from="o5989" id="r842" name="bearing" negation="false" src="d0_s13" to="o5990" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 6;</text>
      <biological_entity id="o5991" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers dehiscing by 2 apically hinged flaps;</text>
      <biological_entity id="o5992" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character constraint="by flaps" constraintid="o5993" is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o5993" name="flap" name_original="flaps" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pollen exine reticulate;</text>
      <biological_entity constraint="pollen" id="o5994" name="exine" name_original="exine" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s16" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary bladderlike;</text>
    </statement>
    <statement id="d0_s18">
      <text>placentation appearing basal;</text>
      <biological_entity id="o5995" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="bladderlike" value_original="bladderlike" />
        <character is_modifier="false" name="placentation" src="d0_s18" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>styles eccentric.</text>
      <biological_entity id="o5996" name="style" name_original="styles" src="d0_s19" type="structure">
        <character is_modifier="false" name="position" src="d0_s19" value="eccentric" value_original="eccentric" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits not developed, ovary wall soon rupturing.</text>
      <biological_entity id="o5997" name="fruit" name_original="fruits" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="not" name="development" src="d0_s20" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="ovary" id="o5998" name="wall" name_original="wall" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="soon" name="dehiscence" src="d0_s20" value="rupturing" value_original="rupturing" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds 2, elevated on elongating stalk, naked at maturity;</text>
      <biological_entity id="o5999" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="2" value_original="2" />
        <character constraint="on stalk" constraintid="o6000" is_modifier="false" name="prominence" src="d0_s21" value="elevated" value_original="elevated" />
        <character constraint="at maturity" is_modifier="false" name="architecture" notes="" src="d0_s21" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o6000" name="stalk" name_original="stalk" src="d0_s21" type="structure">
        <character is_modifier="true" name="length" src="d0_s21" value="elongating" value_original="elongating" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>seed-coat blue, fleshy, glaucous;</text>
      <biological_entity id="o6001" name="seed-coat" name_original="seed-coat" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s22" value="blue" value_original="blue" />
        <character is_modifier="false" name="texture" src="d0_s22" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s22" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>aril absent.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 8.</text>
      <biological_entity id="o6002" name="aril" name_original="aril" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o6003" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Blue cohosh</other_name>
  <other_name type="common_name">caulophylle</other_name>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Caulophyllum species are understory herbs of mesophytic forests, alluvial flats, rich mesic slopes, and limestone slopes. The seeds of Caulophyllum are buoyant and showy and may be dispersed by water as well as other means; seed germination is hypogeal, the cotyledons remaining underground. Caulophyllum is occasionally cultivated in woodland gardens.</discussion>
  <references>
    <reference>Brett, J. F. 1981. The Morphology and Taxonomy of Caulophyllum thalictroides (L.) Michx. (Berberidaceae) in North America. M.S. thesis. University of Guelph.</reference>
    <reference>Loconte, H. and J. R. Estes. 1989. Generic relationships within Leonticeae (Berberidaceae). Canad. J. Bot. 67: 2310-2316.</reference>
    <reference>Moore, R. J. 1963. Karyotype evolution in Caulophyllum. Canad. J. Genet. Cytol. 5: 384-388.</reference>
    <reference>Loconte, H. and W. H. Blackwell. 1985. Intrageneric taxonomy of Caulophyllum (Berberidaceae). Rhodora 87: 463-469.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistil 3–5 mm; style 1–2 mm; stamen filaments 1.5–2.5 mm; sepals 6–9 mm; inflorescences with 4–18 flowers; 1st leaf (2–)3-ternate; leaflets 5–10 cm.</description>
      <determination>1 Caulophyllum giganteum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistil 1–3 mm; style 0.25–1 mm; stamen filaments 0.5–1.5 mm; sepals 3–6 mm; inflorescences with 5– 70 flowers; 1st leaf 3(–4)-ternate; leaflets 3–8 cm.</description>
      <determination>2 Caulophyllum thalictroides</determination>
    </key_statement>
  </key>
</bio:treatment>