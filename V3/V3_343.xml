<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Chamisso in C. G. D. Nees" date="1793" rank="genus">eschscholzia</taxon_name>
    <taxon_name authority="Bentham" date="1835" rank="species">hypecoides</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Hort. Soc. London, ser.</publication_title>
      <place_in_publication>2, 1: 408. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus eschscholzia;species hypecoides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500636</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eschscholzia</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">caespitosa</taxon_name>
    <taxon_name authority="(Bentham) A. Gray" date="unknown" rank="variety">hypecoides</taxon_name>
    <taxon_hierarchy>genus Eschscholzia;species caespitosa;variety hypecoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, caulescent, erect, 5-30 cm, sparsely pubescent.</text>
      <biological_entity id="o19798" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o19799" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade with ultimate lobes usually obtuse.</text>
      <biological_entity id="o19800" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity constraint="ultimate" id="o19801" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o19800" id="r2670" name="with" negation="false" src="d0_s2" to="o19801" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences cymose or 1-flowered;</text>
      <biological_entity id="o19802" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>buds nodding.</text>
      <biological_entity id="o19803" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: receptacle turbinate, less than 1.5 mm broad, cup without spreading free rim;</text>
      <biological_entity id="o19804" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o19805" name="receptacle" name_original="receptacle" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19806" name="cup" name_original="cup" src="d0_s5" type="structure" />
      <biological_entity id="o19807" name="rim" name_original="rim" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="fusion" src="d0_s5" value="free" value_original="free" />
      </biological_entity>
      <relation from="o19806" id="r2671" name="without" negation="false" src="d0_s5" to="o19807" />
    </statement>
    <statement id="d0_s6">
      <text>calyx acute, pubescent;</text>
      <biological_entity id="o19808" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19809" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals yellow, sometimes with orange spot at base, 10-20 mm.</text>
      <biological_entity id="o19810" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19811" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character constraint="at base" constraintid="o19812" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="orange spot" value_original="orange spot" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19812" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Capsules 3-7 cm.</text>
      <biological_entity id="o19813" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds brown, spheric to ellipsoid, 1-1.3 mm, reticulate.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 12.</text>
      <biological_entity id="o19814" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s9" to="ellipsoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s9" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19815" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy areas in woodland, chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy areas" constraint="in woodland" />
        <character name="habitat" value="woodland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  
</bio:treatment>