<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">urticaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">parietaria</taxon_name>
    <taxon_name authority="B. D. Hinton" date="1968" rank="species">praetermissa</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>3: 192. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family urticaceae;genus parietaria;species praetermissa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500866</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 1-5.5 dm.</text>
      <biological_entity id="o19307" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="5.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems freely branched, decumbent to ascending.</text>
      <biological_entity id="o19308" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="freely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades narrowly to broadly ovate or rarely lanceolate, 1-6.5 × 0.6-4 cm, base rounded or very broadly cuneate, apex short to long-acuminate or attenuate.</text>
      <biological_entity id="o19309" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19310" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="very broadly" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19311" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s2" value="long-acuminate" value_original="long-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: involucral-bracts 1.3-4.7 mm;</text>
      <biological_entity id="o19312" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o19313" name="involucral-bract" name_original="involucral-bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s3" to="4.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>tepals 1.7-2.3 mm, equal to or shorter than bracts.</text>
      <biological_entity id="o19314" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o19315" name="tepal" name_original="tepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s4" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s4" value="equal" value_original="equal" />
        <character constraint="than bracts" constraintid="o19316" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o19316" name="bract" name_original="bracts" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Achenes light-brown, asymmetric, 1-1.4 × 0.7-1.1 mm, apex obtuse, mucro subapical;</text>
      <biological_entity id="o19317" name="achene" name_original="achenes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s5" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s5" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19318" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o19319" name="mucro" name_original="mucro" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="subapical" value_original="subapical" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipe slanting, short-cylindric, not centered, basally dilated.</text>
      <biological_entity id="o19320" name="stipe" name_original="stipe" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="slanting" value_original="slanting" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-cylindric" value_original="short-cylindric" />
        <character is_modifier="false" modifier="not" name="position" src="d0_s6" value="centered" value_original="centered" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s6" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shell mounds, calcareous outcrops, hammocks, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shell mounds" />
        <character name="habitat" value="calcareous outcrops" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., La., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Parietaria praetermissa is endemic to the Atlantic and Gulf coastal plains of southeastern United States. The name Parietaria floridana has been incorrectly applied to this species.</discussion>
  
</bio:treatment>