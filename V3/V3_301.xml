<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">menispermaceae</taxon_name>
    <taxon_name authority="de Candolle" date="1818" rank="genus">cocculus</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle" date="1818" rank="species">carolinus</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>1: 524. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family menispermaceae;genus cocculus;species carolinus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500419</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Menispermum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">carolinum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 340. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Menispermum;species carolinum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epibaterium</taxon_name>
    <taxon_name authority="(Linnaeus) Britton" date="unknown" rank="species">carolinum</taxon_name>
    <taxon_hierarchy>genus Epibaterium;species carolinum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines, to 5 m or more;</text>
      <biological_entity id="o15448" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes to 1.4 cm diam.</text>
      <biological_entity id="o15449" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s1" to="1.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems with spreading pubescence.</text>
      <biological_entity id="o15450" name="stem" name_original="stems" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole to 10 cm.</text>
      <biological_entity id="o15451" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o15452" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade generally ovate or deltate, sometimes sagittate or hastate, to 17 × 14 cm, membranous to leathery, base sometimes with 3-5 lobes, margins usually entire, apex acuminate to rounded, then often retuse, mucronate;</text>
      <biological_entity id="o15453" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="generally" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="deltate" value_original="deltate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="hastate" value_original="hastate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s4" to="17" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s4" to="leathery" />
      </biological_entity>
      <biological_entity id="o15454" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o15455" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o15456" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15457" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <relation from="o15454" id="r2149" name="with" negation="false" src="d0_s4" to="o15455" />
    </statement>
    <statement id="d0_s5">
      <text>surfaces abaxially slightly pale, rarely glaucous, sparsely to densely pilose, adaxially glabrous to sparsely pilose;</text>
    </statement>
    <statement id="d0_s6">
      <text>venation 5.</text>
      <biological_entity id="o15458" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially slightly" name="coloration" src="d0_s5" value="pale" value_original="pale" />
        <character char_type="range_value" from="rarely glaucous" name="pubescence" src="d0_s5" to="sparsely densely pilose adaxially glabrous" />
        <character char_type="range_value" from="rarely glaucous" name="pubescence" src="d0_s5" to="sparsely densely pilose adaxially glabrous" />
        <character char_type="range_value" from="rarely glaucous" name="pubescence" src="d0_s5" to="sparsely densely pilose adaxially glabrous" />
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences to 22 cm;</text>
      <biological_entity id="o15459" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="22" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles often present;</text>
      <biological_entity id="o15460" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachis glabrous or tomentose, not glaucous.</text>
      <biological_entity id="o15461" name="rachis" name_original="rachis" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s9" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: perianth parts not glaucous;</text>
      <biological_entity id="o15462" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="perianth" id="o15463" name="part" name_original="parts" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s10" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals in 3 series, outer sepals 0-3, ovate, 0.3-1.4 × 0.2-0.8 mm, pilose, middle 3 sepals ovate to elliptic or obovate, 1-3 × 0.6-2 mm, glabrous to pilose, inner 3 sepals elliptic to nearly orbiculate or obovate, 0.8-3 × 0.8-2 mm, glabrous to sparsely pilose;</text>
      <biological_entity id="o15464" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15465" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o15466" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o15467" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s11" to="3" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s11" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s11" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="position" src="d0_s11" value="middle" value_original="middle" />
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o15468" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="elliptic or obovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s11" to="pilose" />
      </biological_entity>
      <biological_entity constraint="inner" id="o15469" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o15470" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="nearly orbiculate or obovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s11" to="sparsely pilose" />
      </biological_entity>
      <relation from="o15465" id="r2150" name="in" negation="false" src="d0_s11" to="o15466" />
    </statement>
    <statement id="d0_s12">
      <text>petals (5-) 6, yellow, elliptic, deltate, rhombic, obovate, or nearly orbiculate, 0.6-2 × 0.4-1.4 mm, glabrous.</text>
      <biological_entity id="o15471" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o15472" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s12" to="6" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s12" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s12" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s12" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Staminate flowers: stamens (5-) 6, to 2.2 mm;</text>
      <biological_entity id="o15473" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o15474" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s13" to="6" to_inclusive="false" />
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistillodes to 0.5 mm.</text>
      <biological_entity id="o15475" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o15476" name="pistillode" name_original="pistillodes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: staminodes to 0.8 mm;</text>
      <biological_entity id="o15477" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15478" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pistils to 2 mm.</text>
      <biological_entity id="o15479" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15480" name="pistil" name_original="pistils" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Drupes red, 5-8 mm diam. 2n = 78.</text>
      <biological_entity id="o15481" name="drupe" name_original="drupes" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="red" value_original="red" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s17" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15482" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="78" value_original="78" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodland and shrub borders, along streams, fencerows, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodland" constraint="along streams , fencerows , waste places" />
        <character name="habitat" value="shrub borders" constraint="along streams , fencerows , waste places" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., Ill., Ind., Kans., Ky., La., Miss., Mo., N.C., Okla., S.C., Tenn., Tex., Va.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Red-berried moonseed</other_name>
  <other_name type="common_name">coral vine</other_name>
  <other_name type="common_name">margil</other_name>
  <other_name type="common_name">hierba de ojo</other_name>
  <discussion>Cocculus carolinus was used by some Native American tribes medicinally to treat blood ailments (D. E. Moerman 1986).</discussion>
  
</bio:treatment>