<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="N. I. Malyutin" date="1987" rank="subsection">exaltata</taxon_name>
    <taxon_name authority="Tidestrom" date="1902" rank="species">sapellonis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>34: 453. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection exaltata;species sapellonis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500548</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (50-) 100-180 (-220) cm;</text>
      <biological_entity id="o17966" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="180" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="220" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="180" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base sometimes reddish, glabrous sometimes glaucous.</text>
      <biological_entity id="o17967" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, 10-20, absent from proximal 1/5 of stem at anthesis;</text>
      <biological_entity id="o17968" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="20" />
        <character constraint="from proximal 1/5" constraintid="o17969" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17969" name="1/5" name_original="1/5" src="d0_s2" type="structure" />
      <biological_entity id="o17970" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o17969" id="r2419" name="part_of" negation="false" src="d0_s2" to="o17970" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 5-12 cm.</text>
      <biological_entity id="o17971" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade round to pentagonal, 6-10 × 8-16 cm, nearly glabrous;</text>
      <biological_entity id="o17972" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s4" to="pentagonal" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s4" to="16" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 5-15, width 5-25 mm.</text>
      <biological_entity constraint="ultimate" id="o17973" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="15" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences (12-) 30-80 (-120) -flowered;</text>
      <biological_entity id="o17974" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="(12-)30-80(-120)-flowered" value_original="(12-)30-80(-120)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicel 0.5-2 cm, glandular-puberulent;</text>
      <biological_entity id="o17975" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 3-5 mm from flowers, green to purple, linear, 5-8 mm, glandular-puberulent.</text>
      <biological_entity id="o17976" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o17977" from="3" from_unit="mm" name="location" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s8" to="purple" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o17977" name="flower" name_original="flowers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals (in bud) yellowish or brownish purple, becoming browner or yellower with age, glandular-puberulent, lateral sepals forward pointing, 8-12 × 3-5 mm, spurs straight, ascending 20-45° above horizontal, 8-11 mm;</text>
      <biological_entity id="o17978" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17979" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brownish purple" value_original="brownish purple" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s9" value="browner" value_original="browner" />
        <character name="coloration" src="d0_s9" value="yellower" value_original="yellower" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s9" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o17980" name="age" name_original="age" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o17981" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="forward" name="orientation" src="d0_s9" value="pointing" value_original="pointing" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17982" name="spur" name_original="spurs" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character constraint="above horizontal" is_modifier="false" modifier="20-45°" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
      <relation from="o17979" id="r2420" name="with" negation="false" src="d0_s9" to="o17980" />
    </statement>
    <statement id="d0_s10">
      <text>lower petal blades slightly elevated, ± exposing stamens, 2.5-5 mm, clefts 1-2 mm;</text>
      <biological_entity id="o17983" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="petal" id="o17984" name="blade" name_original="blades" src="d0_s10" type="structure" constraint_original="lower petal">
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s10" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="2.5" from_unit="mm" modifier="more or less" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17985" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o17986" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o17984" id="r2421" modifier="more or less" name="exposing" negation="false" src="d0_s10" to="o17985" />
    </statement>
    <statement id="d0_s11">
      <text>hairs centered, mostly above base of cleft, yellow.</text>
      <biological_entity id="o17987" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17988" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="centered" value_original="centered" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o17989" name="base" name_original="base" src="d0_s11" type="structure" />
      <relation from="o17988" id="r2422" modifier="mostly" name="above" negation="false" src="d0_s11" to="o17989" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits 12-18 mm, 3-4 times longer than wide, puberulent.</text>
      <biological_entity id="o17990" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="18" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="3-4" value_original="3-4" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds wing-margined;</text>
      <biological_entity id="o17991" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat cells elongate, surfaces smooth.</text>
      <biological_entity constraint="seed-coat" id="o17992" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o17993" name="surface" name_original="surfaces" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine meadows and open coniferous forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine meadows" />
        <character name="habitat" value="open coniferous" />
        <character name="habitat" value="forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2600-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Sapello Canyon larkspur</other_name>
  <discussion>Delphinium sapellonis hybridizes with D. barbeyi and D. robustum. It replaces D. robustum and represents the southern Cordilleran complex at higher elevations of the southern Sangre de Cristo Mountains east of Santa Fe. It is not known elsewhere.</discussion>
  
</bio:treatment>