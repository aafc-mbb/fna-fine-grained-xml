<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Hjelmquist" date="1948" rank="genus">CHRYSOLEPIS</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Not., Suppl.</publication_title>
      <place_in_publication>2(1): 117. 1948</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus CHRYSOLEPIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chrysos, gold, and lepis, scale, referring to yellow glands on various organs of the plant</other_info_on_name>
    <other_info_on_name type="fna_id">106995</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, evergreen.</text>
      <biological_entity id="o24037" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Terminal buds present, ovoid or subglobose, scales imbricate.</text>
      <biological_entity constraint="terminal" id="o24039" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s1" value="subglobose" value_original="subglobose" />
      </biological_entity>
      <biological_entity id="o24040" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules prominent on new growth, often persistent around buds.</text>
      <biological_entity id="o24041" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24042" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character constraint="on growth" constraintid="o24043" is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character constraint="around buds" constraintid="o24044" is_modifier="false" modifier="often" name="duration" notes="" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o24043" name="growth" name_original="growth" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="new" value_original="new" />
      </biological_entity>
      <biological_entity id="o24044" name="bud" name_original="buds" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade thick, leathery, margins entire or obscurely toothed, secondary-veins obscure, branching and anastomosing before reaching margin.</text>
      <biological_entity id="o24045" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s3" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o24046" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o24047" name="secondary-vein" name_original="secondary-veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
      <biological_entity id="o24048" name="margin" name_original="margin" src="d0_s3" type="structure" />
      <relation from="o24047" id="r3282" name="reaching" negation="false" src="d0_s3" to="o24048" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences staminate or androgynous, axillary, clustered at ends of branches, spicate, ascending, rigid or flexible;</text>
      <biological_entity id="o24049" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="androgynous" value_original="androgynous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character constraint="at ends" constraintid="o24050" is_modifier="false" name="arrangement_or_growth_form" src="d0_s4" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="spicate" value_original="spicate" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="fragility" src="d0_s4" value="pliable" value_original="flexible" />
      </biological_entity>
      <biological_entity id="o24050" name="end" name_original="ends" src="d0_s4" type="structure" />
      <biological_entity id="o24051" name="branch" name_original="branches" src="d0_s4" type="structure" />
      <relation from="o24050" id="r3283" name="part_of" negation="false" src="d0_s4" to="o24051" />
    </statement>
    <statement id="d0_s5">
      <text>androgynous inflorescences with pistillate cupules/flowers toward base and staminate flowers distally.</text>
      <biological_entity id="o24052" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s5" value="androgynous" value_original="androgynous" />
      </biological_entity>
      <biological_entity id="o24053" name="flower" name_original="cupules/flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24054" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o24055" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o24052" id="r3284" name="with" negation="false" src="d0_s5" to="o24053" />
      <relation from="o24053" id="r3285" name="toward" negation="false" src="d0_s5" to="o24054" />
      <relation from="o24053" id="r3286" name="toward" negation="false" src="d0_s5" to="o24055" />
    </statement>
    <statement id="d0_s6">
      <text>Staminate flowers: sepals distinct;</text>
      <biological_entity id="o24056" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24057" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens (6-) 12 (-18), typically surrounding indurate pistillode covered with silky hairs.</text>
      <biological_entity id="o24058" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24059" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s7" to="12" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="18" />
        <character name="quantity" src="d0_s7" value="12" value_original="12" />
      </biological_entity>
      <biological_entity id="o24060" name="pistillode" name_original="pistillode" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="typically" name="position_relational" src="d0_s7" value="surrounding" value_original="surrounding" />
        <character is_modifier="true" name="texture" src="d0_s7" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o24061" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s7" value="silky" value_original="silky" />
      </biological_entity>
      <relation from="o24060" id="r3287" name="covered with" negation="false" src="d0_s7" to="o24061" />
    </statement>
    <statement id="d0_s8">
      <text>Pistillate flowers (1-) 3 or more per cupule;</text>
      <biological_entity id="o24062" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character constraint="per cupule" constraintid="o24063" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o24063" name="cupule" name_original="cupule" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals distinct;</text>
      <biological_entity id="o24064" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>carpels and styles typically 3.</text>
      <biological_entity id="o24065" name="carpel" name_original="carpels" src="d0_s10" type="structure" />
      <biological_entity id="o24066" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits: maturation in 2d year following pollination (termed biennial by many authors);</text>
      <biological_entity id="o24067" name="fruit" name_original="fruits" src="d0_s11" type="structure" />
      <biological_entity id="o24068" name="year" name_original="year" src="d0_s11" type="structure">
        <character is_modifier="true" name="maturation" src="d0_s11" value="2d" value_original="2d" />
      </biological_entity>
      <relation from="o24067" id="r3288" name="in" negation="false" src="d0_s11" to="o24068" />
    </statement>
    <statement id="d0_s12">
      <text>cupule 2-several-valved, valves distinct, completely enclosing nuts, densely spiny, spines irregularly branched, interlocking, without simple hairs, with large, yellowish, multicellular glands;</text>
      <biological_entity id="o24069" name="fruit" name_original="fruits" src="d0_s12" type="structure" />
      <biological_entity id="o24070" name="cupule" name_original="cupule" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-several-valved" value_original="2-several-valved" />
      </biological_entity>
      <biological_entity id="o24071" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="densely" name="architecture_or_shape" src="d0_s12" value="spiny" value_original="spiny" />
      </biological_entity>
      <biological_entity id="o24072" name="nut" name_original="nuts" src="d0_s12" type="structure" />
      <biological_entity id="o24073" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s12" value="branched" value_original="branched" />
        <character is_modifier="false" name="position_relational" src="d0_s12" value="interlocking" value_original="interlocking" />
      </biological_entity>
      <biological_entity id="o24074" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o24075" name="gland" name_original="glands" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="large" value_original="large" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <relation from="o24071" id="r3289" modifier="completely" name="enclosing" negation="false" src="d0_s12" to="o24072" />
      <relation from="o24073" id="r3290" name="without" negation="false" src="d0_s12" to="o24074" />
      <relation from="o24073" id="r3291" name="with" negation="false" src="d0_s12" to="o24075" />
    </statement>
    <statement id="d0_s13">
      <text>nuts (1-) 3-several per cupule, 3-angled to rounded in cross-section, not winged, adjacent nuts separated from each other by internal cupule valves.</text>
      <biological_entity id="o24076" name="fruit" name_original="fruits" src="d0_s13" type="structure" />
      <biological_entity id="o24077" name="nut" name_original="nuts" src="d0_s13" type="structure">
        <character constraint="per cupule" constraintid="o24078" is_modifier="false" name="quantity" src="d0_s13" value="(1-)3-several" value_original="(1-)3-several" />
        <character char_type="range_value" constraint="in cross-section" constraintid="o24079" from="3-angled" name="shape" notes="" src="d0_s13" to="rounded" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s13" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o24078" name="cupule" name_original="cupule" src="d0_s13" type="structure" />
      <biological_entity id="o24079" name="cross-section" name_original="cross-section" src="d0_s13" type="structure" />
      <biological_entity constraint="cupule" id="o24081" name="valve" name_original="valves" src="d0_s13" type="structure" constraint_original="internal cupule" />
    </statement>
    <statement id="d0_s14">
      <text>x = 12.</text>
      <biological_entity id="o24080" name="nut" name_original="nuts" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
        <character constraint="by internal cupule valves" constraintid="o24081" is_modifier="false" name="arrangement" src="d0_s13" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity constraint="x" id="o24082" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Western chinkapin</other_name>
  <discussion>Species 2</discussion>
  <discussion>Nuts are sweet and edible but difficult to remove from the spiny cupules unless completely ripe. The two species of Chrysolepis have sometimes been included in Castanopsis; the latter, however, is a related genus of Fagaceae, native to Asia, with very different cupule structure (H.Hjelmquist 1948; L.L. Forman 1966).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf apex acute or acuminate; trees or erect shrubs, bark thick, rough.</description>
      <determination>1 Chrysolepis chrysophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf apex obtuse, occasionally somewhat acute; low rhizomatous shrubs, bark thin, smooth.</description>
      <determination>2 Chrysolepis sempervirens</determination>
    </key_statement>
  </key>
</bio:treatment>