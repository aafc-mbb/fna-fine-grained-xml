<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="G. Don in J. C. Loudon" date="1830" rank="section">lobatae</taxon_name>
    <taxon_name authority="Trelease" date="1924" rank="species">viminea</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Natl. Acad. Sci.</publication_title>
      <place_in_publication>20: 123, plate 222. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section lobatae;species viminea</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501096</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, evergreen or drought-deciduous, to 10 m.</text>
      <biological_entity id="o18818" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="drought-deciduous" value_original="drought-deciduous" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs brown to dark reddish-brown, 2-3 mm diam., sparsely to uniformly pubescent.</text>
      <biological_entity id="o18819" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s1" to="dark reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to uniformly" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Terminal buds brown to reddish-brown, acutely ovoid, 1.5-2.5 mm, glabrous.</text>
      <biological_entity constraint="terminal" id="o18820" name="bud" name_original="buds" src="d0_s2" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s2" to="reddish-brown" />
        <character is_modifier="false" modifier="acutely" name="shape" src="d0_s2" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 2-5 mm, pubescent.</text>
      <biological_entity id="o18821" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18822" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade narrowly lanceolate, widest at or proximal to middle, 35-60 [-150] × 7-10 mm, base usually rounded or obtuse, often oblique, occasionally somewhat cordate, margins entire with 1 apical awn, apex acute to acuminate;</text>
      <biological_entity id="o18823" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="proximal" name="position" src="d0_s4" to="middle" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18824" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="often" name="orientation_or_shape" src="d0_s4" value="oblique" value_original="oblique" />
        <character is_modifier="false" modifier="occasionally somewhat" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o18825" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="with apical awn" constraintid="o18826" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="apical" id="o18826" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o18827" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>surfaces abaxially glabrous or with prominent tufts of tomentum near base of blade, adaxially glabrous or with pubescence along midrib and scattered on blade.</text>
      <biological_entity id="o18828" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with prominent tufts" value_original="with prominent tufts" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with pubescence" value_original="with pubescence" />
        <character constraint="on blade" constraintid="o18834" is_modifier="false" name="arrangement" notes="" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o18829" name="tuft" name_original="tufts" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o18830" name="tomentum" name_original="tomentum" src="d0_s5" type="structure" />
      <biological_entity id="o18831" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o18832" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o18833" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o18834" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <relation from="o18828" id="r2523" name="with" negation="false" src="d0_s5" to="o18829" />
      <relation from="o18829" id="r2524" name="part_of" negation="false" src="d0_s5" to="o18830" />
      <relation from="o18829" id="r2525" name="near" negation="false" src="d0_s5" to="o18831" />
      <relation from="o18831" id="r2526" name="part_of" negation="false" src="d0_s5" to="o18832" />
      <relation from="o18828" id="r2527" name="along" negation="false" src="d0_s5" to="o18833" />
    </statement>
    <statement id="d0_s6">
      <text>Acorns (not known within our range) biennial;</text>
      <biological_entity id="o18835" name="acorn" name_original="acorns" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cup cupshaped, 5-8 mm high × 8-10 mm wide, covering ca. 1/3 nut, inner surface pubescent, scales with appressed, obtuse tips;</text>
      <biological_entity id="o18836" name="cup" name_original="cup" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="×8-10" value_original="×8-10" />
      </biological_entity>
      <biological_entity id="o18837" name="nut" name_original="nut" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18838" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o18839" name="scale" name_original="scales" src="d0_s7" type="structure" />
      <biological_entity id="o18840" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o18836" id="r2528" name="covering" negation="false" src="d0_s7" to="o18837" />
      <relation from="o18839" id="r2529" name="with" negation="false" src="d0_s7" to="o18840" />
    </statement>
    <statement id="d0_s8">
      <text>nut ovoid, 9-15 × 7-10 mm.</text>
      <biological_entity id="o18841" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring or summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Habitat not specified</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="habitat" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Chihuahua, Sonora, and southward).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (and southward)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>As with many of the Madrean counterparts of the oak flora, in very dry years trees progressively lose their leaves through the long dry spring and may become virtually leafless by the time the rains come in early summer (R. W. Spellenberg, pers. comm.).</discussion>
  <discussion>In the flora, Quercus viminea is known from a single sterile specimen collected at Red Mountain, Santa Cruz County, Arizona. Recent attempts to find the taxon at this site have been fruitless. The Arizona specimen is similar to specimens from northern Mexico that have small, usually entire, narrowly lanceolate leaves with short petioles. These northern forms appear to grade clinally into larger-leaved southern forms that fit the general description of Q. bolanyosensis Trelease.</discussion>
  
</bio:treatment>