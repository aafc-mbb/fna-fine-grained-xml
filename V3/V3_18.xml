<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">papaver</taxon_name>
    <taxon_name authority="Kadereit" date="1988" rank="section">Californicum</taxon_name>
    <taxon_name authority="A. Gray" date="1887" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>22: 313. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus papaver;section californicum;species californicum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500842</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Papaver</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">lemmonii</taxon_name>
    <taxon_hierarchy>genus Papaver;species lemmonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 6.5 dm, glabrate or sparsely pilose.</text>
      <biological_entity id="o15787" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="6.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or branching.</text>
      <biological_entity id="o15788" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves to 15 cm.</text>
      <biological_entity id="o15789" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: peduncle glabrous or sparsely pilose.</text>
      <biological_entity id="o15790" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o15791" name="peduncle" name_original="peduncle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: petals light orange or orange-red, with pink-edged, greenish basal spot, to 2.5 cm;</text>
      <biological_entity id="o15792" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o15793" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="light orange" value_original="light orange" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="orange-red" value_original="orange-red" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15794" name="spot" name_original="spot" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pink-edged" value_original="pink-edged" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="greenish" value_original="greenish" />
      </biological_entity>
      <relation from="o15793" id="r2178" name="with" negation="false" src="d0_s4" to="o15794" />
    </statement>
    <statement id="d0_s5">
      <text>anthers yellow;</text>
      <biological_entity id="o15795" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o15796" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stigmas 4-8 (-11), disc conic, usually umbonate.</text>
      <biological_entity id="o15797" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15798" name="stigma" name_original="stigmas" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="11" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="8" />
      </biological_entity>
      <biological_entity id="o15799" name="disc" name_original="disc" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="umbonate" value_original="umbonate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsules sessile, ellipsoid to obovoid-turbinate, distinctly ribbed, to 1.8 cm. 2n = 28.</text>
      <biological_entity id="o15800" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s7" to="obovoid-turbinate" />
        <character is_modifier="false" modifier="distinctly" name="architecture_or_shape" src="d0_s7" value="ribbed" value_original="ribbed" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="1.8" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15801" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chaparral and oak woodlands, especially in grassy areas, clearings, burns and other disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="grassy areas" modifier="woodlands especially in" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="burns" />
        <character name="habitat" value="other disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Western poppy</other_name>
  <discussion>Papaver californicum grows in central western and southwestern California in the Coast, Transverse, and Peninsular ranges. This is the only caulescent poppy, and the only annual one, native to the flora. In the past it has been included in Papaver sect. Rhoeadium, together with the other annuals that have glabrous capsules and distal leaves not clasping, which are native to Eurasia. Recently, based on differences in filament color, stigmatic disc shape, and capsule dehiscence, J. W. Kadereit (1988b) assigned P. californicum to a new monotypic section and suggested that it originated from the same stock as the perennial, scapose, arctic-alpine poppies (Papaver sect. Meconella).</discussion>
  
</bio:treatment>