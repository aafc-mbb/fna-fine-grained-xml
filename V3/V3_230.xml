<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Linnaeus" date="unknown" rank="family">fumariaceae</taxon_name>
    <taxon_name authority="Bernhardi" date="1833" rank="genus">dicentra</taxon_name>
    <taxon_name authority="(Linnaeus) Bernhardi" date="1833" rank="species">cucullaria</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>8: 457, 468. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fumariaceae;genus dicentra;species cucullaria</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220004018</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fumaria</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">cucullaria</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 699. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Fumaria;species cucullaria;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bicuculla</taxon_name>
    <taxon_name authority="(Linnaeus) Millspaugh" date="unknown" rank="species">cucullaria</taxon_name>
    <taxon_hierarchy>genus Bicuculla;species cucullaria;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bicuculla</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">occidentalis</taxon_name>
    <taxon_hierarchy>genus Bicuculla;species occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicentra</taxon_name>
    <taxon_name authority="(Linnaeus) Bernhardi" date="unknown" rank="species">cucullaria</taxon_name>
    <taxon_name authority="(Rydberg) M. Peck" date="unknown" rank="variety">occidentalis</taxon_name>
    <taxon_hierarchy>genus Dicentra;species cucullaria;variety occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, scapose, from short rootstocks bearing pink to white, teardrop-shaped bulblets.</text>
      <biological_entity id="o10771" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o10772" name="rootstock" name_original="rootstocks" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o10773" name="bulblet" name_original="bulblets" src="d0_s0" type="structure">
        <character char_type="range_value" from="pink" is_modifier="true" name="coloration" src="d0_s0" to="white" />
        <character is_modifier="true" name="shape" src="d0_s0" value="teardrop--shaped" value_original="teardrop--shaped" />
      </biological_entity>
      <relation from="o10771" id="r1500" name="from" negation="false" src="d0_s0" to="o10772" />
      <relation from="o10772" id="r1501" name="bearing" negation="false" src="d0_s0" to="o10773" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves (10-) 14-16 (-36) × (4-) 6-14 (-18) cm;</text>
      <biological_entity id="o10774" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_length" src="d0_s1" to="14" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="36" to_unit="cm" />
        <character char_type="range_value" from="14" from_unit="cm" name="length" src="d0_s1" to="16" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_width" src="d0_s1" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="18" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s1" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole (5-) 8-16 (-24) cm;</text>
      <biological_entity id="o10775" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="24" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s2" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade with 4 orders of leaflets and lobes;</text>
      <biological_entity id="o10776" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o10777" name="order" name_original="orders" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o10778" name="leaflet" name_original="leaflets" src="d0_s3" type="structure" />
      <biological_entity id="o10779" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <relation from="o10776" id="r1502" name="with" negation="false" src="d0_s3" to="o10777" />
      <relation from="o10777" id="r1503" name="part_of" negation="false" src="d0_s3" to="o10778" />
      <relation from="o10777" id="r1504" name="part_of" negation="false" src="d0_s3" to="o10779" />
    </statement>
    <statement id="d0_s4">
      <text>abaxial surface glaucous;</text>
      <biological_entity constraint="abaxial" id="o10780" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes linear to linear-elliptic or linear-obovate, (2-) 5-15 (-23) × (0.4-) 2-3 (-4.2) mm, usually minutely apiculate.</text>
      <biological_entity constraint="ultimate" id="o10781" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-obovate" value_original="linear-obovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="23" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="atypical_width" src="d0_s5" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="usually minutely" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences racemose, 3-14-flowered, usually exceeding leaves;</text>
      <biological_entity id="o10782" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-14-flowered" value_original="3-14-flowered" />
      </biological_entity>
      <biological_entity id="o10783" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o10782" id="r1505" modifier="usually" name="exceeding" negation="false" src="d0_s6" to="o10783" />
    </statement>
    <statement id="d0_s7">
      <text>bracts minute.</text>
      <biological_entity id="o10784" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers pendent;</text>
      <biological_entity id="o10785" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels (2-) 4-7 (-12) mm;</text>
      <biological_entity id="o10786" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals broadly ovate, 1.8-5 × 1.3-4 mm;</text>
      <biological_entity id="o10787" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white, frequently suffused pink, apex yellow to orange-yellow;</text>
      <biological_entity id="o10788" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" modifier="frequently" name="coloration" src="d0_s11" value="suffused pink" value_original="suffused pink" />
      </biological_entity>
      <biological_entity id="o10789" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="orange-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>outer petals (10-) 12-16 (-20) × (3-) 6-10 (-13) mm, reflexed portion 2-5 mm;</text>
      <biological_entity constraint="outer" id="o10790" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s12" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s12" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s12" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10791" name="portion" name_original="portion" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner petals (7.5-) 9-12 (-14) mm, blade 1.8-4 mm, claw linear, 4-8 × less than 1 mm, crest prominent, ca. 2 mm diam.;</text>
      <biological_entity constraint="inner" id="o10792" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="14" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10793" name="blade" name_original="blade" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10794" name="claw" name_original="claw" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="8" />
      </biological_entity>
      <biological_entity id="o10795" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character name="diameter" src="d0_s13" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments of each bundle connate from base to shortly below anthers;</text>
      <biological_entity id="o10796" name="filament" name_original="filaments" src="d0_s14" type="structure" constraint="bundle" constraint_original="bundle; bundle">
        <character constraint="from base" constraintid="o10798" is_modifier="false" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o10797" name="bundle" name_original="bundle" src="d0_s14" type="structure" />
      <biological_entity id="o10798" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o10799" name="anther" name_original="anthers" src="d0_s14" type="structure" />
      <relation from="o10796" id="r1506" name="part_of" negation="false" src="d0_s14" to="o10797" />
      <relation from="o10798" id="r1507" name="shortly below" negation="false" src="d0_s14" to="o10799" />
    </statement>
    <statement id="d0_s15">
      <text>nectariferous tissue forming 1-3 (-4.5) mm spur diverging at angle from base of bundle;</text>
      <biological_entity constraint="nectariferous" id="o10800" name="tissue" name_original="tissue" src="d0_s15" type="structure">
        <character constraint="at angle" constraintid="o10802" is_modifier="false" name="orientation" src="d0_s15" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o10801" name="spur" name_original="spur" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_some_measurement" src="d0_s15" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10802" name="angle" name_original="angle" src="d0_s15" type="structure" />
      <biological_entity id="o10803" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity id="o10804" name="bundle" name_original="bundle" src="d0_s15" type="structure" />
      <relation from="o10800" id="r1508" name="forming" negation="false" src="d0_s15" to="o10801" />
      <relation from="o10802" id="r1509" name="from" negation="false" src="d0_s15" to="o10803" />
      <relation from="o10803" id="r1510" name="part_of" negation="false" src="d0_s15" to="o10804" />
    </statement>
    <statement id="d0_s16">
      <text>style 2-4 mm;</text>
      <biological_entity id="o10805" name="style" name_original="style" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma 2-horned with 2 lateral papillae.</text>
      <biological_entity id="o10806" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character constraint="with lateral papillae" constraintid="o10807" is_modifier="false" name="shape" src="d0_s17" value="2-horned" value_original="2-horned" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o10807" name="papilla" name_original="papillae" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules ovoid, attenuate at both ends, (7-) 9-13 (-16) × 3-5 mm.</text>
      <biological_entity id="o10808" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character constraint="at ends" constraintid="o10809" is_modifier="false" name="shape" src="d0_s18" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o10809" name="end" name_original="ends" src="d0_s18" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" notes="alterIDs:o10809" src="d0_s18" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" notes="alterIDs:o10809" src="d0_s18" to="16" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" notes="alterIDs:o10809" src="d0_s18" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="alterIDs:o10809" src="d0_s18" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds reniform, ca. 2 mm diam., very obscurely reticulate, elaiosome present.</text>
      <biological_entity id="o10810" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="reniform" value_original="reniform" />
        <character name="diameter" src="d0_s19" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" modifier="very obscurely" name="architecture_or_coloration_or_relief" src="d0_s19" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>2n = 32.</text>
      <biological_entity id="o10811" name="elaiosome" name_original="elaiosome" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10812" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous woods and clearings, in rich loam soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous woods" constraint="in rich loam soils" />
        <character name="habitat" value="clearings" constraint="in rich loam soils" />
        <character name="habitat" value="rich loam soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., D.C., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Vt., Va., Wash., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Dutchman's-breeches</other_name>
  <other_name type="common_name">dicentre &amp;agrave; capuchon</other_name>
  <discussion>Dicentra cucullaria is occasionally confused with D. canadensis, with which it is sympatric. It is distinguished from that species by its basally pointed (versus rounded) outer petal spurs, by its flowers lacking a fragrance, by flowering 7-10 days earlier, and by its pink to white, teardrop-shaped (versus yellow, pea-shaped) bulblets.</discussion>
  <discussion>After fruit set, the bulblets of both Dicentra cucullaria and D. canadensis remain dormant until fall, when stored starch is converted to sugar. At this time also, flower buds and leaf primordia are produced below ground; these then remain dormant until spring (P. G. Risser and G. Cottam 1968; B. J. Kieckhefer 1964; K. R. Stern 1961). Pollination of both species is effected by bumblebees (Bombus spp.) and other long-tongued insects (L. W. Macior 1970, 1978; K. R. Stern 1961).</discussion>
  <discussion>Flavonoid components indicate that Dicentra canadensis and D. cucullaria are more closely related to each other than to any other member of the genus (D. Fahselt 1971). Even so, species purported to be hybrids between them probably are not. There is considerable variation in floral morphology within D. cucullaria, which can have flowers superficially resembling those of D. canadensis. However, when all characters of the plants are examined, these putative hybrids almost always are clearly assignable to one species or the other.</discussion>
  <discussion>The western populations of Dicentra cucullaria appear to have been separated from the eastern ones for at least a thousand years. The western plants are generally somewhat coarser, which apparently led Rydberg to designate the western populations as a separate species. Plants from the Blue Ridge Mountains of Virginia, however, are virtually indistinguishable from those of the West, and much of the variation (which is considerable) within the species probably involves phenotypic response to the environment, or represents ecotypes within the species.</discussion>
  <discussion>The Iroquois prepared infusions from the roots of Dicentra cucullaria for a medicinal liniment (D. E. Moerman 1986).</discussion>
  
</bio:treatment>