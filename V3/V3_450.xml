<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="Linneaus" date="unknown" rank="section">quercus</taxon_name>
    <taxon_name authority="Engelmann" date="1887" rank="species">muehlenbergii</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Acad. Sci. St. Louis</publication_title>
      <place_in_publication>3: 391. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section quercus;species muehlenbergii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242417084</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="(Michaux) Sargent" date="unknown" rank="species">acuminata</taxon_name>
    <taxon_hierarchy>genus Quercus;species acuminata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">brayi</taxon_name>
    <taxon_hierarchy>genus Quercus;species brayi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">prinus</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="variety">acuminata</taxon_name>
    <taxon_hierarchy>genus Quercus;species prinus;variety acuminata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, deciduous, moderate to large, to 30 m, occasionally large shrubs (ca. 3 m) on drier sites.</text>
      <biological_entity id="o11393" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="moderate" name="size" src="d0_s0" to="large" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="30" to_unit="m" />
        <character name="growth_form" value="tree" />
        <character is_modifier="true" modifier="occasionally" name="size" src="d0_s0" value="large" value_original="large" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o11395" name="site" name_original="sites" src="d0_s0" type="structure">
        <character is_modifier="true" name="condition" src="d0_s0" value="drier" value_original="drier" />
      </biological_entity>
      <relation from="o11393" id="r1597" name="on" negation="false" src="d0_s0" to="o11395" />
    </statement>
    <statement id="d0_s1">
      <text>Bark gray, thin, flaky to papery.</text>
      <biological_entity id="o11396" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="flaky" value_original="flaky" />
        <character is_modifier="false" name="texture" src="d0_s1" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs brownish, 1.5-3 (-4) mm diam., sparsely fine-pubescent, soon becoming glabrate, graying in 2d year.</text>
      <biological_entity id="o11397" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="fine-pubescent" value_original="fine-pubescent" />
        <character is_modifier="false" modifier="soon becoming" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character constraint="in year" constraintid="o11398" is_modifier="false" name="coloration" src="d0_s2" value="graying" value_original="graying" />
      </biological_entity>
      <biological_entity id="o11398" name="year" name_original="year" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Buds brown to redbrown, subrotund to broadly ovoid, 20-40 × (10-) 15-25 mm, apex rounded, very sparsely pubescent.</text>
      <biological_entity id="o11399" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s3" to="redbrown" />
        <character char_type="range_value" from="subrotund" name="shape" src="d0_s3" to="broadly ovoid" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s3" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11400" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="very sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole (7-) 10-30 (-37) mm.</text>
      <biological_entity id="o11401" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o11402" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="37" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade usually obovate, sometimes lanceolate to oblanceolate, (32-) 50-150 (-210) × (10-) 40-80 (-106) mm, leathery, base truncate to cuneate, margins regularly undulate, toothed or shallow-lobed, teeth or lobes rounded, or acute-acuminate, often strongly antrorse, secondary-veins usually (9-) 10-14 (-16) on each side, ± parallel, apex short-acute to acuminate or apiculate;</text>
      <biological_entity id="o11403" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="lanceolate" modifier="sometimes" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="32" from_unit="mm" name="atypical_length" src="d0_s5" to="50" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="210" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s5" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="106" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="width" src="d0_s5" to="80" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o11404" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o11405" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="regularly" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="shallow-lobed" value_original="shallow-lobed" />
      </biological_entity>
      <biological_entity id="o11406" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute-acuminate" value_original="acute-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute-acuminate" value_original="acute-acuminate" />
        <character is_modifier="false" modifier="often strongly" name="orientation" src="d0_s5" value="antrorse" value_original="antrorse" />
      </biological_entity>
      <biological_entity id="o11407" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute-acuminate" value_original="acute-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute-acuminate" value_original="acute-acuminate" />
        <character is_modifier="false" modifier="often strongly" name="orientation" src="d0_s5" value="antrorse" value_original="antrorse" />
      </biological_entity>
      <biological_entity id="o11408" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" name="atypical_quantity" src="d0_s5" to="10" to_inclusive="false" />
        <character char_type="range_value" from="14" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="16" />
        <character char_type="range_value" constraint="on side" constraintid="o11409" from="10" name="quantity" src="d0_s5" to="14" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s5" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o11409" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o11410" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="short-acute" name="shape" src="d0_s5" to="acuminate or apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially glaucous or light green, appearing glabrate but with scattered or crowded minute, appressed, symmetric, 6-10-rayed stellate hairs, adaxially lustrous dark green, glabrate.</text>
      <biological_entity id="o11411" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="light green" value_original="light green" />
        <character constraint="with hairs" constraintid="o11412" is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" notes="" src="d0_s6" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o11412" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="crowded" value_original="crowded" />
        <character is_modifier="true" name="size" src="d0_s6" value="minute" value_original="minute" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s6" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Acorns 1-2, subsessile or on axillary peduncle to 8 mm;</text>
      <biological_entity id="o11413" name="acorn" name_original="acorns" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="on axillary peduncle" value_original="on axillary peduncle" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o11414" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o11413" id="r1598" name="on" negation="false" src="d0_s7" to="o11414" />
    </statement>
    <statement id="d0_s8">
      <text>cup hemispheric or shallowly cupped, 4-12 mm deep × 8-22 mm wide, enclosing 1/4-1/2 nut, base rounded, margin usually thin, scales closely appressed, moderately to prominently tuberculate, uniformly short gray-pubescent;</text>
      <biological_entity id="o11415" name="cup" name_original="cup" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character name="shape" src="d0_s8" value="shallowly" value_original="shallowly" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="12" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="×8-22" value_original="×8-22" />
      </biological_entity>
      <biological_entity id="o11416" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character char_type="range_value" from="1/4" is_modifier="true" name="quantity" src="d0_s8" to="1/2" />
      </biological_entity>
      <biological_entity id="o11417" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o11418" name="margin" name_original="margin" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="width" src="d0_s8" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o11419" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="moderately to prominently" name="relief" src="d0_s8" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="uniformly" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="gray-pubescent" value_original="gray-pubescent" />
      </biological_entity>
      <relation from="o11415" id="r1599" name="enclosing" negation="false" src="d0_s8" to="o11416" />
    </statement>
    <statement id="d0_s9">
      <text>nut light-brown, oblong to ovoid, (13-) 15-20 (-28) × 10-13 (-16) mm.</text>
      <biological_entity id="o11420" name="nut" name_original="nut" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="ovoid" />
        <character char_type="range_value" from="13" from_unit="mm" name="atypical_length" src="d0_s9" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="28" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="16" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cotyledons distinct.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 24.</text>
      <biological_entity id="o11421" name="cotyledon" name_original="cotyledons" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11422" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mixed deciduous forest, woodlands and thickets, sometimes restricted to n slopes and riparian habitats in w parts of range, limestone and calcareous soils, rarely on other substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed deciduous forest" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="sometimes restricted to n slopes" />
        <character name="habitat" value="riparian habitats" constraint="in w parts of range" />
        <character name="habitat" value="w parts" constraint="of range" />
        <character name="habitat" value="range" />
        <character name="habitat" value="calcareous" />
        <character name="habitat" value="other substrates" modifier="rarely on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.; Mexico (Coahuila, Nuevo León, Hidalgo, and Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Hidalgo)" establishment_means="native" />
        <character name="distribution" value="Mexico (and Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42.</number>
  <other_name type="common_name">Chinkapin oak</other_name>
  <other_name type="common_name">chinquapin oak</other_name>
  <other_name type="common_name">yellow chestnut oak</other_name>
  <discussion>Shrubby forms of Quercus muhlenbergii are difficult to distinguish from Quercus prinoides, but Q. muhlenbergii does not spread clonally or produce acorns on small shrubs as does Q. prinoides. The edaphic preferences of these two species are distinctive, with Q. muhlenbergii never far from limestone substrates and Q. prinoides occurring mostly on dry shales and deep sands. Populations of Q. muhlenbergii from the southwest part of its range, on the Edwards Plateau of Texas and westward, sometimes are segregated as Q. brayi Small, but the variation appears to be clinal with inconsistent differences. Distributed from Hidalgo, Mexico to Maine, Q. muhlenbergii is one of the most widespread species of temperate North American trees.</discussion>
  <discussion>The Delaware-Ontario prepared infusions from the bark of Quercus muhlenbergii to stop vomiting (D. E. Moerman 1986).</discussion>
  
</bio:treatment>