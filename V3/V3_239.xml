<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="N. I. Malyutin" date="1987" rank="subsection">exaltata</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1838" rank="species">californicum</taxon_name>
    <taxon_name authority="(Eastwood) Ewan" date="1945" rank="subspecies">interius</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Colorado Stud., Ser. D, Phys. Sci.</publication_title>
      <place_in_publication>2: 146. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection exaltata;species californicum;subspecies interius;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500483</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">californicum</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="variety">interius</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>2: 137. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species californicum;variety interius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems proximally sparsely puberulent, remainder ± glabrous.</text>
      <biological_entity id="o27506" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="proximally sparsely" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades glabrous.</text>
      <biological_entity id="o27507" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences nearly glabrous.</text>
      <biological_entity id="o27508" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals spreading to erect, greenish white, hairs usually near apex only, lateral sepals 6-8 mm, spurs 7-10 mm;</text>
      <biological_entity id="o27509" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o27510" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="greenish white" value_original="greenish white" />
      </biological_entity>
      <biological_entity id="o27511" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <biological_entity id="o27512" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity constraint="lateral" id="o27513" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27514" name="spur" name_original="spurs" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <relation from="o27511" id="r3680" name="near" negation="false" src="d0_s3" to="o27512" />
    </statement>
    <statement id="d0_s4">
      <text>upper petals glabrous.</text>
      <biological_entity id="o27515" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="upper" id="o27516" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Usually on inland facing slopes in open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" modifier="usually on inland facing" constraint="in open woods" />
        <character name="habitat" value="open woods" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b.</number>
  <other_name type="common_name">Hospital Canyon larkspur</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Delphinium californicum subsp. interius is known from fewer than a dozen localities. It apparently hybridizes with D. hesperium subsp. pallescens.</discussion>
  
</bio:treatment>