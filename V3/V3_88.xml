<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="Sessé &amp; Moçiño" date="1894" rank="species">fasciculatus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Mexic. ed.</publication_title>
      <place_in_publication>2, 134. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species fasciculatus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233501140</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="Humboldt" date="unknown" rank="species">macranthus</taxon_name>
    <taxon_name authority="L. D. Benson" date="unknown" rank="variety">arsenei</taxon_name>
    <taxon_hierarchy>genus Ranunculus;species macranthus;variety arsenei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">petiolaris</taxon_name>
    <place_of_publication>
      <publication_title>Bonpland, &amp; Kunth ex de Candolle var. arsenei (L. D. Benson) T. Duncan</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus Ranunculus;species petiolaris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or decumbent, not rooting nodally, hirsute to nearly glabrous, base not bulbous.</text>
      <biological_entity id="o11562" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="not; nodally" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s0" to="nearly glabrous" />
      </biological_entity>
      <biological_entity id="o11563" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fleshy and somewhat tuberous.</text>
      <biological_entity id="o11564" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaf-blades ovate to deltate in outline, 3 (-5) -foliolate, 2.5-14.9 × 2.3-19.9 cm, leaflets 1-2×-lobed or parted, ultimate segments narrowly oblong to elliptic or lanceolate, margins toothed, apex narrowly acute to rounded-acute.</text>
      <biological_entity constraint="basal" id="o11565" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="in outline" constraintid="o11566" from="ovate" name="shape" src="d0_s2" to="deltate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="3(-5)-foliolate" value_original="3(-5)-foliolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s2" to="14.9" to_unit="cm" />
        <character char_type="range_value" from="2.3" from_unit="cm" name="width" src="d0_s2" to="19.9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11566" name="outline" name_original="outline" src="d0_s2" type="structure" />
      <biological_entity id="o11567" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="1-2×-lobed" value_original="1-2×-lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="parted" value_original="parted" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o11568" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s2" to="elliptic or lanceolate" />
      </biological_entity>
      <biological_entity id="o11569" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o11570" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s2" to="rounded-acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: receptacle hispid;</text>
      <biological_entity id="o11571" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o11572" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals reflexed 1-2 mm above base, 5-10 × 3-5 mm, hispid;</text>
      <biological_entity id="o11573" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o11574" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" constraint="above base" constraintid="o11575" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o11575" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals 11-16, yellow, 8-21 × 2-5 mm.</text>
      <biological_entity id="o11576" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o11577" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s5" to="16" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="21" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes globose to ovoid, 6-13 × 7-9 mm;</text>
      <biological_entity id="o11578" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s6" to="ovoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="13" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11579" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o11578" id="r1611" name="part_of" negation="false" src="d0_s6" to="o11579" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 2.4-3.4 × 2-2.4 mm, glabrous, margin forming rib or narrow wing 0.1-0.4 mm wide;</text>
      <biological_entity id="o11580" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s7" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11581" name="margin" name_original="margin" src="d0_s7" type="structure" />
      <biological_entity id="o11582" name="rib" name_original="rib" src="d0_s7" type="structure" />
      <biological_entity id="o11583" name="wing" name_original="wing" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.4" to_unit="mm" />
      </biological_entity>
      <relation from="o11581" id="r1612" name="forming" negation="false" src="d0_s7" to="o11582" />
    </statement>
    <statement id="d0_s8">
      <text>beak filiform from deltate base, straight, 1.8-2.5 mm, filiform tip often deciduous, leaving 1-1.2 mm deltate beak.</text>
      <biological_entity id="o11584" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character constraint="from base" constraintid="o11585" is_modifier="false" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="course" notes="" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11585" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
      </biological_entity>
      <biological_entity id="o11586" name="tip" name_original="tip" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="often" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o11587" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, lakeshores, and marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>I am following G. L. Nesom (1993) in treating Ranunculus fasciculatus as a distinct species. This taxon was considered a variety of R. macranthus by L. D. Benson (1948) and a variety of R. petiolaris by T. Duncan (1980). These disparate opinions result from different interpretations of Mexican members of the R. petiolaris group.</discussion>
  
</bio:treatment>