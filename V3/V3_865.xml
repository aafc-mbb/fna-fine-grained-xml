<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">betula</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">nigra</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 982. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus betula;species nigra;</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500258</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Betula</taxon_name>
    <taxon_name authority="F. Michaux" date="unknown" rank="species">rubra</taxon_name>
    <taxon_hierarchy>genus Betula;species rubra;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 25 m;</text>
      <biological_entity id="o11106" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="25" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks often several, crowns round.</text>
      <biological_entity id="o11107" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="quantity" src="d0_s1" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o11108" name="crown" name_original="crowns" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="round" value_original="round" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bark of mature trunks and branches grayish brown, yellowish, reddish, or creamy white, smooth, irregularly shredding and exfoliating in shaggy sheets when mature;</text>
      <biological_entity id="o11109" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish brown" value_original="grayish brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character constraint="in sheets" constraintid="o11112" is_modifier="false" modifier="irregularly" name="relief" src="d0_s2" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
      <biological_entity id="o11110" name="trunk" name_original="trunks" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o11111" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o11112" name="sheet" name_original="sheets" src="d0_s2" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s2" value="shaggy" value_original="shaggy" />
      </biological_entity>
      <relation from="o11109" id="r1553" name="part_of" negation="false" src="d0_s2" to="o11110" />
      <relation from="o11109" id="r1554" name="part_of" negation="false" src="d0_s2" to="o11111" />
    </statement>
    <statement id="d0_s3">
      <text>lenticels dark, horizontally expanded.</text>
      <biological_entity id="o11113" name="lenticel" name_original="lenticels" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark" value_original="dark" />
        <character is_modifier="false" modifier="horizontally" name="size" src="d0_s3" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Twigs without wintergreen taste or odor, glabrous to sparsely pubescent, often with scattered, tiny, resinous glands.</text>
      <biological_entity id="o11114" name="twig" name_original="twigs" src="d0_s4" type="structure">
        <character constraint="without" is_modifier="false" name="wintergreen" src="d0_s4" value="taste" value_original="taste" />
        <character is_modifier="false" name="wintergreen" src="d0_s4" value="odor" value_original="odor" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="sparsely pubescent" />
      </biological_entity>
      <biological_entity id="o11115" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="size" src="d0_s4" value="tiny" value_original="tiny" />
        <character is_modifier="true" name="coating" src="d0_s4" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o11114" id="r1555" modifier="often" name="with" negation="false" src="d0_s4" to="o11115" />
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade rhombic-ovate, with 5–12 pairs of lateral-veins, 4–8 × 3–6 cm, base broadly cuneate to truncate, margins coarsely doubly serrate to dentate, apex acuminate;</text>
      <biological_entity id="o11116" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" notes="" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" notes="" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11117" name="pair" name_original="pairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="12" />
      </biological_entity>
      <biological_entity id="o11118" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure" />
      <biological_entity id="o11119" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate to truncate" value_original="cuneate to truncate" />
      </biological_entity>
      <biological_entity id="o11120" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="coarsely doubly" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s5" value="serrate to dentate" value_original="serrate to dentate" />
      </biological_entity>
      <biological_entity id="o11121" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o11116" id="r1556" name="with" negation="false" src="d0_s5" to="o11117" />
      <relation from="o11117" id="r1557" name="part_of" negation="false" src="d0_s5" to="o11118" />
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially moderately pubescent to velutinous, especially along major veins and in vein-axils, often with scattered, minute, resinous glands.</text>
      <biological_entity id="o11122" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character char_type="range_value" from="abaxially moderately pubescent" name="pubescence" src="d0_s6" to="velutinous" />
      </biological_entity>
      <biological_entity id="o11123" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="major" value_original="major" />
      </biological_entity>
      <biological_entity id="o11124" name="vein-axil" name_original="vein-axils" src="d0_s6" type="structure" />
      <biological_entity id="o11125" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="size" src="d0_s6" value="minute" value_original="minute" />
        <character is_modifier="true" name="coating" src="d0_s6" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o11122" id="r1558" modifier="especially" name="along" negation="false" src="d0_s6" to="o11123" />
      <relation from="o11122" id="r1559" name="in" negation="false" src="d0_s6" to="o11124" />
      <relation from="o11122" id="r1560" modifier="often" name="with" negation="false" src="d0_s6" to="o11125" />
    </statement>
    <statement id="d0_s7">
      <text>Infructescences erect, conic or nearly globose, 1.5–3 × 1–2.5 cm, shattering with fruits in late spring or early summer;</text>
      <biological_entity id="o11126" name="infructescence" name_original="infructescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s7" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s7" value="globose" value_original="globose" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="season" src="d0_s7" value="spring" value_original="spring" />
        <character is_modifier="false" name="season" src="d0_s7" value="early" value_original="early" />
        <character is_modifier="false" name="season" src="d0_s7" value="summer" value_original="summer" />
      </biological_entity>
      <biological_entity id="o11127" name="fruit" name_original="fruits" src="d0_s7" type="structure" />
      <relation from="o11126" id="r1561" name="shattering with" negation="false" src="d0_s7" to="o11127" />
    </statement>
    <statement id="d0_s8">
      <text>scales often persistent into early winter, lobes 3, ascending, branching distal to middle, narrow, elongate, equal to somewhat unequal in length, apex acute.</text>
      <biological_entity id="o11128" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character constraint="into early winter" is_modifier="false" modifier="often" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o11129" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="position" src="d0_s8" value="branching" value_original="branching" />
        <character char_type="range_value" from="distal" name="position" src="d0_s8" to="middle" />
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="somewhat" name="length" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o11130" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Samaras with wings narrower than body, usually broadest near summit, not extended beyond body apically.</text>
      <biological_entity id="o11132" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character constraint="than body" constraintid="o11133" is_modifier="false" name="width" src="d0_s9" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o11133" name="body" name_original="body" src="d0_s9" type="structure" />
      <biological_entity id="o11134" name="summit" name_original="summit" src="d0_s9" type="structure" />
      <biological_entity id="o11135" name="body" name_original="body" src="d0_s9" type="structure" />
      <relation from="o11131" id="r1562" name="with" negation="false" src="d0_s9" to="o11132" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 28.</text>
      <biological_entity id="o11131" name="samara" name_original="samaras" src="d0_s9" type="structure">
        <character constraint="than body" constraintid="o11133" is_modifier="false" name="width" src="d0_s9" value="narrower" value_original="narrower" />
        <character constraint="near summit" constraintid="o11134" is_modifier="false" modifier="usually" name="width" src="d0_s9" value="broadest" value_original="broadest" />
        <character constraint="beyond body" constraintid="o11135" is_modifier="false" modifier="not" name="size" notes="" src="d0_s9" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11136" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Riverbanks and flood plains, often where land is periodically inundated</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="flood" />
        <character name="habitat" value="plains" />
        <character name="habitat" value="land" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Minn., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">River birch</other_name>
  <other_name type="common_name">red birch</other_name>
  <discussion>Betula nigra is a large and characteristic floodplain tree. Like several other species of this habitat (e.g., Acer saccharinum Marshall and Ulmus americana Linnaeus), it releases its fruits in early summer; the seeds germinate immediately (at a time when the surrounding land is unlikely to be flooded). The wood of Betula nigra is not in high demand for timber because of its generally poor quality. Cultivars with freely exfoliating bark are commonly cultivated in the Northeast and Midwest.</discussion>
  <discussion>Native Americans used Betula nigra medicinally to treat dysentery, colds, and milky urine (D. E. Moerman 1986).</discussion>
  
</bio:treatment>