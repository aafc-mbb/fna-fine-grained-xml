<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">clematis</taxon_name>
    <taxon_name authority="(Linnaeus) Torrey &amp; A. Gray" date="1838" rank="subgenus">atragene</taxon_name>
    <taxon_name authority="(Hornemann) de Candolle" date="1824" rank="species">occidentalis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">occidentalis</taxon_name>
    <taxon_hierarchy>family ranunculaceae;genus clematis;subgenus atragene;species occidentalis;variety occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500403</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atragene</taxon_name>
    <taxon_name authority="Sims" date="unknown" rank="species">americana</taxon_name>
    <taxon_hierarchy>genus Atragene;species americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clematis</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">verticillaris</taxon_name>
    <taxon_hierarchy>genus Clematis;species verticillaris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clematis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">verticillaris</taxon_name>
    <taxon_name authority="B.Boivin" date="unknown" rank="variety">cacuminis</taxon_name>
    <taxon_hierarchy>genus Clematis;species verticillaris;variety cacuminis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clematis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">verticillaris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">grandiflora</taxon_name>
    <taxon_hierarchy>genus Clematis;species verticillaris;variety grandiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ± viny, climbing or trailing, 0.25-3.5 m.</text>
      <biological_entity id="o11344" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s0" value="viny" value_original="viny" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character char_type="range_value" from="0.25" from_unit="m" name="some_measurement" src="d0_s0" to="3.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: leaflets unlobed or occasionally 1-3-lobed, (2-) 3-6 (-10) cm, margins entire or shallowly crenate-serrate.</text>
      <biological_entity id="o11345" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o11346" name="leaflet" name_original="leaflets" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s1" value="1-3-lobed" value_original="1-3-lobed" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11347" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s1" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals remaining moderately divergent, reddish violet, ovate to oblongelliptic, 2.5-6 cm, margins not fluted, tips rounded-mucronate to nearly acuminate.</text>
      <biological_entity id="o11348" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o11350" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="moderately" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="reddish violet" value_original="reddish violet" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s2" to="oblongelliptic" />
        <character char_type="range_value" from="2.5" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11351" name="tip" name_original="tips" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="not" name="shape" src="d0_s2" value="fluted" value_original="fluted" />
        <character is_modifier="true" modifier="moderately" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="reddish violet" value_original="reddish violet" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s2" to="oblongelliptic" />
        <character char_type="range_value" from="2.5" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
      <relation from="o11349" id="r1592" name="remaining" negation="false" src="d0_s2" to="o11350" />
      <relation from="o11349" id="r1593" name="remaining" negation="false" src="d0_s2" to="o11351" />
    </statement>
    <statement id="d0_s3">
      <text>2n = 16.</text>
      <biological_entity id="o11349" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded-mucronate" name="shape" src="d0_s2" to="nearly acuminate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11352" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous cliffs, rock ledges, talus slopes, gravelly embankments, rocky woods, and clearings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous cliffs" />
        <character name="habitat" value="rock ledges" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="gravelly embankments" />
        <character name="habitat" value="rocky woods" />
        <character name="habitat" value="clearings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., Que.; Conn., Del., Ill., Iowa, Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Pa., R.I., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14a.</number>
  <discussion>Clematis occidentalis var. occidentalis formerly occurred in Ohio. Plants in the western part of the range of this variety tend to have larger, more abruptly tapering sepals; they have been segregated as var. grandiflora B.Boivin, but they do not appear to constitute a distinct taxon.</discussion>
  
</bio:treatment>