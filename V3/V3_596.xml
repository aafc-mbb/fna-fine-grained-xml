<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">quinquefolia</taxon_name>
    <taxon_name authority="(de Candolle) Frodin" date="1994" rank="variety">minima</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 86. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species quinquefolia;variety minima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500083</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">minima</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>1: 206. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Anemone;species minima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 5-15 cm.</text>
      <biological_entity id="o10415" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 4-12 cm;</text>
      <biological_entity id="o10416" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o10417" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>terminal leaflet of basal leaves 1-2.5 × 1-2 cm, pilose;</text>
      <biological_entity id="o10418" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="terminal" id="o10419" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10420" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o10419" id="r1445" name="part_of" negation="false" src="d0_s2" to="o10420" />
    </statement>
    <statement id="d0_s3">
      <text>lateral leaflets unlobed or occasionally 1×-lobed;</text>
      <biological_entity id="o10421" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="lateral" id="o10422" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="1×-lobed" value_original="1×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ultimate lobes 4-8 mm wide.</text>
      <biological_entity id="o10423" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="ultimate" id="o10424" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: involucral-bracts with terminal leaflet of 1-2 × 0.5-1 cm wide, base ± narrowly cuneate, margins crenate or serrate to dentate, apex acuminate to narrowly acute, surfaces pilose;</text>
      <biological_entity id="o10425" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o10426" name="involucral-bract" name_original="involucral-bracts" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o10427" name="leaflet" name_original="leaflet" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10428" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less narrowly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o10429" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s5" value="serrate to dentate" value_original="serrate to dentate" />
      </biological_entity>
      <biological_entity id="o10430" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="narrowly acute" />
      </biological_entity>
      <biological_entity id="o10431" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <relation from="o10426" id="r1446" name="with" negation="false" src="d0_s5" to="o10427" />
    </statement>
    <statement id="d0_s6">
      <text>lateral leaflets unlobed, rarely slightly 1×-lobed.</text>
      <biological_entity id="o10432" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="lateral" id="o10433" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="rarely slightly" name="shape" src="d0_s6" value="1×-lobed" value_original="1×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals (7-) 8 (-11) × ca. 4 mm;</text>
      <biological_entity id="o10434" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o10435" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s7" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="11" to_unit="mm" />
        <character name="length" src="d0_s7" unit="mm" value="8" value_original="8" />
        <character name="width" src="d0_s7" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 30-40.</text>
      <biological_entity id="o10436" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10437" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s8" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicel 1-4 cm in fruit.</text>
      <biological_entity id="o10438" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o10439" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10439" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Achenes: body 2.5-3 mm. 2n=32.</text>
      <biological_entity id="o10440" name="achene" name_original="achenes" src="d0_s10" type="structure" />
      <biological_entity id="o10441" name="body" name_original="body" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10442" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, frequently acidic, wooded hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic" modifier="frequently" />
        <character name="habitat" value="wooded hillsides" />
        <character name="habitat" value="damp" modifier="frequently" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16b.</number>
  
</bio:treatment>