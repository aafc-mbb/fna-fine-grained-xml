<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">papaver</taxon_name>
    <taxon_name authority="Spach" date="1839" rank="section">Meconella</taxon_name>
    <taxon_name authority="Hultén" date="1928" rank="species">alboroseum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Kamtchatka</publication_title>
      <place_in_publication>2: 141, plate 3, fig. c. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus papaver;section meconella;species alboroseum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500840</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, to 1.5 dm.</text>
      <biological_entity id="o21183" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves to 4 cm;</text>
      <biological_entity id="o21184" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>petiole 1/2 length of leaf or less;</text>
    </statement>
    <statement id="d0_s3">
      <text>blade gray-green on both surfaces, broadly lanceolate, 1-2×-lobed with 1 or 2 pairs of primary lateral lobes, white to brown-setose;</text>
      <biological_entity id="o21186" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="on surfaces" constraintid="o21187" is_modifier="false" name="coloration" src="d0_s3" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character constraint="of primary lateral lobes" constraintid="o21188" is_modifier="false" name="shape" src="d0_s3" value="1-2×-lobed" value_original="1-2×-lobed" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="brown-setose" value_original="brown-setose" />
      </biological_entity>
      <biological_entity id="o21187" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity constraint="primary lateral" id="o21188" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>primary lobes obovate to strap-shaped, margins sometimes toothed, apex obtuse-rounded to acute, bristle-tipped.</text>
      <biological_entity constraint="primary" id="o21189" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="strap-shaped" />
      </biological_entity>
      <biological_entity id="o21190" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o21191" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse-rounded" name="shape" src="d0_s4" to="acute" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="bristle-tipped" value_original="bristle-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scapes often decumbent, bowed, spreading-hispid.</text>
      <biological_entity id="o21192" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o21193" name="scape" name_original="scapes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s5" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s5" value="bowed" value_original="bowed" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="spreading-hispid" value_original="spreading-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers to 2.5 cm diam.;</text>
      <biological_entity id="o21194" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white to rose with yellow basal spot;</text>
      <biological_entity id="o21195" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="with basal spot" constraintid="o21196" from="white" name="coloration" src="d0_s7" to="rose" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21196" name="spot" name_original="spot" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers yellow;</text>
      <biological_entity id="o21197" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigmas 5-7, disc convex.</text>
      <biological_entity id="o21198" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
      <biological_entity id="o21199" name="disc" name_original="disc" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules subglobose to ellipsoid, to 1.3 cm, 1-2 times longer than broad, strigose, trichomes light (ivory).</text>
      <biological_entity id="o21200" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s10" to="ellipsoid" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s10" to="1.3" to_unit="cm" />
        <character is_modifier="false" name="length_or_size_or_width" src="d0_s10" value="1-2 times longer than broad" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 28.</text>
      <biological_entity id="o21201" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="light" value_original="light" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21202" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky tundra of ridges and mountain summits, ash and cinder slopes, and in sand and gravel of glacial outwash and river flood plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky tundra" constraint="of ridges and mountain summits , ash and cinder slopes , and in sand and gravel" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="mountain summits" />
        <character name="habitat" value="ash" />
        <character name="habitat" value="cinder slopes" />
        <character name="habitat" value="sand" />
        <character name="habitat" value="gravel" />
        <character name="habitat" value="glacial outwash" />
        <character name="habitat" value="river flood plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Yukon; Alaska; Asia (Russian Far East, Kamchatka).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="Asia (Kamchatka)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Papaver alboroseum is infrequent at scattered localities on high mountains within the area mapped. It is locally and unusually abundant in gravels below the terminus of the Portage Glacier, near Anchorage, Alaska. Reports of its presence in arctic Alaska are based on misidentifications of P. lapponicum.</discussion>
  
</bio:treatment>