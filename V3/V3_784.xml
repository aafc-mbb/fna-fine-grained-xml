<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">clematis</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1895" rank="subgenus">viorna</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">viorna</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 543. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus clematis;subgenus viorna;species viorna</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500415</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clematis</taxon_name>
    <taxon_name authority="(Small) R. O. Erickson" date="unknown" rank="species">beadlei</taxon_name>
    <taxon_hierarchy>genus Clematis;species beadlei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clematis</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">viorna</taxon_name>
    <taxon_name authority="(Small ex Rydberg) R. O. Erickson" date="unknown" rank="variety">flaccida</taxon_name>
    <taxon_hierarchy>genus Clematis;species viorna;variety flaccida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viorna</taxon_name>
    <taxon_name authority="(Small ex Rydberg) Small" date="unknown" rank="species">beadlei</taxon_name>
    <taxon_hierarchy>genus Viorna;species beadlei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viorna</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">flaccida</taxon_name>
    <taxon_hierarchy>genus Viorna;species flaccida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viorna</taxon_name>
    <taxon_name authority="(Linnaeus) Small" date="unknown" rank="species">gattingeri</taxon_name>
    <taxon_hierarchy>genus Viorna;species gattingeri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viorna</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">viorna</taxon_name>
    <taxon_hierarchy>genus Viorna;species viorna;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems viny, to 4 m, without cobwebby pubescence, nearly glabrous, or moderately pilose-pubescent proximal to nodes.</text>
      <biological_entity id="o18305" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="viny" value_original="viny" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
      </biological_entity>
      <biological_entity id="o18306" name="node" name_original="nodes" src="d0_s0" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s0" value="cobwebby" value_original="cobwebby" />
        <character is_modifier="true" name="character" src="d0_s0" value="pubescence" value_original="pubescence" />
        <character is_modifier="true" modifier="nearly" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" modifier="moderately" name="pubescence" src="d0_s0" value="pilose-pubescent" value_original="pilose-pubescent" />
        <character is_modifier="true" name="position" src="d0_s0" value="proximal" value_original="proximal" />
      </biological_entity>
      <relation from="o18305" id="r2460" name="without" negation="false" src="d0_s0" to="o18306" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade mostly 1-pinnate, some simple;</text>
      <biological_entity id="o18307" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s1" value="1-pinnate" value_original="1-pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets usually 4-8 plus additional tendril-like terminal leaflet, lanceolate to ovate, unlobed or 2-3-lobed, or most proximal 3-foliolate, 2-12 × 1-5 (-6) cm, thin, not conspicuously reticulate;</text>
      <biological_entity id="o18308" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="8" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s2" to="ovate unlobed or 2-3-lobed" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate unlobed or 2-3-lobed" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o18309" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="additional" value_original="additional" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="tendril-like" value_original="tendril-like" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18310" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not conspicuously" name="architecture_or_coloration_or_relief" src="d0_s2" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>surfaces abaxially sparsely to densely pilose, not glaucous.</text>
      <biological_entity id="o18311" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, 1-7-flowered;</text>
      <biological_entity id="o18312" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-7-flowered" value_original="1-7-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts well above base of peduncle/pedicel.</text>
      <biological_entity id="o18313" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <biological_entity id="o18314" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o18315" name="pedicel" name_original="peduncle/pedicel" src="d0_s5" type="structure" />
      <relation from="o18313" id="r2461" name="above" negation="false" src="d0_s5" to="o18314" />
      <relation from="o18314" id="r2462" name="part_of" negation="false" src="d0_s5" to="o18315" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers broadly urn-shaped to bell-shaped;</text>
      <biological_entity id="o18316" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly urn-shaped" name="shape" src="d0_s6" to="bell-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals pale lavender to reddish purple, grading to cream-yellow toward tip, ovatelanceolate, 1.5-3 cm, margins not expanded, very thick, not crispate, tomentose, tips acuminate, recurved, abaxially sparsely to densely pubescent.</text>
      <biological_entity id="o18317" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale lavender" name="coloration" src="d0_s7" to="reddish purple" />
        <character constraint="toward tip" constraintid="o18318" is_modifier="false" name="coloration" src="d0_s7" value="cream-yellow" value_original="cream-yellow" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18318" name="tip" name_original="tip" src="d0_s7" type="structure" />
      <biological_entity id="o18319" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s7" value="expanded" value_original="expanded" />
        <character is_modifier="false" modifier="very" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="crispate" value_original="crispate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o18320" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="abaxially sparsely; sparsely to densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes: bodies silky-pubescent;</text>
      <biological_entity id="o18321" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <biological_entity id="o18322" name="body" name_original="bodies" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="silky-pubescent" value_original="silky-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak 2.5-6 cm, plumose.</text>
      <biological_entity id="o18323" name="achene" name_original="achenes" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 16.</text>
      <biological_entity id="o18324" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s9" to="6" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18325" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wooded cliffs and stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wooded cliffs" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., D.C., Ga., Ill., Ind., Ky., Md., Miss., Mo., N.C., Ohio, Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Leather-flower</other_name>
  <discussion>Formerly Clematis viorna was locally naturalized near Guelph, Ontario; no recent reports are known. D.S. Correll and M.C. Johnston (1970) mention "a report of a specimen" from Texas; neither the specimen nor further details have been located.</discussion>
  <discussion>The Fox Indians prepared a drink from the roots of Clematis viorna to use medicinally as a panacea (D. E. Moerman 1986).</discussion>
  
</bio:treatment>