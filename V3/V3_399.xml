<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="Ewan" date="1936" rank="subsection">Subscaposa</taxon_name>
    <taxon_name authority="A. Gray" date="1887" rank="species">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>12: 53. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection subscaposa;species parryi;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500533</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (10-) 40-80 (-110) cm;</text>
      <biological_entity id="o14965" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base reddish, puberulent.</text>
      <biological_entity id="o14966" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves variably distributed;</text>
      <biological_entity id="o14967" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="variably" name="arrangement" src="d0_s2" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>green leaves usually absent on proximal 1/5 of stem at anthesis;</text>
      <biological_entity id="o14968" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character constraint="on proximal 1/5" constraintid="o14969" is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14969" name="1/5" name_original="1/5" src="d0_s3" type="structure" />
      <biological_entity id="o14970" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <relation from="o14969" id="r2087" name="part_of" negation="false" src="d0_s3" to="o14970" />
    </statement>
    <statement id="d0_s4">
      <text>basal leaves 0-9 at anthesis;</text>
      <biological_entity constraint="basal" id="o14971" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s4" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline leaves 2-7 at anthesis;</text>
      <biological_entity constraint="cauline" id="o14972" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="2" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole 1-13 cm.</text>
      <biological_entity id="o14973" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaf-blade pentagonal, 1-7 × 2-10 cm, ± puberulent;</text>
      <biological_entity id="o14974" name="leaf-blade" name_original="leaf-blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="pentagonal" value_original="pentagonal" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ultimate lobes 3-27, width 1-20 mm (basal), 0.5-5 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o14975" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="27" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences (2-) 8-24 (-48) -flowered, cylindric;</text>
      <biological_entity id="o14976" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="(2-)8-24(-48)-flowered" value_original="(2-)8-24(-48)-flowered" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicel ± spreading, (0.5-) 1-3 (-6.8) cm, usually puberulent;</text>
      <biological_entity id="o14977" name="pedicel" name_original="pedicel" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="6.8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracteoles 2-7 (-16) mm from flowers, green to blue, lance-linear, 2-6 (-10) mm, puberulent.</text>
      <biological_entity id="o14978" name="bracteole" name_original="bracteoles" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="16" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o14979" from="2" from_unit="mm" name="location" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s11" to="blue" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="lance-linear" value_original="lance-linear" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o14979" name="flower" name_original="flowers" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals dark blue to bluish purple, puberulent, lateral sepals spreading or reflexed, (7-) 10-20 (-25) × 4-9 mm, spurs straight, ascending 0-30° above horizontal, 9-17 (-21) mm;</text>
      <biological_entity id="o14980" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14981" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="dark blue" name="coloration" src="d0_s12" to="bluish purple" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o14982" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s12" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s12" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14983" name="spur" name_original="spurs" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character constraint="above horizontal" is_modifier="false" modifier="0-30°" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="21" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower petal blades slightly elevated, ± exposing stamens, 3-10 mm, clefts 2-6 mm;</text>
      <biological_entity id="o14984" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="petal" id="o14985" name="blade" name_original="blades" src="d0_s13" type="structure" constraint_original="lower petal">
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s13" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="3" from_unit="mm" modifier="more or less" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14986" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o14987" name="cleft" name_original="clefts" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o14985" id="r2088" modifier="more or less" name="exposing" negation="false" src="d0_s13" to="o14986" />
    </statement>
    <statement id="d0_s14">
      <text>hairs mostly near base of cleft, centered or on inner lobes, white.</text>
      <biological_entity id="o14988" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o14989" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o14990" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o14991" name="centered" name_original="centered" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s14" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity constraint="inner" id="o14992" name="lobe" name_original="lobes" src="d0_s14" type="structure" />
      <relation from="o14989" id="r2089" name="near" negation="false" src="d0_s14" to="o14990" />
      <relation from="o14990" id="r2090" name="part_of" negation="false" src="d0_s14" to="o14991" />
      <relation from="o14990" id="r2091" name="part_of" negation="false" src="d0_s14" to="o14992" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits 10-19 mm, 2.8-4 times longer than wide, puberulent or glabrous.</text>
      <biological_entity id="o14993" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="19" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s15" value="2.8-4" value_original="2.8-4" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds: seed-coat cells ± brick-shaped, cell margins undulate, surfaces ± roughened.</text>
      <biological_entity id="o14994" name="seed" name_original="seeds" src="d0_s16" type="structure" />
      <biological_entity constraint="seed-coat" id="o14995" name="cell" name_original="cells" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="brick--shaped" value_original="brick--shaped" />
      </biological_entity>
      <biological_entity constraint="cell" id="o14996" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o14997" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief_or_texture" src="d0_s16" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America (Calif.)</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America (Calif.)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <discussion>Subspecies 5 (5 in the flora).</discussion>
  <discussion>A number of local phases are found in Delphinium parryi. Five of these appear consistently distinct and are recognized here. Other phases may be locally distinct but grade into other nearby phases. Delphinium parryi hybridizes with D. cardinale (D. ×inflexum Davidson).</discussion>
  <discussion>The Kawaiisu used the ground root of Delphinium parryi medicinally as a salve for swollen limbs (D. E. Moerman 1986, no subspecies specified).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaves usually absent at anthesis.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaves usually present at anthesis.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lateral sepals 16–25 mm.</description>
      <determination>23d Delphinium parryi subsp. blochmaniae</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lateral sepals 9–15 mm.</description>
      <determination>23a Delphinium parryi subsp. parryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Lateral sepals 7–11 mm; above 700 m elevation.</description>
      <determination>23c Delphinium parryi subsp. purpureum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Lateral sepals (9–)12–20 mm; below 700 m elevation.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals usually reflexed.</description>
      <determination>23e Delphinium parryi subsp. eastwoodiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals usually spreading.</description>
      <determination>23b Delphinium parryi subsp. maritimum</determination>
    </key_statement>
  </key>
</bio:treatment>