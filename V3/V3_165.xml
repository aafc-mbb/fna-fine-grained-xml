<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="Britton" date="1892" rank="species">macounii</taxon_name>
    <place_of_publication>
      <publication_title>Trans. New York Acad. Sci.</publication_title>
      <place_in_publication>12: 3. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species macounii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501174</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">macounii</taxon_name>
    <taxon_name authority="(A. Gray) K. C. Davis" date="unknown" rank="variety">oreganus</taxon_name>
    <taxon_hierarchy>genus Ranunculus;species macounii;variety oreganus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems prostrate to nearly erect, often rooting nodally, hirsute or glabrous, base not bulbous.</text>
      <biological_entity id="o8345" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s0" to="nearly erect" />
        <character is_modifier="false" modifier="often; nodally" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8346" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots never tuberous.</text>
      <biological_entity id="o8347" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaf-blades cordate to reniform in outline, 3-foliolate, 3.7-7.5 × 4.5-9.5 cm, leaflets 3-lobed or parted, ultimate segments elliptic or lance-elliptic, margins toothed or lobulate, apex acute to broadly acute.</text>
      <biological_entity constraint="basal" id="o8348" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="in outline" constraintid="o8349" from="cordate" name="shape" src="d0_s2" to="reniform" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
        <character char_type="range_value" from="3.7" from_unit="cm" name="length" src="d0_s2" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="width" src="d0_s2" to="9.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8349" name="outline" name_original="outline" src="d0_s2" type="structure" />
      <biological_entity id="o8350" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="parted" value_original="parted" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o8351" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="lance-elliptic" value_original="lance-elliptic" />
      </biological_entity>
      <biological_entity id="o8352" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lobulate" value_original="lobulate" />
      </biological_entity>
      <biological_entity id="o8353" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="broadly acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: receptacle hirsute;</text>
      <biological_entity id="o8354" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o8355" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals spreading or reflexed ca. 1 mm above base, 4-6 × 1.5-3 mm, glabrous or hirsute;</text>
      <biological_entity id="o8356" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o8357" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character constraint="above base" constraintid="o8358" name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o8358" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals 5, yellow, 4-6 × 3.5-5 mm.</text>
      <biological_entity id="o8359" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o8360" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes globose or ovoid, 7-11 × 7-10 mm;</text>
      <biological_entity id="o8361" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8362" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o8361" id="r1170" name="part_of" negation="false" src="d0_s6" to="o8362" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 2.4-3 × 2-2.4 mm, glabrous, margin forming narrow rib 0.1-0.2 mm wide;</text>
      <biological_entity id="o8363" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8364" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8365" name="rib" name_original="rib" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o8364" id="r1171" name="forming" negation="false" src="d0_s7" to="o8365" />
    </statement>
    <statement id="d0_s8">
      <text>beak persistent, lanceolate to broadly lanceolate, straight or nearly so, 1-1.2 mm. 2n = 32, 48.</text>
      <biological_entity id="o8366" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="broadly lanceolate" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character name="course" src="d0_s8" value="nearly" value_original="nearly" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8367" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="32" value_original="32" />
        <character name="quantity" src="d0_s8" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, depressions in woodlands, ditches, edges of streams and ponds, on wet soil or emergent from shallow water</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="depressions" constraint="in woodlands , ditches , edges of streams and ponds ," />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="edges" constraint="of streams and ponds" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="wet soil" modifier="on" />
        <character name="habitat" value="emergent" />
        <character name="habitat" value="shallow water" modifier="from" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Nfld. and Labr. (Nfld.), N.W.T., Ont., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Mich., Minn., Mont., Nebr., Nev., N.Mex., N.Dak., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Renoncule de Macoun</other_name>
  <discussion>Through most of its range, Ranunculus macounii has conspicuously hispid herbage. Glabrous plants are found, however, in the lower Columbia River valley (southwestern Washington and adjacent Oregon). This variant has been called R. macounii var. oreganus.</discussion>
  
</bio:treatment>