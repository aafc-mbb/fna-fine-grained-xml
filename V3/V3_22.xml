<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="(Prantl) L. D. Benson" date="1936" rank="section">epirotes</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">nivalis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 553. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section epirotes;species nivalis;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233501178</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect from short caudices, 4-22 cm, glabrous or sparsely pilose, each with 1 flower.</text>
      <biological_entity id="o9474" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="from caudices" constraintid="o9475" is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="22" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o9475" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o9476" name="flower" name_original="flower" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="1" value_original="1" />
      </biological_entity>
      <relation from="o9474" id="r1333" name="with" negation="false" src="d0_s0" to="o9476" />
    </statement>
    <statement id="d0_s1">
      <text>Roots slender, 0.4-0.8 mm thick.</text>
      <biological_entity id="o9477" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="thickness" src="d0_s1" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves persistent or deciduous, blades reniform, 3-parted, 0.6-2 × 1.3-3 cm, at least lateral segments again lobed or margins toothed, base truncate or cordate, apices of segments rounded-apiculate.</text>
      <biological_entity constraint="basal" id="o9478" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o9479" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-parted" value_original="3-parted" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9480" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="at-least" name="position" src="d0_s2" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="again" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o9481" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o9482" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o9483" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="rounded-apiculate" value_original="rounded-apiculate" />
      </biological_entity>
      <biological_entity id="o9484" name="segment" name_original="segments" src="d0_s2" type="structure" />
      <relation from="o9483" id="r1334" name="part_of" negation="false" src="d0_s2" to="o9484" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers: pedicels glabrous or brown-pilose;</text>
      <biological_entity id="o9485" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o9486" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="brown-pilose" value_original="brown-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>receptacle glabrous;</text>
      <biological_entity id="o9487" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o9488" name="receptacle" name_original="receptacle" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 6-8 × 3-5 mm, abaxially densely brown-hispid;</text>
      <biological_entity id="o9489" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o9490" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s5" value="brown-hispid" value_original="brown-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 5 (-6), 8-11 × 7-12 mm;</text>
      <biological_entity id="o9491" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o9492" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="6" />
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>nectary scale glabrous.</text>
      <biological_entity id="o9493" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="nectary" id="o9494" name="scale" name_original="scale" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads of achenes cylindric or ovoid-cylindric, 7-14 × 5-6 mm;</text>
      <biological_entity id="o9495" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid-cylindric" value_original="ovoid-cylindric" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9496" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <relation from="o9495" id="r1335" name="part_of" negation="false" src="d0_s8" to="o9496" />
    </statement>
    <statement id="d0_s9">
      <text>achenes 1.5-2.2 × 1.2-1.6 mm, glabrous;</text>
      <biological_entity id="o9497" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>beak slender, straight, 1-2 mm. 2n = 48.</text>
      <biological_entity id="o9498" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9499" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet or dry alpine meadows, often around late snowbeds, cliffs, and streamsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="dry alpine meadows" />
        <character name="habitat" value="late snowbeds" modifier="often" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="streamsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Nfld. and Labr. (Nfld.), N.W.T., Que., Yukon; Alaska; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="common_name">Renoncule nivale</other_name>
  
</bio:treatment>