<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Marilyn M. Park, Dennis Festerling Jr.</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">THALICTRUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 545. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 242. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus THALICTRUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Thaliktron, an ancient name used by Dioscorides</other_info_on_name>
    <other_info_on_name type="fna_id">132688</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from woody rhizomes, caudices, or tuberous roots.</text>
      <biological_entity id="o22031" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o22032" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o22033" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o22034" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="tuberous" value_original="tuberous" />
      </biological_entity>
      <relation from="o22031" id="r3012" name="from" negation="false" src="d0_s0" to="o22032" />
      <relation from="o22031" id="r3013" name="from" negation="false" src="d0_s0" to="o22033" />
      <relation from="o22031" id="r3014" name="from" negation="false" src="d0_s0" to="o22034" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, proximal leaves petiolate, distal leaves sessile;</text>
      <biological_entity id="o22035" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o22036" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o22037" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves alternate.</text>
      <biological_entity constraint="cauline" id="o22038" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade 1-4×-ternately or pinnately compound;</text>
      <biological_entity id="o22039" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets cordate-reniform, obovate, lanceolate, or linear, sometimes 3-lobed or more, margins entire or crenate.</text>
      <biological_entity id="o22040" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate-reniform" value_original="cordate-reniform" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o22041" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, sometimes also axillary, (1-) 2-200-flowered panicles, racemes, corymbs, umbels, or flowers solitary, to 41 cm;</text>
      <biological_entity id="o22042" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o22043" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="(1-)2-200-flowered" value_original="(1-)2-200-flowered" />
      </biological_entity>
      <biological_entity id="o22044" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
      <biological_entity id="o22045" name="corymb" name_original="corymbs" src="d0_s5" type="structure" />
      <biological_entity id="o22046" name="umbel" name_original="umbels" src="d0_s5" type="structure" />
      <biological_entity id="o22047" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="41" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucres absent or present, involucral-bracts 2-3 (these compound, often resembling whorl of 6-9 simple bracts), leaflike, not closely subtending flowers.</text>
      <biological_entity id="o22048" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o22049" name="involucral-bract" name_original="involucral-bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o22050" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <relation from="o22049" id="r3015" name="closely subtending" negation="true" src="d0_s6" to="o22050" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers all bisexual, bisexual and unisexual on same plant, or all unisexual with sexes on same or different plants, radially symmetric;</text>
      <biological_entity id="o22051" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character constraint="on plant" constraintid="o22052" is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character constraint="on plants" constraintid="o22053" is_modifier="false" name="sexes" notes="" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" notes="" src="d0_s7" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o22052" name="plant" name_original="plant" src="d0_s7" type="structure" />
      <biological_entity id="o22053" name="plant" name_original="plants" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>sepals not persistent in fruit, 4-10, whitish to greenish yellow or purplish, plane, lanceolate to reniform or spatulate, 1-18 mm;</text>
      <biological_entity id="o22054" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o22055" is_modifier="false" modifier="not" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="whitish" name="coloration" notes="" src="d0_s8" to="greenish yellow or purplish" />
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="reniform or spatulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22055" name="fruit" name_original="fruit" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals absent;</text>
      <biological_entity id="o22056" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 7-30;</text>
      <biological_entity id="o22057" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s10" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments filiform to clavate or distally dilated;</text>
      <biological_entity id="o22058" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s11" to="clavate or distally dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>staminodes absent between stamens and pistils;</text>
      <biological_entity id="o22059" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character constraint="between stamens, pistils" constraintid="o22060, o22061" is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o22060" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o22061" name="pistil" name_original="pistils" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>pistils 1-16, simple;</text>
      <biological_entity id="o22062" name="pistil" name_original="pistils" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="16" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovule 1 per pistil;</text>
      <biological_entity id="o22063" name="ovule" name_original="ovule" src="d0_s14" type="structure">
        <character constraint="per pistil" constraintid="o22064" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o22064" name="pistil" name_original="pistil" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style present or absent.</text>
      <biological_entity id="o22065" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits achenes, usually aggregate, sessile or stipitate, ovoid to obovoid, falcate, or discoid, sides prominently veined or ribbed;</text>
      <biological_entity constraint="fruits" id="o22066" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s16" value="aggregate" value_original="aggregate" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s16" to="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="discoid" value_original="discoid" />
      </biological_entity>
      <biological_entity id="o22067" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s16" value="veined" value_original="veined" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak present or absent, terminal, straight to coiled, 0-4 mm. x = 7.</text>
      <biological_entity id="o22068" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="coiled" value_original="coiled" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="x" id="o22069" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide, mostly temperate.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="mostly temperate" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Meadow-rue</other_name>
  <other_name type="common_name">pigamon</other_name>
  <discussion>Species 120-200 (22 in the flora).</discussion>
  <discussion>Thalictrum is a taxonomically difficult genus that should be carefully researched through additional population-based field studies. Past treatments of Thalictrum have often emphasized leaf characters that are highly variable in most species; they are therefore of poor diagnostic value and not indicati relationships. Because of the paucity of field studies and a continuing emphasis on highly variable characters, the literature is replete with names that do not represent distinct entities. Often mixes of character states can be found within a single population; many of the character states used in past studies were neither ecologically nor geographically distinct.</discussion>
  <discussion>Some species of Thalictrum have been divided into varieties by previous authors. In the absence of carefully collected, supporting evidence from field studies, we are unwilling to perpetuate the use of any infraspecific names.</discussion>
  <discussion>Characters useful in identifying species of Thalictrum include leaflet shape, degree of dilation of filaments, anther length, shape of anther apex, achene shape and venation patterns, and vestiture (glands and/or hairs) of leaves and achenes. Leaflets described in this treatment are the central, distalmost of a midstem leaf; proximal and distal leaves are more variable and often not representative of the species. Stigma and filament colors refer to fresh material in the following descriptions.</discussion>
  <discussion>In Thalictrum species, the stigma extends down the side of the style, so length of style in fruit (beak) includes the stigma.</discussion>
  <discussion>For many species no reliable characteristics for the identification of staminate material are known. Extensive field work and careful analysis are required to determine if such characteristics exist.</discussion>
  <discussion>In a narrow strip from southeastern Ontario to Ohio to Louisiana, some individuals of some species in Thalictrum section Leucocoma may lack their normal vestiture. In the absence of glands or pubescence, the differences among species are difficult to describe. The remaining characteristics overlap considerably. The species involved may be identified in the final couplets of the key as follows: if the plant in hand falls into the area of overlap for the first character of the couplet, go on to the next character, and so forth, until a distinguishing character is found. One or more of the characters offered should distinguish the infrequent, problematic individual.</discussion>
  <discussion>Several species of Thalictrum are used as ornamentals. At least one species, T. aquilegiifolium Linnaeus, occasionally escapes cultivation in Ontario and Quebec and possibly elsewhere. The plant is tall (40-100 cm); flowers bisexual, mauve to pink; and achenes few, filiform, 3-winged, stipitate, very small, and hidden at anthesis among the bases of long, rigid stamens.</discussion>
  <discussion>Numerous alkaloids have been identified from plants of the genus, some with pharmacologic potential. Some exhibit antimicrobial activity; others inhibit growth of tumors or lower blood pressure in mammals.</discussion>
  <references>
    <reference>Boivin, B. 1944. American Thalictra and their Old World allies. Rhodora 46: 337-377, 391-445, 453-487.</reference>
    <reference>Boivin, B. 1948. Key to Canadian species of Thalictra. Canad. Field-Naturalist 62: 169-170.</reference>
    <reference>Lecoyer, J. C. 1885. Monographie du genre Thalictrum. Bull. Soc. Roy. Bot. Belgique 24: 78-324.</reference>
    <reference>Park, M. M. 1992. A Biosystematic Study of Thalictrum Section Leucocoma (Ranunculaceae). Ph.D. dissertation. Pennsylvania State University.</reference>
    <reference>Tamura, M. 1992. A new classification of the family Ranunculaceae. Acta Phytotax. Geobot. 43: 53-58.</reference>
    <reference>Tamura, M. 1968. Morphology, ecology, and phylogeny of the Ranunculaceae. VIII. Sci. Rep. Coll. Gen. Educ. Osaka Univ. 17: 41-56.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences umbels or flowers solitary (sect. Anemonella).</description>
      <determination>1 Thalictrum thalictroides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences panicles, racemes, or corymbs.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers bisexual; sepals 5 (often 4 in T. alpinum).</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers unisexual, or unisexual and bisexual, rarely only bisexual; sepals 4(–6).</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Achenes sessile to nearly sessile; filaments filiform (sect. Thalictrum).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Achenes stipitate; filaments ± dilated distally.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems 15–150 cm; sepals 3–4mm; achenes 3–15.</description>
      <determination>2 Thalictrum minus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems (3–)5–20(–30) cm; sepals 1–2.3(–2.7) mm; achenes 2–6.</description>
      <determination>3 Thalictrum alpinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Filaments weakly dilated; achenes short-stipitate, stipe less than 1.5 mm, body (4–)5–6 m; Canada, w United States (sect. Omalophysa).</description>
      <determination>6 Thalictrum sparsiflorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Filaments strongly clavate; achenes long-stipitate, stipe 1–3.5(–4) mm, body 2.5–5 mm; se United States (sect. Physocarpum).</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Adaxial margin of achene concave, ca. 2 times length of stipe; filaments 2.5–4 mm.</description>
      <determination>4 Thalictrum clavatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Adaxial margin of achene straight, ± equaling length of stipe; filaments 2–3 mm</description>
      <determination>5 Thalictrum mirabile</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaflets apically 3–12-lobed, lobe margins crenate (rarely entire in T. debile); filaments variously colored, rarely white, filiform (sect. Heterogamia).</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaflets undivided or 3-lobed apically, lobe margins entire (some leaflet margins on some individuals rarely crenate); filaments usually white, rarely lavender, filiform to clavate (sect. Leucocoma).</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Lateral veins of achene anastomosing-reticulate.</description>
      <determination>17 Thalictrum polycarpum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Lateral veins of achene not reticulate, veins parallel, converging, or rarely branched.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Achenes laterally compressed.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Achenes not laterally compressed, or very slightly so.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blade membranous, green; leaflets (5–)10–20 × (6–)8– 12(–18) mm; stems (20–)30–60(–150) cm; achenes 7–11(–14) per flower.</description>
      <determination>15 Thalictrum fendleri</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blade leathery, glaucous; leaflets 5–8 × 4–5 mm; stems 14–50 cm; achenes 4–5(–6) per flower.</description>
      <determination>16 Thalictrum heliophilum</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Achenes stipitate; stipe 0.7–2.5 mm.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Achenes nearly sessile; stipe 0–0.3 mm.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Achenes erect; beak 1.5–3 mm.</description>
      <determination>8 Thalictrum coriaceum</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Achenes spreading to reflexed; beak 3–4.5(–6) mm.</description>
      <determination>14 Thalictrum occidentale</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Achenes incurved.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Achenes straight.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Beak (2–)2.5–4(–5) mm; adaxial surface of achene 4–6 mm.</description>
      <determination>12 Thalictrum confine</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Beak 1.5–2.5(–3) mm; adaxial surface of achene 3– 4(–6) mm.</description>
      <determination>13 Thalictrum venulosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Roots fibrous; stems erect, 30–80 cm; largest leaflets more than 15 mm wide.</description>
      <determination>7 Thalictrum dioicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Roots tuberous; stems reclining to erect, usually less than 30(–45) cm; largest leaflets less than 15 mm wide.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Beak 0.5–1 mm; achenes ovoid; stems erect; roots black when dry.</description>
      <determination>11 Thalictrum texanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Beak 1.3–2mm; achenes oblong to elliptic-lanceolate; stems reclining or decumbent; roots brown.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Achenes 0.7–1.2 mm wide, veins 6–8, prominent; beak 1.3–3 mm.</description>
      <determination>9 Thalictrum debile</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Achenes 1.5–2 mm wide, veins 10–12; beak (1.3–)2.3–3 mm.</description>
      <determination>10 Thalictrum arkansanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Achenes, peduncles, abaxial surfaces of leaflets, and/or petioles and rachises with stipitate glands.</description>
      <determination>19 Thalictrum amphibolum</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Achenes, peduncles, abaxial surfaces of leaflets, and/or petioles and rachises without stipitate glands.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Achenes, peduncles, abaxial surfaces of leaflets, and/or petioles and rachises with minute papillae (i.e., sessile glands), may also be pubescent.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Achenes, peduncles, abaxial surfaces of leaflets, petioles, and rachises without papillae, may be pubescent or glabrous.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaflet length 0.9–5.25 times width; nonglandular trichomes absent; filaments 2.5–7.8 mm; anthers (0.7–)1.2–2.7(–3) mm; stipe 0.2–1.7 mm; e North America, rare w of Missouri.</description>
      <determination>19 Thalictrum amphibolum</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaflet length 0.9–2.6 times width; nonglandular trichomes present or absent; filaments 2–6.5 mm; anthers 1–3.6(–4) mm; stipe 0–1.1 mm; c North America, very rare e of Ohio.</description>
      <determination>20 Thalictrum dasycarpum</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Achenes, peduncles, abaxial surfaces of leaflets, and/or petioles and rachises pubescent.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Achenes, peduncles, abaxial surfaces of leaflets, and/or petioles and rachises glabrous.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Anthers less than 1.5 mm, apex blunt or slightly apiculate; filaments rigid, ascending, prominently clavate; beak straight or coiled distally, ca. 1/2 as long as achene body.</description>
      <determination>18 Thalictrum pubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Anthers usually 1–3.6(–4) mm, apex usually strongly apiculate; filaments flexible, drooping, filiform, scarcely dilated distally; beak ± straight, filiform, about as long as achene body.</description>
      <determination>20 Thalictrum dasycarpum</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Leaflets linear to narrowly lanceolate or oblanceolate, (2.6–)4–26 times longer than wide.</description>
      <determination>22 Thalictrum cooleyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Leaflets nearly orbiculate to ovate, or lanceolate to obovate, usually less than 4 times longer than wide.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaflets undivided or apically 2–3-lobed, largest usually less than 22 mm wide; filaments 1.8–4 mm; se United States.</description>
      <determination>21 Thalictrum macrostylum</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaflets apically 3-lobed, seldom undivided, largest usually 15–60 mm or more wide; filaments 1.5– 7.8 mm; Ontario to Ohio to Louisiana.</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Anthers 0.5–1.5(–2.1) mm; stigma straight or distally coiled; flowers often bisexual.</description>
      <determination>18 Thalictrum pubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Anthers (0.7–)1–3.6 mm; stigma straight, ± filiform; flowers rarely bisexual (included here are very infrequent forms of T. dasycarpum and T. revolutum).</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaflet length 0.9–5.25 times width; filaments 2.5–7.8 mm; anthers (0.7–)1.2– 2.7(–3) mm; stipe 0.2–1.7 mm; e North America, infrequent w of Missouri.</description>
      <determination>19 Thalictrum amphibolum</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaflet length 0.9–2.6 times width; filaments 2–6.5 mm; anthers 1–3.6(–4) mm; stipe 0–1.1 mm; w, c North America, very infrequent e of Ohio.</description>
      <determination>20 Thalictrum dasycarpum</determination>
    </key_statement>
  </key>
</bio:treatment>