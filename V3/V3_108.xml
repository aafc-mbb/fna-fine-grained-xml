<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">aquilegia</taxon_name>
    <taxon_name authority="Munz" date="1946" rank="species">hinckleyana</taxon_name>
    <place_of_publication>
      <publication_title>Gentes Herb.</publication_title>
      <place_in_publication>7: 141. 1946</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus aquilegia;species hinckleyana</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500109</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aquilegia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">chrysantha</taxon_name>
    <taxon_name authority="(Munz) E. J. Lott" date="unknown" rank="variety">hinckleyana</taxon_name>
    <taxon_hierarchy>genus Aquilegia;species chrysantha;variety hinckleyana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 50-70 cm.</text>
      <biological_entity id="o548" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 2×-ternately compound, 30-40 cm, much shorter than stems;</text>
      <biological_entity constraint="basal" id="o549" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="2×-ternately" name="architecture" src="d0_s1" value="compound" value_original="compound" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character constraint="than stems" constraintid="o550" is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o550" name="stem" name_original="stems" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>leaflets green adaxially, to 20-40 mm, not viscid;</text>
      <biological_entity id="o551" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s2" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>primary petiolules 25-50 mm (leaflets not crowded), pilose.</text>
      <biological_entity constraint="primary" id="o552" name="petiolule" name_original="petiolules" src="d0_s3" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s3" to="50" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers suberect;</text>
      <biological_entity id="o553" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="suberect" value_original="suberect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals perpendicular to floral axis, yellow, ovate, 25-34 × 14-18 mm, apex obtuse;</text>
      <biological_entity id="o554" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="perpendicular" value_original="perpendicular" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s5" to="34" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="floral" id="o555" name="axis" name_original="axis" src="d0_s5" type="structure" />
      <biological_entity id="o556" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals: spurs yellow, straight, ± parallel, 40-56 mm, slender, evenly tapered from base, blades yellow, oblong, 19-23 × 13-17 mm;</text>
      <biological_entity id="o557" name="petal" name_original="petals" src="d0_s6" type="structure" />
      <biological_entity id="o558" name="spur" name_original="spurs" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s6" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s6" to="56" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character constraint="from base" constraintid="o559" is_modifier="false" modifier="evenly" name="shape" src="d0_s6" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o559" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o560" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="19" from_unit="mm" name="length" src="d0_s6" to="23" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s6" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 17-20 mm.</text>
      <biological_entity id="o561" name="petal" name_original="petals" src="d0_s7" type="structure" />
      <biological_entity id="o562" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Follicles 20-25 mm;</text>
      <biological_entity id="o563" name="follicle" name_original="follicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak 20 mm.</text>
      <biological_entity id="o564" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dripping cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" modifier="dripping" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Hinckley's columbine</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Aquilegia hinckleyana is endemic to Capote Falls, Presidio County, Texas. The key and description above are based on specimens seen. E. J. Lott (1979) gave the range of sepal width in A. hinckleyana as 9-17 mm, thus overlapping the range in A. chrysantha. She considered sepal shape to be the most reliable key character for distinguishing these species, with sepals less than 2.5 times as long as wide in A. hinckleyana and more than 2.5 times as long as wide in A. chrysantha. Perhaps because of the overlap in characters, she later reduced A. hinckleyana to a variety of A. chrysantha (E. J. Lott 1985). Until her data are published, I prefer to follow the established taxonomy.</discussion>
  
</bio:treatment>