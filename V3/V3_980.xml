<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="G. Don in J. C. Loudon" date="1830" rank="section">lobatae</taxon_name>
    <taxon_name authority="Lamarck in J. Lamarck et al." date="1785" rank="species">velutina</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al.,  Encycl.</publication_title>
      <place_in_publication>1: 721. 1785</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section lobatae;species velutina</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501095</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="W. Bartram" date="unknown" rank="species">tinctoria</taxon_name>
    <taxon_hierarchy>genus Quercus;species tinctoria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, deciduous, to 25 m.</text>
      <biological_entity id="o18716" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="25" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark dark-brown to black, deeply furrowed, ridges often broken into irregular blocks, inner bark yellow or orange.</text>
      <biological_entity id="o18717" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s1" to="black" />
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s1" value="furrowed" value_original="furrowed" />
      </biological_entity>
      <biological_entity id="o18718" name="ridge" name_original="ridges" src="d0_s1" type="structure">
        <character constraint="into blocks" constraintid="o18719" is_modifier="false" modifier="often" name="condition_or_fragility" src="d0_s1" value="broken" value_original="broken" />
      </biological_entity>
      <biological_entity id="o18719" name="block" name_original="blocks" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s1" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18720" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="orange" value_original="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs dark reddish-brown, (1.5-) 2.5-4.5 (-5) mm diam., glabrous or sparsely pubescent.</text>
      <biological_entity id="o18721" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s2" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s2" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Terminal buds ovoid or ellipsoid to subconic, 6-12 mm, noticeably 5-angled in cross-section, tawny or gray-pubescent.</text>
      <biological_entity constraint="terminal" id="o18722" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s3" to="subconic" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" />
        <character constraint="in cross-section" constraintid="o18723" is_modifier="false" modifier="noticeably" name="shape" src="d0_s3" value="5-angled" value_original="5-angled" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="gray-pubescent" value_original="gray-pubescent" />
      </biological_entity>
      <biological_entity id="o18723" name="cross-section" name_original="cross-section" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 25-70 mm, glabrous to sparsely pubescent.</text>
      <biological_entity id="o18724" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o18725" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="sparsely pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade ovate to obovate, (80-) 100-300 × 80-150 mm, base obtuse to truncate, inequilateral, margins with 5-9 lobes and 15-50 awns, lobes oblong or distally expanded, separated by deep sinuses, apex acute to obtuse;</text>
      <biological_entity id="o18726" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="80" from_unit="mm" name="atypical_length" src="d0_s5" to="100" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s5" to="300" to_unit="mm" />
        <character char_type="range_value" from="80" from_unit="mm" name="width" src="d0_s5" to="150" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18727" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="inequilateral" value_original="inequilateral" />
      </biological_entity>
      <biological_entity id="o18728" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o18729" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o18730" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o18731" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="expanded" value_original="expanded" />
        <character constraint="by sinuses" constraintid="o18732" is_modifier="false" name="arrangement" src="d0_s5" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o18732" name="sinuse" name_original="sinuses" src="d0_s5" type="structure">
        <character is_modifier="true" name="depth" src="d0_s5" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o18733" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <relation from="o18728" id="r2504" name="with" negation="false" src="d0_s5" to="o18729" />
      <relation from="o18728" id="r2505" name="with" negation="false" src="d0_s5" to="o18730" />
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially pale green, glabrous except for small axillary tufts of tomentum or with scattered pubescence, especially along veins, adaxially glossy, dark green, glabrous, secondary-veins raised on both surfaces.</text>
      <biological_entity id="o18734" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale green" value_original="pale green" />
        <character constraint="except-for axillary tufts" constraintid="o18735" is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o18735" name="tuft" name_original="tufts" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="small" value_original="small" />
        <character constraint="on surfaces" constraintid="o18739" is_modifier="false" name="prominence" src="d0_s6" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o18736" name="tomentum" name_original="tomentum" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18737" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="character" src="d0_s6" value="pubescence" value_original="pubescence" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18738" name="secondary-vein" name_original="secondary-veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="character" src="d0_s6" value="pubescence" value_original="pubescence" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18739" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <relation from="o18735" id="r2506" name="part_of" negation="false" src="d0_s6" to="o18736" />
      <relation from="o18735" id="r2507" name="part_of" negation="false" src="d0_s6" to="o18737" />
      <relation from="o18735" id="r2508" name="part_of" negation="false" src="d0_s6" to="o18738" />
    </statement>
    <statement id="d0_s7">
      <text>Acorns biennial;</text>
      <biological_entity id="o18740" name="acorn" name_original="acorns" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cup cupshaped or turbinate, 7-14 mm high × 12-22 mm wide, covering 1/2 nut, cup margins not involute, outer surface puberulent, inner surface pubescent, scale tips loose, especially at margin of cup, acute to acuminate;</text>
      <biological_entity id="o18741" name="cup" name_original="cup" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cup-shaped" value_original="cup-shaped" />
        <character is_modifier="false" name="shape" src="d0_s8" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="14" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="×12-22" value_original="×12-22" />
      </biological_entity>
      <biological_entity id="o18742" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity constraint="cup" id="o18743" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18744" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18745" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="scale" id="o18746" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s8" value="loose" value_original="loose" />
        <character char_type="range_value" from="acute" name="shape" notes="" src="d0_s8" to="acuminate" />
      </biological_entity>
      <biological_entity id="o18747" name="margin" name_original="margin" src="d0_s8" type="structure" />
      <biological_entity id="o18748" name="cup" name_original="cup" src="d0_s8" type="structure" />
      <relation from="o18741" id="r2509" name="covering" negation="false" src="d0_s8" to="o18742" />
      <relation from="o18746" id="r2510" modifier="especially" name="at" negation="false" src="d0_s8" to="o18747" />
      <relation from="o18747" id="r2511" name="part_of" negation="false" src="d0_s8" to="o18748" />
    </statement>
    <statement id="d0_s9">
      <text>nut subglobose to ovoid, 10-20 × 10-18 mm, glabrate, scar diam. 5.5-12 mm. n = 12 ± 1;</text>
      <biological_entity id="o18749" name="nut" name_original="nut" src="d0_s9" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s9" to="ovoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s9" to="18" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="n" id="o18751" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="more,less" value="12" value_original="12" />
        <character name="quantity" src="d0_s9" unit="more,less" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>2n = 24.</text>
      <biological_entity id="o18750" name="scar" name_original="scar" src="d0_s9" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="diam" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18752" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Commonly on dry slopes and upland areas, occasionally on sandy lowlands (especially in north) and poorly drained uplands and terraces</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry slopes" modifier="commonly on" />
        <character name="habitat" value="upland areas" />
        <character name="habitat" value="sandy lowlands" modifier="occasionally on" />
        <character name="habitat" value="north" />
        <character name="habitat" value="drained uplands" modifier="poorly" />
        <character name="habitat" value="terraces" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  <other_name type="common_name">Black oak</other_name>
  <discussion>The bark of this species (quercitron) is rich in tannins and was once an important source of these chemicals used for tanning leather. (The yellow dye obtained from the bark is also called quercitron.)</discussion>
  <discussion>Native Americans used Quercus velutina medicinally for indigestion, chronic dysentery, mouth sores, chills and fevers, chapped skin, hoarseness, milky urine, lung trouble, sore eyes, and as a tonic, an antiseptic, and an emetic (D. E. Moerman 1986).</discussion>
  <discussion>Quercus velutina reportedly hybridizes with Q. coccinea, Q. ellipsoidalis (= Q. ×paleolithicola Trelease), Q. falcata [= Q. ×willdenowiana (Dippel) Zabel] (= Q. ×pinetorum Moldenke)], Q. ilicifolia (= Q. ×rehderi Trelease), Q. imbricaria (= Q. ×leana Nuttall), Q. incana, Q. laevis, and Q. laurifolia (= Q. ×cocksii Sargent, although E. J. Palmer [1948] challenged the validity of this claim), Q. marilandica, Q. nigra, Q. palustris (= Q. ×vaga E. J. Palmer &amp; Steyermark), Q. phellos (= Q. ×filialis Little), Q. rubra, Q. shumardii, and possibly Q. arkansana (D. M. Hunt 1989).</discussion>
  
</bio:treatment>