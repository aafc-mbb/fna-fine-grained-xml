<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">calycanthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">CALYCANTHUS</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1066. 1759, name conserved</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family calycanthaceae;genus CALYCANTHUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kályx, covering, cup, and anthos, flower</other_info_on_name>
    <other_info_on_name type="fna_id">105270</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Duhamel" date="unknown" rank="genus">Butneria</taxon_name>
    <taxon_hierarchy>genus Butneria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Twigs quadrangular to nearly terete, pubescent to glabrous.</text>
      <biological_entity id="o10211" name="twig" name_original="twigs" src="d0_s0" type="structure">
        <character char_type="range_value" from="quadrangular" name="shape" src="d0_s0" to="nearly terete" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s0" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Buds naked.</text>
      <biological_entity id="o10212" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2-ranked.</text>
      <biological_entity id="o10213" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade elliptic, ovate, or ovatelanceolate;</text>
      <biological_entity id="o10214" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces adaxially scabrous.</text>
      <biological_entity id="o10215" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="adaxially" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers maroon or reddish-brown, rarely greenish or green-tipped, with strawberry or pineapple scent.</text>
      <biological_entity id="o10217" name="strawberry" name_original="strawberry" src="d0_s5" type="structure" />
      <relation from="o10216" id="r1424" name="with" negation="false" src="d0_s5" to="o10217" />
    </statement>
    <statement id="d0_s6">
      <text>x = 11.</text>
      <biological_entity id="o10216" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green-tipped" value_original="green-tipped" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green-tipped" value_original="green-tipped" />
        <character is_modifier="false" name="scent" src="d0_s5" value="pineapple" value_original="pineapple" />
      </biological_entity>
      <biological_entity constraint="x" id="o10218" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Strawberry-shrub</other_name>
  <other_name type="common_name">sweetshrub</other_name>
  <other_name type="common_name">spicebush</other_name>
  <discussion>Species 3 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tepals oblong-elliptic to obovate-lanceolate, apex acute; stamens 10–20, oblong; mature hypanthium cylindric, ellipsoid, pyriform, or globose; lateral bud partially hidden by petiole base; e North America.</description>
      <determination>1 Calycanthus floridus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tepals linear to linear-spatulate or ovate-elliptic, apex rounded; stamens 10–15, linear to oblong-linear; mature hypanthium campanulate; lateral bud exposed; California.</description>
      <determination>2 Calycanthus occidentalis</determination>
    </key_statement>
  </key>
</bio:treatment>