<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">nymphaeaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">nymphaea</taxon_name>
    <taxon_name authority="Morong" date="1888" rank="species">leibergii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>13: 124, in note. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nymphaeaceae;genus nymphaea;species leibergii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500826</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castalia</taxon_name>
    <taxon_name authority="Morong" date="unknown" rank="species">leibergii</taxon_name>
    <taxon_hierarchy>genus Castalia;species leibergii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nymphaea</taxon_name>
    <taxon_name authority="Georgi" date="unknown" rank="species">tetragona</taxon_name>
    <taxon_name authority="(Morong) A. E. Porsild" date="unknown" rank="subspecies">leibergii</taxon_name>
    <taxon_hierarchy>genus Nymphaea;species tetragona;subspecies leibergii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes unbranched, erect, cylindric;</text>
      <biological_entity id="o23077" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s0" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stolons absent.</text>
      <biological_entity id="o23078" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole glabrous.</text>
      <biological_entity id="o23079" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23080" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade abaxially green to deep purple, adaxially green, ovate to elliptic, 3-19 × 2-15 cm, margins entire;</text>
      <biological_entity id="o23081" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="abaxially green" name="coloration" src="d0_s3" to="deep purple" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="19" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>venation radiate centrally, without weblike pattern, principal veins 7-13;</text>
      <biological_entity id="o23082" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="centrally" name="architecture_or_arrangement" src="d0_s4" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o23083" name="pattern" name_original="pattern" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="weblike" value_original="weblike" />
      </biological_entity>
      <biological_entity constraint="principal" id="o23084" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s4" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>surfaces glabrous.</text>
      <biological_entity id="o23085" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers floating, 3-7.5 cm diam., opening and closing diurnally, only sepals and outermost (occasionally innermost) petals in distinct whorls of 4;</text>
      <biological_entity id="o23086" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="growth_form_or_location" src="d0_s6" value="floating" value_original="floating" />
        <character char_type="range_value" from="3" from_unit="cm" name="diameter" src="d0_s6" to="7.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23087" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="true" name="condition" src="d0_s6" value="closing" value_original="closing" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o23088" name="petal" name_original="petals" src="d0_s6" type="structure" />
      <biological_entity id="o23089" name="whorl" name_original="whorls" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <relation from="o23086" id="r3158" name="opening" negation="false" src="d0_s6" to="o23087" />
      <relation from="o23088" id="r3159" name="in" negation="false" src="d0_s6" to="o23089" />
    </statement>
    <statement id="d0_s7">
      <text>sepals uniformly green, obscurely veined, lines of insertion on receptacle not prominent;</text>
      <biological_entity id="o23090" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s7" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o23091" name="line" name_original="lines" src="d0_s7" type="structure" />
      <biological_entity id="o23092" name="receptacle" name_original="receptacle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o23091" id="r3160" name="on" negation="false" src="d0_s7" to="o23092" />
    </statement>
    <statement id="d0_s8">
      <text>petals 8-15, white;</text>
      <biological_entity id="o23093" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="15" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 20-40, yellow, connective appendage projecting less than 0.2 mm beyond anther;</text>
      <biological_entity id="o23094" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="40" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="connective" id="o23095" name="appendage" name_original="appendage" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="projecting" value_original="projecting" />
        <character char_type="range_value" constraint="beyond anther" constraintid="o23096" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23096" name="anther" name_original="anther" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>filaments widest above middle, much longer than anthers;</text>
      <biological_entity id="o23097" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character constraint="above middle" constraintid="o23098" is_modifier="false" name="width" src="d0_s10" value="widest" value_original="widest" />
        <character constraint="than anthers" constraintid="o23099" is_modifier="false" name="length_or_size" notes="" src="d0_s10" value="much longer" value_original="much longer" />
      </biological_entity>
      <biological_entity id="o23098" name="middle" name_original="middle" src="d0_s10" type="structure" />
      <biological_entity id="o23099" name="anther" name_original="anthers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pistil 5-12-locular, appendages at margin of stigmatic disk tapered or slightly boatshaped, 0.6-1.5 × 0.8-1.4 mm.</text>
      <biological_entity id="o23100" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="5-12-locular" value_original="5-12-locular" />
      </biological_entity>
      <biological_entity id="o23101" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" notes="" src="d0_s11" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" notes="" src="d0_s11" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23102" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="boat-shaped" value_original="boat-shaped" />
      </biological_entity>
      <biological_entity id="o23103" name="stigmatic" name_original="stigmatic" src="d0_s11" type="structure" />
      <biological_entity id="o23104" name="disk" name_original="disk" src="d0_s11" type="structure" />
      <relation from="o23101" id="r3161" name="at" negation="false" src="d0_s11" to="o23102" />
      <relation from="o23102" id="r3162" name="part_of" negation="false" src="d0_s11" to="o23103" />
      <relation from="o23102" id="r3163" name="part_of" negation="false" src="d0_s11" to="o23104" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds ovoid, ca. 2-3 × 1.5-2 mm, ca. 1.3-1.5 times as long as broad, lacking papillae on surface.</text>
      <biological_entity id="o23105" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="length_or_width" src="d0_s12" value="1.3-1.5 times as long as broad" />
      </biological_entity>
      <biological_entity id="o23106" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o23107" name="surface" name_original="surface" src="d0_s12" type="structure" />
      <relation from="o23106" id="r3164" name="on" negation="false" src="d0_s12" to="o23107" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ponds, lakes, and quiet streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="quiet streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Ont., Que., Sask.; Idaho, Maine, Mich., Minn., Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Pygmy water-lily</other_name>
  <other_name type="common_name">small white water-lily</other_name>
  <discussion>Although widely distributed in northern North America, Nymphaea leibergii is apparently not common in much of its range. It is closely related to N. tetragona (see also), with which it has been confused; it differs in a number of floral and foliar characteristics. Although the two species are sympatric over central and western Canada, the distinctions are maintained. Coexistent populations are unknown at this time; such populations would provide useful study opportunities and should be sought in the area of overlap, especially in southeastern and central Manitoba and east-central British Columbia where nearby populations exist.</discussion>
  <discussion>A presumed natural hybrid between Nymphaea leibergii and N. odorata subsp. odorata has recently been found by Hellquist in northern Maine. It is intermediate in morphology between the two parental species and appears to be sterile.</discussion>
  
</bio:treatment>