<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">aquilegia</taxon_name>
    <taxon_name authority="E. James" date="1823" rank="species">coerulea</taxon_name>
    <place_of_publication>
      <publication_title>Account Exped. Pittsburgh</publication_title>
      <place_in_publication>2: 15. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus aquilegia;species coerulea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500096</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 15-80 cm.</text>
      <biological_entity id="o12387" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves (1-) 2 (-3) ×-ternately compound, 9-37 cm, much shorter than stems;</text>
      <biological_entity constraint="basal" id="o12388" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="(1-)2(-3)×-ternately" name="architecture" src="d0_s1" value="compound" value_original="compound" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s1" to="37" to_unit="cm" />
        <character constraint="than stems" constraintid="o12389" is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o12389" name="stem" name_original="stems" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>leaflets green adaxially, to 13-42 (-61) mm, not viscid;</text>
      <biological_entity id="o12390" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="42" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="61" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s2" to="42" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s2" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>primary petiolules (10-) 20-70 mm (leaflets not crowded), glabrous or occasionally pilose.</text>
      <biological_entity constraint="primary" id="o12391" name="petiolule" name_original="petiolules" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s3" to="70" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers erect;</text>
      <biological_entity id="o12392" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals perpendicular to floral axis, white, blue, or sometimes pink, elliptic-ovate to lanceovate, 26-51 × 8-23 mm, apex obtuse to acute or acuminate;</text>
      <biological_entity id="o12393" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="perpendicular" value_original="perpendicular" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="pink" value_original="pink" />
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s5" to="lanceovate" />
        <character char_type="range_value" from="26" from_unit="mm" name="length" src="d0_s5" to="51" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="floral" id="o12394" name="axis" name_original="axis" src="d0_s5" type="structure" />
      <biological_entity id="o12395" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute or acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals: spurs white, blue, or sometimes pink, straight, ± parallel or divergent, 28-72 mm, slender, evenly tapered from base, blades white, oblong or spatulate, 13-28 × 5-14 mm;</text>
      <biological_entity id="o12396" name="petal" name_original="petals" src="d0_s6" type="structure" />
      <biological_entity id="o12397" name="spur" name_original="spurs" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s6" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="28" from_unit="mm" name="some_measurement" src="d0_s6" to="72" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character constraint="from base" constraintid="o12398" is_modifier="false" modifier="evenly" name="shape" src="d0_s6" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o12398" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o12399" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s6" to="28" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 13-24 mm.</text>
      <biological_entity id="o12400" name="petal" name_original="petals" src="d0_s7" type="structure" />
      <biological_entity id="o12401" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s7" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Follicles 20-30 mm;</text>
      <biological_entity id="o12402" name="follicle" name_original="follicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak 8-12 mm.</text>
      <biological_entity id="o12403" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Idaho, Mont., N.Mex., Nev., Utah, Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <discussion>Aquilegia coerulea shows considerable geographic variation in flower color and in size of different floral organs, reflecting adaptation to different pollinators in different parts of its range (R. B. Miller 1981). Four weakly differentiated varieties are recognized.</discussion>
  <discussion>Aquilegia coerulea var. coerulea and A. coerulea var. ochroleuca intergrade to some extent; northwestern populations of var. coerulea often contain individuals with pale flowers, and eastern populations of var. ochroleuca often contain blue-flowered plants.</discussion>
  <discussion>The Gosivte tribe chewed the seeds of Aquilegia coerulea or used an infusion made from the roots to treat abdominal pains or as a panacea (D. E. Moerman 1986).</discussion>
  <discussion>Most authors have spelled the epithet "caerulea"; "coerulea" is the original spelling.</discussion>
  <discussion>Columbine (as Aquilegia caerulea) is the state flower of Colorado.</discussion>
  <references>
    <reference>Miller, R. B. 1981. Hawkmoths and the geographic patterns of floral variation in Aquilegia caerulea. Evolution 35: 763-774.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petal blades 13–17 mm.</description>
      <determination>7c Aquilegia coerulea var. alpina</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petal blades 19–28 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals medium to deep blue.</description>
      <determination>7a Aquilegia coerulea var. coerulea</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals white, pale blue, or pink.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Spurs 36–54 mm (means of populations 40–48 mm); stamens 13–18 mm; Utah to Nevada, Montana.</description>
      <determination>7b Aquilegia coerulea var. ochroleuca</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Spurs 45–72 mm (means of populations 50–58 mm); stamens 18–24 mm; Utah, Arizona.</description>
      <determination>7d Aquilegia coerulea var. pinetorum</determination>
    </key_statement>
  </key>
</bio:treatment>