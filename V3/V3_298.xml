<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(N. I. Malyutin) M. J. Warnock" date="1995" rank="subsection">grumosa</taxon_name>
    <taxon_name authority="Bentham" date="1849" rank="species">patens</taxon_name>
    <taxon_name authority="(Munz) Ewan" date="1942" rank="subspecies">montanum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>69: 147. 1942</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection grumosa;species patens;subspecies montanum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500541</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">parryi</taxon_name>
    <taxon_name authority="Munz" date="unknown" rank="variety">montanum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. S. California Acad. Sci.</publication_title>
      <place_in_publication>31: 61. 1932</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species parryi;variety montanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: basal leaves usually absent at anthesis;</text>
      <biological_entity id="o5414" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o5415" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>basal and proximal cauline leaves usually cleft more than 4/5 radius of blade.</text>
      <biological_entity id="o5416" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal and proximal cauline" id="o5417" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s1" value="cleft" value_original="cleft" />
        <character name="quantity" src="d0_s1" value="4" value_original="4" />
        <character constraint="of blade" constraintid="o5418" name="quantity" src="d0_s1" value="/5" value_original="/5" />
      </biological_entity>
      <biological_entity id="o5418" name="blade" name_original="blade" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade: ultimate lobes 5-10, width less than 10 mm.</text>
      <biological_entity id="o5419" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure" />
      <biological_entity constraint="ultimate" id="o5420" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="10" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: pedicel generally glandular-puberulent.</text>
      <biological_entity id="o5421" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o5422" name="pedicel" name_original="pedicel" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="generally" name="pubescence" src="d0_s3" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: lateral sepals 7-11 mm, spurs 8-14 mm;</text>
      <biological_entity id="o5423" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="lateral" id="o5424" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5425" name="spur" name_original="spurs" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lower petal blades 3-6 mm;</text>
      <biological_entity id="o5426" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="petal" id="o5427" name="blade" name_original="blades" src="d0_s5" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>hairs symmetrically distributed.</text>
      <biological_entity id="o5428" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o5429" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="symmetrically" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open coniferous forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous forest" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>60c.</number>
  <discussion>Delphinium patens subsp. montanum is usually found on drier, leeward sides of mountain ranges; subsp. hepaticoideum is found on wetter, windward sides of many of the same mountain ranges.</discussion>
  <discussion>Delphinium patens subsp. montanum is likely to be confused with D. gracilentum or D. nuttallianum; see discussion under those species for distinguishing features.</discussion>
  <discussion>Hybrids with D. umbraculorum are known.</discussion>
  
</bio:treatment>