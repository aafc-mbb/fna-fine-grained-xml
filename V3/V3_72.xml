<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(N. I. Malyutin) M. J. Warnock" date="1995" rank="subsection">grumosa</taxon_name>
    <taxon_name authority="Bentham" date="1849" rank="species">patens</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>296. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection grumosa;species patens;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500539</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (10-) 20-50 (-90) cm;</text>
      <biological_entity id="o2487" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base reddish, glabrous to puberulent.</text>
      <biological_entity id="o2488" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s1" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly on proximal 1/3 of stem;</text>
      <biological_entity id="o2489" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o2490" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <biological_entity id="o2491" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o2489" id="r306" name="on" negation="false" src="d0_s2" to="o2490" />
      <relation from="o2490" id="r307" name="part_of" negation="false" src="d0_s2" to="o2491" />
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 1-3 at anthesis;</text>
      <biological_entity constraint="basal" id="o2492" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 2-4 at anthesis;</text>
      <biological_entity constraint="cauline" id="o2493" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1.5-12 cm.</text>
      <biological_entity id="o2494" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round to pentagonal, 1-5.5 × 2-7.5 cm, nearly glabrous;</text>
      <biological_entity id="o2495" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s6" to="pentagonal" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="7.5" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-9, less often wedge-shaped, 5 or more extending more than 3/5 distance to petiole, width 5-30 (-50) mm (basal), 1-15 mm (cauline), widest at middle or in proximal 1/2.</text>
      <biological_entity constraint="ultimate" id="o2496" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="9" />
        <character is_modifier="false" modifier="less often" name="shape" src="d0_s7" value="wedge--shaped" value_original="wedge--shaped" />
        <character name="quantity" src="d0_s7" unit="or moreextending" value="5" value_original="5" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character constraint="to petiole" constraintid="o2497" name="quantity" src="d0_s7" value="/5" value_original="/5" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s7" to="50" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character constraint="at " constraintid="o2499" is_modifier="false" name="width" src="d0_s7" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o2497" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2498" name="middle" name_original="middle" src="d0_s7" type="structure" />
      <biological_entity constraint="proximal" id="o2499" name="1/2" name_original="1/2" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 4-25 (-36) -flowered;</text>
      <biological_entity id="o2500" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="4-25(-36)-flowered" value_original="4-25(-36)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel spreading from rachis at usually less than 70°, 1-4 (-8) cm, glabrous to glandular;</text>
      <biological_entity id="o2501" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character constraint="from rachis" constraintid="o2502" is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" modifier="usually" name="atypical_some_measurement" notes="" src="d0_s9" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="usually" name="some_measurement" notes="" src="d0_s9" to="4" to_unit="cm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s9" to="glandular" />
      </biological_entity>
      <biological_entity id="o2502" name="rachis" name_original="rachis" src="d0_s9" type="structure">
        <character name="degree" src="d0_s9" value="0-70°" value_original="0-70°" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 3-12 (-23) mm from flowers, blue to green, linear, 3-8 (-16) mm, glabrous to glandular.</text>
      <biological_entity id="o2503" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="23" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o2504" from="3" from_unit="mm" name="location" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="blue" name="coloration" notes="" src="d0_s10" to="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s10" to="glandular" />
      </biological_entity>
      <biological_entity id="o2504" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals dark blue, usually retaining color upon drying, glabrous, lateral sepals reflexed, (6-) 10-15 (-20) × 4-8 mm, spurs straight, ascending ca. 30° above horizontal, 8-18 mm;</text>
      <biological_entity id="o2505" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2506" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark blue" value_original="dark blue" />
        <character constraint="upon lateral sepals" constraintid="o2507" is_modifier="false" modifier="usually" name="character" src="d0_s11" value="color" value_original="color" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o2507" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="true" name="condition" src="d0_s11" value="drying" value_original="drying" />
        <character is_modifier="true" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" notes="alterIDs:o2507" src="d0_s11" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" notes="alterIDs:o2507" src="d0_s11" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" notes="alterIDs:o2507" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" notes="alterIDs:o2507" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2508" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character constraint="above horizontal" is_modifier="false" modifier="30°" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades elevated, exposing stamens, 3-6 (-8) mm, clefts 1-3 mm;</text>
      <biological_entity id="o2509" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o2510" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2511" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o2512" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o2510" id="r308" name="exposing" negation="false" src="d0_s12" to="o2511" />
    </statement>
    <statement id="d0_s13">
      <text>hairs centered on base of cleft or on inner lobes, scattered, white, rarely yellow.</text>
      <biological_entity id="o2513" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o2514" name="hair" name_original="hairs" src="d0_s13" type="structure" />
      <biological_entity constraint="inner" id="o2515" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" modifier="rarely" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <relation from="o2514" id="r309" name="centered on base of cleft or on" negation="false" src="d0_s13" to="o2515" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 12-23 mm, 3.3-3.6 times longer than wide, glabrous or puberulent.</text>
      <biological_entity id="o2516" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s14" to="23" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="3.3-3.6" value_original="3.3-3.6" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds unwinged;</text>
      <biological_entity id="o2517" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="unwinged" value_original="unwinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>seed-coats ± pitted, cell surfaces roughened.</text>
      <biological_entity id="o2518" name="seed-coat" name_original="seed-coats" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s16" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="cell" id="o2519" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief_or_texture" src="d0_s16" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>60.</number>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lobes of proximal cauline leaves usually more than 15 mm wide; basal and proximal cauline leaves rarely cleft more than 4/5 radius of blade.</description>
      <determination>60b Delphinium patens subsp. hepaticoideum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lobes of proximal cauline leaves usually less than 15 mm wide; basal and proximal cauline leaves usually cleft more than 4/5 radius of blade.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pedicels puberulent, usually glandular; most leaf blades with more than 7 ultimate lobes.</description>
      <determination>60c Delphinium patens subsp. montanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pedicels usually glabrous; most leaf blades with fewer than 7 ultimate lobes.</description>
      <determination>60a Delphinium patens subsp. patens</determination>
    </key_statement>
  </key>
</bio:treatment>