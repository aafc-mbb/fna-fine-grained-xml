<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(de Candolle) B. Boivin" date="1944" rank="section">Heterogamia</taxon_name>
    <taxon_name authority="Trelease" date="1886" rank="species">venulosum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Boston Soc. Nat. Hist.</publication_title>
      <place_in_publication>23: 302. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section heterogamia;species venulosum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501277</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 20-50 cm, glabrous, from rhizomes.</text>
      <biological_entity id="o9246" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9247" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o9246" id="r1306" name="from" negation="false" src="d0_s0" to="o9247" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o9248" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 1-3, those proximal to inflorescence petiolate, those subtending panicle branches sessile.</text>
      <biological_entity constraint="cauline" id="o9249" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o9250" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="panicle" id="o9251" name="branch" name_original="branches" src="d0_s2" type="structure" constraint_original="subtending panicle">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade 3-4×-ternately compound;</text>
      <biological_entity id="o9252" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="3-4×-ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets obovate to orbiculate, apically 3-5-lobed, 5-20 mm, lobe margins crenate, surfaces abaxially glabrous or glandular-puberulent.</text>
      <biological_entity id="o9253" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="orbiculate" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s4" value="3-5-lobed" value_original="3-5-lobed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o9254" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o9255" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, panicles, narrow and dense, many flowered.</text>
      <biological_entity id="o9256" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o9257" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals greenish white, lanceolate or broadly ovate to elliptic or obovate, 2-4 mm;</text>
      <biological_entity id="o9258" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o9259" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish white" value_original="greenish white" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s6" to="elliptic or obovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments colored, not white, (1.8-) 3-5.5 mm;</text>
      <biological_entity id="o9260" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o9261" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="colored" value_original="colored" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 2-3.5 mm, blunt to mucronate;</text>
      <biological_entity id="o9262" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9263" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="blunt" name="shape" src="d0_s8" to="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigma commonly yellowish.</text>
      <biological_entity id="o9264" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9265" name="stigma" name_original="stigma" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="commonly" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Achenes 5-17, erect to spreading, not reflexed, nearly sessile;</text>
      <biological_entity id="o9266" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="17" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s10" to="spreading" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stipe 0.1-0.3 mm;</text>
      <biological_entity id="o9267" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>body often distinctly curved, elliptic-oblong, nearly terete to slightly flattened, adaxial surface 3-4 (-6) mm, glabrous to glandular, veins distinct, not anastomosing-reticulate;</text>
      <biological_entity id="o9268" name="body" name_original="body" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="often distinctly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character char_type="range_value" from="nearly terete" name="shape" src="d0_s12" to="slightly flattened" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9269" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s12" to="glandular" />
      </biological_entity>
      <biological_entity id="o9270" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="not" name="architecture_or_coloration_or_relief" src="d0_s12" value="anastomosing-reticulate" value_original="anastomosing-reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>beak 1.5-2.5 (-3) mm.</text>
      <biological_entity id="o9271" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer-mid summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="early summer" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, riparian woods, and coniferous, deciduous, and mixed forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="riparian woods" />
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="deciduous" />
        <character name="habitat" value="mixed forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Que., Sask., Yukon; Colo., Idaho, Minn., Mont., Nebr., Nev., N.Mex., N.Dak., Oreg., S.Dak., Utah, Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">Veiny meadow-rue</other_name>
  <other_name type="common_name">early meadow-rue</other_name>
  <discussion>Thalctrum venulosum is similar to T. confine and T. occidentale. Careful field studies are needed to clarify the relationships among these taxa.</discussion>
  
</bio:treatment>