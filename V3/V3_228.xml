<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick G. Meyer</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">3</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MAGNOLIACEAE</taxon_name>
    <taxon_hierarchy>family MAGNOLIACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10530</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, deciduous or evergreen, aromatic.</text>
      <biological_entity id="o19343" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pith homogeneous or diaphragmed.</text>
      <biological_entity id="o19345" name="pith" name_original="pith" src="d0_s1" type="structure">
        <character is_modifier="false" name="variability" src="d0_s1" value="homogeneous" value_original="homogeneous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="diaphragmed" value_original="diaphragmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, simple, petiolate;</text>
      <biological_entity id="o19346" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules early or tardily deciduous, at first surrounding stem, adnate on adaxial side of petiole (free in Magnolia grandiflora), often ochreate, leaving persistent annular scar around node.</text>
      <biological_entity id="o19347" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character constraint="on adaxial side" constraintid="o19349" is_modifier="false" name="fusion" notes="" src="d0_s3" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o19348" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s3" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19349" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o19350" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity id="o19351" name="scar" name_original="scar" src="d0_s3" type="structure">
        <character is_modifier="true" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="shape" src="d0_s3" value="annular" value_original="annular" />
      </biological_entity>
      <biological_entity id="o19352" name="node" name_original="node" src="d0_s3" type="structure" />
      <relation from="o19349" id="r2604" name="part_of" negation="false" src="d0_s3" to="o19350" />
      <relation from="o19347" id="r2605" modifier="often" name="leaving" negation="false" src="d0_s3" to="o19351" />
      <relation from="o19347" id="r2606" modifier="often" name="around" negation="false" src="d0_s3" to="o19352" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade pinnately veined, unlobed (or evenly 2-10-lobed in Liriodendron), margins entire.</text>
      <biological_entity id="o19353" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o19354" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, solitary flowers (often paired in Magnolia ashei), pedunculate;</text>
      <biological_entity id="o19355" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o19356" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathaceous bracts 2 (Magnolia) or 1 (Liriodendron).</text>
      <biological_entity id="o19357" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="spathaceous" value_original="spathaceous" />
        <character name="quantity" src="d0_s6" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s6" unit="or" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth hypogynous, segments imbricate;</text>
      <biological_entity id="o19358" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19359" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o19360" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals deciduous, 6-18, in 3 or more whorls of 3, ± similar or outer tepals sepaloid, inner tepals petaloid;</text>
      <biological_entity id="o19361" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o19362" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="18" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o19363" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="in 3 or morewhorls" name="architecture" src="d0_s8" value="sepaloid" value_original="sepaloid" />
      </biological_entity>
      <biological_entity constraint="inner" id="o19364" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petaloid" value_original="petaloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens numerous, hypogynous, free, spirally arranged;</text>
      <biological_entity id="o19365" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19366" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s9" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s9" value="arranged" value_original="arranged" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments very short to 1/2 length of anthers;</text>
      <biological_entity id="o19367" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19368" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character char_type="range_value" from="0 length of anthers" name="length" src="d0_s10" to="1/2 length of anthers" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers introrse, latrorse, or extrorse, longitudinally dehiscent;</text>
      <biological_entity id="o19369" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19370" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="introrse" value_original="introrse" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="latrorse" value_original="latrorse" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="extrorse" value_original="extrorse" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="latrorse" value_original="latrorse" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="extrorse" value_original="extrorse" />
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>connective with distal appendage;</text>
      <biological_entity id="o19371" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o19372" name="connective" name_original="connective" src="d0_s12" type="structure" />
      <biological_entity constraint="distal" id="o19373" name="appendage" name_original="appendage" src="d0_s12" type="structure" />
      <relation from="o19372" id="r2607" name="with" negation="false" src="d0_s12" to="o19373" />
    </statement>
    <statement id="d0_s13">
      <text>pistils numerous, superior, spirally arranged on elongate receptacle (torus), stalked or sessile, free or ±concrescent, 1-locular;</text>
      <biological_entity id="o19374" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19375" name="pistil" name_original="pistils" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="position" src="d0_s13" value="superior" value_original="superior" />
        <character constraint="on receptacle" constraintid="o19376" is_modifier="false" modifier="spirally" name="arrangement" src="d0_s13" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="free" value_original="free" />
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s13" value="concrescent" value_original="concrescent" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="1-locular" value_original="1-locular" />
      </biological_entity>
      <biological_entity id="o19376" name="receptacle" name_original="receptacle" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>placentation marginal, placenta 1;</text>
      <biological_entity id="o19377" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s14" value="marginal" value_original="marginal" />
      </biological_entity>
      <biological_entity id="o19378" name="placenta" name_original="placenta" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovules 1-2;</text>
      <biological_entity id="o19379" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o19380" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s15" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style 1, short and recurved (Magnolia) or large and winglike (Liriodendron);</text>
      <biological_entity id="o19381" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o19382" name="style" name_original="style" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="short" value_original="short" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="size" src="d0_s16" value="large" value_original="large" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="winglike" value_original="winglike" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma 1, terminal or terminal decurrent (Magnolia) or recurved (Liriodendron).</text>
      <biological_entity id="o19383" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o19384" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s17" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits conelike syncarps consisting of aggregates of coalescent, woody follicles (follicetums, as in Magnolia) or apocarps consisting of aggregates of indehiscent samaras (samaracetums, as in Liriodendron).</text>
      <biological_entity id="o19385" name="fruit" name_original="fruits" src="d0_s18" type="structure" />
      <biological_entity id="o19386" name="syncarp" name_original="syncarps" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="conelike" value_original="conelike" />
      </biological_entity>
      <biological_entity id="o19387" name="aggregate" name_original="aggregates" src="d0_s18" type="structure" />
      <biological_entity id="o19388" name="follicle" name_original="follicles" src="d0_s18" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s18" value="coalescent" value_original="coalescent" />
        <character is_modifier="true" name="texture" src="d0_s18" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o19389" name="apocarp" name_original="apocarps" src="d0_s18" type="structure" />
      <biological_entity id="o19390" name="aggregate" name_original="aggregates" src="d0_s18" type="structure" />
      <biological_entity id="o19391" name="samara" name_original="samaras" src="d0_s18" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s18" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <relation from="o19386" id="r2608" name="consisting of" negation="false" src="d0_s18" to="o19387" />
      <relation from="o19386" id="r2609" name="part_of" negation="false" src="d0_s18" to="o19388" />
      <relation from="o19386" id="r2610" name="part_of" negation="false" src="d0_s18" to="o19389" />
      <relation from="o19386" id="r2611" name="consisting of" negation="false" src="d0_s18" to="o19390" />
      <relation from="o19386" id="r2612" name="consisting of" negation="false" src="d0_s18" to="o19391" />
    </statement>
    <statement id="d0_s19">
      <text>Seeds 1-2 per pistil, arillate, endosperm oily (Magnolia), or without aril, adherent to dry endocarp (Liriodendron).</text>
      <biological_entity id="o19392" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per pistil" constraintid="o19393" from="1" name="quantity" src="d0_s19" to="2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s19" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity id="o19393" name="pistil" name_original="pistil" src="d0_s19" type="structure" />
      <biological_entity id="o19394" name="endosperm" name_original="endosperm" src="d0_s19" type="structure">
        <character is_modifier="false" name="coating" src="d0_s19" value="oily" value_original="oily" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s19" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o19395" name="aril" name_original="aril" src="d0_s19" type="structure" />
      <biological_entity id="o19396" name="endocarp" name_original="endocarp" src="d0_s19" type="structure">
        <character is_modifier="true" name="condition_or_texture" src="d0_s19" value="dry" value_original="dry" />
      </biological_entity>
      <relation from="o19394" id="r2613" name="without" negation="false" src="d0_s19" to="o19395" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly in Asia, the Pacific Islands, and the Western Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly in Asia" establishment_means="native" />
        <character name="distribution" value="the Pacific Islands" establishment_means="native" />
        <character name="distribution" value="and the Western Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Composite Family</other_name>
  <discussion>Genera ca. 6(-12), species ca. 220 (2 genera, 9 species in the flora).</discussion>
  <discussion>Magnoliaceae are pollinated by beetles.</discussion>
  <discussion>Herbarium material of Magnolia is usually incomplete and inadequate for critical study. Collections should include material of the stipules, spathaceous bracts, a full complement of stamens, and all of the perianth segments to facilitate identification of Magnolia species.</discussion>
  <references>
    <reference>Canright, J. E. 1960. The comparative morphology and relationships of the Magnoliaceae. III. Carpels. Amer. J. Bot. 47(2): 145-155.</reference>
    <reference>Demuth, P. and F. S. Santamour Jr. 1978. Carotenoid flower pigments in Liriodendron and Magnolia. Bull. Torrey Bot. Club 105(1): 65-66.</reference>
    <reference>Hardin, J. W. and K. A. Jones. 1989. Atlas of foliar surface features in woody plants, X. Magnoliaceae of the United States. Bull. Torrey Bot. Club 116(2): 164-173.</reference>
    <reference>Nooteboom, J. P. 1985. Notes on Magnoliaceae. Blumea 31: 65-121.</reference>
    <reference>Praglowski, J. 1974. Magnoliaceae Juss. Taxonomy by J. E. Dandy. World Pollen Spore Fl. 3: 1-48.</reference>
    <reference>Sargent, C. S. 1890-1902. The Silva of North America.... 14 vols. Boston and New York. Vol. 1, pp. 1-20.</reference>
    <reference>Spongberg, S. A. 1976. Magnoliaceae hardy in temperate North America. J. Arnold Arbor. 57: 250-312.</reference>
    <reference>Wood, C. E. Jr. 1958. The genera of the woody Ranales in the southeastern United States. J. Arnold Arbor. 39: 296-346.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade entire, base deeply cordate or auriculate, or cuneate to abruptly narrowed or rounded, apex obtuse or acute to acuminate; stipules adnate on petiole or rarely free, early deciduous; tepals petaloid, usually spreading, creamy white, rarely greenish or yellow to orange-yellow, outermost tepals sepaloid, sometimes reflexed, greenish; anthers introrse or latrorse; follicles persistent, coalescent; seeds with brightly colored aril, extruded from follicles and suspended by funiculi.</description>
      <determination>1 Magnolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade evenly 2-10-lobed, base rounded to shallowly cordate or truncate, apex broadly truncate or notched; stipules free, erect, leafy, tardily deciduous; tepals petaloid, tip recurved, greenish yellow with feathered orange band near base, outermost tepals sepaloid, reflexed, green; anthers extrorse; samaras caducous, forming elongate spindle-shaped dry cone, indehiscent; seeds without aril, adherent to dry endocarp.</description>
      <determination>2 Liriodendron</determination>
    </key_statement>
  </key>
</bio:treatment>