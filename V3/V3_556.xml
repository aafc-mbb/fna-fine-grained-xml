<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="M. J. Warnock" date="1995" rank="subsection">virescens</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">carolinianum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>155. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection virescens;species carolinianum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500485</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (20-) 40-90 (-150) cm;</text>
      <biological_entity id="o8872" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base reddish or not, ± pubescent.</text>
      <biological_entity id="o8873" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character name="coloration" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o8874" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 0-10 at anthesis;</text>
      <biological_entity constraint="basal" id="o8875" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 4-12 at anthesis;</text>
      <biological_entity constraint="cauline" id="o8876" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="4" name="quantity" src="d0_s4" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.1-14 cm.</text>
      <biological_entity id="o8877" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s5" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round to pentagonal, 1-8 × 2-12 cm, pubescence variable;</text>
      <biological_entity id="o8878" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s6" to="pentagonal" />
        <character char_type="range_value" from="1" from_unit="cm" name="pubescence" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="pubescence" src="d0_s6" to="12" to_unit="cm" />
        <character is_modifier="false" name="variability" src="d0_s6" value="variable" value_original="variable" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-29, width 2-10 mm (basal), 1-7 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o8879" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="29" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences (3-) 8-27 (-94) -flowered;</text>
      <biological_entity id="o8880" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="(3-)8-27(-94)-flowered" value_original="(3-)8-27(-94)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel (0.4-) 0.7-1.8 (-5.7) cm, nearly glabrous to glandular;</text>
      <biological_entity id="o8881" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="0.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="5.7" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s9" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="nearly glabrous" name="pubescence" src="d0_s9" to="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 1-3.5 (-6) mm from flowers, green or blue, linear, 2-7 mm, pubescence nearly glabrous to glandular.</text>
      <biological_entity id="o8882" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o8883" from="1" from_unit="mm" name="location" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blue" value_original="blue" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescence" value_original="pubescence" />
        <character char_type="range_value" from="nearly glabrous" name="pubescence" src="d0_s10" to="glandular" />
      </biological_entity>
      <biological_entity id="o8883" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals purple to blue to white, nearly glabrous, lateral sepals spreading, (7-) 9-14 (-17) × (3-) 3.5-6 (-8) mm, spurs ± upcurved, ascending 20-90° above vertical, (9-) 11-17 (-19) mm;</text>
      <biological_entity id="o8884" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8885" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="purple" name="coloration" src="d0_s11" to="blue" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o8886" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s11" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="17" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s11" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s11" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8887" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s11" value="upcurved" value_original="upcurved" />
        <character constraint="above vertical , (9-)11-17(-19) mm" is_modifier="false" modifier="20-90°" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades ± covering stamens, 5-7 mm, cleft 2-4 mm;</text>
      <biological_entity id="o8888" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o8889" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8890" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <relation from="o8889" id="r1248" modifier="more or less" name="covering" negation="false" src="d0_s12" to="o8890" />
    </statement>
    <statement id="d0_s13">
      <text>hairs centered, densest near base of cleft, white, sometimes blue or yellow.</text>
      <biological_entity id="o8891" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8892" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="centered" value_original="centered" />
        <character constraint="near base" constraintid="o8893" is_modifier="false" name="density" src="d0_s13" value="densest" value_original="densest" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o8893" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits (10-) 12.5-18.5 (-27) mm, 4-4.5 times longer than wide, glabrous to puberulent.</text>
      <biological_entity id="o8894" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="12.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="27" to_unit="mm" />
        <character char_type="range_value" from="12.5" from_unit="mm" name="some_measurement" src="d0_s14" to="18.5" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="4-4.5" value_original="4-4.5" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s14" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds: seed-coat cells with surfaces pustulate or smooth.</text>
      <biological_entity id="o8895" name="seed" name_original="seeds" src="d0_s15" type="structure" />
      <biological_entity constraint="seed-coat" id="o8896" name="cell" name_original="cells" src="d0_s15" type="structure" />
      <biological_entity id="o8897" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief" src="d0_s15" value="pustulate" value_original="pustulate" />
        <character is_modifier="false" name="relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o8896" id="r1249" name="with" negation="false" src="d0_s15" to="o8897" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man.; Ala., Ark., Colo., Fla., Ga., Ill., Iowa, Kans., Ky., La., Minn., Miss., Mo., N.C., N.Dak., Nebr., Okla., S.C., S.Dak., Tenn., Tex., Wis.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>36.</number>
  <discussion>Subspecies 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaves absent at anthesis, cauline leaves divided into many narrow (less than 2 mm wide) segments, blade not distinctly 3-parted; distalmost petiole less than 5 mm; sepals usually blue or purple (rarely white).</description>
      <determination>36a Delphinium carolinianum subsp. carolinianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaves usually present at anthesis, and/or cauline leaf lobes usually wider than 2 mm, blade often distinctly 3-parted or more; distalmost petiole more than 5 mm; sepals blue or white.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade distinctly 3-parted with few additional divisions; sepals blue to white; roots usually ± vertical, often without major branches.</description>
      <determination>36c Delphinium carolinianum subsp. vimineum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade with 3–5 or more major divisions, each further divided into segments; sepals white to very pale blue; roots ±horizontal with several major branches.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems usually less than 45 cm; in thin soils over limestone in clearings of deciduous woods; leaf blade with 3 major divisions; e of Mississippi River.</description>
      <determination>36b Delphinium carolinianum subsp. calciphilum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems usually more than 45 cm; in deeper soils in grasslands; leaf blade with 5 or more major divisions; w of Mississippi River.</description>
      <determination>36d Delphinium carolinianum subsp. virescens</determination>
    </key_statement>
  </key>
</bio:treatment>