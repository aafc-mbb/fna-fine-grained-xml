<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="(Prantl) L. D. Benson" date="1936" rank="section">epirotes</taxon_name>
    <taxon_name authority="Schlechtendal" date="1820" rank="species">eschscholtzii</taxon_name>
    <taxon_name authority="(Greene) L. D. Benson" date="1941" rank="variety">eximius</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>68: 654. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section epirotes;species eschscholtzii;variety eximius;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501135</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">eximius</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>3: 19. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ranunculus;species eximius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: caudex 1.5-3.5 cm, with few or no persistent leaf-bases.</text>
      <biological_entity id="o7096" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o7097" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s0" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7098" name="leaf-base" name_original="leaf-bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="few" value_original="few" />
        <character is_modifier="true" name="quantity" src="d0_s0" value="no" value_original="no" />
        <character is_modifier="true" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o7097" id="r985" name="with" negation="false" src="d0_s0" to="o7098" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaf-blades obovate to broadly oblong, distally 5-9-lobed or sometimes 3-parted with lateral segments again lobed, 1.4-4.1 × 1.4-3.3 cm, base obtuse or rounded, middle segment unlobed, ultimate segments and sinuses acute or acuminate.</text>
      <biological_entity constraint="basal" id="o7099" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s1" to="broadly oblong" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s1" value="5-9-lobed" value_original="5-9-lobed" />
        <character constraint="with lateral segments" constraintid="o7100" is_modifier="false" modifier="sometimes; sometimes" name="shape" src="d0_s1" value="3-parted" value_original="3-parted" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="length" notes="" src="d0_s1" to="4.1" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="width" notes="" src="d0_s1" to="3.3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o7100" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="again" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o7101" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="middle" id="o7102" name="segment" name_original="segment" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o7103" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s1" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o7104" name="sinuse" name_original="sinuses" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s1" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: petals 9-16 mm.</text>
      <biological_entity id="o7105" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o7106" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s2" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Idaho, Mont., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31c.</number>
  
</bio:treatment>