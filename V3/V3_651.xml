<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Endlicher" date="unknown" rank="family">cannabaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">cannabis</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">sativa</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1027. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cannabaceae;genus cannabis;species sativa</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200006342</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Staminate plants usually taller, less robust than pistillate plants.</text>
      <biological_entity id="o3405" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="usually" name="height" src="d0_s0" value="taller" value_original="taller" />
        <character constraint="than pistillate plants" constraintid="o3406" is_modifier="false" name="fragility" src="d0_s0" value="less robust" value_original="less robust" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3406" name="plant" name_original="plants" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.2-6 m.</text>
      <biological_entity id="o3407" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s1" to="6" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 2-7 cm.</text>
      <biological_entity id="o3408" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3409" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaflet blades mostly 3-9, linear to linear-lanceolate, 3-15 × 0.2-1.7 cm, margins coarsely serrate;</text>
      <biological_entity constraint="leaflet" id="o3410" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="9" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3411" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially whitish green with scattered, yellowish-brown, resinous dots, strigose, adaxially darker green with large, stiff, bulbous-based conic hairs.</text>
      <biological_entity id="o3412" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character constraint="with hairs" constraintid="o3413" is_modifier="false" name="coloration" src="d0_s4" value="whitish green" value_original="whitish green" />
      </biological_entity>
      <biological_entity id="o3413" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="true" name="coating" src="d0_s4" value="resinous" value_original="resinous" />
        <character is_modifier="true" name="coloration_or_relief" src="d0_s4" value="dots" value_original="dots" />
        <character is_modifier="true" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="true" modifier="adaxially" name="coloration" src="d0_s4" value="darker green" value_original="darker green" />
        <character is_modifier="true" name="size" src="d0_s4" value="large" value_original="large" />
        <character is_modifier="true" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="bulbous-based" value_original="bulbous-based" />
        <character is_modifier="true" name="shape" src="d0_s4" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences numerous.</text>
      <biological_entity id="o3414" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers unisexual, often transitional flowers and flowers of opposite sex developing later.</text>
      <biological_entity id="o3415" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o3417" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="often" name="architecture" src="d0_s6" value="transitional" value_original="transitional" />
        <character constraint="of opposite sex" is_modifier="false" name="development" src="d0_s6" value="developing" value_original="developing" />
        <character is_modifier="false" name="condition" src="d0_s6" value="later" value_original="later" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: pedicels 0.5-3 mm;</text>
      <biological_entity id="o3418" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3419" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals ovate to lanceolate, 2.5-4 mm, puberulent;</text>
      <biological_entity id="o3420" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3421" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens caducous after anthesis, somewhat shorter than sepals;</text>
      <biological_entity id="o3422" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3423" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character constraint="after sepals" constraintid="o3425" is_modifier="false" name="duration" src="d0_s9" value="caducous" value_original="caducous" />
      </biological_entity>
      <biological_entity id="o3425" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" modifier="somewhat" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o3424" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" modifier="somewhat" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 0.5-1 mm.</text>
      <biological_entity id="o3426" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3427" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers ± sessile, enclosed by glandular, beaked bracteole and subtended by bract;</text>
      <biological_entity id="o3428" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o3429" name="bracteole" name_original="bracteole" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture_or_function_or_pubescence" src="d0_s11" value="glandular" value_original="glandular" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s11" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o3430" name="bract" name_original="bract" src="d0_s11" type="structure" />
      <relation from="o3428" id="r455" name="enclosed by" negation="false" src="d0_s11" to="o3429" />
      <relation from="o3428" id="r456" name="subtended by" negation="false" src="d0_s11" to="o3430" />
    </statement>
    <statement id="d0_s12">
      <text>perianth appressed to and surrounding base of ovary.</text>
      <biological_entity id="o3431" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o3432" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s12" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity id="o3433" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <relation from="o3432" id="r457" name="part_of" negation="false" src="d0_s12" to="o3433" />
    </statement>
    <statement id="d0_s13">
      <text>Achenes white or greenish, mottled with purple, ovoid, somewhat compressed, 2-5 mm, with ± persistent perianth that sometimes flakes off.</text>
      <biological_entity id="o3435" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="more or less" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o3436" name="flake" name_original="flakes" src="d0_s13" type="structure" />
      <relation from="o3434" id="r458" name="with" negation="false" src="d0_s13" to="o3435" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 20.</text>
      <biological_entity id="o3434" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="mottled with purple" value_original="mottled with purple" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3437" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer–fall; staminate plants generally dying after anthesis, pistillate plants remaining dark green, persisting until frost.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Well-manured, moist farmyards, and in open habitats, waste places (roadsides, railways, vacant lots), occasionally in fallow fields and open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist farmyards" modifier="well-manured" />
        <character name="habitat" value="open habitats" modifier="and in" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railways" />
        <character name="habitat" value="vacant lots" />
        <character name="habitat" value="fallow fields" modifier=") occasionally in" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; principal naturalized range (see map) Ont., Que.; Ark., Conn., Del., Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Nebr., N.H., N.J., N.Y., N.Dak., Ohio, Okla., Pa., R.I., S.Dak., Vt., Va., W.Va., Wis.; native to Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="principal naturalized range (see map) Ont" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="native to Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Hemp</other_name>
  <other_name type="common_name">marihuana (marijuana)</other_name>
  <other_name type="common_name">pot</other_name>
  <other_name type="common_name">grass</other_name>
  <other_name type="common_name">maryjane; chanvre</other_name>
  <other_name type="common_name">cannabis</other_name>
  <discussion>Cannabis sativa has been reported as cultivated illegally and as apparently ruderal in all provinces and states except Alaska. It has been collected least frequently in Mississippi and Idaho. It seems to be best established in the prairies and plains of central North America.</discussion>
  <discussion>Hemp is a short-day plant; flowering depends upon the latitude of origin. Races originating closer to the equator (and generally higher in psychointoxicant) require a longer induction period for flowering than races originating farther north.</discussion>
  <discussion>The taxonomy of Cannabis sativa, a polymorphic species, has been debated in scientific and legal forums. The name C. sativa subsp. indica (Lamarck) E. Small &amp; Cronquist has been applied to plants with a mean leaf content of the psychotomimetic (hallucinatory) delta-9-tetrahydrocannabinol of at least 0.3%; those with a lesser content fall under C. sativa subsp. sativa. When separate species are recognized, the name C. indica Lamarck has generally been applied to variants with high levels of the intoxicant chemical, whereas the name C. sativa Linnaeus, interpreted in a restricted sense, has generally been applied to plants selected for their yield of bast fibers in the stems. (The latter generally have taller, hollow stems with longer internodes and less branching than races selected for drug content.)</discussion>
  <discussion>Superimposed on this dimension of variation is selection for nonabscising achenes in cultivation and abscising achenes in the wild (i.e., outside of cultivation). This is analagous to selection of nonshattering cereals from wild, shattering grasses. Achenes selected for cultivation tend to be longer than 3.8 mm and lack a basal constricted zone; by contrast, achenes selected for wild existence tend to be shorter than 3.8 mm and to have a basal constricted zone that seems to facilitate disarticulation and a mottled, persistent perianth apparently serving as camouflage.</discussion>
  <discussion>Within Cannabis sativa subsp. sativa, the wild phase has been named C. sativa var. spontanea Vavilov (= C. ruderalis Janishevsky), in contrast to the domesticated C. sativa var. sativa. Within C. sativa subsp. indica, the wild phase (not to be expected in North America) has been designated C. sativa var. kafiristanica (Vavilov) E. Small &amp; Cronquist, as distinct from the domesticated C. sativa var. indica. The chemical and morphologic distinctions by which Cannabis has been split into taxa are often not readily discernible, appear to be environmentally modifiable, and vary in a continuous fashion. For most purposes it will suffice to apply the name Cannabis sativa to all plants encountered in North America.*</discussion>
  <discussion>The Iroquois used Cannabis sativa medicinally to convince patients that they had recovered. They also found it useful as a stimulant (D. E. Moerman 1986).</discussion>
  
</bio:treatment>