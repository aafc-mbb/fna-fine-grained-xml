<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus f." date="1782" rank="species">pensylvanicus</taxon_name>
    <place_of_publication>
      <publication_title>Suppl. Pl.,</publication_title>
      <place_in_publication>272. 1782</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species pensylvanicus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501196</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, never rooting nodally, hispid, base not bulbous.</text>
      <biological_entity id="o12916" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="never; nodally" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o12917" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots never tuberous.</text>
      <biological_entity id="o12918" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaf-blades broadly cordate in outline, 3-foliolate, 1.6-7 × 3-9 cm, leaflets cleft, usually deeply so, ultimate segments narrowly elliptic, margins toothed, apex acute.</text>
      <biological_entity constraint="basal" id="o12919" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character constraint="in outline" is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="length" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12920" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o12921" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually deeply; deeply; narrowly" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o12922" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o12923" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: receptacle hirsute;</text>
      <biological_entity id="o12924" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o12925" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals reflexed ca. 1 mm above base, 3-5 × 1.5-2 mm, ± hispid;</text>
      <biological_entity id="o12926" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o12927" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character constraint="above base" constraintid="o12928" name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" notes="" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o12928" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals 5, yellow, 2-4 × 1-2.5 mm.</text>
      <biological_entity id="o12929" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o12930" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes cylindric, 9-12 × 5-7 mm;</text>
      <biological_entity id="o12931" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12932" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o12931" id="r1799" name="part_of" negation="false" src="d0_s6" to="o12932" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 1.8-2.8 × 1.6-2 mm, glabrous, margin forming narrow rib 0.1-0.2 mm wide;</text>
      <biological_entity id="o12933" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s7" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12934" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12935" name="rib" name_original="rib" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o12934" id="r1800" name="forming" negation="false" src="d0_s7" to="o12935" />
    </statement>
    <statement id="d0_s8">
      <text>beak persistent, broadly lanceolate or nearly deltate, straight or nearly so, 0.6-0.8 mm. 2n = 16.</text>
      <biological_entity id="o12936" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character name="course" src="d0_s8" value="nearly" value_original="nearly" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12937" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, bogs, moist clearings, depressions in woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="moist clearings" />
        <character name="habitat" value="depressions" constraint="in woodlands" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask.; Alaska, Ariz., Colo., Conn., Del., D.C., Idaho, Ill., Ind., Iowa, Maine, Md., Mass., Mich., Minn., Mont., Nebr., N.H., N.J., N.Mex., N.Y., N.Dak., Ohio, Pa., R.I., S.Dak., Vt., Wash., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Renoncule de Pennsylvanie</other_name>
  <discussion>Ojibwa tribes used Ranunculus pensylvanicus as a hunting medicine; the Potawatomi used it as an astringent for miscellaneous diseases (D. E. Moerman 1986).</discussion>
  
</bio:treatment>