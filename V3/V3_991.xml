<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="M. J. Warnock" date="1995" rank="subsection">Multiplex</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>78: 81. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection multiplex;</taxon_hierarchy>
    <other_info_on_name type="fna_id">302023</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots 2-6-branched, (5-) 10-40 (-50) cm, twisted fibrous, dry to fleshy, thin threadlike segments apparent nearly entire length;</text>
      <biological_entity id="o21829" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="2-6-branched" value_original="2-6-branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s0" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
        <character char_type="range_value" from="dry" name="texture" src="d0_s0" to="fleshy" />
      </biological_entity>
      <biological_entity id="o21830" name="segment" name_original="segments" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" value_original="thin" />
        <character is_modifier="true" name="shape" src="d0_s0" value="thread-like" value_original="threadlike" />
        <character is_modifier="false" name="prominence" src="d0_s0" value="apparent" value_original="apparent" />
        <character is_modifier="false" modifier="nearly" name="length" src="d0_s0" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>buds more than 3 mm, sometimes present during dormant season.</text>
      <biological_entity id="o21831" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" upper_restricted="false" />
        <character constraint="during season" constraintid="o21832" is_modifier="false" modifier="sometimes" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21832" name="season" name_original="season" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="dormant" value_original="dormant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1-4 (-8) per root, usually unbranched, rarely less than 60 cm, elongation delayed 1-3 weeks after leaf initiation;</text>
      <biological_entity id="o21833" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="8" />
        <character char_type="range_value" constraint="per root" constraintid="o21834" from="1" name="quantity" src="d0_s2" to="4" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="rarely" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21834" name="root" name_original="root" src="d0_s2" type="structure" />
      <biological_entity id="o21835" name="elongation" name_original="elongation" src="d0_s2" type="structure" />
      <biological_entity id="o21836" name="week" name_original="weeks" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <biological_entity id="o21837" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <relation from="o21835" id="r2984" name="delayed" negation="false" src="d0_s2" to="o21836" />
      <relation from="o21835" id="r2985" name="after" negation="false" src="d0_s2" to="o21837" />
    </statement>
    <statement id="d0_s3">
      <text>base usually not narrowed, firmly attached to root;</text>
      <biological_entity id="o21838" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character constraint="to root" constraintid="o21839" is_modifier="false" modifier="firmly" name="fixation" src="d0_s3" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o21839" name="root" name_original="root" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal internodes much shorter than those of midstem.</text>
      <biological_entity constraint="proximal" id="o21840" name="internode" name_original="internodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline, largest near base of stem, others usually abruptly smaller on distal portion of stem;</text>
      <biological_entity id="o21841" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character constraint="near base" constraintid="o21842" is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o21842" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o21843" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <biological_entity id="o21844" name="other" name_original="others" src="d0_s5" type="structure">
        <character constraint="on distal portion" constraintid="o21845" is_modifier="false" modifier="usually abruptly" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21845" name="portion" name_original="portion" src="d0_s5" type="structure" />
      <biological_entity id="o21846" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o21842" id="r2986" name="part_of" negation="false" src="d0_s5" to="o21843" />
      <relation from="o21845" id="r2987" name="part_of" negation="false" src="d0_s5" to="o21846" />
    </statement>
    <statement id="d0_s6">
      <text>basal petioles ± spreading, cauline petioles ascending;</text>
      <biological_entity constraint="basal" id="o21847" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o21848" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal leaves more rounded and with fewer, wider lobes than cauline leaves.</text>
      <biological_entity constraint="basal" id="o21849" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="wider" id="o21851" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="fewer" value_original="fewer" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o21850" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o21849" id="r2988" name="with" negation="false" src="d0_s7" to="o21851" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences usually 3-12 flowers per 5 cm, dense to open, cylindric, spurs sometimes intersecting rachis;</text>
      <biological_entity id="o21852" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
      <biological_entity id="o21853" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="per 5 cm" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o21854" name="spur" name_original="spurs" src="d0_s8" type="structure" />
      <biological_entity id="o21855" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <relation from="o21854" id="r2989" modifier="sometimes" name="intersecting" negation="false" src="d0_s8" to="o21855" />
    </statement>
    <statement id="d0_s9">
      <text>pedicel ascending, usually less than 2 cm;</text>
    </statement>
    <statement id="d0_s10">
      <text>if stems less than 60 cm, then pedicel remotely ascending (sigmoid);</text>
      <biological_entity id="o21856" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21857" name="stem" name_original="stems" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s10" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21858" name="pedicel" name_original="pedicel" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="remotely" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracts usually markedly smaller and fewer lobed than leaves.</text>
      <biological_entity id="o21859" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually markedly" name="size" src="d0_s11" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="quantity" src="d0_s11" value="fewer" value_original="fewer" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o21860" name="leaf" name_original="leaves" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits erect.</text>
      <biological_entity id="o21861" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds rectangular to crescent-shaped, 1.8-3 × 1.3-2.5 mm, not ringed at proximal end, wing-margined or not;</text>
      <biological_entity id="o21862" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s13" to="crescent-shaped" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s13" to="2.5" to_unit="mm" />
        <character constraint="at proximal end" constraintid="o21863" is_modifier="false" modifier="not" name="relief" src="d0_s13" value="ringed" value_original="ringed" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="wing-margined" value_original="wing-margined" />
        <character name="architecture" src="d0_s13" value="not" value_original="not" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21863" name="end" name_original="end" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>seed-coats with small irregular wavy ridges or ripples, cells elongate (more than 3 times longer than wide), cell margins straight.</text>
      <biological_entity id="o21864" name="seed-coat" name_original="seed-coats" src="d0_s14" type="structure" />
      <biological_entity id="o21865" name="ridge" name_original="ridges" src="d0_s14" type="structure">
        <character is_modifier="true" name="size" src="d0_s14" value="small" value_original="small" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s14" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="shape" src="d0_s14" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o21866" name="ripple" name_original="ripples" src="d0_s14" type="structure" />
      <biological_entity id="o21867" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="cell" id="o21868" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o21864" id="r2990" name="with" negation="false" src="d0_s14" to="o21865" />
      <relation from="o21864" id="r2991" name="with" negation="false" src="d0_s14" to="o21866" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16b.3.</number>
  <discussion>Species 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals yellowish green.</description>
      <determination>22 Delphinium viridescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals bluish or purplish (sometimes white, or lavender), not yellow or yellowish.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves usually absent from proximal 1/5 of stem at anthesis.</description>
      <determination>21 Delphinium multiplex</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves present on proximal 1/5 of stem at anthesis.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Bracteoles less than 4mm; spurs 9–12 mm; pedicel 0.3–1.5(–2.5) cm; sepals white to light blue.</description>
      <determination>20 Delphinium inopinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Bracteoles more than 4mm; spurs 11–22 mm; pedicel 1–4(–15) cm; sepals bluish purple.</description>
      <determination>19 Delphinium polycladon</determination>
    </key_statement>
  </key>
</bio:treatment>