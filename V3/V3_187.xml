<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="M. J. Warnock" date="1995" rank="subsection">depauperata</taxon_name>
    <taxon_name authority="Geyer ex Hooker" date="1847" rank="species">distichum</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot.</publication_title>
      <place_in_publication>6: 68. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection depauperata;species distichum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500494</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">strictum</taxon_name>
    <taxon_name authority="(Hooker) H. St. John" date="unknown" rank="variety">distichiflorum</taxon_name>
    <taxon_hierarchy>genus Delphinium;species strictum;variety distichiflorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (25-) 30-60 (-80) cm;</text>
      <biological_entity id="o4903" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base sometimes reddish, puberulent.</text>
      <biological_entity id="o4904" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o4905" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves usually absent at anthesis;</text>
      <biological_entity constraint="basal" id="o4906" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 9-18 (-28) at anthesis;</text>
      <biological_entity constraint="cauline" id="o4907" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="28" />
        <character char_type="range_value" constraint="at anthesis" from="9" name="quantity" src="d0_s4" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0-9 cm.</text>
      <biological_entity id="o4908" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade cuneate to semicircular, 1-5 × 1.5-7 cm, puberulent;</text>
      <biological_entity id="o4909" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s6" to="semicircular" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="7" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 5-19, width 2-8 (-15) mm (basal), 0.5-3 (-5) mm (cauline);</text>
      <biological_entity constraint="ultimate" id="o4910" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="19" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins of basal leaf, measured less than 1 cm from blade base, demarcating considerably more than 90° of arc when leaf laid flat;</text>
      <biological_entity constraint="basal" id="o4912" name="leaf" name_original="leaf" src="d0_s8" type="structure" />
      <biological_entity id="o4913" name="arc" name_original="arc" src="d0_s8" type="structure">
        <character char_type="range_value" from="90" is_modifier="true" modifier="considerably" name="quantity" src="d0_s8" upper_restricted="false" />
      </biological_entity>
      <relation from="o4911" id="r695" name="part_of" negation="false" src="d0_s8" to="o4912" />
      <relation from="o4911" id="r696" modifier="from blade base" name="demarcating" negation="false" src="d0_s8" to="o4913" />
    </statement>
    <statement id="d0_s9">
      <text>most cauline leaf-blades exceeding internodes.</text>
      <biological_entity id="o4911" name="margin" name_original="margins" src="d0_s8" type="structure" constraint="leaf" constraint_original="leaf; leaf" />
      <biological_entity constraint="cauline" id="o4914" name="leaf-blade" name_original="leaf-blades" src="d0_s9" type="structure" />
      <biological_entity id="o4915" name="internode" name_original="internodes" src="d0_s9" type="structure" />
      <relation from="o4914" id="r697" name="exceeding" negation="false" src="d0_s9" to="o4915" />
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences 8-30 (-40) -flowered, usually dense;</text>
      <biological_entity id="o4916" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="8-30(-40)-flowered" value_original="8-30(-40)-flowered" />
        <character is_modifier="false" modifier="usually" name="density" src="d0_s10" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicel 0.5-1.5 cm, puberulent;</text>
      <biological_entity id="o4917" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s11" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracteoles 0-3 mm from flowers, green to blue, linear, 4-8 mm, puberulent.</text>
      <biological_entity id="o4918" name="bracteole" name_original="bracteoles" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o4919" from="0" from_unit="mm" name="location" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s12" to="blue" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o4919" name="flower" name_original="flowers" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: sepals dark blue to bluish purple, puberulent, lateral sepals ± erect, 8-12 × 3.5-5 mm, spurs straight, horizontal or nearly so, 9-15 mm;</text>
      <biological_entity id="o4920" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o4921" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character char_type="range_value" from="dark blue" name="coloration" src="d0_s13" to="bluish purple" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4922" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s13" to="12" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4923" name="spur" name_original="spurs" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="horizontal" value_original="horizontal" />
        <character name="orientation" src="d0_s13" value="nearly" value_original="nearly" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower petal blades ± covering stamens, 4.5-6.5 mm, clefts 2-3 mm;</text>
      <biological_entity id="o4924" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="petal" id="o4925" name="blade" name_original="blades" src="d0_s14" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4926" name="stamen" name_original="stamens" src="d0_s14" type="structure" />
      <biological_entity id="o4927" name="cleft" name_original="clefts" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o4925" id="r698" modifier="more or less" name="covering" negation="false" src="d0_s14" to="o4926" />
    </statement>
    <statement id="d0_s15">
      <text>hairs centered mostly near base of cleft, white.</text>
      <biological_entity id="o4928" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o4929" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character constraint="near base" constraintid="o4930" is_modifier="false" name="position" src="d0_s15" value="centered" value_original="centered" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s15" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o4930" name="base" name_original="base" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits 7-13 mm, 3.5-4 times longer than wide, ± puberulent.</text>
      <biological_entity id="o4931" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s16" to="13" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s16" value="3.5-4" value_original="3.5-4" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds: seed-coat cells with surfaces roughened.</text>
      <biological_entity id="o4932" name="seed" name_original="seeds" src="d0_s17" type="structure" />
      <biological_entity id="o4934" name="surface" name_original="surfaces" src="d0_s17" type="structure">
        <character is_modifier="false" name="relief_or_texture" src="d0_s17" value="roughened" value_original="roughened" />
      </biological_entity>
      <relation from="o4933" id="r699" name="with" negation="false" src="d0_s17" to="o4934" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 16.</text>
      <biological_entity constraint="seed-coat" id="o4933" name="cell" name_original="cells" src="d0_s17" type="structure" />
      <biological_entity constraint="2n" id="o4935" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Idaho, Mont., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34.</number>
  <other_name type="common_name">Strict larkspur</other_name>
  <other_name type="common_name">meadow larkspur</other_name>
  <discussion>Delphinium distichum hybridizes with D. multiplex and D. nuttallianum (D. ×diversicolor Rydberg). The name D. burkei has often been misapplied to D. distichum.</discussion>
  
</bio:treatment>