<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bryan E. Dutton, Carl S. Keener, Bruce A. Ford</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ANEMONE</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 538. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 241. 1754</place_in_publication>
      <other_info_on_pub>name conserved</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ANEMONE</taxon_hierarchy>
    <other_info_on_name type="etymology">etymology not clear: probably Greek anemos, wind; possibly from Naaman, Se for Adonis, whose blood, according to myth, produced Anemone coronaria</other_info_on_name>
    <other_info_on_name type="fna_id">101733</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Holub" date="unknown" rank="genus">Anemonastrum</taxon_name>
    <taxon_hierarchy>genus Anemonastrum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(Spach) Holub" date="unknown" rank="genus">Anemonidium</taxon_name>
    <taxon_hierarchy>genus Anemonidium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Miller" date="unknown" rank="genus">Anemonoides</taxon_name>
    <taxon_hierarchy>genus Anemonoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Miller" date="unknown" rank="genus">Hepatica</taxon_name>
    <taxon_hierarchy>genus Hepatica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="A. Löve &amp; D. Löve" date="unknown" rank="genus">Jurtsevia</taxon_name>
    <taxon_hierarchy>genus Jurtsevia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Miller" date="unknown" rank="genus">Pulsatilla</taxon_name>
    <taxon_hierarchy>genus Pulsatilla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from rhizomes, caudices, or tubers.</text>
      <biological_entity id="o19558" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o19559" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o19560" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <biological_entity id="o19561" name="tuber" name_original="tubers" src="d0_s0" type="structure" />
      <relation from="o19558" id="r2632" name="from" negation="false" src="d0_s0" to="o19559" />
      <relation from="o19558" id="r2633" name="from" negation="false" src="d0_s0" to="o19560" />
      <relation from="o19558" id="r2634" name="from" negation="false" src="d0_s0" to="o19561" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal, simple or compound, petiolate.</text>
      <biological_entity id="o19562" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade lobed or parted or undivided, reniform to obtriangular or lanceolate, margins entire or variously toothed.</text>
      <biological_entity id="o19563" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="parted or undivided reniform" name="shape" src="d0_s2" to="obtriangular or lanceolate" />
        <character char_type="range_value" from="parted or undivided reniform" name="shape" src="d0_s2" to="obtriangular or lanceolate" />
      </biological_entity>
      <biological_entity id="o19564" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="variously" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal, 2-9-flowered cymes or umbels, or flowers solitary, to 60 cm;</text>
      <biological_entity id="o19565" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o19566" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="2-9-flowered" value_original="2-9-flowered" />
      </biological_entity>
      <biological_entity id="o19567" name="umbel" name_original="umbels" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="2-9-flowered" value_original="2-9-flowered" />
      </biological_entity>
      <biological_entity id="o19568" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>involucres present, often with primary involucres subtending inflorescences, and secondary and tertiary involucres subtending inflorescence branches or single flowers (primary, secondary, and tertiary involucres appearing to in tiers), involucral-bracts 2-7 (-9), leaflike or sepaloid, distant from or close to flowers.</text>
      <biological_entity id="o19569" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="primary" id="o19570" name="involucre" name_original="involucres" src="d0_s4" type="structure" />
      <biological_entity id="o19571" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="secondary and tertiary" id="o19572" name="involucre" name_original="involucres" src="d0_s4" type="structure" />
      <biological_entity constraint="inflorescence" id="o19573" name="branch" name_original="branches" src="d0_s4" type="structure" constraint_original="subtending inflorescence" />
      <biological_entity constraint="inflorescence" id="o19574" name="flower" name_original="flowers" src="d0_s4" type="structure" constraint_original="subtending inflorescence">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o19575" name="involucral-bract" name_original="involucral-bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="9" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sepaloid" value_original="sepaloid" />
        <character constraint="from close-to-flowers" constraintid="o19576" is_modifier="false" name="arrangement" src="d0_s4" value="distant" value_original="distant" />
      </biological_entity>
      <biological_entity id="o19576" name="close-to-flower" name_original="close-to-flowers" src="d0_s4" type="structure" />
      <relation from="o19569" id="r2635" modifier="often" name="with" negation="false" src="d0_s4" to="o19570" />
      <relation from="o19570" id="r2636" name="subtending" negation="false" src="d0_s4" to="o19571" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o19577" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals not persistent in fruit, 4-20 (-27), white, purple, blue, green, yellow, pink, or red, plane, linear to oblong or ovate to obovate, 3.5-40 mm;</text>
      <biological_entity id="o19578" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o19579" is_modifier="false" modifier="not" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="oblong or ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19579" name="fruit" name_original="fruit" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="27" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals usually absent (present in A. patens), distinct, plane, obovate to elliptic, 1.5-2 mm;</text>
      <biological_entity id="o19580" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s7" value="plane" value_original="plane" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s7" to="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nectary present;</text>
      <biological_entity id="o19581" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 10-200;</text>
      <biological_entity id="o19582" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="200" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments filiform or somewhat broadened at base;</text>
      <biological_entity id="o19583" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="filiform" value_original="filiform" />
        <character constraint="at base" constraintid="o19584" is_modifier="false" modifier="somewhat" name="width" src="d0_s10" value="broadened" value_original="broadened" />
      </biological_entity>
      <biological_entity id="o19584" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>staminodes absent between stamens and pistils;</text>
      <biological_entity id="o19585" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character constraint="between stamens, pistils" constraintid="o19586, o19587" is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o19586" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o19587" name="pistil" name_original="pistils" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pistils many, simple;</text>
      <biological_entity id="o19588" name="pistil" name_original="pistils" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovule 1 per pistil;</text>
      <biological_entity id="o19589" name="ovule" name_original="ovule" src="d0_s13" type="structure">
        <character constraint="per pistil" constraintid="o19590" name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o19590" name="pistil" name_original="pistil" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style present.</text>
      <biological_entity id="o19591" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits achenes, aggregate, sessile or stalked, ovoid to obovoid, sides not veined;</text>
      <biological_entity constraint="fruits" id="o19592" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="aggregate" value_original="aggregate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="stalked" value_original="stalked" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s15" to="obovoid" />
      </biological_entity>
      <biological_entity id="o19593" name="side" name_original="sides" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>beak (persistent style) present, sometimes rudimentary, terminal, straight or curved, to 40 (-50) mm, sometimes plumose.</text>
    </statement>
    <statement id="d0_s17">
      <text>x=7 or 8.</text>
      <biological_entity id="o19594" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s16" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s16" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s16" value="curved" value_original="curved" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="50" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s16" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="x" id="o19595" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="7" value_original="7" />
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide, primarily in cooler temperate and arctic regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="primarily in cooler temperate and arctic regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Windflower</other_name>
  <other_name type="common_name">anémone</other_name>
  <discussion>Species ca. 150 (25 in the flora).</discussion>
  <discussion>The taxonomy of Anemone continues to be problematic. Anemone occidentalis and A. patens var. multifida (the first two taxa in this treatment) are frequently placed in the genus Pulsatilla Miller on the basis of the long plumose achene beaks, and A. acutiloba and A. americana (the last two taxa in this treatment) in the genus Hepatica Miller, primarily on the basis of the involucre immediately subtending the flower and the lobed, persistent leaves. Recent phylogenetic analyses of Anemone in the broad sense, however, indicate that both Pulsatilla and Hepatica should be subsumed within Anemone. While traditional morphologic characters are useful in distinguishing between Pulsatilla and Hepatica species, respectively, many other morphologic and molecular attributes are shared with Anemone, strongly suggesting that these genera should be united (S. B. Hoot et al. 1994). In addition, a number of genera that have been recognized primarily on a cytotaxonomic basis (e.g., Anemonastrum, Anemonidium, Anemonoides, and Jurtsevia) are reduced to synonymy here. Some North American species of Anemone are closely related to plants in Europe, Asia, and South America and continue to be recognized at different ranks. For example, Anemone patens Linnaeus var. multifida (a species included in this treatment) was called Pulsatilla multifida (Pritzel) Juzepczuk for the former Soviet Union by S. V. Juzepczuk (1970) and Pulsatilla patens (Linnaeus) Miller var. multifida (Pritzel) Li S.H. &amp; Huang Y. H. for China by Wang W.-T. (1980). Moreover, interspecific hybridization among some sympatric or nearly sympatric North American species also contributes to the confusion (see N. L. Britton 1891; C. L. Hitchcock et al. 1955-1969, vol. 2; R. S. Mitchell and J. K. Dean 1982). Additional analyses (e.g., G. Boraiah and M. Heimburger 1964; M. Heimburger 1959; C. Joseph and M. Heimburger 1966; and C. S. Keener et al. 1995) may prove to be helpful in resolving the taxonomy within this morphologically diverse genus.</discussion>
  <discussion>Anemone nemorosa Linnaeus, A. ranunculoides Linnaeus, and A. blanda Schott &amp; Kotschy, all native to Europe, are cultivated and may persist in the flora. Although apparently they rarely become naturalized, A. nemorosa is established at two sites in Newfoundland and Quebec, and A. ranunculoides in Quebec. Both are close relatives of A. quinquefolia and its allies.</discussion>
  <discussion>Anemone ranunculoides is the only species in North America combining yellow sepals with rhizomes and 1-2-ternate leaves. Anemone blanda will key to A. caroliniana or A. berlandieri in this treatment. It can be distinguished by its short-pilose achenes, in contrast to the densely woolly achenes of A. caroliniana and A. berlandieri. Anemone nemorosa will key to A. quinquefolia; it differs in having 6-8 sepals and brown or black (never white) rhizomes with a 3-5 mm diameter in contrast to the 5 sepals and white or black rhizomes with 1-3 mmdiameter of A. quinquefolia.</discussion>
  <discussion>Protoanemonin, an irritating acrid oil, is an enzymatic breakdown product of the glycoside ranunculin and is found in many species of Anemone. While protoanemonin can cause severe topical and gastrointestinal irritation, it is unstable and changes into harmless anemonin when plants are dried (N. J. Turner and A. F. Szczawinski 1991).</discussion>
  <discussion>A caudex, as the term is used here, is the "woody," perennating base of an aerial shoot (inflorescences and basal leaves). The word tuber refers to a swollen, more or less vertical underground stem. The aerial shoots arise from the apex of either of those persistent structures. Rhizome, as the term is used here, refers to an underground, usually horizontal stem (more or less vertical in Anemone piperi), that is nearly uniform in diameter (about 1-4 mm diam., depending on the species) along its length. Aerial shoots arise directly from nodes at or near the apex of the rhizome.</discussion>
  <discussion>Many species of Anemone have only one type of underground stem. Some species, however, have both rhizomes and caudices. In such cases the aerial shoots arise from the apex of a caudex attached to the rhizome. Some other species sometimes have both tubers and rhizomes. In those, one or more horizontal rhizomes arise near the apex of the tuber; the aerial shoots arise from the apex of the tuber.</discussion>
  <discussion>Proportions given in the key for the middle lobes of basal leaves are calculated as follows: measure length of lobe from apex to a line connecting bases of sinuses; and measure total length of blade from leaf apex to summit of petiole.</discussion>
  <references>
    <reference>Fernald, M. L. 1928b. The North American species of Anemone § Anemonanthea. Rhodora 30: 180-188.</reference>
    <reference>Frodin, D.G. 1964. A Preliminary Revision of the Section Anemonanthea of Anemone in Eastern North America, with Special Reference to the Southern Appalachian Mountains. M.S. thesis. University of Tennessee.</reference>
    <reference>Hoot, S. B., A. A. Reznicek, and J. D. Palmer. 1994. Phylogenetic relationships in Anemone (Ranunculaceae) based on morphology and chloroplast DNA. Syst. Bot. 19: 169-200.</reference>
    <reference>Wang,W.-T. 1980. Anemone In: W.-T. Wang, ed. 1980. Flora Republicae Popularis Sinicae. Vol. 28, pp. 1-56.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achene beak 20 mm or more, plumose.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achene beak 6 mm or less, glabrous or pubescent, not plumose.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucral bracts compound, ultimate segments of lateral leaflets 2-3 mm wide; leaflets of basal leaves pinnatifid to dissected, lateral leaflets 2×-parted; petals absent; sepals white, purple tinged (rarely abaxially blue proximally, white distally, and adaxially white).</description>
      <determination>1 Anemone occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucral bracts simple, each leaf deeply divided into 4-6 segments, segments 1-2(-3) mm wide; leaflets of basal leaves dichotomously dissected, lateral leaflets 3-4×-parted; petals present; sepals blue or purple (rarely nearly white).</description>
      <determination>2 Anemone patens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Basal leaves simple (often deeply divided).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Basal leaves compound.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucral bracts remotely subtending flowers, margins crenate or sharply and irregularly serrate, ±similar to basal leaves.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucral bracts closely subtending flowers, margins entire, not resembling basal leaves.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Achenes winged, beak straight or indistinct; tiers of involucral bracts usually 2.</description>
      <determination>3 Anemone canadensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Achenes not winged, beak recurved; tier of involucral bracts 1.</description>
      <determination>4 Anemone richardsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf lobes acute or acuminate, middle lobe 70-90% of total blade length; involucral bracts ±acute.</description>
      <determination>24 Anemone acutiloba</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf lobes rounded, middle lobe 50-70(-75%) of total blade length; involucral bracts obtuse.</description>
      <determination>25 Anemone americana</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Involucral bracts of distalmost tier simple (sometimes deeply lobed and sessile, occasionally appearing compound in primary involucres of A.tuberosa and A.okennonii).</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Involucral bracts of distalmost tier compound (petiole sometimes short and flat).</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Achene body winged, glabrous; inflorescences umbels or flowers solitary.</description>
      <determination>5 Anemone narcissiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Achene body not winged, hairy; inflorescences cymes or flowers solitary (sometimes umbelliform in A.cylindrica).</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Involucral bracts in 2 or more tiers.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Involucral bracts in 1 tier.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Involucral bracts ±similar to basal leaves.</description>
      <determination>6 Anemone tuberosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Involucral bracts dissimilar to basal leaves.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Basal leaves usually ternate; sepals (8-)10-20, 2-3 mm wide.</description>
      <determination>7 Anemone edwardsiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Basal leaves 2-3-ternate; sepals 7-11, 3-4.5 mm wide.</description>
      <determination>8 Anemone okennonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Aerial shoots from tubers (tuber caudex-like in A.tuberosa), tubers bearing rhizomes in some species; stamens 70 or fewer; mostly s United States.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Aerial shoots from rhizomes (from short caudices on rhizomes in A.parviflora); stamens 70 or more; nw United States and Canada.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Sepals 8-10; aerial shoots from caudex-like tuber, rhizomes absent.</description>
      <determination>6 Anemone tuberosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Sepals (7-)10-20(-30); aerial shoots from tubers, sometimes with rhizomes arising near apex of tubers (tubers rarely bearing rhizomes in Anemone berlandieri).</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">All basal leaf blades lobed or dissected differently from involucral bracts; involucres borne above middle of scape at anthesis.</description>
      <determination>9 Anemone berlandieri</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">One or more basal leaf blades dissected similarly to involucral bracts; involucres borne below middle of scape at anthesis.</description>
      <determination>10 Anemone caroliniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaflets of basal leaves (0.5-)0.7-1.8(-2.2) × 0.5-1.3 cm; sepals abaxially hairy; stamens 70-80; achenes ca. 1 mm wide, beak 1-2.5 mm.</description>
      <determination>11 Anemone parviflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaflets of basal leaves (2.5-)3-5(-6) × (2-)2.5-3.5 cm; sepals abaxially glabrous; stamens 100-120; achenes 2-3 mm wide, beak ca. 0.5 mm.</description>
      <determination>15 Anemone deltoidea</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Stamens 80 or more; involucral bracts 2-ternate; involucral bracts in 1 tier.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Stamens 80(-90) or fewer; involucral bracts 1(-2)-ternate; involucral bracts in 1 or more tiers.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Sepals white or adaxially white and abaxially bluish; stamens whitish; style white.</description>
      <determination>12 Anemone drummondii</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Sepals uniformly dark blue to purple; stamens purple; style red.</description>
      <determination>13 Anemone multiceps</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Aerial shoots from caudices or from caudices on rhizomes; involucral bracts in 1-2(-3) tiers.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Aerial shoots always from rhizomes; tier of involucral bracts 1.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Ultimate lobes of leaflets of involucral bracts 1.5-3(-4.3) mm wide or less.</description>
      <determination>14 Anemone multifida</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Ultimate lobes of leaflets of involucral bracts (4-)6 mm wide or more.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Heads of achenes usually spheric or oblong-ellipsoid; achene beak 1-1.5 mm; involucral bracts of primary involucre 3(-5).</description>
      <determination>22 Anemone virginiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Heads of achenes cylindric; achene beak 0.5-1 mm; involucral bracts 3-7(-9).</description>
      <determination>23 Anemone cylindrica</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Rhizomes mostly vertical.</description>
      <determination>18 Anemone piperi</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Rhizomes mostly horizontal.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Lateral leaflets of basal leaves and involucral bracts mostly 1×-lobed or -parted.</description>
      <determination>16a Anemone quinquefolia var. quinquefolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Lateral leaflets of basal leaves and involucral bracts only occasionally lobed.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Achene body 2.5-3 mm.</description>
      <determination>16b Anemone quinquefolia var. minima</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Achene body 3-5 mm.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Achene beak (0.5-)1-1.5 mm; sepals 20 mm or less; stamens 75 or fewer.</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Achene beak 0.5-1 mm; sepals 15 mm or less; stamens 40 or fewer.</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaflets of basal leaves 0.7-2.5(-3.5) cm wide; peduncle distally villous to pilose; w North America.</description>
      <determination>19 Anemone oregana</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaflets of basal leaves 2.5-4(-6) cm wide; peduncle ±glabrous; e North America.</description>
      <determination>17 Anemone lancifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Sepals 7-15 × 4-8 mm; pedicel (0.5-)3-10 cm in fruit; achene beak 0.6-1 mm; stamens 25-40.</description>
      <determination>21 Anemone grayi</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Sepals 3.5-8(-10) × 1.5-3(-3.5) mm; pedicel 1-3(-4) cm in fruit; achene beak ca. 0.5 mm; stamens 10-30(-35).</description>
      <determination>20 Anemone lyallii</determination>
    </key_statement>
  </key>
</bio:treatment>