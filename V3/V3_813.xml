<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="Linneaus" date="unknown" rank="section">quercus</taxon_name>
    <taxon_name authority="Small" date="1897" rank="species">geminata</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>24: 438. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section quercus;species geminata</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501037</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">virginiana</taxon_name>
    <taxon_name authority="(Small) Sargent" date="unknown" rank="variety">geminata</taxon_name>
    <taxon_hierarchy>genus Quercus;species virginiana;variety geminata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, sometimes shrubs, subevergreen, trees to 25 m, shrubs sometimes rhizomatous (if spreading rhizomatously, without numerous straight, short, erect stems emerging from gound, or if so, mixed with other larger branches, infertile, and without dimorphic or asymmetric leaf form).</text>
      <biological_entity id="o9210" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" notes="" src="d0_s0" value="subevergreen" value_original="subevergreen" />
        <character name="growth_form" value="tree" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="25" to_unit="m" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark dark-brown or black, scaly.</text>
      <biological_entity id="o9214" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="black" value_original="black" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs yellowish, becoming light gray, 1.5-3 mm diam., tomentulose, glabrate in 2d year.</text>
      <biological_entity id="o9215" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s2" value="light gray" value_original="light gray" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose" />
        <character constraint="in year" constraintid="o9216" is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o9216" name="year" name_original="year" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Buds reddish or dark-brown, globose or ovoid, 1-2.5 (-3) mm;</text>
      <biological_entity id="o9217" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s3" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>scale margins glabrous or puberulent.</text>
      <biological_entity constraint="scale" id="o9218" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves: petiole 3-10 (-20) mm.</text>
      <biological_entity id="o9219" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o9220" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade narrowly lanceolate or elliptic, rarely orbiculate, convex-cupped, (10-) 35-60 (-120) × (7-) 10-30 (-45) mm, base narrowly cuneate, rarely truncate or rounded, margins strongly revolute, entire, secondary-veins 8-10 (-12) on each side, apex acute, sometimes obtuse;</text>
      <biological_entity id="o9221" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex-cupped" value_original="convex-cupped" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s6" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="120" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s6" to="60" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_width" src="d0_s6" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="45" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9222" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9223" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9224" name="secondary-vein" name_original="secondary-veins" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="12" />
        <character char_type="range_value" constraint="on side" constraintid="o9225" from="8" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity id="o9225" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o9226" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>surfaces abaxially whitish or glaucous, densely covered with minute, appressed, fused-stellate hairs (visible under magnification), and with additional scattered, erect, felty or spreading hairs (sometimes deciduous), or light green and glabrate in shade leaves, adaxially dark or light green, glossy, glabrous or with minute, scattered, stellate hairs, secondary-veins moderately to deeply impressed.</text>
      <biological_entity id="o9227" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="densely" name="position_relational" src="d0_s7" value="covered" value_original="covered" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="light green" value_original="light green" />
        <character constraint="in shade leaves , adaxially dark or light green , glossy , glabrous or with hairs" constraintid="o9228" is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o9228" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="minute" value_original="minute" />
        <character is_modifier="true" name="arrangement" src="d0_s7" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s7" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity id="o9229" name="secondary-vein" name_original="secondary-veins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately to deeply" name="prominence" src="d0_s7" value="impressed" value_original="impressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Acorns 1-3, on peduncle 10-100 mm;</text>
      <biological_entity id="o9230" name="acorn" name_original="acorns" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o9231" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="100" to_unit="mm" />
      </biological_entity>
      <relation from="o9230" id="r1305" name="on" negation="false" src="d0_s8" to="o9231" />
    </statement>
    <statement id="d0_s9">
      <text>cup hemispheric or deeply goblet-shaped, sometimes saucer-shaped, 8-15 mm deep × 5-15 mm wide, base often constricted, scales whitish or grayish, thickened basally, keeled, acute-attenuate, tomentulose, tips reddish, glabrous or puberulent;</text>
      <biological_entity id="o9232" name="cup" name_original="cup" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s9" value="goblet--shaped" value_original="goblet--shaped" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s9" value="saucer--shaped" value_original="saucer--shaped" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s9" to="15" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="×5-15" value_original="×5-15" />
      </biological_entity>
      <biological_entity id="o9233" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often" name="size" src="d0_s9" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o9234" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="basally" name="size_or_width" src="d0_s9" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute-attenuate" value_original="acute-attenuate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
      <biological_entity id="o9235" name="tip" name_original="tips" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nut dark-brown, ovoid, barrel-shaped, or acute, (13-) 15-20 (-25) × (8-) 9-12 (-15) mm, glabrous.</text>
      <biological_entity id="o9236" name="nut" name_original="nut" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s10" value="barrel--shaped" value_original="barrel--shaped" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="barrel--shaped" value_original="barrel--shaped" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character char_type="range_value" from="13" from_unit="mm" name="atypical_length" src="d0_s10" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_width" src="d0_s10" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s10" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cotyledons connate.</text>
      <biological_entity id="o9237" name="cotyledon" name_original="cotyledons" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal plain, open evergreen woodlands and scrublands on deep sandy soils, often with pines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open evergreen woodlands" modifier="coastal plain" constraint="on deep sandy soils , often with pines" />
        <character name="habitat" value="scrublands" constraint="on deep sandy soils , often with pines" />
        <character name="habitat" value="deep sandy soils" />
        <character name="habitat" value="pines" />
        <character name="habitat" value="coastal" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>88.</number>
  <other_name type="common_name">Sand live oak</other_name>
  <discussion>Quercus geminata occurs in Cuba as putative hybrids.</discussion>
  <discussion>Although some recent authors prefer to treat Quercus geminata as a variety of Q. virginiana, the two species are easily separable and rarely intergrade through most of the broad range in which they are sympatric. Apparently this is primarily because of habitat separation, but additionally Q. geminata flowers much later than Q. virginiana in any given geographic area. At the northern extreme of the range of Q. geminata, apparent intermediates with Q. virginiana are more common, possibly because flowering times of the two species overlap to a greater extent because of slower warming in the spring. Scattered intermediates also occur where the two species are sympatric on sands in coastal Mississippi.</discussion>
  
</bio:treatment>