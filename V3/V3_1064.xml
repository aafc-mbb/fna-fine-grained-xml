<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">annonaceae</taxon_name>
    <taxon_name authority="Small" date="1924" rank="genus">deeringothamnus</taxon_name>
    <taxon_name authority="(B. L. Robinson) Small" date="1930" rank="species">rugelii</taxon_name>
    <place_of_publication>
      <publication_title>Addisonia</publication_title>
      <place_in_publication>15: 17, plate 489. 1930</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family annonaceae;genus deeringothamnus;species rugelii</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500468</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Asimina</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="species">rugelii</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(1): 465. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Asimina;species rugelii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: petiole 1-2 mm.</text>
      <biological_entity id="o14112" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o14113" name="petiole" name_original="petiole" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s0" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade oblong to obovate or oblanceolate, 1-7 cm, margins revolute, apex acute to obtuse, broadly rounded, often notched.</text>
      <biological_entity id="o14114" name="blade-leaf" name_original="leaf-blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s1" to="obovate or oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14115" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s1" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o14116" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s1" to="obtuse" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s1" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: peduncle slender, 1-2.5 cm.</text>
      <biological_entity id="o14117" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o14118" name="peduncle" name_original="peduncle" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers spreading-ascending to recurved, canary yellow, rarely purple, faintly fragrant;</text>
      <biological_entity id="o14119" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="spreading-ascending" name="orientation" src="d0_s3" to="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="canary yellow" value_original="canary yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration_or_density" src="d0_s3" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="faintly" name="odor" src="d0_s3" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals 2-3, erect or slightly spreading, ovate to oblong, ca. 10 mm, apex acute;</text>
      <biological_entity id="o14120" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblong" />
        <character name="some_measurement" src="d0_s4" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o14121" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals prevalently 6, ascending to slightly spreading, lance-oblong, ca. 1.5 cm × ca. 3-4 mm;</text>
      <biological_entity id="o14122" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="6" value_original="6" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s5" to="slightly spreading" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lance-oblong" value_original="lance-oblong" />
        <character name="length" src="d0_s5" unit="cm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pistils (2-) 5 (-7), fusiform.</text>
      <biological_entity id="o14123" name="pistil" name_original="pistils" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s6" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="7" />
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s6" value="fusiform" value_original="fusiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Berries 3-6 cm.</text>
      <biological_entity id="o14124" name="berry" name_original="berries" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 1-1.5 cm, ± compressed laterally.</text>
      <biological_entity id="o14125" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="more or less; laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring, all year as disturbance reaction.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="flowering time" char_type="range_value" modifier="as disturbance reacti" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist sandy peats of slash pine-palmetto flats, savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy peats" modifier="moist" constraint="of slash pine-palmetto flats , savannas" />
        <character name="habitat" value="slash pine-palmetto flats" />
        <character name="habitat" value="savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Rugel's pawpaw</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Deeringothamnus rugelii commonly associates with both Asimina pygmaea and A. reticulata, and overlaps in flowering time with both. No hybrids between the two genera had been observed until a recent discovery of a putative hybrid between D. rugelii and Asimina reticulata by Dr. E. Norman (pers. comm. 1994).</discussion>
  <references>
    <reference>Small, J. K. 1930. Deeringothamnus rugelii. Addisonia 15: 17-18.</reference>
  </references>
  
</bio:treatment>