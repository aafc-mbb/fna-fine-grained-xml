<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">clematis</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1895" rank="subgenus">viorna</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="species">hirsutissima</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 385. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus clematis;subgenus viorna;species hirsutissima</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500394</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coriflora</taxon_name>
    <taxon_name authority="(Pursh) W. A. Weber" date="unknown" rank="species">hirsutissima</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>79: 65-66. 1996[1995]</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Coriflora;species hirsutissima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, not viny, 1.5-6.5 dm, hirsute (sometimes sparsely so in var. hirsutissima) or densely short, soft-pubescent to nearly glabrous.</text>
      <biological_entity id="o14232" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="viny" value_original="viny" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="6.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="densely" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character char_type="range_value" from="soft-pubescent" name="pubescence" src="d0_s0" to="nearly glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade 2-3-pinnate;</text>
      <biological_entity id="o14233" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-3-pinnate" value_original="2-3-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets often deeply 2-several-lobed, if lobed than lateral lobes usually small and distinctly narrower than central portion, leaflets or lobes linear to lanceolate, 1-6 × 0.05-1.5 cm, thin, not prominently reticulate;</text>
      <biological_entity id="o14234" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often deeply" name="shape" src="d0_s2" value="2-several-lobed" value_original="2-several-lobed" />
        <character constraint="than lateral lobes" constraintid="o14235" is_modifier="false" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character constraint="than central portion , leaflets or lobes" constraintid="o14236, o14237, o14238" is_modifier="false" name="width" src="d0_s2" value="distinctly narrower" value_original="distinctly narrower" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" notes="" src="d0_s2" to="6" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not prominently" name="architecture_or_coloration_or_relief" src="d0_s2" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o14235" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <biological_entity constraint="central" id="o14236" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="0.05" from_unit="cm" name="width" notes="" src="d0_s2" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o14237" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="central" id="o14238" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>surfaces sparsely to densely silky-hirsute, not glaucous.</text>
      <biological_entity id="o14239" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="silky-hirsute" value_original="silky-hirsute" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, flowers solitary.</text>
      <biological_entity id="o14240" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o14241" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers broadly cylindric to urn-shaped;</text>
      <biological_entity id="o14242" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly cylindric" name="shape" src="d0_s5" to="urn-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals very dark violet-blue or rarely pink or white, oblong-lanceolate, 2.5-4.5 cm, margins narrowly expanded distally, 0.5-2 mm wide, thin, distally ± crisped, tomentose, tips obtuse to acute, slightly spreading, abaxially usually densely hirsute, occasionally moderately so.</text>
      <biological_entity id="o14243" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark violet-blue" value_original="dark violet-blue" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14244" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly; distally" name="size" src="d0_s6" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="distally more or less" name="shape" src="d0_s6" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o14245" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="abaxially usually densely" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Achenes: bodies densely long-pubescent;</text>
      <biological_entity id="o14246" name="achene" name_original="achenes" src="d0_s7" type="structure" />
      <biological_entity id="o14247" name="body" name_original="bodies" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="long-pubescent" value_original="long-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>beak 4-9 cm, plumose.</text>
      <biological_entity id="o14248" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <biological_entity id="o14249" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s8" to="9" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Idaho, Mont., N.Mex., Nebr., Okla., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The varieties of Clematis hirsutissima, although highly dissimilar in their extreme forms, intergrade extensively in Wyoming, Colorado, and Utah.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets and lobes linear to narrowly lanceolate, 0.5–6(–10) mm wide.</description>
      <determination>32a Clematis hirsutissima var. hirsutissima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets and lobes narrowly to broadly lanceolate or ovate, 5–15 mm wide.</description>
      <determination>32b Clematis hirsutissima var. scottii</determination>
    </key_statement>
  </key>
</bio:treatment>