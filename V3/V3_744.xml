<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="N. I. Malyutin" date="1987" rank="subsection">exaltata</taxon_name>
    <taxon_name authority="S. Watson" date="1880" rank="species">glaucum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. California</publication_title>
      <place_in_publication>2: 427. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection exaltata;species glaucum;</taxon_hierarchy>
    <other_info_on_name type="special_status">W2</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500500</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">scopulorum</taxon_name>
    <taxon_name authority="(S. Watson) A. Gray" date="unknown" rank="variety">glaucum</taxon_name>
    <taxon_hierarchy>genus Delphinium;species scopulorum;variety glaucum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="G. N. Jones" date="unknown" rank="species">splendens</taxon_name>
    <taxon_hierarchy>genus Delphinium;species splendens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (60-) 100-200 (-300) cm;</text>
      <biological_entity id="o3167" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base usually green, glabrous, glaucous.</text>
      <biological_entity id="o3168" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, 15-20, absent from proximal 1/5 of stem at anthesis;</text>
      <biological_entity id="o3169" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s2" to="20" />
        <character constraint="from proximal 1/5" constraintid="o3170" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3170" name="1/5" name_original="1/5" src="d0_s2" type="structure" />
      <biological_entity id="o3171" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o3170" id="r413" name="part_of" negation="false" src="d0_s2" to="o3171" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1-14 cm.</text>
      <biological_entity id="o3172" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade round to pentagonal, 2-11 × 3-18 cm, margins seldom laciniate, glabrous;</text>
      <biological_entity id="o3173" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s4" to="pentagonal" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="11" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3174" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="seldom" name="shape" src="d0_s4" value="laciniate" value_original="laciniate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 5-9 (-15), width 5-24 (-35) mm, tips abruptly tapered to mucronate apex;</text>
      <biological_entity constraint="ultimate" id="o3175" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="15" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="9" />
        <character char_type="range_value" from="24" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3176" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character char_type="range_value" from="abruptly tapered" name="shape" src="d0_s5" to="mucronate" />
      </biological_entity>
      <biological_entity id="o3177" name="apex" name_original="apex" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>midcauline leaf lobes more than 3 times longer than wide.</text>
      <biological_entity constraint="leaf" id="o3178" name="lobe" name_original="lobes" src="d0_s6" type="structure" constraint_original="midcauline leaf">
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="3+" value_original="3+" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences (13-) 40-90 (-140) -flowered;</text>
      <biological_entity id="o3179" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="(13-)40-90(-140)-flowered" value_original="(13-)40-90(-140)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 1-3 (-5) cm, puberulent or glabrous;</text>
      <biological_entity id="o3180" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles 2-6 (-10) mm from flowers, green to blue, linear, 2-7 mm, puberulent or glabrous.</text>
      <biological_entity id="o3181" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o3182" from="2" from_unit="mm" name="location" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s9" to="blue" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3182" name="flower" name_original="flowers" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals bluish purple to lavender, puberulent, lateral sepals forward pointing to spreading, 8-14 (-21) × 3-6 mm, spurs straight, ascending to ca. 45°, 10-15 (-19) mm;</text>
      <biological_entity id="o3183" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3184" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="bluish purple" name="coloration" src="d0_s10" to="lavender" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o3185" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pointing" name="orientation" src="d0_s10" to="spreading" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="21" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3186" name="spur" name_original="spurs" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="45°" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="19" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower petal blades ± covering stamens, 4-6 mm, clefts 1-3 mm;</text>
      <biological_entity id="o3187" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="petal" id="o3188" name="blade" name_original="blades" src="d0_s11" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3189" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o3190" name="cleft" name_original="clefts" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o3188" id="r414" modifier="more or less" name="covering" negation="false" src="d0_s11" to="o3189" />
    </statement>
    <statement id="d0_s12">
      <text>hairs centered, mostly near base of cleft, white.</text>
      <biological_entity id="o3191" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3192" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="centered" value_original="centered" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o3193" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o3192" id="r415" modifier="mostly" name="near" negation="false" src="d0_s12" to="o3193" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits 9-20 mm, 3.5-4.5 times longer than wide, glabrous to puberulent.</text>
      <biological_entity id="o3194" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="20" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s13" value="3.5-4.5" value_original="3.5-4.5" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s13" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds wing-margined;</text>
      <biological_entity id="o3195" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>seed-coat cells elongate but short, surfaces smooth or roughened.</text>
      <biological_entity constraint="seed-coat" id="o3196" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 16.</text>
      <biological_entity id="o3197" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s15" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3198" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, wet thickets, bogs, streamsides, open coniferous woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="wet thickets" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="open coniferous woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Sask., Yukon; Alaska, Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Mountain larkspur</other_name>
  <other_name type="common_name">Brown's larkspur</other_name>
  <other_name type="common_name">Hooker's larkspur</other_name>
  <other_name type="common_name">pale-flowered Brown's larkspur</other_name>
  <other_name type="common_name">duncecap larkspur</other_name>
  <other_name type="common_name">giant larkspur</other_name>
  <other_name type="common_name">tall larkspur</other_name>
  <other_name type="common_name">western larkspur</other_name>
  <discussion>At the sites in Manitoba and Saskatchewan, Delphinium glaucum is naturalized, not native.</discussion>
  <discussion>Delphinium glaucum hybridizes extensively with D. barbeyi in Utah and Colorado to the extent that hybrids [D. ×occidentale (S. Watson) S. Watson] are more common in many areas than individuals of either parental stock. It occasionally hybridizes with D. distichum, D. polycladon, D. ramosum, and D. stachydeum. Hybrids with D. brachycentrum are called D. ×nutans A. Nelson.</discussion>
  <discussion>Tremendous variation is apparent in what is here recognized as Delphinium glaucum. This is the northern expression of the complex described in the discussion under Delphinium subsect. Exaltata. Although some geographic patterns are apparent in the variation within D. glaucum, infraspecific entities are not here recognized. Apparently because of rather recent and/or incomplete genetic isolation, the degree of differentiation between these units is not such that they can be consistently recognized.</discussion>
  <discussion>Specimens named Delphinium splendens represent plants grown in high-moisture, low-light conditions and may occur as sporadic individuals anywhere from California to Alaska. Type specimens of D. brownii Rydberg, D. canmorense Rydberg, and D. hookeri A. Nelson represent plants grown on relatively dry sites at high latitudes. Plants from dry sites at low latitudes are represented by D. bakerianum Bornmüller and D. occidentale var. reticulatum A. Nelson. Plants with lavender to white flowers are represented by type specimens of D. brownii forma pallidiflorum B. Boivin and D. cucullatum A. Nelson. Type specimens of D. alatum A. Nelson and D. glaucum var. alpinum F. L. Wynd (an invalid name) represent plants growing above or near treeline.</discussion>
  <discussion>Delphinium glaucum may be confused with D. californicum, D. exaltatum, D. polycladon, or D. stachydeum. For distinctions from D. californicum, see discussion under that species. Absence of basal or proximal cauline leaves, generally much larger plants (greater than 1.5 m), more flowers in the inflorescence, and shorter petioles on the leaves of D. glaucum are features that serve to distinguish this species from D. polycladon. In the latter, the leaves are primarily on the proximal stem, plants often less than 1.5 m, flowers more scattered, and petioles more than twice the length of leaf blades. Features of the sepals may be used to distinguish D. glaucum (dark lavender to blue purple, usually only minutely puberulent) from D. stachydeum (bright blue, densely puberulent). Vegetative parts of D. stachydeum are also densely puberulent, while those of D. glaucum typically are glabrous.</discussion>
  
</bio:treatment>