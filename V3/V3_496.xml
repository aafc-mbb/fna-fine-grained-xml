<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce A. Ford</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="J. Ellis in C. Linnaeus" date="1759" rank="genus">HYDRASTIS</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1088. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus HYDRASTIS</taxon_hierarchy>
    <other_info_on_name type="etymology">referring to superficial resemblance to some species of Hydrophyllum</other_info_on_name>
    <other_info_on_name type="fna_id">115979</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from creeping, yellow, thick rhizomes.</text>
      <biological_entity id="o16402" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o16403" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="true" name="coloration" src="d0_s0" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o16402" id="r2255" name="from" negation="false" src="d0_s0" to="o16403" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, simple, petiolate;</text>
      <biological_entity id="o16404" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves alternate.</text>
      <biological_entity constraint="cauline" id="o16405" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade palmately 3-9-lobed [variously divided], broadly cordate-orbiculate, margins serrate.</text>
      <biological_entity id="o16406" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="3-9-lobed" value_original="3-9-lobed" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="cordate-orbiculate" value_original="cordate-orbiculate" />
      </biological_entity>
      <biological_entity id="o16407" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, solitary flowers;</text>
      <biological_entity id="o16408" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o16409" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts absent.</text>
      <biological_entity id="o16410" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o16411" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals not persistent in fruit, 3, greenish white to creamy white, plane, ovate, oval, or elliptic, 3.5-7 mm;</text>
      <biological_entity id="o16412" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o16413" is_modifier="false" modifier="not" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="greenish white" name="coloration" notes="" src="d0_s7" to="creamy white" />
        <character is_modifier="false" name="shape" src="d0_s7" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16413" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals absent;</text>
      <biological_entity id="o16414" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 50-75;</text>
      <biological_entity id="o16415" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s9" to="75" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments abruptly narrowed near apex;</text>
      <biological_entity id="o16416" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character constraint="near apex" constraintid="o16417" is_modifier="false" modifier="abruptly" name="shape" src="d0_s10" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o16417" name="apex" name_original="apex" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>staminodes absent between stamens and pistils;</text>
      <biological_entity id="o16418" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character constraint="between stamens, pistils" constraintid="o16419, o16420" is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o16419" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o16420" name="pistil" name_original="pistils" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pistils 5-15, simple;</text>
      <biological_entity id="o16421" name="pistil" name_original="pistils" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules 2 per ovary;</text>
      <biological_entity id="o16422" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character constraint="per ovary" constraintid="o16423" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o16423" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style short.</text>
      <biological_entity id="o16424" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits berries, aggregate, sessile, spheric, sides not veined;</text>
      <biological_entity constraint="fruits" id="o16425" name="berry" name_original="berries" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="aggregate" value_original="aggregate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s15" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity id="o16426" name="side" name_original="sides" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>beak terminal, ± straight, 0.6-1 mm.</text>
      <biological_entity id="o16427" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s16" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds black, ellipsoid, smooth, lustrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 13.</text>
      <biological_entity id="o16428" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="lustrous" value_original="lustrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o16429" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, Asia (Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Goldenseal</other_name>
  <other_name type="common_name">orangeroot</other_name>
  <other_name type="common_name">yellow-puccoon</other_name>
  <other_name type="common_name">sceau d'or</other_name>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Many classifications have included Hydrastis within Ranunculaceae (A. Cronquist 1981; W. C. Gregory 1941; O. F. Langlet 1932; M. Tamura 1963, 1968, 1993). Recent workers (H. Tobe and R. C. Keating 1985; A. L. Takhtajan 1987), however, have assigned Hydrastis to its own family, intermediate between Ranunculaceae and Berberidaceae. Phylogenetic studies by S. B. Hoot (1991) confirmed the isolated position of Hydrastis and suggested that characteristics such as the following justify its exclusion from Ranunculaceae: undifferentiated mesophyll, xylem straight (instead of V-shaped) in cross section, scalariform perforations in the vessels, micropyle defined by two integuments, pollen with a distinct striate-reticulate tectum, and a base chromosome number of x = 13 (as opposed to 6, 7, 8, or 9).</discussion>
  <discussion>C. S. Keener (1993) challenged these conclusions, stating that features such as scalariform perforations in vessels, striate-reticulate pollen, and the micropyle defined by two integuments are found in other genera within Ranunculaceae. Only straight xylem in cross section and the base chromosome number are distinctive. Whether these features warrant segregation of Hydrastis into its own family is debatable. Decisions involving the circumscription of this genus await a molecular study involving Berberidaceae, Ranunculaceae, and related families.</discussion>
  <references>
    <reference>Eichenberger, M. D. and G. R. Parker. 1976. Goldenseal (Hydrastis canadensis L.) distribution, phenology and biomass in an oak-hickory forest. Ohio J. Sci. 76: 204-210.</reference>
    <reference>Keener, C. S. 1993. A review of the classification of the genus Hydrastis (Ranunculaceae). Aliso 13: 551-558.</reference>
    <reference>Massey, J. R., D. K. S. Otte, T. A. Atkinson, and R. D. Whetstone. 1983. An Atlas and Illustrated Guide to Threatened and Endangered Vascular Plants of the Mountains of North Carolina and Virginia. Washington. [U.S.D.A. Forest Serv., Gen. Techn. Rep. SE-20.]</reference>
    <reference>Tobe, H. and R. C. Keating. 1985. The morphology and anatomy of Hydrastis (Ranunculales): Systematic reevaluation of the genus. Bot. Mag. (Tokyo) 98: 291-316.</reference>
  </references>
  
</bio:treatment>