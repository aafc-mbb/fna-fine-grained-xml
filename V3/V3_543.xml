<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="de Candolle" date="1824" rank="section">Hecatonia</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>1: 30. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section hecatonia;</taxon_hierarchy>
    <other_info_on_name type="fna_id">302017</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glabrous or sometimes hirsute.</text>
      <biological_entity id="o22474" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping (erect in Ranunculus sceleratus), not bulbous-based, without bulbils.</text>
      <biological_entity id="o22475" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="bulbous-based" value_original="bulbous-based" />
      </biological_entity>
      <biological_entity id="o22476" name="bulbil" name_original="bulbils" src="d0_s1" type="structure" />
      <relation from="o22475" id="r3070" name="without" negation="false" src="d0_s1" to="o22476" />
    </statement>
    <statement id="d0_s2">
      <text>Roots basal and usually also nodal, never tuberous.</text>
      <biological_entity id="o22477" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s2" value="nodal" value_original="nodal" />
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s2" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves all cauline or both basal and cauline;</text>
      <biological_entity id="o22478" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o22479" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal and cauline" id="o22480" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>basal and lower cauline leaves similar, petiolate, blades lobed to divided or (submerged leaves) deeply dissected, segments undivided or lobed.</text>
      <biological_entity constraint="basal and lower cauline" id="o22481" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o22482" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lobed" name="shape" src="d0_s4" to="divided or deeply dissected" />
      </biological_entity>
      <biological_entity id="o22483" name="segment" name_original="segments" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 2-25-flowered cymes or axillary solitary flowers.</text>
      <biological_entity id="o22484" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o22485" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="2-25-flowered" value_original="2-25-flowered" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o22486" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="2-25-flowered" value_original="2-25-flowered" />
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers pedicellate;</text>
      <biological_entity id="o22487" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals deciduous soon after anthesis, 3-5;</text>
      <biological_entity id="o22488" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="after anthesis" is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 3-14, yellow;</text>
      <biological_entity id="o22489" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="14" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary scale variable, forming crescent-shaped ridge surrounding but not covering nectary, or funnel-shaped tube surrounding nectary, or distal margin of funnel expanded to form large flap, then nectary sometimes displaced onto flap, glabrous, free margin entire;</text>
      <biological_entity constraint="nectary" id="o22490" name="scale" name_original="scale" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="variable" value_original="variable" />
        <character is_modifier="false" name="position_relational" src="d0_s9" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity id="o22491" name="ridge" name_original="ridge" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="crescent--shaped" value_original="crescent--shaped" />
      </biological_entity>
      <biological_entity id="o22492" name="nectary" name_original="nectary" src="d0_s9" type="structure" />
      <biological_entity id="o22493" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="funnel--shaped" value_original="funnel--shaped" />
      </biological_entity>
      <biological_entity id="o22494" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s9" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity constraint="distal" id="o22495" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <biological_entity id="o22496" name="flap" name_original="flap" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="funnel" value_original="funnel" />
        <character is_modifier="true" name="size" src="d0_s9" value="expanded" value_original="expanded" />
        <character is_modifier="true" name="size" src="d0_s9" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o22497" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22498" name="flap" name_original="flap" src="d0_s9" type="structure" />
      <biological_entity id="o22499" name="margin" name_original="margin" src="d0_s9" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o22490" id="r3071" name="forming" negation="false" src="d0_s9" to="o22491" />
      <relation from="o22490" id="r3072" name="covering" negation="true" src="d0_s9" to="o22492" />
      <relation from="o22495" id="r3073" name="part_of" negation="false" src="d0_s9" to="o22496" />
      <relation from="o22497" id="r3074" name="displaced onto" negation="false" src="d0_s9" to="o22498" />
    </statement>
    <statement id="d0_s10">
      <text>style present or absent.</text>
      <biological_entity id="o22500" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits achenes, 1-locular;</text>
      <biological_entity constraint="fruits" id="o22501" name="achene" name_original="achenes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>achene body thick-lenticular or compressed-ellipsoid to discoid, 1.2-3 times as wide as thick, not prolonged beyond seed;</text>
      <biological_entity constraint="achene" id="o22502" name="body" name_original="body" src="d0_s12" type="structure">
        <character char_type="range_value" from="compressed-ellipsoid" name="shape" src="d0_s12" to="discoid" />
        <character is_modifier="false" name="width_or_width" src="d0_s12" value="1.2-3 times as wide as thick" />
        <character constraint="beyond seed" constraintid="o22503" is_modifier="false" modifier="not" name="length" src="d0_s12" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o22503" name="seed" name_original="seed" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>achene wall thick, smooth or with weak transverse wrinkles, glabrous;</text>
      <biological_entity constraint="achene" id="o22504" name="wall" name_original="wall" src="d0_s13" type="structure">
        <character is_modifier="false" name="width" src="d0_s13" value="thick" value_original="thick" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="with weak transverse wrinkles" value_original="with weak transverse wrinkles" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22505" name="wrinkle" name_original="wrinkles" src="d0_s13" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s13" value="weak" value_original="weak" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s13" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o22504" id="r3075" name="with" negation="false" src="d0_s13" to="o22505" />
    </statement>
    <statement id="d0_s14">
      <text>margin a thick, low or high corky band or ridge;</text>
      <biological_entity id="o22506" name="margin" name_original="margin" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="thick" value_original="thick" />
        <character is_modifier="false" name="position" src="d0_s14" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o22507" name="band" name_original="band" src="d0_s14" type="structure">
        <character is_modifier="true" name="height" src="d0_s14" value="high" value_original="high" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s14" value="corky" value_original="corky" />
      </biological_entity>
      <biological_entity id="o22508" name="ridge" name_original="ridge" src="d0_s14" type="structure">
        <character is_modifier="true" name="height" src="d0_s14" value="high" value_original="high" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s14" value="corky" value_original="corky" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak much shorter than achene body or sometimes absent.</text>
      <biological_entity id="o22509" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character constraint="than achene body" constraintid="o22510" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="much shorter" value_original="much shorter" />
        <character is_modifier="false" modifier="sometimes" name="presence" notes="" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="achene" id="o22510" name="body" name_original="body" src="d0_s15" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Widespread, in marshy or aquatic habitats.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Widespread" establishment_means="native" />
        <character name="distribution" value="in marshy or aquatic habitats" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.5.</number>
  <discussion>Species ca. 15 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems erect, rooting only at base (very rarely also at proximal nodes).</description>
      <determination>61 Ranunculus sceleratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems prostrate and rooting at nodes, or floating and rootless.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 0.3–1.2 cm, deeply 3-lobed or -parted, terminal segment entire or distally crenulate; nectary-scale low crescent-shaped ridge surrounding nectary; style 0.1–0.2 mm.</description>
      <determination>60 Ranunculus hyperboreus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 0.6–7.3 cm, 3-parted, terminal segment again lobed or dissected; nectary-scale a free flap, nectary on surface of flap; style 0.2–1.2 mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Styles 0.2–0.4 mm in flower; achenes 1–1.6 mm, beaks 0.4–0.8 mm; petals 3–7 mm</description>
      <determination>62 Ranunculus gmelinii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Styles 0.8–1.2 mm in flower; achenes 1.8–2.2 mm, beaks 1–1.8 mm; petals 7–12 mm.</description>
      <determination>63 Ranunculus flabellaris</determination>
    </key_statement>
  </key>
</bio:treatment>