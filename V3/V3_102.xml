<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">argemone</taxon_name>
    <taxon_name authority="Durand &amp; Hilgard" date="1854" rank="species">munita</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia, ser.</publication_title>
      <place_in_publication>2, 3: 37. 1854</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus argemone;species munita</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500133</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial.</text>
      <biological_entity id="o25296" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 4-16 dm, densely to sparingly prickly.</text>
      <biological_entity id="o25297" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s1" to="16" to_unit="dm" />
        <character is_modifier="false" modifier="densely to sparingly" name="architecture" src="d0_s1" value="prickly" value_original="prickly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades: surfaces copiously prickly on veins and intervein surfaces to sparingly prickly on main veins only;</text>
      <biological_entity id="o25298" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure" />
      <biological_entity id="o25299" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character constraint="on veins; on veins and intervein surfaces" constraintid="o25301, o25302" is_modifier="false" modifier="copiously" name="architecture" src="d0_s2" value="prickly" value_original="prickly" />
      </biological_entity>
      <biological_entity id="o25300" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character constraint="on main veins" constraintid="o25303" is_modifier="false" modifier="sparingly" name="architecture" src="d0_s2" value="prickly" value_original="prickly" />
      </biological_entity>
      <biological_entity id="o25301" name="vein" name_original="veins" src="d0_s2" type="structure" />
      <biological_entity constraint="intervein" id="o25302" name="surface" name_original="surfaces" src="d0_s2" type="structure" />
      <biological_entity constraint="main" id="o25303" name="vein" name_original="veins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>basal lobed ca. 1/2 distance to midrib, lobe apices usually distinctly rounded, marginal teeth usually less than 1 mm;</text>
      <biological_entity id="o25304" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o25305" name="midrib" name_original="midrib" src="d0_s3" type="structure" />
      <biological_entity constraint="lobe" id="o25306" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually distinctly" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o25307" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal usually definitely clasping.</text>
      <biological_entity id="o25308" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="usually definitely" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: buds oblong to ellipsoid to obovoid, body 12-22 × 10-16 mm, prickly;</text>
      <biological_entity id="o25309" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o25310" name="bud" name_original="buds" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="ellipsoid" />
      </biological_entity>
      <biological_entity id="o25311" name="body" name_original="body" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s5" to="22" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="16" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="prickly" value_original="prickly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepal horns terete, flattened or angular in cross-section, (4-) 6-8 (-10) mm, unarmed to densely prickly.</text>
      <biological_entity id="o25312" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="sepal" id="o25313" name="horn" name_original="horns" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
        <character constraint="in cross-section" constraintid="o25314" is_modifier="false" name="shape" src="d0_s6" value="angular" value_original="angular" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s6" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="unarmed" name="architecture" src="d0_s6" to="densely prickly" />
      </biological_entity>
      <biological_entity id="o25314" name="cross-section" name_original="cross-section" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 5-10 (-13) cm broad, not closely subtended by foliaceous bracts;</text>
      <biological_entity id="o25315" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="width" src="d0_s7" to="13" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25316" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <relation from="o25315" id="r3419" modifier="not closely; closely" name="subtended by" negation="false" src="d0_s7" to="o25316" />
    </statement>
    <statement id="d0_s8">
      <text>petals white;</text>
      <biological_entity id="o25317" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 150-250;</text>
      <biological_entity id="o25318" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="150" name="quantity" src="d0_s9" to="250" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments pale-yellow;</text>
      <biological_entity id="o25319" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistil 3-5-carpellate.</text>
      <biological_entity id="o25320" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-5-carpellate" value_original="3-5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ovoid, ellipsoid, or lanceoloid, 35-55 × 9-15 (-18) mm (including stigma and excluding prickles), prickly, longest prickles to 10 mm, widely spaced, or shorter, more numerous, and interspersed with still shorter prickles, surface then partially obscured.</text>
      <biological_entity id="o25321" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceoloid" value_original="lanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s12" to="55" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="18" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s12" to="15" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="prickly" value_original="prickly" />
      </biological_entity>
      <biological_entity constraint="longest" id="o25322" name="prickle" name_original="prickles" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s12" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="quantity" src="d0_s12" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o25323" name="prickle" name_original="prickles" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="still" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25324" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="partially" name="prominence" src="d0_s12" value="obscured" value_original="obscured" />
      </biological_entity>
      <relation from="o25322" id="r3420" name="interspersed with" negation="false" src="d0_s12" to="o25323" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1.8-2.6 mm.</text>
      <biological_entity id="o25325" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s13" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Idaho, Nev., Oreg., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Subspecies 4 (4 in the flora).</discussion>
  <discussion>The Kawaiisu used Argemone munita medicinally to treat burns (D. E. Moerman 1986, no varieties specified).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Prickles 0–30 per cm² on main stem below capsule; leaf surfaces unarmed or sparingly prickly on veins and intervein areas.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Prickles 100–500 per cm² on main stem below capsule; leaf surfaces mostly closely prickly on veins and intervein areas.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Prickles 0–10 per cm²; abaxial leaf surface sparingly prickly on main veins, adaxial surface usually unarmed.</description>
      <determination>7a Argemone munita subsp. robusta</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Prickles 10–30 per cm²; both leaf surfaces sparingly to moderately prickly on veins and often on intervein areas.</description>
      <determination>7b Argemone munita subsp. munita</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Longest prickles of stem 4–6 mm; stems usually purplish; bud prickles sometimes branched.</description>
      <determination>7c Argemone munita subsp. rotundata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Longest prickles of stem 7–10 mm; stems greenish white; bud prickles simple.</description>
      <determination>7d Argemone munita subsp. argentea</determination>
    </key_statement>
  </key>
</bio:treatment>