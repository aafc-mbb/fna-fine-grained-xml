<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="G. Don in J. C. Loudon" date="1830" rank="section">lobatae</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">laevis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>234. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section lobatae;species laevis</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501054</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">catesbaei</taxon_name>
    <taxon_hierarchy>genus Quercus;species catesbaei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, deciduous, to 20 m.</text>
      <biological_entity id="o1193" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark bluish gray, deeply furrowed, inner bark orangish or reddish.</text>
      <biological_entity id="o1195" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="bluish gray" value_original="bluish gray" />
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s1" value="furrowed" value_original="furrowed" />
      </biological_entity>
      <biological_entity constraint="inner" id="o1196" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="orangish" value_original="orangish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs dark reddish-brown with distinct grayish cast, (1.5-) 2-3.5 (-4) mm diam., sparsely pubescent to almost glabrous.</text>
      <biological_entity id="o1197" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character constraint="with cast" constraintid="o1198" is_modifier="false" name="pubescence" src="d0_s2" value="pubescent to almost" value_original="pubescent to almost" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" notes="" src="d0_s2" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="diameter" notes="" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" notes="" src="d0_s2" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent to almost" value_original="pubescent to almost" />
        <character is_modifier="false" modifier="almost" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1198" name="cast" name_original="cast" src="d0_s2" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="grayish" value_original="grayish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Terminal buds light-brown to reddish-brown, conic or narrowly ovoid-ellipsoid, 5.5-12 mm, pubescent.</text>
      <biological_entity constraint="terminal" id="o1199" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s3" to="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s3" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="ovoid-ellipsoid" value_original="ovoid-ellipsoid" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 5-25 mm, glabrous.</text>
      <biological_entity id="o1200" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1201" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade circular or broadly ovate-elliptic, widest near or proximal to middle, 100-200 × 80-150 mm, base attenuate to acute, occasionally obtuse or rounded, blade decurrent on petiole, margins with 3-7 (-9) lobes and 7-20 awns, lobes attenuate to falcate, occasionally oblong or distally expanded, apex acute to acuminate;</text>
      <biological_entity id="o1202" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="circular" value_original="circular" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="width" src="d0_s5" value="widest" value_original="widest" />
        <character char_type="range_value" from="proximal" name="position" src="d0_s5" to="middle" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s5" to="200" to_unit="mm" />
        <character char_type="range_value" from="80" from_unit="mm" name="width" src="d0_s5" to="150" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1203" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="acute occasionally obtuse or rounded" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="acute occasionally obtuse or rounded" />
      </biological_entity>
      <biological_entity id="o1204" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character constraint="on petiole" constraintid="o1205" is_modifier="false" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o1205" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
      <biological_entity id="o1206" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o1207" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="9" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o1208" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="9" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o1209" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="falcate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="distally; distally" name="size" src="d0_s5" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o1210" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <relation from="o1206" id="r141" name="with" negation="false" src="d0_s5" to="o1207" />
      <relation from="o1206" id="r142" name="with" negation="false" src="d0_s5" to="o1208" />
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially occasionally orange-scurfy, usually glabrous except for conspicuous axillary tufts of tomentum, adaxially glabrous, secondary-veins raised on both surfaces.</text>
      <biological_entity id="o1211" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially occasionally" name="pubescence" src="d0_s6" value="orange-scurfy" value_original="orange-scurfy" />
        <character constraint="except-for axillary tufts" constraintid="o1212" is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" notes="" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o1212" name="tuft" name_original="tufts" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o1213" name="tomentum" name_original="tomentum" src="d0_s6" type="structure" />
      <biological_entity id="o1214" name="secondary-vein" name_original="secondary-veins" src="d0_s6" type="structure" />
      <biological_entity id="o1215" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <relation from="o1212" id="r143" name="part_of" negation="false" src="d0_s6" to="o1213" />
      <relation from="o1214" id="r144" name="raised on both" negation="false" src="d0_s6" to="o1215" />
    </statement>
    <statement id="d0_s7">
      <text>Acorns biennial;</text>
      <biological_entity id="o1216" name="acorn" name_original="acorns" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cup somewhat goblet-shaped, 9-14 mm high × 16-24 mm wide, covering 1/3 nut, outer surface puberulent, inner surface pubescent, scales occasionally tuberculate, tips loose, especially at margin of cup, acute, margin conspicuously involute;</text>
      <biological_entity id="o1217" name="cup" name_original="cup" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s8" value="goblet--shaped" value_original="goblet--shaped" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s8" to="14" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="×16-24" value_original="×16-24" />
      </biological_entity>
      <biological_entity id="o1218" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o1219" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="inner" id="o1220" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o1221" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="occasionally" name="relief" src="d0_s8" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o1222" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s8" value="loose" value_original="loose" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o1223" name="margin" name_original="margin" src="d0_s8" type="structure" />
      <biological_entity id="o1224" name="cup" name_original="cup" src="d0_s8" type="structure" />
      <biological_entity id="o1225" name="margin" name_original="margin" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
      </biological_entity>
      <relation from="o1217" id="r145" name="covering" negation="false" src="d0_s8" to="o1218" />
      <relation from="o1222" id="r146" modifier="especially" name="at" negation="false" src="d0_s8" to="o1223" />
      <relation from="o1223" id="r147" name="part_of" negation="false" src="d0_s8" to="o1224" />
    </statement>
    <statement id="d0_s9">
      <text>nut ovoid to broadly ellipsoid, 17-28 × 12-18 mm, often faintly striate, glabrate, scar diam. 6-10 mm.</text>
      <biological_entity id="o1226" name="nut" name_original="nut" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s9" to="broadly ellipsoid" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s9" to="28" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s9" to="18" to_unit="mm" />
        <character is_modifier="false" modifier="often faintly" name="coloration_or_pubescence_or_relief" src="d0_s9" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o1227" name="scar" name_original="scar" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diam" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early to mid spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid spring" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sandy soils of barrens, sandhills, and well-drained ridges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sandy soils" constraint="of barrens , sandhills , and well-drained ridges" />
        <character name="habitat" value="barrens" />
        <character name="habitat" value="sandhills" />
        <character name="habitat" value="well-drained ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-150 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Turkey oak</other_name>
  <discussion>Quercus laevis reportedly hybridizes with Q. falcata (= Q. ×blufftonensis Trelease), Q. hemisphaerica, Q. incana, and Q. marilandica (C. S. Sargent 1918); with Q. nigra; and with Q. arkansana, Q. coccinea, Q. myrtifolia, Q. phellos, Q. shumardii, and Q. velutina (D. M. Hunt 1989).</discussion>
  
</bio:treatment>