<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">nymphaeaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">NYMPHAEA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 510. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 227. 1754, name conserved</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nymphaeaceae;genus NYMPHAEA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek nymphaia and Latin nymphaea, water-lily, from Latin (nympha) or Greek (nymphe) mythology, goddess of mountains, waters, meadows, and forests</other_info_on_name>
    <other_info_on_name type="fna_id">122531</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Salisbury" date="unknown" rank="genus">Castalia</taxon_name>
    <taxon_hierarchy>genus Castalia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes branched or unbranched, erect or repent;</text>
      <biological_entity id="o2933" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="repent" value_original="repent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>elongate stolons present or absent.</text>
      <biological_entity id="o2934" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly floating (vernal leaves submersed; blades sessile, broad).</text>
      <biological_entity id="o2935" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="growth_form_or_location" src="d0_s2" value="floating" value_original="floating" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade orbiculate to widely ovate or elliptic, basal lobes divergent to overlapping, margins entire to spinose-dentate, apex of lobe acute or acuminate to widely rounded;</text>
      <biological_entity id="o2936" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s3" to="widely ovate or elliptic" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2937" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="divergent" name="arrangement" src="d0_s3" to="overlapping" />
      </biological_entity>
      <biological_entity id="o2938" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="entire to spinose-dentate" value_original="entire to spinose-dentate" />
      </biological_entity>
      <biological_entity id="o2940" name="lobe" name_original="lobe" src="d0_s3" type="structure" />
      <relation from="o2939" id="r370" name="part_of" negation="false" src="d0_s3" to="o2940" />
    </statement>
    <statement id="d0_s4">
      <text>primary venation mostly palmate, midrib with 1 vein.</text>
      <biological_entity id="o2939" name="apex" name_original="apex" src="d0_s3" type="structure" constraint="lobe" constraint_original="lobe; lobe">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="widely rounded" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="primary" value_original="primary" />
      </biological_entity>
      <biological_entity id="o2941" name="whole-organism" name_original="" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s4" value="palmate" value_original="palmate" />
      </biological_entity>
      <biological_entity id="o2942" name="midrib" name_original="midrib" src="d0_s4" type="structure" />
      <biological_entity id="o2943" name="vein" name_original="vein" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <relation from="o2942" id="r371" name="with" negation="false" src="d0_s4" to="o2943" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers floating or emersed, opening diurnally or nocturnally;</text>
      <biological_entity id="o2944" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="location" src="d0_s5" value="floating" value_original="floating" />
        <character is_modifier="false" name="location" src="d0_s5" value="emersed" value_original="emersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth perigynous, spreading at anthesis;</text>
      <biological_entity id="o2945" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="perigynous" value_original="perigynous" />
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals 4, mostly greenish, ovate to elliptic;</text>
      <biological_entity id="o2946" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 8-many, spirally arranged or wholly or partially whorled, showy, white, pink, blue, or yellow, broadly lanceolate or ovate to obovate, grading into stamens;</text>
      <biological_entity id="o2947" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" is_modifier="false" name="quantity" src="d0_s8" to="many" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="wholly" name="arrangement" src="d0_s8" value="or" value_original="or" />
        <character is_modifier="false" modifier="partially" name="arrangement" src="d0_s8" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="showy" value_original="showy" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="obovate" />
      </biological_entity>
      <biological_entity id="o2948" name="stamen" name_original="stamens" src="d0_s8" type="structure" />
      <relation from="o2947" id="r372" name="into" negation="false" src="d0_s8" to="o2948" />
    </statement>
    <statement id="d0_s9">
      <text>stamens yellow or cream-colored, inserted on lateral surface of ovary, spreading at anthesis, sometimes with distal connective appendage;</text>
      <biological_entity id="o2949" name="stamen" name_original="stamens" src="d0_s9" type="structure" constraint="ovary" constraint_original="ovary; ovary">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="cream-colored" value_original="cream-colored" />
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o2950" name="surface" name_original="surface" src="d0_s9" type="structure" />
      <biological_entity id="o2951" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
      <biological_entity constraint="connective" id="o2952" name="appendage" name_original="appendage" src="d0_s9" type="structure" constraint_original="distal connective" />
      <relation from="o2949" id="r373" name="inserted on" negation="false" src="d0_s9" to="o2950" />
      <relation from="o2949" id="r374" name="part_of" negation="false" src="d0_s9" to="o2951" />
      <relation from="o2949" id="r375" modifier="sometimes" name="with" negation="false" src="d0_s9" to="o2952" />
    </statement>
    <statement id="d0_s10">
      <text>ovary shorter than petals and stamens;</text>
      <biological_entity id="o2953" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character constraint="than petals and stamens" constraintid="o2954, o2955" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o2954" name="petal" name_original="petals" src="d0_s10" type="structure" />
      <biological_entity id="o2955" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stigmatic disk with prominent, distinct, upwardly incurved appendages around margin.</text>
      <biological_entity constraint="stigmatic" id="o2956" name="disk" name_original="disk" src="d0_s11" type="structure" />
      <biological_entity id="o2957" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="true" modifier="upwardly" name="orientation" src="d0_s11" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity id="o2958" name="margin" name_original="margin" src="d0_s11" type="structure" />
      <relation from="o2956" id="r376" name="with" negation="false" src="d0_s11" to="o2957" />
      <relation from="o2957" id="r377" name="around" negation="false" src="d0_s11" to="o2958" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits borne on curved or coiled peduncles.</text>
      <biological_entity id="o2959" name="fruit" name_original="fruits" src="d0_s12" type="structure" />
      <biological_entity id="o2960" name="peduncle" name_original="peduncles" src="d0_s12" type="structure">
        <character is_modifier="true" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="coiled" value_original="coiled" />
      </biological_entity>
      <relation from="o2959" id="r378" name="borne on" negation="false" src="d0_s12" to="o2960" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds nearly globose to ellipsoid, to 5 mm;</text>
      <biological_entity id="o2961" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="nearly globose" name="shape" src="d0_s13" to="ellipsoid" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>aril present.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 14.</text>
      <biological_entity id="o2962" name="aril" name_original="aril" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o2963" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Water-lily</other_name>
  <other_name type="common_name">nymphéa</other_name>
  <other_name type="common_name">lis d'eau</other_name>
  <other_name type="common_name">nénuphar blanc</other_name>
  <discussion>Species 35-40 (9 in the flora).</discussion>
  <discussion>Nymphaea is an important genus of ornamental plants, with numerous cultivars or wild forms grown in water gardens. Some have become naturalized in some places, particularly in Florida, and two such taxa are included in this treatment. A third, N. ×daubenyana W. T. Baxter ex Daubeny (N. micrantha Guillemin &amp; Perrottet × N. caerulea Savigny), with blue flowers and entire leaves and with a proliferous mound of fibrous tissue above insertion of petiole, may also be encountered in Florida.</discussion>
  <discussion>Prior to conservation in its current sense, the name Nymphaea was frequently used for the genus now known as Nuphar.</discussion>
  <references>
    <reference>Conard, H. S. 1905. The waterlilies: A monograph of the genus Nymphaea. Publ. Carnegie Inst. Wash. 4: 1-279.</reference>
    <reference>Meeuse, B. J. D. and E. L. Schneider. 1980. Nymphaea revisited: A preliminary communication. Israel J. Bot. 28: 65-79.</reference>
    <reference>Moseley, M. F. Jr. 1961. Morphological studies of the Nymphaeaceae. II. The flower of Nymphaea. Bot. Gaz. 122: 233-259.</reference>
    <reference>Ward, D. B. 1977. Keys to the flora of Florida. 4. Nymphaea (Nymphaeaceae). Phytologia 37: 443-448.</reference>
    <reference>Wiersema, J. H. 1987. A monograph of Nymphaea subgenus Hydrocallis (Nymphaeaceae). Syst. Bot. Monogr. 16: 1-112.</reference>
    <reference>Wiersema, J. H. 1988. Reproductive biology of Nymphaea (Nymphaeaceae). Ann. Missouri Bot. Gard. 75: 795-804.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals abaxially flecked with short dark lines.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals abaxially uniformly greenish, reddish, or yellowish.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade with dentate to spinose-dentate margins; petals white; connective appendage projecting to 3 mm or more beyond anther.</description>
      <determination>1 Nymphaea ampla</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade with entire to sinuate margins; petals pale violet to nearly white; connective appendage mostly projecting 1 mm or less beyond anther.</description>
      <determination>2 Nymphaea elegans</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals blue, lavender, or purple; connective appendage projecting to 4 mm or more beyond anther.</description>
      <determination>3 Nymphaea capensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals white to pink, cream-colored, or yellow; connective appendage projecting less than 2 mm beyond anther.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade with spinose-dentate margins, abaxially slightly to densely puberulent.</description>
      <determination>5 Nymphaea lotus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade with entire or sinuate margins, abaxially glabrous.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Appendages at margin of stigmatic disk slightly club-shaped; flowers opening nocturnally; leaf blade with central web of cross veins between major veins.</description>
      <determination>4 Nymphaea jamesoniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Appendages at margin of stigmatic disk tapered or boat-shaped; flowers opening diurnally; leaf blade with radiate venation, without central weblike pattern.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Petals yellow; plants bearing stolons.</description>
      <determination>6 Nymphaea mexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Petals white; plants not bearing stolons.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Petals 17–43; filament widest below middle; rhizomes repent.</description>
      <determination>9 Nymphaea odorata</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Petals 8–17; filament widest above middle; rhizomes erect.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Appendages at margin of stigmatic disk 0.6–1.5 mm; lines of insertion of sepals on receptacle not prominent.</description>
      <determination>7 Nymphaea leibergii</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Appendages at margin of stigmatic disk mostly 3 mm or more; lines of insertion of sepals very prominent, forming tetragon on receptacle.</description>
      <determination>8 Nymphaea tetragona</determination>
    </key_statement>
  </key>
</bio:treatment>