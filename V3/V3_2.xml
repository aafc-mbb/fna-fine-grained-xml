<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">quercus</taxon_name>
    <taxon_name authority="G. Don in J. C. Loudon" date="1830" rank="section">lobatae</taxon_name>
    <taxon_name authority="(E. J. Palmer) Stoynoff &amp; W. J. Hess ex R. J. Jensen" date="1997" rank="species">acerifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>3: 465. 1997</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus quercus;section lobatae;species acerifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501004</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quercus</taxon_name>
    <taxon_name authority="Buckley" date="unknown" rank="species">shumardii</taxon_name>
    <taxon_name authority="E. J. Palmer" date="1927" rank="variety">acerifolia</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>8: 54. 1927</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Quercus;species shumardii;variety acerifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, deciduous, to 15 m.</text>
      <biological_entity id="o4337" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark dark gray to almost black, sometimes becoming rough and furrowed.</text>
      <biological_entity id="o4339" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark gray to almost" value_original="dark gray to almost" />
        <character is_modifier="false" modifier="almost" name="coloration" src="d0_s1" value="black" value_original="black" />
        <character is_modifier="false" modifier="sometimes becoming" name="pubescence_or_relief" src="d0_s1" value="rough" value_original="rough" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs grayish brown to reddish-brown, 1.5-3 (-3.5) mm diam., glabrous or sparsely pubescent.</text>
      <biological_entity id="o4340" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character char_type="range_value" from="grayish brown" name="coloration" src="d0_s2" to="reddish-brown" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s2" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Terminal buds gray to grayish brown, ovoid or broadly ellipsoid, 3.5-5.5 mm, glabrous.</text>
      <biological_entity constraint="terminal" id="o4341" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s3" to="grayish brown" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s3" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 20-45 mm, glabrous.</text>
      <biological_entity id="o4342" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o4343" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="45" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade oblate to broadly elliptic, 70-140 × (60-) 100-150 (-180) mm, base cordate-truncate to obtuse, margins with 5-7 (-9) lobes and 11-48 awns, lobes ovate-oblong or markedly distally expanded, the middle or apical pairs often overlapping, apex acute;</text>
      <biological_entity id="o4344" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblate" name="shape" src="d0_s5" to="broadly elliptic" />
        <character char_type="range_value" from="70" from_unit="mm" name="length" src="d0_s5" to="140" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="atypical_width" src="d0_s5" to="100" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="180" to_unit="mm" />
        <character char_type="range_value" from="100" from_unit="mm" name="width" src="d0_s5" to="150" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4345" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="cordate-truncate" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity id="o4346" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o4347" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="9" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o4348" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="9" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o4349" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" modifier="markedly distally" name="size" src="d0_s5" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="position" src="d0_s5" value="middle" value_original="middle" />
        <character is_modifier="false" name="position" src="d0_s5" value="apical" value_original="apical" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o4350" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o4346" id="r614" name="with" negation="false" src="d0_s5" to="o4347" />
      <relation from="o4346" id="r615" name="with" negation="false" src="d0_s5" to="o4348" />
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially glabrous or with prominent axillary tufts of tomentum, occasionally with scattered pubescence, adaxially glabrous, secondary-veins raised on both surfaces.</text>
      <biological_entity id="o4351" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="with prominent axillary tufts" value_original="with prominent axillary tufts" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o4352" name="tuft" name_original="tufts" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o4353" name="tomentum" name_original="tomentum" src="d0_s6" type="structure" />
      <biological_entity id="o4354" name="secondary-vein" name_original="secondary-veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="character" src="d0_s6" value="pubescence" value_original="pubescence" />
        <character is_modifier="true" modifier="adaxially" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4355" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <relation from="o4351" id="r616" name="with" negation="false" src="d0_s6" to="o4352" />
      <relation from="o4352" id="r617" name="part_of" negation="false" src="d0_s6" to="o4353" />
      <relation from="o4351" id="r618" modifier="occasionally" name="with" negation="false" src="d0_s6" to="o4354" />
      <relation from="o4354" id="r619" name="raised on both" negation="false" src="d0_s6" to="o4355" />
    </statement>
    <statement id="d0_s7">
      <text>Acorns biennial;</text>
      <biological_entity id="o4356" name="acorn" name_original="acorns" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cup saucer to cupshaped, 4-7 mm high × 10-20 mm wide, covering 1/4-1/3 nut, outer surface glabrous or puberulent, inner surface light-brown to redbrown, glabrous or with ring of pubescence around scar, scales often with pale margins, tips tightly appressed, obtuse or acute;</text>
      <biological_entity id="o4357" name="cup" name_original="cup" src="d0_s8" type="structure">
        <character constraint="to cupshaped , 4-7 mm high×10-20 mm" is_modifier="false" name="width" src="d0_s8" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o4358" name="nut" name_original="nut" src="d0_s8" type="structure">
        <character char_type="range_value" from="1/4" is_modifier="true" name="quantity" src="d0_s8" to="1/3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o4359" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="inner" id="o4360" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s8" to="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with ring" value_original="with ring" />
      </biological_entity>
      <biological_entity id="o4361" name="ring" name_original="ring" src="d0_s8" type="structure" />
      <biological_entity id="o4362" name="scar" name_original="scar" src="d0_s8" type="structure" />
      <biological_entity id="o4363" name="scale" name_original="scales" src="d0_s8" type="structure" />
      <biological_entity id="o4364" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o4365" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o4357" id="r620" name="covering" negation="false" src="d0_s8" to="o4358" />
      <relation from="o4360" id="r621" name="with" negation="false" src="d0_s8" to="o4361" />
      <relation from="o4361" id="r622" name="around" negation="false" src="d0_s8" to="o4362" />
      <relation from="o4363" id="r623" name="with" negation="false" src="d0_s8" to="o4364" />
    </statement>
    <statement id="d0_s9">
      <text>nut ovoid to oblong, 10.5-20 × 9-15 mm, glabrous or pubescent, scar diam. 5-9 mm.</text>
      <biological_entity id="o4366" name="nut" name_original="nut" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s9" to="oblong" />
        <character char_type="range_value" from="10.5" from_unit="mm" name="length" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o4367" name="scar" name_original="scar" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diam" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry glades, slopes, and ridge tops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry glades" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="ridge tops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <other_name type="common_name">Maple-leaf oak</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Quercus acerifolia is known only from four localities in Arkansas: Magazine Mountain, Logan County; Porter Mountain, Polk County; Pryor Mountain, Montgomery County; and Sugarloaf Mountain, Sebastian County (N. Stoynoff and W. J. Hess 1990; G. P. Johnson 1992, 1994). Some specimens suggest hybridization with Q. marilandica and/or Q. velutina, but no hybrids have been reported.</discussion>
  
</bio:treatment>