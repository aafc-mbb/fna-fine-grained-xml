<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="(Webb) Rouy &amp; Foucaud" date="1893" rank="section">Flammula</taxon_name>
    <taxon_name authority="Geyer ex Bentham" date="1849" rank="species">alismifolius</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="1922" rank="variety">hartwegii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Calif.</publication_title>
      <place_in_publication>1: 534. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section flammula;species alismifolius;variety hartwegii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501110</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ranunculus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">hartwegii</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>3: 45. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ranunculus;species hartwegii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 13-30 cm × 1-3 mm, glabrous.</text>
      <biological_entity id="o26017" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="13" from_unit="cm" name="length" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s0" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots not or scarcely fusiform-thickened proximally.</text>
      <biological_entity id="o26018" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="scarcely; proximally" name="size_or_width" src="d0_s1" value="fusiform-thickened" value_original="fusiform-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole glabrous.</text>
      <biological_entity id="o26019" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26020" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade lanceolate (usually narrowly so), 3.4-9.5 × 0.8-1.5 cm, base acute to acuminate (or sometimes obtuse), margins entire.</text>
      <biological_entity id="o26021" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3.4" from_unit="cm" name="length" src="d0_s3" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26022" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
      <biological_entity id="o26023" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: petals 5-6, 6-8 × 3-5 mm.</text>
      <biological_entity id="o26024" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o26025" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, open slopes and stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="open slopes" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>50b.</number>
  <discussion>This variety is poorly defined and grades into several other varieties.</discussion>
  
</bio:treatment>