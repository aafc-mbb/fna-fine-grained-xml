<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <place_of_publication>
      <publication_title>Deut. Dendrol.</publication_title>
      <place_in_publication>106, 1893 (as Betulae)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily BETULOIDEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">20501</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trunks and branches terete.</text>
      <biological_entity id="o10331" name="trunk" name_original="trunks" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o10332" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark thin, close or exfoliating in thin sheets, becoming thicker and frequently furrowed or broken in age;</text>
      <biological_entity id="o10333" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="close" value_original="close" />
        <character constraint="in sheets" constraintid="o10334" is_modifier="false" name="relief" src="d0_s1" value="exfoliating" value_original="exfoliating" />
        <character is_modifier="false" modifier="frequently" name="architecture" src="d0_s1" value="furrowed" value_original="furrowed" />
        <character constraint="in age" constraintid="o10335" is_modifier="false" name="condition_or_fragility" src="d0_s1" value="broken" value_original="broken" />
      </biological_entity>
      <biological_entity id="o10334" name="sheet" name_original="sheets" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="becoming" name="width" notes="alterIDs:o10334" src="d0_s1" value="thicker" value_original="thicker" />
      </biological_entity>
      <biological_entity id="o10335" name="age" name_original="age" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>lenticels often present, prominent, sometimes becoming greatly expanded horizontally.</text>
      <biological_entity id="o10336" name="lenticel" name_original="lenticels" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="sometimes becoming greatly; horizontally" name="size" src="d0_s2" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark and wood strongly tanniferous.</text>
      <biological_entity id="o10337" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s3" value="tanniferous" value_original="tanniferous" />
      </biological_entity>
      <biological_entity id="o10338" name="wood" name_original="wood" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s3" value="tanniferous" value_original="tanniferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Young twigs and buds often covered with small to large, resinous glands;</text>
      <biological_entity id="o10339" name="twig" name_original="twigs" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o10340" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o10341" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="small" is_modifier="true" name="size" src="d0_s4" to="large" />
        <character is_modifier="true" name="coating" src="d0_s4" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o10339" id="r1435" modifier="often" name="covered with" negation="false" src="d0_s4" to="o10341" />
      <relation from="o10340" id="r1436" modifier="often" name="covered with" negation="false" src="d0_s4" to="o10341" />
    </statement>
    <statement id="d0_s5">
      <text>pith triangular in cross-section.</text>
      <biological_entity id="o10342" name="pith" name_original="pith" src="d0_s5" type="structure">
        <character constraint="in cross-section" constraintid="o10343" is_modifier="false" name="shape" src="d0_s5" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o10343" name="cross-section" name_original="cross-section" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Leaves 3-ranked, occasionally nearly 2-ranked.</text>
      <biological_entity id="o10344" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="3-ranked" value_original="3-ranked" />
        <character is_modifier="false" modifier="occasionally nearly" name="arrangement" src="d0_s6" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: perianth of 4 (–6) sepals, well defined, minute, membranaceous.</text>
      <biological_entity id="o10345" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o10346" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="well" name="prominence" notes="" src="d0_s7" value="defined" value_original="defined" />
        <character is_modifier="false" name="size" src="d0_s7" value="minute" value_original="minute" />
        <character is_modifier="false" name="texture" src="d0_s7" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
      <biological_entity id="o10347" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="6" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <relation from="o10346" id="r1437" name="consist_of" negation="false" src="d0_s7" to="o10347" />
    </statement>
    <statement id="d0_s8">
      <text>Pistillate flowers 2–3 per scale, scales arranged in conelike catkins;</text>
      <biological_entity id="o10348" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="per scale" constraintid="o10349" from="2" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o10349" name="scale" name_original="scale" src="d0_s8" type="structure" />
      <biological_entity id="o10350" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character constraint="in catkins" constraintid="o10351" is_modifier="false" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o10351" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="conelike" value_original="conelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth not obvious;</text>
      <biological_entity id="o10352" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s9" value="obvious" value_original="obvious" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovules with 1 integument.</text>
      <biological_entity id="o10353" name="ovule" name_original="ovules" src="d0_s10" type="structure" />
      <biological_entity id="o10354" name="integument" name_original="integument" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <relation from="o10353" id="r1438" name="with" negation="false" src="d0_s10" to="o10354" />
    </statement>
    <statement id="d0_s11">
      <text>Infructescences 1–4 cm, conelike, composed of many scales;</text>
      <biological_entity id="o10355" name="infructescence" name_original="infructescences" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="conelike" value_original="conelike" />
      </biological_entity>
      <biological_entity id="o10356" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="many" value_original="many" />
      </biological_entity>
      <relation from="o10355" id="r1439" name="composed of" negation="false" src="d0_s11" to="o10356" />
    </statement>
    <statement id="d0_s12">
      <text>scales either persistent or deciduous with fruits, crowded, small, woody or leathery.</text>
      <biological_entity id="o10357" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character constraint="with fruits" constraintid="o10358" is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s12" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="size" src="d0_s12" value="small" value_original="small" />
        <character is_modifier="false" name="texture" src="d0_s12" value="woody" value_original="woody" />
        <character is_modifier="false" name="texture" src="d0_s12" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o10358" name="fruit" name_original="fruits" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits tiny samaras, lateral wings 2, membranous, sometimes reduced to ridges;</text>
      <biological_entity id="o10359" name="fruit" name_original="fruits" src="d0_s13" type="structure" />
      <biological_entity id="o10360" name="samara" name_original="samaras" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="tiny" value_original="tiny" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o10361" name="wing" name_original="wings" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
        <character constraint="to ridges" constraintid="o10362" is_modifier="false" modifier="sometimes" name="size" src="d0_s13" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o10362" name="ridge" name_original="ridges" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pericarp thin, leathery.</text>
      <biological_entity id="o10363" name="pericarp" name_original="pericarp" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s14" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Primarily boreal and cool temperate zones of Northern Hemisphere</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Primarily boreal and cool temperate zones of Northern Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31a.</number>
  <discussion>Genera 2, species 60 (2 genera, 26 species in the flora).</discussion>
  
</bio:treatment>