<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="Ewan" date="1936" rank="subsection">Subscaposa</taxon_name>
    <taxon_name authority="A. Gray" date="1887" rank="species">parryi</taxon_name>
    <taxon_name authority="(Davidson) M. J. Warnock" date="1990" rank="subspecies">maritimum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>68(1): 2. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection subscaposa;species parryi;subspecies maritimum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500536</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">parryi</taxon_name>
    <taxon_name authority="Davidson" date="unknown" rank="variety">maritimum</taxon_name>
    <place_of_publication>
      <publication_title>Muhlenbergia</publication_title>
      <place_in_publication>4: 35. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species parryi;variety maritimum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots less than 10 cm.</text>
      <biological_entity id="o11893" name="root" name_original="roots" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 15-40 cm.</text>
      <biological_entity id="o11894" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves well distributed;</text>
      <biological_entity id="o11895" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s2" value="distributed" value_original="distributed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and cauline leaves present at anthesis;</text>
      <biological_entity constraint="basal and cauline" id="o11896" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ultimate lobes 5-10, width usually more than 6 mm.</text>
      <biological_entity constraint="ultimate" id="o11897" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="10" />
        <character char_type="range_value" from="6" from_unit="mm" modifier="usually" name="width" src="d0_s4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: bracteoles 4-9 mm.</text>
      <biological_entity id="o11898" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o11899" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals usually spreading, lateral sepals 9-20 mm, spurs 8-21 mm;</text>
      <biological_entity id="o11900" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o11901" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o11902" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11903" name="spur" name_original="spurs" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lower petal blades 4-11 mm. 2n = 16.</text>
      <biological_entity id="o11904" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="petal" id="o11905" name="blade" name_original="blades" src="d0_s7" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11906" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23b.</number>
  <other_name type="common_name">Maritime larkspur</other_name>
  <discussion>Populations of Delphinium parryi subsp. maritimum are local, very near the coast. The species also occurs on islands off southern California and northern Baja California, Mexico. Occasional individuals with white to grayish blue sepals occur (mostly in Ventura and western Los Angeles counties, where entire populations may consist of such individuals). Collections of subsp. maritimum made before 1940 are numerous; recent collections are much less common. Population reductions have probably resulted from urbanization of its preferred habitat.</discussion>
  <discussion>Confused with Delphinium variegatum, D. parryi subsp. maritimum lacks the long hairs of that species. Some plants of subsp. maritimum (unringed seeds, erect fruits, and arched hairs) from very near the coast appear superficially like some plants of D. nuttallianum (ringed seeds, spreading fruits, and no arched hairs).</discussion>
  
</bio:treatment>