<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(de Candolle) B. Boivin" date="1944" rank="section">Heterogamia</taxon_name>
    <taxon_name authority="Fernald" date="1900" rank="species">confine</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>2: 232. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section heterogamia;species confine;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501261</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="B. Boivin" date="unknown" rank="species">turneri</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species turneri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="Trelease" date="unknown" rank="species">venulosum</taxon_name>
    <taxon_name authority="(Fernald) B. Boivin" date="unknown" rank="variety">confine</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species venulosum;variety confine;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, to 100 cm, from rhizomes.</text>
      <biological_entity id="o17272" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17273" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o17272" id="r2372" name="from" negation="false" src="d0_s0" to="o17273" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: cauline leaves 3-4, those proximal to inflorescence petiolate, those subtending panicle branches sessile.</text>
      <biological_entity id="o17274" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="cauline" id="o17275" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="4" />
        <character is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o17276" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="panicle" id="o17277" name="branch" name_original="branches" src="d0_s1" type="structure" constraint_original="subtending panicle">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade: leaflets reniform-cordate, apically 3-5-lobed, 15-50 mm wide, lobe margins crenate, surfaces glabrous to glandular.</text>
      <biological_entity id="o17278" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure" />
      <biological_entity id="o17279" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="reniform-cordate" value_original="reniform-cordate" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s2" value="3-5-lobed" value_original="3-5-lobed" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s2" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o17280" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o17281" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s2" to="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal, panicles, narrow with ascending branches, many flowered.</text>
      <biological_entity id="o17282" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o17283" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character constraint="with branches" constraintid="o17284" is_modifier="false" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="quantity" notes="" src="d0_s3" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="flowered" value_original="flowered" />
      </biological_entity>
      <biological_entity id="o17284" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals yellowish to purple, oblong, 1.5-5 mm;</text>
      <biological_entity id="o17285" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o17286" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s4" to="purple" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments colored, not white;</text>
      <biological_entity id="o17287" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o17288" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="colored" value_original="colored" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers 3-4.5 mm, mucronate;</text>
      <biological_entity id="o17289" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o17290" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stigma purple.</text>
      <biological_entity id="o17291" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o17292" name="stigma" name_original="stigma" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes 4-13, erect, incurved, not reflexed, sessile;</text>
      <biological_entity id="o17293" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="13" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>body fusiform to ovoid, not laterally compressed, adaxial surface 4-6 mm, glandular, veins prominent, not anastomosing-reticulate;</text>
      <biological_entity id="o17294" name="body" name_original="body" src="d0_s9" type="structure">
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s9" to="ovoid" />
        <character is_modifier="false" modifier="not laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17295" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o17296" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="not" name="architecture_or_coloration_or_relief" src="d0_s9" value="anastomosing-reticulate" value_original="anastomosing-reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>beak (2-) 2.5-4 (-5) mm.</text>
      <biological_entity id="o17297" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early-mid summer (Jun–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="early" constraint="Jun-Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alluvial or shingly calcareous shores and talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alluvial" />
        <character name="habitat" value="shingly calcareous" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., P.E.I., Que.; N.Y., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Northern meadow-rue</other_name>
  <other_name type="common_name">pigamon des fronterières</other_name>
  <discussion>The relationship between Thalictrum confine and T. venulosum is unclear and requires additional field study.</discussion>
  
</bio:treatment>