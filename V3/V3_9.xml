<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">berberis</taxon_name>
    <taxon_name authority="Kearney &amp; Peebles" date="1939" rank="species">harrisoniana</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>29: 477. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus berberis;species harrisoniana</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500231</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, evergreen, 0.5-1.5 m.</text>
      <biological_entity id="o18925" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems often ± dimorphic, with elongate primary and somewhat elongate axillary shoots.</text>
      <biological_entity id="o18926" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often more or less" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="primary" id="o18927" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="true" modifier="somewhat" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o18926" id="r2541" name="with" negation="false" src="d0_s1" to="o18927" />
    </statement>
    <statement id="d0_s2">
      <text>Bark of 2d-year stems brown or gray, glabrous.</text>
      <biological_entity id="o18928" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18929" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <relation from="o18928" id="r2542" name="part_of" negation="false" src="d0_s2" to="o18929" />
    </statement>
    <statement id="d0_s3">
      <text>Bud-scales 1.5-3 mm, deciduous.</text>
      <biological_entity id="o18930" name="bud-scale" name_original="bud-scales" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines absent.</text>
      <biological_entity id="o18931" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves 3-foliolate;</text>
      <biological_entity id="o18932" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petioles 1.5-5 cm.</text>
      <biological_entity id="o18933" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaflet blades thick and rigid;</text>
      <biological_entity constraint="leaflet" id="o18934" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>surfaces abaxially ± dull, papillose, adaxially dull, rarely glossy, somewhat glaucous;</text>
      <biological_entity id="o18935" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abaxially more or less" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
        <character is_modifier="false" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="rarely" name="reflectance" src="d0_s8" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>terminal leaflet sessile, blade 2.9-5.4 × 2.2-3.2 cm, 1.3-2.4 times as long as wide;</text>
      <biological_entity constraint="terminal" id="o18936" name="leaflet" name_original="leaflet" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o18937" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.9" from_unit="cm" name="length" src="d0_s9" to="5.4" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="width" src="d0_s9" to="3.2" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s9" value="1.3-2.4" value_original="1.3-2.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflet blades ovate or rhombic to lanceolate, 1-3-veined from base, base acute to rounded-obtuse, margins plane or undulate, lobed, with 1-2 teeth 5-13 mm high tipped with spines to 2-3.4 × 0.3-0.4 mm, apex acuminate.</text>
      <biological_entity constraint="leaflet" id="o18938" name="blade" name_original="blades" src="d0_s10" type="structure" constraint_original="lateral leaflet">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s10" to="lanceolate" />
        <character constraint="from base" constraintid="o18939" is_modifier="false" name="architecture" src="d0_s10" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o18939" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o18940" name="base" name_original="base" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="rounded-obtuse" />
      </biological_entity>
      <biological_entity id="o18941" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s10" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
        <character constraint="with spines" constraintid="o18943" is_modifier="false" name="architecture" src="d0_s10" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o18942" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
        <character char_type="range_value" from="5" from_unit="mm" name="height" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18943" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18944" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o18941" id="r2543" name="with" negation="false" src="d0_s10" to="o18942" />
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences racemose, rather dense, 6-11-flowered, 1.5-2.5 cm;</text>
      <biological_entity id="o18945" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="racemose" value_original="racemose" />
        <character is_modifier="false" modifier="rather" name="density" src="d0_s11" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="6-11-flowered" value_original="6-11-flowered" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s11" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracteoles membranous, apex acute or obtuse.</text>
      <biological_entity id="o18946" name="bracteole" name_original="bracteoles" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o18947" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers: anther-filaments with distal pair of recurved lateral teeth.</text>
      <biological_entity id="o18948" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o18949" name="anther-filament" name_original="anther-filaments" src="d0_s13" type="structure" />
      <biological_entity constraint="distal" id="o18950" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="lateral" id="o18951" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o18949" id="r2544" name="with" negation="false" src="d0_s13" to="o18950" />
      <relation from="o18950" id="r2545" name="part_of" negation="false" src="d0_s13" to="o18951" />
    </statement>
    <statement id="d0_s14">
      <text>Berries blue-black, glaucous, spheric to short-ovoid, 5-6 mm, juicy, solid.</text>
      <biological_entity id="o18952" name="berry" name_original="berries" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="blue-black" value_original="blue-black" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s14" to="short-ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s14" value="juicy" value_original="juicy" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter (Jan–Mar).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="winter" from="winter" constraint="Jan-Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shady spots in rocky canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shady spots" constraint="in rocky canyons" />
        <character name="habitat" value="rocky canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Berberis harrisoniana is endemic to the Kofa and Ajo mountains. It has not been tested for resistance to infection by Puccinia graminis.</discussion>
  
</bio:treatment>