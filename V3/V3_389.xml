<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">betula</taxon_name>
    <taxon_name authority="Ehrhart" date="1790" rank="species">pubescens</taxon_name>
    <taxon_name authority="(Ledebour) Nyman" date="1881" rank="subspecies">tortuosa</taxon_name>
    <place_of_publication>
      <publication_title>Consp. Fl. Eur.,</publication_title>
      <place_in_publication>672. 1881</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus betula;species pubescens;subspecies tortuosa;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500264</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Betula</taxon_name>
    <taxon_name authority="Ledebour" date="unknown" rank="species">tortuosa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Ross.</publication_title>
      <place_in_publication>3: 652. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Betula;species tortuosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Betula</taxon_name>
    <taxon_name authority="Bechstein" date="unknown" rank="species">odorata</taxon_name>
    <taxon_hierarchy>genus Betula;species odorata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1–12 m;</text>
      <biological_entity id="o22375" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="12" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks usually many, often interlacing, branches ascending.</text>
      <biological_entity id="o22376" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s1" value="many" value_original="many" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s1" value="interlacing" value_original="interlacing" />
      </biological_entity>
      <biological_entity id="o22377" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs with numerous small resinous glands.</text>
      <biological_entity id="o22378" name="twig" name_original="twigs" src="d0_s2" type="structure" />
      <biological_entity id="o22379" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
        <character is_modifier="true" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o22378" id="r3061" name="with" negation="false" src="d0_s2" to="o22379" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade ovate, rhombic-ovate, or suborbiculate-rhombic, 1–2.5 (–3.5) × 1–2 (–3) cm, base cuneate to truncate, margins coarsely serrate or dentate, apex acute;</text>
      <biological_entity id="o22380" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate-rhombic" value_original="suborbiculate-rhombic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate-rhombic" value_original="suborbiculate-rhombic" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22381" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate to truncate" value_original="cuneate to truncate" />
      </biological_entity>
      <biological_entity id="o22382" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o22383" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially moderately pubescent.</text>
      <biological_entity id="o22384" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially moderately" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Infructescences 1–1.5 (–2) × 0.4–0.8 cm;</text>
      <biological_entity id="o22385" name="infructescence" name_original="infructescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scales pubescent to glabrous, often ciliate, central lobe oblong or narrowly triangular, apex acute to obtuse, lateral lobes divergent and ascending, about equal in length but somewhat broader.</text>
      <biological_entity id="o22386" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s6" to="glabrous" />
        <character is_modifier="false" modifier="often" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="central" id="o22387" name="lobe" name_original="lobe" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o22388" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o22389" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="length" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="somewhat" name="width" src="d0_s6" value="broader" value_original="broader" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Samaras with wings about equal in diameter to body, broadest near summit, usually extended beyond body apically.</text>
      <biological_entity id="o22391" name="wing" name_original="wings" src="d0_s7" type="structure">
        <character constraint="to body" constraintid="o22392" is_modifier="false" name="diameter" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o22392" name="body" name_original="body" src="d0_s7" type="structure" />
      <biological_entity id="o22393" name="summit" name_original="summit" src="d0_s7" type="structure" />
      <biological_entity id="o22394" name="body" name_original="body" src="d0_s7" type="structure" />
      <relation from="o22390" id="r3062" name="with" negation="false" src="d0_s7" to="o22391" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 56.</text>
      <biological_entity id="o22390" name="samara" name_original="samaras" src="d0_s7" type="structure">
        <character constraint="near summit" constraintid="o22393" is_modifier="false" name="width" notes="" src="d0_s7" value="broadest" value_original="broadest" />
        <character constraint="beyond body" constraintid="o22394" is_modifier="false" modifier="usually" name="size" notes="" src="d0_s7" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22395" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Protected inland valleys</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="valleys" modifier="protected inland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Native, Greenland; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Native" establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9b.</number>
  <discussion>Betula pubescens subsp. tortuosa, which also occurs in Iceland, the mountains of Scandinavia, and arctic Europe, is native to southwestern Greenland; it has also been reported as well from Baffin Island, although I have not seen specimens. Based on pollen analyses (B. Fredskild 1991), this subspecies arrived in Greenland from Europe ca. 3500 years ago. Hybridization of this taxon with other birches in Greenland, Iceland, and Scandinavia has been studied by M. Sulkinoja (1990).</discussion>
  
</bio:treatment>