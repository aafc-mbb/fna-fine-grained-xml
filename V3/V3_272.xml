<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Henk van der Werff</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">26</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LAURACEAE</taxon_name>
    <taxon_hierarchy>family LAURACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10479</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs to tall trees, evergreen or rarely deciduous (Cassytha a parasitic vine with leaves reduced to scales), usually aromatic.</text>
      <biological_entity id="o25750" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="usually" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o25751" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character is_modifier="true" name="height" src="d0_s0" value="tall" value_original="tall" />
      </biological_entity>
      <relation from="o25750" id="r3470" name="to" negation="false" src="d0_s0" to="o25751" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate, rarely whorled or opposite, simple, without stipules, petiolate.</text>
      <biological_entity id="o25752" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o25753" name="stipule" name_original="stipules" src="d0_s1" type="structure" />
      <relation from="o25752" id="r3471" name="without" negation="false" src="d0_s1" to="o25753" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade: unlobed (unlobed or lobed in Sassafras), margins entire, occasionally with domatia (crevices or hollows serving as lodging for mites) in axils of main lateral-veins (in Cinnamomum).</text>
      <biological_entity id="o25754" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o25755" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25756" name="domatium" name_original="domatia" src="d0_s2" type="structure" />
      <biological_entity id="o25757" name="axil" name_original="axils" src="d0_s2" type="structure" />
      <biological_entity constraint="main" id="o25758" name="lateral-vein" name_original="lateral-veins" src="d0_s2" type="structure" />
      <relation from="o25755" id="r3472" modifier="occasionally" name="with" negation="false" src="d0_s2" to="o25756" />
      <relation from="o25756" id="r3473" name="in" negation="false" src="d0_s2" to="o25757" />
      <relation from="o25757" id="r3474" name="part_of" negation="false" src="d0_s2" to="o25758" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences in axils of leaves or deciduous bracts, panicles (rarely heads), racemes, compound cymes, or pseudoumbels (spikes in Cassytha), sometimes enclosed by decussate bracts.</text>
      <biological_entity id="o25759" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o25760" name="axil" name_original="axils" src="d0_s3" type="structure" />
      <biological_entity id="o25761" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o25762" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="true" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o25763" name="panicle" name_original="panicles" src="d0_s3" type="structure" />
      <biological_entity id="o25764" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o25765" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o25766" name="pseudoumbel" name_original="pseudoumbels" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o25767" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="decussate" value_original="decussate" />
      </biological_entity>
      <relation from="o25759" id="r3475" name="in" negation="false" src="d0_s3" to="o25760" />
      <relation from="o25760" id="r3476" name="part_of" negation="false" src="d0_s3" to="o25761" />
      <relation from="o25760" id="r3477" name="part_of" negation="false" src="d0_s3" to="o25762" />
      <relation from="o25763" id="r3478" modifier="sometimes" name="enclosed by" negation="false" src="d0_s3" to="o25767" />
      <relation from="o25764" id="r3479" modifier="sometimes" name="enclosed by" negation="false" src="d0_s3" to="o25767" />
      <relation from="o25765" id="r3480" modifier="sometimes" name="enclosed by" negation="false" src="d0_s3" to="o25767" />
      <relation from="o25766" id="r3481" modifier="sometimes" name="enclosed by" negation="false" src="d0_s3" to="o25767" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers bisexual or unisexual, bisexual only, or staminate and pistillate on different plants, or staminate and bisexual on some plants, pistillate and bisexual on others;</text>
      <biological_entity id="o25768" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="only" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o25769" is_modifier="false" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o25770" is_modifier="false" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character constraint="on others" constraintid="o25771" is_modifier="false" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o25769" name="plant" name_original="plants" src="d0_s4" type="structure" />
      <biological_entity id="o25770" name="plant" name_original="plants" src="d0_s4" type="structure" />
      <biological_entity id="o25771" name="other" name_original="others" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>flowers usually yellow to greenish or white, rarely reddish;</text>
      <biological_entity id="o25772" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually yellow" name="coloration" src="d0_s5" to="greenish or white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s5" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>hypanthium well developed, resembling calyx-tube, tepals and stamens perigynous;</text>
      <biological_entity id="o25773" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s6" value="developed" value_original="developed" />
        <character is_modifier="false" name="position" src="d0_s6" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o25774" name="calyx-tube" name_original="calyx-tube" src="d0_s6" type="structure" />
      <biological_entity id="o25775" name="tepal" name_original="tepals" src="d0_s6" type="structure" />
      <biological_entity id="o25776" name="stamen" name_original="stamens" src="d0_s6" type="structure" />
      <relation from="o25773" id="r3482" name="resembling" negation="false" src="d0_s6" to="o25774" />
      <relation from="o25773" id="r3483" name="resembling" negation="false" src="d0_s6" to="o25775" />
      <relation from="o25773" id="r3484" name="resembling" negation="false" src="d0_s6" to="o25776" />
    </statement>
    <statement id="d0_s7">
      <text>tepals 6 (-9), in 2 (-3) whorls of 3, sepaloid, equal or rarely unequal, if unequal then usually outer 3 smaller than inner 3 (occasionally absent in Litsea);</text>
      <biological_entity id="o25777" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="9" />
        <character name="quantity" src="d0_s7" value="6" value_original="6" />
        <character constraint="than inner tepals" constraintid="o25780" is_modifier="false" name="size" src="d0_s7" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o25778" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o25779" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="sepaloid" value_original="sepaloid" />
        <character is_modifier="true" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character is_modifier="true" modifier="rarely" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="true" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="true" modifier="usually" name="position" src="d0_s7" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity constraint="inner" id="o25780" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <relation from="o25777" id="r3485" name="in" negation="false" src="d0_s7" to="o25778" />
      <relation from="o25778" id="r3486" name="part_of" negation="false" src="d0_s7" to="o25779" />
    </statement>
    <statement id="d0_s8">
      <text>stamens (3-) 9 (-12), in whorls of 3, but 1 or more whorls frequently staminodial or absent;</text>
      <biological_entity id="o25781" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s8" to="9" to_inclusive="false" />
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="12" />
        <character name="quantity" src="d0_s8" value="9" value_original="9" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o25782" name="whorl" name_original="whorls" src="d0_s8" type="structure" />
      <biological_entity id="o25783" name="staminodial" name_original="staminodial" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" modifier="frequently staminodial" name="position" src="d0_s8" value="staminodial" value_original="staminodial" />
      </biological_entity>
      <relation from="o25781" id="r3487" name="in" negation="false" src="d0_s8" to="o25782" />
      <relation from="o25782" id="r3488" name="part_of" negation="false" src="d0_s8" to="o25783" />
    </statement>
    <statement id="d0_s9">
      <text>stamens of 3d whorl with 2 glands near base;</text>
      <biological_entity id="o25784" name="stamen" name_original="stamens" src="d0_s9" type="structure" constraint="whorl" constraint_original="whorl; whorl" />
      <biological_entity id="o25785" name="whorl" name_original="whorl" src="d0_s9" type="structure" />
      <biological_entity id="o25786" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o25787" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o25784" id="r3489" name="part_of" negation="false" src="d0_s9" to="o25785" />
      <relation from="o25784" id="r3490" name="with" negation="false" src="d0_s9" to="o25786" />
      <relation from="o25786" id="r3491" name="near" negation="false" src="d0_s9" to="o25787" />
    </statement>
    <statement id="d0_s10">
      <text>anthers 2-locular or 4-locular, locules opening by valves;</text>
      <biological_entity id="o25788" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="4-locular" value_original="4-locular" />
      </biological_entity>
      <biological_entity id="o25789" name="locule" name_original="locules" src="d0_s10" type="structure" />
      <biological_entity id="o25790" name="valve" name_original="valves" src="d0_s10" type="structure" />
      <relation from="o25789" id="r3492" name="opening by" negation="false" src="d0_s10" to="o25790" />
    </statement>
    <statement id="d0_s11">
      <text>pistil 1, 1-carpellate;</text>
      <biological_entity id="o25791" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-carpellate" value_original="1-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary 1-locular;</text>
    </statement>
    <statement id="d0_s13">
      <text>placentation basal;</text>
      <biological_entity id="o25792" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="placentation" src="d0_s13" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovule 1;</text>
      <biological_entity id="o25793" name="ovule" name_original="ovule" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma subsessile, discoid or capitate.</text>
      <biological_entity id="o25794" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits drupes, drupe borne on pedicel with or without persistent tepals at base, or seated in ± deeply cupshaped receptacle (cupule), or enclosed in accrescent floral-tube.</text>
      <biological_entity constraint="fruits" id="o25795" name="drupe" name_original="drupes" src="d0_s16" type="structure" />
      <biological_entity id="o25796" name="drupe" name_original="drupe" src="d0_s16" type="structure">
        <character constraint="in receptacle" constraintid="o25800" is_modifier="false" name="location" notes="" src="d0_s16" value="seated" value_original="seated" />
      </biological_entity>
      <biological_entity id="o25797" name="pedicel" name_original="pedicel" src="d0_s16" type="structure" />
      <biological_entity id="o25798" name="tepal" name_original="tepals" src="d0_s16" type="structure">
        <character is_modifier="true" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o25799" name="base" name_original="base" src="d0_s16" type="structure" />
      <biological_entity id="o25800" name="receptacle" name_original="receptacle" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="more or less deeply" name="shape" src="d0_s16" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
      <biological_entity id="o25801" name="floral-tube" name_original="floral-tube" src="d0_s16" type="structure">
        <character is_modifier="true" name="size" src="d0_s16" value="accrescent" value_original="accrescent" />
      </biological_entity>
      <relation from="o25796" id="r3493" name="borne on" negation="false" src="d0_s16" to="o25797" />
      <relation from="o25796" id="r3494" name="with or without" negation="false" src="d0_s16" to="o25798" />
      <relation from="o25798" id="r3495" name="at" negation="false" src="d0_s16" to="o25799" />
      <relation from="o25796" id="r3496" name="enclosed in" negation="false" src="d0_s16" to="o25801" />
    </statement>
    <statement id="d0_s17">
      <text>Seed 1;</text>
      <biological_entity id="o25802" name="seed" name_original="seed" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endosperm absent.</text>
      <biological_entity id="o25803" name="endosperm" name_original="endosperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pantropical, a few species also in subtropical and temperate regions</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pantropical" establishment_means="native" />
        <character name="distribution" value="a few species also in subtropical and temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Laurel Family</other_name>
  <discussion>Genera ca. 50, species 2000-3000 (9 genera, 13 species in the flora).</discussion>
  <discussion>Cassytha is sometimes placed in its own family, Cassythaceae; it is here retained in Lauraceae.</discussion>
  <references>
    <reference>Wood, C. E. Jr. 1958. The genera of the woody Ranales in the southeastern United States. J. Arnold Arbor. 39: 296-346.</reference>
  </references>
  <key>
    <key_head>Key to Genera Based on Flowering Material</key_head>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Parasitic vines, leaves reduced to minute scales; stems pale green to yellow-green or orange, twining.</description>
      <determination>9 Cassytha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs or trees, leafy; stem various in color but not orange, not twinning.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants deciduous; flowers appearing before or with new leaves.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants evergreen; flowers appearing when leaves mature.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers in racemes or panicles; leaf blade often lobed.</description>
      <determination>3 Sassafras</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers in pseudoumbels; leaf blade always unlobed.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Anthers 2-locular.</description>
      <determination>1 Lindera</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Anthers 4-locular.</description>
      <determination>2 Listea</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers in pseudoumbels.</description>
      <determination>4 Umbellularia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers in panicles or compound cymes.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stamens 3, anthers 2-locular.</description>
      <determination>6 Licaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stamens 9, anthers 4-locular.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Outer 3 tepals shorter than inner 3.</description>
      <determination>8 Persea</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Tepals equal.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade pinnately veined, domatia absent; terminal bud not covered by imbricate scales.</description>
      <determination>7 Nectandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade with (1-)3 primary veins, pubescent domatia in axils of main lateral veins; terminal bud covered by imbricate scales, young twigs with clusters of scars from fallen scales.</description>
      <determination>5 Cinnamomum</determination>
    </key_statement>
  </key>
  <key>
    <key_head>Key to Genera Based on Fruiting Material</key_head>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Parasitic vines, leaves reduced to minute scales.</description>
      <determination>9 Cassytha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs or trees, leafy.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade usually lobed (often unlobed).</description>
      <determination>3 Sassafras</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade always unlobed.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Tepals persistent at base of fruit; cupule absent.</description>
      <determination>8 Persea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Tepals deciduous; small cupule present.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cupule usuallly double-rimmed.</description>
      <determination>6 Licaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cupule single-rimmed.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruit at maturity 2 cm or more in greatest dimension; California, Oregon</description>
      <determination>4 Umbellularia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruit at maturity less than 2 cm in greatest dimension; e of Rocky Mountains.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Infructescences umbellate or not branched, about 1 cm.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Infructescences paniculate, more than 4 cm.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blade 4 × 1.5 cm or less.</description>
      <determination>2 Listea</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blade 4 × 2 cm or more.</description>
      <determination>1 Lindera</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade pinnately veined, domatia absent; terminal bud not covered by imbricate scales.</description>
      <determination>7 Nectandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade with (1-)3 primary veins, pubescent domatia in axils of main lateral veins; terminal bud covered by imbricate scales, young twigs with clusters of scars from fallen scales.</description>
      <determination>5 Cinnamomum</determination>
    </key_statement>
  </key>
</bio:treatment>