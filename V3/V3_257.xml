<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">532</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">CORYLOIDEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">carpinus</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">caroliniana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>236. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily coryloideae;genus carpinus;species caroliniana;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500311</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carpinus</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">americana</taxon_name>
    <taxon_hierarchy>genus Carpinus;species americana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 12 m;</text>
      <biological_entity id="o20310" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="12" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks short, often crooked, longitudinally or transversely fluted, crowns spreading.</text>
      <biological_entity id="o20311" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" modifier="often" name="course_or_orientation" src="d0_s1" value="crooked" value_original="crooked" />
        <character is_modifier="false" modifier="longitudinally; transversely" name="shape" src="d0_s1" value="fluted" value_original="fluted" />
      </biological_entity>
      <biological_entity id="o20312" name="crown" name_original="crowns" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bark gray, smooth to somewhat roughened.</text>
      <biological_entity id="o20313" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
        <character is_modifier="false" name="relief" src="d0_s2" value="smooth to somewhat" value_original="smooth to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="relief_or_texture" src="d0_s2" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Wood whitish, extremely hard, heavy.</text>
      <biological_entity id="o20314" name="wood" name_original="wood" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="extremely" name="texture" src="d0_s3" value="hard" value_original="hard" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Winter buds containing inflorescences squarish in cross-section, somewhat divergent, 3–4 mm.</text>
      <biological_entity id="o20315" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="true" name="season" src="d0_s4" value="winter" value_original="winter" />
        <character constraint="in cross-section" constraintid="o20317" is_modifier="false" name="shape" src="d0_s4" value="squarish" value_original="squarish" />
        <character is_modifier="false" modifier="somewhat" name="arrangement" notes="" src="d0_s4" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20316" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o20317" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <relation from="o20315" id="r2741" name="containing" negation="false" src="d0_s4" to="o20316" />
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade ovate to elliptic, 3–12 × 3–6 cm, margins doubly serrate, teeth typically obtuse and evenly arranged, primary teeth often not much longer than secondary;</text>
      <biological_entity id="o20318" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="elliptic" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20319" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o20320" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="typically" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity constraint="primary" id="o20321" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character constraint="than secondary" constraintid="o20322" is_modifier="false" name="length_or_size" src="d0_s5" value="often not much longer" value_original="often not much longer" />
      </biological_entity>
      <biological_entity id="o20322" name="secondary" name_original="secondary" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially slightly to moderately pubescent, especially on major veins, with or without conspicuous dark glands.</text>
      <biological_entity id="o20323" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly to moderately" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20324" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="major" value_original="major" />
      </biological_entity>
      <biological_entity id="o20325" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="dark" value_original="dark" />
      </biological_entity>
      <relation from="o20323" id="r2742" modifier="especially" name="on" negation="false" src="d0_s6" to="o20324" />
      <relation from="o20323" id="r2743" name="with or without" negation="false" src="d0_s6" to="o20325" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences: staminate inflorescences 2–6 cm;</text>
      <biological_entity id="o20326" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o20327" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistillate inflorescences 1–2.5 cm.</text>
      <biological_entity id="o20328" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o20329" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Infructescences 2.5–12 cm;</text>
      <biological_entity id="o20330" name="infructescence" name_original="infructescences" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s9" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts relatively uncrowded, 2–3.5 × 1.4–2.8 cm, lobes narrow, elongate, apex nearly acute, obtuse, or rounded, central lobe (1–) 2–3 cm.</text>
      <biological_entity id="o20331" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="relatively" name="arrangement_or_density" src="d0_s10" value="uncrowded" value_original="uncrowded" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s10" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="width" src="d0_s10" to="2.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20332" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o20333" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="central" id="o20334" name="lobe" name_original="lobe" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Carpinus caroliniana consists of two rather well-marked geographical races, treated here as subspecies. These hybridize or intergrade in a band extending from Long Island along the Atlantic coast through coastal Virginia and North Carolina, and then westward in northern South Carolina, Georgia, Alabama, Mississippi, and Louisiana. Plants with intermediate features are also found throughout the highlands of Missouri and Arkansas. J. J. Furlow (1987b) has described the variation of this complex in detail.</discussion>
  <discussion>Native Americans used Carpinus caroliniana medicinally to treat flux, navel yellowness, cloudy urine, Italian itch, consumption, diarrhea, and constipation, as an astringent, a tonic, and a wash, and to facilitate childbirth (D. E. Moerman 1986; no subspecies specified).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade narrowly ovate to oblong-ovate, 3–8.5(–12) cm, apex acute to obtuse; secondary teeth small and blunt; surfaces abaxially without small dark glands.</description>
      <determination>1a Carpinus caroliniana subsp. caroliniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade ovate to elliptic, mostly 8–12 cm, apex usually abruptly narrowing, nearly caudate, sometimes long, gradually tapered, long-acuminate; secondary teeth often almost as large as primary teeth, sharp-tipped; surfaces abaxially covered with tiny, dark brown glands</description>
      <determination>1b Carpinus caroliniana subsp. virginiana</determination>
    </key_statement>
  </key>
</bio:treatment>