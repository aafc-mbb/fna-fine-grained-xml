<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(Ewan) N. I. Malyutin" date="1987" rank="subsection">echinata</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1896" rank="species">hansenii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">hansenii</taxon_name>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection echinata;species hansenii;subspecies hansenii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500507</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">hesperium</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="variety">hansenii</taxon_name>
    <taxon_hierarchy>genus Delphinium;species hesperium;variety hansenii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hansenii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">arcuatum</taxon_name>
    <taxon_hierarchy>genus Delphinium;species hansenii;subspecies arcuatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 40-80 (-180) cm, base simple and/or long-puberulent.</text>
      <biological_entity id="o21519" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="180" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21520" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="long-puberulent" value_original="long-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mainly cauline;</text>
      <biological_entity id="o21521" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o21522" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mainly" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal leaves usually absent at anthesis;</text>
      <biological_entity constraint="basal" id="o21523" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves 3 or more, gradually smaller than basal leaves.</text>
      <biological_entity constraint="cauline" id="o21524" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" unit="or more" value="3" value_original="3" />
        <character constraint="than basal leaves" constraintid="o21525" is_modifier="false" name="size" src="d0_s3" value="gradually smaller" value_original="gradually smaller" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21525" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences (2-) 6-11 flowers per 5 cm, dense or less commonly open.</text>
      <biological_entity id="o21526" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="11" />
      </biological_entity>
      <biological_entity id="o21527" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="per 5 cm" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="less commonly" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals dark blue-purple to white or pink, lateral sepals 7-10 (-13) mm, spurs 8-13 mm. 2n = 16, 32.</text>
      <biological_entity id="o21528" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o21529" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="dark blue-purple" name="coloration" src="d0_s5" to="white or pink" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21530" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="13" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21531" name="spur" name_original="spurs" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21532" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="16" value_original="16" />
        <character name="quantity" src="d0_s5" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring(-early summer).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="flowering time" char_type="atypical_range" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open oak woods, grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open oak woods" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42a.</number>
  <other_name type="common_name">Hansen's larkspur</other_name>
  <discussion>Delphinium hansenii subsp. hansenii produces natural hybrids with D. gypsophilum, D. hesperium, and D. variegatum.</discussion>
  
</bio:treatment>