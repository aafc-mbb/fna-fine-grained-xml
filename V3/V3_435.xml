<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="Poiret in J. Lamarck et al." date="1810" rank="species">multifida</taxon_name>
    <taxon_name authority="(A. Nelson) B. E. Dutton &amp; Keener" date="1994" rank="variety">stylosa</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 84. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species multifida;variety stylosa</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500068</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">stylosa</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>42: 52. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Anemone;species stylosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 10-15 cm.</text>
      <biological_entity id="o18783" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves abaxially sparsely long-pilose, adaxially glabrous or nearly glabrous.</text>
      <biological_entity constraint="basal" id="o18784" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s1" value="long-pilose" value_original="long-pilose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: flowers solitary;</text>
      <biological_entity id="o18785" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o18786" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>peduncle sparsely long-pilose;</text>
      <biological_entity id="o18787" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o18788" name="peduncle" name_original="peduncle" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="long-pilose" value_original="long-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>involucral-bracts 1-tiered, abaxially sparsely long-pilose, adaxially glabrous or nearly glabrous.</text>
      <biological_entity id="o18789" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o18790" name="involucral-bract" name_original="involucral-bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="1-tiered" value_original="1-tiered" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s4" value="long-pilose" value_original="long-pilose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 6 (-8), purple to red or green to red, ovate to oblong, 8-10 mm.</text>
      <biological_entity id="o18791" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o18792" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="8" />
        <character name="quantity" src="d0_s5" value="6" value_original="6" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s5" to="red or green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Achenes: beak strongly hooked distally.</text>
      <biological_entity id="o18793" name="achene" name_original="achenes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>2n=32.</text>
      <biological_entity id="o18794" name="beak" name_original="beak" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly; distally" name="shape" src="d0_s6" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18795" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jul–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jul-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2700-3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="2700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14d.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>