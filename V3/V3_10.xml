<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce A. Ford</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">NIGELLA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 534. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 238. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus NIGELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin niger, black, and ella, diminutive; pertaining to seeds</other_info_on_name>
    <other_info_on_name type="fna_id">122301</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, from taproots.</text>
      <biological_entity id="o3742" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o3743" name="taproot" name_original="taproots" src="d0_s0" type="structure" />
      <relation from="o3742" id="r521" name="from" negation="false" src="d0_s0" to="o3743" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, petiolate or distal leaves sessile;</text>
      <biological_entity id="o3744" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3745" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves alternate.</text>
      <biological_entity constraint="cauline" id="o3746" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade 2-3-pinnately dissected, segments linear, threadlike [ovate or oblong or sometimes undivided, short], margins entire.</text>
      <biological_entity id="o3747" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="2-3-pinnately" name="shape" src="d0_s3" value="dissected" value_original="dissected" />
      </biological_entity>
      <biological_entity id="o3748" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="thread-like" value_original="threadlike" />
      </biological_entity>
      <biological_entity id="o3749" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal or axillary, flowers solitary;</text>
      <biological_entity id="o3750" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o3751" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>involucres present [absent], involucral-bracts 5-6, finely pinnately dissected, closely subtending flower.</text>
      <biological_entity id="o3752" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o3753" name="involucral-bract" name_original="involucral-bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="false" modifier="finely pinnately" name="shape" src="d0_s5" value="dissected" value_original="dissected" />
      </biological_entity>
      <biological_entity id="o3754" name="flower" name_original="flower" src="d0_s5" type="structure" />
      <relation from="o3753" id="r522" name="closely subtending" negation="false" src="d0_s5" to="o3754" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o3755" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals persistent in fruit, 5-25, blue to white or pink [yellowish white or green], plane, ovate, clawed [not clawed], 8-25 mm, apex acuminate;</text>
      <biological_entity id="o3756" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o3757" is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="blue" name="coloration" notes="" src="d0_s7" to="white or pink" />
        <character is_modifier="false" name="shape" src="d0_s7" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3757" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="25" />
      </biological_entity>
      <biological_entity id="o3758" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals (0-) 5-10, distinct, lead-colored, hooded, obovate, 2-labiate, 2-5 mm;</text>
      <biological_entity id="o3759" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="lead-colored" value_original="lead-colored" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-labiate" value_original="2-labiate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary apical;</text>
      <biological_entity id="o3760" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="apical" value_original="apical" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 15-75;</text>
      <biological_entity id="o3761" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s10" to="75" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments filiform;</text>
      <biological_entity id="o3762" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>staminodes absent between stamens and pistils;</text>
      <biological_entity id="o3763" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character constraint="between stamens, pistils" constraintid="o3764, o3765" is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o3764" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o3765" name="pistil" name_original="pistils" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>pistil compound [carpels connate proximally], [2-] 5-10-carpellate;</text>
      <biological_entity id="o3766" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="[2-]5-10-carpellate" value_original="[2-]5-10-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovules 25-100;</text>
      <biological_entity id="o3767" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s14" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style present.</text>
      <biological_entity id="o3768" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsular [partially connate follicles], sessile, inflated-spheric [not inflated], sides not prominently veined;</text>
      <biological_entity id="o3769" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s16" value="inflated-spheric" value_original="inflated-spheric" />
      </biological_entity>
      <biological_entity id="o3770" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not prominently" name="architecture" src="d0_s16" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>beak terminal, straight, 13-20 mm.</text>
      <biological_entity id="o3771" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s17" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds black, broadly obovate, reticulate, with raised ridges.</text>
      <biological_entity id="o3773" name="ridge" name_original="ridges" src="d0_s18" type="structure" />
      <relation from="o3772" id="r523" name="raised" negation="false" src="d0_s18" to="o3773" />
    </statement>
    <statement id="d0_s19">
      <text>x=6.</text>
      <biological_entity id="o3772" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="black" value_original="black" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s18" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s18" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o3774" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, s Europe, sw, c Asia, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="sw" establishment_means="native" />
        <character name="distribution" value="c Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Love-in-a-mist</other_name>
  <discussion>Species ca. 20 (1 in the flora).</discussion>
  <references>
    <reference>Mitchell, R. S. and J. K. Dean. 1982. Ranunculaceae (Crowfoot Family) of New York State. Bull. New York State Mus. Sci. Serv. 446.</reference>
    <reference>Zohary, M. 1983. The genus Nigella (Ranunculaceae)–A taxonomic revision. Pl. Syst. Evol. 142: 71-107.</reference>
  </references>
  
</bio:treatment>