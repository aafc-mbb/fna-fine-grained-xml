<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(Ewan) N. I. Malyutin" date="1987" rank="subsection">echinata</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1896" rank="species">hansenii</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 94. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection echinata;species hansenii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500505</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">hesperium</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="variety">hansenii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.</publication_title>
      <place_in_publication>3: 304. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species hesperium;variety hansenii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (25-) 40-80 (-180) cm;</text>
      <biological_entity id="o23178" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="180" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base usually reddish, pubescent.</text>
      <biological_entity id="o23179" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline and basal;</text>
      <biological_entity id="o23180" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 0-5 at anthesis;</text>
      <biological_entity constraint="basal" id="o23181" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 2-8 at anthesis;</text>
      <biological_entity constraint="cauline" id="o23182" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="2" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5-8 cm, petioles of proximal leaves long-pubescent.</text>
      <biological_entity id="o23183" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23184" name="petiole" name_original="petioles" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="long-pubescent" value_original="long-pubescent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23185" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o23184" id="r3178" name="part_of" negation="false" src="d0_s5" to="o23185" />
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade pentagonal, 1.5-5 × 2.5-8 cm, long-pubescent, especially abaxially;</text>
      <biological_entity id="o23186" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pentagonal" value_original="pentagonal" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s6" to="8" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="long-pubescent" value_original="long-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 0-18, width 4-20 mm (basal), 2-9 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o23187" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="18" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences (9-) 15-40 (-160) -flowered, dense to open;</text>
      <biological_entity id="o23188" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="(9-)15-40(-160)-flowered" value_original="(9-)15-40(-160)-flowered" />
        <character is_modifier="false" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel 0.3-2.5 (-6) cm, puberulent;</text>
      <biological_entity id="o23189" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 1-5 (-8) mm from flowers, green, sometimes white-margined, linear-lanceolate, 2-6 (-8) mm, puberulent.</text>
      <biological_entity id="o23190" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" constraint="from flowers" constraintid="o23191" from="1" from_unit="mm" name="location" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s10" value="white-margined" value_original="white-margined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o23191" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals violet to white, ± puberulent, lateral sepals spreading to forward pointing, 7-10 (-13) × 3-6 (-8) mm, spurs gently upcurved, ascending 0-30° above horizontal, (6-) 9-13 (-16) mm;</text>
      <biological_entity id="o23192" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o23193" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="violet" name="coloration" src="d0_s11" to="white" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o23194" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading to forward" value_original="spreading to forward" />
        <character is_modifier="false" modifier="forward" name="orientation" src="d0_s11" value="pointing" value_original="pointing" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23195" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="gently" name="orientation" src="d0_s11" value="upcurved" value_original="upcurved" />
        <character constraint="above horizontal" is_modifier="false" modifier="0-30°" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="16" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades elevated, ± exposing stamens, 3-7 mm, cleft 1-2 (-4) mm;</text>
      <biological_entity id="o23196" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o23197" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="3" from_unit="mm" modifier="more or less" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23198" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <relation from="o23197" id="r3179" modifier="more or less" name="exposing" negation="false" src="d0_s12" to="o23198" />
    </statement>
    <statement id="d0_s13">
      <text>hairs centered, densest on inner lobes near base of cleft, white.</text>
      <biological_entity id="o23199" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o23200" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="centered" value_original="centered" />
        <character constraint="on inner lobes" constraintid="o23201" is_modifier="false" name="density" src="d0_s13" value="densest" value_original="densest" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="inner" id="o23201" name="lobe" name_original="lobes" src="d0_s13" type="structure" />
      <biological_entity id="o23202" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o23201" id="r3180" name="near" negation="false" src="d0_s13" to="o23202" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 8-20 mm, 2.2-4 times longer than wide, glabrous.</text>
      <biological_entity id="o23203" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="2.2-4" value_original="2.2-4" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds echinate, appearing fuzzy to naked eye;</text>
      <biological_entity id="o23204" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="echinate" value_original="echinate" />
      </biological_entity>
      <biological_entity id="o23205" name="eye" name_original="eye" src="d0_s15" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s15" value="fuzzy" value_original="fuzzy" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="naked" value_original="naked" />
      </biological_entity>
      <relation from="o23204" id="r3181" name="appearing" negation="false" src="d0_s15" to="o23205" />
    </statement>
    <statement id="d0_s16">
      <text>seed-coat cells with margins straight, surfaces sparsely pustulate.</text>
      <biological_entity constraint="seed-coat" id="o23206" name="cell" name_original="cells" src="d0_s16" type="structure" />
      <biological_entity id="o23207" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o23208" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sparsely" name="relief" src="d0_s16" value="pustulate" value_original="pustulate" />
      </biological_entity>
      <relation from="o23206" id="r3182" name="with" negation="false" src="d0_s16" to="o23207" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42.</number>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <discussion>Although Delphinium hansenii has often been confused with D. hesperium, seeds will instantly allow identification. Seeds of Delphinium hansenii are, as far as known, unique, bearing numerous, elongate, prismlike raised structures (extensions of single cells or small groups of cells) over the entire seed coat. If seeds are absent, larger flowers, more open inflorescences (except in D. hesperium subsp. cuyamacae), and general absence of pubescence of long hairs in D. hesperium are apparent upon comparison of the two species. Separating D. hansenii from D. variegatum may also be difficult. Again, seeds leave no doubt. In addition, smaller flowers and greater number of flowers per plant of D. hansenii should serve to distinguish D. hansenii from D. variegatum. White-flowered D. hansenii has been confused with D. gypsophilum and with D. hesperium subsp. pallescens. Other than seeds, pubescence of long hairs and smaller flowers present in D. hansenii and absent in the others will distinguish them.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals reddish purple to dark maroon.</description>
      <determination>42c Delphinium hansenii subsp. ewanianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals dark blue-purple to white.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves primarily basal (although leaves may be dry at anthesis and thus lost in herbarium specimens, petiole base will be present); cauline leaves usually fewer than 3; sepals bright blue to white.</description>
      <determination>42b Delphinium hansenii subsp. kernense</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves primarily cauline, basal leaves usually absent at anthesis; cauline leaves 3 or more; sepals dark blue-purple to white or pink.</description>
      <determination>42a Delphinium hansenii subsp. hansenii</determination>
    </key_statement>
  </key>
</bio:treatment>