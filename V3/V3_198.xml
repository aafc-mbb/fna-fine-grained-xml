<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">clematis</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1895" rank="subgenus">viorna</taxon_name>
    <taxon_name authority="Kral" date="1987" rank="species">morefieldii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>74: 665. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus clematis;subgenus viorna;species morefieldii</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500399</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems viny, to 5 m, cobwebby-tomentose and pilose.</text>
      <biological_entity id="o11264" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="viny" value_original="viny" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="cobwebby-tomentose" value_original="cobwebby-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade 1-pinnate;</text>
      <biological_entity id="o11265" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="1-pinnate" value_original="1-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets 4-10 plus additional tendril-like terminal leaflet, narrowly to broadly ovate, unlobed or 2-3-lobed, 3.5-10 × 2-6.5 cm, thin, reticulate;</text>
      <biological_entity id="o11266" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="10" />
        <character is_modifier="false" modifier="narrowly to broadly; broadly" name="shape" notes="" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="2-3-lobed" value_original="2-3-lobed" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="6.5" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s2" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o11267" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="additional" value_original="additional" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="tendril-like" value_original="tendril-like" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>surfaces abaxially densely silky-pilose, not glaucous.</text>
      <biological_entity id="o11268" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s3" value="silky-pilose" value_original="silky-pilose" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, 1-5-flowered;</text>
      <biological_entity id="o11269" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-5-flowered" value_original="1-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts at or near base of peduncle/pedicel.</text>
      <biological_entity id="o11270" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <biological_entity id="o11271" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o11272" name="pedicel" name_original="peduncle/pedicel" src="d0_s5" type="structure" />
      <relation from="o11270" id="r1584" name="at or near" negation="false" src="d0_s5" to="o11271" />
      <relation from="o11271" id="r1585" name="part_of" negation="false" src="d0_s5" to="o11272" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers urn-shaped;</text>
      <biological_entity id="o11273" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="urn--shaped" value_original="urn--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals pinkish, suffused with green, oblong-lanceolate, 2-2.5 cm, margins not expanded, thick, not crispate, tomentose, tips acuminate, slightly spreading to short-reflexed, abaxially densely silky-pubescent.</text>
      <biological_entity id="o11274" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="suffused with green" value_original="suffused with green" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11275" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s7" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="crispate" value_original="crispate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o11276" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="slightly spreading" name="orientation" src="d0_s7" to="short-reflexed" />
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s7" value="silky-pubescent" value_original="silky-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes: bodies silky-pubescent;</text>
      <biological_entity id="o11277" name="achene" name_original="achenes" src="d0_s8" type="structure" />
      <biological_entity id="o11278" name="body" name_original="bodies" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="silky-pubescent" value_original="silky-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak 3-3.5 cm, plumose.</text>
      <biological_entity id="o11279" name="achene" name_original="achenes" src="d0_s9" type="structure" />
      <biological_entity id="o11280" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s9" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woods among limestone boulders</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woods" modifier="open" constraint="among limestone boulders" />
        <character name="habitat" value="limestone boulders" modifier="among" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Morefield's clematis</other_name>
  <other_name type="common_name">Morefield's leather-flower</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Clematis morefieldii is known only from limestone uplands east of Huntsville, Madison County, Alabama.</discussion>
  <discussion>From all variants of the closely related Clematis viorna, C. morefieldii differs in the cobwebby tomentose as well as villous pubescence of its stems, and in having bracts at or very near the base of the peduncle rather than well above the base.</discussion>
  
</bio:treatment>