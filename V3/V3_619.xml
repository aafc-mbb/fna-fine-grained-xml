<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">aristolochiaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1825" rank="genus">hexastylis</taxon_name>
    <taxon_name authority="H. L. Blomquist" date="1957" rank="species">contracta</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>8: 279. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aristolochiaceae;genus hexastylis;species contracta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500666</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Asarum</taxon_name>
    <taxon_name authority="(H. L. Blomquist) Barringer" date="unknown" rank="species">contractum</taxon_name>
    <taxon_hierarchy>genus Asarum;species contractum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes: internodes short, leaves crowded at rhizome apex.</text>
      <biological_entity id="o17298" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o17299" name="internode" name_original="internodes" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o17300" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="at rhizome apex" constraintid="o17301" is_modifier="false" name="arrangement" src="d0_s0" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity constraint="rhizome" id="o17301" name="apex" name_original="apex" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade rarely variegate, orbiculate-cordate;</text>
      <biological_entity id="o17302" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="orbiculate-cordate" value_original="orbiculate-cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>calyx-tube rhombic-ovoid, conspicuously tapered above middle, with prominent constrictions just above base and just below sinuses, 15-27 × 12-17 mm, inner surface with reticulations absent or poorly developed, ridges 0-1 mm high, lobes erect to spreading, 4-5 × 7-8 mm, adaxially puberulent;</text>
      <biological_entity id="o17303" name="calyx-tube" name_original="calyx-tube" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rhombic-ovoid" value_original="rhombic-ovoid" />
        <character constraint="above middle" constraintid="o17304" is_modifier="false" modifier="conspicuously" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" notes="" src="d0_s2" to="27" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" notes="" src="d0_s2" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17304" name="middle" name_original="middle" src="d0_s2" type="structure" />
      <biological_entity id="o17305" name="constriction" name_original="constrictions" src="d0_s2" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o17306" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o17307" name="sinuse" name_original="sinuses" src="d0_s2" type="structure" />
      <biological_entity constraint="inner" id="o17308" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="poorly" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o17309" name="reticulation" name_original="reticulations" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o17310" name="ridge" name_original="ridges" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="height" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17311" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="spreading" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o17303" id="r2373" name="with" negation="false" src="d0_s2" to="o17305" />
      <relation from="o17305" id="r2374" name="above" negation="false" src="d0_s2" to="o17306" />
      <relation from="o17303" id="r2375" name="just below" negation="false" src="d0_s2" to="o17307" />
      <relation from="o17308" id="r2376" name="with" negation="false" src="d0_s2" to="o17309" />
    </statement>
    <statement id="d0_s3">
      <text>stamen connective extending beyond pollen-sacs;</text>
      <biological_entity constraint="stamen" id="o17312" name="connective" name_original="connective" src="d0_s3" type="structure" />
      <biological_entity id="o17313" name="pollen-sac" name_original="pollen-sacs" src="d0_s3" type="structure" />
      <relation from="o17312" id="r2377" name="extending beyond" negation="false" src="d0_s3" to="o17313" />
    </statement>
    <statement id="d0_s4">
      <text>ovary ca. 1/3-inferior;</text>
      <biological_entity id="o17314" name="ovary" name_original="ovary" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>ovules 6 per locule;</text>
      <biological_entity id="o17315" name="ovule" name_original="ovules" src="d0_s5" type="structure">
        <character constraint="per locule" constraintid="o17316" name="quantity" src="d0_s5" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o17316" name="locule" name_original="locule" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>styles 2-cleft halfway to stigma.</text>
      <biological_entity id="o17317" name="style" name_original="styles" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="2-cleft" value_original="2-cleft" />
        <character constraint="to stigma" constraintid="o17318" is_modifier="false" name="position" src="d0_s6" value="halfway" value_original="halfway" />
      </biological_entity>
      <biological_entity id="o17318" name="stigma" name_original="stigma" src="d0_s6" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (May–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="May-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acid soils in deciduous forests, with Kalmia and Rhododendron</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acid soils" constraint="in deciduous forests" />
        <character name="habitat" value="deciduous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ky., N.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>The distribution of Hexastylis contracta is unique; it is disjunct between the Cumberland Plateau of central Tennessee and Kentucky and the southeastern Blue Ridge Province of western North Carolina.</discussion>
  
</bio:treatment>