<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">clematis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">clematis</taxon_name>
    <taxon_name authority="Linnaeus" date="1755" rank="species">virginiana</taxon_name>
    <place_of_publication>
      <publication_title>Cent. Pl. I,</publication_title>
      <place_in_publication>15. 1755</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus clematis;subgenus clematis;species virginiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500416</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clematis</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_hierarchy>genus Clematis;species canadensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clematis</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">holosericea</taxon_name>
    <taxon_hierarchy>genus Clematis;species holosericea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clematis</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">missouriensis</taxon_name>
    <taxon_hierarchy>genus Clematis;species missouriensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clematis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virginiana</taxon_name>
    <taxon_name authority="(Rydberg) E. J. Palmer &amp; Steyermark" date="unknown" rank="variety">missouriensis</taxon_name>
    <taxon_hierarchy>genus Clematis;species virginiana;variety missouriensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems climbing, 2-7 m.</text>
      <biological_entity id="o21499" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade 3-foliolate;</text>
      <biological_entity id="o21500" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets ovate to lanceolate, 3.5-9 × 1.5-7.5 cm, margins coarsely toothed to entire;</text>
      <biological_entity id="o21501" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="7.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21502" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="coarsely toothed" name="shape" src="d0_s2" to="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>surfaces abaxially sparsely to densely pilose, adaxially glabrate.</text>
      <biological_entity id="o21503" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, 3-many-flowered simple or compound cymes.</text>
      <biological_entity id="o21504" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-many-flowered" value_original="3-many-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o21505" name="cyme" name_original="cymes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers unisexual;</text>
      <biological_entity id="o21506" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pedicel slender, 1-2 cm;</text>
      <biological_entity id="o21507" name="pedicel" name_original="pedicel" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals widespreading, not recurved, white to cream, elliptic or nearly oblong to oblanceolate, 6-14 mm, abaxially densely white-hairy, adaxially sparsely white-hairy;</text>
      <biological_entity id="o21508" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="widespreading" value_original="widespreading" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="cream" />
        <character char_type="range_value" from="nearly oblong" name="shape" src="d0_s7" to="oblanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s7" value="white-hairy" value_original="white-hairy" />
        <character is_modifier="false" modifier="adaxially sparsely" name="pubescence" src="d0_s7" value="white-hairy" value_original="white-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens ca. 30-50+;</text>
      <biological_entity id="o21509" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s8" to="50" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments glabrous;</text>
      <biological_entity id="o21510" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodes absent or fewer than stamens;</text>
      <biological_entity id="o21511" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character constraint="than stamens" constraintid="o21512" is_modifier="false" name="quantity" src="d0_s10" value="absent or fewer" value_original="absent or fewer" />
        <character constraint="than stamens" constraintid="o21512" is_modifier="false" name="quantity" src="d0_s10" value="absent or fewer" value_original="absent or fewer" />
      </biological_entity>
      <biological_entity id="o21512" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pistils 40-70;</text>
      <biological_entity id="o21513" name="pistil" name_original="pistils" src="d0_s11" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s11" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>beak nearly equaling sepals.</text>
      <biological_entity id="o21514" name="beak" name_original="beak" src="d0_s12" type="structure" />
      <biological_entity id="o21515" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="nearly" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes ovate, 2.5-3.5 × l. 5 mm, conspicously rimmed, sparsely short-hairy;</text>
      <biological_entity id="o21516" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" modifier="conspicously" name="relief" src="d0_s13" value="rimmed" value_original="rimmed" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak 2.5-5 cm. 2n = 16.</text>
      <biological_entity id="o21517" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s14" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21518" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Streamsides, wet roadsides, fencerows, and other moist, disturbed, wooded or open sites, locally abundant</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="wet roadsides" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="other moist" />
        <character name="habitat" value="disturbed" />
        <character name="habitat" value="wooded" />
        <character name="habitat" value="open sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Virgin's-bower</other_name>
  <other_name type="common_name">clématite de Virginie</other_name>
  <discussion>Clematis virginiana is the most frequent and widespread virgin's-bower in eastern North America. It is easily distinguished from C. catesbyana by the presence of three ovate leaflets.</discussion>
  <discussion>Native Americans used infusions prepared from the roots of Clematis virginiana medicinally to treat kidney ailments, and mixed them with milkweed to heal backaches and venereal sores. Decoctions of stems were ingested to induce strange dreams. In addition, the plant was used as an ingredient in green corn medicine (D. E. Moerman 1986).</discussion>
  
</bio:treatment>