<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="M. J. Warnock" date="1995" rank="subsection">multiplex</taxon_name>
    <taxon_name authority="(Ewan) C. L. Hitchcock in C. L. Hitchcock et al." date="1964" rank="species">multiplex</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al.,  Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>2: 357. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection multiplex;species multiplex;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500521</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Piper forma multiplex Ewan" date="unknown" rank="species">cyanoreios</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Colorado Stud., Ser. D, Phys. Sci.</publication_title>
      <place_in_publication>2: 163. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species cyanoreios;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (40-) 80-130 (-180) cm;</text>
      <biological_entity id="o28415" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="130" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="180" to_unit="cm" />
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s0" to="130" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base usually green, glabrous.</text>
      <biological_entity id="o28416" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly on proximal 1/3 of stem, usually absent from proximal 1/5 stem at anthesis;</text>
      <biological_entity id="o28417" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="from stem" constraintid="o28420" is_modifier="false" modifier="usually" name="presence" notes="" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o28418" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <biological_entity id="o28419" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity id="o28420" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="true" name="quantity" src="d0_s2" value="1/5" value_original="1/5" />
      </biological_entity>
      <relation from="o28417" id="r3805" name="on" negation="false" src="d0_s2" to="o28418" />
      <relation from="o28418" id="r3806" name="part_of" negation="false" src="d0_s2" to="o28419" />
    </statement>
    <statement id="d0_s3">
      <text>basal leaves 0-4 at anthesis;</text>
      <biological_entity constraint="basal" id="o28421" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="0" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 10-20 at anthesis;</text>
      <biological_entity constraint="cauline" id="o28422" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="10" name="quantity" src="d0_s4" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5-15 cm.</text>
      <biological_entity id="o28423" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round on proximal stem, ± cuneate on distal stem, 1.5-9 × 2-14 cm, glabrous;</text>
      <biological_entity id="o28424" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character constraint="on proximal stem" constraintid="o28425" is_modifier="false" name="shape" src="d0_s6" value="round" value_original="round" />
        <character constraint="on distal stem" constraintid="o28426" is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" notes="" src="d0_s6" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" notes="" src="d0_s6" to="14" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o28425" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o28426" name="stem" name_original="stem" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 3-15, width 15-30 mm (basal), 1-20 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o28427" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s7" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 25-50-flowered, ± dense;</text>
      <biological_entity id="o28428" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="25-50-flowered" value_original="25-50-flowered" />
        <character is_modifier="false" modifier="more or less" name="density" src="d0_s8" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel 0.5-2 cm, glandular-puberulent;</text>
      <biological_entity id="o28429" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 4-5 mm from flowers, green to blue, lanceolate-linear, 3-7 mm, glandular-puberulent.</text>
      <biological_entity id="o28430" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o28431" from="4" from_unit="mm" name="location" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s10" to="blue" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate-linear" value_original="lanceolate-linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o28431" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals dark blue, puberulent, lateral sepals forward pointing to spreading, 12-15 × 5-7 mm, spurs straight, within 20° of horizontal, 12-16 mm;</text>
      <biological_entity id="o28432" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o28433" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark blue" value_original="dark blue" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o28434" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="pointing" name="orientation" src="d0_s11" to="spreading" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28435" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character constraint="of horizontal" name="degree" src="d0_s11" value="20°" value_original="20°" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades slightly elevated, exposing stamens, 6-8 mm, clefts 2-3 mm;</text>
      <biological_entity id="o28436" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o28437" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28438" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o28439" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o28437" id="r3807" name="exposing" negation="false" src="d0_s12" to="o28438" />
    </statement>
    <statement id="d0_s13">
      <text>hairs centered, mostly near base of cleft, white.</text>
      <biological_entity id="o28440" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o28441" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="centered" value_original="centered" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o28442" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o28441" id="r3808" modifier="mostly" name="near" negation="false" src="d0_s13" to="o28442" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 10-15 mm, 3-4 times longer than wide, glandular-puberulent.</text>
      <biological_entity id="o28443" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="15" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="3-4" value_original="3-4" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds narrowly wing-margined;</text>
      <biological_entity id="o28444" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s15" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>seed-coat cells with surfaces somewhat roughened.</text>
      <biological_entity id="o28446" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="somewhat" name="relief_or_texture" src="d0_s16" value="roughened" value_original="roughened" />
      </biological_entity>
      <relation from="o28445" id="r3809" name="with" negation="false" src="d0_s16" to="o28446" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 16.</text>
      <biological_entity constraint="seed-coat" id="o28445" name="cell" name_original="cells" src="d0_s16" type="structure" />
      <biological_entity constraint="2n" id="o28447" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky streambeds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky streambeds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <discussion>Delphinium multiplex hybridizes with D. glaucum and D. distichum.</discussion>
  
</bio:treatment>