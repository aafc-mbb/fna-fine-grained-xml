<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">urticaceae</taxon_name>
    <taxon_name authority="Jacquin" date="1760" rank="genus">BOEHMERIA</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Syst. Pl.,</publication_title>
      <place_in_publication>9. 1760</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family urticaceae;genus BOEHMERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">for G. R. Böhmer, German botanist</other_info_on_name>
    <other_info_on_name type="fna_id">104157</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, subshrubs, or shrubs, perennial, rhizomatous, without stinging hairs, sparsely to ± densely pubescent or tomentose with hooked, curved, or straight, nonstinging hairs on all parts of plant.</text>
      <biological_entity id="o8099" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" notes="" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character char_type="range_value" constraint="with hooked" from="sparsely" name="pubescence" notes="" src="d0_s0" to="more or less densely pubescent or tomentose" />
        <character is_modifier="false" name="course" src="d0_s0" value="curved" value_original="curved" />
        <character is_modifier="false" name="course" src="d0_s0" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s0" value="curved" value_original="curved" />
        <character is_modifier="false" name="course" src="d0_s0" value="straight" value_original="straight" />
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o8102" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="toxicity" src="d0_s0" value="stinging" value_original="stinging" />
      </biological_entity>
      <biological_entity id="o8103" name="hair" name_original="hairs" src="d0_s0" type="structure" />
      <biological_entity id="o8104" name="part" name_original="parts" src="d0_s0" type="structure" />
      <biological_entity id="o8105" name="plant" name_original="plant" src="d0_s0" type="structure" />
      <relation from="o8099" id="r1129" name="without" negation="false" src="d0_s0" to="o8102" />
      <relation from="o8103" id="r1130" name="on" negation="false" src="d0_s0" to="o8104" />
      <relation from="o8104" id="r1131" name="part_of" negation="false" src="d0_s0" to="o8105" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple to freely branched, erect.</text>
      <biological_entity id="o8106" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="simple" name="architecture" src="d0_s1" to="freely branched" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite or nearly opposite, or alternate;</text>
      <biological_entity id="o8107" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules present.</text>
      <biological_entity id="o8108" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades lanceolate to broadly ovate, base rounded to cordate, less often cuneate, margins dentate or serrate, apex acuminate;</text>
      <biological_entity id="o8109" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="broadly ovate" />
      </biological_entity>
      <biological_entity id="o8110" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="cordate" />
        <character is_modifier="false" modifier="less often" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o8111" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o8112" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cystoliths rounded.</text>
      <biological_entity id="o8113" name="cystolith" name_original="cystoliths" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, spikelike or paniculate.</text>
      <biological_entity id="o8114" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, staminate and pistillate flowers on same plant, rarely on different plants, in remote or crowded clusters in same or separate inflorescence;</text>
      <biological_entity id="o8115" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8116" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="remote" value_original="remote" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
        <character constraint="in inflorescence" constraintid="o8119" is_modifier="false" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o8117" name="plant" name_original="plant" src="d0_s7" type="structure" />
      <biological_entity id="o8118" name="plant" name_original="plants" src="d0_s7" type="structure" />
      <biological_entity id="o8119" name="inflorescence" name_original="inflorescence" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="separate" value_original="separate" />
      </biological_entity>
      <relation from="o8116" id="r1132" name="on" negation="false" src="d0_s7" to="o8117" />
      <relation from="o8116" id="r1133" modifier="rarely" name="on" negation="false" src="d0_s7" to="o8118" />
    </statement>
    <statement id="d0_s8">
      <text>bracts linear.</text>
      <biological_entity id="o8120" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate flowers: tepals 4;</text>
      <biological_entity id="o8121" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8122" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 4;</text>
      <biological_entity id="o8123" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8124" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillode globose.</text>
      <biological_entity id="o8125" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8126" name="pistillode" name_original="pistillode" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: tepals 4, connate, adnate to ovary;</text>
      <biological_entity id="o8127" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8128" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character constraint="to ovary" constraintid="o8129" is_modifier="false" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o8129" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>staminodes absent;</text>
      <biological_entity id="o8130" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8131" name="staminode" name_original="staminodes" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style persistent, elongate;</text>
      <biological_entity id="o8132" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8133" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s14" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma linear, straight or hooked.</text>
      <biological_entity id="o8134" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8135" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s15" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes sessile, laterally compressed, ovoid, nearly orbicular, or ellipsoid, tightly enclosed by persistent perianth.</text>
      <biological_entity id="o8137" name="perianth" name_original="perianth" src="d0_s16" type="structure">
        <character is_modifier="true" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o8136" id="r1134" modifier="tightly" name="enclosed by" negation="false" src="d0_s16" to="o8137" />
    </statement>
    <statement id="d0_s17">
      <text>x = 14.</text>
      <biological_entity id="o8136" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s16" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s16" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity constraint="x" id="o8138" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical and subtropical regions worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical and subtropical regions worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">False-nettle</other_name>
  <discussion>Species 50 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades abaxially glabrous, puberulent, or short-pilose, not white-tomentose; inflorescences spikelike.</description>
      <determination>1 Boehmeria cylindrica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades abaxially densely white-tomentose; inflorescences paniculate.</description>
      <determination>2 Boehmeria nivea</determination>
    </key_statement>
  </key>
</bio:treatment>