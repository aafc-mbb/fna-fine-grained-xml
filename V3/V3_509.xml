<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="Ewan" date="1936" rank="subsection">Subscaposa</taxon_name>
    <taxon_name authority="Ewan" date="1945" rank="species">lineapetalum</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Colorado Stud., Ser. D, Phys. Sci.</publication_title>
      <place_in_publication>2: 126. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection subscaposa;species lineapetalum;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500515</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Pritzel" date="unknown" rank="species">nuttallianum</taxon_name>
    <taxon_name authority="(Ewan) C. L. Hitchcock" date="unknown" rank="variety">lineapetalum</taxon_name>
    <taxon_hierarchy>genus Delphinium;species nuttallianum;variety lineapetalum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (15-) 30-60 cm;</text>
      <biological_entity id="o14083" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base usually reddish, glabrous to glaucous.</text>
      <biological_entity id="o14084" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s1" to="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o14085" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves absent at anthesis;</text>
      <biological_entity constraint="basal" id="o14086" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 2-5 at anthesis;</text>
      <biological_entity constraint="cauline" id="o14087" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="2" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.2-8 cm.</text>
      <biological_entity id="o14088" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-blade round, 1-5 × 2-6 cm, glabrous;</text>
      <biological_entity id="o14089" name="leaf-blade" name_original="leaf-blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="round" value_original="round" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate lobes 5-16, width 1-4 mm (basal), 0.5-3 mm (cauline).</text>
      <biological_entity constraint="ultimate" id="o14090" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="16" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences (3-) 9-24 (-40) -flowered, pyramidal;</text>
      <biological_entity id="o14091" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="(3-)9-24(-40)-flowered" value_original="(3-)9-24(-40)-flowered" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel spreading, 1-2.5 cm, glabrous or glandular-puberulent;</text>
      <biological_entity id="o14092" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles 4-7 mm from flowers, blue or green, linear, 1-3 mm, glabrous or glandular-puberulent.</text>
      <biological_entity id="o14093" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o14094" from="4" from_unit="mm" name="location" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o14094" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals light blue to lavender, nearly glabrous, lateral sepals reflexed, 10-13 × 3-5 mm, spurs straight to slightly decurved, nearly horizontal to ascending ca. 30°, 11-17 mm;</text>
      <biological_entity id="o14095" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14096" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="light blue" name="coloration" src="d0_s11" to="lavender" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o14097" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14098" name="spur" name_original="spurs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="decurved" value_original="decurved" />
        <character char_type="range_value" from="nearly horizontal" modifier="30°" name="orientation" src="d0_s11" to="ascending" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower petal blades elevated, exposing stamens, 3-5 mm, clefts 0.5-2 mm;</text>
      <biological_entity id="o14099" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="petal" id="o14100" name="blade" name_original="blades" src="d0_s12" type="structure" constraint_original="lower petal">
        <character is_modifier="false" name="prominence" src="d0_s12" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14101" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o14102" name="cleft" name_original="clefts" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o14100" id="r1957" name="exposing" negation="false" src="d0_s12" to="o14101" />
    </statement>
    <statement id="d0_s13">
      <text>hairs centered mostly on inner lobes near base of cleft, white.</text>
      <biological_entity id="o14103" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o14104" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character constraint="on inner lobes" constraintid="o14105" is_modifier="false" name="position" src="d0_s13" value="centered" value_original="centered" />
        <character is_modifier="false" modifier="of cleft" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="inner" id="o14105" name="lobe" name_original="lobes" src="d0_s13" type="structure" />
      <biological_entity id="o14106" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o14105" id="r1958" name="near" negation="false" src="d0_s13" to="o14106" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits 13-21 mm, 4-4.5 times longer than wide, puberulent to glabrous.</text>
      <biological_entity id="o14107" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s14" to="21" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s14" value="4-4.5" value_original="4-4.5" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s14" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds: seed-coat cells narrow, short, cell margins straight, surfaces smooth.</text>
      <biological_entity id="o14108" name="seed" name_original="seeds" src="d0_s15" type="structure" />
      <biological_entity constraint="seed-coat" id="o14109" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="cell" id="o14110" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o14111" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open pine woods, dry meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open pine woods" />
        <character name="habitat" value="dry meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>