<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Allan J. Bornstein</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">429</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Blume" date="unknown" rank="family">MYRICACEAE</taxon_name>
    <taxon_hierarchy>family MYRICACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10594</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, evergreen or deciduous, usually aromatic and resinous.</text>
      <biological_entity id="o5211" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="usually" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="usually" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots commonly with nitrogen-fixing nodules.</text>
      <biological_entity id="o5213" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity constraint="nitrogen-fixing" id="o5214" name="nodule" name_original="nodules" src="d0_s1" type="structure" />
      <relation from="o5213" id="r744" name="with" negation="false" src="d0_s1" to="o5214" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, simple or pinnatifid;</text>
      <biological_entity id="o5215" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules absent or present;</text>
      <biological_entity id="o5216" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present.</text>
      <biological_entity id="o5217" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaf-blade commonly with peltate, multicellular, glandular trichomes.</text>
      <biological_entity id="o5218" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure" />
      <biological_entity constraint="glandular" id="o5219" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="peltate" value_original="peltate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <relation from="o5218" id="r745" name="with" negation="false" src="d0_s5" to="o5219" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary catkins;</text>
      <biological_entity id="o5220" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="axillary" id="o5221" name="catkin" name_original="catkins" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o5222" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers usually unisexual, occasionally bisexual, staminate and pistillate flowers usually on different plants, occasionally on same plants;</text>
      <biological_entity id="o5223" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="occasionally" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5224" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5225" name="plant" name_original="plants" src="d0_s8" type="structure" />
      <biological_entity id="o5226" name="plant" name_original="plants" src="d0_s8" type="structure" />
      <relation from="o5224" id="r746" name="on" negation="false" src="d0_s8" to="o5225" />
      <relation from="o5224" id="r747" modifier="occasionally" name="on" negation="false" src="d0_s8" to="o5226" />
    </statement>
    <statement id="d0_s9">
      <text>perianth absent.</text>
      <biological_entity id="o5227" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers subtended by solitary bract;</text>
      <biological_entity id="o5228" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5229" name="bract" name_original="bract" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o5228" id="r748" name="subtended by" negation="false" src="d0_s10" to="o5229" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 2-14 (-22), hypogynous or ± epigynous;</text>
      <biological_entity id="o5230" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="22" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="14" />
        <character is_modifier="false" name="position" src="d0_s11" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s11" value="epigynous" value_original="epigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments filiform, distinct or basally connate;</text>
      <biological_entity id="o5231" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers dorsifixed, 2-locular, extrorsely dehiscent by longitudinal slits.</text>
      <biological_entity id="o5232" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s13" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="2-locular" value_original="2-locular" />
        <character constraint="by slits" constraintid="o5233" is_modifier="false" modifier="extrorsely" name="dehiscence" src="d0_s13" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o5233" name="slit" name_original="slits" src="d0_s13" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s13" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers subtended by solitary bract, bracteoles present or absent, usually 2-4 (-8);</text>
      <biological_entity id="o5234" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5235" name="bract" name_original="bract" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s14" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o5236" name="bracteole" name_original="bracteoles" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character char_type="range_value" from="4" from_inclusive="false" modifier="usually" name="atypical_quantity" src="d0_s14" to="8" />
        <character char_type="range_value" from="2" modifier="usually" name="quantity" src="d0_s14" to="4" />
      </biological_entity>
      <relation from="o5234" id="r749" name="subtended by" negation="false" src="d0_s14" to="o5235" />
    </statement>
    <statement id="d0_s15">
      <text>pistils 1, 2-carpellate, 1-locular;</text>
      <biological_entity id="o5237" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-carpellate" value_original="2-carpellate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 1, basal, erect;</text>
      <biological_entity id="o5238" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s16" value="basal" value_original="basal" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles, if present, short;</text>
      <biological_entity id="o5239" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas 2.</text>
      <biological_entity id="o5240" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits drupaceous or nutlike, smooth or often covered with warty protuberances, these commonly with waxy coating;</text>
      <biological_entity id="o5241" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="drupaceous" value_original="drupaceous" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="nutlike" value_original="nutlike" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s19" value="often covered with warty protuberances" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>fruits sometimes enclosed by persistent, accrescent bracts and bracteoles.</text>
      <biological_entity id="o5242" name="fruit" name_original="fruits" src="d0_s20" type="structure" />
      <biological_entity id="o5243" name="bract" name_original="bracts" src="d0_s20" type="structure">
        <character is_modifier="true" name="duration" src="d0_s20" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="size" src="d0_s20" value="accrescent" value_original="accrescent" />
      </biological_entity>
      <biological_entity id="o5244" name="bracteole" name_original="bracteoles" src="d0_s20" type="structure">
        <character is_modifier="true" name="duration" src="d0_s20" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="size" src="d0_s20" value="accrescent" value_original="accrescent" />
      </biological_entity>
      <relation from="o5242" id="r750" modifier="sometimes" name="enclosed by" negation="false" src="d0_s20" to="o5243" />
      <relation from="o5242" id="r751" modifier="sometimes" name="enclosed by" negation="false" src="d0_s20" to="o5244" />
    </statement>
    <statement id="d0_s21">
      <text>Seeds with little or no endosperm;</text>
      <biological_entity id="o5245" name="seed" name_original="seeds" src="d0_s21" type="structure" />
      <biological_entity id="o5246" name="endosperm" name_original="endosperm" src="d0_s21" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s21" value="no" value_original="no" />
      </biological_entity>
      <relation from="o5245" id="r752" name="with" negation="false" src="d0_s21" to="o5246" />
    </statement>
    <statement id="d0_s22">
      <text>embryo straight, with 2 planoconvex cotyledons.</text>
      <biological_entity id="o5247" name="embryo" name_original="embryo" src="d0_s22" type="structure">
        <character is_modifier="false" name="course" src="d0_s22" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o5248" name="cotyledon" name_original="cotyledons" src="d0_s22" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s22" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s22" value="planoconvex" value_original="planoconvex" />
      </biological_entity>
      <relation from="o5247" id="r753" name="with" negation="false" src="d0_s22" to="o5248" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Widespread in temperate and subtropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Widespread in temperate and subtropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Wax-myrtle Family</other_name>
  <discussion>Genera 2-4, species ca. 50 (2 genera, 8 species in the flora).</discussion>
  <discussion>Significant disagreement exists concerning the number of genera to be recognized in Myricaceae. Myrica in the broad sense is sometimes divided into three genera. Comptonia L'Héritier ex Aiton is often segregated on the basis of leaf type, presence of stipules, and the burlike fruits with 6-8 accrescent bracts and bracteoles. Morella Loureiro sometimes is elevated from its usual rank of subgenus to emphasize differences concerning position of the catkins, size of the staminate bracts, and appearance of the fruits (A. Chevalier 1901; J. R. Baird 1968). The real question is the appropriate rank at which recognition should be made (T. S. Elias 1971). I follow a traditional approach in recognizing just Myrica and Comptonia in North America.</discussion>
  <references>
    <reference>Baird, J. R. 1968. A Taxonomic Revision of the Plant Family Myricaceae of North America, North of Mexico. Ph.D. thesis. University of North Carolina.</reference>
    <reference>Elias, T. S. 1971. The genera of Myricaceae in the southeastern United States. J. Arnold Arbor. 52: 305-318.</reference>
    <reference>Sheffy, M. V. 1972. A Study of the Myricaceae from Eocene Sediments of Southeastern North America. Ph.D. thesis. Indiana University.</reference>
    <reference>Youngken, H. W. 1919. The comparative morphology, taxonomy and distribution of the Myricaceae of the eastern United States. Contr. Bot. Lab. Morris Arbor. Univ. Pennsylvania 4: 339-400.</reference>
    <reference>Chevalier, A. 1901. Monographie des Myricacées. Mém. Soc. Sci. Nat. Cherbourg 32: 85-340.</reference>
    <reference>Wilbur, R. L. 1994. The Myricaceae of the United States and Canada: Genera, subgenera, and series. Sida 16(1): 93-107.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves entire or serrate-denticulate, stipules absent; fruiting catkins short-cylindric; bracteoles 2-6, broadly ovate and equal to or shorter than fruits, or absent.</description>
      <determination>1 Myrica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves pinnatifid, stipules present; fruiting catkins globose-ovoid; bracteoles 2 at anthesis, linear-subulate, accrescent, developing 4-8 tertiary bracteoles, these much exceeding fruit.</description>
      <determination>2 Comptonia</determination>
    </key_statement>
  </key>
</bio:treatment>