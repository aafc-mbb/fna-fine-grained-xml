<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>A. Linn Bogle</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">414</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Bentham" date="unknown" rank="family">LEITNERIACEAE</taxon_name>
    <taxon_hierarchy>family LEITNERIACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10485</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees.</text>
      <biological_entity id="o3122" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Wood white to yellowish, soft, brittle, even-grained;</text>
      <biological_entity id="o3124" name="wood" name_original="wood" src="d0_s1" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s1" to="yellowish" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s1" value="soft" value_original="soft" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
        <character is_modifier="false" name="texture" src="d0_s1" value="even-grained" value_original="even-grained" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>secretory canals in outer pith;</text>
      <biological_entity constraint="secretory" id="o3125" name="canal" name_original="canals" src="d0_s2" type="structure" />
      <biological_entity constraint="outer" id="o3126" name="pith" name_original="pith" src="d0_s2" type="structure" />
      <relation from="o3125" id="r408" name="in" negation="false" src="d0_s2" to="o3126" />
    </statement>
    <statement id="d0_s3">
      <text>resin yellowish.</text>
      <biological_entity id="o3127" name="resin" name_original="resin" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves alternate, not aromatic;</text>
      <biological_entity id="o3128" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="not" name="odor" src="d0_s4" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules absent;</text>
      <biological_entity id="o3129" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole and veins with secretory canals.</text>
      <biological_entity id="o3130" name="petiole" name_original="petiole" src="d0_s6" type="structure" />
      <biological_entity id="o3131" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <biological_entity constraint="secretory" id="o3132" name="canal" name_original="canals" src="d0_s6" type="structure" />
      <relation from="o3130" id="r409" name="with" negation="false" src="d0_s6" to="o3132" />
      <relation from="o3131" id="r410" name="with" negation="false" src="d0_s6" to="o3132" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary catkins, erect or lax.</text>
      <biological_entity id="o3133" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o3134" name="catkin" name_original="catkins" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers unisexual, staminate and pistillate on different (rarely on same) plants, inconspicuous, sessile;</text>
      <biological_entity id="o3135" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o3136" is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o3136" name="plant" name_original="plants" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>perianth absent or of small, undifferentiated sepals, hypogynous;</text>
      <biological_entity id="o3137" name="perianth" name_original="perianth" src="d0_s9" type="structure" constraint="sepal" constraint_original="sepal; sepal">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="of small , undifferentiated sepals" />
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o3138" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="small" value_original="small" />
        <character is_modifier="true" name="prominence" src="d0_s9" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
      <relation from="o3137" id="r411" name="part_of" negation="false" src="d0_s9" to="o3138" />
    </statement>
    <statement id="d0_s10">
      <text>pistils simple (rarely compound), 1 (-2) -carpellate;</text>
      <biological_entity id="o3139" name="pistil" name_original="pistils" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1(-2)-carpellate" value_original="1(-2)-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary superior;</text>
    </statement>
    <statement id="d0_s12">
      <text>placentation parietal;</text>
      <biological_entity id="o3140" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="superior" value_original="superior" />
        <character is_modifier="false" name="placentation" src="d0_s12" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovule 1, pendulous;</text>
      <biological_entity id="o3141" name="ovule" name_original="ovule" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="pendulous" value_original="pendulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style 1.</text>
      <biological_entity id="o3142" name="style" name_original="style" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits drupes, leathery, dry.</text>
      <biological_entity constraint="fruits" id="o3143" name="drupe" name_original="drupes" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s15" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <other_name type="common_name">Corkwood Family</other_name>
  <discussion>Genus 1, species 1.</discussion>
  <discussion>Leitneriaceae are a relict family confined to the flora area and occurring on coastal and riverine flood plains of the Atlantic and Gulf coasts. The fossil record of the family extends to the Middle Oligocene (32-30 M.Y.B.P.).</discussion>
  <discussion>Taxonomic affinities are uncertain. Various alliances have been suggested: Hamamelidales (A. Cronquist 1981), Sapindales (R. M. T. Dahlgren 1980), and Rutales (R. F. Thorne 1983) of the Rutiflorae. Biochemical studies (D. E. Giannasi 1986; F. P. Petersen and D. E. Fairbrothers 1983, 1985) suggest affinities with Simaroubaceae in Rutales.</discussion>
  <references>
    <reference>Abbe, E. C. and T. T. Earle. 1940. Inflorescence, floral anatomy and morphology of Leitneria floridana. Bull. Torrey Bot. Club 67: 173-193.</reference>
    <reference>Channell, R. B. and C. E. Wood Jr. 1962. The Leitneriaceae in the southeastern United States. J. Arnold Arbor. 43: 435-438.</reference>
    <reference>Giannasi, D. E. 1986. Phytochemical aspects of phylogeny in Hamamelidae. Ann. Missouri Bot. Gard. 73: 417-437.</reference>
    <reference>Jarvis, C. E. 1989. A review of the order Leitneriales Engler. In: P. R. Crane and S. Blackmore, eds. 1989. Evolution, Systematics, and Fossil History of the Hamamelidae. 2 vols. Oxford. Vol. 2, pp. 189-192. [Syst. Assoc. Special Vol. 40B.]</reference>
    <reference>Petersen, F. P. and D. E. Fairbrothers. 1983. A serotaxonomic appraisal of Amphipterygium and Leitneria–two amentiferous taxa of Rutiflorae (Rosidae). Syst. Bot. 8: 134-148.</reference>
  </references>
  
</bio:treatment>