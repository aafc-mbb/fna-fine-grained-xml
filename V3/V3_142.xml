<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="S. Watson" date="1880" rank="species">drummondii</taxon_name>
    <taxon_name authority="(Rydberg) C. L. Hitchcock" date="1964" rank="variety">lithophila</taxon_name>
    <place_of_publication>
      <publication_title>Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>2: 325. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species drummondii;variety lithophila</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500057</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">lithophila</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 152. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Anemone;species lithophila;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">globosa</taxon_name>
    <taxon_name authority="(Rydberg) M.Peck" date="unknown" rank="variety">lithophila</taxon_name>
    <taxon_hierarchy>genus Anemone;species globosa;variety lithophila;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots (7-) 10-25 cm.</text>
      <biological_entity id="o9808" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 5-12 (-15);</text>
      <biological_entity constraint="basal" id="o9809" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="15" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 5-9 cm;</text>
      <biological_entity id="o9810" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets nearly glabrous or pilose;</text>
      <biological_entity id="o9811" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ultimate segments of lateral leaflets 1.5-2.6 mm wide.</text>
      <biological_entity constraint="leaflet" id="o9812" name="segment" name_original="segments" src="d0_s4" type="structure" constraint_original="leaflet ultimate; leaflet">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o9813" name="leaflet" name_original="leaflets" src="d0_s4" type="structure" />
      <relation from="o9812" id="r1379" name="part_of" negation="false" src="d0_s4" to="o9813" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: peduncle pilose;</text>
      <biological_entity id="o9814" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o9815" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucral-bracts nearly glabrous or pilose;</text>
      <biological_entity id="o9816" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o9817" name="involucral-bract" name_original="involucral-bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ultimate segments of lateral leaflets 1.5-2.6 mm wide.</text>
      <biological_entity id="o9818" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="ultimate" id="o9819" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o9820" name="leaflet" name_original="leaflets" src="d0_s7" type="structure" />
      <relation from="o9819" id="r1380" name="part_of" negation="false" src="d0_s7" to="o9820" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals white, tinged blue, rarely abaxially white and blue, adaxially white, abaxially hairy;</text>
      <biological_entity id="o9821" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9822" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="tinged blue" value_original="tinged blue" />
        <character is_modifier="false" modifier="rarely abaxially; abaxially" name="coloration" src="d0_s8" value="white and blue" value_original="white and blue" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments yellow.</text>
      <biological_entity id="o9823" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9824" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Heads of achenes spheric.</text>
      <biological_entity id="o9826" name="achene" name_original="achenes" src="d0_s10" type="structure" />
      <relation from="o9825" id="r1381" name="part_of" negation="false" src="d0_s10" to="o9826" />
    </statement>
    <statement id="d0_s11">
      <text>2n=48.</text>
      <biological_entity id="o9825" name="head" name_original="heads" src="d0_s10" type="structure" constraint="achene" constraint_original="achene; achene">
        <character is_modifier="false" name="shape" src="d0_s10" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9827" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Slopes, ridges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" />
        <character name="habitat" value="ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100-3300m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Alaska, Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12b.</number>
  
</bio:treatment>