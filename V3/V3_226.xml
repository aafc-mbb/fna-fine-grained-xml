<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">annonaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ANNONA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 536. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 241. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family annonaceae;genus ANNONA</taxon_hierarchy>
    <other_info_on_name type="etymology">native Hispaniolan anon or hanon, given to A. muricata</other_info_on_name>
    <other_info_on_name type="fna_id">101891</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, taprooted;</text>
      <biological_entity id="o2338" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks buttressed or not buttressed at base.</text>
      <biological_entity id="o2340" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="buttressed" value_original="buttressed" />
        <character constraint="at base" constraintid="o2341" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="buttressed" value_original="buttressed" />
      </biological_entity>
      <biological_entity id="o2341" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Bark thin, mostly broadly and shallowly fissured, scaly, fissures anastomosing.</text>
      <biological_entity id="o2342" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="mostly broadly; broadly; shallowly" name="relief" src="d0_s2" value="fissured" value_original="fissured" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s2" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o2343" name="fissure" name_original="fissures" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Shoots slender, stiff, terete;</text>
      <biological_entity id="o2344" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lenticels raised;</text>
      <biological_entity id="o2345" name="lenticel" name_original="lenticels" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="raised" value_original="raised" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>buds naked.</text>
      <biological_entity id="o2346" name="bud" name_original="buds" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves persistent or deciduous to late deciduous.</text>
      <biological_entity id="o2347" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaf-blade leathery or membranous, glabrous to pubescent.</text>
      <biological_entity id="o2348" name="leaf-blade" name_original="leaf-blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s7" to="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences axillary or supra-axillary, occasionally from axillary buds on main-stem or older stems, solitary flowers or fascicles;</text>
      <biological_entity id="o2349" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s8" value="supra-axillary" value_original="supra-axillary" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o2350" name="bud" name_original="buds" src="d0_s8" type="structure" />
      <biological_entity id="o2351" name="main-stem" name_original="main-stem" src="d0_s8" type="structure" />
      <biological_entity id="o2352" name="stem" name_original="stems" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="older" value_original="older" />
      </biological_entity>
      <biological_entity id="o2353" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <relation from="o2349" id="r295" modifier="occasionally" name="from" negation="false" src="d0_s8" to="o2350" />
      <relation from="o2350" id="r296" name="on" negation="false" src="d0_s8" to="o2351" />
      <relation from="o2350" id="r297" name="on" negation="false" src="d0_s8" to="o2352" />
    </statement>
    <statement id="d0_s9">
      <text>peduncle bracteolate.</text>
      <biological_entity id="o2354" name="peduncle" name_original="peduncle" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="bracteolate" value_original="bracteolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: receptacle convex to ±globose or elongate, elevated;</text>
      <biological_entity id="o2355" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2356" name="receptacle" name_original="receptacle" src="d0_s10" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s10" to="more or less globose or elongate" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="elevated" value_original="elevated" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals deciduous, 3 (-4), smaller than outer petals, valvate in bud;</text>
      <biological_entity id="o2357" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2358" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="4" />
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character constraint="than outer petals" constraintid="o2359" is_modifier="false" name="size" src="d0_s11" value="smaller" value_original="smaller" />
        <character constraint="in bud" constraintid="o2360" is_modifier="false" name="arrangement_or_dehiscence" src="d0_s11" value="valvate" value_original="valvate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o2359" name="petal" name_original="petals" src="d0_s11" type="structure" />
      <biological_entity id="o2360" name="bud" name_original="bud" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals 6 (-8) in 2 whorls, usually fleshy, those of outer whorl larger, valvate in bud, those of inner whorl more ascending, distinctly smaller or reduced, rarely absent, valvate or imbricate in bud;</text>
      <biological_entity id="o2361" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2362" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="8" />
        <character constraint="in whorls" constraintid="o2363" name="quantity" src="d0_s12" value="6" value_original="6" />
        <character is_modifier="false" modifier="usually" name="texture" notes="" src="d0_s12" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="size" src="d0_s12" value="larger" value_original="larger" />
        <character constraint="in bud" constraintid="o2365" is_modifier="false" name="arrangement_or_dehiscence" src="d0_s12" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="distinctly" name="size" src="d0_s12" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="size" src="d0_s12" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="valvate" value_original="valvate" />
        <character constraint="in bud" constraintid="o2367" is_modifier="false" name="arrangement" src="d0_s12" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o2363" name="whorl" name_original="whorls" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="outer" id="o2364" name="whorl" name_original="whorl" src="d0_s12" type="structure" />
      <biological_entity id="o2365" name="bud" name_original="bud" src="d0_s12" type="structure" />
      <biological_entity constraint="inner" id="o2366" name="whorl" name_original="whorl" src="d0_s12" type="structure" />
      <biological_entity id="o2367" name="bud" name_original="bud" src="d0_s12" type="structure" />
      <relation from="o2362" id="r298" name="part_of" negation="false" src="d0_s12" to="o2364" />
      <relation from="o2362" id="r299" name="part_of" negation="false" src="d0_s12" to="o2366" />
    </statement>
    <statement id="d0_s13">
      <text>nectaries present as darker-pigmented, usually corrugate zones adaxially near petal bases;</text>
      <biological_entity id="o2368" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o2369" name="nectary" name_original="nectaries" src="d0_s13" type="structure">
        <character constraint="as zones" constraintid="o2370" is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2370" name="zone" name_original="zones" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="darker-pigmented" value_original="darker-pigmented" />
        <character is_modifier="true" modifier="usually" name="arrangement_or_relief" src="d0_s13" value="corrugate" value_original="corrugate" />
      </biological_entity>
      <biological_entity constraint="petal" id="o2371" name="base" name_original="bases" src="d0_s13" type="structure" />
      <relation from="o2370" id="r300" name="near" negation="false" src="d0_s13" to="o2371" />
    </statement>
    <statement id="d0_s14">
      <text>stamens very numerous, packed into ball, club-shaped, curved;</text>
      <biological_entity id="o2372" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o2373" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="very" name="quantity" src="d0_s14" value="numerous" value_original="numerous" />
        <character constraint="into ball" constraintid="o2374" is_modifier="false" name="arrangement" src="d0_s14" value="packed" value_original="packed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="club--shaped" value_original="club--shaped" />
        <character is_modifier="false" name="course" src="d0_s14" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o2374" name="ball" name_original="ball" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>connective dilated, hooded or pointed beyond anther sac;</text>
      <biological_entity id="o2375" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o2376" name="connective" name_original="connective" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="hooded" value_original="hooded" />
        <character constraint="beyond anther sac" constraintid="o2377" is_modifier="false" name="shape" src="d0_s15" value="pointed" value_original="pointed" />
      </biological_entity>
      <biological_entity constraint="anther" id="o2377" name="sac" name_original="sac" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>pistils numerous, sessile, partially connate to various degrees with at least stigmas distinct;</text>
      <biological_entity id="o2378" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o2379" name="pistil" name_original="pistils" src="d0_s16" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s16" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="partially" name="fusion" src="d0_s16" value="connate" value_original="connate" />
        <character is_modifier="false" name="variability" src="d0_s16" value="various" value_original="various" />
      </biological_entity>
      <biological_entity id="o2380" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 1 (-2) per pistil;</text>
      <biological_entity id="o2381" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o2382" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="2" />
        <character constraint="per pistil" constraintid="o2383" name="quantity" src="d0_s17" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2383" name="pistil" name_original="pistil" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style and stigma club-shaped or narrowly conic.</text>
      <biological_entity id="o2384" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o2385" name="style" name_original="style" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="club--shaped" value_original="club--shaped" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity id="o2386" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="club--shaped" value_original="club--shaped" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits fleshy syncarps, 1 per flower, usually ovoid to nearly globose, surface variable depending on orientation, structure, and relative connation of pistil apices.</text>
      <biological_entity id="o2387" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character char_type="range_value" from="usually ovoid" name="shape" notes="" src="d0_s19" to="nearly globose" />
      </biological_entity>
      <biological_entity id="o2388" name="syncarp" name_original="syncarps" src="d0_s19" type="structure">
        <character is_modifier="true" name="texture" src="d0_s19" value="fleshy" value_original="fleshy" />
        <character constraint="per flower" constraintid="o2389" name="quantity" src="d0_s19" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2389" name="flower" name_original="flower" src="d0_s19" type="structure" />
      <biological_entity id="o2390" name="surface" name_original="surface" src="d0_s19" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s19" value="variable" value_original="variable" />
      </biological_entity>
      <biological_entity id="o2391" name="structure" name_original="structure" src="d0_s19" type="structure" />
      <biological_entity constraint="pistil" id="o2392" name="apex" name_original="apices" src="d0_s19" type="structure" />
      <relation from="o2391" id="r301" name="part_of" negation="false" src="d0_s19" to="o2392" />
    </statement>
    <statement id="d0_s20">
      <text>Seed usually 1 per pistil, ovoid to ellipsoid, beanlike, coat tough, margins various, narrow.</text>
      <biological_entity id="o2393" name="seed" name_original="seed" src="d0_s20" type="structure">
        <character constraint="per pistil" constraintid="o2394" name="quantity" src="d0_s20" value="1" value_original="1" />
        <character char_type="range_value" from="ovoid" name="shape" notes="" src="d0_s20" to="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s20" value="beanlike" value_original="beanlike" />
      </biological_entity>
      <biological_entity id="o2394" name="pistil" name_original="pistil" src="d0_s20" type="structure" />
      <biological_entity id="o2395" name="coat" name_original="coat" src="d0_s20" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s20" value="tough" value_original="tough" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>x=7.</text>
      <biological_entity id="o2396" name="margin" name_original="margins" src="d0_s20" type="structure">
        <character is_modifier="false" name="variability" src="d0_s20" value="various" value_original="various" />
        <character is_modifier="false" name="size_or_width" src="d0_s20" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="x" id="o2397" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly neotropic; North America; 10 in Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly neotropic" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="10 in Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Custard-apple</other_name>
  <other_name type="common_name">soursop</other_name>
  <other_name type="common_name">alligator-apple</other_name>
  <discussion>Species 110 (2 in the flora).</discussion>
  <references>
    <reference>Safford, W. E. 1914. Classification of the genus Annona, with descriptions of new and imperfectly known species. Contr. U.S. Natl. Herb. 18: i-ix, 1-68.</reference>
    <reference>Sargent, C. S. 1922. Manual of the Trees of North America (Exclusive of Mexico), ed. 2. Boston and New York. Pp. 354-356. [Reprints in 2 vols., 1961, 1965. New York.]</reference>
    <reference>Wood, C. E. 1958. The genera of the woody Ranales in the southeastern United States. J.Arnold Arbor.39: 296-346.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals reniform-cordate; petals ovate, adaxially concave, those of inner whorl at least 2/3 length of outer whorl; syncarp smooth.</description>
      <determination>1 Annona glabra</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals deltate; outer petals oblong or lance-oblong, adaxially keeled, abaxially furrowed, those of inner whorl minute; syncarp muricate.</description>
      <determination>2 Annona squamosa</determination>
    </key_statement>
  </key>
</bio:treatment>