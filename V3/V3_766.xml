<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="(de Candolle) Seringe" date="1849" rank="subgenus">Batrachium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">aquatilis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 556. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus batrachium;species aquatilis;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501117</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Batrachium</taxon_name>
    <taxon_name authority="(Linnaeus) Dumortier" date="unknown" rank="species">aquatile</taxon_name>
    <taxon_hierarchy>genus Batrachium;species aquatile;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous.</text>
      <biological_entity id="o12303" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves laminate and filiform-dissected;</text>
      <biological_entity id="o12304" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="laminate" value_original="laminate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform-dissected" value_original="filiform-dissected" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>laminate leaf-blades reniform, 3-parted, 0.4-1.1 × 0.7-2.3 cm, segments obovate or fan-shaped, shallowly cleft, margins crenate;</text>
      <biological_entity id="o12305" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="laminate" value_original="laminate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-parted" value_original="3-parted" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s2" to="1.1" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s2" to="2.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12306" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="fan--shaped" value_original="fan--shaped" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s2" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o12307" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>filiform-dissected leaves with stipules gradually tapering upward, connate for their whole length.</text>
      <biological_entity id="o12308" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="filiform-dissected" value_original="filiform-dissected" />
        <character is_modifier="false" name="length" notes="" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o12309" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <relation from="o12308" id="r1714" name="with" negation="false" src="d0_s3" to="o12309" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: receptacle hispid, rarely glabrous;</text>
      <biological_entity id="o12310" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o12311" name="receptacle" name_original="receptacle" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals spreading or reflexed, 2-4 × 1-2 mm, glabrous;</text>
      <biological_entity id="o12312" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o12313" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 5, 4-7 × 1-5 mm;</text>
      <biological_entity id="o12314" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o12315" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 0.1-0.8 mm.</text>
      <biological_entity id="o12316" name="flower" name_original="flowers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels usually recurved.</text>
      <biological_entity id="o12317" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s7" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o12318" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o12317" id="r1715" name="fruiting" negation="false" src="d0_s8" to="o12318" />
    </statement>
    <statement id="d0_s9">
      <text>Heads of achenes hemispheric to ovoid, 2-4 × 2-5 mm;</text>
      <biological_entity id="o12319" name="head" name_original="heads" src="d0_s9" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s9" to="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12320" name="achene" name_original="achenes" src="d0_s9" type="structure" />
      <relation from="o12319" id="r1716" name="part_of" negation="false" src="d0_s9" to="o12320" />
    </statement>
    <statement id="d0_s10">
      <text>achenes 1-2 × 0.8-1.4 mm, glabrous or hispid;</text>
      <biological_entity id="o12321" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>beak persistent, filiform, 0.1-1.2 mm.</text>
      <biological_entity id="o12322" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., N.W.T., Nfld. and Labr. (Nfld.), Ont., P.E.I., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Mass., Md., Mich., Minn., Mo., Mont., N.C., N.Dak., N.H., N.J., N.Mex., N.Y., Nebr., Nev., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Va., Vt., W.Va., Wash., Wis., Wyo.; Eurasia</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>73.</number>
  <other_name type="common_name">White water crowsfoot</other_name>
  <other_name type="common_name">renoncule aquatique</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Ranunculus aquatilis is very variable. In the past it has often been treated as three or four species and many varieties. These segregate taxa have been based on the size, petiolation and rigidity of the leaves (now known to be primarily under environmental control), petal size and curvature of the fruiting pedicel (both sometimes variable along single stem), and number of achenes per head and length of achene beak (which vary continuously and are not correlated with one another). Unless reliable characters can be found, only two of the taxa recognized in older floras can be maintained.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves of 2 types: laminate and filiform-dissected; w North America.</description>
      <determination>73a Ranunculus aquatilis var. aquatilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves all filiform-dissected; widespread.</description>
      <determination>73b Ranunculus aquatilis var. diffusus</determination>
    </key_statement>
  </key>
</bio:treatment>