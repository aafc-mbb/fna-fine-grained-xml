<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Agardh" date="unknown" rank="family">piperaceae</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavon" date="1794" rank="genus">peperomia</taxon_name>
    <taxon_name authority="(Jacquin) A. Dietrich" date="1831" rank="species">magnoliifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 153. 1831 (as magnoliaefolia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family piperaceae;genus peperomia;species magnoliifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500898</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Piper</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="species">magnoliaefolium</taxon_name>
    <place_of_publication>
      <publication_title>Collectanea</publication_title>
      <place_in_publication>3: 210. 1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Piper;species magnoliaefolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Peperomia</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">spathulifolia</taxon_name>
    <taxon_hierarchy>genus Peperomia;species spathulifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchophorum</taxon_name>
    <taxon_name authority="(Small) Small" date="unknown" rank="species">spathulifolium</taxon_name>
    <taxon_hierarchy>genus Rhynchophorum;species spathulifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous, erect, decumbent, or reclining, simple or sparsely branched, 10-40 cm, glabrous, without black, glandular dots.</text>
      <biological_entity id="o21786" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="reclining" value_original="reclining" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="reclining" value_original="reclining" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate;</text>
      <biological_entity id="o21787" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1/6-1/4 length of blade, glabrous.</text>
      <biological_entity id="o21788" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1/6 length of blade" name="length" src="d0_s2" to="1/4 length of blade" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade prominently to obscurely pinnately veined, spatulate to broadly ovate or broadly elliptic, 3-14 × 1.5-6 cm, lateral-veins arching-ascending, originating from base to near apex of blade, base attenuate or narrowly to broadly cuneate, not auriculate, not clasping, slightly decurrent along petiole, apex notched or retuse;</text>
      <biological_entity id="o21789" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="prominently to obscurely pinnately" name="architecture" src="d0_s3" value="veined" value_original="veined" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="broadly ovate or broadly elliptic" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21790" name="lateral-vein" name_original="lateral-veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="arching-ascending" value_original="arching-ascending" />
      </biological_entity>
      <biological_entity id="o21791" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o21792" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o21793" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o21794" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="attenuate or" name="shape" src="d0_s3" to="narrowly broadly cuneate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
        <character constraint="along petiole" constraintid="o21795" is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o21795" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity id="o21796" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="notched" value_original="notched" />
        <character is_modifier="false" name="shape" src="d0_s3" value="retuse" value_original="retuse" />
      </biological_entity>
      <relation from="o21790" id="r2976" name="originating from" negation="false" src="d0_s3" to="o21791" />
      <relation from="o21790" id="r2977" name="to" negation="false" src="d0_s3" to="o21792" />
      <relation from="o21792" id="r2978" name="part_of" negation="false" src="d0_s3" to="o21793" />
    </statement>
    <statement id="d0_s4">
      <text>surfaces resinous-dotted, especially abaxially.</text>
      <biological_entity id="o21797" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="resinous-dotted" value_original="resinous-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikes mostly terminal, 1-3, densely flowered, 6-17 cm;</text>
      <biological_entity id="o21798" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s5" value="flowered" value_original="flowered" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s5" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncle glabrous, mature fruiting spikes 2-3 mm diam.</text>
      <biological_entity id="o21799" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="life_cycle" src="d0_s6" value="mature" value_original="mature" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21800" name="spike" name_original="spikes" src="d0_s6" type="structure" />
      <relation from="o21799" id="r2979" name="fruiting" negation="false" src="d0_s6" to="o21800" />
    </statement>
    <statement id="d0_s7">
      <text>Fruits sessile, ellipsoid, base rounded or tapering, tapering gradually to beak, 0.7-0.9 × 0.5-0.6 mm, minutely warty;</text>
      <biological_entity id="o21801" name="fruit" name_original="fruits" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o21802" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="tapering" value_original="tapering" />
        <character constraint="to beak" constraintid="o21803" is_modifier="false" name="shape" src="d0_s7" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" notes="" src="d0_s7" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" notes="" src="d0_s7" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="pubescence_or_relief" src="d0_s7" value="warty" value_original="warty" />
      </biological_entity>
      <biological_entity id="o21803" name="beak" name_original="beak" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>beak elongate, 0.5-1 mm, tapering smoothly from broadened base to sharply acute apex, without filiform apical portion, straight, bent, or gradually hooked from about middle.</text>
      <biological_entity id="o21804" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
        <character constraint="from base" constraintid="o21805" is_modifier="false" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="course" notes="" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s8" value="bent" value_original="bent" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="shape" src="d0_s8" value="bent" value_original="bent" />
        <character constraint="from middle portion" constraintid="o21808" is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity id="o21805" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="broadened" value_original="broadened" />
      </biological_entity>
      <biological_entity id="o21806" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="sharply" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="apical" id="o21807" name="portion" name_original="portion" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity constraint="middle" id="o21808" name="portion" name_original="portion" src="d0_s8" type="structure" />
      <relation from="o21805" id="r2980" name="to" negation="false" src="d0_s8" to="o21806" />
      <relation from="o21804" id="r2981" name="without" negation="false" src="d0_s8" to="o21807" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering all year.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hummmocks, terrestrial or epiphytic</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hummmocks" />
        <character name="habitat" value="terrestrial" />
        <character name="habitat" value="epiphytic" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Bermuda; n South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Bermuda" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  
</bio:treatment>