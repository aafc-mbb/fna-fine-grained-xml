<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(Greene) B. Boivin" date="1944" rank="section">Leucocoma</taxon_name>
    <taxon_name authority="Small &amp; A. Heller" date="1892" rank="species">macrostylum</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Torrey Bot. Club</publication_title>
      <place_in_publication>3: 8. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section leucocoma;species macrostylum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501269</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="B. Boivin" date="unknown" rank="species">subrotundum</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species subrotundum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect to ± reclining, slender, 50-200 cm, glabrous.</text>
      <biological_entity id="o565" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="more or less reclining" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o566" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline petiolate, distal cauline sessile;</text>
      <biological_entity constraint="basal and proximal cauline" id="o567" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="cauline distal" id="o568" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles and rachises glabrous, neither pubescent nor glandular.</text>
      <biological_entity id="o569" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o570" name="rachis" name_original="rachises" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade ternately and pinnately decompound;</text>
      <biological_entity id="o571" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="decompound" value_original="decompound" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets grayish green to brownish to bright green, nearly orbiculate to ovate or obovate, apically undivided or shallowly 2-3-lobed, 5-16 (-22) × 3-18 mm, length 1-3.3 times width, leathery and prominently reticulate abaxially, or sometimes quite membranous, margins sometimes revolute, lobe margins entire;</text>
      <biological_entity id="o572" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="grayish green" name="coloration" src="d0_s5" to="brownish" />
        <character char_type="range_value" from="nearly orbiculate" name="shape" src="d0_s5" to="ovate or obovate apically undivided or shallowly 2-3-lobed" />
        <character char_type="range_value" from="nearly orbiculate" name="shape" src="d0_s5" to="ovate or obovate apically undivided or shallowly 2-3-lobed" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="22" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="1-3.3" value_original="1-3.3" />
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="prominently; abaxially" name="architecture_or_coloration_or_relief" src="d0_s5" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" modifier="sometimes quite" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o573" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o574" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>surfaces abaxially glabrous.</text>
      <biological_entity id="o575" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences racemes or panicles, elongate, few flowered;</text>
      <biological_entity id="o576" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="flowered" value_original="flowered" />
      </biological_entity>
      <biological_entity id="o577" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="flowered" value_original="flowered" />
      </biological_entity>
      <biological_entity id="o578" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncles and pedicels neither pubescent nor glandular.</text>
      <biological_entity id="o579" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o580" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers either unisexual with staminate and pistillate on different plants, or bisexual and unisexual with staminate and bisexual on some plants, pistillate and bisexual on others;</text>
      <biological_entity id="o581" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character constraint="with staminate" is_modifier="false" modifier="either" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character constraint="on plants" constraintid="o582" is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character constraint="on plants" constraintid="o583" is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character constraint="on others" constraintid="o584" is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o582" name="plant" name_original="plants" src="d0_s9" type="structure" />
      <biological_entity id="o583" name="plant" name_original="plants" src="d0_s9" type="structure" />
      <biological_entity id="o584" name="other" name_original="others" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>sepals 4 (-6), greenish to white, nearly orbiculate, 1-2 mm;</text>
      <biological_entity id="o585" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="6" />
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s10" to="white" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s10" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments white, filiform or sometimes clavate, 1.8-4 mm, rigid to flexible;</text>
      <biological_entity id="o586" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s11" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="fragility" src="d0_s11" value="pliable" value_original="flexible" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 0.5-1.2 mm.</text>
      <biological_entity id="o587" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes numerous, slightly stipitate;</text>
      <biological_entity id="o588" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="numerous" value_original="numerous" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="stipitate" value_original="stipitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stipe 0.3-0.7 mm;</text>
      <biological_entity id="o589" name="stipe" name_original="stipe" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>body ovoid, 3-4.5 mm, prominently veined, glabrous;</text>
      <biological_entity id="o590" name="body" name_original="body" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s15" value="veined" value_original="veined" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>beak 0.7-1.7 mm. 2n = 56.</text>
      <biological_entity id="o591" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s16" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o592" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (early Jun-mid Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="mid Jul" from="early Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Low woods, rich wooded slopes, cliffs, swampy forests, meadows, and limestone sinks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="low woods" />
        <character name="habitat" value="rich wooded slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="swampy forests" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="limestone sinks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <discussion>Much variation in Thalictrum macrostylum seems to be associated with habitat differences, especially the amount of sunlight received. The name T. subrotundum merely represents plants of T. macrostylum growing in deep shade. Common garden studies and cluster analyses do not support recognition of two species (M. Park 1992).</discussion>
  
</bio:treatment>