<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="M. J. Warnock" date="1995" rank="subsection">virescens</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">carolinianum</taxon_name>
    <taxon_name authority="(D. Don) M. J. Warnock" date="1981" rank="subspecies">vimineum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>6: 49. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection virescens;species carolinianum;subspecies vimineum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500488</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="D. Don" date="unknown" rank="species">vimineum</taxon_name>
    <place_of_publication>
      <publication_title>in R. Sweet, Brit. Fl. Gard.</publication_title>
      <place_in_publication>7: plate 374. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species vimineum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots usually with elongate single central axis, axis ± vertical, terminal branches (if present) sometimes approaching horizontal.</text>
      <biological_entity id="o24166" name="root" name_original="roots" src="d0_s0" type="structure" />
      <biological_entity constraint="central" id="o24167" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="quantity" src="d0_s0" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o24168" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o24169" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <relation from="o24166" id="r3299" name="with" negation="false" src="d0_s0" to="o24167" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 20-60 (-100) cm.</text>
      <biological_entity id="o24170" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually basal and cauline at anthesis;</text>
      <biological_entity id="o24171" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o24172" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>distalmost petioles more than 5 mm.</text>
      <biological_entity constraint="distalmost" id="o24173" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade distinctly 3-parted;</text>
      <biological_entity id="o24174" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s4" value="3-parted" value_original="3-parted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes of midcauline leaf-blades 3-9, width 3-10 mm.</text>
      <biological_entity constraint="ultimate" id="o24175" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24176" name="midcauline" name_original="midcauline" src="d0_s5" type="structure" />
      <biological_entity id="o24177" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <relation from="o24175" id="r3300" name="consist_of" negation="false" src="d0_s5" to="o24176" />
      <relation from="o24175" id="r3301" name="consist_of" negation="false" src="d0_s5" to="o24177" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals various shades of blue to white.</text>
      <biological_entity id="o24178" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 16, 32.</text>
      <biological_entity id="o24179" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="variability" src="d0_s6" value="various" value_original="various" />
        <character char_type="range_value" from="blue" name="coloration" src="d0_s6" to="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24180" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="16" value_original="16" />
        <character name="quantity" src="d0_s7" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or clay soils, coastal prairies, pine woods, Prosopis or Quercus scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="coastal prairies" />
        <character name="habitat" value="pine woods" />
        <character name="habitat" value="quercus scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., La., Okla., Tex.; Mexico (Coahuila, Nuevo León, and Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (and Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>36c.</number>
  <other_name type="common_name">Pine woods larkspur</other_name>
  <other_name type="common_name">blue larkspur</other_name>
  <other_name type="common_name">Gulf Coast larkspur</other_name>
  <discussion>Delphinium carolinianum subsp. vimineum hybridizes with D. madrense especially on southern edge of Edwards Plateau in Texas.</discussion>
  
</bio:treatment>