<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Chamisso in C. G. D. Nees" date="1793" rank="genus">eschscholzia</taxon_name>
    <taxon_name authority="Greene" date="1885" rank="species">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 183. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus eschscholzia;species parishii</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500642</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, caulescent, erect, 5-30 cm.</text>
      <biological_entity id="o15041" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o15042" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade bright green or yellow-green, glabrous;</text>
      <biological_entity id="o15043" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ultimate lobes obtuse except terminal one slender, acute.</text>
      <biological_entity constraint="ultimate" id="o15044" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character constraint="except slender" constraintid="o15045" is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o15045" name="slender" name_original="slender" src="d0_s3" type="structure" constraint="terminal" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences cymose or 1-flowered;</text>
      <biological_entity id="o15046" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>buds nodding.</text>
      <biological_entity id="o15047" name="bud" name_original="buds" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: receptacle obconic, cup without spreading free rim;</text>
      <biological_entity id="o15048" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15049" name="receptacle" name_original="receptacle" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obconic" value_original="obconic" />
      </biological_entity>
      <biological_entity id="o15050" name="cup" name_original="cup" src="d0_s6" type="structure" />
      <biological_entity id="o15051" name="rim" name_original="rim" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="fusion" src="d0_s6" value="free" value_original="free" />
      </biological_entity>
      <relation from="o15050" id="r2101" name="without" negation="false" src="d0_s6" to="o15051" />
    </statement>
    <statement id="d0_s7">
      <text>calyx apiculate, glabrous, sometimes glaucous;</text>
      <biological_entity id="o15052" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15053" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow, 15-30 mm.</text>
      <biological_entity id="o15054" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15055" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 5-7 cm.</text>
      <biological_entity id="o15056" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s9" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds tan to brown, spheric to ellipsoid, 1-1.4 mm, reticulate.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 12.</text>
      <biological_entity id="o15057" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s10" to="brown" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s10" to="ellipsoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s10" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15058" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert slopes, hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert slopes" />
        <character name="habitat" value="hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California and Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California and Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Plants of the El Paso and Rand mountains in the western Mojave Desert that have been referred to this species are Eschscholzia minutiflora.</discussion>
  <discussion>The Kawaiisu used Eschscholzia parishii medicinally to treat venereal sores, gonorrhea, and syphilis (D. E. Moerman 1986).</discussion>
  
</bio:treatment>