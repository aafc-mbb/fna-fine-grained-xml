<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">menispermaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">MENISPERMUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 340. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 158. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family menispermaceae;genus MENISPERMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek mene, moon, and sperma, seed</other_info_on_name>
    <other_info_on_name type="fna_id">120237</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Vines, twining, or lianas.</text>
      <biological_entity id="o27426" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="twining" value_original="twining" />
        <character name="growth_form" value="vine" />
        <character name="growth_form" value="liana" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems green, apically tomentose grading to glabrate on older portions.</text>
      <biological_entity id="o27428" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="apically" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character constraint="on portions" constraintid="o27429" is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o27429" name="portion" name_original="portions" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="older" value_original="older" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves peltate, petiole inserted near margin, rarely with petiole attached basally.</text>
      <biological_entity id="o27430" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="peltate" value_original="peltate" />
      </biological_entity>
      <biological_entity id="o27431" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
      <biological_entity id="o27432" name="margin" name_original="margin" src="d0_s2" type="structure" />
      <biological_entity id="o27433" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="basally" name="fixation" src="d0_s2" value="attached" value_original="attached" />
      </biological_entity>
      <relation from="o27431" id="r3677" name="inserted near" negation="false" src="d0_s2" to="o27432" />
      <relation from="o27431" id="r3678" modifier="rarely" name="with" negation="false" src="d0_s2" to="o27433" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade broadly ovate, generally (3-) 5-7-lobed or angled, base cordate, truncate, or rounded, margins entire, apex acuminate to rounded, mucronate, rarely notched or obcordate;</text>
      <biological_entity id="o27434" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="generally" name="shape" src="d0_s3" value="(3-)5-7-lobed" value_original="(3-)5-7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o27435" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o27436" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o27437" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="rounded mucronate rarely notched or obcordate" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="rounded mucronate rarely notched or obcordate" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="rounded mucronate rarely notched or obcordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>surfaces abaxially very pale, sometimes silvery, glabrous to pilose, adaxially glabrous to sparsely pilose.</text>
      <biological_entity id="o27438" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially very" name="coloration" src="d0_s4" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="silvery" value_original="silvery" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="pilose adaxially glabrous" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="pilose adaxially glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, panicles well developed;</text>
      <biological_entity id="o27439" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o27440" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s5" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts minute (bracteoles).</text>
      <biological_entity id="o27441" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3-ranked;</text>
      <biological_entity id="o27442" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="3-ranked" value_original="3-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals usually 6, ovate to elliptic or obovate, glabrous;</text>
      <biological_entity id="o27443" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="elliptic or obovate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals (4-) 6 (-12), free, to 4 mm, entire, auriculate basal lobes absent.</text>
      <biological_entity id="o27444" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s9" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="12" />
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="basal" id="o27445" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers: petals entire;</text>
      <biological_entity id="o27446" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o27447" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 12-36;</text>
      <biological_entity id="o27448" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o27449" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s11" to="36" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct;</text>
      <biological_entity id="o27450" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o27451" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 4-locular;</text>
      <biological_entity id="o27452" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o27453" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="4-locular" value_original="4-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistillodes absent.</text>
      <biological_entity id="o27454" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o27455" name="pistillode" name_original="pistillodes" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: staminodes 6-9, well developed;</text>
      <biological_entity id="o27456" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27457" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s15" to="9" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s15" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pistils (2-) 3 (-4);</text>
      <biological_entity id="o27458" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27459" name="pistil" name_original="pistils" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s16" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="4" />
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary slightly asymmetrically pouched, glabrous;</text>
      <biological_entity id="o27460" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27461" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="slightly asymmetrically" name="architecture_or_shape" src="d0_s17" value="pouched" value_original="pouched" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma slightly lobed.</text>
      <biological_entity id="o27462" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27463" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s18" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Drupes globose, glabrous;</text>
      <biological_entity id="o27464" name="drupe" name_original="drupes" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="globose" value_original="globose" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>endocarp bony, warty, ribbed.</text>
      <biological_entity id="o27465" name="endocarp" name_original="endocarp" src="d0_s20" type="structure">
        <character is_modifier="false" name="texture" src="d0_s20" value="osseous" value_original="bony" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s20" value="warty" value_original="warty" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, and Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="and Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Moonseed</other_name>
  <discussion>Species 3 (1 in the flora).</discussion>
  
</bio:treatment>