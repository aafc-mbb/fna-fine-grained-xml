<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">thalictrum</taxon_name>
    <taxon_name authority="(de Candolle) B. Boivin" date="1944" rank="section">Heterogamia</taxon_name>
    <taxon_name authority="A. Gray" date="1873" rank="species">occidentale</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 372. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus thalictrum;section heterogamia;species occidentale;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501271</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">occidentale</taxon_name>
    <taxon_name authority="B. Boivin" date="unknown" rank="variety">macounii</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species occidentale;variety macounii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thalictrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">occidentale</taxon_name>
    <taxon_name authority="H. St. John" date="unknown" rank="variety">palousense</taxon_name>
    <taxon_hierarchy>genus Thalictrum;species occidentale;variety palousense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots yellow to medium brown or black, thin, fibrous.</text>
      <biological_entity id="o26511" name="root" name_original="roots" src="d0_s0" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s0" to="medium brown or black" />
        <character is_modifier="false" name="width" src="d0_s0" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 30-100 (-120) dm, glabrous, often from well-developed rhizomes.</text>
      <biological_entity id="o26512" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="120" to_unit="dm" />
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s1" to="100" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26513" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="development" src="d0_s1" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <relation from="o26512" id="r3582" modifier="often" name="from" negation="false" src="d0_s1" to="o26513" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline.</text>
      <biological_entity id="o26514" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26515" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade 3-4×-ternately compound;</text>
      <biological_entity id="o26516" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="3-4×-ternately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets orbiculate to obovate-cuneate or cordate, apically 3-lobed, 10-30 mm wide, lobe margins coarsely crenate, surfaces glabrous to glandular.</text>
      <biological_entity id="o26517" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s4" to="obovate-cuneate or cordate" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s4" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o26518" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o26519" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal (some flowers in axils of distal leaves), panicles, rather open, many flowered.</text>
      <biological_entity id="o26520" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o26521" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals whitish or greenish or purplish tinged, ovate, 3.5-4.4 mm in staminate flowers, 1.5-2 mm in pistillate flowers;</text>
      <biological_entity id="o26522" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o26523" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish tinged" value_original="purplish tinged" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" constraint="in flowers" constraintid="o26524" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4.4" to_unit="mm" />
        <character char_type="range_value" constraint="in flowers" constraintid="o26525" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26524" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26525" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments purplish, 4-10 mm;</text>
      <biological_entity id="o26526" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o26527" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 1.5-4 mm, long-apiculate;</text>
      <biological_entity id="o26528" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26529" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="long-apiculate" value_original="long-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigma often purplish.</text>
      <biological_entity id="o26530" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o26531" name="stigma" name_original="stigma" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Achenes 6-9, spreading to reflexed, short-stipitate;</text>
      <biological_entity id="o26532" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="9" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s10" to="reflexed" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="short-stipitate" value_original="short-stipitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stipe 0.4-1.2 mm;</text>
      <biological_entity id="o26533" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>body fusiform, not laterally compressed, (4-) 6-9 (-10) mm, tapering at both ends, glandular, strongly 3-veined on each side, veins not anastomosing;</text>
      <biological_entity id="o26534" name="body" name_original="body" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="not laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
        <character constraint="at ends" constraintid="o26535" is_modifier="false" name="shape" src="d0_s12" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" notes="" src="d0_s12" value="glandular" value_original="glandular" />
        <character constraint="on side" constraintid="o26536" is_modifier="false" modifier="strongly" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o26535" name="end" name_original="ends" src="d0_s12" type="structure" />
      <biological_entity id="o26536" name="side" name_original="side" src="d0_s12" type="structure" />
      <biological_entity id="o26537" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>beak 3-4.5 (-6) mm.</text>
      <biological_entity id="o26538" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer-mid summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="early summer" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woods, meadows, and copses</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woods" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="copses" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask., Yukon; Alaska, Calif., Colo., Idaho, Mont., Nev., N.Dak., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Western meadow-rue</other_name>
  <discussion>Thalictrum occidentale is similar to T. confine and T. venulosum; thorough field studies are needed to determine whether or not they should be maintained as separate species. Thalictrum occidentale can usually be distinguished by its reflexed achenes.</discussion>
  <discussion>Plants of northern British Columbia, sometimes called Thalictrum occidentale var. breitungii (B. Boivin) Brayshaw, appear to be intermediate between T. occidentale and T. venulosum (T. C. Brayshaw, pers. comm.); achenes are ascending, ± compressed, and beaks rather short (2-4 mm) (T. C. Brayshaw 1989).</discussion>
  <discussion>Some of the Native Americans used Thalictrum occidentale medicinally for headaches, eye trouble, and sore legs, to loosen phlem, and to improve blood circulation (D. E. Moerman 1986).</discussion>
  
</bio:treatment>