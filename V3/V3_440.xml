<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">papaver</taxon_name>
    <taxon_name authority="Spach" date="1839" rank="section">Meconella</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Nat. Vég.</publication_title>
      <place_in_publication>7: 19. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus papaver;section Meconella</taxon_hierarchy>
    <other_info_on_name type="fna_id">302022</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Papaver</taxon_name>
    <taxon_name authority="Elkan" date="unknown" rank="section">Scapiflora</taxon_name>
    <taxon_hierarchy>genus Papaver;section Scapiflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Papaver</taxon_name>
    <taxon_name authority="(Bernhardi) Pfeiffer" date="unknown" rank="section">Lasiotrachyphylla</taxon_name>
    <taxon_hierarchy>genus Papaver;section Lasiotrachyphylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, scapose.</text>
      <biological_entity id="o5816" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades unlobed or 1-2×-lobed.</text>
      <biological_entity id="o5817" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="1-2×-lobed" value_original="1-2×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: filaments white or yellow, filiform.</text>
      <biological_entity id="o5818" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o5819" name="filament" name_original="filaments" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s2" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Capsules obscurely valvate, ribbed, variously pubescent.</text>
      <biological_entity id="o5820" name="capsule" name_original="capsules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="obscurely" name="arrangement_or_dehiscence" src="d0_s3" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" modifier="variously" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
  </description>
  <number>11d.</number>
  <discussion>More study is needed for a comprehensive assessment of the North American scapose poppies in circumpolar context. The circumscriptions and interrelationships of the Alaskan and Asiatic members of this section are especially problematic.</discussion>
  <discussion>Typification of Papaver radicatum Rottbfll also is problematic, and interpretation of its identity has significant consequences in the complex nomenclature of the arctic-alpine poppies. For details and discussion, see G. Knaben (1958); A. Löve (1962b, 1962c); and G. Knaben and N. Hylander (1970).</discussion>
  <references>
    <reference>Knaben, G. 1958. Papaver-studier, med et forsvar for P. radicatum Rottb. som en Islandsk-Skandinavisk art. Blyttia 16: 61-80.</reference>
    <reference>Knaben, G. 1959. On the evolution of the radicatum-group of the Scapiflora Papavers as studied in 70 and 56 chromosome species. Part A. Cytotaxonomical aspects. Part B. Experimental studies. Opera Bot. 2(3), 3(3).</reference>
    <reference>Knaben, G. and N. Hylander. 1970. On the typification of Papaver radicatum Rottb. and its nomenclatural consequences. Bot. Not. 123: 338-345.</reference>
    <reference>Löve, A. 1962b. Typification of Papaver radicatum–A nomenclatural detective story. Bot. Not. 115: 113-136.</reference>
    <reference>Löve, A. 1962c. Nomenclature of North Atlantic Papavers. Taxon 11: 132-138.</reference>
    <reference>Löve, D. 1969. Papaver at high altitudes in the Rocky Mountains. Brittonia 21: 1-10.</reference>
    <reference>Rändel, U. 1974. Beitrage zur Kenntnis der Sippenstruktur der Gattung Papaver Sectio Scapiflora Reichenb. (Papaveraceae). Feddes Repert. 84: 655-732.</reference>
    <reference>Rändel, U. 1975. Die Beziehungen von Papaver pygmaeum Rydb. aus den Rocky Mountains zum nordamerikanischen P. kluanense D. Löve sowie zu einigen nordost-asiatischen Vertretern der Sektion Scapiflorae Reichenb. im Vergleich mit P. alpinum L. (Papaveraceae). Feddes Repert. 86: 19-37.</reference>
    <reference>Rändel, U. 1977. Über Sippen des subarktisch-arktischen Nordamerikas, des Beringia-Gebietes und Nordost-Asiens der Sektion Lasiotrachyphylla Bernh. (Papaveraceae) und deren Beziehungen zu einander und zu Sippen anderer Arealteile der Sektion. Feddes Repert. 88: 421-450.</reference>
    <reference>Rändel, U. 1977b. Über die grönländischen Vertreter der Sektion Lasiotrachyphylla Bernh. (Papaveraceae) und deren Beziehungen zu Vertretern anderer arktischer Arealteile der Sektion. Feddes Repert. 88: 451-464.</reference>
  </references>
  
</bio:treatment>