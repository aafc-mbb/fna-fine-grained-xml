<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">PAPAVER</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 506. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 224. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus PAPAVER</taxon_hierarchy>
    <other_info_on_name type="etymology">classical Latin name for poppy; perhaps from Greek papa (pap), alluding to the thick, sometimes milky sap</other_info_on_name>
    <other_info_on_name type="fna_id">123791</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, biennial, or perennial, scapose or caulescent, from taproots;</text>
      <biological_entity id="o4779" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o4780" name="taproot" name_original="taproots" src="d0_s0" type="structure" />
      <relation from="o4779" id="r682" name="from" negation="false" src="d0_s0" to="o4780" />
    </statement>
    <statement id="d0_s1">
      <text>sap white, orange, or red.</text>
      <biological_entity id="o4781" name="sap" name_original="sap" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems when present leafy.</text>
      <biological_entity id="o4782" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when present" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal rosulate, petiolate;</text>
      <biological_entity id="o4783" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o4784" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline alternate, proximal leaves petiolate, distal subsessile or sessile, sometimes clasping (in P. somniferum);</text>
      <biological_entity id="o4785" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o4786" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4787" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4788" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade unlobed or 1-3× pinnately lobed or parted;</text>
      <biological_entity id="o4789" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o4790" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
        <character name="shape" src="d0_s5" value="1-3×pinnately" value_original="1-3×pinnately" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="parted" value_original="parted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins entire or toothed, scalloped, or incised.</text>
      <biological_entity id="o4791" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o4792" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="scalloped" value_original="scalloped" />
        <character is_modifier="false" name="shape" src="d0_s6" value="incised" value_original="incised" />
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="scalloped" value_original="scalloped" />
        <character is_modifier="false" name="shape" src="d0_s6" value="incised" value_original="incised" />
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="scalloped" value_original="scalloped" />
        <character is_modifier="false" name="shape" src="d0_s6" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymiform, with flowers disposed in 1s, 2s or 3s on long scapes or peduncles;</text>
      <biological_entity id="o4793" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <biological_entity id="o4794" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character constraint="in 1s" is_modifier="false" name="arrangement" src="d0_s7" value="disposed" value_original="disposed" />
      </biological_entity>
      <biological_entity id="o4795" name="scape" name_original="scapes" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o4796" name="peduncle" name_original="peduncles" src="d0_s7" type="structure" />
      <relation from="o4793" id="r683" name="with" negation="false" src="d0_s7" to="o4794" />
      <relation from="o4793" id="r684" name="on" negation="false" src="d0_s7" to="o4795" />
      <relation from="o4793" id="r685" name="on" negation="false" src="d0_s7" to="o4796" />
    </statement>
    <statement id="d0_s8">
      <text>bracts present;</text>
      <biological_entity id="o4797" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>buds nodding [erect].</text>
      <biological_entity id="o4798" name="bud" name_original="buds" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 2 (-3), distinct;</text>
      <biological_entity id="o4799" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4800" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="3" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 4 (-6);</text>
      <biological_entity id="o4801" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4802" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens many;</text>
      <biological_entity id="o4803" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o4804" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistil 3-18 [-22] -carpellate;</text>
      <biological_entity id="o4805" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o4806" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-18[-22]-carpellate" value_original="3-18[-22]-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 1-locular, sometimes incompletely multilocular by placental intrusion;</text>
      <biological_entity id="o4807" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o4808" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="1-locular" value_original="1-locular" />
        <character constraint="by placental" constraintid="o4809" is_modifier="false" modifier="sometimes incompletely" name="architecture" src="d0_s14" value="multilocular" value_original="multilocular" />
      </biological_entity>
      <biological_entity id="o4809" name="placental" name_original="placental" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style absent;</text>
      <biological_entity id="o4810" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o4811" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigmas 3-18 [-22], radiating on sessile, ± lobed disc, velvety.</text>
      <biological_entity id="o4812" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o4813" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="22" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s16" to="18" />
        <character constraint="on disc" constraintid="o4814" is_modifier="false" name="arrangement" src="d0_s16" value="radiating" value_original="radiating" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s16" value="velvety" value_original="velvety" />
      </biological_entity>
      <biological_entity id="o4814" name="disc" name_original="disc" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s16" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules erect, 3-18 [-22] -pored or short-valved immediately beneath persistent or sometimes deciduous (in P. hybridum) stigmatic disc.</text>
      <biological_entity id="o4815" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character constraint="beneath stigmatic disc" constraintid="o4816" is_modifier="false" name="architecture" src="d0_s17" value="short-valved" value_original="short-valved" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o4816" name="disc" name_original="disc" src="d0_s17" type="structure">
        <character is_modifier="true" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character is_modifier="true" modifier="sometimes" name="duration" src="d0_s17" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds many, minutely pitted, aril absent.</text>
      <biological_entity id="o4817" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s18" value="many" value_original="many" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s18" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>x = 7.</text>
      <biological_entity id="o4818" name="aril" name_original="aril" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o4819" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate and arctic North America, Eurasia, n, s Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate and arctic North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Poppy</other_name>
  <other_name type="common_name">pavot</other_name>
  <discussion>Species 70-100 (16 in the flora).</discussion>
  <discussion>Papaver is rich in alkaloids, notably opiates. The genus is quite complex cytologically; in addition to diploids, there are numerous polyploid species and some that apparently are aneuploid. Most commonly, n = 7 or a multiple, and 2n ranges from 14 to over 100. There are published chromosome counts for almost every taxon in the flora, but for the introduced species none has been made from wild-collected North American material.</discussion>
  <discussion>The scapose poppies in the flora are native; the caulescent ones, except Papaver californicum, are introduced Eurasian ornamentals, crop weeds, and ballast waifs. All the scapose species are confined to arctic and alpine habitats. Plants of the introduced caulescent species, especially P. rhoeas, P. dubium, and P. somniferum, vary greatly in size, and surprisingly diminutive mature individuals are sometimes found, especially northward.</discussion>
  <discussion>Excluded species:</discussion>
  <discussion>Papaver dahlianum Nordhagen, Bergens Mus. Årbok 2: 46. 1931</discussion>
  <discussion>Papaver radicatum Rottbfll subsp. dahlianum (Nordhagen) Rändel</discussion>
  <discussion>We regard this species as being restricted to arctic Europe, a narrower circumscription than U. Rändel's (1977).</discussion>
  <discussion>Papaver microcarpum de Candolle, Syst. Nat. 2: 71. 1821</discussion>
  <discussion>We are so far unable to substantiate D. Löve's (1969) report of this essentially Asiatic species "from Seward and Kenai peninsulas in Alaska, the Aleutian Islands."</discussion>
  <references>
    <reference>Kadereit, J. W. 1988. Sectional affinities and geographical distribution in the genus Papaver L. (Papaveraceae). Beitr. Biol. Pflanzen 63: 139-156.</reference>
    <reference>Kadereit, J. W. 1990. Some suggestions on the geographical origin of the central, west and north European synanthropic species of Papaver L. Bot. J. Linn. Soc. 103: 221-231.</reference>
    <reference>Kiger, R. W. 1973. Sectional nomenclature in Papaver L. Taxon 22: 579-582.</reference>
    <reference>Kiger, R. W. 1975. Papaver in North America north of Mexico. Rhodora 77: 410-422.</reference>
    <reference>Kiger, R. W. 1985. Revised sectional nomenclature in Papaver L. Taxon 34: 150-152.</reference>
    <reference>Novák, J. and V. Preininger. 1987. Chemotaxonomic review of the genus Papaver. Preslia 59: 1-13.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants caulescent (sometimes subscapose), at least a few cauline leaves present.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants strictly scapose, leaves all basal (sect. Meconella).</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Blades of distal leaves clasping stem (sect. Papaver).</description>
      <determination>14 Papaver somniferum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Blades of distal leaves not clasping stem.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules setose (sect. Argemonideum).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules glabrous.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules obovoid-ellipsoid to subglobose, densely and firmly setose.</description>
      <determination>2 Papaver hybridum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules oblong to clavate, sparsely and weakly setose.</description>
      <determination>1 Papaver argemone</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants perennial; flowers 10 cm or more broad (sect. Macrantha).</description>
      <determination>4 Papaver orientale</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants annual; flowers less than 10 cm broad.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Peduncles glabrous or sparsely pilose; petals with greenish basal spot; stigmatic disc conic, usually umbonate; capsules distinctly short-valvate (sect. Californicum).</description>
      <determination>3 Papaver californicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Peduncles hispid; petals unspotted or with dark basal spot; stigmatic disc ± flat; capsules poricidal (sect. Rhoeadium).</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Peduncles markedly spreading-hispid distally; capsules less than 2 times longer than broad.</description>
      <determination>16 Papaver rhoeas</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Peduncles strongly appressed-hispid distally; capsules 2 times or more longer than broad.</description>
      <determination>15 Papaver dubium</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades mostly with 3 primary lobes.</description>
      <determination>12 Papaver walpolei</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades with 5–many primary lobes.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Capsules more than 4 times longer than broad.</description>
      <determination>6 Papaver macounii</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Capsules 1–2.5 times longer than broad.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Trichomes on capsules ivory colored.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Trichomes on capsules light to dark brown or black.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Plants tall, seldom less than 2 dm.</description>
      <determination>5 Papaver nudicaule</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Plants short, seldom more than 1.5 dm.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades setose.</description>
      <determination>10 Papaver alboroseum</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades glabrous or sparsely hirsute.</description>
      <determination>11 Papaver pygmaeum</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades mostly with 5 primary lobes, lobes mostly simple.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades with more than 5 primary lobes, lobes mostly divided.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Primary leaf lobes oblanceolate to strap-shaped.</description>
      <determination>13 Papaver gorodkovii</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Primary leaf lobes broadly lanceolate to ovate.</description>
      <determination>8 Papaver radicatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Scapes straight, erect, generally longer than 20 cm; capsules oblong-ellipsoid.</description>
      <determination>7 Papaver lapponicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Scapes curved, erect or decumbent, less than 15 cm, capsules obconic to subglobose.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades generally green, not glaucous, primary lobes lanceolate, their apices acute to obtuse.</description>
      <determination>8 Papaver radicatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades generally gray- and blue-green, glaucous, primary lobes obovate to strap-shaped, their apices rounded.</description>
      <determination>9 Papaver mcconnellii</determination>
    </key_statement>
  </key>
</bio:treatment>