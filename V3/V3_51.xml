<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">casuarinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">casuarina</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="species">equisetifolia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">equisetifolia</taxon_name>
    <taxon_hierarchy>family casuarinaceae;genus casuarina;species equisetifolia;subspecies equisetifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">233500329</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, 7-35 m, not suckering.</text>
      <biological_entity id="o19677" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="m" name="some_measurement" src="d0_s0" to="35" to_unit="m" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark gray-brown to black, scaly.</text>
      <biological_entity id="o19678" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="gray-brown" name="coloration" src="d0_s1" to="black" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Branchlets drooping;</text>
      <biological_entity id="o19679" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="drooping" value_original="drooping" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>segments 5-8 [-13] × 0.5-0.7 [-1] mm, usually densely pubescent at least in furrows, not waxy;</text>
      <biological_entity id="o19680" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="13" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="0.7" to_unit="mm" />
        <character constraint="in furrows" constraintid="o19681" is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="not" name="texture" notes="" src="d0_s3" value="ceraceous" value_original="waxy" />
      </biological_entity>
      <biological_entity id="o19681" name="furrow" name_original="furrows" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>longitudinal ridges angular with median rib;</text>
      <biological_entity id="o19682" name="ridge" name_original="ridges" src="d0_s4" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s4" value="longitudinal" value_original="longitudinal" />
        <character constraint="with median rib" constraintid="o19683" is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="angular" value_original="angular" />
      </biological_entity>
      <biological_entity constraint="median" id="o19683" name="rib" name_original="rib" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>teeth not marcescent, (6-) 7-8, erect, 0.3-0.8 mm.</text>
      <biological_entity id="o19684" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="condition" src="d0_s5" value="marcescent" value_original="marcescent" />
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s5" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="8" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Young permanent shoots with erect to spreading teeth.</text>
      <biological_entity id="o19685" name="shoot" name_original="shoots" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="young" value_original="young" />
        <character is_modifier="true" name="duration" src="d0_s6" value="permanent" value_original="permanent" />
      </biological_entity>
      <biological_entity id="o19686" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="erect" is_modifier="true" name="orientation" src="d0_s6" to="spreading" />
      </biological_entity>
      <relation from="o19685" id="r2647" name="with" negation="false" src="d0_s6" to="o19686" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, staminate and pistillate on same plant.</text>
      <biological_entity id="o19687" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character constraint="on plant" constraintid="o19688" is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19688" name="plant" name_original="plant" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate spikes 0.7-4 cm, 7-11.5 whorls per cm;</text>
      <biological_entity id="o19689" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="11.5" />
      </biological_entity>
      <biological_entity id="o19690" name="whorl" name_original="whorls" src="d0_s8" type="structure" />
      <biological_entity id="o19691" name="cm" name_original="cm" src="d0_s8" type="structure" />
      <relation from="o19690" id="r2648" name="per" negation="false" src="d0_s8" to="o19691" />
    </statement>
    <statement id="d0_s9">
      <text>anthers 0.6-0.8 mm.</text>
      <biological_entity id="o19692" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Infructescences sparsely pubescent [tomentose];</text>
      <biological_entity id="o19693" name="infructescence" name_original="infructescences" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>peduncles 3-10 mm;</text>
      <biological_entity id="o19694" name="peduncle" name_original="peduncles" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>infructescence body 12-24 × 9-11 mm;</text>
      <biological_entity constraint="infructescence" id="o19695" name="body" name_original="body" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s12" to="24" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s12" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>bracteoles acute.</text>
      <biological_entity id="o19696" name="bracteole" name_original="bracteoles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Samaras 6-8 mm.</text>
      <biological_entity id="o19697" name="samara" name_original="samaras" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy seasides, native to tropical and subtropical coastlines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy seasides" />
        <character name="habitat" value="tropical" />
        <character name="habitat" value="subtropical coastlines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; native, Southeast Asia; native, s Pacific Islands (e to Tahiti and Samoa); native, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="native" establishment_means="native" />
        <character name="distribution" value="Southeast Asia" establishment_means="native" />
        <character name="distribution" value="native" establishment_means="native" />
        <character name="distribution" value="s Pacific Islands (e to Tahiti and Samoa)" establishment_means="native" />
        <character name="distribution" value="native" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <discussion>Casuarina litorea Rumphius ex Stickman is not a valid name.</discussion>
  <discussion>Casuarina equisetifolia is widely cultivated in many parts of the world because of its salt tolerance; it is now considered an invasive pest in parts of Florida. Only C. equisetifolia subsp. equisetifolia is known from the flora area. Casuarina equisetifolia subsp. incana (Bentham) L. A. S. Johnson, from Australia and the Pacific Islands region, is cultivated elsewhere in the world and possibly has been introduced to (although not yet discovered in) the flora area. That subspecies is a smaller tree with a more rounded crown, longer and thicker branchlets, and more pubescent branchlets and infructescences.</discussion>
  
</bio:treatment>