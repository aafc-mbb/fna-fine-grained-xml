<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="(Ewan) N. I. Malyutin" date="1987" rank="subsection">echinata</taxon_name>
    <taxon_name authority="A. Gray" date="1887" rank="species">hesperium</taxon_name>
    <taxon_name authority="(Abrams) H. F. Lewis &amp; Epling" date="1954" rank="subspecies">cuyamacae</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>8: 11. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection echinata;species hesperium;subspecies cuyamacae;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500510</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Delphinium</taxon_name>
    <taxon_name authority="Abrams" date="unknown" rank="species">cuyamacae</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>32: 538. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Delphinium;species cuyamacae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems proximally puberulent with arched hairs.</text>
      <biological_entity id="o6096" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="with hairs" constraintid="o6097" is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o6097" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation_or_shape" src="d0_s0" value="arched" value_original="arched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences generally more than 25-flowered, dense;</text>
      <biological_entity id="o6098" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="generally" name="architecture" src="d0_s1" value="25+-flowered" value_original="25+-flowered" />
        <character is_modifier="false" name="density" src="d0_s1" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>pedicel ascending.</text>
      <biological_entity id="o6099" name="pedicel" name_original="pedicel" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals dark blue-purple, spreading to erect, lateral sepals 7-10 mm, less than 4 (-5) mm wide, spurs 8-12 mm;</text>
      <biological_entity id="o6100" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o6101" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark blue-purple" value_original="dark blue-purple" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="erect" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6102" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6103" name="spur" name_original="spurs" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lower petal blades 3-5 mm.</text>
      <biological_entity id="o6104" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="petal" id="o6105" name="blade" name_original="blades" src="d0_s4" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits 2.2-2.5 times longer than wide.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 16.</text>
      <biological_entity id="o6106" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="2.2-2.5" value_original="2.2-2.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6107" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassland, open pine woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassland" />
        <character name="habitat" value="open pine woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39b.</number>
  <other_name type="common_name">Cuyamaca larkspur</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Delphinium hesperium subsp. cuyamacae is local near Cuyamaca Lake. Although it has been reported also from Mt. Palomar, specimens have not been seen.</discussion>
  <discussion>No other Delphinium with similar features grows in the region where D. hesperium subsp. cuyamacae grows. Superficially, specimens of this taxon resemble those of some D. hansenii subsp. hansenii. Seeds are quite different, as are pubescence patterns and venation on abaxial surface of leaves. Delphinium parryi occurs near D. hesperium subsp. cuyamacae; flowers of D. parryi in that area are much larger and more widely spaced on the inflorescence than in D. hesperium subsp. cuyamacae.</discussion>
  
</bio:treatment>