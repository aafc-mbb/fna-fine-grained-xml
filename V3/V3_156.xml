<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">narcissiflora</taxon_name>
    <taxon_name authority="(A. Nelson) B. E. Dutton &amp; Keener" date="1994" rank="variety">zephyra</taxon_name>
    <place_of_publication>
      <publication_title>Phytolgia</publication_title>
      <place_in_publication>77: 85. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species narcissiflora;variety zephyra</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500072</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">zephyra</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>42: 51. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Anemone;species zephyra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anemone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">narcissiflora</taxon_name>
    <taxon_name authority="(A. Nelson) A. Löve" date="unknown" rank="subspecies">zephyra</taxon_name>
    <place_of_publication>
      <publication_title>D. Löve, &amp; B. M. Kapoor</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus Anemone;species narcissiflora;subspecies zephyra;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots 7-40 cm.</text>
      <biological_entity id="o1101" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 3-7;</text>
      <biological_entity constraint="basal" id="o1102" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole (4-) 5-10 (-15) cm;</text>
      <biological_entity id="o1103" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets glabrous, sparsely pilose, or villous;</text>
      <biological_entity id="o1104" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lateral leaflets 2 (-3) ×-lobed;</text>
      <biological_entity constraint="lateral" id="o1105" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="2(-3)×-lobed" value_original="2(-3)×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 3-8 mm wide.</text>
      <biological_entity constraint="ultimate" id="o1106" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2-3 (-4) -flowered umbels or flowers solitary;</text>
      <biological_entity id="o1107" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o1108" name="umbel" name_original="umbels" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-3(-4)-flowered" value_original="2-3(-4)-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o1109" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-3(-4)-flowered" value_original="2-3(-4)-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle pilose or villous;</text>
      <biological_entity id="o1110" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts obtriangular, 3-cleft, (1.5-) 2.5-5 cm, surfaces glabrous, sparsely pilose, or villous;</text>
      <biological_entity id="o1111" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtriangular" value_original="obtriangular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="3-cleft" value_original="3-cleft" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1112" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral segments unlobed.</text>
      <biological_entity constraint="lateral" id="o1113" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 5-9, yellow or yellowish;</text>
      <biological_entity id="o1114" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1115" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="9" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 40-60.</text>
      <biological_entity id="o1116" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1117" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s11" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pedicels 5-10 cm in fruit.</text>
      <biological_entity id="o1118" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o1119" from="5" from_unit="cm" name="some_measurement" src="d0_s12" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1119" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Achenes: body ca. 5 mm.</text>
      <biological_entity id="o1120" name="achene" name_original="achenes" src="d0_s13" type="structure" />
      <biological_entity id="o1121" name="body" name_original="body" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forest margins, alpine meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forest margins" />
        <character name="habitat" value="alpine meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2700-4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="2700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5c.</number>
  
</bio:treatment>