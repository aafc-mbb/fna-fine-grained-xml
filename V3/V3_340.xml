<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">urticaceae</taxon_name>
    <taxon_name authority="Torrey" date="1857" rank="genus">HESPEROCNIDE</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 139. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family urticaceae;genus HESPEROCNIDE</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hesperos, west, and knide, nettle</other_info_on_name>
    <other_info_on_name type="fna_id">115234</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with stinging and nonstinging hairs.</text>
      <biological_entity id="o12634" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o12635" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="toxicity" src="d0_s0" value="stinging" value_original="stinging" />
      </biological_entity>
      <relation from="o12634" id="r1762" name="with" negation="false" src="d0_s0" to="o12635" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually branched, erect, spreading, or reclining.</text>
      <biological_entity id="o12636" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="reclining" value_original="reclining" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="reclining" value_original="reclining" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o12637" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules present.</text>
      <biological_entity id="o12638" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades ovate to broadly ovate, distal blades sometimes broadly elliptic, margins serrate;</text>
      <biological_entity id="o12639" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="broadly ovate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12640" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes broadly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o12641" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cystoliths elongate.</text>
      <biological_entity id="o12642" name="cystolith" name_original="cystoliths" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, globose, nearly globose, or elongate-racemose or paniculate.</text>
      <biological_entity id="o12643" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s6" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s6" value="globose" value_original="globose" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="elongate-racemose" value_original="elongate-racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, staminate and pistillate in loose to dense clusters in same inflorescence;</text>
      <biological_entity id="o12644" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character constraint="in inflorescence" constraintid="o12645" is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o12645" name="inflorescence" name_original="inflorescence" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracts absent.</text>
      <biological_entity id="o12646" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate flowers: tepals 4, distinct, equal;</text>
      <biological_entity id="o12647" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o12648" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 4;</text>
      <biological_entity id="o12649" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o12650" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillode present.</text>
      <biological_entity id="o12651" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o12652" name="pistillode" name_original="pistillode" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: tepals 4, connate, forming persistent saclike structure covered with delicate, hooked hairs and completely enclosing mature, flattened achene;</text>
      <biological_entity id="o12653" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o12654" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o12655" name="structure" name_original="structure" src="d0_s12" type="structure">
        <character is_modifier="true" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="shape" src="d0_s12" value="sac-like" value_original="saclike" />
      </biological_entity>
      <biological_entity id="o12656" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s12" value="delicate" value_original="delicate" />
        <character is_modifier="true" name="shape" src="d0_s12" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity id="o12657" name="achene" name_original="achene" src="d0_s12" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s12" value="mature" value_original="mature" />
        <character is_modifier="true" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o12654" id="r1763" name="forming" negation="false" src="d0_s12" to="o12655" />
      <relation from="o12654" id="r1764" name="covered with" negation="false" src="d0_s12" to="o12656" />
      <relation from="o12654" id="r1765" modifier="completely" name="enclosing" negation="false" src="d0_s12" to="o12657" />
    </statement>
    <statement id="d0_s13">
      <text>staminodes absent;</text>
      <biological_entity id="o12658" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o12659" name="staminode" name_original="staminodes" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style absent, stigma tufted, persistent.</text>
      <biological_entity id="o12660" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o12661" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12662" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s14" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes subsessile, laterally compressed, ovoid, tightly enclosed in persistent tepals.</text>
      <biological_entity id="o12664" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="true" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o12663" id="r1766" modifier="tightly" name="enclosed in" negation="false" src="d0_s15" to="o12664" />
    </statement>
    <statement id="d0_s16">
      <text>x = 12.</text>
      <biological_entity id="o12663" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity constraint="x" id="o12665" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>1 in Calif. and Mexico, 1 in Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="1 in Calif. and Mexico" establishment_means="native" />
        <character name="distribution" value="1 in Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Species 2 (1 in the flora).</discussion>
  <references>
    <reference>Woodland, D. W., I. J. Bassett, and C. W. Crompton. 1976. The annual species of stinging nettle (Hesperocnide and Urtica) in North America. Canad. J. Bot. 54: 374-383.</reference>
  </references>
  
</bio:treatment>