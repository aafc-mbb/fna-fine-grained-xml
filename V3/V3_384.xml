<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray in A. Gray et al." date="1895" rank="subgenus">Cyrtorhyncha</taxon_name>
    <taxon_name authority="(A. Gray) L. D. Benson" date="1936" rank="section">Pseudaphanostemma</taxon_name>
    <taxon_name authority="A. Gray" date="1867" rank="species">hystriculus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 328. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus cyrtorhyncha;section pseudaphanostemma;species hystriculus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501164</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kumlienia</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">hystriculus</taxon_name>
    <taxon_hierarchy>genus Kumlienia;species hystriculus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect from short caudices, not rooting nodally, glabrous, not bulbous-based.</text>
      <biological_entity id="o20424" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="from caudices" constraintid="o20425" is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not; nodally" name="architecture" notes="" src="d0_s0" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbous-based" value_original="bulbous-based" />
      </biological_entity>
      <biological_entity id="o20425" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Tuberous roots absent.</text>
      <biological_entity id="o20426" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaf-blades semicircular or reniform in outline, shallowly 5-7-lobed, 1.2-4.6 × 1.8-6.6 cm, ultimate segments semicircular, margins crenate, apex rounded or weakly apiculate;</text>
      <biological_entity id="o20427" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o20428" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="semicircular" value_original="semicircular" />
        <character constraint="in outline" constraintid="o20429" is_modifier="false" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="shallowly" name="shape" notes="" src="d0_s2" value="5-7-lobed" value_original="5-7-lobed" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s2" to="4.6" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s2" to="6.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20429" name="outline" name_original="outline" src="d0_s2" type="structure" />
      <biological_entity constraint="ultimate" id="o20430" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="semicircular" value_original="semicircular" />
      </biological_entity>
      <biological_entity id="o20431" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o20432" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves 0-2, scalelike.</text>
      <biological_entity id="o20433" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o20434" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="2" />
        <character is_modifier="false" name="shape" src="d0_s3" value="scale-like" value_original="scalelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: receptacle glabrous;</text>
      <biological_entity id="o20435" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o20436" name="receptacle" name_original="receptacle" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals spreading, white or pale-yellow, 6-13 × 3-6 mm, glabrous;</text>
      <biological_entity id="o20437" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o20438" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 8-12, greenish, 2-4 × 0.6-1.6 mm.</text>
      <biological_entity id="o20439" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o20440" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="12" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s6" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads of achenes ovoid, 6-7 × 6-8 mm;</text>
      <biological_entity id="o20441" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20442" name="achene" name_original="achenes" src="d0_s7" type="structure" />
      <relation from="o20441" id="r2754" name="part_of" negation="false" src="d0_s7" to="o20442" />
    </statement>
    <statement id="d0_s8">
      <text>achenes 3.8-4.2 × 0.8-1 mm, canescent;</text>
      <biological_entity id="o20443" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s8" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak persistent, filiform, 1.2-1.4 mm, hooked distally.</text>
      <biological_entity id="o20444" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s9" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–summer (Feb–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="winter" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet places near streams, especially around waterfalls</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet places" constraint="near streams" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="waterfalls" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>67.</number>
  <discussion>Ranunculus hystriculus is endemic to the west slope of the Sierra Nevada.</discussion>
  
</bio:treatment>