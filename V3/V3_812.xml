<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="de Candolle" date="1824" rank="section">Echinella</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="1838" rank="species">hebecarpus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>316. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section echinella;species hebecarpus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501156</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, pilose.</text>
      <biological_entity id="o24808" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal and lower cauline leaf-blades cordate-reniform, deeply 3-parted, 0.6-2.3 × 1.2-3.5 cm, segments undivided or 2-4-lobed, base shallowly cordate, margins entire or 2-4-dentate, apex of ultimate segments acute.</text>
      <biological_entity constraint="basal and lower cauline" id="o24809" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="cordate-reniform" value_original="cordate-reniform" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s1" value="3-parted" value_original="3-parted" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s1" to="2.3" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s1" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24810" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-4-lobed" value_original="2-4-lobed" />
      </biological_entity>
      <biological_entity id="o24811" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s1" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o24812" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-4-dentate" value_original="2-4-dentate" />
      </biological_entity>
      <biological_entity id="o24813" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o24814" name="segment" name_original="segments" src="d0_s1" type="structure" />
      <relation from="o24813" id="r3378" name="part_of" negation="false" src="d0_s1" to="o24814" />
    </statement>
    <statement id="d0_s2">
      <text>Flowers pedicellate;</text>
      <biological_entity id="o24815" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>receptacle glabrous;</text>
      <biological_entity id="o24816" name="receptacle" name_original="receptacle" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals 5, spreading, 1.1-1.8 × 0.5-1 mm, pilose;</text>
      <biological_entity id="o24817" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s4" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 0-5, 1.3-2 × 0.3-0.7 mm.</text>
      <biological_entity id="o24818" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="5" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads of achenes discoid, 4-6 × 3 mm;</text>
      <biological_entity id="o24819" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o24820" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <relation from="o24819" id="r3379" name="part_of" negation="false" src="d0_s6" to="o24820" />
    </statement>
    <statement id="d0_s7">
      <text>achenes 4-9 per head, 1.7-2.3 × 1.7-2 mm, faces and margin papillose, each papilla crowned with hooked bristle, otherwise glabrous;</text>
      <biological_entity id="o24821" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per head" constraintid="o24822" from="4" name="quantity" src="d0_s7" to="9" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" notes="" src="d0_s7" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" notes="" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24822" name="head" name_original="head" src="d0_s7" type="structure" />
      <biological_entity id="o24823" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o24824" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o24825" name="papilla" name_original="papilla" src="d0_s7" type="structure">
        <character constraint="with bristle" constraintid="o24826" is_modifier="false" name="architecture" src="d0_s7" value="crowned" value_original="crowned" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24826" name="bristle" name_original="bristle" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>beak lanceolate, hooked distally, 0.5-0.7 mm.</text>
      <biological_entity id="o24827" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s8" value="hooked" value_original="hooked" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, open woodlands, and chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Oreg., Wash.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <discussion>Ranunculus hebecarpus is native to western North America.</discussion>
  
</bio:treatment>