<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">delphinium</taxon_name>
    <taxon_name authority="Huth" date="1895" rank="section">Diedropetala</taxon_name>
    <taxon_name authority="M. J. Warnock" date="1995" rank="subsection">multiplex</taxon_name>
    <taxon_name authority="Leiberg" date="1897" rank="species">viridescens</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>11: 39. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus delphinium;section diedropetala;subsection multiplex;species viridescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500562</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 90-150 cm;</text>
      <biological_entity id="o25580" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="90" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base usually green, glabrous.</text>
      <biological_entity id="o25581" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, 17-30 at anthesis;</text>
      <biological_entity id="o25582" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" constraint="at anthesis" from="17" name="quantity" src="d0_s2" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.2-8 cm.</text>
      <biological_entity id="o25583" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blade cuneate to semicircular, 2-5 × 3-12 cm, nearly glabrous;</text>
      <biological_entity id="o25584" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="semicircular" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 3-21, width 1-8 mm.</text>
      <biological_entity constraint="ultimate" id="o25585" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="21" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 25-80-flowered, dense;</text>
      <biological_entity id="o25586" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="25-80-flowered" value_original="25-80-flowered" />
        <character is_modifier="false" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicel 0.5-2 cm, glandular-pubescent;</text>
      <biological_entity id="o25587" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 1-4 mm from flowers, green, lanceolate, 3.5-6 mm, glandular-pubescent.</text>
      <biological_entity id="o25588" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="from flowers" constraintid="o25589" from="1" from_unit="mm" name="location" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o25589" name="flower" name_original="flowers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals yellowish green, nearly glabrous, lateral sepals forward pointing, 7-9 × 3-4 mm, spurs decurved, 30-45° below horizontal, often hooked apically, 8-11 mm;</text>
      <biological_entity id="o25590" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25591" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o25592" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="forward" name="orientation" src="d0_s9" value="pointing" value_original="pointing" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25593" name="spur" name_original="spurs" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="decurved" value_original="decurved" />
        <character constraint="below horizontal" name="degree" src="d0_s9" value="30-45°" value_original="30-45°" />
        <character is_modifier="false" modifier="often; apically" name="shape" src="d0_s9" value="hooked" value_original="hooked" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower petal blades ± covering stamens, 4-6 mm, clefts 0.5-1.5 mm;</text>
      <biological_entity id="o25594" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="petal" id="o25595" name="blade" name_original="blades" src="d0_s10" type="structure" constraint_original="lower petal">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25596" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o25597" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o25595" id="r3446" modifier="more or less" name="covering" negation="false" src="d0_s10" to="o25596" />
    </statement>
    <statement id="d0_s11">
      <text>hairs centered, mostly near junction of blade and claw, yellow.</text>
      <biological_entity id="o25598" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o25599" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="centered" value_original="centered" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o25600" name="junction" name_original="junction" src="d0_s11" type="structure" />
      <biological_entity id="o25601" name="blade" name_original="blade" src="d0_s11" type="structure" />
      <biological_entity id="o25602" name="claw" name_original="claw" src="d0_s11" type="structure" />
      <relation from="o25599" id="r3447" modifier="mostly" name="near" negation="false" src="d0_s11" to="o25600" />
      <relation from="o25600" id="r3448" name="part_of" negation="false" src="d0_s11" to="o25601" />
      <relation from="o25600" id="r3449" name="part_of" negation="false" src="d0_s11" to="o25602" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits 8-11 mm, 2.5-3 times longer than wide, puberulent.</text>
      <biological_entity id="o25603" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="2.5-3" value_original="2.5-3" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds ± wing-margined;</text>
      <biological_entity id="o25604" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s13" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat cells with surfaces ± roughened.</text>
      <biological_entity constraint="seed-coat" id="o25605" name="cell" name_original="cells" src="d0_s14" type="structure" />
      <biological_entity id="o25606" name="surface" name_original="surfaces" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief_or_texture" src="d0_s14" value="roughened" value_original="roughened" />
      </biological_entity>
      <relation from="o25605" id="r3450" name="with" negation="false" src="d0_s14" to="o25606" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows and streamsides in coniferous forest, heavy clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" constraint="in coniferous forest , heavy clay soils" />
        <character name="habitat" value="streamsides" constraint="in coniferous forest , heavy clay soils" />
        <character name="habitat" value="coniferous forest" />
        <character name="habitat" value="heavy clay soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Wenatchee larkspur</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Delphinium viridescens is local in mountains southwest of Wenatchee, Washington.</discussion>
  
</bio:treatment>