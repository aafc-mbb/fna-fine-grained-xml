<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Salisbury" date="1807" rank="genus">coptis</taxon_name>
    <taxon_name authority="Salisbury" date="1807" rank="species">aspleniifolia</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>8:306. 1807</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus coptis;species aspleniifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500423</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes pale-brown.</text>
      <biological_entity id="o16151" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade 2-pinnate with pinnatifid leaflets to 3-pinnate, occasionally 2-ternate;</text>
      <biological_entity id="o16152" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o16153" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character constraint="with leaflets" constraintid="o16154" is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" modifier="occasionally" name="architecture" notes="" src="d0_s1" value="2-ternate" value_original="2-ternate" />
      </biological_entity>
      <biological_entity id="o16154" name="leaflet" name_original="leaflets" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="3-pinnate" value_original="3-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets short to long-petiolulate, blade ovate, deeply lobed or incised, margins sharply serrate.</text>
      <biological_entity id="o16155" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16156" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="long-petiolulate" value_original="long-petiolulate" />
      </biological_entity>
      <biological_entity id="o16157" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o16158" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 2-3-flowered, often longer than leaves at anthesis, 8-12 cm, elongating to 35 cm in fruit.</text>
      <biological_entity id="o16159" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-3-flowered" value_original="2-3-flowered" />
        <character constraint="than leaves" constraintid="o16160" is_modifier="false" name="length_or_size" src="d0_s3" value="often longer" value_original="often longer" />
        <character char_type="range_value" from="8" from_unit="cm" modifier="at anthesis" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
        <character constraint="in fruit" constraintid="o16161" is_modifier="false" name="length" src="d0_s3" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o16160" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16161" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers nodding;</text>
      <biological_entity id="o16162" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals reflexed and ascending, linear-lanceolate, 6-11 (-15) × 0.3-1 mm;</text>
      <biological_entity id="o16163" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals linear-lanceolate, nectary nearly basal, blade flattened, narrowly ligulate at apex;</text>
      <biological_entity id="o16164" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o16165" name="nectary" name_original="nectary" src="d0_s6" type="structure" />
      <biological_entity id="o16166" name="nectary" name_original="nectary" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="nearly" name="position" src="d0_s6" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o16167" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
        <character constraint="at apex" constraintid="o16168" is_modifier="false" modifier="narrowly" name="architecture" src="d0_s6" value="ligulate" value_original="ligulate" />
      </biological_entity>
      <biological_entity id="o16168" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>stamens 9-15.</text>
      <biological_entity id="o16169" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s7" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Follicles 6-10;</text>
      <biological_entity id="o16170" name="follicle" name_original="follicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stipe equal to or slightly longer than body;</text>
      <biological_entity id="o16171" name="stipe" name_original="stipe" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="than body" constraintid="o16172" is_modifier="false" name="length_or_size" src="d0_s9" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o16172" name="body" name_original="body" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>body oblong, 7-10 mm;</text>
      <biological_entity id="o16173" name="body" name_original="body" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>beak recurved, less than 1mm.</text>
      <biological_entity id="o16174" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 1.8-2.2 mm. 2n=18.</text>
      <biological_entity id="o16175" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16176" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, coniferous forests, seeps, and bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous forests" modifier="moist" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>This species is widespread in coastal areas from southern British Columbia to southeastern Alaska. The Washington State Heritage Program tracks this species as "state-rare" in Snohomish County, Washington; I have not seen any specimens to confirm its presence in the state.</discussion>
  <discussion>Coptis aspleniifolia, C. laciniata, and C. occidentalis form a group of morphologically similar, allopatric species that are probably recently derived. The species may have originated in response to the opening of the western Cordilleran landscape after Pleistocene glaciation and could be considered localized variants of a single species. Although most individuals can be readily distinguished, some can be difficult to place.</discussion>
  <discussion>A putative hybrid between Coptis aspleniifolia and C. trifolia has been found along the Kennedy River of Vancouver Island, British Columbia (T.C. Brayshaw, pers. comm.). It has 3-5 deeply dissected leaflets per leaf and no complete flowers.</discussion>
  
</bio:treatment>