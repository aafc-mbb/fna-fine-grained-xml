<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">papaveraceae</taxon_name>
    <taxon_name authority="R. Brown in D. Denham and H. Clapperton" date="1767" rank="genus">macleaya</taxon_name>
    <taxon_name authority="(Willdenow) R. Brown in D. Denham and H. Clapperton" date="1826" rank="species">cordata</taxon_name>
    <place_of_publication>
      <publication_title>in D. Denham and H. Clapperton,  Narr. Travels Africa, app.,</publication_title>
      <place_in_publication>218. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family papaveraceae;genus macleaya;species cordata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200009159</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bocconia</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">cordata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2(2): 841. 1799</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bocconia;species cordata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 25 dm.</text>
      <biological_entity id="o19456" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="25" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or branching distally.</text>
      <biological_entity id="o19457" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 10-30 cm long and wide;</text>
      <biological_entity id="o19458" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s2" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2-15 (-20) cm;</text>
      <biological_entity id="o19459" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly ovate-cordate, 7-9-lobed;</text>
      <biological_entity id="o19460" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="ovate-cordate" value_original="ovate-cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="7-9-lobed" value_original="7-9-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins irregularly and coarsely toothed;</text>
      <biological_entity id="o19461" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial surface usually whitish and densely short-pubescent, adaxial glabrous.</text>
      <biological_entity constraint="abaxial" id="o19462" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="short-pubescent" value_original="short-pubescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19463" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences to 30 cm or more.</text>
      <biological_entity id="o19464" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: pedicels 4-10 mm;</text>
      <biological_entity id="o19465" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o19466" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals white to cream colored, spatulate, 5-10 mm;</text>
      <biological_entity id="o19467" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19468" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="cream colored" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style to ca. 1 mm.</text>
      <biological_entity id="o19469" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19470" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules oblanceoloid, strongly compressed, 15-20 mm, glabrous.</text>
      <biological_entity id="o19471" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblanceoloid" value_original="oblanceoloid" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds dark-brown, reticulate.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 20.</text>
      <biological_entity id="o19472" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s12" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19473" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous woods, thickets, old fields, ditches, roadsides, pond margins, and along watercourses</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous woods" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="margins" modifier="pond" />
        <character name="habitat" value="watercourses" modifier="and along" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont., Que.; Ala., Conn., Ill., Ind., Maine, Mass., Mich., N.J., N.Y., N.C., Ohio, Pa., Va., W.Va.; e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>A garden perennial esteemed for its vegetative features and large, plumelike inflorescence, Macleaya cordata occasionally escapes from cultivation, persisting and reproducing fairly successfully. It might be found almost anywhere in temperate North America east of the Mississippi River at elevations below 1000 m.</discussion>
  
</bio:treatment>