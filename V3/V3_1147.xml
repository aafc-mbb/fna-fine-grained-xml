<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">nymphaeaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">nymphaea</taxon_name>
    <taxon_name authority="Aiton" date="1789" rank="species">odorata</taxon_name>
    <taxon_name authority="(Paine) Wiersema &amp; Hellquist" date="1994" rank="subspecies">tuberosa</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>96: 170. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nymphaeaceae;genus nymphaea;species odorata;subspecies tuberosa</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500830</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nymphaea</taxon_name>
    <taxon_name authority="Paine" date="unknown" rank="species">tuberosa</taxon_name>
    <place_of_publication>
      <publication_title>Cat. Pl. Oneida Co.,</publication_title>
      <place_in_publication>132. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Nymphaea;species tuberosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes often constricted at branch joints to form detachable tubers.</text>
      <biological_entity id="o26624" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character constraint="at branch joints" constraintid="o26625" is_modifier="false" modifier="often" name="size" src="d0_s0" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity constraint="branch" id="o26625" name="joint" name_original="joints" src="d0_s0" type="structure" />
      <biological_entity id="o26626" name="tuber" name_original="tubers" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s0" value="detachable" value_original="detachable" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole green with brown-purple stripes, stout.</text>
      <biological_entity id="o26627" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o26628" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character constraint="with brown-purple stripes" is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade abaxially green or faintly purple.</text>
      <biological_entity id="o26629" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="faintly" name="coloration" src="d0_s2" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: petals white, rarely pink, elliptic to oblanceolate, outer usually with broadly rounded apex.</text>
      <biological_entity id="o26630" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o26631" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s3" value="pink" value_original="pink" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="oblanceolate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o26632" name="petal" name_original="petals" src="d0_s3" type="structure" />
      <biological_entity id="o26633" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o26632" id="r3591" name="with" negation="false" src="d0_s3" to="o26633" />
    </statement>
    <statement id="d0_s4">
      <text>Seeds mostly 2.8-4.5 mm.</text>
      <biological_entity id="o26634" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s4" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mainly alkaline ponds, lakes, and sluggish streams and rivers, usually in very oozy sediments</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ponds" modifier="mainly alkaline" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="sluggish streams" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="oozy sediments" modifier="usually in very" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Ont., Que.; Conn., Ill., Ind., Iowa, Kans., Maine, Mass., Mich., Minn., Mo., Nebr., N.H., N.Y., Ohio, Okla., Pa., Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9b.</number>
  <other_name type="common_name">Nymphéa tubéreux</other_name>
  <discussion>This taxon, which has been included within Nymphaea odorata by some recent workers, was formerly almost universally accepted as a distinct species. In the southern parts of the range of subsp. tuberosa, where subsp. odorata is absent, subsp. tuberosa is easily distinguished morphologically from subsp. odorata. Farther north, where their ranges overlap, the distinctions break down in some populations but are maintained in others. Some western populations are probably the result of introductions. A pink-flowered form seen in southeastern Ohio appears to be derived from this subspecies.</discussion>
  
</bio:treatment>