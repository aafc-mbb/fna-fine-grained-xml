<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Marshall" date="1785" rank="genus">xanthorhiza</taxon_name>
    <taxon_name authority="Marshall" date="1785" rank="species">simplicissima</taxon_name>
    <place_of_publication>
      <publication_title>Arbust. Amer.,</publication_title>
      <place_in_publication>167. 1785</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus xanthorhiza;species simplicissima</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220014377</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xanthorhiza</taxon_name>
    <taxon_name authority="L'Héritier" date="unknown" rank="species">apiifolia</taxon_name>
    <taxon_hierarchy>genus Xanthorhiza;species apiifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 20-70 cm, 3-6mm diam.;</text>
      <biological_entity id="o22683" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s0" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bark smooth, ringed with leaf-scars, inner bark yellow.</text>
      <biological_entity id="o22684" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character constraint="with leaf-scars" constraintid="o22685" is_modifier="false" name="relief" src="d0_s1" value="ringed" value_original="ringed" />
      </biological_entity>
      <biological_entity id="o22685" name="leaf-scar" name_original="leaf-scars" src="d0_s1" type="structure" />
      <biological_entity constraint="inner" id="o22686" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves clustered near stem apex, to 18cm;</text>
      <biological_entity id="o22687" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="near stem apex" constraintid="o22688" is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o22688" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>leaflets 3-5, 2.5-10 × 2-8 cm, sessile to short-petiolulate.</text>
      <biological_entity id="o22689" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s3" to="short-petiolulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences broad-paniculate, arising from cluster of leaves, 6-21 cm, short-pilose;</text>
      <biological_entity id="o22690" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="broad-paniculate" value_original="broad-paniculate" />
        <character constraint="of leaves" constraintid="o22691" is_modifier="false" name="orientation" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="21" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="short-pilose" value_original="short-pilose" />
      </biological_entity>
      <biological_entity id="o22691" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>pedicel 2-5 mm.</text>
      <biological_entity id="o22692" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals spreading, acuminate;</text>
      <biological_entity id="o22693" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o22694" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals with nectary transversely oblong, 2-lobed.</text>
      <biological_entity id="o22695" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o22696" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o22697" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
      <relation from="o22696" id="r3091" name="with" negation="false" src="d0_s7" to="o22697" />
    </statement>
    <statement id="d0_s8">
      <text>Follicles yellowish-brown, glossy, somewhat inflated, 3-4 mm, distally ciliate.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n=36.</text>
      <biological_entity id="o22698" name="follicle" name_original="follicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="reflectance" src="d0_s8" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22699" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Apr–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded stream banks, moist woods, thickets, and rocky ledges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded stream banks" />
        <character name="habitat" value="moist woods" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="rocky ledges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Ky., La., Maine, Md., Mass., Miss., N.H., N.Y., N.C., Ohio, S.C., Tenn., Tex., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Yellowroot</other_name>
  <other_name type="common_name">shrub yellowroot</other_name>
  <other_name type="common_name">brook-feather</other_name>
  <discussion>Xanthorhiza simplicissima is cultivated as an effective ground cover in moist soils. In the northeastern and midwestern United States it occasionally escapes from cultivation, and either persists or becomes established. In southeastern Texas and western Louisiana the species is apparently native, though infrequent. The leaves bear a striking resemblance to those of celery, hence apiifolia, the epithet in synonymy.</discussion>
  <discussion>The yellow inner bark and roots contain a bitter principle; the roots have been used medicinally and in making yellow dye. Bundles of yellowroot are widely sold in Alabama and Georgia for treatment of gastrointestinal disorders and fever blisters (R. D. Whetstone, pers. comm.).</discussion>
  <discussion>Native Americans used Xanthorhiza simplicissima medicinally to treat ulcerated stomachs, colds, jaundice, piles, sore mouth, sore throat, cancer, and cramps, and as a blood tonic (D. E. Moerman 1986).</discussion>
  
</bio:treatment>