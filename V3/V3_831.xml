<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">anemone</taxon_name>
    <taxon_name authority="Tharp" date="1945" rank="species">edwardsiana</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>33: 669. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus anemone;species edwardsiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500058</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial shoots (20-) 30-50 cm, from tubers, tubers ±vertical.</text>
      <biological_entity id="o19018" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19019" name="tuber" name_original="tubers" src="d0_s0" type="structure" />
      <biological_entity id="o19020" name="tuber" name_original="tubers" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o19018" id="r2558" name="from" negation="false" src="d0_s0" to="o19019" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 3-6, 1- (nearly 2) -ternate;</text>
      <biological_entity constraint="basal" id="o19021" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-ternate" value_original="1-ternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 8-15 cm;</text>
      <biological_entity id="o19022" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet petiolulate, oblanceolate to obovate, 2-5 × 2-2.5 cm, base broadly cuneate, margins coarsely crenate on distal 1/2, apex obtuse to broadly acute, surfaces glabrous;</text>
      <biological_entity constraint="terminal" id="o19023" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolulate" value_original="petiolulate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19024" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19025" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="on distal 1/2" constraintid="o19026" is_modifier="false" modifier="coarsely" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19026" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <biological_entity id="o19027" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="broadly acute" />
      </biological_entity>
      <biological_entity id="o19028" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lateral leaflets 1×-lobed;</text>
      <biological_entity constraint="lateral" id="o19029" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="1×-lobed" value_original="1×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ultimate lobes 4-8 (-12) mm wide.</text>
      <biological_entity constraint="ultimate" id="o19030" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences (1-) 2-5-flowered cymes;</text>
      <biological_entity id="o19031" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o19032" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="(1-)2-5-flowered" value_original="(1-)2-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle proximally nearly glabrous, distally densely pubescent;</text>
      <biological_entity id="o19033" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally nearly" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts primarily 3, 2-tiered, simple, dissimilar to basal leaves, 3-cleft to pinnatifid, 2-5 cm, bases clasping, ±connate, margins coarsely crenate throughout, apex narrowly acute to acuminate, surfaces glabrous;</text>
      <biological_entity id="o19034" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="2-tiered" value_original="2-tiered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character char_type="range_value" from="3-cleft" name="shape" notes="" src="d0_s8" to="pinnatifid" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o19035" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o19036" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s8" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o19037" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="coarsely; throughout" name="shape" src="d0_s8" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o19038" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
      <biological_entity id="o19039" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o19034" id="r2559" name="to" negation="false" src="d0_s8" to="o19035" />
    </statement>
    <statement id="d0_s9">
      <text>segments primarily 3, linear;</text>
      <biological_entity id="o19040" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral segments unlobed or occasionally 1×-lobed;</text>
      <biological_entity constraint="lateral" id="o19041" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s10" value="1×-lobed" value_original="1×-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ultimate lobes 3-5 mm wide.</text>
      <biological_entity constraint="ultimate" id="o19042" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals (8-) 10-20, white to bluish, oblanceolate, 10-16 × 2-3 mm, sparsely hairy to nearly glabrous;</text>
      <biological_entity id="o19043" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o19044" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s12" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="20" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="bluish" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s12" to="16" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="sparsely hairy" name="pubescence" src="d0_s12" to="nearly glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 50-60.</text>
      <biological_entity id="o19045" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19046" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s13" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Heads of achenes obconic to fusiform;</text>
      <biological_entity id="o19047" name="head" name_original="heads" src="d0_s14" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s14" to="fusiform" />
      </biological_entity>
      <biological_entity id="o19048" name="achene" name_original="achenes" src="d0_s14" type="structure" />
      <relation from="o19047" id="r2560" name="part_of" negation="false" src="d0_s14" to="o19048" />
    </statement>
    <statement id="d0_s15">
      <text>pedicel 10-20 cm.</text>
      <biological_entity id="o19049" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s15" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes: body orbiculate, flat, not winged, ±dull or varnished, woolly with long tuft of hairs at base, or glabrous;</text>
      <biological_entity id="o19050" name="achene" name_original="achenes" src="d0_s16" type="structure" />
      <biological_entity id="o19051" name="body" name_original="body" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="more or less" name="reflectance" src="d0_s16" value="dull" value_original="dull" />
        <character is_modifier="false" name="reflectance" src="d0_s16" value="varnished" value_original="varnished" />
        <character constraint="with tuft" constraintid="o19052" is_modifier="false" name="pubescence" src="d0_s16" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19052" name="tuft" name_original="tuft" src="d0_s16" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s16" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o19053" name="hair" name_original="hairs" src="d0_s16" type="structure" />
      <biological_entity id="o19054" name="base" name_original="base" src="d0_s16" type="structure" />
      <relation from="o19052" id="r2561" name="part_of" negation="false" src="d0_s16" to="o19053" />
      <relation from="o19052" id="r2562" name="at" negation="false" src="d0_s16" to="o19054" />
    </statement>
    <statement id="d0_s17">
      <text>beak straight, rarely curved, 0.5-1 mm, glabrous or sparsely villous proximally, not plumose.</text>
      <biological_entity id="o19055" name="achene" name_original="achenes" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n=16.</text>
      <biological_entity id="o19056" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s17" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s17" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19057" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Anemone edwardsiana is found only in Texas on the Edwards Plateau.</discussion>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achenes woolly, ± dull.</description>
      <determination>7a Anemone edwardsiana var. edwardsiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achenes glabrous, varnished.</description>
      <determination>7b Anemone edwardsiana var. petraea</determination>
    </key_statement>
  </key>
</bio:treatment>