<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">betulaceae</taxon_name>
    <taxon_name authority="Koehne" date="1893" rank="subfamily">BETULOIDEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">alnus</taxon_name>
    <taxon_name authority="(Linnaeus) Gaertner" date="1790" rank="species">glutinosa</taxon_name>
    <place_of_publication>
      <publication_title>Fruct. Sem. Pl.</publication_title>
      <place_in_publication>2: 54. 1790</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family betulaceae;subfamily betuloideae;genus alnus;species glutinosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="fna_id">233500032</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Betula</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">alnus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="variety">(a) glutinosa</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 983. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Betula;species alnus;variety (a) glutinosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 20 m;</text>
      <biological_entity id="o7371" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunks often several, crowns narrow.</text>
      <biological_entity id="o7372" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="quantity" src="d0_s1" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o7373" name="crown" name_original="crowns" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bark dark-brown, smooth, becoming darker and breaking into shallow fissures in age;</text>
      <biological_entity id="o7374" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s2" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o7375" name="fissure" name_original="fissures" src="d0_s2" type="structure">
        <character is_modifier="true" name="depth" src="d0_s2" value="shallow" value_original="shallow" />
      </biological_entity>
      <relation from="o7374" id="r1023" name="breaking into" negation="false" src="d0_s2" to="o7375" />
    </statement>
    <statement id="d0_s3">
      <text>lenticels pale, horizontal.</text>
      <biological_entity id="o7376" name="lenticel" name_original="lenticels" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale" value_original="pale" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Winter buds stipitate, ellipsoid to obovoid, 6–10 mm, apex obtuse;</text>
      <biological_entity id="o7377" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="true" name="season" src="d0_s4" value="winter" value_original="winter" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s4" to="obovoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7378" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stalks 2–5 mm;</text>
      <biological_entity id="o7379" name="stalk" name_original="stalks" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scales 2–3, outer 2 equal, valvate, usually heavily resin-coated.</text>
      <biological_entity id="o7380" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o7381" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s6" value="valvate" value_original="valvate" />
        <character is_modifier="false" modifier="usually heavily" name="coating" src="d0_s6" value="resin-coated" value_original="resin-coated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaf-blade obovate to nearly orbiculate, 3–9 × 3–8 cm, leathery, base obtuse to broadly cuneate, margins flat, coarsely and often irregularly doubly serrate to nearly dentate, major teeth acute to obtuse or rounded, apex often retuse or obcordate, or occasionally rounded;</text>
      <biological_entity id="o7382" name="leaf-blade" name_original="leaf-blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s7" to="nearly orbiculate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="9" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s7" to="8" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o7383" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="broadly cuneate" />
      </biological_entity>
      <biological_entity id="o7384" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character char_type="range_value" from="often irregularly doubly serrate" modifier="coarsely" name="architecture_or_shape" src="d0_s7" to="nearly dentate" />
      </biological_entity>
      <biological_entity id="o7385" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="major" value_original="major" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="obtuse or rounded" />
      </biological_entity>
      <biological_entity id="o7386" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obcordate" value_original="obcordate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obcordate" value_original="obcordate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>surfaces abaxially glabrous to sparsely pubescent, often more heavily on veins, both surfaces heavily resin-coated.</text>
      <biological_entity id="o7387" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character char_type="range_value" from="abaxially glabrous" name="pubescence" src="d0_s8" to="sparsely pubescent" />
      </biological_entity>
      <biological_entity id="o7388" name="vein" name_original="veins" src="d0_s8" type="structure" />
      <biological_entity id="o7389" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="heavily" name="coating" src="d0_s8" value="resin-coated" value_original="resin-coated" />
      </biological_entity>
      <relation from="o7387" id="r1024" modifier="often; heavily" name="on" negation="false" src="d0_s8" to="o7388" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences formed season before flowering and exposed during winter;</text>
      <biological_entity id="o7390" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character constraint="during winter" is_modifier="false" name="prominence" src="d0_s9" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity id="o7391" name="season" name_original="season" src="d0_s9" type="structure" />
      <relation from="o7390" id="r1025" name="formed" negation="false" src="d0_s9" to="o7391" />
    </statement>
    <statement id="d0_s10">
      <text>staminate catkins in 1 or more clusters of 2–5, 4–13 cm;</text>
      <biological_entity id="o7392" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_unit="cm" modifier="of 2-5" name="some_measurement" src="d0_s10" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate catkins in 1 or more clusters of 2–5.</text>
    </statement>
    <statement id="d0_s12">
      <text>Flowering before new growth in spring.</text>
      <biological_entity id="o7393" name="catkin" name_original="catkins" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character constraint="before growth" constraintid="o7394" is_modifier="false" name="life_cycle" src="d0_s12" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity id="o7394" name="growth" name_original="growth" src="d0_s12" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s12" value="new" value_original="new" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Infructescences ovoid to nearly globose, 1.2–2.5 × 1–1.5 cm;</text>
      <biological_entity id="o7395" name="infructescence" name_original="infructescences" src="d0_s13" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s13" to="nearly globose" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s13" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s13" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peduncles 1–10 (–20) mm.</text>
      <biological_entity id="o7396" name="peduncle" name_original="peduncles" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Samaras obovate, wings reduced to narrow, thickened ridges.</text>
      <biological_entity id="o7397" name="samara" name_original="samaras" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o7398" name="wing" name_original="wings" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="reduced to narrow" value_original="reduced to narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 28.</text>
      <biological_entity id="o7399" name="ridge" name_original="ridges" src="d0_s15" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s15" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7400" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, moist flood plains, damp depressions, borders of wetlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="moist flood plains" />
        <character name="habitat" value="damp depressions" />
        <character name="habitat" value="borders" constraint="of wetlands" />
        <character name="habitat" value="wetlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Conn., Ill., Ind., Iowa, Mass., Mich., Minn., N.J., N.Y., Ohio, Pa., R.I., Wis.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Black alder</other_name>
  <other_name type="common_name">European alder</other_name>
  <discussion>Alnus glutinosa is cultivated as an ornamental tree throughout eastern North America and is available in a variety of cultivars, including cut-leafed and compact-branching forms. This species has also been used extensively to control erosion and improve the soil on recently cleared or unstable substrates, such as sand dunes and mine spoils. It has escaped and become widely naturalized throughout the temperate Northeast, occasionally becoming a weedy pest. In Europe the black alder has served for many centuries as an important source of hardwood for timbers and carved items, including wooden shoes.</discussion>
  <discussion>Alnus glutinosa has been called A. vulgaris Hill in some older literature; that name was not validly published.</discussion>
  
</bio:treatment>