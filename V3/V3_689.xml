<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="C. Morren &amp; Decaisne" date="1834" rank="genus">vancouveria</taxon_name>
    <taxon_name authority="Greene" date="1885" rank="species">chrysantha</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 66. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus vancouveria;species chrysantha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501340</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves persistent, 2-ternately compound or 3-5-foliolate, 10-18 cm;</text>
      <biological_entity id="o2193" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="2-ternately" name="architecture" src="d0_s0" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="3-5-foliolate" value_original="3-5-foliolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>petiole 3-12 cm, sparsely hairy.</text>
      <biological_entity id="o2194" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaflet blades ovate to oblong, slightly 3-lobed, leathery, base cordate, margins thickened, crisped, apex notched;</text>
      <biological_entity constraint="leaflet" id="o2195" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="oblong" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o2196" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o2197" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o2198" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>surfaces abaxially pubescent, glaucous, adaxially glabrous to rarely pubescent.</text>
      <biological_entity id="o2199" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="adaxially glabrous" name="pubescence" src="d0_s3" to="rarely pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: peduncle 2-3 dm;</text>
      <biological_entity id="o2200" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o2201" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s4" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel 1-4 cm, stipitate-glandular.</text>
      <biological_entity id="o2202" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o2203" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 4-15;</text>
      <biological_entity id="o2204" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles 6-9, tan to brown, 1-4 mm, caducous, stipitate-glandular;</text>
      <biological_entity id="o2205" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="9" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s7" to="brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s7" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 6, yellow, spatulate, 6-10 mm, stipitate-glandular;</text>
      <biological_entity id="o2206" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s8" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 6, yellow, 4-6 mm, margins entire, apex strongly reflexed, apical nectary darker yellow;</text>
      <biological_entity id="o2207" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2208" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2209" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="apical" id="o2210" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="darker yellow" value_original="darker yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments stipitate-glandular.</text>
      <biological_entity id="o2211" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Follicles greenish brown, 8-15 mm including beak, beak 3-4 mm, densely stipitate-glandular.</text>
      <biological_entity id="o2212" name="follicle" name_original="follicles" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish brown" value_original="greenish brown" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2213" name="beak" name_original="beak" src="d0_s11" type="structure" />
      <biological_entity id="o2214" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o2212" id="r274" name="including" negation="false" src="d0_s11" to="o2213" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds 3-10, reddish-brown, reniform to oblong, 3-4 mm. 2n = 12.</text>
      <biological_entity id="o2215" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="10" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="reniform" name="shape" src="d0_s12" to="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2216" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (May–Jun); fruiting spring–summer (Jun–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="May-Jun" />
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, mixed evergreen forests and thickets on serpentine substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed evergreen forests" modifier="open" constraint="on serpentine substrates" />
        <character name="habitat" value="thickets" constraint="on serpentine substrates" />
        <character name="habitat" value="serpentine substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Siskiyou inside-out flower</other_name>
  <other_name type="common_name">golden inside-out flower</other_name>
  
</bio:treatment>