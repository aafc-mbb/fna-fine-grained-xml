<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kevin C. Nixon</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">fagaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">QUERCUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 994. 175</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 431. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fagaceae;genus QUERCUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Classical Latin for the English oak, Quercus robur, from some central European language</other_info_on_name>
    <other_info_on_name type="fna_id">127839</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, evergreen or winter-deciduous, sometimes rhizomatous.</text>
      <biological_entity id="o16836" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-deciduous" value_original="winter-deciduous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-deciduous" value_original="winter-deciduous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Terminal buds spheric to ovoid, terete or angled, all scales imbricate.</text>
      <biological_entity constraint="terminal" id="o16838" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character char_type="range_value" from="spheric" name="shape" src="d0_s1" to="ovoid terete or angled" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s1" to="ovoid terete or angled" />
      </biological_entity>
      <biological_entity id="o16839" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules deciduous and inconspicuous (except in Quercus sadleriana).</text>
      <biological_entity id="o16840" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16841" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade lobed or unlobed, thin or leathery, margins entire, toothed, or awned-toothed, secondary-veins either unbranched, ± parallel, extending to margin, or branching and anastomosing before reaching margin.</text>
      <biological_entity id="o16842" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s3" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o16843" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="awned-toothed" value_original="awned-toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="awned-toothed" value_original="awned-toothed" />
      </biological_entity>
      <biological_entity id="o16844" name="secondary-vein" name_original="secondary-veins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="either" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character constraint="before margin" constraintid="o16846" is_modifier="false" name="architecture" src="d0_s3" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
      <biological_entity id="o16845" name="margin" name_original="margin" src="d0_s3" type="structure" />
      <biological_entity id="o16846" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s3" value="reaching" value_original="reaching" />
      </biological_entity>
      <relation from="o16844" id="r2312" name="extending to" negation="false" src="d0_s3" to="o16845" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences unisexual, in axils of leaves or bud-scales, usually clustered at base of new growth;</text>
      <biological_entity id="o16847" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="unisexual" value_original="unisexual" />
        <character constraint="at base" constraintid="o16851" is_modifier="false" modifier="usually" name="arrangement_or_growth_form" notes="" src="d0_s4" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o16848" name="axil" name_original="axils" src="d0_s4" type="structure" />
      <biological_entity id="o16849" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16850" name="bud-scale" name_original="bud-scales" src="d0_s4" type="structure" />
      <biological_entity id="o16851" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o16852" name="growth" name_original="growth" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="new" value_original="new" />
      </biological_entity>
      <relation from="o16847" id="r2313" name="in" negation="false" src="d0_s4" to="o16848" />
      <relation from="o16848" id="r2314" name="part_of" negation="false" src="d0_s4" to="o16849" />
      <relation from="o16848" id="r2315" name="part_of" negation="false" src="d0_s4" to="o16850" />
      <relation from="o16851" id="r2316" name="part_of" negation="false" src="d0_s4" to="o16852" />
    </statement>
    <statement id="d0_s5">
      <text>staminate inflorescences lax, spicate;</text>
      <biological_entity id="o16853" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pistillate inflorescences usually stiff, with terminal cupule and sometimes 1-several sessile, lateral cupules.</text>
      <biological_entity id="o16854" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s6" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o16855" name="cupule" name_original="cupule" src="d0_s6" type="structure" />
      <biological_entity constraint="lateral" id="o16856" name="cupule" name_original="cupules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="several" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o16854" id="r2317" name="with" negation="false" src="d0_s6" to="o16855" />
      <relation from="o16854" id="r2318" name="with" negation="false" src="d0_s6" to="o16856" />
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: sepals connate;</text>
      <biological_entity id="o16857" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16858" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens (2-) 6 (-12), surrounding tuft of silky hairs (apparently a reduced pistillode).</text>
      <biological_entity id="o16859" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16860" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s8" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="12" />
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o16861" name="tuft" name_original="tuft" src="d0_s8" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s8" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity id="o16862" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s8" value="silky" value_original="silky" />
      </biological_entity>
      <relation from="o16861" id="r2319" name="part_of" negation="false" src="d0_s8" to="o16862" />
    </statement>
    <statement id="d0_s9">
      <text>Pistillate flower 1 per cupule;</text>
      <biological_entity id="o16863" name="flower" name_original="flower" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character constraint="per cupule" constraintid="o16864" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16864" name="cupule" name_original="cupule" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>sepals connate;</text>
      <biological_entity id="o16865" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>carpels and styles 3 (-6).</text>
      <biological_entity id="o16866" name="carpel" name_original="carpels" src="d0_s11" type="structure" />
      <biological_entity id="o16867" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits: maturation annual or biennial;</text>
      <biological_entity id="o16868" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="maturation" value_original="maturation" />
        <character is_modifier="false" name="duration" src="d0_s12" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s12" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>cup variously shaped (saucer to cup or bowl to goblet-shaped), without indication of valves, covering base of nut (rarely whole nut), scaly, scales imbricate or reduced to tubercles, not or weakly reflexed, never hooked;</text>
      <biological_entity id="o16869" name="fruit" name_original="fruits" src="d0_s13" type="structure" />
      <biological_entity id="o16870" name="cup" name_original="cup" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="variously--shaped" value_original="variously--shaped" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s13" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o16871" name="indication" name_original="indication" src="d0_s13" type="structure" />
      <biological_entity id="o16872" name="valve" name_original="valves" src="d0_s13" type="structure" />
      <biological_entity id="o16873" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o16874" name="nut" name_original="nut" src="d0_s13" type="structure" />
      <biological_entity id="o16875" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s13" value="imbricate" value_original="imbricate" />
        <character constraint="to tubercles" constraintid="o16876" is_modifier="false" name="size" src="d0_s13" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="not; weakly" name="orientation" notes="" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="never" name="shape" src="d0_s13" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity id="o16876" name="tubercle" name_original="tubercles" src="d0_s13" type="structure" />
      <relation from="o16870" id="r2320" name="without" negation="false" src="d0_s13" to="o16871" />
      <relation from="o16871" id="r2321" name="part_of" negation="false" src="d0_s13" to="o16872" />
      <relation from="o16870" id="r2322" name="covering" negation="false" src="d0_s13" to="o16873" />
      <relation from="o16870" id="r2323" name="part_of" negation="false" src="d0_s13" to="o16874" />
    </statement>
    <statement id="d0_s14">
      <text>nut 1 per cup, round in cross-section, not winged.</text>
      <biological_entity id="o16877" name="fruit" name_original="fruits" src="d0_s14" type="structure" />
      <biological_entity id="o16879" name="cup" name_original="cup" src="d0_s14" type="structure" />
      <biological_entity id="o16880" name="cross-section" name_original="cross-section" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>x = 12.</text>
      <biological_entity id="o16878" name="nut" name_original="nut" src="d0_s14" type="structure">
        <character constraint="per cup" constraintid="o16879" name="quantity" src="d0_s14" value="1" value_original="1" />
        <character constraint="in cross-section" constraintid="o16880" is_modifier="false" name="shape" notes="" src="d0_s14" value="round" value_original="round" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="x" id="o16881" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America (Colombia only), Eurasia, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (Colombia only)" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Oak</other_name>
  <other_name type="common_name">chêne</other_name>
  <discussion>Species ca. 400 (90 in the flora).</discussion>
  <discussion>Quercus is without doubt one of the most important woody genera of the Northern Hemisphere. Historically, oaks have been an important source of fuel, fodder, and building materials throughout their range. Other products include tannins and dyes, and oak bark and leaves were often used for tanning leather. Acorns were historically an important food for indigenous people in North America, Central America, Europe, and Asia. In some areas, acorn consumption is still important, but in general, because of the intense preparation necessary to remove tannins and strong flavor of acorn products, they have fallen out of use as human food in developed areas. They do remain, however, an important mast for wildlife and domesticated animals in many rural areas.</discussion>
  <discussion>Among the most important diagnostic characters within Quercus, and particularly the white oak group (Quercus sect. Quercus), are features of the foliar trichomes. Often these can be seen with a 10× or 15× hand lens; higher magnifications are sometimes required and are useful particularly when characters for a species or complex are first studied and mastered for later use in the field. Although these microscopic characters may seem intimidating, the alternative characters of leaf shape and dentition, so often used in the field, are unreliable in many cases. The large number of misidentified specimens in herbaria that can be easily identified properly with the use of trichome characters illustrates this point. Additionally, many specimens are encountered, both in field and herbarium, that lack fruit or have only immature fruit. Very few species require mature fruit for proper diagnosis; most can be adequately identified with a representative selection of mature sun leaves attached, if possible, to twigs with mature buds. The combination of leaf vestiture, form of the margin (entire, lobed, toothed, spinose), twig vestiture, and bud form and vestiture constitute the majority of diagnostic features minimally required at species level.</discussion>
  <discussion>Staminate floral and inflorescence characters have not been used to any significant extent in the taxonomy of Quercus. Immature, flowering material is often difficult to identify with certainty, and floral features such as number and form of sepals, number of stamens, and pubescence of flowers or floral rachises seem to vary independently of species affinity within many groups. Because of these problems, descriptions of staminate features are excluded in this treatment as unreliable and of little diagnostic value. When collecting flowering oaks, make a point of gathering fallen fruit and mature leaves carefully from the ground, if available, and revisit such populations again when mature material is available to verify identifications.</discussion>
  <discussion>The character of acorn maturation in the first year (annual maturation) or second year (biennial maturation) after pollination is commonly used to differentiate major groups within Quercus. All of the North American white oaks have annual maturation; all of the Protobalanus group have biennial maturation; and the vast majority of red oaks have biennial maturation, with one eastern North American and a few western species with annual maturation. In the field, this character can be observed throughout the growing season by examining a sample of twigs from the same tree. If developing fruits exhibit a single size class and are found only on the current year's growth, maturation is annual; if the developing fruits exhibit two size classes with small pistillate flowers on new growth and larger developing fruit on the previous year's twigs, maturation is biennial. In Quercus sect. Protobalanus, biennial maturation may be mistaken for annual maturation because all of the species are fully evergreen, and often the twigs bearing fruit do not produce new growth in the second year after pollination. In such cases, careful examination of a broad sample of twigs from within one tree and throughout a population will verify biennial maturation. Herbarium specimens are sometimes inadequate for this determination.</discussion>
  <discussion>Hybridization among species of Quercus has been widely documented and even more widely suspected. An astounding number of hybrid combinations have been reported in the literature, and many of these have been given species names, either before or after their hybrid status was known (E. J. Palmer 1948). Hybrids are known to occur in the wild only between members of the same section, and attempts at artificial crosses between species from different sections or subgenera within Quercus have failed with very few exceptions (W. P. Cottam et al. 1982).</discussion>
  <discussion>Hybridization in most cases results in solitary unusual trees or scattered clusters of intermediate individuals (J. W. Hardin 1975). In some cases, however, populations of fairly broad distribution and extreme variability, often with a majority of intermediate types, may occur. Such instances occur in both the red oak and white oak groups, and to a lesser extent in the Protobalanus group.</discussion>
  <discussion>When dealing with a suspected hybrid, therefore, one should first consider the possibility of intraspecific variation or environmental plasticity, and then seek parentage among sympatric members of the same section. Because of the almost infinite number of possible hybrid combinations, and the myriad names applied to them, only those that appear to be prominent either locally or in widespread areas are dealt with here. The interested reader is referred to various discussions of oak hybridization in the literature (e.g., E. J. Palmer 1948; J. W. Hardin 1975).</discussion>
  <references>
    <reference>Hunt, D. M. 1989. A Systematic Review of Quercus Series Laurifoliae, Marilandicae and Nigrae. Ph.D. dissertation. University of Georgia.</reference>
    <reference>Muller, C. H. 1961. The live oaks of the series Virentes. Amer. Midl. Naturalist 65: 17-39.</reference>
    <reference>Nixon, K. C. 1993. Infrageneric classification of Quercus (Fagaceae) and typification of sectional names. Ann. Sci. Forest. 50(suppl.1): 25-34.</reference>
    <reference>Nixon, K. C. 1993b. The genus Quercus in Mexico. In: T. P. Ramamoorthy et al., eds. 1993. Biological Diversity of Mexico: Origin and Distribution. New York. Pp. 447-458.</reference>
    <reference>Palmer, E. J. 1948. Hybrid oaks of North America. J. Arnold Arbor. 29: 1-48.</reference>
    <reference>Sargent, C. S. 1918. Notes on North American trees. I. Quercus. Bot. Gaz. 65: 423-459.</reference>
    <reference>Tillson, A. H. and C. H. Muller. 1942. Anatomical and taxonomic approaches to subgeneric segregation in American Quercus. Amer. J. Bot. 29: 523-529.</reference>
    <reference>Trelease, W. 1924. The American oaks. Mem. Natl. Acad. Sci. 20: 1-255.</reference>
    <reference>Muller, C. H. 1951. The oaks of Texas. Contr. Texas Res. Found., Bot. Stud. 1: 21-323.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Mature bark smooth or deeply furrowed (but see also Q. montana), not scaly or papery; cup scales flattened, never tuberculate, never embedded in tomentum; leaf blade if lobed then with awned teeth, if entire then often with bristle at apex; endocarp densely tomentose or silky over entire inner surface of nut.</description>
      <determination>5a Sect. Lobatae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Mature bark scaly or papery, rarely deeply furrowed; cup scales thickened, tuberculate (with darkened or hairy callous at base of each scale), or embedded in tomentum; leaf blade if lobed without awned teeth, if unlobed without bristle at apex, merely mucronate; endocarp glabrous or somewhat sparsely and irregularly hairy, not densely tomentose over entire inner surface of nut.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Acorn maturation biennial, nut requiring 2 seasons to mature; cup scalesembedded in tawny or glandular tomentum, only scale tips visible; plants evergreen, leaves leathery, abaxially glaucous; California, Oregon, and Arizona.</description>
      <determination>5b Sect. Protobalanus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Acorn maturation annual, nut requiring 1 growing season to mature; cup scales not embedded in tawny or glandular tomentum, although scales may be tomentose; plants variously evergreen or deciduous; widespread.</description>
      <determination>5c Sect. Quercus</determination>
    </key_statement>
  </key>
</bio:treatment>