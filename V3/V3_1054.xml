<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ranunculaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="subgenus">Ranunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">ranunculus</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey &amp; A. Gray" date="1838" rank="species">occidentalis</taxon_name>
    <taxon_name authority="L. F. Henderson" date="1930" rank="variety">dissectus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>32: 25. 1930</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ranunculaceae;genus ranunculus;subgenus ranunculus;section ranunculus;species occidentalis;variety dissectus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501181</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect to spreading, 1-3 mm thick, hirsute or sometimes glabrous.</text>
      <biological_entity id="o2477" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s0" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaf-blades 3-parted or sometimes 3-foliolate, ultimate segments lanceolate or oblanceolate, margins entire or sparsely dentate.</text>
      <biological_entity constraint="basal" id="o2478" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="3-parted" value_original="3-parted" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o2479" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o2480" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals 5, 4-6 mm;</text>
      <biological_entity id="o2481" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o2482" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="5" value_original="5" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petals 5-6, 6-10 × 3-6 mm.</text>
      <biological_entity id="o2483" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o2484" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="6" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Achenes 2.6-3.6 × 2-2.8 mm, glabrous;</text>
      <biological_entity id="o2485" name="achene" name_original="achenes" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="length" src="d0_s4" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>beak lance-subulate, straight, 1.2-2.2 mm.</text>
      <biological_entity id="o2486" name="beak" name_original="beak" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lance-subulate" value_original="lance-subulate" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s5" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet to dry meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet to dry meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9c.</number>
  <discussion>Ranunculus occidentalis var. dissectus is found in the Great Basin, Klamath region, and southern Cascade Mountains. As noted below, it is very similar to var. howellii.</discussion>
  
</bio:treatment>