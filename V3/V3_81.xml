<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/04 16:16:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">berberidaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">berberis</taxon_name>
    <taxon_name authority="(Eastwood) L. C. Wheeler" date="1937" rank="species">amplectens</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>39: 376. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus berberis;species amplectens</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500222</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mahonia</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">amplectens</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>4, 20: 145. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mahonia;species amplectens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, evergreen, 0.2-1.2 m.</text>
      <biological_entity id="o24778" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="1.2" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems monomorphic, without short axillary shoots.</text>
      <biological_entity id="o24779" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o24780" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <relation from="o24779" id="r3372" name="without" negation="false" src="d0_s1" to="o24780" />
    </statement>
    <statement id="d0_s2">
      <text>Bark of 2d-year stems purple, glabrous.</text>
      <biological_entity id="o24781" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s2" value="purple" value_original="purple" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24782" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <relation from="o24781" id="r3373" name="part_of" negation="false" src="d0_s2" to="o24782" />
    </statement>
    <statement id="d0_s3">
      <text>Bud-scales 3-6 mm, deciduous.</text>
      <biological_entity id="o24783" name="bud-scale" name_original="bud-scales" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="distance" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines absent.</text>
      <biological_entity id="o24784" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves 5-7-foliolate;</text>
      <biological_entity id="o24785" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-7-foliolate" value_original="5-7-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petioles 1.5-5 cm.</text>
      <biological_entity id="o24786" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaflet blades thick and rigid;</text>
      <biological_entity constraint="leaflet" id="o24787" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>surfaces abaxially dull, papillose, adaxially dull, ± glaucous;</text>
      <biological_entity id="o24788" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abaxially" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
        <character is_modifier="false" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>terminal leaflet stalked, blade 4.4-5.5 × 3.1-4.6 cm, 1.1-1.4 times as long as wide;</text>
      <biological_entity constraint="terminal" id="o24789" name="leaflet" name_original="leaflet" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o24790" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.4" from_unit="cm" name="length" src="d0_s9" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="3.1" from_unit="cm" name="width" src="d0_s9" to="4.6" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s9" value="1.1-1.4" value_original="1.1-1.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflet blades oblong or circular, 1-5-veined from base, base truncate or cordate, margins undulate or crispate, toothed, each with 9-15 teeth 1-3 mm tipped with spines to 1.4-2.4 × 0.2-0.4 mm, apex truncate or broadly rounded.</text>
      <biological_entity constraint="leaflet" id="o24791" name="blade" name_original="blades" src="d0_s10" type="structure" constraint_original="lateral leaflet">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="circular" value_original="circular" />
        <character constraint="from base" constraintid="o24792" is_modifier="false" name="architecture" src="d0_s10" value="1-5-veined" value_original="1-5-veined" />
      </biological_entity>
      <biological_entity id="o24792" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o24793" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o24794" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="crispate" value_original="crispate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="toothed" value_original="toothed" />
        <character constraint="with spines" constraintid="o24796" is_modifier="false" name="architecture" src="d0_s10" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o24795" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s10" to="15" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24796" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s10" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24797" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o24794" id="r3374" name="with" negation="false" src="d0_s10" to="o24795" />
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences racemose, dense, 25-35-flowered, 3-6 cm;</text>
      <biological_entity id="o24798" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="density" src="d0_s11" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="25-35-flowered" value_original="25-35-flowered" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s11" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracteoles membranous, apex obtuse or rounded.</text>
      <biological_entity id="o24799" name="bracteole" name_original="bracteoles" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o24800" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers: anther-filaments distally with pair of recurved teeth: author had no data available.</text>
      <biological_entity id="o24801" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o24802" name="anther-filament" name_original="anther-filaments" src="d0_s13" type="structure" />
      <biological_entity id="o24803" name="pair" name_original="pair" src="d0_s13" type="structure" />
      <biological_entity id="o24804" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o24805" name="data" name_original="data" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="no" value_original="no" />
      </biological_entity>
      <relation from="o24802" id="r3375" name="with" negation="false" src="d0_s13" to="o24803" />
      <relation from="o24803" id="r3376" name="part_of" negation="false" src="d0_s13" to="o24804" />
      <relation from="o24803" id="r3377" name="had" negation="false" src="d0_s13" to="o24805" />
    </statement>
    <statement id="d0_s14">
      <text>Berries dark blue, glaucous, ovoid to elliptic, 7-9 mm, juicy, solid.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 28.</text>
      <biological_entity id="o24806" name="berry" name_original="berries" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark blue" value_original="dark blue" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="elliptic" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s14" value="juicy" value_original="juicy" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24807" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Apr-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes in chaparral and open forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" constraint="in chaparral and open forest" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="open forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Berberis amplectens is endemic to the Peninsular Ranges of southern California. It is resistant to infection by Puccinia graminis.</discussion>
  
</bio:treatment>