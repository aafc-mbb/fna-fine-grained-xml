<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="treatment_page">81</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">aizoaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">sesuvium</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="species">verrucosum</taxon_name>
    <place_of_publication>
      <publication_title>New Fl.</publication_title>
      <place_in_publication>4: 16. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aizoaceae;genus sesuvium;species verrucosum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415131</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sesuvium</taxon_name>
    <taxon_name authority="Correll" date="unknown" rank="species">erectum</taxon_name>
    <taxon_hierarchy>genus Sesuvium;species erectum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, papillate with crystalline globules abundant, glabrous.</text>
      <biological_entity id="o12327" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character constraint="with globules" constraintid="o12328" is_modifier="false" name="relief" src="d0_s0" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o12328" name="globule" name_original="globules" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s0" value="crystalline" value_original="crystalline" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, to 1 m, forming mats to 2 m diam., branched from base, finely verrucose;</text>
      <biological_entity id="o12330" name="mat" name_original="mats" src="d0_s1" type="structure" />
      <biological_entity id="o12331" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o12329" id="r1784" name="forming" negation="false" src="d0_s1" to="o12330" />
    </statement>
    <statement id="d0_s2">
      <text>not rooting at nodes.</text>
      <biological_entity id="o12329" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="1" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="2" to_unit="m" />
        <character constraint="from base" constraintid="o12331" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="finely" name="relief" notes="" src="d0_s1" value="verrucose" value_original="verrucose" />
        <character constraint="at nodes" constraintid="o12332" is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o12332" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade linear to widely spatulate, to 4 cm, base tapered or flared and clasping.</text>
      <biological_entity id="o12333" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o12334" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="widely spatulate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12335" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flared" value_original="flared" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: flowers solitary;</text>
      <biological_entity id="o12336" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o12337" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel absent or to 2 mm.</text>
      <biological_entity id="o12338" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o12339" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="0-2 mm" value_original="0-2 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes rose or orange adaxially, ovatelanceolate, 2–10 mm, margins scarious, apex hooded or beaked, papillate abaxially;</text>
      <biological_entity id="o12340" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o12341" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="rose" value_original="rose" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="orange" value_original="orange" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12342" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o12343" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="beaked" value_original="beaked" />
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s6" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 30;</text>
      <biological_entity id="o12344" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o12345" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="30" value_original="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments connate in proximal 1/2, reddish;</text>
      <biological_entity id="o12346" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12347" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character constraint="in proximal 1/2" constraintid="o12348" is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o12348" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pistil 5-carpellate;</text>
      <biological_entity id="o12349" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12350" name="pistil" name_original="pistil" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-carpellate" value_original="5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary 5-loculed;</text>
      <biological_entity id="o12351" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12352" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="5-loculed" value_original="5-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 5.</text>
      <biological_entity id="o12353" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12354" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ovoid-globose, 4–5 mm.</text>
      <biological_entity id="o12355" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid-globose" value_original="ovoid-globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 20–40, dark-brown to black, 0.8–1 mm, shiny, smooth.</text>
      <biological_entity id="o12356" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="40" />
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s13" to="black" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist or seasonally dry flats, margins of usually saline or alkaline habitats, including coastal wetlands and desert playa lakes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="dry flats" modifier="seasonally" />
        <character name="habitat" value="margins" constraint="of usually saline or alkaline habitats , including coastal wetlands and desert playa lakes" />
        <character name="habitat" value="saline" modifier="usually" />
        <character name="habitat" value="alkaline habitats" />
        <character name="habitat" value="coastal wetlands" />
        <character name="habitat" value="desert playa lakes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Ark., Calif., Colo., Kans., La., Nev., N.Mex., Okla., Oreg., Tex., Utah; Mexico; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Western-purslane</other_name>
  <discussion>Sesuvium verrucosum is widespread and variable, with habitat preferences extending from coastal, saline wetlands to reservoir margins and desert alkali playas in North America and South America. Several names, including S. sessile Persoon, have been applied or misapplied to this species, which can resemble S. portulacastrum. It differs from S. portulacastrum in having sessile or occasionally pedicellate flowers and in lacking roots at stem nodes. Plants from coastal environments such as margins of estuaries are usually smaller in stature, with smaller morphological features than interior desert plants; plants at some coastal sites may function as annuals. Further investigation of this variation could provide useful insight into the relationships of different populations now assigned to S. verrucosum.</discussion>
  
</bio:treatment>