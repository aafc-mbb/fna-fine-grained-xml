<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">128</other_info_on_meta>
    <other_info_on_meta type="mention_page">141</other_info_on_meta>
    <other_info_on_meta type="treatment_page">140</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">opuntia</taxon_name>
    <taxon_name authority="Philbrick" date="1964" rank="species">oricola</taxon_name>
    <place_of_publication>
      <publication_title>Cac t. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>36: 163, 3 figs. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus opuntia;species oricola;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415207</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, spreading, 2–3 m;</text>
      <biological_entity id="o10306" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk, when present, to 30 cm.</text>
      <biological_entity id="o10308" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" modifier="when present" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem segments not disarticulating, dark green, flattened, subcircular to broadly obovate, 16–25 × 16–19 cm, nearly smooth, glabrous;</text>
      <biological_entity constraint="stem" id="o10309" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="subcircular" name="shape" src="d0_s2" to="broadly obovate" />
        <character char_type="range_value" from="16" from_unit="cm" name="length" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="16" from_unit="cm" name="width" src="d0_s2" to="19" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles 8–10 per diagonal across midstem segment, prominent, subcircular, 4.5–5.5 mm, greatly enlarging to 10 mm diam.;</text>
      <biological_entity id="o10310" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per midstem segment" constraintid="o10311" from="8" name="quantity" src="d0_s3" to="10" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s3" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subcircular" value_original="subcircular" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s3" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="greatly" name="diam" src="d0_s3" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o10311" name="segment" name_original="segment" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="diagonal" value_original="diagonal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>wool tan to gray.</text>
      <biological_entity id="o10312" name="wool" name_original="wool" src="d0_s4" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s4" to="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spines 5–13 per areole, in most areoles, usually reflexed, translucent yellow, aging redbrown, angular, curved, subulate, the longest 20–25 (–50) mm.</text>
      <biological_entity id="o10313" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o10314" from="5" name="quantity" src="d0_s5" to="13" />
        <character is_modifier="false" modifier="usually" name="orientation" notes="" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="translucent yellow" value_original="translucent yellow" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="angular" value_original="angular" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="length" src="d0_s5" value="longest" value_original="longest" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10314" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity id="o10315" name="areole" name_original="areoles" src="d0_s5" type="structure" />
      <relation from="o10313" id="r1489" name="in" negation="false" src="d0_s5" to="o10315" />
    </statement>
    <statement id="d0_s6">
      <text>Glochids in rather dense crescent along adaxial margins, increasing in length toward base, subapical tuft poorly developed, tan, aging brown, to 6 mm.</text>
      <biological_entity id="o10316" name="glochid" name_original="glochids" src="d0_s6" type="structure" />
      <biological_entity constraint="adaxial" id="o10317" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity constraint="subapical" id="o10318" name="tuft" name_original="tuft" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="toward base; poorly" name="development" src="d0_s6" value="developed" value_original="developed" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="tan" value_original="tan" />
        <character is_modifier="false" name="life_cycle" src="d0_s6" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brown" value_original="brown" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o10316" id="r1490" modifier="in rather dense crescent" name="along" negation="false" src="d0_s6" to="o10317" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: inner tepals yellow throughout, 25–40 mm;</text>
      <biological_entity id="o10319" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="inner" id="o10320" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments yellow to orange-yellow;</text>
      <biological_entity id="o10321" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10322" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="orange-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers yellow;</text>
      <biological_entity id="o10323" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10324" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style red;</text>
      <biological_entity id="o10325" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10326" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigma lobes green or light green.</text>
      <biological_entity id="o10327" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="stigma" id="o10328" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="light green" value_original="light green" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits red to red-purple, pale-yellow inside, with seed pulp red, subspheric to barrel-shaped, 37–60 × 30–45 mm, juicy, glabrous, spineless;</text>
      <biological_entity id="o10329" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s12" to="red-purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="subspheric" name="shape" notes="" src="d0_s12" to="barrel-shaped" />
        <character char_type="range_value" from="37" from_unit="mm" name="length" src="d0_s12" to="60" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s12" to="45" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s12" value="juicy" value_original="juicy" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="spineless" value_original="spineless" />
      </biological_entity>
      <biological_entity constraint="seed" id="o10330" name="pulp" name_original="pulp" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
      <relation from="o10329" id="r1491" name="with" negation="false" src="d0_s12" to="o10330" />
    </statement>
    <statement id="d0_s13">
      <text>areoles 23–63.</text>
      <biological_entity id="o10331" name="areole" name_original="areoles" src="d0_s13" type="structure">
        <character char_type="range_value" from="23" name="quantity" src="d0_s13" to="63" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds gray-brown, subcircular to semicircular, 3.5–4 mm diam.;</text>
      <biological_entity id="o10332" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="gray-brown" value_original="gray-brown" />
        <character char_type="range_value" from="subcircular" name="shape" src="d0_s14" to="semicircular" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>girdle protruding to 0.4 mm. 2n = 33 (an abnormal, polyhaploid individual), 66.</text>
      <biological_entity id="o10333" name="girdle" name_original="girdle" src="d0_s15" type="structure">
        <character constraint="to 0.4 mm" is_modifier="false" name="prominence" src="d0_s15" value="protruding" value_original="protruding" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10334" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="33" value_original="33" />
        <character name="quantity" src="d0_s15" value="66" value_original="66" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="flowering time" char_type="atypical_range" to="May" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal sage scrub, coastal chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal sage scrub" />
        <character name="habitat" value="coastal chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif. (including Channel Islands); Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Calif. (including Channel Islands)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion>Opuntia demissa Griffiths is apparently a hybrid between O. oricola and an unknown taxon (B. D. Parfitt and M. A. Baker 1993), likely to be O. littoralis. The hybrid appears to be rather widespread and blurs distinctions between the putative parents.</discussion>
  
</bio:treatment>