<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Matthew H. Hils,John W. Thieret,James D. Morefield</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Nees in M. P. zu Wied" date="1839" rank="genus">SARCOBATUS</taxon_name>
    <place_of_publication>
      <publication_title>in M. P. zu Wied, Reise Nord-Amer.</publication_title>
      <place_in_publication>1: 510. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus SARCOBATUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek sarco, flesh, and batos, bramble, alluding to the leaves and thorns</other_info_on_name>
    <other_info_on_name type="fna_id">129190</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Torrey" date="unknown" rank="genus">Fremontia</taxon_name>
    <taxon_hierarchy>genus Fremontia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, deciduous, monoecious, glabrous or pubescent.</text>
      <biological_entity id="o18420" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, much-branched, not jointed, dimorphic, not fleshy;</text>
      <biological_entity id="o18421" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="jointed" value_original="jointed" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>long-shoots solitary, axillary, elongate, bearing leaves and axillary thorns;</text>
      <biological_entity id="o18422" name="long-shoot" name_original="long-shoots" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s2" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o18423" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="axillary" id="o18424" name="thorn" name_original="thorns" src="d0_s2" type="structure" />
      <relation from="o18422" id="r2654" name="bearing" negation="false" src="d0_s2" to="o18423" />
    </statement>
    <statement id="d0_s3">
      <text>short-shoots clustered, budlike, usually subtending thorns or lateral branches, bearing leaves;</text>
      <biological_entity id="o18425" name="short-shoot" name_original="short-shoots" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s3" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o18426" name="thorn" name_original="thorns" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="budlike" value_original="budlike" />
        <character is_modifier="true" modifier="usually" name="position" src="d0_s3" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o18427" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="budlike" value_original="budlike" />
        <character is_modifier="true" modifier="usually" name="position" src="d0_s3" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o18428" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o18426" id="r2655" name="bearing" negation="false" src="d0_s3" to="o18428" />
      <relation from="o18427" id="r2656" name="bearing" negation="false" src="d0_s3" to="o18428" />
    </statement>
    <statement id="d0_s4">
      <text>thorns 1 (–2) per node.</text>
      <biological_entity id="o18429" name="thorn" name_original="thorns" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="2" />
        <character constraint="per node" constraintid="o18430" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o18430" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves mostly alternate on long-shoots (sometimes opposite proximally) or clustered (“tufted”) on short-shoots, sessile, succulent;</text>
      <biological_entity id="o18431" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="on " constraintid="o18433" is_modifier="false" modifier="mostly" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="texture" src="d0_s5" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o18432" name="long-shoot" name_original="long-shoots" src="d0_s5" type="structure" />
      <biological_entity id="o18433" name="short-shoot" name_original="short-shoots" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade linear, nearly terete to somewhat flattened, margins entire, apex obtuse (mucronate).</text>
      <biological_entity id="o18434" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete to somewhat" value_original="terete to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o18435" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18436" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences: pistillate flowers solitary or paired in axils of foliage leaves;</text>
      <biological_entity id="o18437" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o18438" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="solitary" value_original="solitary" />
        <character constraint="in axils" constraintid="o18439" is_modifier="false" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
      </biological_entity>
      <biological_entity id="o18439" name="axil" name_original="axils" src="d0_s7" type="structure" />
      <biological_entity constraint="foliage" id="o18440" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o18439" id="r2657" name="part_of" negation="false" src="d0_s7" to="o18440" />
    </statement>
    <statement id="d0_s8">
      <text>staminate inflorescences distal to pistillate, terminal, pedunculate, cylindric spike of 10–40 spirally arranged flowers.</text>
      <biological_entity id="o18441" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o18442" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity id="o18443" name="spike" name_original="spike" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o18444" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s8" to="40" />
        <character is_modifier="true" modifier="spirally" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o18443" id="r2658" name="consist_of" negation="false" src="d0_s8" to="o18444" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers unisexual, ebracteolate;</text>
    </statement>
    <statement id="d0_s10">
      <text>pistillate sessile (sometimes short-pedunculate in fruit);</text>
    </statement>
    <statement id="d0_s11">
      <text>“perianth” accrescent, adnate, cuplike, margins subentire to shallowly lobed, flaring to form wing in fruit;</text>
      <biological_entity id="o18445" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="ebracteolate" value_original="ebracteolate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o18446" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cuplike" value_original="cuplike" />
      </biological_entity>
      <biological_entity id="o18447" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s11" to="shallowly lobed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="flaring" value_original="flaring" />
      </biological_entity>
      <biological_entity id="o18448" name="wing" name_original="wing" src="d0_s11" type="structure" />
      <biological_entity id="o18449" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
      <relation from="o18448" id="r2659" name="in" negation="false" src="d0_s11" to="o18449" />
    </statement>
    <statement id="d0_s12">
      <text>stigmas 2;</text>
      <biological_entity id="o18450" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminate flowers: perianth absent;</text>
      <biological_entity id="o18451" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o18452" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers caducous, (1–) 2–4 (–5), almost sessile, arising from spike axis and around stalk of persistent, overtopping and concealing, eccentrically peltate, rhombic, scarious scale.</text>
      <biological_entity id="o18453" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o18454" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="caducous" value_original="caducous" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s14" to="2" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="4" />
        <character is_modifier="false" modifier="almost" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
        <character constraint="from spike axis and around stalk" constraintid="o18455" is_modifier="false" name="orientation" src="d0_s14" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o18455" name="stalk" name_original="stalk" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o18456" name="scale" name_original="scale" src="d0_s14" type="structure">
        <character is_modifier="true" name="position" src="d0_s14" value="concealing" value_original="concealing" />
        <character is_modifier="true" modifier="eccentrically" name="architecture" src="d0_s14" value="peltate" value_original="peltate" />
        <character is_modifier="true" name="shape" src="d0_s14" value="rhombic" value_original="rhombic" />
        <character is_modifier="true" name="texture" src="d0_s14" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o18454" id="r2660" name="overtopping" negation="false" src="d0_s14" to="o18456" />
    </statement>
    <statement id="d0_s15">
      <text>Fruiting structures: fruits achenes, proximal portion below wing turbinate, enlarging less than distal portion;</text>
      <biological_entity id="o18458" name="structure" name_original="structures" src="d0_s15" type="structure" />
      <biological_entity constraint="fruits" id="o18459" name="achene" name_original="achenes" src="d0_s15" type="structure" />
      <biological_entity constraint="proximal" id="o18460" name="portion" name_original="portion" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="less" name="size" notes="" src="d0_s15" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o18461" name="wing" name_original="wing" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="turbinate" value_original="turbinate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18462" name="portion" name_original="portion" src="d0_s15" type="structure" />
      <relation from="o18460" id="r2661" name="below" negation="false" src="d0_s15" to="o18461" />
    </statement>
    <statement id="d0_s16">
      <text>distal portion above wing conic;</text>
      <biological_entity id="o18463" name="whole-organism" name_original="" src="d0_s16" type="structure" />
      <biological_entity id="o18464" name="structure" name_original="structures" src="d0_s16" type="structure" />
      <biological_entity constraint="distal" id="o18465" name="portion" name_original="portion" src="d0_s16" type="structure" />
      <biological_entity id="o18466" name="wing" name_original="wing" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="conic" value_original="conic" />
      </biological_entity>
      <relation from="o18463" id="r2662" name="fruiting" negation="false" src="d0_s16" to="o18464" />
      <relation from="o18465" id="r2663" name="above" negation="false" src="d0_s16" to="o18466" />
    </statement>
    <statement id="d0_s17">
      <text>wing sometimes red-tinged, veiny, broad, margins shallowly to deeply lobed, undulate, crenulate to erose.</text>
      <biological_entity id="o18468" name="structure" name_original="structures" src="d0_s17" type="structure" />
      <biological_entity id="o18469" name="wing" name_original="wing" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s17" value="red-tinged" value_original="red-tinged" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="veiny" value_original="veiny" />
        <character is_modifier="false" name="width" src="d0_s17" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o18470" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="shallowly to deeply" name="shape" src="d0_s17" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s17" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s17" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds vertical, flattened, orbiculate;</text>
      <biological_entity id="o18471" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s18" value="orbiculate" value_original="orbiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>seed-coat transparent, membranous;</text>
      <biological_entity id="o18472" name="seed-coat" name_original="seed-coat" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="transparent" value_original="transparent" />
        <character is_modifier="false" name="texture" src="d0_s19" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>perisperm absent.</text>
    </statement>
    <statement id="d0_s21">
      <text>x = 9.</text>
      <biological_entity id="o18473" name="perisperm" name_original="perisperm" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o18474" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, including nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="including nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Sarcobatus was considered monotypic until F. V. Coville published a second species, from western Nevada. The following features have been used (with variable success) to separate the taxa: stature, bark color, leaf length and vestiture, fruit dimensions and vestiture, length of staminate spikes, and number of pistillate flowers per flowering branch. Sarcobatus baileyi has been treated as a species by some, as S. vermiculatus var. baileyi (Coville) Jepson by others, as an unnamed form, and as a taxon not worthy of recognition. Further study may resolve its status; we prefer to maintain the specific rank originally assigned to it.</discussion>
  <discussion>Plants of Sarcobatus are divaricating shrubs, with elongate lateral branches and thorns extending from the main stems at more-or-less right angles, resulting in a tangle of branches and thorns in mature individuals. In addition to elongate lateral branches, they also have short budlike shoots, often clustered into a “cushionlike” structure, that subtend these elongate lateral branches or thorns (i.e., in the position of leaf scars). One key difference between the species is the abundance and behavior of these short shoots and how they produce additional lateral leafy and flowering branches.</discussion>
  <discussion>The two species of Sarcobatus can grow together and may intergrade. Many equivocal specimens can be identified as either somewhat depauperate S. vermiculatus or somewhat robust S. baileyi, both apparently resulting from intermediate soil-moisture conditions or possibly as a response to infestation by fungi or arthropods. Other specimens are problematical, combining in various degrees the features allegedly defining the two taxa. Assigning such specimens to either of the taxa requires a certain amount of arbitrariness; some specimens we cannot determine with certainty. Collectors should record notes on habit, height, and foliage color; photographs of the plants may be helpful. Occasional individuals may be F1 hybrids, being intermediate in morphology, having malformed leaves and inflorescences, and appearing to be sterile.</discussion>
  <discussion>We have seen a few equivocal specimens of Sarcobatus that look disfigured and show numerous, large, dense, short shoots with numerous leaves, giving the appearance of witches’-broom disease. These short shoots, although resembling those of S. baileyi, are unlike those of either species because they are much larger and possess many more leaves than occur on typical short shoots. In addition, the long shoots on these anomalous specimens are like those characteristic of S. vermiculatus.</discussion>
  <references>
    <reference>Baillon, H. E. 1887. Développement de la fleur femelle du Sarcobatus. Bull. Mens. Soc. Linn. Paris 1: 649.</reference>
    <reference>Behnke, H.-D. 1997. Sarcobataceae—a new family of Caryophyllales. Taxon 46: 495–507.</reference>
    <reference>Carlquist, S. 2000b. Wood and stem anatomy of Sarcobatus (Caryophyllales): Systematic and ecological implications. Taxon 49: 27–34.</reference>
    <reference>Sanderson, S. C., H. C. Stutz, M. Stutz, and R. C. Roos. 1999. Chromosome races in Sarcobatus (Sarcobataceae, Caryophyllales). Great Basin Naturalist 59: 301–314.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades usually glabrous, or slightly pubescent, bright green to yellowish green in life, (0.3-)1.5-4(-5) cm; leaves mostly solitary on elongate shoots of current season; pistillate flowers and staminate spikes on long lateral branches with 3-9 readily visible internodes; staminate spikes at maturity usually 11-40 mm; widespread in w North America</description>
      <determination>1 Sarcobatus vermiculatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades usually pubescent, dull green to grayish green in life, (0.5-)1-1.6 cm; leaves mostly clustered on cushionlike base on older wood; pistillate flowers and staminate spikes on short branches with 1-3 barely discernible internodes; staminate spikes at maturity usu- ally to 10 mm; Nevada</description>
      <determination>2 Sarcobatus baileyi</determination>
    </key_statement>
  </key>
</bio:treatment>