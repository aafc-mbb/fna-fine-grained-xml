<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">159</other_info_on_meta>
    <other_info_on_meta type="mention_page">164</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Engelmann in F. A.Wislizenus" date="1848" rank="genus">echinocereus</taxon_name>
    <taxon_name authority="Engelmann in F. A. Wislizenus" date="1848" rank="species">coccineus</taxon_name>
    <place_of_publication>
      <publication_title>in F. A. Wislizenus, Mem. Tour N. Mexico,</publication_title>
      <place_in_publication>93. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinocereus;species coccineus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415254</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocereus</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">coccineus</taxon_name>
    <taxon_name authority="(Engelmann ex S. Watson) W. Blum" date="unknown" rank="subspecies">aggregatus</taxon_name>
    <place_of_publication>
      <publication_title>Mich. Lange &amp; Rutow</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus Echinocereus;species coccineus;subspecies aggregatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinoce</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">triglochidiatus</taxon_name>
    <taxon_name authority="(Engelmann) L. D. Benson" date="unknown" rank="variety">melanacanthus</taxon_name>
    <taxon_hierarchy>genus Echinoce;species triglochidiatus;variety melanacanthus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants commonly 20–100 (–500) -branched, loosely aggregated into clumps or tightly packed into rounded mounds, to 100 cm diam.</text>
      <biological_entity id="o13362" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="commonly" name="architecture" src="d0_s0" value="20-100(-500)-branched" value_original="20-100(-500)-branched" />
        <character constraint="into " constraintid="o13364" is_modifier="false" modifier="loosely" name="arrangement" src="d0_s0" value="aggregated" value_original="aggregated" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" notes="" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13363" name="clump" name_original="clumps" src="d0_s0" type="structure" />
      <biological_entity id="o13364" name="mound" name_original="mounds" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character is_modifier="true" modifier="tightly" name="arrangement" src="d0_s0" value="packed" value_original="packed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, cylindric (or spheric), 5–40 × 4–15 cm;</text>
      <biological_entity id="o13365" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ribs (5–) 6–14, crests slightly (or conspicuously) undulate;</text>
      <biological_entity id="o13366" name="rib" name_original="ribs" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s2" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="14" />
      </biological_entity>
      <biological_entity id="o13367" name="crest" name_original="crests" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles 10–20 (–42) mm apart.</text>
      <biological_entity id="o13368" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="42" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines (1–) 5–16 (–22) per areole, mostly straight except on unusually long-spined individuals, ashy white to gray, brown, yellowish, reddish, or black, often dark tipped;</text>
      <biological_entity id="o13369" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="16" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="22" />
        <character char_type="range_value" constraint="per areole" constraintid="o13370" from="5" name="quantity" src="d0_s4" to="16" />
        <character is_modifier="false" modifier="mostly" name="course" notes="" src="d0_s4" value="straight" value_original="straight" />
        <character char_type="range_value" from="ashy white" name="coloration" notes="" src="d0_s4" to="gray brown yellowish reddish or black" />
        <character char_type="range_value" from="ashy white" name="coloration" src="d0_s4" to="gray brown yellowish reddish or black" />
        <character char_type="range_value" from="ashy white" name="coloration" src="d0_s4" to="gray brown yellowish reddish or black" />
        <character char_type="range_value" from="ashy white" name="coloration" src="d0_s4" to="gray brown yellowish reddish or black" />
        <character char_type="range_value" from="ashy white" name="coloration" src="d0_s4" to="gray brown yellowish reddish or black" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="dark tipped" value_original="dark tipped" />
      </biological_entity>
      <biological_entity id="o13370" name="areole" name_original="areole" src="d0_s4" type="structure" />
      <biological_entity id="o13371" name="individual" name_original="individuals" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="unusually" name="architecture" src="d0_s4" value="long-spined" value_original="long-spined" />
      </biological_entity>
      <relation from="o13369" id="r1933" name="except on" negation="false" src="d0_s4" to="o13371" />
    </statement>
    <statement id="d0_s5">
      <text>radial spines (1–) 4–13 (–18) per areole, appressed to slightly projecting, (3–) 5–40 (–49) mm;</text>
      <biological_entity id="o13372" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="radial" value_original="radial" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="4" to_inclusive="false" />
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="18" />
        <character char_type="range_value" constraint="per areole" constraintid="o13373" from="4" name="quantity" src="d0_s5" to="13" />
        <character char_type="range_value" from="appressed" name="orientation" notes="" src="d0_s5" to="slightly projecting" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="49" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13373" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>central spines 0–6 per areole, spreading to projecting outward, terete (to angular), (5–) 10–80 mm.</text>
      <biological_entity constraint="central" id="o13374" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o13375" from="0" name="quantity" src="d0_s6" to="6" />
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s6" to="projecting" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="80" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13375" name="areole" name_original="areole" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, (2.5–) 3.8–8 (–9) × (1.5–) 3–7 cm;</text>
      <biological_entity id="o13376" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_length" src="d0_s7" to="3.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="9" to_unit="cm" />
        <character char_type="range_value" from="3.8" from_unit="cm" name="length" src="d0_s7" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_width" src="d0_s7" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s7" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>flower tube (12–) 15–40 × 8–30 mm;</text>
      <biological_entity constraint="flower" id="o13377" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_length" src="d0_s8" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s8" to="40" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>flower tube hairs usually 1–2 mm;</text>
      <biological_entity constraint="tube" id="o13378" name="hair" name_original="hairs" src="d0_s9" type="structure" constraint_original="flower tube">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner tepals crimson or scarlet, less often orange-red (very rarely rose-pink), with or without whitish or yellowish (or pink) proximal portion, usually 14–40 × 5–16 mm, tips thick and rigid;</text>
      <biological_entity constraint="inner" id="o13379" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="crimson" value_original="crimson" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="scarlet" value_original="scarlet" />
        <character is_modifier="false" modifier="less often" name="coloration" src="d0_s10" value="orange-red" value_original="orange-red" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o13380" name="portion" name_original="portion" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s10" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13381" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s10" value="rigid" value_original="rigid" />
      </biological_entity>
      <relation from="o13379" id="r1934" name="with or without" negation="false" src="d0_s10" to="o13380" />
    </statement>
    <statement id="d0_s11">
      <text>anthers usually pink or purple (rarely yellow);</text>
      <biological_entity id="o13382" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectar chamber 4–10 mm (longer if measurement includes tube formed by connate stamen bases).</text>
      <biological_entity constraint="nectar" id="o13383" name="chamber" name_original="chamber" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits greenish or yellowish to pinkish, bright red or brownish tinged, 20–40 (–72) mm or less, pulp white.</text>
      <biological_entity id="o13384" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s13" to="pinkish bright red or brownish tinged" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s13" to="pinkish bright red or brownish tinged" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="72" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="40" to_unit="mm" />
        <character name="some_measurement" src="d0_s13" value="less" value_original="less" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 44.</text>
      <biological_entity id="o13385" name="pulp" name_original="pulp" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13386" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Mar–Jun; fruiting 2-3 months after flowering.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="late Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chihuahuan Desert, desert scrub, desert grasslands, pinyon-juniper and oak woodlands, Great Plains grasslands, montane forest, bajadas, rocky slopes, and cliffs, igneous, metamorphic, and limestone substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chihuahuan desert" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="pinyon-juniper and oak woodlands" />
        <character name="habitat" value="great plains" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="forest" modifier="montane" />
        <character name="habitat" value="bajadas" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="cliffs" modifier="and" />
        <character name="habitat" value="igneous" />
        <character name="habitat" value="metamorphic" />
        <character name="habitat" value="limestone substrates" modifier="and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150-2700(-3000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="150" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3000" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Claret-cup cactus</other_name>
  <other_name type="common_name">scarlet hedgehog cactus</other_name>
  <discussion>Tetraploids belonging to Echinocereus coccineus constituted the greater part of L. D. Benson’s concept (1969, 1969b, 1969c, 1982) of E. triglochidiatus var. melanacanthus (see also discussion under 12. E. triglochidiatus). Where sympatric, the diploids and tetraploids are usually different in appearance, except in southeastern Arizona and extreme southwestern New Mexico (see discussion under 13. E. arizonicus), and in northern Arizona.</discussion>
  <discussion>The common, tetraploid, claret-cup cacti of southeastern Arizona mountain ranges have bisexual flowers, and they have been named Echinocereus santaritensis W. Blum &amp; Rutow. Similar plants from southwestern New Mexico are the basis for E. coccineus subsp. aggregatus [also called E. aggregatus (Engelmann ex S. Watson) Rydberg].</discussion>
  <discussion>Populations of Echinocereus coccineus form an intergrading series from densely spine-covered typical coccineus in Colorado and northern New Mexico to sparsely spined plants in west-central Texas. Populations in the mildest climates have strikingly large stems, but shrink when transplanted (D. Weniger 1970). Populations intermediate between those extremes in the El Paso region sometimes are segregated as E. coccineus subsp. rosei.</discussion>
  <discussion>Populations in northwestern Arizona with unusually small, narrow flowers Echinocereus toroweapensis (P. C. Fisher) Fuersch appear identical to E. canyonensis Clover &amp; Jotter (M. A. Baker, pers. comm.). A type specimen for E. toroweapensis was apparently never preserved, so the name may be invalid.</discussion>
  <discussion>Populations in the granitic region of central Texas (chromosome number unknown), probably belonging in Echinocereus coccineus, have been called E. coccineus subsp. roemeri (Muehlenpfordt) W. Blum, Mich. Lange &amp; Rutow. Spines are more numerous than in the surrounding populations on limestone.</discussion>
  <discussion>Echinocereus coccineus var. gurneyi (L. D. Benson) D. Ferguson was based on a short-spined plant, apparently introgressed from E. dasyacanthus, and so it pertains to E. ×roetteri Rümpler in the broad sense. It is not a true geographic race of E. coccineus.</discussion>
  <discussion>Echinocereus santaritensis and the diploid called E. nigrihorridispinus (see discussion under 13. E. arizonicus) are ecologically and reproductively segregated but difficult to distinguish morphologically, especially when sterile. Spines of E. santaritensis tend to be thinner but only extremes are identifiable by spine thickness alone. Arizona reports of E. triglochidiatus var. neomexicanus were based on robust individuals from both of those taxa, whereas slender-spined specimens were identified mostly as E. triglochidiatus var. melanacanthus. Arizona reports of E. polyacanthus were based on either the hairy salverform flowers of E. santaritensis or the robust plants of E. nigrihorridispinus.</discussion>
  
</bio:treatment>