<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">18</other_info_on_meta>
    <other_info_on_meta type="treatment_page">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">boerhavia</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts, ser.</publication_title>
      <place_in_publication>2, 15: 322. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus boerhavia;species wrightii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415016</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
      <biological_entity id="o1732" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot tapered, soft or ± woody.</text>
      <biological_entity id="o1733" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="texture" src="d0_s1" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending, unbranched in small plants to ca. 1–5 times branched throughout, 20–60 dm, densely glandular-pubescent throughout.</text>
      <biological_entity id="o1734" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character constraint="in plants" constraintid="o1735" is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="size_or_quantity" notes="" src="d0_s2" value="1-5 times branched 20-60 dm densely glandular-pubescent" />
        <character is_modifier="false" modifier="throughout" name="size_or_quantity" src="d0_s2" value="1-5 times branched 20-60 dm densely glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o1735" name="plant" name_original="plants" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
        <character is_modifier="false" modifier="throughout" name="size_or_quantity" src="d0_s2" value="1-5 times branched 20-60 dm densely glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly in basal 1/2;</text>
      <biological_entity id="o1736" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o1737" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <relation from="o1736" id="r220" name="in" negation="false" src="d0_s3" to="o1737" />
    </statement>
    <statement id="d0_s4">
      <text>larger leaves with petiole 5–35 mm, blade ovate-triangular, ovate, or broadly lanceolate, 15–55 × 7–35 mm (distal leaves smaller, proportionately narrower), adaxial surface usually pubescent, often glandular, sometimes glabrate but with hairs along midrib, abaxial surface similar, somewhat paler, both surfaces often finely punctate with groups of brown cells, base usually round to obtuse, occasionally subtruncate, margins ± sinuate, apex acute, occasionally obtuse.</text>
      <biological_entity constraint="larger" id="o1738" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1739" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1740" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="55" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1741" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="often" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
        <character constraint="with hairs" constraintid="o1742" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o1742" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o1743" name="midrib" name_original="midrib" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o1744" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="somewhat" name="coloration" src="d0_s4" value="paler" value_original="paler" />
      </biological_entity>
      <biological_entity id="o1745" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character constraint="with groups" constraintid="o1746" is_modifier="false" modifier="often finely" name="coloration_or_relief" src="d0_s4" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity id="o1746" name="group" name_original="groups" src="d0_s4" type="structure" />
      <biological_entity id="o1747" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o1748" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually round" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s4" value="subtruncate" value_original="subtruncate" />
      </biological_entity>
      <biological_entity id="o1749" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o1750" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o1738" id="r221" name="with" negation="false" src="d0_s4" to="o1739" />
      <relation from="o1742" id="r222" name="along" negation="false" src="d0_s4" to="o1743" />
      <relation from="o1746" id="r223" name="part_of" negation="false" src="d0_s4" to="o1747" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, branched ca. 1–4 times unequally, without sticky internodal bands;</text>
      <biological_entity id="o1751" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="branched" value_original="branched" />
        <character constraint="band" constraintid="o1752" is_modifier="false" modifier="unequally" name="size_or_quantity" src="d0_s5" value="1-4 times without sticky internodal bands" />
        <character constraint="band" constraintid="o1753" is_modifier="false" name="size_or_quantity" src="d0_s5" value="1-4 times without sticky internodal bands" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o1752" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="internodal" id="o1753" name="band" name_original="bands" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches strongly ascending, terminating in spicate or racemose flower clusters, axis 10–35 mm.</text>
      <biological_entity id="o1754" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o1755" name="flower" name_original="flower" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="spicate" value_original="spicate" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o1756" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
      </biological_entity>
      <relation from="o1754" id="r224" name="terminating in" negation="false" src="d0_s6" to="o1755" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: pedicel 0.3–0.7 mm;</text>
      <biological_entity id="o1757" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1758" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts at base of perianth persistent, 1–2, ovate-acuminate, 1.5–4 mm;</text>
      <biological_entity id="o1759" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1760" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ovate-acuminate" value_original="ovate-acuminate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1761" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <biological_entity id="o1762" name="perianth" name_original="perianth" src="d0_s8" type="structure" />
      <relation from="o1760" id="r225" name="at" negation="false" src="d0_s8" to="o1761" />
      <relation from="o1761" id="r226" name="part_of" negation="false" src="d0_s8" to="o1762" />
    </statement>
    <statement id="d0_s9">
      <text>perianth whitish to pale-pink (rarely golden yellow), campanulate distal to constriction, 1.2–1.4 mm;</text>
      <biological_entity id="o1763" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1764" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s9" to="pale-pink" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character constraint="to constriction" constraintid="o1765" is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1765" name="constriction" name_original="constriction" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 2–3, included or barely exserted.</text>
      <biological_entity id="o1766" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1767" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="3" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character is_modifier="false" modifier="barely" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits 4–15 per cluster, well spaced to completely overlapping, straw colored to tan at maturity, broadly obovoid, 2.1–2.5 × 1–2 mm (l/w: 1.3–2.1), apex rounded, glabrous;</text>
      <biological_entity id="o1768" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per apex" constraintid="o1769" from="4" name="quantity" src="d0_s11" to="15" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1769" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="spaced" is_modifier="true" name="arrangement" src="d0_s11" to="completely overlapping" />
        <character char_type="range_value" from="straw colored" is_modifier="true" name="coloration" src="d0_s11" to="tan" />
        <character is_modifier="true" modifier="well" name="life_cycle" src="d0_s11" value="maturity" value_original="maturity" />
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2.1" from_unit="mm" is_modifier="true" name="length" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="width" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ribs 4 (–5), broadly acute, never winged, slightly rugose adjacent to sulci;</text>
      <biological_entity id="o1770" name="rib" name_original="ribs" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="5" />
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="never" name="architecture" src="d0_s12" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s12" value="rugose" value_original="rugose" />
        <character constraint="to sulci" constraintid="o1771" is_modifier="false" name="arrangement" src="d0_s12" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o1771" name="sulcus" name_original="sulci" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>sulci 0.5–1 times as wide as base of ribs, coarsely transversely rugose, not papillate.</text>
      <biological_entity id="o1773" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o1774" name="rib" name_original="ribs" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 54.</text>
      <biological_entity id="o1772" name="sulcus" name_original="sulci" src="d0_s13" type="structure">
        <character constraint="of ribs" constraintid="o1774" is_modifier="false" name="width" src="d0_s13" value="0.5-1 times as wide as base" />
        <character is_modifier="false" modifier="coarsely transversely" name="relief" notes="" src="d0_s13" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1775" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, among desert shrubs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" constraint="among desert shrubs" />
        <character name="habitat" value="desert shrubs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., N.Mex., Tex., Utah; Mexico (Chihuahua, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  
</bio:treatment>