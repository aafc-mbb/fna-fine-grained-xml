<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">abronia</taxon_name>
    <taxon_name authority="Torrey ex S. Watson" date="1871" rank="species">turbinata</taxon_name>
    <place_of_publication>
      <publication_title>Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>285, plate 31, figs. 1–5, 8, 9. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus abronia;species turbinata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415090</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, infrequently perennial.</text>
      <biological_entity id="o10237" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="infrequently" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending, much branched, elongate, reddish at least basally, glandular-pubescent, rarely glabrous or viscid-pubescent.</text>
      <biological_entity id="o10238" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="at-least basally; basally" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="viscid-pubescent" value_original="viscid-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1–4.5 cm;</text>
      <biological_entity id="o10239" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10240" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly ovate to orbiculate, 1–5 × 0.5–3 cm, margins entire or ± repand and undulate, surfaces glabrous or sparsely glandular-pubescent.</text>
      <biological_entity id="o10241" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10242" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s3" to="orbiculate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10243" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="repand" value_original="repand" />
        <character is_modifier="false" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o10244" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: peduncle longer than subtending petiole;</text>
      <biological_entity id="o10245" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o10246" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character constraint="than subtending petiole" constraintid="o10247" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o10247" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracts lanceolate to ovate, 3–10 × 1–5 mm, papery, puberulent to densely glandular-pubescent;</text>
      <biological_entity id="o10248" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o10249" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="papery" value_original="papery" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s5" to="densely glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flowers 15–35.</text>
      <biological_entity id="o10250" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o10251" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s6" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perianth: tube greenish to coral pink, 6–18 mm, limb white to pale-pink, 5–8 mm diam.</text>
      <biological_entity id="o10252" name="perianth" name_original="perianth" src="d0_s7" type="structure" />
      <biological_entity id="o10253" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s7" to="coral pink" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10254" name="limb" name_original="limb" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pale-pink" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits winged, turbinate, 3–8 × 3–6 mm, coriaceous, apex broadly tapered to prominent beak;</text>
      <biological_entity id="o10255" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s8" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o10256" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o10257" name="beak" name_original="beak" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>wings (2–) 5 (when 2, folded together) truncate distally with conspicuous dilations, cavities extending throughout.</text>
      <biological_entity id="o10258" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character constraint="with dilations" constraintid="o10259" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o10259" name="dilation" name_original="dilations" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o10260" name="cavity" name_original="cavities" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  
</bio:treatment>