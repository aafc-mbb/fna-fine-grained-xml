<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="treatment_page">213</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose in N. L. Britton and A. Brown" date="1913" rank="genus">pediocactus</taxon_name>
    <taxon_name authority="(Croizat) L. D. Benson" date="1962" rank="species">peeblesianus</taxon_name>
    <taxon_name authority="(Backeberg ex Hochstätter) Lüthy" date="1999" rank="subspecies">fickeiseniae</taxon_name>
    <place_of_publication>
      <publication_title>Kakteen And. Sukk.</publication_title>
      <place_in_publication>50: 278. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus pediocactus;species peeblesianus;subspecies fickeiseniae;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415305</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Navajoa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">peeblesiana</taxon_name>
    <taxon_name authority="Backeberg ex Hochstätter" date="unknown" rank="variety">fickeisenii</taxon_name>
    <place_of_publication>
      <publication_title>Succulenta (Netherlands)</publication_title>
      <place_in_publication>73: 135, figs. (p. 137, 183). 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Navajoa;species peeblesiana;variety fickeisenii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Navajoa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">peeblesiana</taxon_name>
    <taxon_name authority="(Backeberg ex Hochstätter) Hochstätter" date="unknown" rank="subspecies">fickeisenii</taxon_name>
    <taxon_hierarchy>genus Navajoa;species peeblesiana;subspecies fickeisenii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants branched or unbranched, brances 1–4.</text>
      <biological_entity id="o23647" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="4" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 2.5–6.5 × 2–5.5 cm.</text>
      <biological_entity id="o23648" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s1" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s1" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spines usually radial and central (rarely all radial);</text>
      <biological_entity constraint="central" id="o23650" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually" name="arrangement" src="d0_s2" value="radial" value_original="radial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>radial spines 6–7 per areole, 3–7 × 0.3–0.5 mm;</text>
      <biological_entity id="o23651" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o23652" from="6" name="quantity" src="d0_s3" to="7" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" notes="" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" notes="" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23652" name="areole" name_original="areole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>central spine usually 1 per areole, straight to strongly curved, 5–18 × 1 mm.</text>
      <biological_entity constraint="central" id="o23653" name="spine" name_original="spine" src="d0_s4" type="structure">
        <character constraint="per areole" constraintid="o23654" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character char_type="range_value" from="straight" name="course" notes="" src="d0_s4" to="strongly curved" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="18" to_unit="mm" />
        <character name="width" src="d0_s4" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o23654" name="areole" name_original="areole" src="d0_s4" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly soils of alkaline desert scrub and desert grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly soils" constraint="of alkaline desert scrub and desert grasslands" />
        <character name="habitat" value="alkaline desert scrub" />
        <character name="habitat" value="desert grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b.</number>
  <other_name type="common_name">Fickeisen’s Navajo cactus</other_name>
  <discussion>Although endemic to northern Arizona, Pediocactus peeblesianus subsp. fickeiseniae is much more widespread than subsp. peeblesianus. “Pediocactus peeblesianus var. fickeiseniae L. D. Benson” was not validly published.</discussion>
  
</bio:treatment>