<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">422</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">amaranthus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Amaranthus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">retroflexus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 991. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus amaranthus;subgenus amaranthus;species retroflexus;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">200006986</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amaranthus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">retroflexus</taxon_name>
    <taxon_name authority="I. M. Johnston" date="unknown" rank="variety">salicifolius</taxon_name>
    <taxon_hierarchy>genus Amaranthus;species retroflexus;variety salicifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely to moderately pubescent, especially distal parts of stem and branches.</text>
      <biological_entity id="o13060" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely to moderately" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13061" name="part" name_original="parts" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="especially" name="position_or_shape" src="d0_s0" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o13062" name="stem" name_original="stem" src="d0_s0" type="structure" />
      <biological_entity id="o13063" name="branch" name_original="branches" src="d0_s0" type="structure" />
      <relation from="o13061" id="r1882" name="part_of" negation="false" src="d0_s0" to="o13062" />
      <relation from="o13061" id="r1883" name="part_of" negation="false" src="d0_s0" to="o13063" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, reddish near base, branched in distal part to simple 0.2–1.5 (–2) m;</text>
      <biological_entity id="o13065" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity constraint="distal" id="o13066" name="part" name_original="part" src="d0_s1" type="structure">
        <character char_type="range_value" from="in distal part" name="architecture" src="d0_s1" to="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>underdeveloped or damaged plants rarely ascending to nearly prostrate.</text>
      <biological_entity id="o13064" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="near base" constraintid="o13065" is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character constraint="in distal part" constraintid="o13066" is_modifier="false" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="development" src="d0_s2" value="underdeveloped" value_original="underdeveloped" />
      </biological_entity>
      <biological_entity id="o13067" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="condition" src="d0_s2" value="damaged" value_original="damaged" />
        <character char_type="range_value" from="rarely ascending" name="orientation" src="d0_s2" to="nearly prostrate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 1/2 to equaling blade;</text>
      <biological_entity id="o13068" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13069" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="to blade" constraintid="o13070" name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o13070" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to rhombic-ovate, 2–15 × 1–7 cm, base cuneate to rounded-cuneate, margins entire, plane or slightly undulate, apex acute, obtuse, or slightly emarginate, with terminal mucro.</text>
      <biological_entity id="o13071" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o13072" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="rhombic-ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13073" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded-cuneate" />
      </biological_entity>
      <biological_entity id="o13074" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o13075" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o13076" name="mucro" name_original="mucro" src="d0_s4" type="structure" />
      <relation from="o13075" id="r1884" name="with" negation="false" src="d0_s4" to="o13076" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal and axillary, erect or reflexed at tip, green or silvery green, often with reddish or yellowish tint, branched, leafless at least distally, usually short and thick.</text>
      <biological_entity id="o13077" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character constraint="at tip" constraintid="o13078" is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="silvery green" value_original="silvery green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s5" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish tint" value_original="yellowish tint" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="at-least distally; distally" name="architecture" src="d0_s5" value="leafless" value_original="leafless" />
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o13078" name="tip" name_original="tip" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Bracts lanceolate to subulate, (2.5–) 3.5–5 (–6) mm, exceeding tepals, apex acuminate with excurrent midrib.</text>
      <biological_entity id="o13079" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="subulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13080" name="tepal" name_original="tepals" src="d0_s6" type="structure" />
      <biological_entity id="o13081" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character constraint="with midrib" constraintid="o13082" is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o13082" name="midrib" name_original="midrib" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <relation from="o13079" id="r1885" name="exceeding" negation="false" src="d0_s6" to="o13080" />
    </statement>
    <statement id="d0_s7">
      <text>Pistillate flowers: tepals 5, spatulate-obovate, lanceolate-spatulate, not clawed, subequal or unequal, (2–) 2.5–3.5 (–4) mm, membranaceous, apex emarginate or obtuse, with mucro;</text>
      <biological_entity id="o13083" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13084" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s7" value="spatulate-obovate" value_original="spatulate-obovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate-spatulate" value_original="lanceolate-spatulate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="clawed" value_original="clawed" />
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
      <biological_entity id="o13085" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o13086" name="mucro" name_original="mucro" src="d0_s7" type="structure" />
      <relation from="o13085" id="r1886" name="with" negation="false" src="d0_s7" to="o13086" />
    </statement>
    <statement id="d0_s8">
      <text>style-branches erect or slightly spreading,;</text>
      <biological_entity id="o13087" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13088" name="style-branch" name_original="style-branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigmas 3.</text>
      <biological_entity id="o13089" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13090" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers few at tips of inflorescences;</text>
      <biological_entity id="o13091" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character constraint="at tips" constraintid="o13092" is_modifier="false" name="quantity" src="d0_s10" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o13092" name="tip" name_original="tips" src="d0_s10" type="structure" />
      <biological_entity id="o13093" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure" />
      <relation from="o13092" id="r1887" name="part_of" negation="false" src="d0_s10" to="o13093" />
    </statement>
    <statement id="d0_s11">
      <text>tepals 5;</text>
      <biological_entity id="o13094" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens (3–) 4–5.</text>
      <biological_entity id="o13095" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s12" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Utricles broadly obovoid to broadly elliptic, 1.5–2.5 mm, shorter than or subequal to tepals, smooth or slightly rugose, especially near base and in distal part, dehiscence regularly circumscissile.</text>
      <biological_entity id="o13096" name="utricle" name_original="utricles" src="d0_s13" type="structure">
        <character char_type="range_value" from="broadly obovoid" name="shape" src="d0_s13" to="broadly elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character constraint="than or subequal to tepals" constraintid="o13097" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s13" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="regularly" name="dehiscence" notes="" src="d0_s13" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o13097" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o13098" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity constraint="distal" id="o13099" name="part" name_original="part" src="d0_s13" type="structure" />
      <relation from="o13096" id="r1888" modifier="especially" name="near" negation="false" src="d0_s13" to="o13098" />
      <relation from="o13096" id="r1889" name="in" negation="false" src="d0_s13" to="o13099" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds black to dark reddish-brown, lenticular to subglobose-lenticular, 1–1.3 mm, smooth, shiny.</text>
      <biological_entity id="o13100" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="black" name="coloration" src="d0_s14" to="dark reddish-brown" />
        <character char_type="range_value" from="lenticular" name="shape" src="d0_s14" to="subglobose-lenticular" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Banks of rivers, lakes, and streams, disturbed habitats, agricultural fields, railroads, roadsides, waste areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="banks" constraint="of rivers , lakes ," />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="disturbed habitats" />
        <character name="habitat" value="agricultural fields" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask.; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; introduced and naturalized nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="and naturalized nearly worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Redroot pigweed</other_name>
  <other_name type="common_name">redroot amaranth</other_name>
  <other_name type="common_name">wild-beet amaranth</other_name>
  <other_name type="common_name">rough pigweed</other_name>
  <other_name type="common_name">common amaranth</other_name>
  <discussion>Amaranthus retroflexus, native to central and eastern North America, is a successful invasive species and has effectively colonized a wide range of habitats on all inhabited continents. Its variability is extremely wide; usually the species is easily recognized and its identification causes no specific problems. Infraspecific entities described within A. retroflexus are mostly ecologic variants of little or no taxonomic value. Two varieties are more easily recognized: the common var. retroflexus, with bracts about 1.5–2 times as long as tepals, and a more rare var. delilei (Richter &amp; Loret) Thellung (= A. delilei Richter &amp; Loret), with bracts 1–1.5 times as long as tepals.</discussion>
  <discussion>Occasional forms morphologically intermediate between Amaranthus retroflexus and taxa of the A. hybridus aggregate (e.g., A. powellii and A. hybridus, in the strict sense) are known both in the Americas and the Old World. Usually such plants are treated as hybrids; in many cases they are probably just extremes of the natural variability of A. retroflexus. Putative hybrids of A. retroflexus were described from Europe as A. ×ozanonii Thellung (A. hybridus × A. retroflexus) and A. ×soproniensis Priszter &amp; Karpáti (A. powellii × A. retroflexus) (see A. Thellung 1914–1919; S. Priszter 1958; P. Aellen 1959; F. Grüll and S. Priszter 1973).</discussion>
  
</bio:treatment>