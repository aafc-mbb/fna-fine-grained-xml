<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="mention_page">139</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="treatment_page">142</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">opuntia</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1768" rank="species">ficus-indica</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Opuntia no. 2. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus opuntia;species ficus-indica;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242415212</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cactus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">ficus-indica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 468. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cactus;species ficus-indica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cactus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">opuntia</taxon_name>
    <taxon_hierarchy>genus Cactus;species opuntia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="J. F. Macbride" date="unknown" rank="species">compressa</taxon_name>
    <taxon_hierarchy>genus Opuntia;species compressa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">vulgaris</taxon_name>
    <taxon_hierarchy>genus Opuntia;species vulgaris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, 3–6 m;</text>
      <biological_entity id="o21327" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 30–45 cm diam.</text>
      <biological_entity id="o21328" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="diameter" src="d0_s1" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem segments green, broadly oblong to ovate to narrowly elliptic, (20–) 4–60 × 2–3+ cm, low tuberculate;</text>
      <biological_entity constraint="stem" id="o21329" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="broadly oblong" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="20" from_unit="cm" name="average_length" src="d0_s2" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="60" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="position" src="d0_s2" value="low" value_original="low" />
        <character is_modifier="false" name="relief" src="d0_s2" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles 7–11 per diagonal row across midstem segment, rhombic to subcircular, 2–4 (–5) mm diam.;</text>
      <biological_entity id="o21330" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per diagonal row" constraintid="o21331" from="7" name="quantity" src="d0_s3" to="11" />
        <character char_type="range_value" from="rhombic" name="shape" notes="" src="d0_s3" to="subcircular" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o21331" name="row" name_original="row" src="d0_s3" type="structure" />
      <biological_entity constraint="midstem" id="o21332" name="segment" name_original="segment" src="d0_s3" type="structure" />
      <relation from="o21331" id="r3088" name="across" negation="false" src="d0_s3" to="o21332" />
    </statement>
    <statement id="d0_s4">
      <text>wool brown.</text>
      <biological_entity id="o21333" name="wool" name_original="wool" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spines 1–6 per areole, absent or very highly reduced, or in marginal to nearly all areoles, erect to spreading, whitish, tan, or brown, setaceous only or setaceous and subulate, straight to slightly curved, basally angular-flattened, 1–10 (–40) mm;</text>
      <biological_entity id="o21335" name="areole" name_original="areole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" notes="alterIDs:o21335" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21336" name="marginal" name_original="marginal" src="d0_s5" type="structure" />
      <biological_entity id="o21337" name="areole" name_original="areoles" src="d0_s5" type="structure">
        <character char_type="range_value" from="erect" name="orientation" notes="alterIDs:o21337" src="d0_s5" to="spreading" />
      </biological_entity>
      <relation from="o21334" id="r3089" name="in" negation="false" src="d0_s5" to="o21336" />
      <relation from="o21336" id="r3090" name="to" negation="false" src="d0_s5" to="o21337" />
    </statement>
    <statement id="d0_s6">
      <text>0–2 small bristlelike deflexed spines to 5 mm.</text>
      <biological_entity id="o21334" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o21335" from="1" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="false" modifier="very highly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="only" name="shape" src="d0_s5" value="setaceous" value_original="setaceous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="setaceous" value_original="setaceous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="straight" name="course" src="d0_s5" to="slightly curved" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s5" value="angular-flattened" value_original="angular-flattened" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <biological_entity id="o21338" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="small" value_original="small" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="bristlelike" value_original="bristlelike" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="deflexed" value_original="deflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Glochids along adaxial margin of areole and small, inconspicuous tuft, yellowish, aging brown, less than 2 mm.</text>
      <biological_entity id="o21339" name="glochid" name_original="glochids" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="life_cycle" src="d0_s7" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21340" name="margin" name_original="margin" src="d0_s7" type="structure" />
      <biological_entity id="o21341" name="areole" name_original="areole" src="d0_s7" type="structure" />
      <biological_entity id="o21342" name="tuft" name_original="tuft" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="true" name="prominence" src="d0_s7" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <relation from="o21339" id="r3091" name="along" negation="false" src="d0_s7" to="o21340" />
      <relation from="o21340" id="r3092" name="part_of" negation="false" src="d0_s7" to="o21341" />
      <relation from="o21340" id="r3093" name="part_of" negation="false" src="d0_s7" to="o21342" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: inner tepals yellow to orange throughout, 25–50 mm;</text>
      <biological_entity id="o21343" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="inner" id="o21344" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" modifier="throughout" name="coloration" src="d0_s8" to="orange" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s8" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments and anthers yellow;</text>
      <biological_entity id="o21345" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o21346" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o21347" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style bright red;</text>
      <biological_entity id="o21348" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21349" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="bright red" value_original="bright red" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigma lobes yellow.</text>
      <biological_entity id="o21350" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="stigma" id="o21351" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits yellow to orange to purple, 50–100 × 40–90 mm, fleshy to ± juicy, glabrous, usually spineless;</text>
      <biological_entity id="o21352" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s12" to="orange" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s12" to="100" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="width" src="d0_s12" to="90" to_unit="mm" />
        <character char_type="range_value" from="fleshy" name="texture" src="d0_s12" to="more or less juicy" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s12" value="spineless" value_original="spineless" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>areoles 45–60, evenly distributed on fruit.</text>
      <biological_entity id="o21353" name="areole" name_original="areoles" src="d0_s13" type="structure">
        <character char_type="range_value" from="45" name="quantity" src="d0_s13" to="60" />
        <character constraint="on fruit" constraintid="o21354" is_modifier="false" modifier="evenly" name="arrangement" src="d0_s13" value="distributed" value_original="distributed" />
      </biological_entity>
      <biological_entity id="o21354" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds pale tan, subcircular, 4–5 mm diam., warped;</text>
      <biological_entity id="o21355" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale tan" value_original="pale tan" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subcircular" value_original="subcircular" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="warped" value_original="warped" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>girdle protruding to 1 mm. 2n = 88.</text>
      <biological_entity id="o21356" name="girdle" name_original="girdle" src="d0_s15" type="structure">
        <character constraint="to 1 mm" is_modifier="false" name="prominence" src="d0_s15" value="protruding" value_original="protruding" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21357" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="88" value_original="88" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="flowering time" char_type="atypical_range" to="Apr" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal chaparral, sage scrub, arid uplands, washes, canyons, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal chaparral" />
        <character name="habitat" value="sage scrub" />
        <character name="habitat" value="uplands" modifier="arid" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ariz., Calif.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Indian-fig pricklypear</other_name>
  <other_name type="common_name">mission pricklypear</other_name>
  <other_name type="common_name">tuna cactus</other_name>
  <discussion>R. P. Wunderlin (1998) listed this taxon in Florida, but I have not seen specimens.</discussion>
  <discussion>Opuntia ficus-indica, cultivated nearly worldwide, is presumed to be a native of Mexico, but is definitely known only from cultivation or escapes from cultivation. The species has been used for cattle feed, ornament, and fuel. As human food, the young stem segments, “nopalitos,” are eaten as salad or pickled as a vegetable, and the large delicious fruits, “tunas,” are enjoyed in tropical and subtropical regions worldwide.</discussion>
  <discussion>This species probably originated through selection by native peoples of Mexico for spineless forms of Opuntia streptacantha (also 2n = 88) to ease the culturing and collection of cochineal scale insects for their red dye. Numerous cultivar names are known.</discussion>
  <discussion>Naturalized Opuntia ficus-indica (octoploid, spiny morphotype) is known to hybridize in central California with O. phaeacantha (hexaploid), forming a heptaploid with usually intermediate morphology.</discussion>
  
</bio:treatment>