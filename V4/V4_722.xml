<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">327</other_info_on_meta>
    <other_info_on_meta type="treatment_page">367</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Gaertner) S. L. Welsh" date="2001" rank="subgenus">obione</taxon_name>
    <taxon_name authority="(Standley) S. L. Welsh" date="2001" rank="section">Phyllostegiae</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>102: 424. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus obione;section Phyllostegiae</taxon_hierarchy>
    <other_info_on_name type="fna_id">304247</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="unranked">Phyllostegiae</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton e t al., N. Amer. Fl.</publication_title>
      <place_in_publication>21: 69. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Atriplex;unranked Phyllostegiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, monoecious or subdioecious, sparsely glabrate.</text>
      <biological_entity id="o4861" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="subdioecious" value_original="subdioecious" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves with Kranz anatomy, mostly alternate, petiolate;</text>
      <biological_entity id="o4862" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade oval to ovate, variously rhombic-triangular or lanceolate, entire or subhastate.</text>
      <biological_entity id="o4863" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oval" name="shape" src="d0_s2" to="ovate variously rhombic-triangular or lanceolate entire or subhastate" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s2" to="ovate variously rhombic-triangular or lanceolate entire or subhastate" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s2" to="ovate variously rhombic-triangular or lanceolate entire or subhastate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Staminate flowers in axillary glomerules or in naked terminal spikes.</text>
      <biological_entity constraint="terminal" id="o4865" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="naked" value_original="naked" />
      </biological_entity>
      <relation from="o4864" id="r651" name="in axillary glomerules or in" negation="false" src="d0_s3" to="o4865" />
    </statement>
    <statement id="d0_s4">
      <text>Fruiting bracteoles sessile or stipitate, sharply hastate and often sharply cristate as well, united to beyond middle, enclosed pistillate flower lacking perianth.</text>
      <biological_entity id="o4864" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4866" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure" />
      <biological_entity id="o4867" name="beyond middle" name_original="beyond middle" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s4" value="hastate" value_original="hastate" />
        <character is_modifier="false" modifier="often sharply" name="shape" src="d0_s4" value="cristate" value_original="cristate" />
      </biological_entity>
      <biological_entity constraint="enclosed" id="o4868" name="flower" name_original="flower" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="enclosed" id="o4869" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="lacking" value_original="lacking" />
      </biological_entity>
      <relation from="o4864" id="r652" name="fruiting" negation="false" src="d0_s4" to="o4866" />
    </statement>
    <statement id="d0_s5">
      <text>Seeds: radicle superior.</text>
      <biological_entity id="o4870" name="seed" name_original="seeds" src="d0_s5" type="structure" />
      <biological_entity id="o4871" name="radicle" name_original="radicle" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="superior" value_original="superior" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20b.4.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>