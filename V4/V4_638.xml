<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">323</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="treatment_page">336</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Atriplex</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="section">Teutliopsis</taxon_name>
    <taxon_name authority="Boucher ex de Candolle in J. Lamarck and A. P. de Candolle" date="1805" rank="species">prostrata</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck and A. P. de Candolle, Fl. Franç. ed.</publication_title>
      <place_in_publication>3, 3: 387. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus atriplex;section teutliopsis;species prostrata;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242414719</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">triangularis</taxon_name>
    <taxon_hierarchy>genus Atriplex;species triangularis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, monoecious, erect, decumbent or procumbent, branching, 1–10 dm;</text>
      <biological_entity id="o16565" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems subangular to angular, green or striped.</text>
      <biological_entity id="o16566" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="subangular" name="shape" src="d0_s1" to="angular" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="striped" value_original="striped" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite or subopposite at least proximally;</text>
      <biological_entity id="o16567" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="at-least proximally; proximally" name="arrangement" src="d0_s2" value="subopposite" value_original="subopposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole (0–) 1–3 (–4) cm;</text>
      <biological_entity id="o16568" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade triangular-hastate, lobes spreading, 20–100 mm and almost as wide, base truncate or subcordate, margin entire, serrate, dentate, or irregularly toothed, apex acute to obtuse.</text>
      <biological_entity id="o16569" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="triangular-hastate" value_original="triangular-hastate" />
      </biological_entity>
      <biological_entity id="o16570" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" constraint="as wide" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="100" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16571" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o16572" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o16573" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers in spiciform naked spikes 2–9 cm, sometimes forming terminal panicles;</text>
      <biological_entity id="o16574" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o16575" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="naked" value_original="naked" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o16576" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <relation from="o16574" id="r2397" name="in" negation="false" src="d0_s5" to="o16575" />
      <relation from="o16574" id="r2398" modifier="sometimes" name="forming" negation="false" src="d0_s5" to="o16576" />
    </statement>
    <statement id="d0_s6">
      <text>glomerules tight, contiguous or irregularly spaced.</text>
    </statement>
    <statement id="d0_s7">
      <text>Fruiting bracteoles green, becoming brown to black at maturity, triangular-hastate to triangular-ovate, veined or veins obscure, 3–5 mm, thin to thickened, spongy, base truncate to obtuse, margin united at base, lateral angles mostly entire, apex acute, faces smooth or with 2 tubercles.</text>
      <biological_entity id="o16577" name="glomerule" name_original="glomerules" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="tight" value_original="tight" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="contiguous" value_original="contiguous" />
        <character is_modifier="false" modifier="irregularly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity id="o16578" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure" />
      <biological_entity id="o16579" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character char_type="range_value" constraint="at maturity" from="brown" modifier="becoming" name="coloration" src="d0_s7" to="black" />
        <character char_type="range_value" from="triangular-hastate" name="shape" src="d0_s7" to="triangular-ovate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="veined" value_original="veined" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="thin" name="width" src="d0_s7" to="thickened" />
        <character is_modifier="false" name="texture" src="d0_s7" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o16580" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s7" to="obtuse" />
      </biological_entity>
      <biological_entity id="o16581" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character constraint="at base" constraintid="o16582" is_modifier="false" name="fusion" src="d0_s7" value="united" value_original="united" />
      </biological_entity>
      <biological_entity id="o16582" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity constraint="lateral" id="o16583" name="angle" name_original="angles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16584" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o16585" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="with 2 tubercles" value_original="with 2 tubercles" />
      </biological_entity>
      <biological_entity id="o16586" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o16577" id="r2399" name="fruiting" negation="false" src="d0_s7" to="o16578" />
      <relation from="o16585" id="r2400" name="with" negation="false" src="d0_s7" to="o16586" />
    </statement>
    <statement id="d0_s8">
      <text>Seeds dimorphic: brown, flattened, disc-shaped, 1–2.5 mm wide, or black, 1–1.5 mm wide;</text>
      <biological_entity id="o16587" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s8" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s8" value="disc--shaped" value_original="disc--shaped" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>radicle subbasal, obliquely antrorse to spreading.</text>
      <biological_entity id="o16588" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s9" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>2n = 18.</text>
      <biological_entity id="o16589" name="radicle" name_original="radicle" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subbasal" value_original="subbasal" />
        <character char_type="range_value" from="obliquely antrorse" name="orientation" src="d0_s9" to="spreading" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16590" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering in summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sea beaches, salt marshes or other saline habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sea beaches" />
        <character name="habitat" value="salt marshes" />
        <character name="habitat" value="other saline habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., Man., N.B., N.S., Ont., P.E.I., Que., Sask.; Ariz., Calif., Colo., Conn., Del., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Oreg., Pa., R.I., Utah, Va., Wash.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Thinleaf orach</other_name>
  <other_name type="common_name">fat-hen</other_name>
  <discussion>Atriplex prostrata often grows with willow, tamarix, Scirpus (Schoenoplectus and Bulboschoenus segregates), Juncus, Distichlis, and Typha. Perhaps the phase along coastal eastern North America is indigenous, but this and the related Atriplex heterosperma evidently moved quickly from one palustrine habitat to another following subsequent introductions from the Old World. They were probably initially introduced as ballast waifs, and subsequently dispersed by waterfowl. The two species are now commonplace in lands within and adjacent to marshes in much of North America west of the initial sites of introduction.</discussion>
  <discussion>The name for the species taken up here follows the nomenclatural interpretation of J. McNeill et al. (1983).</discussion>
  
</bio:treatment>