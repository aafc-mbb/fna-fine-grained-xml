<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">292</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">CHENOPODIUM</taxon_name>
    <taxon_name authority="(Standley) Clemants &amp; Mosyakin" date="1996" rank="subsection">Fremontia</taxon_name>
    <taxon_name authority="(S. Watson) A. Heller" date="1897" rank="species">incanum</taxon_name>
    <taxon_name authority="Crawford" date="1977" rank="variety">elatum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>29: 292, fig. 3. 1977</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus chenopodium;subgenus chenopodium;section chenopodium;subsection fremontia;species incanum;variety elatum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415435</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2.5–7.5 dm.</text>
      <biological_entity id="o15186" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s0" to="7.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or branched from base.</text>
      <biological_entity id="o15187" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="from base" constraintid="o15188" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o15188" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 1–2.3 cm, apex obtuse to acute, basal teeth acute to rounded.</text>
      <biological_entity id="o15189" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s2" to="2.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15190" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15191" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences with glomerules usually well spaced.</text>
      <biological_entity id="o15192" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o15193" name="glomerule" name_original="glomerules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually well" name="arrangement" src="d0_s3" value="spaced" value_original="spaced" />
      </biological_entity>
      <relation from="o15192" id="r2201" name="with" negation="false" src="d0_s3" to="o15193" />
    </statement>
    <statement id="d0_s4">
      <text>Seeds (1–) 1.1–1.25 mm diam.</text>
      <biological_entity id="o15194" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s4" to="1.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="diameter" src="d0_s4" to="1.25" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late Jun-late Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="late Sep" from="late Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Loose or open sandy and rocky soils, banks, slopes, and bottom lands, sometimes on limestone, gypsum, or salt</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="loose" />
        <character name="habitat" value="open sandy" />
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="banks" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="bottom lands" />
        <character name="habitat" value="limestone" modifier="sometimes" />
        <character name="habitat" value="gypsum" />
        <character name="habitat" value="salt" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23c.</number>
  
</bio:treatment>