<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">262</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">polycnemum</taxon_name>
    <taxon_name authority="A. Braun ex Bogenhard" date="1841" rank="species">majus</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>24: 151. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus polycnemum;species majus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415370</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems procumbent to erect, branches not twisted, 10–30 cm, glabrous.</text>
      <biological_entity id="o14315" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o14316" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves green to bluish green, (3–) 10–25 (–20) × 0.5–1 mm, base hyaline, sometimes with sparsely bristled margins, apex stiffly spine-tipped.</text>
      <biological_entity id="o14317" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="bluish green" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s1" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="average_length" src="d0_s1" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s1" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14318" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o14319" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="sparsely" name="shape" src="d0_s1" value="bristled" value_original="bristled" />
      </biological_entity>
      <biological_entity id="o14320" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="stiffly" name="architecture_or_shape" src="d0_s1" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
      <relation from="o14318" id="r2060" modifier="sometimes" name="with" negation="false" src="d0_s1" to="o14319" />
    </statement>
    <statement id="d0_s2">
      <text>Bracts ± 3 times perianth length.</text>
      <biological_entity id="o14321" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character constraint="perianth" constraintid="o14322" is_modifier="false" name="length" src="d0_s2" value="3 times perianth length" value_original="3 times perianth length" />
      </biological_entity>
      <biological_entity id="o14322" name="perianth" name_original="perianth" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Bracteoles 1–2 times perianth length.</text>
      <biological_entity id="o14323" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure">
        <character constraint="perianth" constraintid="o14324" is_modifier="false" name="length" src="d0_s3" value="1-2 times perianth length" value_original="1-2 times perianth length" />
      </biological_entity>
      <biological_entity id="o14324" name="perianth" name_original="perianth" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Seeds 1.5–2 mm. 2n = 54.</text>
      <biological_entity id="o14325" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14326" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry places, such as bare limestone flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry places" />
        <character name="habitat" value="bare limestone flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont.; Ill., N.Y.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Reports of this species are increasingly rare, perhaps due to loss of habitat.</discussion>
  
</bio:treatment>