<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">154</other_info_on_meta>
    <other_info_on_meta type="treatment_page">153</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton" date="1908" rank="genus">harrisia</taxon_name>
    <taxon_name authority="Small ex Britton &amp; Rose" date="1920" rank="species">simpsonii</taxon_name>
    <place_of_publication>
      <publication_title>Cact.</publication_title>
      <place_in_publication>2: 152, fig. 223. 1920</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus harrisia;species simpsonii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415232</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cereus</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">gracilis</taxon_name>
    <taxon_name authority="(Small ex Britton &amp; Rose) L. D. Benson" date="unknown" rank="variety">simpsonii</taxon_name>
    <taxon_hierarchy>genus Cereus;species gracilis;variety simpsonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, reclining, or clambering, sometimes epiphytic and vinelike, to 4 m;</text>
      <biological_entity id="o16185" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="reclining" value_original="reclining" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="clambering" value_original="clambering" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="reclining" value_original="reclining" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="clambering" value_original="clambering" />
        <character is_modifier="false" modifier="sometimes" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="vinelike" value_original="vinelike" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>ribs 9–10.</text>
      <biological_entity id="o16186" name="rib" name_original="ribs" src="d0_s1" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s1" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spines 7–9 per areole, 1–2.5 cm, yellow-tipped or completely yellowish when young.</text>
      <biological_entity id="o16187" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o16188" from="7" name="quantity" src="d0_s2" to="9" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="yellow-tipped" value_original="yellow-tipped" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o16188" name="areole" name_original="areole" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers: flower tube 10–15 cm, prominently ridged;</text>
      <biological_entity id="o16189" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity constraint="flower" id="o16190" name="tube" name_original="tube" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="prominently" name="shape" src="d0_s3" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>scales turgid at base, with axillary tufts of hairs;</text>
      <biological_entity id="o16191" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o16192" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character constraint="at base" constraintid="o16193" is_modifier="false" name="shape" src="d0_s4" value="turgid" value_original="turgid" />
      </biological_entity>
      <biological_entity id="o16193" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="axillary" id="o16194" name="tuft" name_original="tufts" src="d0_s4" type="structure" />
      <biological_entity id="o16195" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <relation from="o16192" id="r2343" name="with" negation="false" src="d0_s4" to="o16194" />
      <relation from="o16194" id="r2344" name="part_of" negation="false" src="d0_s4" to="o16195" />
    </statement>
    <statement id="d0_s5">
      <text>hairs, white, soft and silky, flexible, 6–10 mm;</text>
      <biological_entity id="o16196" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s5" value="soft" value_original="soft" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="silky" value_original="silky" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="pliable" value_original="flexible" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16197" name="hair" name_original="hairs" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>buds with white hairs.</text>
      <biological_entity id="o16198" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o16199" name="bud" name_original="buds" src="d0_s6" type="structure" />
      <biological_entity id="o16200" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
      <relation from="o16199" id="r2345" name="with" negation="false" src="d0_s6" to="o16200" />
    </statement>
    <statement id="d0_s7">
      <text>Fruits dull red at maturity, depressed-spheric, 40–60 mm diam.</text>
      <biological_entity id="o16201" name="fruit" name_original="fruits" src="d0_s7" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s7" value="depressed-spheric" value_original="depressed-spheric" />
        <character char_type="range_value" from="40" from_unit="mm" name="diameter" src="d0_s7" to="60" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils of dense thickets and hammocks, mangrove swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" constraint="of dense thickets and hammocks , mangrove swamps" />
        <character name="habitat" value="dense thickets" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="mangrove swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Simpson’s prickly apple</other_name>
  <other_name type="common_name">Simpson’s applecactus</other_name>
  <other_name type="common_name">queen-of-the-night</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>D. F. Austin (1984) suggested that the isolated populations and/or clones of Harrisia simpsonii are as different from each other as are the two species, H. aboriginum and H. simpsonii. These observations imply that varietal rank might be more suitable for the two taxa. Further study is warranted.</discussion>
  <discussion>A number of nocturnal-flowering cereoid species with large, white flowers are known as “queen-of-the-night.”</discussion>
  
</bio:treatment>