<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">268</other_info_on_meta>
    <other_info_on_meta type="mention_page">269</other_info_on_meta>
    <other_info_on_meta type="treatment_page">272</other_info_on_meta>
    <other_info_on_meta type="treatment_page">273</other_info_on_meta>
    <other_info_on_meta type="treatment_page">271</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">dysphania</taxon_name>
    <taxon_name authority="(C. A. Meyer) Mosyakin &amp; Clemants" date="2002" rank="section">Botryoides</taxon_name>
    <place_of_publication>
      <publication_title>Ukrayins’k. Bot. Zhurn., n. s.</publication_title>
      <place_in_publication>59: 383. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus dysphania;section Botryoides</taxon_hierarchy>
    <other_info_on_name type="fna_id">304227</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Chenopodium</taxon_name>
    <taxon_name authority="C. A. Meyer" date="unknown" rank="section">Botryoides</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. von Ledebour, Fl. Altaica</publication_title>
      <place_in_publication>1: 410. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chenopodium;section Botryoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o1835" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades with margins pinnately lobate or deeply pinnatifid to nearly entire (in distal leaves).</text>
      <biological_entity id="o1836" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure" />
      <biological_entity id="o1837" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="deeply pinnatifid" name="shape" src="d0_s1" to="nearly entire" />
      </biological_entity>
      <relation from="o1836" id="r233" name="with" negation="false" src="d0_s1" to="o1837" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences solitary flowers or in few-flowered glomerules in lax axillary and terminal cymes.</text>
      <biological_entity id="o1838" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o1839" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o1840" name="glomerule" name_original="glomerules" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="few-flowered" value_original="few-flowered" />
      </biological_entity>
      <biological_entity constraint="axillary and terminal" id="o1841" name="cyme" name_original="cymes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
      </biological_entity>
      <relation from="o1839" id="r234" name="in" negation="false" src="d0_s2" to="o1840" />
      <relation from="o1840" id="r235" name="in" negation="false" src="d0_s2" to="o1841" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers: perianth segments 5, connate basally or ± distinct;</text>
      <biological_entity id="o1842" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity constraint="perianth" id="o1843" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="5" value_original="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="more or less; more or less" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stamens 1–5 or absent;</text>
      <biological_entity id="o1844" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o1845" name="stamen" name_original="stamens" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>styles 2.</text>
      <biological_entity id="o1846" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1847" name="style" name_original="styles" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seeds horizontal.</text>
      <biological_entity id="o1848" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide, native to s North America, n South America, s Eurasia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
        <character name="distribution" value="native to s North America" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
        <character name="distribution" value="s Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6b.</number>
  <discussion>Species 8–9 (3 in the flora).</discussion>
  
</bio:treatment>