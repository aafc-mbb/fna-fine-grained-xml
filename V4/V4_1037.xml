<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">507</other_info_on_meta>
    <other_info_on_meta type="treatment_page">512</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Rafinesque" date="unknown" rank="family">molluginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">glinus</taxon_name>
    <taxon_name authority="(Ruiz &amp; Pavón) Rohrbach in C. F. P. von Martius et al." date="1872" rank="species">radiatus</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. P. von Martius et al., Fl. Bras.</publication_title>
      <place_in_publication>14(2): 238. 1872</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family molluginaceae;genus glinus;species radiatus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415821</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mollugo</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavón" date="unknown" rank="species">radiata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Peruv.</publication_title>
      <place_in_publication>1: 48. 1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mollugo;species radiata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Glinus</taxon_name>
    <taxon_name authority="Fenzl" date="unknown" rank="species">cambessedesii</taxon_name>
    <taxon_hierarchy>genus Glinus;species cambessedesii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mollugo</taxon_name>
    <taxon_name authority="(Fenzl) J. M. Coulter" date="unknown" rank="species">cambessidesii</taxon_name>
    <taxon_hierarchy>genus Mollugo;species cambessidesii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.8–5 dm.</text>
      <biological_entity id="o13212" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.8" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves whorled;</text>
      <biological_entity id="o13213" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–7 mm;</text>
      <biological_entity id="o13214" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade obovate or elliptic to broadly spatulate, 5–25 × 2–17 mm, base cuneate, apex broadly rounded to acute.</text>
      <biological_entity id="o13215" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="broadly spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13216" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13217" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers in clusters of 3–11;</text>
      <biological_entity id="o13218" name="flower" name_original="flowers" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>sepals lanceolate or oblong, 4.1–6.8 × 1.1–2.1 mm, stellate-pubescent abaxially, glabrous adaxially, apex long-acuminate to attenuate;</text>
      <biological_entity id="o13219" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4.1" from_unit="mm" name="length" src="d0_s5" to="6.8" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s5" to="2.1" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13220" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="long-acuminate" name="shape" src="d0_s5" to="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens 3–5.</text>
      <biological_entity id="o13221" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsules ellipsoid, 3–3.5 mm.</text>
      <biological_entity id="o13222" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 10–25 per locule, redbrown to golden brown, 0.4–0.5 × 0.2–0.3 mm, smooth, highly glossy.</text>
      <biological_entity id="o13224" name="locule" name_original="locule" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 18.</text>
      <biological_entity id="o13223" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o13224" from="10" name="quantity" src="d0_s8" to="25" />
        <character char_type="range_value" from="redbrown" name="coloration" notes="" src="d0_s8" to="golden brown" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s8" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="highly" name="reflectance" src="d0_s8" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13225" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist sandy soils, river bottoms, fields, edges of intermittent pools</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist sandy soils" />
        <character name="habitat" value="river bottoms" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="edges" constraint="of intermittent pools" />
        <character name="habitat" value="intermittent pools" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Ark., Calif., La., Okla., Tex.; Mexico; West Indies; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Shining damascisa</other_name>
  <discussion>Glinus radiatus is considered native to tropical and subtropical areas in the New World, although it is doubtfully native in North America. While it is unclear whether the species is native in Louisiana, where it is most common, it is undoubtedly introduced in Arizona (M. A. Lane and D. J. Keil 1976) and California (M. H. Grayum and D. L. Koutnik 1982). In Louisiana, Glinus radiatus occurs in the same habitat as G. lotoides, and the two species grow together in some populations. Some evidence of intermediates in those populations indicates hybrids may form between the two species, but this needs further investigation. Glinus radiatus and G. lotoides are most easily distinguished by seed characteristics (J. W. Thieret 1966b).</discussion>
  
</bio:treatment>