<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">350</other_info_on_meta>
    <other_info_on_meta type="treatment_page">351</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Gaertner) S. L. Welsh" date="2001" rank="subgenus">Obione</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Obione</taxon_name>
    <taxon_name authority="(Standley) S. L. Welsh" date="2001" rank="subsection">Argenteae</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">argentea</taxon_name>
    <taxon_name authority="(M. E. Jones) S. L. Welsh" date="unknown" rank="variety">mohavensis</taxon_name>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus obione;section obione;subsection argenteae;species argentea;variety mohavensis;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415535</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">expansa</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="variety">mohavensis</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>11: 20. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Atriplex;species expansa;variety mohavensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="species">expansa</taxon_name>
    <taxon_name authority="(Jepson) J. F. Macbride" date="unknown" rank="variety">trinervata</taxon_name>
    <taxon_hierarchy>genus Atriplex;species expansa;variety trinervata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="species">sordida</taxon_name>
    <taxon_hierarchy>genus Atriplex;species sordida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">trinervata</taxon_name>
    <taxon_hierarchy>genus Atriplex;species trinervata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or decumbent, much branched, 3–12 (–20) dm, forming clumps 3–10 (–30) dm broad;</text>
      <biological_entity id="o16236" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="12" to_unit="dm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="width" src="d0_s0" to="30" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="10" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o16237" name="clump" name_original="clumps" src="d0_s0" type="structure" />
      <relation from="o16236" id="r2354" name="forming" negation="false" src="d0_s0" to="o16237" />
    </statement>
    <statement id="d0_s1">
      <text>branches obtusely to sharply angled, finely scurfy, white scurfy to glabrate.</text>
      <biological_entity id="o16238" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="obtusely to sharply" name="shape" src="d0_s1" value="angled" value_original="angled" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s1" value="scurfy" value_original="scurfy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="white" value_original="white" />
        <character char_type="range_value" from="scurfy" name="pubescence" src="d0_s1" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 2–21 mm on proximal leaves, distal ones sessile;</text>
      <biological_entity id="o16239" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16240" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="on proximal leaves" constraintid="o16241" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="21" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16241" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o16242" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 3-veined at base, ovate to lanceovate, or deltoid-ovate, (10–) 25–75 mm and often as broad, margin irregularly dentate or entire.</text>
      <biological_entity id="o16243" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16244" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="at base" constraintid="o16245" is_modifier="false" name="architecture" src="d0_s3" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s3" to="lanceovate or deltoid-ovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="lanceovate or deltoid-ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" constraint="as broad" from="25" from_unit="mm" name="some_measurement" src="d0_s3" to="75" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16245" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o16246" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often; irregularly" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers of sexes intermixed in glomerules or staminate ones in distinct glomerules.</text>
      <biological_entity id="o16247" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o16248" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16249" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o16247" id="r2355" name="intermixed in" negation="false" src="d0_s4" to="o16248" />
      <relation from="o16247" id="r2356" name="in" negation="false" src="d0_s4" to="o16249" />
    </statement>
    <statement id="d0_s5">
      <text>Staminate flowers 5-merous.</text>
      <biological_entity id="o16250" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pistillate flowers in axillary fascicles.</text>
      <biological_entity constraint="axillary" id="o16252" name="fascicle" name_original="fascicles" src="d0_s6" type="structure" />
      <relation from="o16251" id="r2357" name="in" negation="false" src="d0_s6" to="o16252" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting bracteoles sessile to subsessile, orbicular or cuneate-orbicular, mostly 3-veined, (3–) 4–7 × 2–4 mm, united to middle, margin sharply dentate, faces with a few irregular, green projections or crests, or unappendaged.</text>
      <biological_entity id="o16251" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16253" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure" />
      <biological_entity id="o16254" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s7" to="subsessile" />
        <character is_modifier="false" name="shape" src="d0_s7" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate-orbicular" value_original="cuneate-orbicular" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s7" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="united" value_original="united" />
        <character is_modifier="false" name="position" src="d0_s7" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o16255" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o16256" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="unappendaged" value_original="unappendaged" />
      </biological_entity>
      <biological_entity id="o16257" name="projection" name_original="projections" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s7" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o16258" name="crest" name_original="crests" src="d0_s7" type="structure" />
      <relation from="o16251" id="r2358" name="fruiting" negation="false" src="d0_s7" to="o16253" />
      <relation from="o16256" id="r2359" name="with" negation="false" src="d0_s7" to="o16257" />
      <relation from="o16256" id="r2360" name="with" negation="false" src="d0_s7" to="o16258" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry or saline substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>above 1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="any" to_unit="m" from="1000" from_unit="m" constraint="above " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Nev., Okla., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25b.</number>
  <other_name type="common_name">Mohave orach</other_name>
  <discussion>Atriplex argentea var. mohavensis is a tall, bushy plant with tumbleweed proportions. Its morphologic differences, slight though they may be, are geographically based.</discussion>
  
</bio:treatment>