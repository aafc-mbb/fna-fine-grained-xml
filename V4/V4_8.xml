<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">7</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">phytolaccaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">phytolacca</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 441. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phytolaccaceae;genus phytolacca;species americana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220010427</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 3 (–7) m.</text>
      <biological_entity id="o10680" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 1–6 cm;</text>
      <biological_entity id="o10681" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o10682" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate to ovate, to 35 × 18 cm, base rounded to cordate, apex acuminate.</text>
      <biological_entity id="o10683" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10684" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="35" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10685" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="cordate" />
      </biological_entity>
      <biological_entity id="o10686" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes open, proximalmost pedicels sometimes bearing 2–few flowers, erect to drooping, 6–30 cm;</text>
      <biological_entity id="o10687" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o10688" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="drooping" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10689" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="few" />
      </biological_entity>
      <relation from="o10688" id="r1563" name="bearing" negation="false" src="d0_s3" to="o10689" />
    </statement>
    <statement id="d0_s4">
      <text>peduncle to 15 cm;</text>
      <biological_entity id="o10690" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel 3–13 mm.</text>
      <biological_entity id="o10691" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 5, white or greenish white to pinkish or purplish, ovate to suborbiculate, equal to subequal, 2.5–3.3 mm;</text>
      <biological_entity id="o10692" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o10693" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="greenish white" name="coloration" src="d0_s6" to="pinkish or purplish" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="suborbiculate" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens (9–) 10 (–12) in 1 whorl;</text>
      <biological_entity id="o10694" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o10695" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" name="atypical_quantity" src="d0_s7" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="12" />
        <character constraint="in whorl" constraintid="o10696" name="quantity" src="d0_s7" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o10696" name="whorl" name_original="whorl" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>carpels 6–12, connate at least in proximal 1/2;</text>
      <biological_entity id="o10697" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10698" name="carpel" name_original="carpels" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="12" />
        <character constraint="in proximal 1/2" constraintid="o10699" is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10699" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>ovary 6–12-loculed.</text>
      <biological_entity id="o10700" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10701" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="6-12-loculed" value_original="6-12-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Berries purple-black, 6–11 mm diam.</text>
      <biological_entity id="o10702" name="berry" name_original="berries" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple-black" value_original="purple-black" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds black, lenticular, 3 mm, shiny.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 36.</text>
      <biological_entity id="o10703" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lenticular" value_original="lenticular" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="reflectance" src="d0_s11" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10704" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., Que.; Ala., Ariz., Ark., Calif., Conn., D.C., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., N.C., N.H., N.J., N.Mex., N.Y., Nebr., Ohio, Okla., Oreg., Pa., R.I., S.C., Tenn., Tex., Va., Vt., W.Va., Wis.; introduced in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Pokeweed</other_name>
  <other_name type="common_name">poke</other_name>
  <other_name type="common_name">pokeberry</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The infraspecific taxonomy of Phytolacca americana has been disputed since J. K. Small (1905) recognized P. rigida as distinct from P. americana on the basis of its “permanently erect panicles” [sic] and “pedicels…much shorter than the diameter of the berries.” J. W. Hardin (1964b) separated P. rigida from P. americana by the length of the raceme (2–12 cm in P. rigida, 5–30 cm in P. americana) and the thickness and diameter of the xylem center of the peduncle (70% greater thickness in P. rigida, 17% greater diameter in P. americana), but he found no discontinuities in any feature. J. W. Nowicke (1968) and J. D. Sauer (1952), among others, treated P. rigida as a synonym of P. americana. Most recently, D. B. Caulkins and R. Wyatt (1990) recognized P. rigida as a variety of P. americana.</discussion>
  <discussion>The varieties are not always clearly distinct. Some specimens combine the erect inflorescences of var. rigida with the long pedicels of var. americana. Such intermediate plants can be seen as far north as coastal Delaware, sometimes growing with var. americana.</discussion>
  <discussion>Collectors of Phytolacca americana should record carefully whether the inflorescences are erect, drooping, or intermediate between the extremes.</discussion>
  <discussion>The fruits and seeds of Phytolacca americana are eaten and disseminated by birds and, probably, mammals. They are said to be an important source of food for mourning doves (A. C. Martin et al. 1951).</discussion>
  <discussion>Phytolacca americana is well known to herbalists, cell biologists, and toxicologists. According to some accounts, its young leaves, after being boiled in two waters (the first being discarded) to deactivate toxins, are edible, even being available canned (they pose no culinary threat to spinach). Young shoots are eaten as a substitute for asparagus. Ripe berries were used to color wine and are eaten (cooked) in pies. Poke is used as an emetic, a purgative, a suppurative, a spring tonic, and a treatment for various skin maladies, especially hemorrhoids.</discussion>
  <discussion>Pokeweed mitogen is a mixture of glycoprotein lectins that are powerful immune stimulants, promoting T- and B-lymphocyte proliferation and increased immunoglobulin levels. “Accidental exposure to juices from Phytolacca americana via ingestion, breaks in the skin, and the conjunctiva has brought about hematological changes in numerous people, including researchers studying this species” (G. K. Rogers 1985). Poke antiviral proteins are of great interest for their broad, potent antiviral (including Human Immunodeficiency Virus) and antifungal properties (P. Wang et al. 1998). Saponins found in P. americana and P. dodecandra are lethal to the molluscan intermediate host of schistosomiasis (J. M. Pezzuto et al. 1984). The toxic compounds in P. americana are phytolaccatoxin and related triterpene saponins, the alkaloid phytolaccin, various histamines, and oxalic acid. When ingested, the roots, leaves, and fruits may poison animals, including Homo sapiens. Symptoms of poke poisoning include sweating, burning of the mouth and throat, severe gastritis, vomiting, bloody diarrhea, blurred vision, elevated white-blood-cell counts, unconsciousness, and, rarely, death.</discussion>
  <discussion>“Poke” is thought to come from “pocan” or “puccoon,” probably from the Algonquin term for a plant that contains dye.</discussion>
  <references>
    <reference>Armesto, J. J., G. P. Cheplick, and M. J. McDonnell. 1983. Observations of the reproductive biology of Phytolacca americana (Phytolaccaceae). Bull. Torrey Bot. Club 110: 380–383.</reference>
    <reference>Caulkins, D. B. and R. Wyatt. 1990. Variation and taxonomy of Phytolacca americana and P. rigida in the southeastern United States. Bull. Torrey Bot. Club 117: 357–367.</reference>
    <reference>Davis, J. I. 1985. Introgression in Central American Phytolacca (Phytolaccaceae). Amer. J. Bot. 72: 1944–1953.</reference>
    <reference>Hardin, J. W. 1964b. A comparison of Phytolacca americana and P. rigida. Castanea 29: 155–164.</reference>
    <reference>Sauer, J. D. 1950. Pokeweed, an old American herb. Missouri Bot. Gard. Bull. 38: 82–88.</reference>
    <reference>Sauer, J. D. 1951. Studies of variation in the weed genus Phytolacca. II. Latitudinally adapted variants within a North American species. Evolution 5: 273–279.</reference>
    <reference>Sauer, J. D. 1952. A geography of pokeweed. Ann. Missouri Bot. Gard. 39: 113–125.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels longer than 6 mm in fruit, longer than berries; racemes (10-)12-30 cm (excluding peduncles), divergent or usually drooping; various habitats, widespread.</description>
      <determination>2a Phytolacca americana var. americana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels shorter than 6(-7) mm in fruit, shorter than berries; racemes 6-9(-13.5) cm, erect; e coastal United States, North Carolina and Vir- ginia to Texas.</description>
      <determination>2b Phytolacca americana var. rigida</determination>
    </key_statement>
  </key>
</bio:treatment>