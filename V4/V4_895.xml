<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="treatment_page">453</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">gomphrena</taxon_name>
    <taxon_name authority="Rothrock" date="1879" rank="species">nitida</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Geogr. Surv., Wheeler,</publication_title>
      <place_in_publication>233. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus gomphrena;species nitida</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415704</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gomphrena</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">globosa</taxon_name>
    <taxon_name authority="Moquin-Tandon" date="unknown" rank="variety">albiflora</taxon_name>
    <taxon_hierarchy>genus Gomphrena;species globosa;variety albiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, not cespitose, 2–7 dm;</text>
      <biological_entity id="o9489" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="7" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots fibrous.</text>
      <biological_entity id="o9490" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually erect, pilose or strigose.</text>
      <biological_entity id="o9491" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile or petiolate;</text>
      <biological_entity id="o9492" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole to 0.7 cm;</text>
      <biological_entity id="o9493" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="0.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade green, obovate or oblong, 1.5–6 × 0.4–2.5 cm, apex obtuse or acute, pilose.</text>
      <biological_entity id="o9494" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9495" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: heads yellowish white or rarely reddish, subglobose, 12–16 mm diam.;</text>
      <biological_entity id="o9496" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o9497" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish white" value_original="yellowish white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s6" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bractlets with laciniate crests.</text>
      <biological_entity id="o9498" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o9499" name="bractlet" name_original="bractlets" src="d0_s7" type="structure" />
      <biological_entity id="o9500" name="crest" name_original="crests" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <relation from="o9499" id="r1353" name="with" negation="false" src="d0_s7" to="o9500" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tube lanate;</text>
      <biological_entity id="o9501" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9502" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="lanate" value_original="lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth lobes white, linear, 4.1 mm, hyaline, apex attenuate.</text>
      <biological_entity id="o9503" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="perianth" id="o9504" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="4.1" value_original="4.1" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o9505" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Utricles ovoid, 1.5 mm, apex truncate.</text>
      <biological_entity id="o9506" name="utricle" name_original="utricles" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o9507" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 1.5 mm.</text>
      <biological_entity id="o9508" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist ground, bottomlands, canyons, rocky open slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bottomlands" modifier="moist ground" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="rocky open slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Pearly globe-amaranth</other_name>
  
</bio:treatment>