<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="treatment_page">189</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Link &amp; Otto" date="1827" rank="genus">echinocactus</taxon_name>
    <taxon_name authority="Lemaire" date="1839" rank="species">horizonthalonius</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Gen. Sp. Nov.,</publication_title>
      <place_in_publication>19. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinocactus;species horizonthalonius;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415269</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocactus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">horizonthalonius</taxon_name>
    <taxon_name authority="L. D. Benson" date="unknown" rank="variety">nicholii</taxon_name>
    <taxon_hierarchy>genus Echinocactus;species horizonthalonius;variety nicholii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants normally unbranched.</text>
      <biological_entity id="o3403" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="normally" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems pale gray-green to bright gray-blue, flat-topped or hemispheric and deep-seated in substrate, spheric with age or stoutly short cylindric (remaining hemispheric at high elevations), 4–25 (–45) × 8–15 (–20) cm;</text>
      <biological_entity id="o3404" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="pale gray-green" name="coloration" src="d0_s1" to="bright gray-blue" />
        <character is_modifier="false" name="shape" src="d0_s1" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" name="shape" src="d0_s1" value="hemispheric" value_original="hemispheric" />
        <character constraint="in substrate" constraintid="o3405" is_modifier="false" name="location" src="d0_s1" value="deep-seated" value_original="deep-seated" />
        <character constraint="with age" constraintid="o3406" is_modifier="false" name="shape" notes="" src="d0_s1" value="spheric" value_original="spheric" />
        <character is_modifier="false" modifier="stoutly" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="45" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3405" name="substrate" name_original="substrate" src="d0_s1" type="structure" />
      <biological_entity id="o3406" name="age" name_original="age" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>ribs (7–) 8 (–9), vertical to helically curving around stem, rib crests broadly rounded, uninterrupted or slightly constricted between areoles.</text>
      <biological_entity id="o3407" name="rib" name_original="ribs" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s2" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="9" />
        <character name="quantity" src="d0_s2" value="8" value_original="8" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="vertical" value_original="vertical" />
        <character constraint="around stem" constraintid="o3408" is_modifier="false" modifier="helically" name="course" src="d0_s2" value="curving" value_original="curving" />
      </biological_entity>
      <biological_entity id="o3408" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity constraint="rib" id="o3409" name="crest" name_original="crests" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="uninterrupted" value_original="uninterrupted" />
        <character constraint="between areoles" constraintid="o3410" is_modifier="false" modifier="slightly" name="size" src="d0_s2" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o3410" name="areole" name_original="areoles" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Spines (5–) 8 (–10) per areole, loosely projecting or strongly decurved, pink, gray, tan, or brown, strongly annulate-ridged, subulate, ± flattened, glabrous, generally not hiding stem surface;</text>
      <biological_entity id="o3411" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s3" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="10" />
        <character constraint="per areole" constraintid="o3412" name="quantity" src="d0_s3" value="8" value_original="8" />
        <character is_modifier="false" modifier="loosely" name="orientation" src="d0_s3" value="projecting" value_original="projecting" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s3" value="decurved" value_original="decurved" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="annulate-ridged" value_original="annulate-ridged" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3412" name="areole" name_original="areole" src="d0_s3" type="structure" />
      <biological_entity constraint="stem" id="o3413" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o3411" id="r438" modifier="generally not" name="hiding" negation="false" src="d0_s3" to="o3413" />
    </statement>
    <statement id="d0_s4">
      <text>radial spines 5 (–8) per areole, similar to central spines;</text>
      <biological_entity id="o3414" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="radial" value_original="radial" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="8" />
        <character constraint="per areole" constraintid="o3415" name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o3415" name="areole" name_original="areole" src="d0_s4" type="structure" />
      <biological_entity constraint="central" id="o3416" name="spine" name_original="spines" src="d0_s4" type="structure" />
      <relation from="o3414" id="r439" name="to" negation="false" src="d0_s4" to="o3416" />
    </statement>
    <statement id="d0_s5">
      <text>central spines 1 (–3) per areole, 18–43 × 1–2.5 (–3) mm, longest spine usually descending, straight or decurved throughout its length.</text>
      <biological_entity constraint="central" id="o3417" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="3" />
        <character constraint="per areole" constraintid="o3418" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" notes="" src="d0_s5" to="43" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3418" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity constraint="longest" id="o3419" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s5" value="descending" value_original="descending" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="length" src="d0_s5" value="decurved" value_original="decurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 5–7 × 5–6.5 (–9.5) cm;</text>
      <biological_entity id="o3420" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner tepals bright-rose-pink or magenta, color ± uniform from base to apex, 3 × 1.5 cm, margins entire to serrate;</text>
      <biological_entity constraint="inner" id="o3421" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="bright-rose-pink" value_original="bright-rose-pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="magenta" value_original="magenta" />
        <character constraint="from base" constraintid="o3422" is_modifier="false" modifier="more or less" name="coloration" src="d0_s7" value="uniform" value_original="uniform" />
        <character name="length" notes="" src="d0_s7" unit="cm" value="3" value_original="3" />
        <character name="width" notes="" src="d0_s7" unit="cm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o3422" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o3423" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o3424" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s7" to="serrate" />
      </biological_entity>
      <relation from="o3422" id="r440" name="to" negation="false" src="d0_s7" to="o3423" />
    </statement>
    <statement id="d0_s8">
      <text>stigma lobes pinkish to olive.</text>
      <biological_entity constraint="stigma" id="o3425" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s8" to="olive" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits indehiscent or weakly dehiscent through basal abscission pore, pink or red, spheric to ovoid-cylindric, surfaces partly or entirely hidden by hairs from axils of scales and long areolar hairs of stem apex, usually quickly drying to tan shell before seed dispersal, 10–30 mm;</text>
      <biological_entity id="o3426" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
        <character constraint="through basal pore" constraintid="o3427" is_modifier="false" modifier="weakly" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s9" to="ovoid-cylindric" />
      </biological_entity>
      <biological_entity id="o3427" name="pore" name_original="pore" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="through" name="position" src="d0_s9" value="basal" value_original="basal" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="abscission" value_original="abscission" />
      </biological_entity>
      <biological_entity id="o3428" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character constraint="by hairs" constraintid="o3429" is_modifier="false" modifier="entirely" name="prominence" src="d0_s9" value="hidden" value_original="hidden" />
        <character is_modifier="false" modifier="usually quickly" name="condition" notes="" src="d0_s9" value="drying" value_original="drying" />
      </biological_entity>
      <biological_entity id="o3429" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o3430" name="axil" name_original="axils" src="d0_s9" type="structure" />
      <biological_entity id="o3431" name="scale" name_original="scales" src="d0_s9" type="structure" />
      <biological_entity constraint="stem" id="o3432" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o3433" name="shell" name_original="shell" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="tan" value_original="tan" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3434" name="seed" name_original="seed" src="d0_s9" type="structure" />
      <relation from="o3429" id="r441" name="from" negation="false" src="d0_s9" to="o3430" />
      <relation from="o3430" id="r442" name="part_of" negation="false" src="d0_s9" to="o3431" />
      <relation from="o3430" id="r443" modifier="of scales and long areolar hairs" name="part_of" negation="false" src="d0_s9" to="o3432" />
      <relation from="o3433" id="r444" name="before" negation="false" src="d0_s9" to="o3434" />
    </statement>
    <statement id="d0_s10">
      <text>scales several, tips dark, spinelike, glabrous.</text>
      <biological_entity id="o3435" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o3436" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark" value_original="dark" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spinelike" value_original="spinelike" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds black or gray, angular or slightly wrinkled, spheric to obovoid, 2–3 mm;</text>
      <biological_entity id="o3437" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="gray" value_original="gray" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="angular" value_original="angular" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s11" value="wrinkled" value_original="wrinkled" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s11" to="obovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>testa cell surfaces slightly convex, with weak network pattern of slightly protruding anticlinal cell-walls.</text>
      <biological_entity id="o3439" name="pattern" name_original="pattern" src="d0_s12" type="structure" constraint="cell-wall" constraint_original="cell-wall; cell-wall">
        <character is_modifier="true" name="fragility" src="d0_s12" value="weak" value_original="weak" />
        <character is_modifier="true" name="arrangement" src="d0_s12" value="network" value_original="network" />
      </biological_entity>
      <biological_entity id="o3440" name="cell-wall" name_original="cell-walls" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="slightly" name="prominence" src="d0_s12" value="protruding" value_original="protruding" />
        <character is_modifier="true" name="orientation" src="d0_s12" value="anticlinal" value_original="anticlinal" />
      </biological_entity>
      <relation from="o3438" id="r445" name="with" negation="false" src="d0_s12" to="o3439" />
      <relation from="o3439" id="r446" name="part_of" negation="false" src="d0_s12" to="o3440" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 22.</text>
      <biological_entity constraint="cell" id="o3438" name="surface" name_original="surfaces" src="d0_s12" type="structure" constraint_original="testa cell">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3441" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arid rocky slopes, primarily limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arid rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1700(-2500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2500" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Blue barrel cactus</other_name>
  <other_name type="common_name">visnaga meloncillo</other_name>
  <discussion>The Sonoran Desert populations of Echinocactus horizonthalonius have been segregated as var. nicholii, but are relatively similar to plants in New Mexico and extreme western Texas. Much greater morphologic diversity exists farther east and in Mexico, where shorter-spined, nearly flat-topped plants, which are more distinctive than the Sonoran Desert populations, have escaped taxonomic distinction.</discussion>
  
</bio:treatment>