<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">79</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">aizoaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">galenia</taxon_name>
    <taxon_name authority="(Linnaeus f.) Sonder in W. H. Harvey et al." date="1862" rank="species">secunda</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Harvey et al., Fl. Cap.</publication_title>
      <place_in_publication>2: 474. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aizoaceae;genus galenia;species secunda</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242415128</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aizoön</taxon_name>
    <taxon_name authority="Linnaeus f." date="unknown" rank="species">secundum</taxon_name>
    <place_of_publication>
      <publication_title>Suppl. Pl.,</publication_title>
      <place_in_publication>261. 1782</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aizoön;species secundum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems prostrate, gray-white, 20–60 cm;</text>
      <biological_entity id="o20330" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray-white" value_original="gray-white" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>younger stems densely clothed with white, loosely appressed scales, 1.5 mm.</text>
      <biological_entity id="o20331" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="younger" value_original="younger" />
        <character name="some_measurement" src="d0_s1" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o20332" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="true" modifier="loosely" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o20331" id="r2963" modifier="densely" name="clothed with" negation="false" src="d0_s1" to="o20332" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves crowded;</text>
      <biological_entity id="o20333" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade gray-white, folded inward, midvein not conspicuous abaxially, obovate, 1.5–2.2 × 0.7–0.9 cm, apex obtuse and recurved, thick, papillate.</text>
      <biological_entity id="o20334" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray-white" value_original="gray-white" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity id="o20335" name="midvein" name_original="midvein" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not; abaxially" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s3" to="0.9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20336" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="false" name="relief" src="d0_s3" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers concealed by leaves, usually on short lateral branches;</text>
      <biological_entity id="o20337" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character constraint="by leaves" constraintid="o20338" is_modifier="false" name="prominence" src="d0_s4" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o20338" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="lateral" id="o20339" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <relation from="o20337" id="r2964" modifier="usually" name="on" negation="false" src="d0_s4" to="o20339" />
    </statement>
    <statement id="d0_s5">
      <text>calyx lobes white to yellow adaxially, oblong or lanceolate-oblong, 3 mm, ciliate near apex, hairy abaxially;</text>
      <biological_entity constraint="calyx" id="o20340" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="white" modifier="adaxially" name="coloration" src="d0_s5" to="yellow" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate-oblong" value_original="lanceolate-oblong" />
        <character name="some_measurement" src="d0_s5" unit="mm" value="3" value_original="3" />
        <character constraint="near apex" constraintid="o20341" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" notes="" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o20341" name="apex" name_original="apex" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>filaments shorter than calyx lobes;</text>
      <biological_entity id="o20342" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character constraint="than calyx lobes" constraintid="o20343" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o20343" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>anthers yellow;</text>
      <biological_entity id="o20344" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>styles 4 (–5).</text>
      <biological_entity id="o20345" name="style" name_original="styles" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules concealed among leaf-bases, persistent.</text>
      <biological_entity id="o20346" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character constraint="among leaf-bases" constraintid="o20347" is_modifier="false" name="prominence" src="d0_s9" value="concealed" value_original="concealed" />
        <character is_modifier="false" name="duration" notes="" src="d0_s9" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o20347" name="leaf-base" name_original="leaf-bases" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Seeds 4 (–5).</text>
      <biological_entity id="o20348" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="5" />
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., N.J.; Africa; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>