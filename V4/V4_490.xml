<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">269</other_info_on_meta>
    <other_info_on_meta type="treatment_page">270</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">dysphania</taxon_name>
    <taxon_name authority="(Moquin-Tandon) Mosyakin &amp; Clemants" date="2002" rank="section">Adenois</taxon_name>
    <taxon_name authority="(Linnaeus) Mosyakin &amp; Clemants" date="2002" rank="species">ambrosioides</taxon_name>
    <place_of_publication>
      <publication_title>Ukrayins’k. Bot. Zhurn., n. s.</publication_title>
      <place_in_publication>59: 382. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus dysphania;section adenois;species ambrosioides;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242414750</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chenopodium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">ambrosioides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 219. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chenopodium;species ambrosioides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chenopodium</taxon_name>
    <taxon_name authority="(Linnaeus) W. A. Weber" date="unknown" rank="species">ambrosioides</taxon_name>
    <taxon_name authority="(Willdenow) Ascherson &amp; Graebner" date="unknown" rank="variety">suffruticosum</taxon_name>
    <taxon_hierarchy>genus Chenopodium;species ambrosioides;variety suffruticosum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Teloxys</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ambrosioides</taxon_name>
    <taxon_hierarchy>genus Teloxys;species ambrosioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o6650" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, much-branched, 3–10 (–15) dm, ± glandular-pubescent.</text>
      <biological_entity id="o6651" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="15" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="10" to_unit="dm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves aromatic, distal leaves sessile;</text>
      <biological_entity id="o6652" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="odor" src="d0_s2" value="aromatic" value_original="aromatic" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6653" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole to 18 mm;</text>
      <biological_entity id="o6654" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to oblong-lanceolate or lanceolate, proximal ones mostly lanceolate, 2–8 (–12) × 0.5–4 (–5.5) cm, base cuneate, margins entire, dentate, or laciniate, apex obtuse to attenuate, copiously gland-dotted (rarely glabrous).</text>
      <biological_entity id="o6655" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblong-lanceolate or lanceolate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6656" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6657" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o6658" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="laciniate" value_original="laciniate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity id="o6659" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="copiously" name="coloration" src="d0_s4" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences lateral spikes, 3–7 cm;</text>
      <biological_entity id="o6660" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6661" name="spike" name_original="spikes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>glomerules globose, 1.5–2.3 mm diam.;</text>
      <biological_entity id="o6662" name="glomerule" name_original="glomerules" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="globose" value_original="globose" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s6" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts leaflike, lanceolate, oblanceolate, spatulate, or linear, 0.3–2.5 cm, apex obtuse, acute, or attenuate.</text>
      <biological_entity id="o6663" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s7" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6664" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth segments 4–5, connate for ca. 1/2 their length, distinct portion ovate, rounded abaxially, 0.7–1 mm, apex obtuse, glandular-pubescent, covering seed at maturity;</text>
      <biological_entity id="o6665" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="perianth" id="o6666" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="5" />
        <character is_modifier="false" name="length" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o6667" name="portion" name_original="portion" src="d0_s8" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6668" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o6669" name="seed" name_original="seed" src="d0_s8" type="structure" />
      <relation from="o6668" id="r908" name="covering" negation="false" src="d0_s8" to="o6669" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 4–5;</text>
      <biological_entity id="o6670" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6671" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 3.</text>
      <biological_entity id="o6672" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6673" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Achenes ovoid;</text>
      <biological_entity id="o6674" name="achene" name_original="achenes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pericarp nonadherent, rugose to smooth.</text>
      <biological_entity id="o6675" name="pericarp" name_original="pericarp" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="nonadherent" value_original="nonadherent" />
        <character char_type="range_value" from="rugose" name="relief" src="d0_s12" to="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds horizontal or vertical, reddish-brown, ovoid, 0.6–1 × 0.4–0.5 mm;</text>
      <biological_entity id="o6676" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s13" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat rugose to smooth.</text>
      <biological_entity id="o6677" name="seed-coat" name_original="seed-coat" src="d0_s14" type="structure">
        <character char_type="range_value" from="rugose" name="relief" src="d0_s14" to="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>River bottoms, dry lake beds, flower beds, waste areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="river bottoms" />
        <character name="habitat" value="dry lake beds" />
        <character name="habitat" value="flower beds" />
        <character name="habitat" value="waste areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ariz., Ark., Calif., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Miss., Mo., Nebr., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis.; native to North America and South America, widely naturalized throughout the tropics and warm-temperate regions of the world.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="native to North America and South America" establishment_means="native" />
        <character name="distribution" value="widely naturalized throughout the tropics and warm-temperate regions of the world" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Mexican-tea</other_name>
  <other_name type="common_name">wormseed</other_name>
  <discussion>Southern populations of Dysphania ambrosioides are native while those populations in the northern part of the flora area are introduced.</discussion>
  
</bio:treatment>