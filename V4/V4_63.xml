<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">34</other_info_on_meta>
    <other_info_on_meta type="treatment_page">36</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">acleisanthes</taxon_name>
    <taxon_name authority="Standley" date="1909" rank="species">acutifolia</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>12: 370. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus acleisanthes;species acutifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415034</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants herbaceous, overall pubescence mixture of capitate-glandular hairs 0.2–0.3 mm and white, capitate, shorter hairs 0.1–0.2 mm.</text>
      <biological_entity id="o21587" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s0" value="herbaceous" value_original="herbaceous" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="pubescence" src="d0_s0" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="capitate" value_original="capitate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="capitate-glandular" id="o21588" name="hair" name_original="hairs" src="d0_s0" type="structure" />
      <biological_entity constraint="shorter" id="o21589" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s0" to="0.2" to_unit="mm" />
      </biological_entity>
      <relation from="o21587" id="r3130" name="part_of" negation="false" src="d0_s0" to="o21588" />
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending, much branched, 10–40 cm, hirtellous to glabrate.</text>
      <biological_entity id="o21590" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s1" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves yellowish green, usually petiolate, distalmost sessile or nearly so, those of pair subequal;</text>
      <biological_entity id="o21591" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o21592" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s2" value="nearly" value_original="nearly" />
        <character constraint="of pair" is_modifier="false" name="size" src="d0_s2" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–15 mm, puberulent to hirtellous;</text>
      <biological_entity id="o21593" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s3" to="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oval to oblongelliptic, (5–) 10–50 × 3–20 (–25) mm, base acute to obtuse, margins crispate or undulate, apex apiculate and acute to obtuse or infrequently rounded;</text>
      <biological_entity id="o21594" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oval" name="shape" src="d0_s4" to="oblongelliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21595" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o21596" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crispate" value_original="crispate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o21597" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse or infrequently rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins and adaxial midveins puberulent or occasionally hirtellous.</text>
      <biological_entity id="o21598" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s5" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21599" name="midvein" name_original="midveins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s5" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences solitary flowers, sessile or nearly so;</text>
      <biological_entity id="o21600" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s6" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o21601" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts linear to subulate, 3–10 mm, hirtellous to puberulent.</text>
      <biological_entity id="o21602" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="subulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s7" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: chasmogamous perianth 2–5 cm, puberulent, tube 1–1.5 mm diam., limbs 10–30 mm diam., stamens 5;</text>
      <biological_entity id="o21603" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o21604" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="chasmogamous" value_original="chasmogamous" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o21605" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21606" name="limb" name_original="limbs" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21607" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>cleistogamous perianth 4–10 mm, hirtellous, stamens 2 (–5).</text>
      <biological_entity id="o21608" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o21609" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="cleistogamous" value_original="cleistogamous" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o21610" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="5" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits with 5 narrow sulci and 5 broad, smooth ribs extending past glands, each bearing sticky, resinous gland in constriction below apex, ovate-oblong, constricted 0.5–1 mm below apex, 5–8 mm, base tapering, apex truncate, sparsely puberulent or glabrous.</text>
      <biological_entity id="o21611" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" name="size" src="d0_s10" value="constricted" value_original="constricted" />
        <character char_type="range_value" constraint="below apex" constraintid="o21618" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21612" name="sulcus" name_original="sulci" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o21613" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o21614" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s10" value="past" value_original="past" />
      </biological_entity>
      <biological_entity id="o21615" name="gland" name_original="gland" src="d0_s10" type="structure">
        <character is_modifier="true" name="texture" src="d0_s10" value="sticky" value_original="sticky" />
        <character is_modifier="true" name="coating" src="d0_s10" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o21616" name="constriction" name_original="constriction" src="d0_s10" type="structure" />
      <biological_entity id="o21617" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o21618" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o21619" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o21620" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o21611" id="r3131" name="with" negation="false" src="d0_s10" to="o21612" />
      <relation from="o21611" id="r3132" name="with" negation="false" src="d0_s10" to="o21613" />
      <relation from="o21612" id="r3133" name="extending" negation="false" src="d0_s10" to="o21614" />
      <relation from="o21613" id="r3134" name="extending" negation="false" src="d0_s10" to="o21614" />
      <relation from="o21611" id="r3135" name="bearing" negation="false" src="d0_s10" to="o21615" />
      <relation from="o21611" id="r3136" name="in" negation="false" src="d0_s10" to="o21616" />
      <relation from="o21616" id="r3137" name="below" negation="false" src="d0_s10" to="o21617" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous or gypseous soils in grasslands and shrublands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous" />
        <character name="habitat" value="gypseous" />
        <character name="habitat" value="soils" constraint="in grasslands and shrublands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="shrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila, Durango, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Havard’s trumpets</other_name>
  
</bio:treatment>