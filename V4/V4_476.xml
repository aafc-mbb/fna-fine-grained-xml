<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">261</other_info_on_meta>
    <other_info_on_meta type="treatment_page">262</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">polycnemum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">arvense</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 35. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus polycnemum;species arvense</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">220010823</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems procumbent or erect, branches spirally twisted, to 10 (–50) cm, glabrous to densely pubescent.</text>
      <biological_entity id="o22501" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o22502" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="spirally" name="architecture" src="d0_s0" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s0" to="densely pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves green, 3–8 (–12) × 1 mm, base somewhat clasping, hyaline, not bristly, apex softly spine-tipped.</text>
      <biological_entity id="o22503" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s1" to="8" to_unit="mm" />
        <character name="width" src="d0_s1" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o22504" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="somewhat" name="architecture_or_fixation" src="d0_s1" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o22505" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="softly" name="architecture_or_shape" src="d0_s1" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bracts: distalmost at least 3 times perianth length.</text>
      <biological_entity id="o22506" name="bract" name_original="bracts" src="d0_s2" type="structure" />
      <biological_entity constraint="distalmost" id="o22507" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character constraint="perianth" constraintid="o22508" is_modifier="false" name="length" src="d0_s2" value="3 times perianth length" value_original="3 times perianth length" />
      </biological_entity>
      <biological_entity id="o22508" name="perianth" name_original="perianth" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Bracteoles ca. equaling perianth length.</text>
      <biological_entity id="o22509" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure" />
      <biological_entity id="o22510" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="true" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Seeds 1–1.5 mm. 2n = 18.</text>
      <biological_entity id="o22511" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22512" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C.; c, s Europe to e Russia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="s Europe to e Russia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Soft needleleaf</other_name>
  <discussion>Once considered weedy, this species has been collected rarely in recent years.</discussion>
  
</bio:treatment>