<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">465</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="treatment_page">475</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">claytonia</taxon_name>
    <taxon_name authority="Pallas ex Willdenow in J. J. Roemer et al." date="1819" rank="species">tuberosa</taxon_name>
    <place_of_publication>
      <publication_title>in J. J. Roemer et al., Syst. Veg.</publication_title>
      <place_in_publication>5: 436. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus claytonia;species tuberosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415757</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Claytonia</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">caroliniana</taxon_name>
    <taxon_name authority="(Pallas ex Willdenow) B. Boivin" date="unknown" rank="variety">tuberosa</taxon_name>
    <taxon_hierarchy>genus Claytonia;species caroliniana;variety tuberosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, with globose tubers 10–30 mm, rarely rhizomatous;</text>
      <biological_entity id="o23333" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="rarely" name="architecture" notes="" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o23334" name="tuber" name_original="tubers" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="globose" value_original="globose" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s0" to="30" to_unit="mm" />
      </biological_entity>
      <relation from="o23333" id="r3386" name="with" negation="false" src="d0_s0" to="o23334" />
    </statement>
    <statement id="d0_s1">
      <text>periderm 5–20 mm.</text>
      <biological_entity id="o23335" name="periderm" name_original="periderm" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 15–25 cm.</text>
      <biological_entity id="o23336" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal leaves usually absent or few, blade linear, 4–15 × 0.4–0.8 cm;</text>
      <biological_entity id="o23337" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o23338" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o23339" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves sessile, blade linear to lanceolate, 2–7 × 0.2–0.6 cm, tapered to slender base, apex acute.</text>
      <biological_entity id="o23340" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o23341" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o23342" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s4" to="0.6" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity constraint="slender" id="o23343" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o23344" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–multibracteate;</text>
      <biological_entity id="o23345" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-multibracteate" value_original="1-multibracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximalmost bract leaflike, distal bracts minute, membranous scales.</text>
      <biological_entity constraint="proximalmost" id="o23346" name="bract" name_original="bract" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23347" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o23348" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 12–20 mm diam.;</text>
      <biological_entity id="o23349" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 4–6 mm;</text>
      <biological_entity id="o23350" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white with yellow blotch at base, 6–14 mm;</text>
      <biological_entity id="o23351" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character constraint="with blotch" constraintid="o23352" is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23352" name="blotch" name_original="blotch" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o23353" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o23352" id="r3387" name="at" negation="false" src="d0_s9" to="o23353" />
    </statement>
    <statement id="d0_s10">
      <text>ovules 6.</text>
      <biological_entity id="o23354" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 2–3 mm diam., shiny and smooth;</text>
      <biological_entity id="o23355" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s11" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>elaiosome 1 mm. 2n = 16, 24, 30.</text>
      <biological_entity id="o23356" name="elaiosome" name_original="elaiosome" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23357" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
        <character name="quantity" src="d0_s12" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet to moist stony tundra slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet to moist stony tundra slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.W.T., Yukon; Alaska; Asia (Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Beringian spring beauty</other_name>
  <discussion>Claytonia czukczorum was included by S. L. Welsh (1974) and E. Hultén (1968) as a variety of C. tuberosa. Based upon the author’s study of type material, it is grouped with C. multiscapa.</discussion>
  
</bio:treatment>