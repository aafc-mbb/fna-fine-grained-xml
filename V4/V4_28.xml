<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">18</other_info_on_meta>
    <other_info_on_meta type="treatment_page">21</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">boerhavia</taxon_name>
    <taxon_name authority="Heimerl" date="1889" rank="species">gracillima</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>11: 86, plate 2, fig. 1. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus boerhavia;species gracillima</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415008</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Boerhavia</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="species">gracillima</taxon_name>
    <taxon_name authority="Heimerl ex Standley" date="unknown" rank="subspecies">decalvata</taxon_name>
    <taxon_hierarchy>genus Boerhavia;species gracillima;subspecies decalvata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Boerhavia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">organensis</taxon_name>
    <taxon_hierarchy>genus Boerhavia;species organensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, often woody at base;</text>
      <biological_entity id="o23990" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character constraint="at base" constraintid="o23991" is_modifier="false" modifier="often" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o23991" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>taproot long and ropelike, woody.</text>
      <biological_entity id="o23992" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="ropelike" value_original="ropelike" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent to erect, usually profusely branched throughout, 2–15 dm, usually minutely pubescent, rarely glabrous basally, usually glabrous, rarely sparsely and minutely pubescent distally.</text>
      <biological_entity id="o23993" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" modifier="usually profusely; throughout" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="15" to_unit="dm" />
        <character is_modifier="false" modifier="usually minutely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely; basally" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely; sparsely; minutely; distally" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly in basal 1/2 of plant;</text>
      <biological_entity id="o23994" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o23995" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <biological_entity id="o23996" name="plant" name_original="plant" src="d0_s3" type="structure" />
      <relation from="o23994" id="r3481" name="in" negation="false" src="d0_s3" to="o23995" />
      <relation from="o23995" id="r3482" name="part_of" negation="false" src="d0_s3" to="o23996" />
    </statement>
    <statement id="d0_s4">
      <text>larger leaves with petiole 3–25 mm, blade broadly rhombic to elliptic-oblong, broadly to narrowly ovate, occasionally wider than long, 18–45 × 13–50 mm (distal leaves smaller, proportionately narrower), adaxial surface glabrous, abaxial surface much paler than adaxial surface, glabrous or with hairs on veins, neither surface punctate, base usually obtuse to round, sometimes shallowly cordate, margins entire or sinuate, often undulate, apex acute, obtuse, or round.</text>
      <biological_entity constraint="larger" id="o23997" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o23998" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23999" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly rhombic" name="shape" src="d0_s4" to="elliptic-oblong" />
        <character char_type="range_value" from="broadly rhombic" name="shape" src="d0_s4" to="elliptic-oblong" />
        <character is_modifier="false" name="width" src="d0_s4" value="occasionally wider than long" value_original="occasionally wider than long" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24000" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24001" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="than adaxial surface" constraintid="o24002" is_modifier="false" name="coloration" src="d0_s4" value="much paler" value_original="much paler" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with hairs" value_original="with hairs" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24002" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o24003" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o24004" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity id="o24005" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s4" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity id="o24006" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually obtuse" name="shape" src="d0_s4" to="round" />
        <character is_modifier="false" modifier="sometimes shallowly" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o24007" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o24008" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
      </biological_entity>
      <relation from="o23997" id="r3483" name="with" negation="false" src="d0_s4" to="o23998" />
      <relation from="o24001" id="r3484" name="with" negation="false" src="d0_s4" to="o24003" />
      <relation from="o24003" id="r3485" name="on" negation="false" src="d0_s4" to="o24004" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary or terminal, forked ca. 6–8 times unequally, diffuse, without sticky internodal bands;</text>
      <biological_entity id="o24009" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s5" value="forked" value_original="forked" />
        <character constraint="band" constraintid="o24010" is_modifier="false" modifier="unequally" name="size_or_quantity" src="d0_s5" value="6-8 times diffuse without sticky internodal bands" />
        <character constraint="band" constraintid="o24011" is_modifier="false" name="size_or_quantity" src="d0_s5" value="6-8 times diffuse without sticky internodal bands" />
        <character constraint="band" constraintid="o24012" is_modifier="false" name="size_or_quantity" src="d0_s5" value="6-8 times diffuse without sticky internodal bands" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o24010" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="internodal" id="o24011" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="internodal" id="o24012" name="band" name_original="bands" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches divergent, terminating in 1 (–3) flowers.</text>
      <biological_entity id="o24013" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o24014" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o24013" id="r3486" name="terminating in" negation="false" src="d0_s6" to="o24014" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: pedicel slender, 3–13 mm;</text>
      <biological_entity id="o24015" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24016" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts at base of perianth quickly deciduous, 2–3, linear-lanceolate to broadly lanceolate, 0.3–1.5 mm, quickly deciduous;</text>
      <biological_entity id="o24017" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24018" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" notes="" src="d0_s8" to="broadly lanceolate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="quickly" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o24019" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="quickly" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o24020" name="perianth" name_original="perianth" src="d0_s8" type="structure" />
      <relation from="o24018" id="r3487" name="at" negation="false" src="d0_s8" to="o24019" />
      <relation from="o24019" id="r3488" name="part_of" negation="false" src="d0_s8" to="o24020" />
    </statement>
    <statement id="d0_s9">
      <text>perianth wine red to brick-red, widely funnelform distal to constriction, 2–4.5 mm;</text>
      <biological_entity id="o24021" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24022" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character char_type="range_value" from="wine red" name="coloration" src="d0_s9" to="brick-red" />
        <character constraint="to constriction" constraintid="o24023" is_modifier="false" modifier="widely" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24023" name="constriction" name_original="constriction" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens (4–) 5 (–6), well exserted.</text>
      <biological_entity id="o24024" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o24025" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="6" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" modifier="well" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits usually borne singly, gray-brown to brown, oblong-clavate, 2.8–4.2 × 1–1.5 mm (l/w: [2–] 2.2–3.5 [–3.8]), apex round to rounded-conic, minutely puberulent, sometimes minutely glandular, rarely glabrous;</text>
      <biological_entity id="o24026" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s11" to="rounded-conic" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_function_or_pubescence" src="d0_s11" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24027" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="singly" value_original="singly" />
        <character char_type="range_value" from="gray-brown" is_modifier="true" name="coloration" src="d0_s11" to="brown" />
        <character is_modifier="true" name="shape" src="d0_s11" value="oblong-clavate" value_original="oblong-clavate" />
        <character char_type="range_value" from="2.8" from_unit="mm" is_modifier="true" name="length" src="d0_s11" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o24026" id="r3489" name="borne" negation="false" src="d0_s11" to="o24027" />
    </statement>
    <statement id="d0_s12">
      <text>ribs 5, rounded, smooth or slightly rugose near sulci;</text>
      <biological_entity id="o24028" name="rib" name_original="ribs" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character constraint="near sulci" constraintid="o24029" is_modifier="false" modifier="slightly" name="relief" src="d0_s12" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o24029" name="sulcus" name_original="sulci" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>sulci 1–2.5 times as wide as base of ribs, not rugose, smooth or minutely papillate.</text>
      <biological_entity id="o24030" name="sulcus" name_original="sulci" src="d0_s13" type="structure">
        <character constraint="of ribs" constraintid="o24032" is_modifier="false" name="width" src="d0_s13" value="1-2.5 times as wide as base" />
        <character is_modifier="false" modifier="not" name="relief" notes="" src="d0_s13" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o24031" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o24032" name="rib" name_original="ribs" src="d0_s13" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, usually rocky areas, often along roads, desert scrub, arid grasslands, pinyon-juniper woodlands [tropical deciduous forests]</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="rocky areas" modifier="usually" />
        <character name="habitat" value="roads" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="grasslands" modifier="arid" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="tropical deciduous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>[100-]600-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="2000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Slimstalk spiderling</other_name>
  <discussion>The phase Boerhavia gracillima subsp. decalvata from low elevations in the Big Bend region of Texas and adjacent Mexico differs from the remainder of B. gracillima in that it is usually erect or strongly ascending, has glabrous fruits, and has flowers with purple to brick red perianths; the flowers are also in the upper half of the size range for the species. It is highly local and completely intergradient with surrounding populations of B. gracillima in the strict sense. In fruit and flower features the phase resembles B. anisophylla; in its diffuse inflorescence with slender branches (0.15 mm diam. proximal to the flower versus 0.25 mm in B. anisophylla) and in its deciduous bracts it is more similar to B. gracillima. Some populations of B. gracillima also have glabrous fruits, and a local endemic in Durango, B. chrysantha Barneby, differs primarily in its yellow perianths similar in size to those of B. gracillima subsp. decalvata. The complex is in need of careful study.</discussion>
  
</bio:treatment>