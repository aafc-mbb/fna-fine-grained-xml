<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Walter A. Kelley</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="mention_page">461</other_info_on_meta>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1823" rank="genus">CALANDRINIA</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>6: 77, plate 526. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus CALANDRINIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For J. L. Calandrini, 1703–1758, Swiss botanist</other_info_on_name>
    <other_info_on_name type="fna_id">105022</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, not rhizomatous or stoloniferous.</text>
      <biological_entity id="o12375" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to erect, branched;</text>
      <biological_entity id="o12376" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes glabrous.</text>
      <biological_entity id="o12377" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate, not articulate at base, somewhat to markedly clasping, attachment points linear;</text>
      <biological_entity id="o12378" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character constraint="at base" constraintid="o12379" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="articulate" value_original="articulate" />
        <character is_modifier="false" modifier="somewhat to markedly" name="architecture_or_fixation" notes="" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o12379" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="attachment" id="o12380" name="point" name_original="points" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear to oblanceolate, or ovate to spatulate, flattened, glabrous or with elongate unicellular hairs.</text>
      <biological_entity id="o12381" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="oblanceolate or ovate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="oblanceolate or ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with elongate unicellular hairs" value_original="with elongate unicellular hairs" />
      </biological_entity>
      <biological_entity id="o12382" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <relation from="o12381" id="r1788" name="with" negation="false" src="d0_s4" to="o12382" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, somewhat to markedly secund (at least distally), elongate, bracteate;</text>
      <biological_entity id="o12383" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
        <character is_modifier="false" modifier="somewhat to markedly" name="architecture" src="d0_s5" value="secund" value_original="secund" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts leaflike.</text>
      <biological_entity id="o12384" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers pedicellate;</text>
      <biological_entity id="o12385" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals persistent in fruit, imbricate, green, distinctly angled or keeled, ovate, herbaceous, glabrous or with elongate, unicellular hairs;</text>
      <biological_entity id="o12386" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character constraint="in fruit , imbricate , green , distinctly angled or keeled , ovate , herbaceous , glabrous or with hairs" constraintid="o12387" is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o12387" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="unicellular" value_original="unicellular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals usually 5, red;</text>
      <biological_entity id="o12388" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 3–15, usually opposite petals, not adnate to petals;</text>
      <biological_entity id="o12389" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="15" />
      </biological_entity>
      <biological_entity id="o12390" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="usually" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character constraint="to petals" constraintid="o12391" is_modifier="false" modifier="not" name="fusion" src="d0_s10" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o12391" name="petal" name_original="petals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovules 6–many;</text>
      <biological_entity id="o12392" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" is_modifier="false" name="quantity" src="d0_s11" to="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style present;</text>
      <biological_entity id="o12393" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas 3.</text>
      <biological_entity id="o12394" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules 3-valved, longitudinally dehiscent from apex, valves not deciduous, reflexed after dehiscence, margins markedly involute;</text>
      <biological_entity id="o12395" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-valved" value_original="3-valved" />
        <character constraint="from apex" constraintid="o12396" is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o12396" name="apex" name_original="apex" src="d0_s14" type="structure" />
      <biological_entity id="o12397" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o12398" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="markedly" name="shape_or_vernation" src="d0_s14" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>endocarp and exocarp not separating.</text>
      <biological_entity id="o12399" name="endocarp" name_original="endocarp" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s15" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o12400" name="exocarp" name_original="exocarp" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s15" value="separating" value_original="separating" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 10–20, black, ± ellipsoid, reticulate or tuberculate viewed at 30×, glabrous, estrophiolate.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 12.</text>
      <biological_entity id="o12401" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s16" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="black" value_original="black" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="relief" src="d0_s16" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="relief" src="d0_s16" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="estrophiolate" value_original="estrophiolate" />
      </biological_entity>
      <biological_entity constraint="x" id="o12402" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate w Americas, with greater diversity in w South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate w Americas" establishment_means="native" />
        <character name="distribution" value="with greater diversity in w South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species 14 (2 in the flora).</discussion>
  <references>
    <reference>Ford, D. I. 1992. Systematics and Evolution of Montiopsis Subgenus Montiopsis (Portulacaceae). Ph.D. thesis. Washington University.</reference>
    <reference>Hershkovitz, M. A. 1993b. Leaf morphology of Calandrinia and Montiopsis (Portulacaceae). Ann. Missouri Bot. Gard. 80: 366–396.</reference>
    <reference>Kelley, W. A. 1973. Pollen Morphology and Relationships in Calandrinia H. B. K. (Portulacaceae). M.S. thesis. California State University, Northridge.</reference>
    <reference>Hershkovitz, M. A. 1993. Revised circumscriptions and subgeneric taxonomies of Calandrinia and Montiopsis with notes on phylogeny of the portulacaceous alliance. Ann. Missouri Bot. Gard. 80: 333–365.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules usually exceeding calyx by 3+ mm; seeds at 30× partially or totally finely tuberculate</description>
      <determination>1 Calandrinia breweri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules usually not exceeding calyx by 3+ mm; seeds at 30× finely reticulate</description>
      <determination>2 Calandrinia ciliata</determination>
    </key_statement>
  </key>
</bio:treatment>