<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="(Engelmann) Lemaire" date="1868" rank="genus">coryphantha</taxon_name>
    <taxon_name authority="(K. Brandegee) Britton &amp; Rose" date="unknown" rank="species">nickelsiae</taxon_name>
    <place_of_publication>
      <publication_title>Cact.</publication_title>
      <place_in_publication>4: 35. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus coryphantha;species nickelsiae;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415317</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mammillaria</taxon_name>
    <taxon_name authority="K. Brandegee" date="unknown" rank="species">nickelsiae</taxon_name>
    <place_of_publication>
      <publication_title>Zoë</publication_title>
      <place_in_publication>5: 31. 1900 (as Mamillaria nickelsa e)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mammillaria;species nickelsiae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coryphantha</taxon_name>
    <taxon_name authority="Boedeker" date="unknown" rank="species">calochlora</taxon_name>
    <taxon_hierarchy>genus Coryphantha;species calochlora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coryphantha</taxon_name>
    <taxon_name authority="Bremer" date="unknown" rank="species">laui</taxon_name>
    <taxon_hierarchy>genus Coryphantha;species laui;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coryphantha</taxon_name>
    <taxon_name authority="Bremer" date="unknown" rank="species">neglecta</taxon_name>
    <taxon_hierarchy>genus Coryphantha;species neglecta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coryphantha</taxon_name>
    <taxon_name authority="(Engelmann) Britton &amp; Rose" date="unknown" rank="species">sulcata</taxon_name>
    <taxon_name authority="(K. Brandegee) L. D. Benson" date="unknown" rank="variety">nickelsiae</taxon_name>
    <taxon_hierarchy>genus Coryphantha;species sulcata;variety nickelsiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants unbranched (western) to freely branched and ± forming mats (eastern), tuberculate with numerous, fine spines.</text>
      <biological_entity id="o1205" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="unbranched" name="architecture" src="d0_s0" to="freely branched" />
        <character constraint="with spines" constraintid="o1207" is_modifier="false" modifier="more or less" name="relief" src="d0_s0" value="tuberculate" value_original="tuberculate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1206" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <biological_entity id="o1207" name="spine" name_original="spines" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="width" src="d0_s0" value="fine" value_original="fine" />
      </biological_entity>
      <relation from="o1205" id="r163" modifier="more or less" name="forming" negation="false" src="d0_s0" to="o1206" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse or short taproots;</text>
      <biological_entity id="o1208" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
      </biological_entity>
      <biological_entity id="o1209" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches root adventitiously.</text>
      <biological_entity constraint="branches" id="o1210" name="root" name_original="root" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems deep-seated, aerial portion hemispheric or ± flat-topped, less than 11 × (4–) 5.2–7 cm;</text>
      <biological_entity id="o1211" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="location" src="d0_s3" value="deep-seated" value_original="deep-seated" />
      </biological_entity>
      <biological_entity id="o1212" name="portion" name_original="portion" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="shape" src="d0_s3" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="flat-topped" value_original="flat-topped" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="11" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_width" src="d0_s3" to="5.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5.2" from_unit="cm" name="width" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>tubercles 7–10 (–13?) × 10 mm, firm;</text>
      <biological_entity id="o1213" name="tubercle" name_original="tubercles" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="10" to_unit="mm" />
        <character name="width" src="d0_s4" unit="mm" value="10" value_original="10" />
        <character is_modifier="false" name="texture" src="d0_s4" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>areolar glands seasonally conspicuous;</text>
      <biological_entity constraint="areolar" id="o1214" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="seasonally" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>parenchyma not mucilaginous;</text>
      <biological_entity id="o1215" name="parenchyma" name_original="parenchyma" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pith 1/2 of lesser stem diam.;</text>
      <biological_entity id="o1216" name="pith" name_original="pith" src="d0_s7" type="structure">
        <character constraint="of stem" constraintid="o1217" name="quantity" src="d0_s7" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o1217" name="stem" name_original="stem" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>medullary vascular system conspicuous.</text>
      <biological_entity constraint="medullary" id="o1218" name="system" name_original="system" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="vascular" value_original="vascular" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spines 13–21 per areole;</text>
      <biological_entity id="o1219" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o1220" from="13" name="quantity" src="d0_s9" to="21" />
      </biological_entity>
      <biological_entity id="o1220" name="areole" name_original="areole" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>uppermost radial spines white, contrasting with others, at least on old plants, sometimes forming conspicuous white tufts, others white to tan or dark-brown;</text>
      <biological_entity constraint="uppermost" id="o1221" name="spine" name_original="spines" src="d0_s10" type="structure" />
      <biological_entity id="o1222" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="radial" value_original="radial" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o1223" name="other" name_original="others" src="d0_s10" type="structure" />
      <biological_entity id="o1224" name="plant" name_original="plants" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="old" value_original="old" />
      </biological_entity>
      <biological_entity id="o1225" name="tuft" name_original="tufts" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o1226" name="other" name_original="others" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" modifier="sometimes" name="coloration" src="d0_s10" to="tan or dark-brown" />
      </biological_entity>
      <relation from="o1222" id="r164" name="contrasting with" negation="false" src="d0_s10" to="o1223" />
      <relation from="o1222" id="r165" modifier="at-least" name="on" negation="false" src="d0_s10" to="o1224" />
      <relation from="o1222" id="r166" modifier="sometimes" name="forming" negation="false" src="d0_s10" to="o1225" />
    </statement>
    <statement id="d0_s11">
      <text>radial spines 13–20 per areole, 13–23 × 0.15–0.17 mm;</text>
      <biological_entity id="o1227" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o1228" from="13" name="quantity" src="d0_s11" to="20" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" notes="" src="d0_s11" to="23" to_unit="mm" />
        <character char_type="range_value" from="0.15" from_unit="mm" name="width" notes="" src="d0_s11" to="0.17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1228" name="areole" name_original="areole" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>central spines 0–1 per areole, porrect, straight or slightly curved downward, longest spines 11–16 mm.</text>
      <biological_entity constraint="central" id="o1229" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o1230" from="0" name="quantity" src="d0_s12" to="1" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s12" value="porrect" value_original="porrect" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="downward" value_original="downward" />
      </biological_entity>
      <biological_entity id="o1230" name="areole" name_original="areole" src="d0_s12" type="structure" />
      <biological_entity constraint="longest" id="o1231" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s12" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers apical or nearly so, 45–50 mm diam.;</text>
      <biological_entity id="o1232" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="apical" value_original="apical" />
        <character name="position" src="d0_s13" value="nearly" value_original="nearly" />
        <character char_type="range_value" from="45" from_unit="mm" name="diameter" src="d0_s13" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>outer tepals entire;</text>
      <biological_entity constraint="outer" id="o1233" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>inner tepals ca. 30 per flower, pale-yellow, ca. 30 mm;</text>
      <biological_entity constraint="inner" id="o1234" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character constraint="per flower" constraintid="o1235" name="quantity" src="d0_s15" value="30" value_original="30" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="pale-yellow" value_original="pale-yellow" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="30" value_original="30" />
      </biological_entity>
      <biological_entity id="o1235" name="flower" name_original="flower" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>outer filaments white or pale-yellow;</text>
      <biological_entity constraint="outer" id="o1236" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers orange-yellow;</text>
      <biological_entity id="o1237" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="orange-yellow" value_original="orange-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma lobes 5–8, white or cream, (2.5–) 5–8 mm.</text>
      <biological_entity constraint="stigma" id="o1238" name="lobe" name_original="lobes" src="d0_s18" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s18" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="cream" value_original="cream" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s18" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits gray-green or bright green (paler proximally), obovoid, spheric, or ellipsoid, 18–23 × 8–10 mm;</text>
      <biological_entity id="o1239" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="shape" src="d0_s19" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s19" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s19" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s19" to="23" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s19" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>floral remnant strongly persistent.</text>
      <biological_entity constraint="floral" id="o1240" name="remnant" name_original="remnant" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="strongly" name="duration" src="d0_s20" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds orange, drying to reddish-brown, spheric to comma-shaped, 1.1–1.7 mm, finely and weakly raised-reticulate.</text>
      <biological_entity id="o1241" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="orange" value_original="orange" />
        <character is_modifier="false" name="condition" src="d0_s21" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s21" to="comma-shaped" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s21" to="1.7" to_unit="mm" />
        <character is_modifier="false" modifier="finely; weakly" name="architecture_or_coloration_or_relief" src="d0_s21" value="raised-reticulate" value_original="raised-reticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Aug–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Aug-Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tamaulipan thorn scrub, eastern edge of Chihuahuan Desert, limestone outcrops, nearby alluvium</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tamaulipan thorn scrub" />
        <character name="habitat" value="eastern edge" constraint="of chihuahuan desert" />
        <character name="habitat" value="limestone outcrops" />
        <character name="habitat" value="nearby alluvium" />
        <character name="habitat" value="desert" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-150 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="past_name">nickelsae</other_name>
  <other_name type="common_name">Nickels’s pincushion cactus</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Coryphantha nickelsiae in the flora is based on old herbarium specimens labeled only as from “Laredo, Texas.” Coryphantha nickelsiae belongs to the same species group as C. ramillosa. Coryphantha sulcata, with which L. D. Benson (1969b, 1969c, 1982) inexplicably united C. nickelsiae, is not closely related.</discussion>
  <discussion>Coryphantha nickelsiae often has ribbonlike uppermost radial spines.</discussion>
  
</bio:treatment>