<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="treatment_page">69</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">abronia</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="species">nana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts.</publication_title>
      <place_in_publication>14: 294. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus abronia;species nana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415101</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, acaulescent or nearly so, usually cespitose.</text>
      <biological_entity id="o20774" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character name="architecture" src="d0_s0" value="nearly" value_original="nearly" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 1–5 cm;</text>
      <biological_entity id="o20775" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o20776" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade elliptic to lanceolate, shortly ovate, or oblong-ovate, (0.4–) 0.5–2.5 × (0.2–) 0.4–1.2 cm, less than 3 times as long as wide, margins entire or ± repand and undulate, surfaces glabrous or glandular-pubescent.</text>
      <biological_entity id="o20777" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o20778" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="lanceolate shortly ovate or oblong-ovate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="lanceolate shortly ovate or oblong-ovate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="lanceolate shortly ovate or oblong-ovate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_length" src="d0_s2" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s2" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s2" to="0.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s2" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="0-3" value_original="0-3" />
      </biological_entity>
      <biological_entity id="o20779" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="repand" value_original="repand" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o20780" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: bracts lanceolate to ovate, 4–9 × 2–7 mm, scarious, glandular-puberulent, often minutely so;</text>
      <biological_entity id="o20781" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o20782" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>flowers 15–25.</text>
      <biological_entity id="o20783" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o20784" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s4" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Perianth: tube pale-pink, 8–30 mm, limb white to pink, 6–10 mm diam.</text>
      <biological_entity id="o20785" name="perianth" name_original="perianth" src="d0_s5" type="structure" />
      <biological_entity id="o20786" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-pink" value_original="pale-pink" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20787" name="limb" name_original="limb" src="d0_s5" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s5" to="pink" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruits obovate to obcordate in profile, 6–10 × 5–7 mm, scarious, apex low and broadly conic;</text>
      <biological_entity id="o20788" name="fruit" name_original="fruits" src="d0_s6" type="structure">
        <character char_type="range_value" from="obovate" modifier="in profile" name="shape" src="d0_s6" to="obcordate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o20789" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="low" value_original="low" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>wings 5, without dilations, without cavities.</text>
      <biological_entity id="o20790" name="wing" name_original="wings" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o20791" name="dilation" name_original="dilations" src="d0_s7" type="structure" />
      <biological_entity id="o20792" name="cavity" name_original="cavities" src="d0_s7" type="structure" />
      <relation from="o20790" id="r3011" name="without" negation="false" src="d0_s7" to="o20791" />
      <relation from="o20790" id="r3012" name="without" negation="false" src="d0_s7" to="o20792" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Abronia nana is a highly variable species. Perhaps contraction of the range of A. nana during the Pleistocene left isolated populations that have since diverged. This is especially apparent on the southern edge of the range of the species. In northeastern Arizona, densely tufted plants with very small blades are similar to short-leaved plants of A. bigelovii from north-central New Mexico.</discussion>
  <discussion>Based on the fruits, the taxon described as Abronia nana var. harrisii S. L. Welsh is A. elliptica.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades elliptic-lanceolate to elliptic-ovate; inflorescence bracts ovate to oblong-lanceolate, or vestigial</description>
      <determination>17a Abronia nana var. nana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades shortly ovate to oblong-ovate; inflorescence bracts lanceolate</description>
      <determination>17b Abronia nana var. covillei</determination>
    </key_statement>
  </key>
</bio:treatment>