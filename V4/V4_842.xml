<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">413</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="treatment_page">431</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">amaranthus</taxon_name>
    <taxon_name authority="(Kunth) Grenier &amp; Godron" date="1855" rank="subgenus">Albersia</taxon_name>
    <taxon_name authority="(Moquin-Tandon) S. Watson in W. H. Brewer et al." date="unknown" rank="species">californicus</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>2: 42. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus amaranthus;subgenus albersia;species californicus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415671</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mengea</taxon_name>
    <taxon_name authority="Moquin-Tandon" date="unknown" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>13(2): 270. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mengea;species californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, glabrous.</text>
      <biological_entity id="o4788" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, whitish or tinged with red, much-branched from base, 0.1–0.5 m, rather fleshy.</text>
      <biological_entity id="o4789" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="tinged with red" value_original="tinged with red" />
        <character constraint="from base" constraintid="o4790" is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" notes="" src="d0_s1" to="0.5" to_unit="m" />
        <character is_modifier="false" modifier="rather" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o4790" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1/2 or less as long as blade;</text>
      <biological_entity id="o4791" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4792" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1/2" value_original="1/2" />
        <character name="quantity" src="d0_s2" value="less" value_original="less" />
      </biological_entity>
      <biological_entity id="o4793" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <relation from="o4792" id="r639" name="as long as" negation="false" src="d0_s2" to="o4793" />
    </statement>
    <statement id="d0_s3">
      <text>blade pale green, veins prominent, obovate, spatulate, or oblanceolate to linear, 0.3–2 (–3) × 0.2–1.5 cm, base cuneate, margins entire, plane or slightly undulate, apex obtuse to subacute, with prominent mucro.</text>
      <biological_entity id="o4794" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4795" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale green" value_original="pale green" />
      </biological_entity>
      <biological_entity id="o4796" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4797" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o4798" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o4799" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="subacute" />
      </biological_entity>
      <biological_entity id="o4800" name="mucro" name_original="mucro" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o4799" id="r640" name="with" negation="false" src="d0_s3" to="o4800" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary clusters borne from bases to tops of plants.</text>
      <biological_entity id="o4801" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o4802" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o4803" name="top" name_original="tops" src="d0_s4" type="structure" />
      <biological_entity id="o4804" name="plant" name_original="plants" src="d0_s4" type="structure" />
      <relation from="o4801" id="r641" name="borne from" negation="false" src="d0_s4" to="o4802" />
      <relation from="o4801" id="r642" name="to" negation="false" src="d0_s4" to="o4803" />
      <relation from="o4803" id="r643" name="part_of" negation="false" src="d0_s4" to="o4804" />
    </statement>
    <statement id="d0_s5">
      <text>Bracts of pistillate flowers linear, 0.5–1 mm, ± equaling tepals.</text>
      <biological_entity id="o4805" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4806" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4807" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s5" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o4805" id="r644" name="part_of" negation="false" src="d0_s5" to="o4806" />
    </statement>
    <statement id="d0_s6">
      <text>Pistillate flowers: tepals 1–3, narrowly lanceolate, unequal, usually with only 1 well-developed tepal, largest 1–1.2 mm, apex acute to acuminate;</text>
      <biological_entity id="o4808" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4809" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4810" name="tepal" name_original="tepal" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="only" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="true" name="development" src="d0_s6" value="well-developed" value_original="well-developed" />
        <character is_modifier="false" name="size" notes="" src="d0_s6" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o4811" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <relation from="o4809" id="r645" modifier="usually" name="with" negation="false" src="d0_s6" to="o4810" />
    </statement>
    <statement id="d0_s7">
      <text>stigmas 3.</text>
      <biological_entity id="o4812" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4813" name="stigma" name_original="stigmas" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers intermixed with pistillate;</text>
      <biological_entity id="o4814" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals (2–) 3;</text>
      <biological_entity id="o4815" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s9" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 3.</text>
      <biological_entity id="o4816" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Utricles subglobose, 1–1.2 mm, smooth or wrinkled (especially in dry plants), dehiscence regularly circumscissile or tardily dehiscent.</text>
      <biological_entity id="o4817" name="utricle" name_original="utricles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s11" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="dehiscence" value_original="dehiscence" />
        <character is_modifier="false" modifier="regularly" name="dehiscence" src="d0_s11" value="circumscissile" value_original="circumscissile" />
        <character is_modifier="false" modifier="tardily" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds very dark reddish-brown, lenticular, (0.6–) 0.7–1 mm diam., shiny.</text>
      <biological_entity id="o4818" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="diameter" src="d0_s12" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="diameter" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s12" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally moist flats, shores of water bodies, waste places, other disturbed habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist flats" modifier="seasonally" />
        <character name="habitat" value="shores" constraint="of water bodies , waste places , other disturbed habitats" />
        <character name="habitat" value="water bodies" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="other disturbed habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Sask.; Calif., Idaho, Kans., Mont., Nebr., Nev., Oreg., S.Dak., Tex., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="past_name">Amarantus</other_name>
  <other_name type="common_name">Californian amaranth</other_name>
  <other_name type="common_name">California amaranth</other_name>
  
</bio:treatment>