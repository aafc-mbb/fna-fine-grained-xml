<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard W. Spellenberg</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="mention_page">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Standley" date="1909" rank="genus">ANULOCAULIS</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>12: 374. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus ANULOCAULIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin anulus, ring, and caule, stem, in reference to the sticky internodal rings</other_info_on_name>
    <other_info_on_name type="fna_id">102132</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, usually short to long-lived perennial, occasionally annual, stout, pubescent to glabrate, from slender and spongy or massive and woody taproots.</text>
      <biological_entity id="o3507" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="duration" src="d0_s0" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="occasionally" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s0" to="glabrate" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity constraint="slender and or" id="o3508" name="taproot" name_original="taproots" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="spongy" value_original="spongy" />
        <character is_modifier="true" name="size" src="d0_s0" value="massive" value_original="massive" />
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o3507" id="r458" name="from" negation="false" src="d0_s0" to="o3508" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, unarmed, with glutinous bands on internodes.</text>
      <biological_entity id="o3509" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <biological_entity id="o3510" name="band" name_original="bands" src="d0_s1" type="structure">
        <character is_modifier="true" name="coating" src="d0_s1" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <biological_entity id="o3511" name="internode" name_original="internodes" src="d0_s1" type="structure" />
      <relation from="o3509" id="r459" name="with" negation="false" src="d0_s1" to="o3510" />
      <relation from="o3510" id="r460" name="on" negation="false" src="d0_s1" to="o3511" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in pairs, ± equal in size in each pair, long petiolate, thick and fleshy, base ± symmetric.</text>
      <biological_entity id="o3512" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="in pair" constraintid="o3514" is_modifier="false" modifier="more or less" name="size" notes="" src="d0_s2" value="equal" value_original="equal" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="width" src="d0_s2" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o3513" name="pair" name_original="pairs" src="d0_s2" type="structure" />
      <biological_entity id="o3514" name="pair" name_original="pair" src="d0_s2" type="structure" />
      <biological_entity id="o3515" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <relation from="o3512" id="r461" name="in" negation="false" src="d0_s2" to="o3513" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal, pedunculate, widely paniculate, flowers ultimately borne singly, in subumbellate clusters, or in glomerules;</text>
      <biological_entity id="o3516" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o3517" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o3518" name="glomerule" name_original="glomerules" src="d0_s3" type="structure" />
      <relation from="o3517" id="r462" modifier="in subumbellate clusters" name="in" negation="false" src="d0_s3" to="o3518" />
    </statement>
    <statement id="d0_s4">
      <text>bracts persistent, not accrescent, 1–2 at base of pedicel, distinct, minute, lanceolate, dark, opaque.</text>
      <biological_entity id="o3519" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s4" value="accrescent" value_original="accrescent" />
        <character char_type="range_value" constraint="at base" constraintid="o3520" from="1" name="quantity" src="d0_s4" to="2" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s4" value="minute" value_original="minute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark" value_original="dark" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="opaque" value_original="opaque" />
      </biological_entity>
      <biological_entity id="o3520" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o3521" name="pedicel" name_original="pedicel" src="d0_s4" type="structure" />
      <relation from="o3520" id="r463" name="part_of" negation="false" src="d0_s4" to="o3521" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual, chasmogamous;</text>
      <biological_entity id="o3522" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth radially symmetric or slightly bilaterally symmetric, tubular-funnelform, constricted beyond ovary, tube abruptly flared to 5-lobed limb;</text>
      <biological_entity id="o3523" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="slightly bilaterally; slightly bilaterally" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character constraint="beyond ovary" constraintid="o3524" is_modifier="false" name="size" src="d0_s6" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o3524" name="ovary" name_original="ovary" src="d0_s6" type="structure" />
      <biological_entity id="o3525" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="abruptly flared" name="shape" src="d0_s6" to="5-lobed" />
      </biological_entity>
      <biological_entity id="o3526" name="limb" name_original="limb" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>stamens 3 or 5, exserted;</text>
      <biological_entity id="o3527" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s7" unit="or" value="5" value_original="5" />
        <character is_modifier="false" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>styles exserted beyond stamens;</text>
      <biological_entity id="o3528" name="style" name_original="styles" src="d0_s8" type="structure" />
      <biological_entity id="o3529" name="stamen" name_original="stamens" src="d0_s8" type="structure" />
      <relation from="o3528" id="r464" name="exserted beyond" negation="false" src="d0_s8" to="o3529" />
    </statement>
    <statement id="d0_s9">
      <text>stigmas peltate.</text>
      <biological_entity id="o3530" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="peltate" value_original="peltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits biturbinate or fusiform, constricted beyond base, apex umbonate, stiffly coriaceous, smooth or irregularly wrinkled, glabrous, with 10 narrow ribs and an equatorial flange or wing, or 5-angulate with a low rib between angles and no equatorial flange, or fusiform and smooth or with 10 low, linear ribs and sometimes with incomplete equatorial low, linear ridge, eglandular or glandular-puberulent.</text>
      <biological_entity id="o3531" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="biturbinate" value_original="biturbinate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="fusiform" value_original="fusiform" />
        <character constraint="beyond base" constraintid="o3532" is_modifier="false" name="size" src="d0_s10" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o3532" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o3533" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="umbonate" value_original="umbonate" />
        <character is_modifier="false" modifier="stiffly" name="texture" src="d0_s10" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="irregularly" name="relief" src="d0_s10" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character constraint="with rib" constraintid="o3537" is_modifier="false" name="shape" notes="" src="d0_s10" value="5-angulate" value_original="5-angulate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="with 10 low" value_original="with 10 low" />
      </biological_entity>
      <biological_entity id="o3534" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="10" value_original="10" />
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="equatorial" id="o3535" name="flange" name_original="flange" src="d0_s10" type="structure" />
      <biological_entity constraint="equatorial" id="o3536" name="wing" name_original="wing" src="d0_s10" type="structure" />
      <biological_entity constraint="between angles and flange" constraintid="o3538-o3539" id="o3537" name="rib" name_original="rib" src="d0_s10" type="structure" constraint_original="between  angles and  flange, ">
        <character is_modifier="true" name="position" src="d0_s10" value="low" value_original="low" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="no" value_original="no" />
      </biological_entity>
      <biological_entity id="o3538" name="angle" name_original="angles" src="d0_s10" type="structure" />
      <biological_entity id="o3539" name="flange" name_original="flange" src="d0_s10" type="structure" />
      <biological_entity constraint="equatorial" id="o3540" name="rib" name_original="rib" src="d0_s10" type="structure" />
      <biological_entity id="o3541" name="low" name_original="low" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o3542" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="equatorial" id="o3543" name="low" name_original="low" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="incomplete" value_original="incomplete" />
      </biological_entity>
      <biological_entity id="o3544" name="ridge" name_original="ridge" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <relation from="o3533" id="r465" name="with" negation="false" src="d0_s10" to="o3534" />
      <relation from="o3533" id="r466" name="with" negation="false" src="d0_s10" to="o3541" />
      <relation from="o3542" id="r467" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o3543" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Ringstem</other_name>
  <discussion>Species 5 (4 in the flora).</discussion>
  <discussion>Species of Anulocaulis have been considered to belong in a broadly circumscribed Boerhavia, the western A. annulatus most recently so in regional treatments. F. R. Fosberg (1978) argued for an inclusive Boerhavia, and suggested that workers emphasizing differences might wish to recognize segregate genera, as is done in this work.</discussion>
  <discussion>Flowers of Anulocaulis, particularly in those species with large ones, are slightly bilaterally symmetric because of curvature of the tube, variation in the amount of curvature of the limb, and the downward sweep of the stamens and the style.</discussion>
  <references>
    <reference>Spellenberg, R. 1993. Taxonomy of Anulocaulis (Nyctaginaceae). Sida 15: 373–389.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth 8-10 mm, tube externally villous; fruits without prominent equatorial ridges (sometimes faint in Anulocaulis annulatus)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth 9-35 mm, tube glabrous or minutely pubescent at edge; fruits with prominent equatorial flange</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers 5-15 in umbel-like clusters; fruits broadly fusiform, with 10 narrow, well- defined linear ribs</description>
      <determination>1 Anulocaulis annulatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers usually borne singly; fruits turbinate, bluntly 5-angled, ribs indefinite</description>
      <determination>2 Anulocaulis eriosolenus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perianth lobes flared but not reflexed; perianth 25-35 mm</description>
      <determination>3 Anulocaulis leiosolenus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perianth lobes sharply reflexed; perianth 9-10 mm</description>
      <determination>4 Anulocaulis reflexus</determination>
    </key_statement>
  </key>
</bio:treatment>