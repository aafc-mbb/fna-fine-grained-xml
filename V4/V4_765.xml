<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Peter W. Ball</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">259</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="A. J. Scott" date="1978" rank="genus">SARCOCORNIA</taxon_name>
    <place_of_publication>
      <publication_title>B ot. J. Linn. Soc.</publication_title>
      <place_in_publication>75: 366. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus SARCOCORNIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek sarco, fleshy, and Latin cornis, horned, in reference to the appearance of the plants</other_info_on_name>
    <other_info_on_name type="fna_id">129207</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Salicornia</taxon_name>
    <taxon_name authority="Duval-Jouve ex Moss" date="unknown" rank="section">Perennes</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot.</publication_title>
      <place_in_publication>49: 178. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Salicornia;section Perennes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, glabrous.</text>
      <biological_entity id="o18861" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems apparently jointed and fleshy when young, becoming woody and not jointed, not armed;</text>
    </statement>
    <statement id="d0_s2">
      <text>some stems terminated by an inflorescences, others entirely vegetative.</text>
      <biological_entity id="o18862" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="apparently" name="architecture" src="d0_s1" value="jointed" value_original="jointed" />
        <character is_modifier="false" modifier="when young" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="jointed" value_original="jointed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="armed" value_original="armed" />
      </biological_entity>
      <biological_entity id="o18863" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o18864" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o18865" name="other" name_original="others" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="entirely" name="reproduction" src="d0_s2" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o18863" id="r2721" name="terminated by" negation="false" src="d0_s2" to="o18864" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite, united at base, petiolate, decurrent forming fleshy joints (sterile segments) fleshy on stem, fleshy;</text>
      <biological_entity id="o18867" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o18868" name="joint" name_original="joints" src="d0_s3" type="structure">
        <character is_modifier="true" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o18869" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <relation from="o18866" id="r2722" name="forming" negation="false" src="d0_s3" to="o18868" />
    </statement>
    <statement id="d0_s4">
      <text>eventually deciduous;</text>
      <biological_entity id="o18866" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character constraint="at base" constraintid="o18867" is_modifier="false" name="fusion" src="d0_s3" value="united" value_original="united" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
        <character constraint="on stem" constraintid="o18869" is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="texture" notes="" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="eventually" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade fleshy triangular projections at tips of joints, edges with narrow scarious margins.</text>
      <biological_entity id="o18870" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o18871" name="projection" name_original="projections" src="d0_s5" type="structure">
        <character is_modifier="true" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="shape" src="d0_s5" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o18872" name="tip" name_original="tips" src="d0_s5" type="structure" />
      <biological_entity id="o18873" name="joint" name_original="joints" src="d0_s5" type="structure" />
      <biological_entity id="o18874" name="edge" name_original="edges" src="d0_s5" type="structure" />
      <biological_entity id="o18875" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o18871" id="r2723" name="at" negation="false" src="d0_s5" to="o18872" />
      <relation from="o18872" id="r2724" name="part_of" negation="false" src="d0_s5" to="o18873" />
      <relation from="o18874" id="r2725" name="with" negation="false" src="d0_s5" to="o18875" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal and lateral, spikelike, apparently jointed, each joint (fertile segment) consisting of 2 opposite, axillary, 3 (–5) -flowered cymes embedded in and adnate to fleshy tissue of distal internode;</text>
      <biological_entity id="o18876" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" modifier="apparently" name="architecture" src="d0_s6" value="jointed" value_original="jointed" />
      </biological_entity>
      <biological_entity id="o18877" name="joint" name_original="joint" src="d0_s6" type="structure" />
      <biological_entity id="o18878" name="axillary" name_original="axillary" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o18879" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="3(-5)-flowered" value_original="3(-5)-flowered" />
      </biological_entity>
      <biological_entity id="o18880" name="tissue" name_original="tissue" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
        <character is_modifier="true" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18881" name="internode" name_original="internode" src="d0_s6" type="structure" />
      <relation from="o18877" id="r2726" name="consisting of" negation="false" src="d0_s6" to="o18878" />
      <relation from="o18879" id="r2727" name="embedded in" negation="false" src="d0_s6" to="o18880" />
      <relation from="o18879" id="r2728" name="part_of" negation="false" src="d0_s6" to="o18881" />
    </statement>
    <statement id="d0_s7">
      <text>flowers in each cyme arranged in transverse rows, central flower separating lateral flowers, slightly larger;</text>
      <biological_entity id="o18882" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18883" name="cyme" name_original="cyme" src="d0_s7" type="structure">
        <character constraint="in rows" constraintid="o18884" is_modifier="false" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o18884" name="row" name_original="rows" src="d0_s7" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s7" value="transverse" value_original="transverse" />
      </biological_entity>
      <biological_entity constraint="central" id="o18885" name="flower" name_original="flower" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" notes="" src="d0_s7" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o18886" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="separating" value_original="separating" />
      </biological_entity>
      <relation from="o18882" id="r2729" name="in" negation="false" src="d0_s7" to="o18883" />
    </statement>
    <statement id="d0_s8">
      <text>flowers in each cyme partially separated by flaps of tissue which persist on stem when flowers have fallen.</text>
      <biological_entity id="o18887" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18888" name="cyme" name_original="cyme" src="d0_s8" type="structure">
        <character constraint="by flaps" constraintid="o18889" is_modifier="false" modifier="partially" name="arrangement" src="d0_s8" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o18889" name="flap" name_original="flaps" src="d0_s8" type="structure" />
      <biological_entity id="o18890" name="tissue" name_original="tissue" src="d0_s8" type="structure" />
      <biological_entity id="o18891" name="stem" name_original="stem" src="d0_s8" type="structure" />
      <relation from="o18887" id="r2730" name="in" negation="false" src="d0_s8" to="o18888" />
      <relation from="o18889" id="r2731" name="part_of" negation="false" src="d0_s8" to="o18890" />
      <relation from="o18889" id="r2732" name="persist on" negation="false" src="d0_s8" to="o18891" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual or unisexual, ± radially symmetric;</text>
      <biological_entity id="o18892" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="more or less radially" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth segments persistent in fruit, 3–4, united except at tip, fleshy;</text>
      <biological_entity constraint="perianth" id="o18893" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o18894" is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s10" value="united" value_original="united" />
        <character is_modifier="false" name="texture" src="d0_s10" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o18894" name="fruit" name_original="fruit" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity id="o18895" name="tip" name_original="tip" src="d0_s10" type="structure" />
      <relation from="o18893" id="r2733" name="except at" negation="false" src="d0_s10" to="o18895" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 1–2;</text>
      <biological_entity id="o18896" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 2–3.</text>
      <biological_entity id="o18897" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits utriclelike, ellipsoid;</text>
      <biological_entity id="o18898" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="utriclelike" value_original="utriclelike" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pericarp membranous.</text>
      <biological_entity id="o18899" name="pericarp" name_original="pericarp" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds vertical, ellipsoid;</text>
      <biological_entity id="o18900" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>seed-coat light-brown, membranous, pubescent;</text>
      <biological_entity id="o18901" name="seed-coat" name_original="seed-coat" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>hairs strongly curved or hooked and slender, or conic, straight or slightly curved;</text>
      <biological_entity id="o18902" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="strongly" name="course" src="d0_s17" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s17" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="size" src="d0_s17" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s17" value="conic" value_original="conic" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s17" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>perisperm absent.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 9.</text>
      <biological_entity id="o18903" name="perisperm" name_original="perisperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o18904" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, w Europe, Mediterranean region, s, e Africa, Australasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="w Europe" establishment_means="native" />
        <character name="distribution" value="Mediterranean region" establishment_means="native" />
        <character name="distribution" value="s" establishment_means="native" />
        <character name="distribution" value="e Africa" establishment_means="native" />
        <character name="distribution" value="Australasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Samphire</other_name>
  <other_name type="common_name">glasswort</other_name>
  <other_name type="common_name">saltwort</other_name>
  <discussion>Species ca. 15 (3 in the flora).</discussion>
  <discussion>Sarcocornia is taxonomically difficult and has never been the subject of a taxonomic revision for the Northern Hemisphere. Although it is possible to identify dry specimens to some extent, by comparison, it is impossible to obtain from dried specimens data that can be used in a taxonomic revision. Characters that may be taxonomically useful are lost on drying, especially flower and inflorescence characters and those derived from the fleshy vegetative segments. Habit appears to be useful, but few specimen labels note the habit of the living plant, and the parts collected rarely allow for a reliable determination of habit. Some species, such as S. perennis are prostrate, with the woody stems readily rooting in the substrate. Others such as S. pacifica are procumbent to erect shrubs in which the woody stems usually do not root. This apparently obvious habit difference is sometimes confounded by external factors, erect species becoming procumbent due to water movement, trampling, or burial by silt or sand deposits. Conversely, prostrate rooting species can be disturbed by erosion and appear to be procumbent plants of a nonrooting species.</discussion>
  <discussion>One of the most useful characters, the indumentum on the testa of the seeds, is rarely present in dried specimens because of the lateness of the plants’ flowering season. Plants collected in August and September rarely have even immature seeds present, so most herbarium specimens do not display this character.</discussion>
  <discussion>The consequence of these problems is that most accounts of Sarcocornia in North America recognize only one species, frequently using the name Salicornia virginica Linnaeus for the collective entity. The type specimens of S. virginica were collected by John Clayton, presumably from Virginia, which are immature annuals and not flowering. The name S. virginica cannot be applied to a species in this genus.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seeds smooth except for conic, straight papillae on edge; papillae 0.6-0.9 mm; anthers 0.9-1.8 mm</description>
      <determination>3 Sarcocornia utahensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seeds covered with slender, hooked or strongly curved hairs; hairs mostly 1-2 mm; anthers 0.7-1 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants with creeping, rooting, woody stems; erect stems simple or sparingly branched, 10-20(-30) cm; longest terminal spikes with 7-14 fertile segments</description>
      <determination>1 Sarcocornia perennis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants with erect or procumbent woody stems, sometimes creeping and rooting at base; stems sparingly to much-branched, 10-50 cm; longest terminal spikes on plant with 12-40 fertile segments</description>
      <determination>2 Sarcocornia pacifica</determination>
    </key_statement>
  </key>
</bio:treatment>