<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">456</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">blutaparon</taxon_name>
    <taxon_name authority="(Linnaeus) Mears" date="1982" rank="species">vermiculare</taxon_name>
    <place_of_publication>
      <publication_title>Taxon</publication_title>
      <place_in_publication>31: 113. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus blutaparon;species vermiculare</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415711</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gomphrena</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">vermicularis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 224. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gomphrena;species vermicularis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Philoxerus</taxon_name>
    <taxon_name authority="(Linnaeus)R Brown ex Smith" date="unknown" rank="species">vermicularis</taxon_name>
    <taxon_hierarchy>genus Philoxerus;species vermicularis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial or occasionally annual, 3–20 dm.</text>
      <biological_entity id="o22098" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="occasionally" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly prostrate with ascending branches, much-branched, glabrous.</text>
      <biological_entity id="o22099" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="with branches" constraintid="o22100" is_modifier="false" modifier="mostly" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22100" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear to linear-lanceolate or oblanceolate, 15–55 × 2–12 mm.</text>
      <biological_entity id="o22101" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="linear-lanceolate or oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: heads white or pinkish (drying silvery white), globose or cylindric, 7–30 × 7–10 mm;</text>
      <biological_entity id="o22102" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o22103" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s3" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracteoles slightly shorter than flowers.</text>
      <biological_entity id="o22104" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o22105" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character constraint="than flowers" constraintid="o22106" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o22106" name="flower" name_original="flowers" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers very short-pedicellate;</text>
      <biological_entity id="o22107" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s5" value="short-pedicellate" value_original="short-pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals connate basally, white, oblong, dorsiventrally compressed, thickened basally, 3–5 mm, chartaceous, apex of outer tepals obtuse, apex of inner tepals acute, spinose-tipped, densely villose.</text>
      <biological_entity id="o22108" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="dorsiventrally" name="shape" src="d0_s6" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="basally" name="size_or_width" src="d0_s6" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s6" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o22109" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="outer" id="o22110" name="tepal" name_original="tepals" src="d0_s6" type="structure" />
      <biological_entity id="o22111" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="spinose-tipped" value_original="spinose-tipped" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="villose" value_original="villose" />
      </biological_entity>
      <biological_entity constraint="inner" id="o22112" name="tepal" name_original="tepals" src="d0_s6" type="structure" />
      <relation from="o22109" id="r3208" name="part_of" negation="false" src="d0_s6" to="o22110" />
      <relation from="o22111" id="r3209" name="part_of" negation="false" src="d0_s6" to="o22112" />
    </statement>
    <statement id="d0_s7">
      <text>Utricles indehiscent or splitting irregularly, included in tepals, 1.3 mm.</text>
      <biological_entity id="o22113" name="utricle" name_original="utricles" src="d0_s7" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s7" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s7" value="splitting" value_original="splitting" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="1.3" value_original="1.3" />
      </biological_entity>
      <biological_entity id="o22114" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <relation from="o22113" id="r3210" name="included in" negation="false" src="d0_s7" to="o22114" />
    </statement>
    <statement id="d0_s8">
      <text>Seeds 0.8–1 mm.</text>
      <biological_entity id="o22115" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Saline soils, sands of beaches, dunes, sand bars</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saline soils" />
        <character name="habitat" value="sands" constraint="of beaches , dunes , sand bars" />
        <character name="habitat" value="beaches" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="sand bars" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., La., Tex.; Mexico; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Silverweed</other_name>
  <other_name type="common_name">saltweed</other_name>
  <other_name type="common_name">samphire</other_name>
  
</bio:treatment>