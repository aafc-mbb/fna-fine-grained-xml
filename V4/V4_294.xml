<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce D. Parfitt,Arthur C. Gibson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="treatment_page">152</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton" date="1908" rank="genus">HARRISIA</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>35: 561. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus harrisia;</taxon_hierarchy>
    <other_info_on_name type="etymology">for William H. Harris, 1860–1920, Superintendent of Public Gardens and Plantations of Jamaica</other_info_on_name>
    <other_info_on_name type="fna_id">114709</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Miller" date="unknown" rank="genus">Cereus</taxon_name>
    <taxon_name authority="A. Berger" date="unknown" rank="subgenus">Eriocereus</taxon_name>
    <taxon_hierarchy>genus Cereus;subgenus Eriocereus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(A.B erger) Riccobono" date="unknown" rank="genus">Eriocereus</taxon_name>
    <taxon_hierarchy>genus Eriocereus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Backeberg" date="unknown" rank="genus">Roseocereus</taxon_name>
    <taxon_hierarchy>genus Roseocereus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, often treelike, erect or old plants with branches usually ascending, clambering, or prostrate, sparingly branched or unbranched.</text>
      <biological_entity id="o9313" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s0" value="arboreous" value_original="treelike" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="clambering" value_original="clambering" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="clambering" value_original="clambering" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9315" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o9313" id="r1331" name="with" negation="false" src="d0_s0" to="o9315" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse (sometimes tuberlike).</text>
      <biological_entity id="o9316" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unsegmented, green, long cylindric, 100–600 × 2.5–6 cm, glabrous;</text>
      <biological_entity id="o9317" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="100" from_unit="cm" name="length" src="d0_s2" to="600" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ribs 8–12, rounded, low, less than 1 cm deep, shallowly to indistinctly tuberculate;</text>
      <biological_entity id="o9318" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s3" to="12" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="position" src="d0_s3" value="low" value_original="low" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="1" to_unit="cm" />
        <character is_modifier="false" name="depth" src="d0_s3" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="shallowly to indistinctly" name="relief" src="d0_s3" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>areoles ca. 2 cm apart along ribs, circular to oval, short woolly, sometimes subtended by small, subulate, deciduous leaves;</text>
      <biological_entity id="o9319" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="cm" value="2" value_original="2" />
        <character constraint="along ribs" constraintid="o9320" is_modifier="false" name="arrangement" src="d0_s4" value="apart" value_original="apart" />
        <character char_type="range_value" from="circular" name="shape" notes="" src="d0_s4" to="oval" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o9320" name="rib" name_original="ribs" src="d0_s4" type="structure" />
      <biological_entity id="o9321" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character is_modifier="true" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <relation from="o9319" id="r1332" modifier="sometimes" name="subtended by" negation="false" src="d0_s4" to="o9321" />
    </statement>
    <statement id="d0_s5">
      <text>areolar glands not apparent;</text>
      <biological_entity constraint="areolar" id="o9322" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s5" value="apparent" value_original="apparent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cortex and pith mucilaginous.</text>
      <biological_entity id="o9323" name="cortex" name_original="cortex" src="d0_s6" type="structure">
        <character is_modifier="false" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o9324" name="pith" name_original="pith" src="d0_s6" type="structure">
        <character is_modifier="false" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spines 6–17 per areole, usually porrect to ascending, white to pinkish or yellow to light-brown, aging gray, sometimes with tips darker or yellowish, acicular, straight, ± terete, 10–40 × 0.5–0.75 [–1.5] mm, hard, smooth and glabrous;</text>
      <biological_entity id="o9325" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o9326" from="6" name="quantity" src="d0_s7" to="17" />
        <character char_type="range_value" from="usually porrect" name="orientation" notes="" src="d0_s7" to="ascending" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pinkish or yellow" />
        <character is_modifier="false" name="life_cycle" src="d0_s7" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="gray" value_original="gray" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="acicular" value_original="acicular" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="terete" value_original="terete" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="0.75" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="0.75" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="hard" value_original="hard" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9326" name="areole" name_original="areole" src="d0_s7" type="structure" />
      <biological_entity id="o9327" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="darker" value_original="darker" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <relation from="o9325" id="r1333" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o9327" />
    </statement>
    <statement id="d0_s8">
      <text>radial spines and central spines not clearly distinguishable.</text>
      <biological_entity constraint="central" id="o9329" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="radial" value_original="radial" />
        <character is_modifier="false" modifier="not clearly" name="prominence" src="d0_s8" value="distinguishable" value_original="distinguishable" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers nocturnal, remaining open next day, lateral or terminal on stems at least 1 year old, funnelform, 12–20 [–25] × 8–12 cm;</text>
      <biological_entity id="o9330" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="nocturnal" value_original="nocturnal" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="old" value_original="old" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="25" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s9" to="20" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s9" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9331" name="stem" name_original="stems" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character is_modifier="true" name="growth_order_or_position" src="d0_s9" value="next" value_original="next" />
        <character is_modifier="true" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="true" name="position" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <relation from="o9330" id="r1334" modifier="at-least" name="remaining" negation="false" src="d0_s9" to="o9331" />
    </statement>
    <statement id="d0_s10">
      <text>outer tepals green to reddish or purplish, linear to lanceolate or narrowly oblong, 50–60 × 4.5–6 mm, margins entire;</text>
      <biological_entity constraint="outer" id="o9332" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="reddish or purplish" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s10" to="lanceolate or narrowly oblong" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s10" to="60" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9333" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>inner tepals white to pinkish, 60–75 × 12–20 mm, margins entire or denticulate;</text>
      <biological_entity constraint="inner" id="o9334" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pinkish" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s11" to="75" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9335" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary usually conspicuously tuberculate, scaly, spineless or spines represented by soft and silky to stiff hairs [or spiny];</text>
      <biological_entity id="o9336" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually conspicuously" name="relief" src="d0_s12" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s12" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="spineless" value_original="spineless" />
      </biological_entity>
      <biological_entity id="o9337" name="spine" name_original="spines" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>scales entire;</text>
      <biological_entity id="o9338" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma lobes 8–15, yellow-green to white, 6–9 mm.</text>
      <biological_entity constraint="stigma" id="o9339" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s14" to="15" />
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s14" to="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits indehiscent or splitting irregularly from apex toward base, green to yellow, red, or orange-red, spheric to ovoid-spheric, [30–] 40–75 [–80] × [30–] 40–75 [–80] mm, usually spineless (or spines bristlelike) [or slender acicular];</text>
      <biological_entity id="o9340" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="indehiscent" value_original="indehiscent" />
        <character constraint="from apex" constraintid="o9341" is_modifier="false" name="dehiscence" src="d0_s15" value="splitting" value_original="splitting" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s15" to="yellow red or orange-red" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s15" to="yellow red or orange-red" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s15" to="yellow red or orange-red" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s15" to="ovoid-spheric" />
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_length" src="d0_s15" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s15" to="80" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s15" to="75" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_width" src="d0_s15" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s15" to="80" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="width" src="d0_s15" to="75" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s15" value="spineless" value_original="spineless" />
      </biological_entity>
      <biological_entity id="o9341" name="apex" name_original="apex" src="d0_s15" type="structure" />
      <biological_entity id="o9342" name="base" name_original="base" src="d0_s15" type="structure" />
      <relation from="o9341" id="r1335" name="toward" negation="false" src="d0_s15" to="o9342" />
    </statement>
    <statement id="d0_s16">
      <text>scales deciduous [sometimes persistent];</text>
      <biological_entity id="o9343" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pulp white;</text>
      <biological_entity id="o9344" name="pulp" name_original="pulp" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>floral remnant usually persistent.</text>
      <biological_entity constraint="floral" id="o9345" name="remnant" name_original="remnant" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds black, broadly ovoid or broadly ellipsoid, 2–3 × 1.5 mm, warty.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = 11.</text>
      <biological_entity id="o9346" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="black" value_original="black" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s19" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s19" to="3" to_unit="mm" />
        <character name="width" src="d0_s19" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s19" value="warty" value_original="warty" />
      </biological_entity>
      <biological_entity constraint="x" id="o9347" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., West Indies, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Applecactus</other_name>
  <discussion>Species ca. 20 (3 in the flora).</discussion>
  <discussion>In lieu of a detailed study of Harrisia, this treatment respects earlier taxonomies based in large part on the fruit color and the colors of the hairs in the areoles of the flowers and fruits. Both characters may prove to be variable within taxa of Harrisia and therefore not be taxonomically useful.</discussion>
  <discussion>Harrisia was traditionally classified with columnar cacti of North America and Central America, e.g., have kinship with Acanthocereus, Selenicereus, or Hylocereus, which also form long-tubed, nocturnal flowers. Recent DNA analyses have demonstrated that Harrisia belongs instead to a large South American clade of columnar cacti (R. S. Wallace and A. C. Gibson 2002), with which they share axillary tufts of silky hairs on the flower tube.</discussion>
  <discussion>The basis for E. F. Anderson’s (2001) inclusion of Harrisia eriophora in the flora, in addition to the species recognized here, is not apparent. He recognized H. donae-antoniae, an invalid name for a local Florida variant of H. aboriginum, in the synonymy of H. gracilis (Miller) Britton. He apparently did not intend, however, to include H. gracilis as native to Florida because he listed its geographic distribution as “Jamaica.”</discussion>
  <discussion>The fruits of several species of Harrisia are sweet and edible.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flower buds with brown hairs; scales of flower tube with axillary tufts of stiff, tawny brown hairs; fruits dull yellow at maturity</description>
      <determination>1 Harrisia aboriginum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flower buds with white hairs; scales of flower tube with axillary tufts of soft, white hairs; fruits dull red or orange-red at maturity</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flower tubes prominently ridged; scales turgid near base; fruits depressed-spheric</description>
      <determination>2 Harrisia simpsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flower tube smooth, not or scarcely ridged; scales flat or nearly so; fruits obovoid</description>
      <determination>3 Harrisia fragrans</determination>
    </key_statement>
  </key>
</bio:treatment>