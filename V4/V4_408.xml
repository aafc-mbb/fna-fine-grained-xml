<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="(K. Schumann) Britton &amp; Rose" date="1922" rank="genus">thelocactus</taxon_name>
    <taxon_name authority="(Galeotti ex Pfeiffer) Britton &amp; Rose" date="1922" rank="species">bicolor</taxon_name>
    <taxon_name authority="Backeberg" date="1941" rank="variety">flavidispinus</taxon_name>
    <place_of_publication>
      <publication_title>Beitr. Sukkulentenk. Sukkulentenpflege</publication_title>
      <place_in_publication>1941: 6. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus thelocactus;species bicolor;variety flavidispinus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415314</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants unbranched (very rarely branched), deep-seated in substrate.</text>
      <biological_entity id="o11696" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character constraint="in substrate" constraintid="o11697" is_modifier="false" name="location" src="d0_s0" value="deep-seated" value_original="deep-seated" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11697" name="substrate" name_original="substrate" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems flat-topped when young, ± spheric (rarely short cylindric when old), 3–9 (–13) × 4–6 (–7) cm, surface hidden by spines;</text>
      <biological_entity id="o11698" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when young" name="shape" src="d0_s1" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="13" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s1" to="9" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="7" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11699" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character constraint="by spines" constraintid="o11700" is_modifier="false" name="prominence" src="d0_s1" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity id="o11700" name="spine" name_original="spines" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>ribs poorly defined, ca. 13, tubercles confluent only at extreme bases (youngest sexually mature plants strictly tuberculate), 8–15 mm wide;</text>
      <biological_entity id="o11701" name="rib" name_original="ribs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="poorly" name="prominence" src="d0_s2" value="defined" value_original="defined" />
        <character name="quantity" src="d0_s2" value="13" value_original="13" />
      </biological_entity>
      <biological_entity id="o11702" name="tubercle" name_original="tubercles" src="d0_s2" type="structure">
        <character constraint="at extreme bases" constraintid="o11703" is_modifier="false" name="arrangement" src="d0_s2" value="confluent" value_original="confluent" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" notes="" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="extreme" id="o11703" name="base" name_original="bases" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>areoles spaced 6–15 mm apart along ribs.</text>
      <biological_entity id="o11704" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="spaced" value_original="spaced" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character constraint="along ribs" constraintid="o11705" is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity id="o11705" name="rib" name_original="ribs" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Spines yellow (very rarely some individuals red);</text>
      <biological_entity id="o11706" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>radial spines 12–20 per areole, longest spines 9–18 (–24) mm;</text>
      <biological_entity id="o11707" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o11708" from="12" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o11708" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity constraint="longest" id="o11709" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="24" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>adaxial (bladelike) spines 18–35 (–40) × 0.5–1.5 mm;</text>
      <biological_entity constraint="adaxial" id="o11710" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="40" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s6" to="35" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>central spines (0–) 1 (–4), longest spines terete, (9–) 13–18 (–24) × 0.4–0.5 mm. 2n = 22.</text>
      <biological_entity constraint="central" id="o11711" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s7" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="4" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="longest" id="o11712" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="terete" value_original="terete" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_length" src="d0_s7" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="24" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s7" to="18" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11713" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Caballos novaculite outcrops in semidesert grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="caballos" />
        <character name="habitat" value="outcrops" modifier="novaculite" constraint="in semidesert grasslands" />
        <character name="habitat" value="semidesert grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <discussion>Thelocactus bicolor var. flavidispinus is the most widespread of the several taxa of cacti endemic to novaculite, a highly fractured, quartzlike rock. Unusually tall and/or red-spined individuals within populations of var. flavidispinus have been the basis for mistaken reports of var. bicolor in the region of novaculite, where only var. flavidispinus occurs. The reports of var. flavidispinus in south Texas (Starr County; L. D. Benson 1982) are based on misidentified T. bicolor var. bicolor.</discussion>
  
</bio:treatment>