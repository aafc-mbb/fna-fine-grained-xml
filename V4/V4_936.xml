<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="treatment_page">470</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">claytonia</taxon_name>
    <taxon_name authority="McNeill" date="1972" rank="species">ogilviensis</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>50: 1895. 1972</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus claytonia;species ogilviensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415738</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, with napiform to globose tubers 10–25 mm diam., mature plants rhizomatous;</text>
      <biological_entity id="o1264" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="mature" value_original="mature" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1265" name="tuber" name_original="tubers" src="d0_s0" type="structure">
        <character char_type="range_value" from="napiform" is_modifier="true" name="shape" src="d0_s0" to="globose" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s0" to="25" to_unit="mm" />
      </biological_entity>
      <relation from="o1264" id="r171" name="with" negation="false" src="d0_s0" to="o1265" />
    </statement>
    <statement id="d0_s1">
      <text>periderm 5–12 mm.</text>
      <biological_entity id="o1267" name="periderm" name_original="periderm" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 4–8 cm.</text>
      <biological_entity id="o1268" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal leaves often absent, petiolate, blade orbiculate, ca. as long as wide, 0.6–1 cm;</text>
      <biological_entity id="o1269" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o1270" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o1271" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="orbiculate" value_original="orbiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves petiolate, blade elliptic to ovate, 1–2.5 × 0.5–1.8 cm, abruptly contracted to petiole.</text>
      <biological_entity id="o1272" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o1273" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o1274" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="1.8" to_unit="cm" />
        <character constraint="to petiole" constraintid="o1275" is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o1275" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences umbellate clusters, multibracteate;</text>
      <biological_entity id="o1276" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="multibracteate" value_original="multibracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts leaflike.</text>
      <biological_entity id="o1277" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 10–16 mm diam.;</text>
      <biological_entity id="o1278" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 5–7 mm;</text>
      <biological_entity id="o1279" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals bright purple, 8–14 mm.</text>
      <biological_entity id="o1280" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright purple" value_original="bright purple" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 2.4–2.5 mm diam.</text>
      <biological_entity id="o1281" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="diameter" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stony slopes, calcareous talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stony slopes" />
        <character name="habitat" value="calcareous talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Ogilvie claytonia</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>