<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="mention_page">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">287</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">chenopodium</taxon_name>
    <taxon_name authority="(Standley) Clemants &amp; Mosyakin" date="1996" rank="subsection">Leptophylla</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>6: 400. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus chenopodium;subgenus chenopodium;section chenopodium;subsection Leptophylla</taxon_hierarchy>
    <other_info_on_name type="fna_id">302060</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chenopodium</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="unranked">Leptophylla</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>21: 14. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chenopodium;unranked Leptophylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades linear or linear-lanceolate, occasionally narrowly oblong-ovate or triangular (C. atrovirens), thick, margins entire or with pair of basal teeth or lobes, farinose or rarely glabrous.</text>
      <biological_entity id="o20819" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s0" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="occasionally narrowly" name="shape" src="d0_s0" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="shape" src="d0_s0" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s0" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s0" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="occasionally narrowly" name="shape" src="d0_s0" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="shape" src="d0_s0" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o20820" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="with pair" value_original="with pair" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="farinose" value_original="farinose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20821" name="pair" name_original="pair" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o20822" name="tooth" name_original="teeth" src="d0_s0" type="structure" />
      <biological_entity id="o20823" name="lobe" name_original="lobes" src="d0_s0" type="structure" />
      <relation from="o20820" id="r3016" name="with" negation="false" src="d0_s0" to="o20821" />
      <relation from="o20821" id="r3017" name="part_of" negation="false" src="d0_s0" to="o20822" />
      <relation from="o20821" id="r3018" name="part_of" negation="false" src="d0_s0" to="o20823" />
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences glomerules on terminal and axillary panicles.</text>
      <biological_entity constraint="inflorescences" id="o20824" name="glomerule" name_original="glomerules" src="d0_s1" type="structure" />
      <biological_entity constraint="terminal and axillary" id="o20825" name="panicle" name_original="panicles" src="d0_s1" type="structure" />
      <relation from="o20824" id="r3019" name="on" negation="false" src="d0_s1" to="o20825" />
    </statement>
    <statement id="d0_s2">
      <text>Seeds with margins rounded or acute;</text>
      <biological_entity id="o20826" name="seed" name_original="seeds" src="d0_s2" type="structure" />
      <biological_entity id="o20827" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o20826" id="r3020" name="with" negation="false" src="d0_s2" to="o20827" />
    </statement>
    <statement id="d0_s3">
      <text>seed-coat smooth, rugulate, or warty.</text>
      <biological_entity id="o20828" name="seed-coat" name_original="seed-coat" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s3" value="rugulate" value_original="rugulate" />
        <character is_modifier="false" name="relief" src="d0_s3" value="warty" value_original="warty" />
        <character is_modifier="false" name="relief" src="d0_s3" value="rugulate" value_original="rugulate" />
        <character is_modifier="false" name="relief" src="d0_s3" value="warty" value_original="warty" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7b.2d.</number>
  <discussion>Species 9 (9 in the flora).</discussion>
  <references>
    <reference>Crawford, D. J. 1975. Systematic relationships in the narrow-leaved species of Chenopodium of the western United States. Brittonia 27: 279–288.</reference>
  </references>
  
</bio:treatment>