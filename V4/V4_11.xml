<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="treatment_page">8</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">phytolaccaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">phytolacca</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1817" rank="species">bogotensis</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>2: 183. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phytolaccaceae;genus phytolacca;species bogotensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415003</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 2 m.</text>
      <biological_entity id="o20418" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 0.5–3 (–4) cm;</text>
      <biological_entity id="o20419" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o20420" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade broadly lanceolate to elliptic, to 18 × 7 cm, base obtuse to attenuate, apex acute to acuminate.</text>
      <biological_entity id="o20421" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o20422" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s2" to="elliptic" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="18" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20423" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="attenuate" />
      </biological_entity>
      <biological_entity id="o20424" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes dense, proximalmost pedicels sometimes bearing 2–few flowers, to 20 cm;</text>
      <biological_entity id="o20425" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o20426" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20427" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="few" />
      </biological_entity>
      <relation from="o20426" id="r2976" name="bearing" negation="false" src="d0_s3" to="o20427" />
    </statement>
    <statement id="d0_s4">
      <text>peduncle to 5 (–8) cm;</text>
      <biological_entity id="o20428" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel 2–4 mm.</text>
      <biological_entity id="o20429" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 5, white to red, oblong to ovate, subequal, 2–3 × 1.2–2 mm;</text>
      <biological_entity id="o20430" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o20431" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s6" to="red" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="ovate" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 7–12, in 1 whorl (rarely, partial 2d whorl present);</text>
      <biological_entity id="o20432" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o20433" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="12" />
      </biological_entity>
      <biological_entity id="o20434" name="whorl" name_original="whorl" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <relation from="o20433" id="r2977" name="in" negation="false" src="d0_s7" to="o20434" />
    </statement>
    <statement id="d0_s8">
      <text>carpels 7–10, fully connate or sometimes connate only in proximal 1/2;</text>
      <biological_entity id="o20435" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o20436" name="carpel" name_original="carpels" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" modifier="fully" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character constraint="in proximal 1/2" constraintid="o20437" is_modifier="false" modifier="sometimes; sometimes" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20437" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>ovary 7–10-loculed.</text>
      <biological_entity id="o20438" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o20439" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="7-10-loculed" value_original="7-10-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Berries green-brown becoming dark-brown, 5–8 mm diam.</text>
      <biological_entity id="o20440" name="berry" name_original="berries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s10" value="green-brown becoming dark-brown" value_original="green-brown becoming dark-brown" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds black, thickly lenticular, 2.5–3.3 mm, shiny.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 36.</text>
      <biological_entity id="o20441" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" modifier="thickly" name="shape" src="d0_s11" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.3" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s11" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20442" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering; fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chrome ore piles</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ore piles" modifier="chrome" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Md.; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>