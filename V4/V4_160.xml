<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Nancy J. Vivrette</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="mention_page">77</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">78</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">aizoaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">GALENIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 359. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 169. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aizoaceae;genus GALENIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Claudius Galenius, a.d. 130–200, Roman physician and writer on medicine</other_info_on_name>
    <other_info_on_name type="fna_id">113166</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs [herbs or shrubs], perennial.</text>
      <biological_entity id="o6705" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fibrous.</text>
      <biological_entity id="o6706" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ± prostrate, semiwoody at base, with vesicular, peltate, hairlike scales.</text>
      <biological_entity id="o6707" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character constraint="at base" constraintid="o6708" is_modifier="false" name="texture" src="d0_s2" value="semiwoody" value_original="semiwoody" />
      </biological_entity>
      <biological_entity id="o6708" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o6709" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape_or_texture" src="d0_s2" value="vesicular" value_original="vesicular" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="peltate" value_original="peltate" />
        <character is_modifier="true" name="shape" src="d0_s2" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <relation from="o6707" id="r910" name="with" negation="false" src="d0_s2" to="o6709" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, alternate [opposite], sessile;</text>
      <biological_entity id="o6710" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules absent;</text>
      <biological_entity id="o6711" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade flat, margins entire;</text>
      <biological_entity id="o6712" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o6713" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>earlier leaves larger within 1 growing season.</text>
      <biological_entity constraint="earlier" id="o6714" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character constraint="within" is_modifier="false" name="size" src="d0_s6" value="larger" value_original="larger" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6715" name="season" name_original="season" src="d0_s6" type="structure" />
      <relation from="o6714" id="r911" name="growing" negation="false" src="d0_s6" to="o6715" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary, flowers solitary, sessile;</text>
      <biological_entity id="o6716" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o6717" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts absent.</text>
      <biological_entity id="o6718" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual, inconspicuous, 2–3 mm diam.;</text>
      <biological_entity id="o6719" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calyx tubular;</text>
      <biological_entity id="o6720" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calyx lobes [4 or] 5, white, pink, or yellow, linear, oblong, or lanceolate-oblong;</text>
      <biological_entity constraint="calyx" id="o6721" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s11" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate-oblong" value_original="lanceolate-oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate-oblong" value_original="lanceolate-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals and petaloid staminodia absent;</text>
      <biological_entity id="o6722" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6723" name="staminodium" name_original="staminodia" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary glands absent;</text>
      <biological_entity constraint="nectary" id="o6724" name="gland" name_original="glands" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 10, paired, pairs alternating with calyx lobes;</text>
      <biological_entity id="o6725" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
        <character is_modifier="false" name="arrangement" src="d0_s14" value="paired" value_original="paired" />
        <character constraint="with calyx lobes" constraintid="o6726" is_modifier="false" name="arrangement" src="d0_s14" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o6726" name="lobe" name_original="lobes" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pistil 2–5-carpellate;</text>
      <biological_entity id="o6727" name="pistil" name_original="pistil" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-5-carpellate" value_original="2-5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary superior, 2–5-loculed, angled;</text>
    </statement>
    <statement id="d0_s17">
      <text>placentation apical-axile;</text>
      <biological_entity id="o6728" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="2-5-loculed" value_original="2-5-loculed" />
        <character is_modifier="false" name="shape" src="d0_s16" value="angled" value_original="angled" />
        <character is_modifier="false" name="placentation" src="d0_s17" value="apical-axile" value_original="apical-axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovule 1 per locule, with long funicles;</text>
      <biological_entity id="o6729" name="ovule" name_original="ovule" src="d0_s18" type="structure">
        <character constraint="per locule" constraintid="o6730" name="quantity" src="d0_s18" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6730" name="locule" name_original="locule" src="d0_s18" type="structure" />
      <biological_entity id="o6731" name="funicle" name_original="funicles" src="d0_s18" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s18" value="long" value_original="long" />
      </biological_entity>
      <relation from="o6729" id="r912" name="with" negation="false" src="d0_s18" to="o6731" />
    </statement>
    <statement id="d0_s19">
      <text>styles 2–5;</text>
      <biological_entity id="o6732" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s19" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas 2–5.</text>
      <biological_entity id="o6733" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s20" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits capsules, 3–5-angled, leathery, apex 3–5-lobed, truncate, dehiscence loculicidal at apex of angles.</text>
      <biological_entity constraint="fruits" id="o6734" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="3-5-angled" value_original="3-5-angled" />
        <character is_modifier="false" name="texture" src="d0_s21" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o6735" name="apex" name_original="apex" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="3-5-lobed" value_original="3-5-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s21" value="truncate" value_original="truncate" />
        <character constraint="at apex" constraintid="o6736" is_modifier="false" name="dehiscence" src="d0_s21" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
      <biological_entity id="o6736" name="apex" name_original="apex" src="d0_s21" type="structure" />
      <biological_entity id="o6737" name="angle" name_original="angles" src="d0_s21" type="structure" />
      <relation from="o6736" id="r913" name="part_of" negation="false" src="d0_s21" to="o6737" />
    </statement>
    <statement id="d0_s22">
      <text>Seeds 2–5, black, ribbed, triangular to compressed and reniform, 1 mm, shiny, tuberculate;</text>
      <biological_entity id="o6738" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s22" to="5" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ribbed" value_original="ribbed" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s22" to="compressed" />
        <character is_modifier="false" name="shape" src="d0_s22" value="reniform" value_original="reniform" />
        <character name="some_measurement" src="d0_s22" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="reflectance" src="d0_s22" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="relief" src="d0_s22" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>arils absent.</text>
      <biological_entity id="o6739" name="aril" name_original="arils" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; s Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Species 27 (2 in the flora).</discussion>
  <discussion>The two species of Galenia introduced in the flora belong to subgenus Kolleria (C. Presl) Frenzl as emended by R. S. Adamson (1956; 21 spp.). Kolleria is characterized by alternate, obovate leaves over 5 mm wide, four or five perianth segments, and four or five styles.</discussion>
  <discussion>The key below is adapted from A. Prescott (1984).</discussion>
  <references>
    <reference>Adamson, R. S. 1956. The South African species of Aizoaceae III. Galenia L. J. S. African Bot. 22: 87–127.</reference>
    <reference>Prescott, A. 1984. Galenia. In: R. Robertson et al., eds. 1981+. Flora of Australia. 23+ vols. Canberra. Vol. 4, pp. 50–52.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves and stems gray-green; younger stems with sparse, closely appressed scales, 0.5 mm; calyx lobes white to pink adaxially; styles 5</description>
      <determination>1 Galenia pubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves and stems gray-white; younger stems with abundant, loosely appressed scales, 1.5 mm; calyx lobes white to yellow adaxially; styles usually 4</description>
      <determination>2 Galenia secunda</determination>
    </key_statement>
  </key>
</bio:treatment>