<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">27</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">boerhavia</taxon_name>
    <taxon_name authority="(Hooker f.) S. Watson" date="unknown" rank="species">coulteri</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">coulteri</taxon_name>
    <taxon_hierarchy>family nyctaginaceae;genus boerhavia;species coulteri;variety coulteri</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415021</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Boerhavia</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="species">rosei</taxon_name>
    <taxon_hierarchy>genus Boerhavia;species rosei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 2–8 dm.</text>
      <biological_entity id="o6114" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 10–50 × 6–32 mm.</text>
      <biological_entity id="o6115" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s1" to="50" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s1" to="32" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: pedicel 0.2–1.6 mm;</text>
      <biological_entity id="o6116" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o6117" name="pedicel" name_original="pedicel" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s2" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts at base of perianth usually 2;</text>
      <biological_entity id="o6118" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o6119" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity id="o6120" name="base" name_original="base" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6121" name="perianth" name_original="perianth" src="d0_s3" type="structure" />
      <relation from="o6119" id="r830" name="at" negation="false" src="d0_s3" to="o6120" />
      <relation from="o6120" id="r831" name="part_of" negation="false" src="d0_s3" to="o6121" />
    </statement>
    <statement id="d0_s4">
      <text>perianth 1–2 mm distal to constriction;</text>
      <biological_entity id="o6122" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o6123" name="perianth" name_original="perianth" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character constraint="to constriction" constraintid="o6124" is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o6124" name="constriction" name_original="constriction" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>stamens 2–3 (–4), slightly exserted.</text>
      <biological_entity id="o6125" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6126" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="4" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s5" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal clusters, axis 15–60 mm.</text>
      <biological_entity id="o6127" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o6128" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="60" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Fruits (4–) 7–20 (–22) per cluster, usually overlapping by 50–100% of their length, or 2–4 in group, separated by small gap from next group and with distal fruits overlapping, pale tan, narrowly obpyramidal, 2.5–3.2 (–3.6) × (0.9–) 1.1 (–1.4) mm (l/w: [2.2–] 2.5–3.1 [–3.3]), apex bluntly conic-truncate, round-truncate, or truncate;</text>
      <biological_entity id="o6129" name="fruit" name_original="fruits" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="7" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="22" />
        <character char_type="range_value" constraint="per cluster" from="7" name="quantity" src="d0_s7" to="20" />
        <character is_modifier="false" modifier="50-100%" name="length" src="d0_s7" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" constraint="in group" constraintid="o6130" from="2" name="quantity" src="d0_s7" to="4" />
        <character constraint="by gap" constraintid="o6131" is_modifier="false" name="arrangement" notes="" src="d0_s7" value="separated" value_original="separated" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="pale tan" value_original="pale tan" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="3.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s7" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_width" src="d0_s7" to="1.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="1.4" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="1.1" value_original="1.1" />
      </biological_entity>
      <biological_entity id="o6130" name="group" name_original="group" src="d0_s7" type="structure" />
      <biological_entity id="o6131" name="gap" name_original="gap" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="small" value_original="small" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6132" name="fruit" name_original="fruits" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o6133" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="bluntly" name="architecture_or_shape" src="d0_s7" value="conic-truncate" value_original="conic-truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="round-truncate" value_original="round-truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
      </biological_entity>
      <relation from="o6131" id="r832" name="from next group and with" negation="false" src="d0_s7" to="o6132" />
    </statement>
    <statement id="d0_s8">
      <text>ribs obtuse, each often with sharp ridge, slightly rugose near sulci;</text>
      <biological_entity id="o6134" name="rib" name_original="ribs" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character constraint="near sulci" constraintid="o6136" is_modifier="false" modifier="slightly" name="relief" notes="" src="d0_s8" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o6135" name="ridge" name_original="ridge" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="sharp" value_original="sharp" />
      </biological_entity>
      <biological_entity id="o6136" name="sulcus" name_original="sulci" src="d0_s8" type="structure" />
      <relation from="o6134" id="r833" modifier="often" name="with" negation="false" src="d0_s8" to="o6135" />
    </statement>
    <statement id="d0_s9">
      <text>sulci 0.1–0.2 times as wide as base of ribs, slightly rugose.</text>
      <biological_entity id="o6137" name="sulcus" name_original="sulci" src="d0_s9" type="structure">
        <character constraint="of ribs" constraintid="o6139" is_modifier="false" name="width" src="d0_s9" value="0.1-0.2 times as wide as base" />
        <character is_modifier="false" modifier="slightly" name="relief" notes="" src="d0_s9" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o6138" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o6139" name="rib" name_original="ribs" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or loamy soils in arid grasslands, among brush, roadsides in deserts and cultivated areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" constraint="in arid grasslands , among brush ," />
        <character name="habitat" value="loamy soils" constraint="in arid grasslands , among brush ," />
        <character name="habitat" value="arid grasslands" constraint="among brush" />
        <character name="habitat" value="brush" />
        <character name="habitat" value="roadsides" constraint="in deserts and cultivated areas" />
        <character name="habitat" value="deserts" />
        <character name="habitat" value="cultivated areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>[100-]500-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="500" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="1400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; N.Mex., Utah; Mexico (Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16a.</number>
  <discussion>Most plants of Boerhavia coulteri var. coulteri have fruit apices that are bluntly conic- or round-truncate. Along the Santa Cruz River particularly, some plants have abruptly truncate fruits; those plants represent the phase known as B. rosei. Early in the twentieth century, J. J. Thornber proposed the name “B. standleyi” for such plants but never published it. P. C. Standley (1911) described the ribs of B. rosei as narrow, adding in 1918 that the sulci were broad and open. As the type demonstrates, the relative breadth of the ribs and sulci is exactly the reverse, confusing regional botanists for several generations.</discussion>
  
</bio:treatment>