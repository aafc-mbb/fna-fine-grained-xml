<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="treatment_page">201</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">sclerocactus</taxon_name>
    <taxon_name authority="Hochstätter" date="1989" rank="species">wetlandicus</taxon_name>
    <place_of_publication>
      <publication_title>Succulenta (Netherlands)</publication_title>
      <place_in_publication>68: 123, figs. s.n. (upper row, p. 124). 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus sclerocactus;species wetlandicus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415287</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pediocactus</taxon_name>
    <taxon_name authority="(Hochstätter) Halda" date="unknown" rank="species">wetlandicus</taxon_name>
    <taxon_hierarchy>genus Pediocactus;species wetlandicus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually unbranched, green to bluish green, spheric, cylindric, or elongate-cylindric, 3–8.5 (–15) × 4–10 (–12) cm, glaucous;</text>
      <biological_entity id="o11582" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="bluish green" />
        <character is_modifier="false" name="shape" src="d0_s0" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s0" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s0" value="elongate-cylindric" value_original="elongate-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s0" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s0" value="elongate-cylindric" value_original="elongate-cylindric" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s0" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s0" to="12" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s0" to="10" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>ribs 12–14 (–15) tubercles evident on ribs.</text>
      <biological_entity id="o11583" name="rib" name_original="ribs" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="15" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s1" to="14" />
      </biological_entity>
      <biological_entity id="o11584" name="tubercle" name_original="tubercles" src="d0_s1" type="structure">
        <character constraint="on ribs" constraintid="o11585" is_modifier="false" name="prominence" src="d0_s1" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o11585" name="rib" name_original="ribs" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Spines slightly or not obscuring stems;</text>
      <biological_entity id="o11586" name="spine" name_original="spines" src="d0_s2" type="structure" />
      <biological_entity id="o11587" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <relation from="o11586" id="r1695" name="obscuring" negation="true" src="d0_s2" to="o11587" />
    </statement>
    <statement id="d0_s3">
      <text>radial spines 6–10 (–14) per areole, white to tan (rarely black), 6–20 mm;</text>
      <biological_entity id="o11588" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="radial" value_original="radial" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="14" />
        <character char_type="range_value" constraint="per areole" constraintid="o11589" from="6" name="quantity" src="d0_s3" to="10" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s3" to="tan" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11589" name="areole" name_original="areole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>central spines 3–4 (–5) per areole, sometimes not greatly different from radial spines;</text>
      <biological_entity constraint="central" id="o11590" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="5" />
        <character char_type="range_value" constraint="per areole" constraintid="o11591" from="3" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o11591" name="areole" name_original="areole" src="d0_s4" type="structure" />
      <biological_entity id="o11592" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="radial" value_original="radial" />
      </biological_entity>
      <relation from="o11590" id="r1696" modifier="sometimes not; not greatly; greatly" name="from" negation="false" src="d0_s4" to="o11592" />
    </statement>
    <statement id="d0_s5">
      <text>abaxial central spines 0–2 per areole, pale tan, brown, reddish-brown to black, straight and unhooked or curved (rarely hooked), 12–26 × 0.8–1 mm;</text>
      <biological_entity constraint="abaxial central" id="o11593" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o11594" from="0" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s5" value="pale tan" value_original="pale tan" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s5" to="black" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s5" value="unhooked" value_original="unhooked" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s5" to="26" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11594" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>lateral central spines 2–4 per areole, similar to abaxial;</text>
      <biological_entity constraint="lateral central" id="o11595" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o11596" from="2" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <biological_entity id="o11596" name="areole" name_original="areole" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial" id="o11597" name="spine" name_original="spines" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>adaxial central spine usually white (rarely light-brown), elliptic in cross-section, 15–29 × 0.5–1.8 mm.</text>
      <biological_entity constraint="adaxial central" id="o11598" name="spine" name_original="spine" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character constraint="in cross-section" constraintid="o11599" is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" notes="" src="d0_s7" to="29" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" notes="" src="d0_s7" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11599" name="cross-section" name_original="cross-section" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers fragrant, funnelform, 2–3.5 (–5) × (2–) 2.5–5 cm;</text>
      <biological_entity id="o11600" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="odor" src="d0_s8" value="fragrant" value_original="fragrant" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s8" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_width" src="d0_s8" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s8" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer tepals with brownish lavender midstripes and pink to violet margins, oblanceolate, 15 (–30) × 30–50 mm;</text>
      <biological_entity constraint="outer" id="o11601" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="pink" name="coloration" notes="" src="d0_s9" to="violet" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="30" to_unit="mm" />
        <character name="length" src="d0_s9" unit="mm" value="15" value_original="15" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s9" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11602" name="midstripe" name_original="midstripes" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="brownish lavender" value_original="brownish lavender" />
      </biological_entity>
      <biological_entity id="o11603" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <relation from="o11601" id="r1697" name="with" negation="false" src="d0_s9" to="o11602" />
    </statement>
    <statement id="d0_s10">
      <text>inner tepals pink or violet, oblanceolate to lanceolate, 17–25 (–30) × 3–6 mm;</text>
      <biological_entity constraint="inner" id="o11604" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="violet" value_original="violet" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s10" to="lanceolate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="30" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments green to white;</text>
      <biological_entity id="o11605" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s11" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers yellow.</text>
      <biological_entity id="o11606" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits not regularly dehiscent, ovoid, barrel-shaped, 9–25 (–30) × 7–12 mm, with a few membranous scales, mostly near apex.</text>
      <biological_entity id="o11607" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not regularly" name="dehiscence" src="d0_s13" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="barrel--shaped" value_original="barrel--shaped" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="30" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s13" to="25" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s13" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11608" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="few" value_original="few" />
        <character is_modifier="true" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o11609" name="apex" name_original="apex" src="d0_s13" type="structure" />
      <relation from="o11607" id="r1698" name="with" negation="false" src="d0_s13" to="o11608" />
      <relation from="o11607" id="r1699" modifier="mostly" name="near" negation="false" src="d0_s13" to="o11609" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds black, 1.5 × 2.5 mm;</text>
      <biological_entity id="o11610" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character name="length" src="d0_s14" unit="mm" value="1.5" value_original="1.5" />
        <character name="width" src="d0_s14" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>cells convex but flattened apically.</text>
      <biological_entity id="o11611" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="late Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravel-covered clay hills, desert grasslands, saltbush, rabbitbrush flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravel-covered clay hills" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="rabbitbrush flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Parriette hookless cactus</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Sclerocactus wetlandicus is morphologically very similar to S. glaucus and frequently has been treated within S. glaucus (L. D. Benson 1982; K. D. Heil and J. M. Porter 1994), a treatment disputed by F. Hochstätter (1997). Populations are geographically separated in Duchesne and Uintah counties, Utah. Phylogenetic analyses of chloroplast DNA (J. M. Porter et al. 2000) are ambiguous regarding the closest relative of S. wetlandicus; however, this species is related to S. brevispinus, S. glaucus, S. wrightiae, S. whipplei, and S. parviflorus.</discussion>
  
</bio:treatment>