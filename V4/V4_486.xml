<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">267</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">beta</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">vulgaris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">vulgaris</taxon_name>
    <taxon_hierarchy>family chenopodiaceae;genus beta;species vulgaris;subspecies vulgaris</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415377</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants biennial;</text>
      <biological_entity id="o3780" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots dark red, white, or yellow, moderately to strongly swollen, fleshy.</text>
      <biological_entity id="o3781" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="moderately to strongly" name="shape" src="d0_s1" value="swollen" value_original="swollen" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, distally branched, 60–120 (–200) cm.</text>
      <biological_entity id="o3782" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="200" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 1/2–1/3 blade length;</text>
      <biological_entity id="o3783" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3784" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s3" to="1/3" />
      </biological_entity>
      <biological_entity id="o3785" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade broadly lanceolate to cordate;</text>
      <biological_entity id="o3786" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3787" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s4" to="cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal leaf-blade 10–20 (–27) × 4.5–8 (–18) cm;</text>
      <biological_entity id="o3788" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o3789" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="27" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="width" src="d0_s5" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline leaves reduced proximally, blade rhombic to narrowly lanceolate.</text>
      <biological_entity id="o3790" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="cauline" id="o3791" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="proximally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o3792" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s6" to="narrowly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cymes 2–8-flowered.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 18.</text>
      <biological_entity id="o3793" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-8-flowered" value_original="2-8-flowered" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3794" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sporadic in waste areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste areas" modifier="sporadic in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Conn., Maine, Mass., N.H., N.Y., N.C., Pa., R.I., S.C., Vt., Va., W.Va.; s Europe; ruderal in South America and e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="ruderal in South America and e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <other_name type="common_name">Spinach chard</other_name>
  <other_name type="common_name">Swiss cha rd</other_name>
  <other_name type="common_name">mangel-wurzel</other_name>
  <other_name type="common_name">sugar beet</other_name>
  <discussion>Subspecies vulgaris is valued as a source of sugar, a domestic vegetable, and food for livestock. Beets will grow in alkaline soils and tolerate arid conditions, making them an especially valuable crop in marginal lands. Aristotle referred to the use of red greens from beets ca. 350 b.c.</discussion>
  
</bio:treatment>