<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">147</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">opuntia</taxon_name>
    <taxon_name authority="Haworth" date="1819" rank="species">polyacantha</taxon_name>
    <taxon_name authority="(L. D. Benson) B. D. Parfitt" date="1998" rank="variety">nicholii</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>70: 188. 1998</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus opuntia;species polyacantha;variety nicholii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415226</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="L. D. Benson" date="unknown" rank="species">nicholii</taxon_name>
    <place_of_publication>
      <publication_title>Cacti Arizona ed.</publication_title>
      <place_in_publication>2, 48. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Opuntia;species nicholii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Engelmann &amp; J. M. Bigelow" date="unknown" rank="species">hystricina</taxon_name>
    <taxon_name authority="(L. D. Benson) Backe berg" date="unknown" rank="variety">nicholii</taxon_name>
    <taxon_hierarchy>genus Opuntia;species hystricina;variety nicholii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stem segments narrowly to broadly obovate, 10–27 × 7–18 cm;</text>
      <biological_entity constraint="stem" id="o11714" name="segment" name_original="segments" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s0" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s0" to="27" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" src="d0_s0" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>areoles 8–11 per diagonal row across midstem segment, 12–30 mm apart.</text>
      <biological_entity id="o11715" name="areole" name_original="areoles" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="per diagonal row" constraintid="o11716" from="8" name="quantity" src="d0_s1" to="11" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" notes="" src="d0_s1" to="30" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o11716" name="row" name_original="row" src="d0_s1" type="structure" />
      <biological_entity constraint="midstem" id="o11717" name="segment" name_original="segment" src="d0_s1" type="structure" />
      <relation from="o11716" id="r1712" name="across" negation="false" src="d0_s1" to="o11717" />
    </statement>
    <statement id="d0_s2">
      <text>Spines in most areoles, of 2 kinds: major spines 1–5 per areole, down curved, ascending near stem segment apex, pink-gray or basal 2/3 brown to black, the longest 45–150 mm;</text>
      <biological_entity id="o11718" name="spine" name_original="spines" src="d0_s2" type="structure" />
      <biological_entity id="o11719" name="areole" name_original="areoles" src="d0_s2" type="structure" />
      <biological_entity id="o11720" name="kind" name_original="kinds" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o11721" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
        <character char_type="range_value" constraint="per areole" constraintid="o11722" from="1" name="quantity" src="d0_s2" to="5" />
        <character is_modifier="false" modifier="down" name="course" notes="" src="d0_s2" value="curved" value_original="curved" />
        <character constraint="near stem segment apex" constraintid="o11723" is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s2" value="pink-gray" value_original="pink-gray" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character name="quantity" src="d0_s2" value="2/3" value_original="2/3" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s2" to="black" />
        <character is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
        <character char_type="range_value" from="45" from_unit="mm" name="some_measurement" src="d0_s2" to="150" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11722" name="areole" name_original="areole" src="d0_s2" type="structure" />
      <biological_entity constraint="segment" id="o11723" name="apex" name_original="apex" src="d0_s2" type="structure" constraint_original="stem segment" />
      <relation from="o11718" id="r1713" name="in" negation="false" src="d0_s2" to="o11719" />
      <relation from="o11718" id="r1714" name="consist_of" negation="false" src="d0_s2" to="o11720" />
    </statement>
    <statement id="d0_s3">
      <text>minor spines 3–8 per areole, deflexed, white to gray, 60–220 cm (spine clusters easily detached in preparing specimens).</text>
      <biological_entity id="o11724" name="spine" name_original="spines" src="d0_s3" type="structure" />
      <biological_entity id="o11725" name="areole" name_original="areoles" src="d0_s3" type="structure" />
      <biological_entity id="o11726" name="kind" name_original="kinds" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="minor" id="o11727" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o11728" from="3" name="quantity" src="d0_s3" to="8" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="deflexed" value_original="deflexed" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s3" to="gray" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s3" to="220" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11728" name="areole" name_original="areole" src="d0_s3" type="structure" />
      <relation from="o11724" id="r1715" name="in" negation="false" src="d0_s3" to="o11725" />
      <relation from="o11724" id="r1716" name="consist_of" negation="false" src="d0_s3" to="o11726" />
    </statement>
    <statement id="d0_s4">
      <text>Fruits stout;</text>
      <biological_entity id="o11729" name="fruit" name_original="fruits" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>areoles 18–24, most bearing 3–12 spines, 8–17 mm. 2n = 66.</text>
      <biological_entity id="o11730" name="areole" name_original="areoles" src="d0_s5" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s5" to="24" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11731" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="12" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11732" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="66" value_original="66" />
      </biological_entity>
      <relation from="o11730" id="r1717" name="bearing" negation="false" src="d0_s5" to="o11731" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Barren areas with saltbush and ephedra, limestone or red, sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="barren areas" constraint="with saltbush and ephedra" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="ephedra" />
        <character name="habitat" value="sandy soils" modifier="red" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34c.</number>
  <other_name type="common_name">Navajo Bridge pricklypear</other_name>
  
</bio:treatment>