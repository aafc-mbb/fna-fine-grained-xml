<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">190</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Link &amp; Otto" date="1827" rank="genus">echinocactus</taxon_name>
    <taxon_name authority="Engelmann &amp; J. M. Bigelow" date="1856" rank="species">polycephalus</taxon_name>
    <taxon_name authority="J. M. Coulter" date="1896" rank="variety">xeranthemoides</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>3: 358. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinocactus;species polycephalus;variety xeranthemoides;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415272</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocactus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">polycephalus</taxon_name>
    <taxon_name authority="(J. M. Coulter) N. P. Taylor" date="unknown" rank="subspecies">xeranthemoides</taxon_name>
    <taxon_hierarchy>genus Echinocactus;species polycephalus;subspecies xeranthemoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems gray-green to yellow-green.</text>
      <biological_entity id="o7261" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="gray-green" name="coloration" src="d0_s0" to="yellow-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spines red to straw colored, canescent, puberulent, or glabrous.</text>
      <biological_entity id="o7262" name="spine" name_original="spines" src="d0_s1" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s1" to="straw colored" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Fruit scales tan, yellow, or reddish, aging yellow, 16–30 mm, usually longer than dried tepals at fruit apex.</text>
      <biological_entity constraint="fruit" id="o7263" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s2" to="30" to_unit="mm" />
        <character constraint="than dried tepals" constraintid="o7264" is_modifier="false" name="length_or_size" src="d0_s2" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o7264" name="tepal" name_original="tepals" src="d0_s2" type="structure">
        <character is_modifier="true" name="condition" src="d0_s2" value="dried" value_original="dried" />
      </biological_entity>
      <biological_entity constraint="fruit" id="o7265" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <relation from="o7264" id="r1001" name="at" negation="false" src="d0_s2" to="o7265" />
    </statement>
    <statement id="d0_s3">
      <text>Seeds rounded (rarely faceted), 2.4–3.1 mm, smooth (exposed surfaces of testa cells flat or slightly convex, uniformly shiny).</text>
    </statement>
    <statement id="d0_s4">
      <text>2n = 22.</text>
      <biological_entity id="o7266" name="seed" name_original="seeds" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s3" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7267" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hills, slopes, and ledges of canyons, Great Basin and Mojave desert scrub, igneous and calcareous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hills" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="ledges" constraint="of canyons , great basin and mojave desert scrub" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="mojave desert scrub" />
        <character name="habitat" value="igneous" />
        <character name="habitat" value="calcareous substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b.</number>
  
</bio:treatment>