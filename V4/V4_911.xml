<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">459</other_info_on_meta>
    <other_info_on_meta type="mention_page">461</other_info_on_meta>
    <other_info_on_meta type="treatment_page">462</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Spach" date="1836" rank="genus">cistanthe</taxon_name>
    <taxon_name authority="(S. Watson) Carolin ex Hershkovitz" date="1990" rank="species">ambigua</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>68: 269. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus cistanthe;species ambigua</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415714</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Claytonia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">ambigua</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 365. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Claytonia;species ambigua;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calandrinia</taxon_name>
    <taxon_name authority="(S. Watson) Howell" date="unknown" rank="species">ambigua</taxon_name>
    <taxon_hierarchy>genus Calandrinia;species ambigua;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, roots fleshy.</text>
      <biological_entity id="o9178" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9179" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to erect, 3–18 cm.</text>
      <biological_entity id="o9180" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, not forming basal rosette;</text>
      <biological_entity id="o9181" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9182" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <relation from="o9181" id="r1305" name="forming" negation="true" src="d0_s2" to="o9182" />
    </statement>
    <statement id="d0_s3">
      <text>blade linear to spatulate, ± terete, 1.5–6 cm, not glaucous.</text>
      <biological_entity id="o9183" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="spatulate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences paniculate with umbellike clusters, dense, usually not exceeding leaves.</text>
      <biological_entity id="o9184" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character constraint="with umbellike clusters" is_modifier="false" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o9185" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <relation from="o9184" id="r1306" modifier="usually not" name="exceeding" negation="false" src="d0_s4" to="o9185" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers pedicellate;</text>
      <biological_entity id="o9186" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals ovate, slightly unequal, 2–5 mm, herbaceous, margins white-scarious;</text>
      <biological_entity id="o9187" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s6" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o9188" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="white-scarious" value_original="white-scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals disarticulate in fruit, 3–5, white, 2–5 mm;</text>
      <biological_entity id="o9189" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9190" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <relation from="o9189" id="r1307" name="disarticulate in" negation="false" src="d0_s7" to="o9190" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 5–10, anther yellow;</text>
      <biological_entity id="o9191" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
      <biological_entity id="o9192" name="anther" name_original="anther" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style present;</text>
      <biological_entity id="o9193" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 3;</text>
      <biological_entity id="o9194" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicel 1–3 mm.</text>
      <biological_entity id="o9195" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ovoid, 1–4 mm;</text>
      <biological_entity id="o9196" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves 3.</text>
      <biological_entity id="o9197" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 6–15, black, ovoid, 1–2 mm, smooth, shiny, reticulate at 30×, not white-hairy.</text>
      <biological_entity id="o9198" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s14" to="15" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
        <character constraint="at 30×" is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s14" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s14" value="white-hairy" value_original="white-hairy" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Nov–Feb.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Feb" from="Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy washes and slopes, desert shrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy washes" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="desert shrub communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>