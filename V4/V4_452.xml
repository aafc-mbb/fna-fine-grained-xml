<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="treatment_page">247</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">ferocactus</taxon_name>
    <taxon_name authority="(Muehlenpfordt) Britton &amp; Rose" date="1922" rank="species">hamatacanthus</taxon_name>
    <taxon_name authority="(A. Dietrich) L. D. Benson" date="1969" rank="variety">sinuatus</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>41: 128. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus ferocactus;species hamatacanthus;variety sinuatus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415350</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocactus</taxon_name>
    <taxon_name authority="A. Dietrich" date="unknown" rank="species">sinuatus</taxon_name>
    <place_of_publication>
      <publication_title>Allg. Gartenzeitung</publication_title>
      <place_in_publication>19: 345. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Echinocactus;species sinuatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems spheric to ovoid, 10–30 × 7.5–20 cm;</text>
      <biological_entity id="o6168" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="spheric" name="shape" src="d0_s0" to="ovoid" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="7.5" from_unit="cm" name="width" src="d0_s0" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>ribs typically 13, undulate to deeply crenate.</text>
      <biological_entity id="o6169" name="rib" name_original="ribs" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="13" value_original="13" />
        <character char_type="range_value" from="undulate" name="shape" src="d0_s1" to="deeply crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spines 12–16 per areole;</text>
      <biological_entity id="o6170" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o6171" from="12" name="quantity" src="d0_s2" to="16" />
      </biological_entity>
      <biological_entity id="o6171" name="areole" name_original="areole" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>central spines 4 per areole, principal central spine strongly flattened, 50–90 × 1.3–3 mm, sometimes ± papery.</text>
      <biological_entity constraint="central" id="o6172" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character constraint="per areole" constraintid="o6173" name="quantity" src="d0_s3" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o6173" name="areole" name_original="areole" src="d0_s3" type="structure" />
      <biological_entity constraint="principal central" id="o6174" name="spine" name_original="spine" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s3" to="90" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes more or less" name="texture" src="d0_s3" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 6–7.5 × 7–9 cm;</text>
      <biological_entity id="o6175" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" src="d0_s4" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stigma lobes 8–10.</text>
      <biological_entity constraint="stigma" id="o6176" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruits green, yellow-green, or olive, 20–25 × 10–15 mm.</text>
      <biological_entity id="o6177" name="fruit" name_original="fruits" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="olive" value_original="olive" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="olive" value_original="olive" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds ca. 1 mm.</text>
      <biological_entity id="o6178" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tamaulipan thorn scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tamaulipan thorn scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b.</number>
  <discussion>Contrary to L. D. Benson’s (1982) map of Ferocactus hamatacanthus, all populations in southern Texas are var. sinuatus. The southern Texas populations form a geographically cohesive taxon superficially similar to Hamatocactus bicolor in their narrow ribs and overall green aspect unlike the coarse, strongly xerophytic F. hamatacanthus var. hamatacanthus farther west. Only the populations in the lower Pecos River region might be intermediates between vars. sinuatus and hamatacanthus.</discussion>
  
</bio:treatment>