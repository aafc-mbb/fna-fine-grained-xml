<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Allan D. Zimmerman,Bruce D. Parfitt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="(K. Schumann) Britton &amp; Rose" date="1922" rank="genus">THELOCACTUS</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>49: 251. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus thelocactus;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek thele, nipple, and Cactus, an old genus name, in reference to the tubercle shape</other_info_on_name>
    <other_info_on_name type="fna_id">132768</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Link &amp; Otto" date="unknown" rank="genus">Echinocactus</taxon_name>
    <taxon_name authority="K. Sch umann" date="unknown" rank="subgenus">Thelocactus</taxon_name>
    <place_of_publication>
      <publication_title>Gesamtbeschr. Kakt.,</publication_title>
      <place_in_publication>429. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Echinocactus;subgenus Thelocactus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, unbranched or branched and forming mounds, deep-seated in substrate or not.</text>
      <biological_entity id="o22146" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character constraint="in substrate; in substrate" is_modifier="false" modifier="not" name="location" src="d0_s0" value="deep-seated" value_original="deep-seated" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22147" name="mound" name_original="mounds" src="d0_s0" type="structure" />
      <relation from="o22146" id="r3213" name="forming" negation="false" src="d0_s0" to="o22147" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse or short taproots.</text>
      <biological_entity id="o22148" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o22149" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="true" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unsegmented, greenish [to pale blue-gray], spheric or flat-topped to short cylindric, 3–38 × 4–20 cm, glabrous;</text>
      <biological_entity id="o22150" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="38" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s2" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ribs usually 7–13 [–25], absent in young plants [and some Mexican taxa], crests very deeply notched above each areole, thus ribs strongly tuberculate, 15–25 mm diam.;</text>
      <biological_entity id="o22151" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="25" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s3" to="13" />
        <character constraint="in plants" constraintid="o22152" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o22152" name="plant" name_original="plants" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o22153" name="crest" name_original="crests" src="d0_s3" type="structure">
        <character constraint="above areole" constraintid="o22154" is_modifier="false" modifier="very deeply" name="shape" src="d0_s3" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity id="o22154" name="areole" name_original="areole" src="d0_s3" type="structure" />
      <biological_entity id="o22155" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="relief" src="d0_s3" value="tuberculate" value_original="tuberculate" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s3" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>tubercles prominent, broadly rounded, conic, pyramidal, bilaterally compressed [or long-decurrent];</text>
      <biological_entity id="o22156" name="tubercle" name_original="tubercles" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" modifier="bilaterally" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>areoles 6–20 [–52] mm apart along ribs, groove connecting areole to spine cluster absent on youngest adults and conspicuous on older plants;</text>
      <biological_entity id="o22157" name="areole" name_original="areoles" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="52" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
        <character constraint="along ribs" constraintid="o22158" is_modifier="false" name="arrangement" src="d0_s5" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity id="o22158" name="rib" name_original="ribs" src="d0_s5" type="structure" />
      <biological_entity id="o22159" name="groove" name_original="groove" src="d0_s5" type="structure">
        <character constraint="on youngest adults" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character constraint="on plants" constraintid="o22162" is_modifier="false" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o22160" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity id="o22161" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o22162" name="plant" name_original="plants" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="older" value_original="older" />
      </biological_entity>
      <relation from="o22159" id="r3214" name="connecting" negation="false" src="d0_s5" to="o22160" />
      <relation from="o22159" id="r3215" name="to" negation="false" src="d0_s5" to="o22161" />
    </statement>
    <statement id="d0_s6">
      <text>areolar glands often conspicuous [or absent];</text>
      <biological_entity constraint="areolar" id="o22163" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cortex and pith firm, not mucilaginous.</text>
      <biological_entity id="o22164" name="cortex" name_original="cortex" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s7" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o22165" name="pith" name_original="pith" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s7" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spines [1–] (8–) 12–30 per areole, white, yellow, or red [black], largest spines 0.2–1.5 mm wide;</text>
      <biological_entity id="o22166" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s8" to="#12" to_inclusive="false" />
        <character char_type="range_value" constraint="per areole" constraintid="o22167" from="#12" name="quantity" src="d0_s8" to="30" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o22167" name="areole" name_original="areole" src="d0_s8" type="structure" />
      <biological_entity constraint="largest" id="o22168" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>radial spines [0–] (8–) 12–20 (–25) per areole, contrasting with other spines, straight [to curved, rarely almost hooked], 9–35 (–45) mm, adaxial radial spines flat and bladelike or ribbonlike;</text>
      <biological_entity id="o22169" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="radial" value_original="radial" />
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s9" to="#12" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="25" />
        <character char_type="range_value" constraint="per areole" constraintid="o22170" from="#12" name="quantity" src="d0_s9" to="20" />
        <character is_modifier="false" name="course" notes="" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="45" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22170" name="areole" name_original="areole" src="d0_s9" type="structure" />
      <biological_entity id="o22171" name="spine" name_original="spines" src="d0_s9" type="structure" />
      <biological_entity constraint="adaxial" id="o22172" name="spine" name_original="spines" src="d0_s9" type="structure" />
      <biological_entity id="o22173" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="radial" value_original="radial" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s9" value="bladelike" value_original="bladelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ribbonlike" value_original="ribbonlike" />
      </biological_entity>
      <relation from="o22171" id="r3216" name="contrasting with" negation="false" src="d0_s9" to="o22171" />
    </statement>
    <statement id="d0_s10">
      <text>central spines 0–4 (–5) per areole, usually straight, sometimes somewhat curved, terete, flattened, or angled.</text>
      <biological_entity constraint="central" id="o22174" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="5" />
        <character char_type="range_value" constraint="per areole" constraintid="o22175" from="0" name="quantity" src="d0_s10" to="4" />
        <character is_modifier="false" modifier="usually" name="course" notes="" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes somewhat" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s10" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s10" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s10" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s10" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s10" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o22175" name="areole" name_original="areole" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers diurnal, from adaxial or axillary extremity of areole, near stem apex, shallowly to deeply funnelform, [2.5–] 4–8 (–10) × [2–] 4–6.5 (–10) cm;</text>
      <biological_entity id="o22176" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="diurnal" value_original="diurnal" />
      </biological_entity>
      <biological_entity id="o22177" name="extremity" name_original="extremity" src="d0_s11" type="structure" />
      <biological_entity id="o22178" name="areole" name_original="areole" src="d0_s11" type="structure" />
      <biological_entity constraint="stem" id="o22179" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" modifier="shallowly to deeply; deeply" name="atypical_length" notes="alterIDs:o22179" src="d0_s11" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" modifier="shallowly to deeply; deeply" name="atypical_length" notes="alterIDs:o22179" src="d0_s11" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" modifier="shallowly to deeply; deeply" name="length" notes="alterIDs:o22179" src="d0_s11" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" modifier="shallowly to deeply; deeply" name="atypical_width" notes="alterIDs:o22179" src="d0_s11" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" modifier="shallowly to deeply; deeply" name="atypical_width" notes="alterIDs:o22179" src="d0_s11" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" modifier="shallowly to deeply; deeply" name="width" notes="alterIDs:o22179" src="d0_s11" to="6.5" to_unit="cm" />
      </biological_entity>
      <relation from="o22176" id="r3217" name="from" negation="false" src="d0_s11" to="o22177" />
      <relation from="o22177" id="r3218" name="part_of" negation="false" src="d0_s11" to="o22178" />
      <relation from="o22176" id="r3219" name="near" negation="false" src="d0_s11" to="o22179" />
    </statement>
    <statement id="d0_s12">
      <text>outer tepals minutely fringed;</text>
      <biological_entity constraint="outer" id="o22180" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s12" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner tepals widely spreading, magenta or pink [white, yellow, or patterned with contrasting veins or transverse white band proximally and/or broad white margins and/or orange-red to crimson proximal portions], 28–50 × 5–12 mm, margins entire or fringed to denticulate;</text>
      <biological_entity constraint="inner" id="o22181" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character char_type="range_value" from="28" from_unit="mm" name="length" src="d0_s13" to="50" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s13" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22182" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character char_type="range_value" from="fringed" name="shape" src="d0_s13" to="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary scaly, hairless, spineless;</text>
      <biological_entity id="o22183" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s14" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hairless" value_original="hairless" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="spineless" value_original="spineless" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>scales 5–21, broad, margins scarious, minutely fringed;</text>
      <biological_entity id="o22184" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s15" to="21" />
        <character is_modifier="false" name="width" src="d0_s15" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o22185" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s15" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma lobes 7–13, reddish to orange or yellowish [or whitish], 4.5 mm.</text>
      <biological_entity constraint="stigma" id="o22186" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s16" to="13" />
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s16" to="orange or yellowish" />
        <character name="some_measurement" src="d0_s16" unit="mm" value="4.5" value_original="4.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits dehiscent through large basal pore, green to brownish purple [to magenta], spheric to short cylindric, (5–) 7–18 × 6–12 (–17) mm, not juicy, drying immediately after ripening, scaly, spineless, hairless;</text>
      <biological_entity id="o22187" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character constraint="through large basal pore" constraintid="o22188" is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s17" to="brownish purple" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s17" to="short cylindric" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s17" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s17" to="18" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s17" to="17" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s17" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s17" value="juicy" value_original="juicy" />
        <character constraint="after ripening" is_modifier="false" name="condition" src="d0_s17" value="drying" value_original="drying" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s17" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="spineless" value_original="spineless" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="hairless" value_original="hairless" />
      </biological_entity>
      <biological_entity constraint="basal" id="o22188" name="pore" name_original="pore" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="through" name="size" src="d0_s17" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>floral remnant persistent.</text>
      <biological_entity constraint="floral" id="o22189" name="remnant" name_original="remnant" src="d0_s18" type="structure">
        <character is_modifier="false" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds black or very dark-brown, short cylindric to pyriform, sometimes constricted above base, 1.4–2.5 × 1–1.75 mm;</text>
      <biological_entity id="o22190" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="black" value_original="black" />
        <character is_modifier="false" modifier="very" name="coloration" src="d0_s19" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="short" value_original="short" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s19" to="pyriform" />
        <character constraint="above base" constraintid="o22191" is_modifier="false" modifier="sometimes" name="size" src="d0_s19" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" notes="" src="d0_s19" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s19" to="1.75" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22191" name="base" name_original="base" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>testa cells convex [or flat, sometimes with shiny, raised, reticulate sculpture of slightly protruding, narrow, straight anticlinal walls].</text>
    </statement>
    <statement id="d0_s21">
      <text>x = 11.</text>
      <biological_entity constraint="testa" id="o22192" name="cell" name_original="cells" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="x" id="o22193" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Arid regions, sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Arid regions" establishment_means="native" />
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <discussion>Species 11 (1 in the flora).</discussion>
  <discussion>Thelocactus is an assortment of morphologically primitive species groups, united primarily by fruit morphology (scaly, dehiscent through gaping poricidal attachment scars), which is shared with Echinomastus intertextus. Seed shape, however, is cylindric or pyriform, unlike the spheric or hemispheric seeds of Echinomastus. Chloroplast DNA evidence (C. A. Butterworth et al. 2002) places certain examined species of Thelocactus close to Ferocactus along with Glandulicactus and the Mexican genus Leuchtenbergia. Unfortunately, because Thelocactus is probably paraphyletic or polyphyletic (species originating from different ancestors), relationships of each species must be examined. Although Echinomastus was not included in that study, DNA evidence obtained by J. M. Porter et al. (2000) confirmed that genus is more closely related to Sclerocactus than to Thelocactus.</discussion>
  <references>
    <reference>Anderson, E. F. 1987. A revision of the genus Thelocactus B. &amp; R. (Cactaceae). Bradleya 5: 49–76.</reference>
    <reference>Glass, C. and R. A. Foster. 1977. The genus Thelocactus in the Chihuahuan Desert. Cact. Succ. J. (Los Angeles) 49: 213–220, 244–251.</reference>
  </references>
  
</bio:treatment>