<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">309</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">311</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Roth" date="1801" rank="genus">kochia</taxon_name>
    <taxon_name authority="G. Beck in H. G. L. Reichenbach et al." date="1908" rank="section">Semibassia</taxon_name>
    <taxon_name authority="(Linnaeus) Schrader" date="1809" rank="species">scoparia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">scoparia</taxon_name>
    <taxon_hierarchy>family chenopodiaceae;genus kochia;section semibassia;species scoparia;subspecies scoparia;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415474</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green or yellowish green, often becoming reddish at maturity, 25–150 (–200) cm, ± glabrous or slightly pubescent.</text>
      <biological_entity id="o20253" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character constraint="at maturity , 25-150(-200) cm" is_modifier="false" modifier="often becoming" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched.</text>
      <biological_entity id="o20254" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile (proximal sometimes with pseudopetiole);</text>
      <biological_entity id="o20255" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade (1–) 3–5-veined, narrowly obovate, lanceolate, or almost linear, flat, 5–75 × 1–15 mm, not fleshy, margins long-ciliate, glabrous or with short appressed hairs adaxially.</text>
      <biological_entity id="o20256" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="(1-)3-5-veined" value_original="(1-)3-5-veined" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="almost" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="75" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o20257" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="long-ciliate" value_original="long-ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="with short appressed hairs" value_original="with short appressed hairs" />
      </biological_entity>
      <biological_entity id="o20258" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o20257" id="r2954" name="with" negation="false" src="d0_s3" to="o20258" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences generally sparsely spicate at maturity;</text>
    </statement>
    <statement id="d0_s5">
      <text>with 1–2 (–5) -flowers in axils of bracts.</text>
      <biological_entity id="o20259" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="generally sparsely" name="architecture" src="d0_s4" value="spicate" value_original="spicate" />
      </biological_entity>
      <biological_entity id="o20260" name="1-2(-5)-flower" name_original="1-2(-5)-flowers" src="d0_s5" type="structure" />
      <biological_entity id="o20261" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity id="o20262" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o20259" id="r2955" name="with" negation="false" src="d0_s5" to="o20260" />
      <relation from="o20260" id="r2956" name="in" negation="false" src="d0_s5" to="o20261" />
      <relation from="o20261" id="r2957" name="part_of" negation="false" src="d0_s5" to="o20262" />
    </statement>
    <statement id="d0_s6">
      <text>Perianth segments glabrous or nearly so;</text>
      <biological_entity constraint="perianth" id="o20263" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s6" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bisexual flowers transversely winged or keeled (rarely tubercled) at maturity, wing membranous, to 1–1.3 mm;</text>
      <biological_entity id="o20264" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="transversely" name="architecture" src="d0_s7" value="winged" value_original="winged" />
        <character constraint="at maturity" is_modifier="false" name="shape" src="d0_s7" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o20265" name="wing" name_original="wing" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistillate flowers generally wingless.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 18.</text>
      <biological_entity id="o20266" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="generally" name="architecture" src="d0_s8" value="wingless" value_original="wingless" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20267" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, cultivated fields, roadsides, other disturbed habitats, seminatural plant communities in arid regions, some forms occasionally cultivated</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="cultivated fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="other disturbed habitats" />
        <character name="habitat" value="seminatural plant communities" constraint="in arid regions , some forms occasionally cultivated" />
        <character name="habitat" value="arid regions" />
        <character name="habitat" value="some forms" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., Ont., Que., Sask.; Ala., Ariz., Calif., Colo., Conn., Del., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., Wis., Wyo.; nearly worldwide, native to arid steppe and desert regions of Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="nearly worldwide" establishment_means="native" />
        <character name="distribution" value="native to arid steppe and desert regions of Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <other_name type="common_name">Mexican fireweed</other_name>
  <discussion>Kochia scoparia is a common weed, especially in the Southwest and the Great Plains region. It is less common in the eastern (especially southeastern) United States, although its distribution there is in need of clarification. It can be expected in Arkansas, District of Columbia, Florida, Georgia, and West Virginia although no reports are known from those states.</discussion>
  <discussion>Kochia scoparia is extremely variable. It is represented in Eurasia by several geographical races usually recognized as subspecies (subsp. scoparia, native to eastern Europe and southwestern Asia; subsp. densiflora (Turczaninov ex Moquin-Tandon) Aellen, native to central and eastern Asia; subsp. indica (Wight) Aellen, native to southern Asia); and subsp. hirsutissima Sukhorukov recently described from Central Asia) or varieties. Kochia scoparia var. subvillosa Moquin-Tandon (= K. scoparia subsp. densiflora; K. sieversiana auct. pro parte, non (Pallas) C. A. Meyer) sometimes occurs in North America as a casual alien. In its typical form var. subvillosa Moquin-Tandon may be distinguished from var. scoparia in having flowers surrounded by a tuft of long hairs exceeding the perianth segments, the axis of the inflorescence distinctly pubescent, and the proximal branches arcuate, almost ascending. Kochia indica Wight is probably a distinct species, which may be expected as a casual or locally naturalized alien in the southern part of the United States.</discussion>
  <discussion>A narrow-leaved form [Kochia scoparia forma trichophylla (A. Voss) Stapf ex Schinz &amp; Thellung] known as “summer-cypress” is widely cultivated as an ornamental plant and sometimes escapes.</discussion>
  <discussion>An Asian species, Kochia iranica Bornmueller has been erroneously reported for Colorado and Missouri by W. A. Weber (1966, 1972) and V. Muhlenbach (1979) respectively (the pertinent herbarium specimens belong to subsp. scoparia).</discussion>
  <discussion>Kochia alata Bates, described as a presumably native North American species, is not specifically distinct from K. scoparia sensu lato, especially from its eastern Asian forms characterized by the somewhat greater wings of the fruiting perianth.</discussion>
  <discussion>Another annual species, Kochia laniflora (S. G. Gmelin) Borbás, is widely distributed in Eurasia and may be expected anywhere in temperate North America as a casual or established alien. This species belongs to sect. Kochia and can be readily distinguished from K. scoparia sensu lato in having narrowly linear or filiform leaves and more prominently winged perianth segments.</discussion>
  
</bio:treatment>