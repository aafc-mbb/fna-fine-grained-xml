<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">34</other_info_on_meta>
    <other_info_on_meta type="treatment_page">36</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">acleisanthes</taxon_name>
    <taxon_name authority="(A. Gray) Bentham &amp; Hooker f. ex Hemsley" date="1882" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Biol. Cent.-Amer., Bot.</publication_title>
      <place_in_publication>3: 6. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus acleisanthes;species wrightii</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415035</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pentacrophys</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts, ser.</publication_title>
      <place_in_publication>2, 15: 261. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pentacrophys;species wrightii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants herbaceous, overall pubescence mixture of capitate-glandular hairs 0.2–0.3 mm, and white, capitate, shorter hairs 0.1–0.2 mm.</text>
      <biological_entity id="o4990" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s0" value="herbaceous" value_original="herbaceous" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="pubescence" src="d0_s0" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="capitate" value_original="capitate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="capitate-glandular" id="o4991" name="hair" name_original="hairs" src="d0_s0" type="structure" />
      <biological_entity constraint="shorter" id="o4992" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s0" to="0.2" to_unit="mm" />
      </biological_entity>
      <relation from="o4990" id="r676" name="part_of" negation="false" src="d0_s0" to="o4991" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending to decumbent, much-branched, 10–40 cm, puberulent to glabrate.</text>
      <biological_entity id="o4993" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s1" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves yellowish green, petiolate, those of pair subequal;</text>
      <biological_entity id="o4994" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character constraint="of pair" is_modifier="false" name="size" src="d0_s2" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–15 mm, puberulent;</text>
      <biological_entity id="o4995" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oval to ovatelanceolate or suborbiculate, 5–35 × 3–20 mm, base slightly unequal and obtuse to rounded or acute, margins crispate, apex apiculate and obtuse or infrequently acute, hirtellous to puberulent.</text>
      <biological_entity id="o4996" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oval" name="shape" src="d0_s4" to="ovatelanceolate or suborbiculate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4997" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s4" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded or acute" />
      </biological_entity>
      <biological_entity id="o4998" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crispate" value_original="crispate" />
      </biological_entity>
      <biological_entity id="o4999" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="infrequently" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s4" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers, sessile or with pedicel to 1 mm;</text>
      <biological_entity id="o5000" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="with pedicel" value_original="with pedicel" />
      </biological_entity>
      <biological_entity id="o5001" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o5002" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o5000" id="r677" name="with" negation="false" src="d0_s5" to="o5002" />
    </statement>
    <statement id="d0_s6">
      <text>bracts linear-subulate, 2–8 mm, hirtellous to puberulent.</text>
      <biological_entity id="o5003" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s6" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: chasmogamous perianth 2.5–5 cm, puberulent to hirtellous, tube 1–1.5 mm diam., limbs 10–15 mm diam., stamens 5;</text>
      <biological_entity id="o5004" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5005" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="chasmogamous" value_original="chasmogamous" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s7" to="hirtellous" />
      </biological_entity>
      <biological_entity id="o5006" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5007" name="limb" name_original="limbs" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5008" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cleistogamous perianth 3–8 mm, hirtellous, stamens 2 (–5).</text>
      <biological_entity id="o5009" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5010" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="cleistogamous" value_original="cleistogamous" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o5011" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits prominently 5-ribbed and deeply 5-sulcate, ribs broad, smooth, each terminating in hemispheric, glandular tubercle, tubercule oblong, truncate at both ends, 5–9 mm, puberulent.</text>
      <biological_entity id="o5012" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_shape" src="d0_s9" value="5-ribbed" value_original="5-ribbed" />
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s9" value="5-sulcate" value_original="5-sulcate" />
      </biological_entity>
      <biological_entity id="o5013" name="rib" name_original="ribs" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o5014" name="tubercle" name_original="tubercle" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o5015" name="tubercule" name_original="tubercule" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character constraint="at ends" constraintid="o5016" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o5016" name="end" name_original="ends" src="d0_s9" type="structure" />
      <relation from="o5013" id="r678" name="terminating in" negation="false" src="d0_s9" to="o5014" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Various soils on calcareous substrates in grasslands or shrublands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="various soils" constraint="on calcareous substrates in grasslands or shrublands" />
        <character name="habitat" value="calcareous substrates" constraint="in grasslands or shrublands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="shrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Wright’s trumpets</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>