<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Allan D. Zimmerman,Bruce D. Parfitt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">HAMATOCACTUS</taxon_name>
    <place_of_publication>
      <publication_title>Cact.</publication_title>
      <place_in_publication>3: 104, figs. 110–114. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus hamatocactus;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin hamatus, hooked, in reference to the hooked central spines, and Cactus, an old genus name</other_info_on_name>
    <other_info_on_name type="fna_id">114543</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, unbranched or branched in basal portion, not deep-seated in substrate.</text>
      <biological_entity id="o20122" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character constraint="in basal portion" constraintid="o20123" is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character constraint="in substrate" constraintid="o20124" is_modifier="false" modifier="not" name="location" notes="" src="d0_s0" value="deep-seated" value_original="deep-seated" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20123" name="portion" name_original="portion" src="d0_s0" type="structure" />
      <biological_entity id="o20124" name="substrate" name_original="substrate" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse.</text>
      <biological_entity id="o20125" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unsegmented, bright deep green, hemispheric when young, becoming spheric or ovoid to cylindric, 3.6–12 (–20) × 4.5–12 cm, glabrous;</text>
      <biological_entity id="o20126" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="reflectance" src="d0_s2" value="bright" value_original="bright" />
        <character is_modifier="false" name="depth" src="d0_s2" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="when young" name="shape" src="d0_s2" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="ovoid" modifier="becoming" name="shape" src="d0_s2" to="cylindric" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="3.6" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="width" src="d0_s2" to="12" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ribs 13, spiraling or vertical, slender, crests sinuate, sharp, not interrupted or undulate, narrow;</text>
      <biological_entity id="o20127" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="13" value_original="13" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="spiraling" value_original="spiraling" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o20128" name="crest" name_original="crests" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="sharp" value_original="sharp" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>areoles circular or, on older parts of stem, elliptic to ovate, adaxially elongated into short areolar grooves;</text>
      <biological_entity id="o20129" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="circular" value_original="circular" />
        <character name="arrangement_or_shape" src="d0_s4" value="," value_original="," />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s4" to="ovate" />
        <character constraint="into areolar, grooves" constraintid="o20132, o20133" is_modifier="false" modifier="adaxially" name="length" src="d0_s4" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o20130" name="part" name_original="parts" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="older" value_original="older" />
      </biological_entity>
      <biological_entity id="o20131" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity id="o20132" name="areolar" name_original="areolar" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o20133" name="groove" name_original="grooves" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <relation from="o20129" id="r2929" name="on" negation="false" src="d0_s4" to="o20130" />
      <relation from="o20130" id="r2930" name="part_of" negation="false" src="d0_s4" to="o20131" />
    </statement>
    <statement id="d0_s5">
      <text>areolar glands golden, darker with age, cylindric or peglike;</text>
      <biological_entity constraint="areolar" id="o20134" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="golden" value_original="golden" />
        <character constraint="with age" constraintid="o20135" is_modifier="false" name="coloration" src="d0_s5" value="darker" value_original="darker" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s5" value="peglike" value_original="peglike" />
      </biological_entity>
      <biological_entity id="o20135" name="age" name_original="age" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>cortex and pith firm, not mucilaginous.</text>
      <biological_entity id="o20136" name="cortex" name_original="cortex" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o20137" name="pith" name_original="pith" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spines 11–20 per areole, not obscuring stems, yellowish, whitish, or reddish-brown, acicular (rarely central spine flattened), longest spines 12–38 mm;</text>
      <biological_entity id="o20138" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o20139" from="11" name="quantity" src="d0_s7" to="20" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acicular" value_original="acicular" />
      </biological_entity>
      <biological_entity id="o20139" name="areole" name_original="areole" src="d0_s7" type="structure" />
      <biological_entity id="o20140" name="stem" name_original="stems" src="d0_s7" type="structure" />
      <biological_entity constraint="longest" id="o20141" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="38" to_unit="mm" />
      </biological_entity>
      <relation from="o20138" id="r2931" name="obscuring" negation="true" src="d0_s7" to="o20140" />
    </statement>
    <statement id="d0_s8">
      <text>radial spines 10–19 per areole, straight or slightly curved toward stem, longest spines 11–32 mm;</text>
      <biological_entity id="o20142" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o20143" from="10" name="quantity" src="d0_s8" to="19" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character constraint="toward stem" constraintid="o20144" is_modifier="false" modifier="slightly" name="course" src="d0_s8" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o20143" name="areole" name_original="areole" src="d0_s8" type="structure" />
      <biological_entity id="o20144" name="stem" name_original="stem" src="d0_s8" type="structure" />
      <biological_entity constraint="longest" id="o20145" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s8" to="32" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>central spines 1 per areole, porrect, hooked, terete (rarely flattened).</text>
      <biological_entity constraint="central" id="o20146" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character constraint="per areole" constraintid="o20147" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="porrect" value_original="porrect" />
        <character is_modifier="false" name="shape" src="d0_s9" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o20147" name="areole" name_original="areole" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers diurnal, near stem apex, at adaxial edge of areoles or at axillary ends of short areolar grooves, widely funnelform, 3.7–7 × 4–7 cm;</text>
      <biological_entity id="o20148" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="diurnal" value_original="diurnal" />
        <character char_type="range_value" from="3.7" from_unit="cm" name="length" notes="" src="d0_s10" to="7" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" notes="" src="d0_s10" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o20149" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o20150" name="groove" name_original="grooves" src="d0_s10" type="structure" />
      <relation from="o20148" id="r2932" name="near" negation="false" src="d0_s10" to="o20149" />
      <relation from="o20148" id="r2933" modifier="widely; widely; widely; widely; widely; widely; widely; widely" name="at adaxial edge of areoles or at axillary ends of short areolar" negation="false" src="d0_s10" to="o20150" />
    </statement>
    <statement id="d0_s11">
      <text>outer tepals finely fringed;</text>
      <biological_entity constraint="outer" id="o20151" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s11" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>inner tepals yellow (to ivory) with red bases, 20–25 × 6–9 mm, margins entire, toothed, or lacerate;</text>
      <biological_entity constraint="inner" id="o20152" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character constraint="with bases" constraintid="o20153" is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" notes="" src="d0_s12" to="25" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" notes="" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20153" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o20154" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s12" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary scaly, hairless, spineless;</text>
      <biological_entity id="o20155" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s13" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairless" value_original="hairless" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="spineless" value_original="spineless" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma lobes 5–11, pale-yellow to orangish, 3–7 mm.</text>
      <biological_entity constraint="stigma" id="o20156" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s14" to="11" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s14" to="orangish" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits indehiscent or eventually dehiscent by vertical slits, bright red, spheric or nearly so, ca. 10 × 8–13 mm, fleshy, with 15 or fewer whitish, broad fringed, naked, spineless scales;</text>
      <biological_entity id="o20157" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="indehiscent" value_original="indehiscent" />
        <character constraint="by slits" constraintid="o20158" is_modifier="false" modifier="eventually" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="bright red" value_original="bright red" />
        <character is_modifier="false" name="shape" src="d0_s15" value="spheric" value_original="spheric" />
        <character name="shape" src="d0_s15" value="nearly" value_original="nearly" />
        <character name="length" src="d0_s15" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s15" to="13" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s15" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o20158" name="slit" name_original="slits" src="d0_s15" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s15" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o20159" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="15" value_original="15" />
        <character is_modifier="true" name="width" src="d0_s15" value="broad" value_original="broad" />
        <character is_modifier="true" name="shape" src="d0_s15" value="fringed" value_original="fringed" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="naked" value_original="naked" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="spineless" value_original="spineless" />
      </biological_entity>
      <relation from="o20157" id="r2934" name="with" negation="false" src="d0_s15" to="o20159" />
    </statement>
    <statement id="d0_s16">
      <text>floral remnant persistent.</text>
      <biological_entity constraint="floral" id="o20160" name="remnant" name_original="remnant" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds black, obovoid, usually 1–1.4 × 0.8–1 mm, minutely papillate;</text>
      <biological_entity id="o20161" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s17" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s17" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="usually; minutely" name="relief" src="d0_s17" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>testa cells weakly convex, nearly flat toward proximal end of seed.</text>
      <biological_entity constraint="seed" id="o20163" name="end" name_original="end" src="d0_s18" type="structure" constraint_original="seed proximal; seed" />
      <biological_entity id="o20164" name="seed" name_original="seed" src="d0_s18" type="structure" />
      <relation from="o20163" id="r2935" name="part_of" negation="false" src="d0_s18" to="o20164" />
    </statement>
    <statement id="d0_s19">
      <text>x = 11.</text>
      <biological_entity constraint="testa" id="o20162" name="cell" name_original="cells" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s18" value="convex" value_original="convex" />
        <character constraint="toward proximal end" constraintid="o20163" is_modifier="false" modifier="nearly" name="prominence_or_shape" src="d0_s18" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="x" id="o20165" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Arid regions, sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Arid regions" establishment_means="native" />
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <other_name type="common_name">Twisted-rib cactus</other_name>
  <discussion>Species 1.</discussion>
  <discussion>Hamatocactus has been submerged in Ferocactus or Thelocactus by various authors and grouped with Glandulicactus by others.</discussion>
  
</bio:treatment>