<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">212</other_info_on_meta>
    <other_info_on_meta type="mention_page">214</other_info_on_meta>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose in N. L. Britton and A. Brown" date="1913" rank="genus">pediocactus</taxon_name>
    <taxon_name authority="S. L. Welsh &amp; Goodrich" date="1980" rank="species">despainii</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>40: 83, fig. 5. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus pediocactus;species despainii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415312</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pediocactella</taxon_name>
    <taxon_name authority="(L. D. Benson) Doweld" date="unknown" rank="species">bradyi</taxon_name>
    <taxon_name authority="(S. L. Welsh &amp; Goodrich) Doweld" date="unknown" rank="variety">despainii</taxon_name>
    <taxon_hierarchy>genus Pediocactella;species bradyi;variety despainii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pediocactus</taxon_name>
    <taxon_name authority="L. D. Benson" date="unknown" rank="species">bradyi</taxon_name>
    <taxon_name authority="(S. L. Welsh &amp; Goodrich) Hochstätter" date="unknown" rank="subspecies">despainii</taxon_name>
    <taxon_hierarchy>genus Pediocactus;species bradyi;subspecies despainii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pediocactus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bradyi</taxon_name>
    <taxon_name authority="(S. L. Welsh &amp; Goodrich) Hochstätter" date="unknown" rank="variety">despainii</taxon_name>
    <taxon_hierarchy>genus Pediocactus;species bradyi;variety despainii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants typically unbranched.</text>
      <biological_entity id="o12764" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="typically" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems subglobose to obovoid, 3.8–6 × 3–9.5 cm;</text>
      <biological_entity id="o12765" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s1" to="obovoid" />
        <character char_type="range_value" from="3.8" from_unit="cm" name="length" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s1" to="9.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>areoles circular to oval, villous to glabrate.</text>
      <biological_entity id="o12766" name="areole" name_original="areoles" src="d0_s2" type="structure">
        <character char_type="range_value" from="circular" name="shape" src="d0_s2" to="oval" />
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s2" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spines smooth, relatively hard, all radial, 9–15 per areole, spreading, white, 2–6 mm.</text>
      <biological_entity id="o12767" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="relatively" name="texture" src="d0_s3" value="hard" value_original="hard" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o12768" from="9" name="quantity" src="d0_s3" to="15" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12768" name="areole" name_original="areole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers 1.5–2.5 × 1.8–2.5 cm;</text>
      <biological_entity id="o12769" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>scales and outer tepals minutely toothed to entire and undulate;</text>
      <biological_entity id="o12770" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character char_type="range_value" from="minutely toothed" name="shape" src="d0_s5" to="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o12771" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="minutely toothed" name="shape" src="d0_s5" to="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer tepals yellow-bronze to peach-bronze or pink with purple midstripes, 4–10 × 3–6 mm;</text>
      <biological_entity constraint="outer" id="o12772" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="with midstripes" constraintid="o12773" from="yellow-bronze" name="coloration" src="d0_s6" to="peach-bronze or pink" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12773" name="midstripe" name_original="midstripes" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner tepals yellow-bronze to peach-bronze (rarely pink), 6–12 × 4–6 mm.</text>
      <biological_entity constraint="inner" id="o12774" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="yellow-bronze" name="coloration" src="d0_s7" to="peach-bronze" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits green, drying reddish-brown, turbinate, 9–11 × 10–12 mm.</text>
      <biological_entity id="o12775" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="condition" src="d0_s8" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s8" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s8" to="11" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds black, 3.5 × 2.5 mm, shiny, papillate and rugose.</text>
      <biological_entity id="o12776" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character name="length" src="d0_s9" unit="mm" value="3.5" value_original="3.5" />
        <character name="width" src="d0_s9" unit="mm" value="2.5" value_original="2.5" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="relief" src="d0_s9" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert pavements of cobble or pebble in pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert pavements" constraint="of cobble or pebble" />
        <character name="habitat" value="cobble" />
        <character name="habitat" value="pebble" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">San Rafael cactus</other_name>
  <other_name type="common_name">Despain footcactus</other_name>
  <other_name type="common_name">Despain’s pincushion cactus</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Population-level studies of chloroplast DNA (J. M. Porter et al. unpubl.) confirm a close relationship between Pediocactus despainii and P. winkleri. Genetic relationships among populations correspond to geographic features of the region: populations with the San Rafael Swell represent one group of related populations, and populations south of the San Rafael Swell represent another group of related populations. This pattern conflicts in some ways with morphology. While all of the populations south of the San Rafael Swell are classified as P. winkleri, within the Swell some populations are P. despainii, several are P. winkleri, and a few are morphologically intermediate.</discussion>
  
</bio:treatment>