<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Nancy J. Vivrette</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">75</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="treatment_page">77</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">aizoaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TETRAGONIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 480. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 215. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aizoaceae;genus TETRAGONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek tetra, four, and gonia, angle, in reference to the shape of the fruit</other_info_on_name>
    <other_info_on_name type="fna_id">132571</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [subshrubs], annual or perennial, succulent, glabrous, pilose, or papillate.</text>
      <biological_entity id="o1135" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="relief" src="d0_s0" value="papillate" value_original="papillate" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fibrous.</text>
      <biological_entity id="o1136" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate [subscandent], semiwoody at base.</text>
      <biological_entity id="o1137" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character constraint="at base" constraintid="o1138" is_modifier="false" name="texture" src="d0_s2" value="semiwoody" value_original="semiwoody" />
      </biological_entity>
      <biological_entity id="o1138" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, alternate, usually petioled;</text>
      <biological_entity id="o1139" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="petioled" value_original="petioled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules absent;</text>
      <biological_entity id="o1140" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade flat, margins entire to slightly sinuate or shallowly lobed.</text>
      <biological_entity id="o1141" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o1142" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s5" to="slightly sinuate or shallowly lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: axillary clusters of 2–3 flowers or flowers solitary, sessile or peduncled;</text>
      <biological_entity id="o1143" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character constraint="of flowers, flowers" constraintid="o1145" is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="peduncled" value_original="peduncled" />
      </biological_entity>
      <biological_entity id="o1145" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts absent.</text>
      <biological_entity id="o1146" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o1147" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual or unisexual, inconspicuous, 5 [–10] mm diam.;</text>
      <biological_entity id="o1148" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s8" to="10" to_unit="mm" />
        <character name="diameter" src="d0_s8" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>calyx campanulate, adnate to ovary, angled, winged, or horned;</text>
      <biological_entity id="o1149" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character constraint="to ovary" constraintid="o1150" is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="angled" value_original="angled" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s9" value="horned" value_original="horned" />
      </biological_entity>
      <biological_entity id="o1150" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>calyx lobes [3–] 4–5 [–7], green or yellow adaxially;</text>
      <biological_entity constraint="calyx" id="o1151" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s10" to="4" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="7" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals and petaloid staminodia absent;</text>
      <biological_entity id="o1152" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1153" name="staminodium" name_original="staminodia" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary absent;</text>
      <biological_entity id="o1154" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 1–20, perigynous;</text>
      <biological_entity id="o1155" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="20" />
        <character is_modifier="false" name="position" src="d0_s13" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistil 3–10-carpellate;</text>
      <biological_entity id="o1156" name="pistil" name_original="pistil" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-10-carpellate" value_original="3-10-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary inferior [half-inferior], [1–] 3–10-loculed;</text>
    </statement>
    <statement id="d0_s16">
      <text>placentation apical;</text>
      <biological_entity id="o1157" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="[1-]3-10-loculed" value_original="[1-]3-10-loculed" />
        <character is_modifier="false" name="placentation" src="d0_s16" value="apical" value_original="apical" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovule 1 per locule;</text>
      <biological_entity id="o1158" name="ovule" name_original="ovule" src="d0_s17" type="structure">
        <character constraint="per locule" constraintid="o1159" name="quantity" src="d0_s17" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1159" name="locule" name_original="locule" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>styles 3–10;</text>
      <biological_entity id="o1160" name="style" name_original="styles" src="d0_s18" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s18" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 3–10.</text>
      <biological_entity id="o1161" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s19" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits nutlike [drupaceous], angled, indehiscent;</text>
      <biological_entity id="o1162" name="fruit" name_original="fruits" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="nutlike" value_original="nutlike" />
        <character is_modifier="false" name="shape" src="d0_s20" value="angled" value_original="angled" />
        <character is_modifier="false" name="dehiscence" src="d0_s20" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>horns [2–] 4–6 [–7].</text>
      <biological_entity id="o1163" name="horn" name_original="horns" src="d0_s21" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s21" to="4" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s21" to="7" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s21" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds 1–10, light-brown, ± reniform, arils absent.</text>
      <biological_entity id="o1164" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s22" to="10" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s22" value="reniform" value_original="reniform" />
      </biological_entity>
      <biological_entity id="o1165" name="aril" name_original="arils" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; South America, e Asia, Africa, Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="e Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">New Zealand spinach</other_name>
  <discussion>Species 60 (1 in the flora).</discussion>
  
</bio:treatment>