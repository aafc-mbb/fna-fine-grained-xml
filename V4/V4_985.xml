<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">montia</taxon_name>
    <taxon_name authority="(A. E. Porsild) S. L. Welsh" date="1968" rank="species">bostockii</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>28: 154. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus montia;species bostockii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415783</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Claytonia</taxon_name>
    <taxon_name authority="A. E. Porsild" date="unknown" rank="species">bostockii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Natl. Mus. Canada</publication_title>
      <place_in_publication>121: 160. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Claytonia;species bostockii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Montiastrum</taxon_name>
    <taxon_name authority="(A. E. Porsild) Ö. Nilsson" date="unknown" rank="species">bostockii</taxon_name>
    <taxon_hierarchy>genus Montiastrum;species bostockii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, rhizomatous or stoloniferous, not bulbiferous, rooting at nodes.</text>
      <biological_entity id="o3795" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bulbiferous" value_original="bulbiferous" />
        <character constraint="at nodes" constraintid="o3796" is_modifier="false" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3796" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 5–15 cm.</text>
      <biological_entity id="o3797" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, secund, petiolate;</text>
      <biological_entity id="o3798" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="secund" value_original="secund" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear, 2–40 × 0.5–2 mm.</text>
      <biological_entity id="o3799" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1-bracteate;</text>
      <biological_entity id="o3800" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-bracteate" value_original="1-bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bract linear to oblanceolate, 10 × 2 mm.</text>
      <biological_entity id="o3801" name="bract" name_original="bract" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="oblanceolate" />
        <character name="length" src="d0_s5" unit="mm" value="10" value_original="10" />
        <character name="width" src="d0_s5" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1–12 (–20);</text>
      <biological_entity id="o3802" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="20" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals 3.5–4.5 mm;</text>
      <biological_entity id="o3803" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 5, white with yellow blotches at base, or pinkish, 10–15 mm;</text>
      <biological_entity id="o3804" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character constraint="at base" constraintid="o3805" is_modifier="false" name="coloration" src="d0_s8" value="yellow blotches" value_original="yellow blotches" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3805" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 5, anther yellow.</text>
      <biological_entity id="o3806" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o3807" name="anther" name_original="anther" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 0.8–1.5 mm, tuberculate;</text>
      <biological_entity id="o3808" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s10" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>elaiosome present.</text>
      <biological_entity id="o3809" name="elaiosome" name_original="elaiosome" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, often north-facing slopes of scree or alpine tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" constraint="of scree or alpine tundra" />
        <character name="habitat" value="north-facing slopes" modifier="often" constraint="of scree or alpine tundra" />
        <character name="habitat" value="scree" />
        <character name="habitat" value="alpine tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Bostock’s montia</other_name>
  <discussion>Considered an ancestral species of Montia and Claytonia by some workers, M. bostockii appears related to M. vassilievii (Kuzeneva) McNeill of Asia and M. linearis of North America. The pollen is distinctly tholate with spiniferous saccae. The flowers of both M. bostockii and M. vassilievii closely resemble claytonias but have only three ovules, as opposed to arctic claytonias, which have six.</discussion>
  <references>
    <reference>McNeill, J. and J. N. Findlay. 1971. The systematic position of Claytonia bostockii. Canad. J. Bot. 49: 713–715.</reference>
  </references>
  
</bio:treatment>