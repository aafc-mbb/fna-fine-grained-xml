<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="treatment_page">342</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">atriplex</taxon_name>
    <taxon_name authority="(Standley) Ulbrich in H. G. A. Engler et al." date="1934" rank="section">Semibaccata</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler et al., Nat. Pflanzenfam. ed.</publication_title>
      <place_in_publication>2, 16c: 515. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus atriplex;section Semibaccata</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415515</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atriplex</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="unranked">Semibaccatae</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>21: 52. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Atriplex;unranked Semibaccatae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or short-lived perennial, erect to procumbent or diffuse.</text>
      <biological_entity id="o22995" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="density" src="d0_s0" value="diffuse" value_original="diffuse" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves with Kranz-type anatomy.</text>
      <biological_entity id="o22996" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Pistillate flowers lacking perianth, enclosed by a pair of bracteoles.</text>
      <biological_entity id="o22997" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o22998" name="perianth" name_original="perianth" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o22999" name="pair" name_original="pair" src="d0_s2" type="structure" />
      <biological_entity id="o23000" name="bracteole" name_original="bracteoles" src="d0_s2" type="structure" />
      <relation from="o22997" id="r3344" name="enclosed by a" negation="false" src="d0_s2" to="o22999" />
      <relation from="o22997" id="r3345" name="part_of" negation="false" src="d0_s2" to="o23000" />
    </statement>
    <statement id="d0_s3">
      <text>Bracteoles distinct or united, thin or variously thickened, but not spongy, appendages tuberculate, foliaceous, inflated, spinose, or lacking, not spongy.</text>
      <biological_entity id="o23001" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="united" value_original="united" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="variously" name="width" src="d0_s3" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o23002" name="appendage" name_original="appendages" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="lacking" value_original="lacking" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="spongy" value_original="spongy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Seeds ascending or erect;</text>
      <biological_entity id="o23003" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>radicle lateral.</text>
      <biological_entity id="o23004" name="radicle" name_original="radicle" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced in United States and elsewhere, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="in United States and elsewhere" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>20a.5.</number>
  <discussion>Species ca. 36 species (2 in the flora).</discussion>
  
</bio:treatment>