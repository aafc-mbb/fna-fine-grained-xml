<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="treatment_page">194</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">echinomastus</taxon_name>
    <taxon_name authority="(Parry ex Engelmann) E. M. Baxter" date="1935" rank="species">johnsonii</taxon_name>
    <place_of_publication>
      <publication_title>Calif. Cact.,</publication_title>
      <place_in_publication>75. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinomastus;species johnsonii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415274</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocactus</taxon_name>
    <taxon_name authority="Parry ex Engelmann" date="unknown" rank="species">johnsonii</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>117. 1871 (as johnsoni)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Echinocactus;species johnsonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinomastus</taxon_name>
    <taxon_name authority="(Parry ex Engelmann) L. D. Benson" date="unknown" rank="species">johnsonii</taxon_name>
    <taxon_name authority="(Parish) Wiggins" date="unknown" rank="variety">lutescens</taxon_name>
    <taxon_hierarchy>genus Echinomastus;species johnsonii;variety lutescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Neolloydia</taxon_name>
    <taxon_name authority="(Parry ex Engelmann) N. P. Taylor" date="unknown" rank="species">johnsonii</taxon_name>
    <taxon_hierarchy>genus Neolloydia;species johnsonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sclerocactus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">johnsonii</taxon_name>
    <taxon_hierarchy>genus Sclerocactus;species johnsonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 10–25 × 7–15 cm;</text>
      <biological_entity id="o20508" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" src="d0_s0" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>ribs (13–) 18–21;</text>
      <biological_entity id="o20509" name="rib" name_original="ribs" src="d0_s1" type="structure">
        <character char_type="range_value" from="13" name="atypical_quantity" src="d0_s1" to="18" to_inclusive="false" />
        <character char_type="range_value" from="18" name="quantity" src="d0_s1" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>areoles (14–) 21–26 mm apart along ribs;</text>
      <biological_entity id="o20510" name="areole" name_original="areoles" src="d0_s2" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="21" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="21" from_unit="mm" name="some_measurement" src="d0_s2" to="26" to_unit="mm" />
        <character constraint="along ribs" constraintid="o20511" is_modifier="false" name="arrangement" src="d0_s2" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity id="o20511" name="rib" name_original="ribs" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>areolar glands present at least seasonally.</text>
      <biological_entity constraint="areolar" id="o20512" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="at-least seasonally; seasonally" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines 13–24 per areole, pale-yellow to grayish lavender to maroon;</text>
      <biological_entity id="o20513" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o20514" from="13" name="quantity" src="d0_s4" to="24" />
        <character char_type="range_value" from="pale-yellow" name="coloration" notes="" src="d0_s4" to="grayish lavender" />
      </biological_entity>
      <biological_entity id="o20514" name="areole" name_original="areole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>radial spines 9–16 per areole;</text>
      <biological_entity id="o20515" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o20516" from="9" name="quantity" src="d0_s5" to="16" />
      </biological_entity>
      <biological_entity id="o20516" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>abaxial (shortest) radial spine 6–19 × (0.2–) 0.4–0.6 mm;</text>
      <biological_entity constraint="abaxial" id="o20517" name="spine" name_original="spines" src="d0_s6" type="structure" />
      <biological_entity id="o20518" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="radial" value_original="radial" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="19" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_width" src="d0_s6" to="0.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s6" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>adaxial and lateral (longest) radial spines ca. 27–40 mm;</text>
      <biological_entity constraint="adaxial lateral" id="o20520" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="radial" value_original="radial" />
        <character char_type="range_value" from="27" from_unit="mm" name="some_measurement" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>central spines 4–9 per areole, present at all ages, 27–41 × (0.7–) 1.1–1.4 mm;</text>
      <biological_entity constraint="central" id="o20521" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o20522" from="4" name="quantity" src="d0_s8" to="9" />
      </biological_entity>
      <biological_entity id="o20522" name="areole" name_original="areole" src="d0_s8" type="structure">
        <character constraint="at ages" constraintid="o20523" is_modifier="false" name="presence" notes="" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o20523" name="age" name_original="ages" src="d0_s8" type="structure">
        <character char_type="range_value" from="27" from_unit="mm" name="length" notes="" src="d0_s8" to="41" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_width" notes="" src="d0_s8" to="1.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" notes="" src="d0_s8" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>longest central spine straight to strongly decurved;</text>
      <biological_entity constraint="longest central" id="o20524" name="spine" name_original="spine" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s9" value="decurved" value_original="decurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>abaxial central spine porrect, ca. 27–40 mm Flowers 4–6.5 × 4–7.7 cm;</text>
      <biological_entity constraint="abaxial central" id="o20525" name="spine" name_original="spine" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="porrect" value_original="porrect" />
        <character char_type="range_value" from="27" from_unit="mm" name="some_measurement" src="d0_s10" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20526" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s10" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s10" to="7.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>inner tepals yellow or pink to magenta, basal portions blotched with maroon, 2.5–3.7 × 1–1.8 cm;</text>
      <biological_entity constraint="inner" id="o20527" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="magenta" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20528" name="portion" name_original="portions" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="blotched with maroon" value_original="blotched with maroon" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s11" to="3.7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s11" to="1.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigma lobes yellowish white to green.</text>
      <biological_entity constraint="stigma" id="o20529" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellowish white" name="coloration" src="d0_s12" to="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits dehiscent only along single, longitudinal split, ± spheric, 17–18 mm. 2n = 22.</text>
      <biological_entity id="o20530" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character constraint="along split" constraintid="o20531" is_modifier="false" name="dehiscence" src="d0_s13" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s13" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s13" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20531" name="split" name_original="split" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="single" value_original="single" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s13" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20532" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb-)Mar–May; fruiting Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="May" from="Feb" />
        <character name="fruiting time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mojave desert scrub and upper edge of Sonoran desert scrub, rocky slopes, gravelly hills</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mojave desert scrub" />
        <character name="habitat" value="upper edge" constraint="of sonoran" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="gravelly hills" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Echinomastus johnsonii varies geographically in both flower and spine color. The yellow-flowered plants have been named E. johnsonii var. lutescens. The pink-flowered plants occur in separate populations, as far as is known, to the north of the yellow-flowered plants, but the interface between the two forms is poorly understood.</discussion>
  
</bio:treatment>