<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">158</other_info_on_meta>
    <other_info_on_meta type="mention_page">159</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Engelmann in F. A.Wislizenus" date="1848" rank="genus">echinocereus</taxon_name>
    <taxon_name authority="Lemaire" date="1868" rank="species">poselgeri</taxon_name>
    <place_of_publication>
      <publication_title>Cactées,</publication_title>
      <place_in_publication>57. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinocereus;species poselgeri;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415264</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocereus</taxon_name>
    <taxon_name authority="(Poselger) Rümpler" date="unknown" rank="species">tuberosus</taxon_name>
    <taxon_hierarchy>genus Echinocereus;species tuberosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Wilcoxia</taxon_name>
    <taxon_name authority="(Lemaire) Britton &amp; Rose" date="unknown" rank="species">poselgeri</taxon_name>
    <taxon_hierarchy>genus Wilcoxia;species poselgeri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants straggling, very tall and slender, sparingly branched at any level.</text>
      <biological_entity id="o6740" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="straggling" value_original="straggling" />
        <character is_modifier="false" modifier="very" name="height" src="d0_s0" value="tall" value_original="tall" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character constraint="at level" constraintid="o6741" is_modifier="false" modifier="sparingly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6741" name="level" name_original="level" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems initially erect, later sprawling or clambering, long cylindric, 12–60 (–130) × 0.6–1 (–2) cm;</text>
      <biological_entity id="o6742" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="initially" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="later" name="orientation" src="d0_s1" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="clambering" value_original="clambering" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="130" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s1" to="60" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ribs 8–10, crests low, uninterrupted or shallowly undulate;</text>
      <biological_entity id="o6743" name="rib" name_original="ribs" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s2" to="10" />
      </biological_entity>
      <biological_entity id="o6744" name="crest" name_original="crests" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="low" value_original="low" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="uninterrupted" value_original="uninterrupted" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles 1–2–5 mm apart.</text>
      <biological_entity id="o6745" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2-5" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines (9–) 11–13 (–17) per areole, stiff and straight, usually tan, brown, or black, sometimes yellow, pale-pink, ashy white, or gray, sometimes black central spines contrasting with white radial spines;</text>
      <biological_entity id="o6746" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" name="atypical_quantity" src="d0_s4" to="11" to_inclusive="false" />
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="17" />
        <character char_type="range_value" constraint="per areole" constraintid="o6747" from="11" name="quantity" src="d0_s4" to="13" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s4" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s4" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="black" value_original="black" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="ashy white" value_original="ashy white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="ashy white" value_original="ashy white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray" value_original="gray" />
      </biological_entity>
      <biological_entity id="o6747" name="areole" name_original="areole" src="d0_s4" type="structure" />
      <biological_entity constraint="central" id="o6748" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes" name="coloration" src="d0_s4" value="black" value_original="black" />
      </biological_entity>
      <biological_entity id="o6749" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="radial" value_original="radial" />
      </biological_entity>
      <relation from="o6748" id="r914" name="contrasting with" negation="false" src="d0_s4" to="o6749" />
    </statement>
    <statement id="d0_s5">
      <text>radial spines 8–16 per areole, 2–5 mm;</text>
      <biological_entity id="o6750" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o6751" from="8" name="quantity" src="d0_s5" to="16" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6751" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>central spines 1 (–3) per areole, closely appressed (except at the stem tip), terete, 4–9 mm.</text>
      <biological_entity constraint="central" id="o6752" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="3" />
        <character constraint="per areole" constraintid="o6753" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" modifier="closely" name="fixation_or_orientation" notes="" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6753" name="areole" name_original="areole" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3.5–6 × 3.5–7 cm;</text>
      <biological_entity id="o6754" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s7" to="6" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s7" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>flower tube 15–20 × 7–18 mm;</text>
      <biological_entity constraint="flower" id="o6755" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>flower tube hairs 3–5 (–10) mm;</text>
      <biological_entity constraint="tube" id="o6756" name="hair" name_original="hairs" src="d0_s9" type="structure" constraint_original="flower tube">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner tepals rose-pink with darker-pink to magenta midstripes, proximally darker, 25–35 × 4–12 mm, tips relatively thin and delicate;</text>
      <biological_entity constraint="inner" id="o6757" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character constraint="with midstripes" constraintid="o6758" is_modifier="false" name="coloration" src="d0_s10" value="rose-pink" value_original="rose-pink" />
        <character is_modifier="false" modifier="proximally" name="coloration" notes="" src="d0_s10" value="darker" value_original="darker" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s10" to="35" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6758" name="midstripe" name_original="midstripes" src="d0_s10" type="structure">
        <character char_type="range_value" from="darker-pink" is_modifier="true" name="coloration" src="d0_s10" to="magenta" />
      </biological_entity>
      <biological_entity id="o6759" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="delicate" value_original="delicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o6760" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectar chamber 1–4 mm.</text>
      <biological_entity constraint="nectar" id="o6761" name="chamber" name_original="chamber" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits dark green to brownish, 2–3 cm, pulp white.</text>
      <biological_entity id="o6762" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s13" to="brownish" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s13" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 22.</text>
      <biological_entity id="o6763" name="pulp" name_original="pulp" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6764" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr; fruiting 2 1/2-3 months after flowering.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tamaulipan thorn scrub, alluvial soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tamaulipan thorn scrub" />
        <character name="habitat" value="alluvial soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200[-1100] m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="1100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Pencil cactus</other_name>
  <other_name type="common_name">dahlia hedgehog cactus</other_name>
  <other_name type="common_name">sacasil</other_name>
  <discussion>Echinocereus poselgeri, with tuberous roots and erect, slender, elongate stems, superficially resembles some Peniocereus species. The polyphyletic genus Wilcoxia Britton &amp; Rose formerly included this species along with species of Peniocereus. The flowers, fruits, and seeds of E. poselgeri are typical for Echinocereus, quite similar to those of E. reichenbachii.</discussion>
  
</bio:treatment>