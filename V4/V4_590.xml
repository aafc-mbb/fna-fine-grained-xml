<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Noel H. Holmgren</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="mention_page">389</other_info_on_meta>
    <other_info_on_meta type="treatment_page">307</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Gueldenstaedt" date="1772" rank="genus">KRASCHENINNIKOVIA</taxon_name>
    <place_of_publication>
      <publication_title>Novi Comme nt. Acad. Sci. Imp. Petrop.</publication_title>
      <place_in_publication>16: 551. 1772</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus KRASCHENINNIKOVIA</taxon_hierarchy>
    <other_info_on_name type="etymology">for S. P. Krasheninnikova, 1711–1755, academician and professor in Saint Petersburg, author of the first flora of Saint Petersburg</other_info_on_name>
    <other_info_on_name type="fna_id">117271</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, monoecious or dioecious, herbage densely tomentose, hairs stellate.</text>
      <biological_entity id="o23580" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity id="o23581" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o23582" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, not jointed or armed;</text>
      <biological_entity id="o23583" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="jointed" value_original="jointed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="armed" value_original="armed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal branches woody, flowering branches herbaceous.</text>
      <biological_entity constraint="basal" id="o23584" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o23585" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s2" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate, petiolate;</text>
      <biological_entity id="o23586" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear to narrowly lanceolate, not fleshy, base truncate, margins entire, revolute, apex blunt.</text>
      <biological_entity id="o23587" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o23588" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o23589" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o23590" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary clusters or small spikes.</text>
      <biological_entity id="o23591" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="axillary" id="o23592" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character is_modifier="true" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers unisexual;</text>
      <biological_entity id="o23593" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>staminate flowers with bractlets absent, perianth 4-parted, stamens 4;</text>
      <biological_entity id="o23594" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23595" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23596" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="4-parted" value_original="4-parted" />
      </biological_entity>
      <biological_entity id="o23597" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <relation from="o23594" id="r3419" name="with" negation="false" src="d0_s7" to="o23595" />
    </statement>
    <statement id="d0_s8">
      <text>pistillate flowers enclosed in 2 partially connate, slightly keeled, densely hirsute bractlets with free tips hornlike, perianth absent, stigmas 2, elongate.</text>
      <biological_entity id="o23598" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23599" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" modifier="partially" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="true" modifier="slightly" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character is_modifier="true" modifier="densely" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o23600" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s8" value="free" value_original="free" />
        <character is_modifier="false" name="shape" src="d0_s8" value="hornlike" value_original="hornlike" />
      </biological_entity>
      <biological_entity id="o23601" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o23598" id="r3420" name="enclosed in" negation="false" src="d0_s8" to="o23599" />
      <relation from="o23598" id="r3421" name="with" negation="false" src="d0_s8" to="o23600" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting structures ovate, flat utricles;</text>
      <biological_entity id="o23602" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o23603" name="structure" name_original="structures" src="d0_s9" type="structure" />
      <biological_entity id="o23604" name="utricle" name_original="utricles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
      </biological_entity>
      <relation from="o23602" id="r3422" name="fruiting" negation="false" src="d0_s9" to="o23603" />
    </statement>
    <statement id="d0_s10">
      <text>pericarp free, thin.</text>
      <biological_entity id="o23605" name="pericarp" name_original="pericarp" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds vertical, ovate;</text>
      <biological_entity id="o23606" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>seed-coat brown, covered with white hairs;</text>
      <biological_entity id="o23607" name="seed-coat" name_original="seed-coat" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o23608" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <relation from="o23607" id="r3423" name="covered with" negation="false" src="d0_s12" to="o23608" />
    </statement>
    <statement id="d0_s13">
      <text>embryo annular, perisperm copious.</text>
      <biological_entity id="o23609" name="embryo" name_original="embryo" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="annular" value_original="annular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>x = 9.</text>
      <biological_entity id="o23610" name="perisperm" name_original="perisperm" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="copious" value_original="copious" />
      </biological_entity>
      <biological_entity constraint="x" id="o23611" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Winterfat</other_name>
  <discussion>Species 3 (1 in the flora).</discussion>
  
</bio:treatment>