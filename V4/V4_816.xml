<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">412</other_info_on_meta>
    <other_info_on_meta type="mention_page">413</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="treatment_page">418</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">amaranthus</taxon_name>
    <taxon_name authority="(Linnaeus) Aellen ex K. R. Robertson" date="1981" rank="subgenus">Acnida</taxon_name>
    <taxon_name authority="Mosyakin &amp; K. R. Robertson" date="1996" rank="section">Saueranthus</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">palmeri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>12: 274. 1877</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus amaranthus;subgenus acnida;section saueranthus;species palmeri;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415653</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glabrous or nearly so.</text>
      <biological_entity id="o3143" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s0" value="nearly" value_original="nearly" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched, usually (0.3–) 0.5–1.5 (–3) m;</text>
      <biological_entity id="o3144" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.3" from_unit="m" modifier="usually" name="atypical_some_measurement" src="d0_s1" to="0.5" to_inclusive="false" to_unit="m" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="m" modifier="usually" name="atypical_some_measurement" src="d0_s1" to="3" to_unit="m" />
        <character char_type="range_value" from="0.5" from_unit="m" modifier="usually" name="some_measurement" src="d0_s1" to="1.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal branches often ascending.</text>
      <biological_entity constraint="proximal" id="o3145" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: long-petiolate;</text>
      <biological_entity id="o3146" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="long-petiolate" value_original="long-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade obovate or rhombic-obovate to elliptic proximally, sometimes lanceolate distally, 1.5–7 × 1–3.5 cm, base broadly to narrowly cuneate, margins entire, plane, apex subobtuse to acute, usually with terminal mucro.</text>
      <biological_entity id="o3147" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3148" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="rhombic-obovate" modifier="proximally" name="shape" src="d0_s4" to="elliptic" />
        <character is_modifier="false" modifier="sometimes; distally" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3149" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o3150" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o3151" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="subobtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o3152" name="mucro" name_original="mucro" src="d0_s4" type="structure" />
      <relation from="o3151" id="r401" modifier="usually" name="with" negation="false" src="d0_s4" to="o3152" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, linear spikes to panicles, usually drooping, occasionally erect, especially when young, with few axillary clusters, uninterrupted or interrupted in proximal part of plant.</text>
      <biological_entity id="o3153" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o3154" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="usually" name="orientation" notes="" src="d0_s5" value="drooping" value_original="drooping" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o3155" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o3156" name="part" name_original="part" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" value_original="few" />
        <character is_modifier="true" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="uninterrupted" value_original="uninterrupted" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
      </biological_entity>
      <biological_entity id="o3157" name="plant" name_original="plant" src="d0_s5" type="structure" />
      <relation from="o3154" id="r402" name="to" negation="false" src="d0_s5" to="o3155" />
      <relation from="o3154" id="r403" modifier="especially; when young" name="with" negation="false" src="d0_s5" to="o3156" />
      <relation from="o3156" id="r404" name="part_of" negation="false" src="d0_s5" to="o3157" />
    </statement>
    <statement id="d0_s6">
      <text>Bracts: of pistillate flowers with long-excurrent midrib, 4–6 mm, longer than tepals, apex acuminate or mucronulate;</text>
      <biological_entity id="o3158" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="6" to_unit="mm" />
        <character constraint="than tepals" constraintid="o3161" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o3159" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o3160" name="midrib" name_original="midrib" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="long-excurrent" value_original="long-excurrent" />
      </biological_entity>
      <biological_entity id="o3161" name="tepal" name_original="tepals" src="d0_s6" type="structure" />
      <biological_entity id="o3162" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <relation from="o3158" id="r405" name="part_of" negation="false" src="d0_s6" to="o3159" />
      <relation from="o3158" id="r406" name="with" negation="false" src="d0_s6" to="o3160" />
    </statement>
    <statement id="d0_s7">
      <text>of staminate flowers, 4 mm, equaling or longer than outer tepals, apex long-acuminate.</text>
      <biological_entity id="o3163" name="bract" name_original="bracts" src="d0_s7" type="structure" constraint="flower" constraint_original="flower; flower">
        <character name="some_measurement" src="d0_s7" unit="mm" value="4" value_original="4" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
        <character constraint="than outer tepals" constraintid="o3165" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o3164" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o3165" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <biological_entity id="o3166" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <relation from="o3163" id="r407" name="part_of" negation="false" src="d0_s7" to="o3164" />
    </statement>
    <statement id="d0_s8">
      <text>Pistillate flowers: tepals 1.7–3.8 mm, apex acuminate, mucronulate;</text>
      <biological_entity id="o3167" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o3168" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s8" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3169" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style-branches spreading;</text>
      <biological_entity id="o3170" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o3171" name="style-branch" name_original="style-branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 2 (–3).</text>
      <biological_entity id="o3172" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o3173" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="3" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: tepals 5, unequal, 2–4 mm, apex acute;</text>
      <biological_entity id="o3174" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3175" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3176" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>inner tepals with prominent midrib excurrent as rigid spine, apex long-acuminate or mucronulate;</text>
      <biological_entity id="o3177" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3178" name="tepal" name_original="tepals" src="d0_s12" type="structure" />
      <biological_entity id="o3179" name="midrib" name_original="midrib" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character constraint="as spine" constraintid="o3180" is_modifier="false" name="architecture" src="d0_s12" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o3180" name="spine" name_original="spine" src="d0_s12" type="structure">
        <character is_modifier="true" name="texture" src="d0_s12" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o3181" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="long-acuminate" value_original="long-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <relation from="o3178" id="r408" name="with" negation="false" src="d0_s12" to="o3179" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 5.</text>
      <biological_entity id="o3182" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3183" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Utricles tan to brown, occasionally reddish-brown, obovoid to subglobose, 1.5–2 mm, shorter than tepals, at maturity walls thin, almost smooth or indistinctly rugose.</text>
      <biological_entity id="o3184" name="utricle" name_original="utricles" src="d0_s14" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s14" to="brown" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s14" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s14" to="subglobose" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character constraint="than tepals" constraintid="o3185" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="almost" name="relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="indistinctly" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o3185" name="tepal" name_original="tepals" src="d0_s14" type="structure" />
      <biological_entity id="o3186" name="wall" name_original="walls" src="d0_s14" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s14" value="maturity" value_original="maturity" />
        <character is_modifier="false" name="width" src="d0_s14" value="thin" value_original="thin" />
      </biological_entity>
      <relation from="o3184" id="r409" name="at" negation="false" src="d0_s14" to="o3186" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds dark reddish-brown to brown, 1–1.2 mm diam., shiny.</text>
      <biological_entity id="o3187" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="dark reddish-brown" name="coloration" src="d0_s15" to="brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s15" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall, occasionally spring–winter in southern part of its native range.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" modifier="occasionally;  in southern part of its native range" to="winter" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Streambanks, disturbed habitats, especially agricultural fields, railroads, waste areas, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streambanks" />
        <character name="habitat" value="disturbed habitats" />
        <character name="habitat" value="agricultural fields" modifier="especially" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="waste areas" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ariz., Ark., Calif., Colo., Fla., Ga., Ill., Kans., Ky., La., Md., Mass., Miss., Mo., Nebr., Nev., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Utah, Va., W.Va., Wis.; Mexico; introduced Europe, Asia, and Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="and Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="past_name">Amarantus</other_name>
  <other_name type="common_name">Palmer’s amara nth</other_name>
  <discussion>Originally native to the North American Southwest, from southern California to Texas and northern Mexico, Amaranthus palmeri at present is a successful invasive species, which is evident from its expansion both in eastern North America and overseas. Because of its rapid spread, the distribution data presented here are probably incomplete.</discussion>
  
</bio:treatment>