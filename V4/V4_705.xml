<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">361</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Gaertner) S. L. Welsh" date="2001" rank="subgenus">Obione</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Obione</taxon_name>
    <taxon_name authority="(Standley) S. L. Welsh" date="2001" rank="subsection">Arenariae</taxon_name>
    <taxon_name authority="A. Nelson ex Abrams" date="1904" rank="species">serenana</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">serenana</taxon_name>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus obione;section obione;subsection arenariae;species serenana;variety serenana;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415556</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Staminate inflorescences of elongate spikes or panicles, with numerous, beadlike glomerules.</text>
      <biological_entity id="o18906" name="spike" name_original="spikes" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o18907" name="panicle" name_original="panicles" src="d0_s0" type="structure" />
      <biological_entity id="o18908" name="glomerule" name_original="glomerules" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="shape" src="d0_s0" value="beadlike" value_original="beadlike" />
      </biological_entity>
      <relation from="o18905" id="r2734" name="part_of" negation="false" src="d0_s0" to="o18906" />
      <relation from="o18905" id="r2735" name="part_of" negation="false" src="d0_s0" to="o18907" />
      <relation from="o18905" id="r2736" name="with" negation="false" src="d0_s0" to="o18908" />
    </statement>
    <statement id="d0_s1">
      <text>Fruiting bracteoles mainly 1-veined, 2.1–3.5 × 1.7–3.5 mm wide.</text>
      <biological_entity id="o18905" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="mainly" name="width" src="d0_s1" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="width" src="d0_s1" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s1" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18909" name="bracteole" name_original="bracteoles" src="d0_s1" type="structure" />
      <relation from="o18905" id="r2737" name="fruiting" negation="false" src="d0_s1" to="o18909" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline valleys, marshy areas, valley grasslands, coastal sage scrub, sometimes ruderal weed</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline valleys" />
        <character name="habitat" value="marshy areas" />
        <character name="habitat" value="valley grasslands" />
        <character name="habitat" value="coastal sage scrub" />
        <character name="habitat" value="ruderal weed" modifier="sometimes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>70-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="70" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35a.</number>
  <discussion>The most striking feature of Atriplex serenana var. serenana is its usually elongate, paniculate staminate inflorescence with its numerous beadlike glomerules. The taxon in the broad sense is closely allied to the disjunct Atriplex wrightii.</discussion>
  
</bio:treatment>