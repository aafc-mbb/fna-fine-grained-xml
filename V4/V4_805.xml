<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">405</other_info_on_meta>
    <other_info_on_meta type="mention_page">407</other_info_on_meta>
    <other_info_on_meta type="treatment_page">409</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">celosia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">cristata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 205. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus celosia;species cristata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200006993</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Celosia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">argentea</taxon_name>
    <taxon_name authority="(Linnaeus) Kuntze" date="unknown" rank="variety">cristata</taxon_name>
    <taxon_hierarchy>genus Celosia;species argentea;variety cristata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o7049" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, mostly 0.3–1 m, glabrous.</text>
      <biological_entity id="o7050" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="m" modifier="mostly" name="some_measurement" src="d0_s1" to="1" to_unit="m" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1–3 cm;</text>
      <biological_entity id="o7051" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7052" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade unlobed, variable, mostly lanceolate or ovate, 8–15 × 1–8 cm, base tapering, apex long-acuminate.</text>
      <biological_entity id="o7053" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7054" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="variability" src="d0_s3" value="variable" value_original="variable" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7055" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o7056" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences variously fasciated, dense, crested or plumose.</text>
      <biological_entity id="o7057" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="variously" name="fusion" src="d0_s4" value="fasciated" value_original="fasciated" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crested" value_original="crested" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: tepals pink, red, yellow, purple, or white, faintly 3-veined at base, 5–8 mm, scarious;</text>
      <biological_entity id="o7058" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o7059" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character constraint="at base" constraintid="o7060" is_modifier="false" modifier="faintly" name="architecture" src="d0_s5" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o7060" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>style elongate, 3–4 mm;</text>
      <biological_entity id="o7061" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o7062" name="style" name_original="style" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stigmas 3.</text>
      <biological_entity id="o7063" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7064" name="stigma" name_original="stigmas" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Utricles 3–4 mm.</text>
      <biological_entity id="o7065" name="utricle" name_original="utricles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 6–10, 1.5 mm diam., faintly reticulate, shiny.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 72.</text>
      <biological_entity id="o7066" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character name="diameter" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" modifier="faintly" name="architecture_or_coloration_or_relief" src="d0_s9" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7067" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Trash dumps, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="trash dumps" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Conn., D.C., Kans., La., Mo., N.C., Ohio, R.I., Tenn., Vt.; West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>In this treatment, Celosia cristata, the cultivated cockscomb, is considered a species separate from C. argentea, its likely wild progenitor; however, it is often treated as an infraspecific entity (variety or form) of the latter. The former is a tetraploid; the latter, an octoploid, although a tetraploid race of C. argentea is known in India (T. N. Khoshoo and M. Pal 1973). Convincing evidence has been presented for recognizing this cytologically and morphologically distinct race as a separate species (W. F. Grant 1961, 1962). Celosia cristata is known only in cultivation or as an escape from cultivation.</discussion>
  
</bio:treatment>