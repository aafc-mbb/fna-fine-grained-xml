<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="treatment_page">370</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) S. L. Welsh" date="2001" rank="subgenus">Pterochiton</taxon_name>
    <taxon_name authority="(Torrey) S. Watson" date="1874" rank="species">acanthocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>9: 117. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus pterochiton;species acanthocarpa;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415578</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Obione</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">acanthocarpa</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 183. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Obione;species acanthocarpa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or subshrubs, dioecious, evergreen, mainly 2–10 × 4–10+ dm, woody especially basally, unarmed;</text>
      <biological_entity id="o6201" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="width" src="d0_s0" to="10" to_unit="dm" upper_restricted="false" />
        <character is_modifier="false" modifier="mainly; especially basally; basally" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="width" src="d0_s0" to="10" to_unit="dm" upper_restricted="false" />
        <character is_modifier="false" modifier="mainly; especially basally; basally" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branchlets obtusely angled to subterete.</text>
      <biological_entity id="o6203" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtusely angled" name="shape" src="d0_s1" to="subterete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, proximal ones opposite, becoming alternate distally, short petiolate or subsessile;</text>
      <biological_entity id="o6204" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6205" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="becoming; distally" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblong to oblong-lanceolate, ovate, obovate, or spatulate, 12–40 (–50) × 5–25 mm, base commonly subhastate to cuneate, margin entire or sinuate-dentate to strongly undulate-crisped, apex acute.</text>
      <biological_entity id="o6206" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="oblong-lanceolate ovate obovate or spatulate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="oblong-lanceolate ovate obovate or spatulate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="oblong-lanceolate ovate obovate or spatulate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="oblong-lanceolate ovate obovate or spatulate" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6207" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="commonly subhastate" name="shape" src="d0_s3" to="cuneate" />
      </biological_entity>
      <biological_entity id="o6208" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s3" value="sinuate-dentate to strongly undulate-crisped" value_original="sinuate-dentate to strongly undulate-crisped" />
      </biological_entity>
      <biological_entity id="o6209" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Staminate flowers in interrupted or crowded glomerules 2–4.5 mm thick, in sparsely leafy paniculate spikes to 5+ dm.</text>
      <biological_entity id="o6210" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6211" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s4" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6212" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sparsely" name="architecture" src="d0_s4" value="leafy" value_original="leafy" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s4" to="5" to_unit="dm" upper_restricted="false" />
      </biological_entity>
      <relation from="o6210" id="r838" name="in" negation="false" src="d0_s4" to="o6211" />
      <relation from="o6210" id="r839" name="in" negation="false" src="d0_s4" to="o6212" />
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers few to solitary, in axillary clusters or in crowded or interrupted, often leafy, erect, branched spicate racemes or racemose-panicles to 25+ cm.</text>
      <biological_entity constraint="axillary" id="o6214" name="cluster" name_original="clusters" src="d0_s5" type="structure" />
      <biological_entity id="o6215" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="crowded" value_original="crowded" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="true" modifier="often" name="architecture" src="d0_s5" value="leafy" value_original="leafy" />
        <character is_modifier="true" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="branched" value_original="branched" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
      </biological_entity>
      <biological_entity id="o6216" name="racemose-panicle" name_original="racemose-panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="25" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <relation from="o6213" id="r840" name="in" negation="false" src="d0_s5" to="o6214" />
      <relation from="o6213" id="r841" name="in" negation="false" src="d0_s5" to="o6215" />
      <relation from="o6213" id="r842" name="in" negation="false" src="d0_s5" to="o6216" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting bracteoles on slender or stout stipes (2–) 4–20 mm (or sessile), body broadly elliptic to globose, 6–15 mm and wide, spongy, united to the linear apex, margin deeply laciniate, faces appendaged with flattened to hornlike tubercles to 8 mm.</text>
      <biological_entity id="o6213" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="few" value_original="few" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o6217" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure" />
      <biological_entity id="o6218" name="stipe" name_original="stipes" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="stout" value_original="stout" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6219" name="body" name_original="body" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s6" to="globose" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s6" value="wide" value_original="wide" />
        <character is_modifier="false" name="texture" src="d0_s6" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o6220" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o6221" name="margin" name_original="margin" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s6" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity id="o6222" name="face" name_original="faces" src="d0_s6" type="structure">
        <character constraint="with tubercles" constraintid="o6223" is_modifier="false" name="architecture" src="d0_s6" value="appendaged" value_original="appendaged" />
      </biological_entity>
      <biological_entity id="o6223" name="tubercle" name_original="tubercles" src="d0_s6" type="structure">
        <character char_type="range_value" from="flattened" is_modifier="true" name="shape" src="d0_s6" to="hornlike" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o6213" id="r843" name="fruiting" negation="false" src="d0_s6" to="o6217" />
      <relation from="o6219" id="r844" name="united to the" negation="false" src="d0_s6" to="o6220" />
    </statement>
    <statement id="d0_s7">
      <text>Seeds brown, 1.5–2 mm.</text>
      <biological_entity id="o6224" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>49.</number>
  <other_name type="common_name">Burscale</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade ovate to rhombic-ovate, elliptic, or spatulate, margin mainly sinuate-dentate; mature fruiting inflorescences ascending to erect; west Texas, s New Mexico, se Arizona</description>
      <determination>49a Atriplex acanthocarpa var. acanthocarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade, at least some, hastate-lanceolate, or strongly undulate-crisped; mature fruiting inflorescences mainly horizontally spreading; s Texas, Mexico</description>
      <determination>49b Atriplex acanthocarpa var. coahuilensis</determination>
    </key_statement>
  </key>
</bio:treatment>