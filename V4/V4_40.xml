<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">26</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">boerhavia</taxon_name>
    <taxon_name authority="(S. Watson) Standley" date="unknown" rank="species">torreyana</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>12: 385. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus boerhavia;species torreyana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415019</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Boerhavia</taxon_name>
    <taxon_name authority="Choisy" date="unknown" rank="species">spicata</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="variety">torreyana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>24: 70. 1889 (as Boerhaavia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Boerhavia;species spicata;variety torreyana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
      <biological_entity id="o17745" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot tapered, soft or ± woody.</text>
      <biological_entity id="o17746" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="texture" src="d0_s1" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate or decumbent-ascending, usually profusely branched throughout, 10–80 dm, minutely puberulent with flat hairs, usually also with spreading hairs, sometimes also glandular basally, glabrous distally.</text>
      <biological_entity id="o17747" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent-ascending" value_original="decumbent-ascending" />
        <character is_modifier="false" modifier="usually profusely; throughout" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s2" to="80" to_unit="dm" />
        <character constraint="with hairs" constraintid="o17748" is_modifier="false" modifier="minutely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes; basally" name="architecture_or_function_or_pubescence" notes="" src="d0_s2" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17748" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o17749" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o17747" id="r2563" modifier="usually" name="with" negation="false" src="d0_s2" to="o17749" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly in basal 1/2;</text>
      <biological_entity id="o17750" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o17751" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <relation from="o17750" id="r2564" name="in" negation="false" src="d0_s3" to="o17751" />
    </statement>
    <statement id="d0_s4">
      <text>larger leaves with petiole 7–20 mm, blade oblong-ovate, oval, ovate, or lanceolate, 20–45 × 9–25 mm (distal leaves usually shorter, proportionately narrower), adaxial surface glabrous or sparsely and minutely puberulent, abaxial surface paler than adaxial, glabrous or minutely puberulent, sometimes glandular, neither surface punctate or both punctate with minute clusters of brown cells, base truncate, rounded, or obtuse, margins sinuate to sinuate-crisped, apex round to obtuse or acute.</text>
      <biological_entity constraint="larger" id="o17752" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17753" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17754" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17755" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s4" value="sparsely" value_original="sparsely" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17756" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="than adaxial surface" constraintid="o17757" is_modifier="false" name="coloration" src="d0_s4" value="paler" value_original="paler" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17757" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o17758" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s4" value="punctate" value_original="punctate" />
        <character name="coloration_or_relief" src="d0_s4" value="both" value_original="both" />
        <character constraint="of cells" constraintid="o17759" is_modifier="false" name="coloration_or_relief" src="d0_s4" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity id="o17759" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o17760" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o17761" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="sinuate" name="shape" src="d0_s4" to="sinuate-crisped" />
      </biological_entity>
      <biological_entity id="o17762" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s4" to="obtuse or acute" />
      </biological_entity>
      <relation from="o17752" id="r2565" name="with" negation="false" src="d0_s4" to="o17753" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal and axillary, forked ca. 3–6 times unequally, diffuse, usually with sticky internodal bands;</text>
      <biological_entity id="o17763" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s5" value="forked" value_original="forked" />
        <character constraint="band" constraintid="o17764" is_modifier="false" modifier="unequally" name="size_or_quantity" src="d0_s5" value="3-6 times diffuse with sticky internodal bands" />
        <character constraint="band" constraintid="o17765" is_modifier="false" name="size_or_quantity" src="d0_s5" value="3-6 times diffuse with sticky internodal bands" />
        <character constraint="band" constraintid="o17766" is_modifier="false" name="size_or_quantity" src="d0_s5" value="3-6 times diffuse with sticky internodal bands" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o17764" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="internodal" id="o17765" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="internodal" id="o17766" name="band" name_original="bands" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches strongly ascending, terminating in spicate or racemose flower clusters, axis 10–40 mm.</text>
      <biological_entity id="o17767" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o17768" name="flower" name_original="flower" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="spicate" value_original="spicate" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o17769" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="40" to_unit="mm" />
      </biological_entity>
      <relation from="o17767" id="r2566" name="terminating in" negation="false" src="d0_s6" to="o17768" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: pedicel 0.1–1.7 mm;</text>
      <biological_entity id="o17770" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o17771" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bract at base of perianth usually soon deciduous, usually 1, broadly lanceolate to lance-attenuate, 0.5–1.2 mm;</text>
      <biological_entity id="o17772" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o17773" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" notes="" src="d0_s8" to="lance-attenuate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17774" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually soon" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character modifier="usually" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o17775" name="perianth" name_original="perianth" src="d0_s8" type="structure" />
      <relation from="o17773" id="r2567" name="at" negation="false" src="d0_s8" to="o17774" />
      <relation from="o17774" id="r2568" name="part_of" negation="false" src="d0_s8" to="o17775" />
    </statement>
    <statement id="d0_s9">
      <text>perianth whitish to purplish-pink, campanulate distal to constriction, 1–1.3 mm;</text>
      <biological_entity id="o17776" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17777" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s9" to="purplish-pink" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character constraint="to constriction" constraintid="o17778" is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17778" name="constriction" name_original="constriction" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 2, slightly exserted.</text>
      <biological_entity id="o17779" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17780" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits 2–12 per cluster, usually remotely spaced, sometimes about 1/2 overlapped, occasionally ± paired, straw colored to grayish tan, narrowly obovoid, 2.2–3 (–3.8) × 0.9–1.1 (–1.3) mm (l/w: [1.9–] 2.2–3.1), apex round or round-obtuse, glabrous;</text>
      <biological_entity id="o17781" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per cluster" from="2" name="quantity" src="d0_s11" to="12" />
        <character is_modifier="false" modifier="usually remotely" name="arrangement" src="d0_s11" value="spaced" value_original="spaced" />
        <character modifier="sometimes" name="quantity" src="d0_s11" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="occasionally more or less" name="arrangement" src="d0_s11" value="paired" value_original="paired" />
        <character char_type="range_value" from="straw colored" name="coloration" src="d0_s11" to="grayish tan" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17782" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s11" value="round-obtuse" value_original="round-obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ribs 5, obtuse or round-obtuse, strongly rugose near sulci;</text>
      <biological_entity id="o17783" name="rib" name_original="ribs" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s12" value="round-obtuse" value_original="round-obtuse" />
        <character constraint="near sulci" constraintid="o17784" is_modifier="false" modifier="strongly" name="relief" src="d0_s12" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o17784" name="sulcus" name_original="sulci" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>sulci 0.5–1 times as wide as base of ribs, not or slightly rugose, papillate.</text>
      <biological_entity id="o17786" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o17787" name="rib" name_original="ribs" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 52.</text>
      <biological_entity id="o17785" name="sulcus" name_original="sulci" src="d0_s13" type="structure">
        <character constraint="of ribs" constraintid="o17787" is_modifier="false" name="width" src="d0_s13" value="0.5-1 times as wide as base" />
        <character is_modifier="false" modifier="not; slightly" name="relief" notes="" src="d0_s13" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17788" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer and early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or rocky soils in deserts or arid grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" constraint="in deserts or arid grasslands" />
        <character name="habitat" value="rocky soils" constraint="in deserts or arid grasslands" />
        <character name="habitat" value="deserts" />
        <character name="habitat" value="arid grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex., Utah; Mexico (Chihuahua, Coahuila); South America (introduced in Argentina).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="South America ( in Argentina)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="past_name">Boerhaavia</other_name>
  <discussion>Boerhavia torreyana occurs in the Chihuahuan Desert, extends north of the Mogollon Rim in arid sites, and barely enters southeastern Arizona. The slender, remotely flowered inflorescences and the papillate sulci distinguish this from other spicate species. There were fewer ants and aphids on plants of B. torreyana (reported as B. spicata) with intact internodal bands as compared to those whose bands had been experimentally altered (Y. McClelan and W. J. Boecklen 1993).</discussion>
  
</bio:treatment>