<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">176</other_info_on_meta>
    <other_info_on_meta type="treatment_page">179</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Haworth" date="1812" rank="genus">epiphyllum</taxon_name>
    <taxon_name authority="(Linnaeus) Haworth" date="1812" rank="species">phyllanthus</taxon_name>
    <taxon_name authority="(Haworth) Kimnach" date="1964" rank="variety">hookeri</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>36: 113. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus epiphyllum;species phyllanthus;variety hookeri;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415115</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epiphyllum</taxon_name>
    <taxon_name authority="Haworth" date="unknown" rank="species">hookeri</taxon_name>
    <place_of_publication>
      <publication_title>Philos. Mag. Ann. Chem.</publication_title>
      <place_in_publication>6: 108. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Epiphyllum;species hookeri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, to 200 cm, epiphytic.</text>
      <biological_entity id="o3188" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem segments light green, 40–120 × 6–9 (–10) cm, margins obtusely serrate;</text>
      <biological_entity constraint="stem" id="o3189" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="light green" value_original="light green" />
        <character char_type="range_value" from="40" from_unit="cm" name="length" src="d0_s1" to="120" to_unit="cm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s1" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3190" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="obtusely" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>areoles 3–8 cm apart along margins.</text>
      <biological_entity id="o3191" name="areole" name_original="areoles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character constraint="along margins" constraintid="o3192" is_modifier="false" name="arrangement" src="d0_s2" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity id="o3192" name="margin" name_original="margins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Spines occasionally present in basal areoles, to 3 per areole, hairlike.</text>
      <biological_entity id="o3193" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character constraint="in basal areoles" constraintid="o3194" is_modifier="false" modifier="occasionally" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3194" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o3195" from="0" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o3195" name="areole" name_original="areole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers salverform, 19–23 × 10–15 (–16) cm;</text>
      <biological_entity id="o3196" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="salverform" value_original="salverform" />
        <character char_type="range_value" from="19" from_unit="cm" name="length" src="d0_s4" to="23" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="16" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flower tube slender;</text>
      <biological_entity constraint="flower" id="o3197" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer tepals greenish white;</text>
      <biological_entity constraint="outer" id="o3198" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish white" value_original="greenish white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>inner tepals 8 × 0.5 cm.</text>
      <biological_entity constraint="inner" id="o3199" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="8" value_original="8" />
        <character name="width" src="d0_s7" unit="cm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits purplish red, typically 5-angled from the flower tube podaria, smooth between the ridges, oblong, 40–70 × 20–35 mm.</text>
      <biological_entity id="o3200" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="purplish red" value_original="purplish red" />
        <character constraint="from flower tube podaria" constraintid="o3201" is_modifier="false" modifier="typically" name="shape" src="d0_s8" value="5-angled" value_original="5-angled" />
        <character constraint="between ridges" constraintid="o3202" is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s8" to="70" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s8" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="tube" id="o3201" name="podarium" name_original="podaria" src="d0_s8" type="structure" constraint_original="flower tube" />
      <biological_entity id="o3202" name="ridge" name_original="ridges" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Seeds 3.2–3.5 mm, minutely pitted.</text>
      <biological_entity id="o3203" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swamps, palmetto</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swamps" />
        <character name="habitat" value="palmetto" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; s Mexico; West Indies; Central America; South America (n Venezuela); cultivated in tropical regions worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="s Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (n Venezuela)" establishment_means="native" />
        <character name="distribution" value="cultivated in tropical regions worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <discussion>Epiphyllum phyllanthus var. hookeri was recently reported by A. N. Lima (1996) from Estero Island, Lee County, Florida, growing on Sabal palmetto. The variety also occurs in the swamp at Fakahatchee Strand, Collier County, Florida. The population biology of this species, as well as its long-term persistence in the flora, need to be monitored. Details of its pollination biology are not known.</discussion>
  
</bio:treatment>