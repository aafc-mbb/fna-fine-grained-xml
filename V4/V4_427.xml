<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="treatment_page">233</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="(Engelmann) Lemaire" date="1868" rank="genus">coryphantha</taxon_name>
    <taxon_name authority="Y. Wright" date="1932" rank="species">hesteri</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>4: 274, fig. (p. 273). 1932</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus coryphantha;species hesteri;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415331</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Escobaria</taxon_name>
    <taxon_name authority="(Y. Wright) Buxbaum" date="unknown" rank="species">hesteri</taxon_name>
    <taxon_hierarchy>genus Escobaria;species hesteri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants unbranched, except in old age, ultimately forming dense clumps, 5–20 (–30) cm diam., stem not obscured by spines when hydrated, obscured when desiccated.</text>
      <biological_entity id="o4134" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" modifier="ultimately" name="diameter" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" modifier="ultimately" name="diameter" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4135" name="age" name_original="age" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
      </biological_entity>
      <biological_entity id="o4136" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o4137" name="stem" name_original="stem" src="d0_s0" type="structure">
        <character constraint="by spines" constraintid="o4138" is_modifier="false" modifier="not" name="prominence" src="d0_s0" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o4138" name="spine" name_original="spines" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="when desiccated" name="prominence" src="d0_s0" value="obscured" value_original="obscured" />
      </biological_entity>
      <relation from="o4134" id="r546" name="except in" negation="false" src="d0_s0" to="o4135" />
      <relation from="o4134" id="r547" modifier="ultimately" name="forming" negation="false" src="d0_s0" to="o4136" />
    </statement>
    <statement id="d0_s1">
      <text>Roots short, fleshy, enlarged taproots, basally ± 1/3–1/2 of stem diam.</text>
      <biological_entity id="o4139" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o4140" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="enlarged" value_original="enlarged" />
        <character char_type="range_value" constraint="of stem" constraintid="o4141" from="1/3" modifier="basally more or less; more or less" name="quantity" src="d0_s1" to="1/2" />
      </biological_entity>
      <biological_entity id="o4141" name="stem" name_original="stem" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stems deep-seated, aerial parts inconspicuous, flat-topped to hemispheric (to ovoid or spheric in horticulture), almost completely withdrawing into substrate when desiccated, (2.4–) 5–9 × 1.5–4.7 cm, aboveground portion 1–6.5 (–9);</text>
      <biological_entity id="o4142" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="location" src="d0_s2" value="deep-seated" value_original="deep-seated" />
      </biological_entity>
      <biological_entity id="o4143" name="part" name_original="parts" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="flat-topped" name="shape" src="d0_s2" to="hemispheric" />
      </biological_entity>
      <biological_entity id="o4144" name="substrate" name_original="substrate" src="d0_s2" type="structure">
        <character char_type="range_value" from="2.4" from_unit="cm" name="atypical_length" src="d0_s2" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="4.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4145" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aboveground" value_original="aboveground" />
        <character char_type="range_value" from="6.5" from_inclusive="false" modifier="almost completely; completely; when desiccated" name="atypical_quantity" src="d0_s2" to="9" />
        <character char_type="range_value" from="1" modifier="almost completely; completely; when desiccated" name="quantity" src="d0_s2" to="6.5" />
      </biological_entity>
      <relation from="o4143" id="r548" modifier="almost completely; completely" name="withdrawing into" negation="false" src="d0_s2" to="o4144" />
    </statement>
    <statement id="d0_s3">
      <text>tubercles 5–9 (–12) × 4–6 (–7) mm;</text>
      <biological_entity id="o4146" name="tubercle" name_original="tubercles" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>areolar glands absent;</text>
      <biological_entity constraint="areolar" id="o4147" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>parenchyma not mucilaginous;</text>
      <biological_entity id="o4148" name="parenchyma" name_original="parenchyma" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s5" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>druses present, largest druses lenticular, 0.3–0.5 mm diam.;</text>
      <biological_entity id="o4149" name="druse" name_original="druses" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="largest" id="o4150" name="druse" name_original="druses" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pith 1/5–1/3 of lesser stem diam.;</text>
      <biological_entity id="o4151" name="pith" name_original="pith" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="of stem" constraintid="o4152" from="1/5" name="quantity" src="d0_s7" to="1/3" />
      </biological_entity>
      <biological_entity id="o4152" name="stem" name_original="stem" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>medullary vascular system not recorded.</text>
      <biological_entity constraint="medullary" id="o4153" name="system" name_original="system" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="vascular" value_original="vascular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spines (12–) 15–20 (–25) per areole, white with brown tips (quickly weathering to gray) or the adaxial spines per areole dark gray-brown throughout;</text>
      <biological_entity id="o4155" name="areole" name_original="areole" src="d0_s9" type="structure" />
      <biological_entity id="o4156" name="tip" name_original="tips" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4157" name="spine" name_original="spines" src="d0_s9" type="structure" />
      <biological_entity id="o4158" name="areole" name_original="areole" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s9" value="dark gray-brown" value_original="dark gray-brown" />
      </biological_entity>
      <relation from="o4157" id="r549" name="per" negation="false" src="d0_s9" to="o4158" />
    </statement>
    <statement id="d0_s10">
      <text>all radial spines or 1–3 adaxial spines sometimes interpreted as central, laterally compressed at base, 6–13 × (0.1–) 0.2–0.3 mm;</text>
      <biological_entity id="o4154" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s9" to="15" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="25" />
        <character char_type="range_value" constraint="per areole" constraintid="o4155" from="15" name="quantity" src="d0_s9" to="20" />
        <character constraint="with adaxial spines" constraintid="o4157" is_modifier="false" name="coloration" notes="" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="1" modifier="sometimes" name="quantity" src="d0_s10" to="3" unit="all radial spines or adaxial spines" />
      </biological_entity>
      <biological_entity constraint="central" id="o4159" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character constraint="at base" constraintid="o4160" is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o4160" name="base" name_original="base" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="atypical_width" src="d0_s10" to="0.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s10" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>subcentral spines 0 (–3) per areole;</text>
      <biological_entity constraint="subcentral" id="o4161" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="3" />
        <character constraint="per areole" constraintid="o4162" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4162" name="areole" name_original="areole" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>central spines 0 (–3) per areole, appressed, straight, ± equal, 9.5–13 (–15) × 0.2–0.3 mm.</text>
      <biological_entity constraint="central" id="o4163" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="3" />
        <character constraint="per areole" constraintid="o4164" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s12" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="15" to_unit="mm" />
        <character char_type="range_value" from="9.5" from_unit="mm" name="length" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4164" name="areole" name_original="areole" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers nearly apical, 18–25 × 20–34 mm;</text>
      <biological_entity id="o4165" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="length" notes="" src="d0_s13" to="25" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" notes="" src="d0_s13" to="34" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4166" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="nearly" name="position" src="d0_s13" value="apical" value_original="apical" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>outer tepals fringed;</text>
      <biological_entity constraint="outer" id="o4167" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>inner tepals 22–27 per flower, bright-rose-pink or magenta, proximally paler, 10–17 × 2–4 mm;</text>
      <biological_entity constraint="inner" id="o4168" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per flower" constraintid="o4169" from="22" name="quantity" src="d0_s15" to="27" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="bright-rose-pink" value_original="bright-rose-pink" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="magenta" value_original="magenta" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s15" value="paler" value_original="paler" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s15" to="17" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4169" name="flower" name_original="flower" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>outer filaments colorless or white to rose;</text>
      <biological_entity constraint="outer" id="o4170" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s16" to="rose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers orange-yellow;</text>
      <biological_entity id="o4171" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="orange-yellow" value_original="orange-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma lobes (4–) 5–6 (–7), white, cream, or pale-pink, 1–3 mm.</text>
      <biological_entity constraint="stigma" id="o4172" name="lobe" name_original="lobes" src="d0_s18" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s18" to="5" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="7" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s18" to="6" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="pale-pink" value_original="pale-pink" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits green, spheric or obovoid, (3.5–) 5–8 (–10) × 3–6 mm, quickly drying;</text>
      <biological_entity id="o4173" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s19" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s19" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s19" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s19" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s19" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s19" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="quickly" name="condition" src="d0_s19" value="drying" value_original="drying" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>floral remnant usually persistent.</text>
      <biological_entity constraint="floral" id="o4174" name="remnant" name_original="remnant" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s20" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds dark-brown, spheric, 0.9–1.1 (–1.2) mm, pitted.</text>
    </statement>
    <statement id="d0_s22">
      <text>2n = 22.</text>
      <biological_entity id="o4175" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s21" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="1.1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s21" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s21" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s21" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4176" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(-Nov); fruiting Aug–Oct(-Jan).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Aug" />
        <character name="fruiting time" char_type="atypical_range" to="Jan" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Semidesert grasslands, oak-juniper-pinyon woodlands, rock crevices, rocky soils, sandstone, limestone, igneous substrates, novaculite</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="semidesert grasslands" />
        <character name="habitat" value="oak-juniper-pinyon woodlands" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="igneous substrates" />
        <character name="habitat" value="novaculite" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Hes ter’s pincushion cactus</other_name>
  <other_name type="common_name">Hester’s foxtail cactus</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Coryphantha hesteri resembles C. vivipara but is much smaller in all parts. Coryphantha hesteri is characteristic of the dwarf cacti on novaculite outcrops but is not limited to that substrate.</discussion>
  
</bio:treatment>