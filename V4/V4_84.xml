<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">40</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="treatment_page">45</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">mirabilis</taxon_name>
    <taxon_name authority="(Choisy) A. Gray in W. H. Emory" date="1859" rank="section">Quamoclidion</taxon_name>
    <taxon_name authority="(Torrey) A. Gray in W. H. Emory" date="1859" rank="species">multiflora</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 173. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus mirabilis;section quamoclidion;species multiflora;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415050</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxybaphus</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">multiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>2: 237. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Oxybaphus;species multiflorus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Quamoclidion</taxon_name>
    <taxon_name authority="(Torrey) Torrey ex A. Gray" date="unknown" rank="species">multiflorum</taxon_name>
    <taxon_hierarchy>genus Quamoclidion;species multiflorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, forming hemispheric clumps 6–10 dm diam., glabrous or densely pubescent.</text>
      <biological_entity id="o3031" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="dm" name="diameter" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o3032" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <relation from="o3031" id="r389" name="forming" negation="false" src="d0_s0" to="o3032" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 4–7 dm.</text>
      <biological_entity id="o3033" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s1" to="7" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading;</text>
      <biological_entity id="o3034" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles of proximal leaves 2–4 cm;</text>
      <biological_entity id="o3035" name="petiole" name_original="petioles" src="d0_s3" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3036" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o3035" id="r390" name="part_of" negation="false" src="d0_s3" to="o3036" />
    </statement>
    <statement id="d0_s4">
      <text>blades of midstem leaves ovate to widely ovate, sometimes suborbiculate, rarely reniform, 5–10 × 4–8 cm, base rounded to cordate, often asymmetric, apex acute or acuminate to obtuse, rarely rounded.</text>
      <biological_entity id="o3037" name="blade" name_original="blades" src="d0_s4" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="widely ovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o3038" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3039" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="cordate" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s4" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
      <biological_entity id="o3040" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o3037" id="r391" name="part_of" negation="false" src="d0_s4" to="o3038" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres: peduncle 4–75 mm;</text>
      <biological_entity id="o3041" name="involucre" name_original="involucres" src="d0_s5" type="structure" />
      <biological_entity id="o3042" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="75" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucres erect or ascending, 33–35 mm;</text>
      <biological_entity id="o3043" name="involucre" name_original="involucres" src="d0_s6" type="structure" />
      <biological_entity id="o3044" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="33" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 5, usually more than 50% connate, apex acute to obtuse or ovate.</text>
      <biological_entity id="o3045" name="involucre" name_original="involucres" src="d0_s7" type="structure" />
      <biological_entity id="o3046" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" modifier="usually; 50+%" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o3047" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="obtuse or ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 6 per involucre;</text>
      <biological_entity id="o3048" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character constraint="per involucre" constraintid="o3049" name="quantity" src="d0_s8" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o3049" name="involucre" name_original="involucre" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>perianth magenta, funnelform, 2.5–6 cm.</text>
      <biological_entity id="o3050" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="magenta" value_original="magenta" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s9" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits brown to black, with 10 slender, tan ribs alternating with 10 dark-brown ribs, or ribs inconspicuous, ovoid or globose, 6–11 mm, smooth to rugulose, glabrous or pubescent, secreting mucilage or not when wetted.</text>
      <biological_entity id="o3051" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s10" to="black" />
      </biological_entity>
      <biological_entity id="o3052" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="10" value_original="10" />
        <character is_modifier="true" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="tan" value_original="tan" />
        <character constraint="with ribs" constraintid="o3053" is_modifier="false" name="arrangement" src="d0_s10" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o3053" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="10" value_original="10" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
      <biological_entity id="o3055" name="mucilage" name_original="mucilage" src="d0_s10" type="structure" />
      <relation from="o3051" id="r392" name="with" negation="false" src="d0_s10" to="o3052" />
      <relation from="o3054" id="r393" name="secreting" negation="false" src="d0_s10" to="o3055" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 66.</text>
      <biological_entity id="o3054" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s10" value="globose" value_original="globose" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s10" to="rugulose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3056" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="66" value_original="66" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., N.Mex., Nev., Tex., Utah; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Colorado four-o’clock</other_name>
  <other_name type="common_name">Froebel’s four-o’clock</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>G. E. Pilz (1978) recognized three partially sympatric varieties based on presence or absence of mucilage production in the fruits, fruit color, and apical acuteness of involucral bracts. Overall, populations are poorly differentiated, and in some areas plants represent a “collage” (Pilz’s term) that combine characteristics of different varieties; S. L. Welsh et al. (1987) recognized no varieties. Mirabilis multiflora is used in the Southwest in a minor way in xeriscapes. Among indigenous peoples, it has been used as food and medicine (V. L. Bohrer 1975; L. S. M. Curtin 1947).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits tuberculate, mucilaginous when wetted; involucral bracts obtuse</description>
      <determination>7c Mirabilis multiflora var. glandulosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits smooth to slightly tuberculate, not mucilaginous when wetted; involucral bracts acute</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruits dark brown to black, ribs inconspicu- ous</description>
      <determination>7a Mirabilis multiflora var. multiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruits light brown, with 10 slender, tan, longitudinal ribs alternating with 10 brown, often interrupted ribs.</description>
      <determination>7b Mirabilis multiflora var. pubescens</determination>
    </key_statement>
  </key>
</bio:treatment>