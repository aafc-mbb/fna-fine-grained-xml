<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">489</other_info_on_meta>
    <other_info_on_meta type="mention_page">490</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="treatment_page">495</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1814" rank="genus">phemeranthus</taxon_name>
    <taxon_name authority="(Torrey) Hershkovitz" date="1997" rank="species">spinescens</taxon_name>
    <place_of_publication>
      <publication_title>Taxon</publication_title>
      <place_in_publication>46: 222. 1997</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus phemeranthus;species spinescens</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415802</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Talinum</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">spinescens</taxon_name>
    <place_of_publication>
      <publication_title>in C. Wilkes et al., U.S. Expl. Exped.</publication_title>
      <place_in_publication>17: 250. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Talinum;species spinescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 3 dm;</text>
      <biological_entity id="o18316" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots elongate, woody.</text>
      <biological_entity id="o18317" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending, simple or branching, bearing ± persistent, straight, spinelike, 5 mm or more, proximal portions of midribs of old leaves, suffrutescent.</text>
      <biological_entity id="o18318" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="suffrutescent" value_original="suffrutescent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18319" name="portion" name_original="portions" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="more or less" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="true" name="shape" src="d0_s2" value="spinelike" value_original="spinelike" />
        <character is_modifier="true" name="some_measurement" src="d0_s2" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o18320" name="midrib" name_original="midribs" src="d0_s2" type="structure" />
      <biological_entity id="o18321" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="old" value_original="old" />
      </biological_entity>
      <relation from="o18318" id="r2640" name="bearing" negation="false" src="d0_s2" to="o18319" />
      <relation from="o18318" id="r2641" name="part_of" negation="false" src="d0_s2" to="o18320" />
      <relation from="o18318" id="r2642" name="part_of" negation="false" src="d0_s2" to="o18321" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves petiolate;</text>
      <biological_entity id="o18322" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade subterete, to 2.5 cm, base attenuate.</text>
      <biological_entity id="o18323" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18324" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymose, overtopping leaves;</text>
      <biological_entity id="o18325" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymose" value_original="cymose" />
      </biological_entity>
      <biological_entity id="o18326" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o18325" id="r2643" name="overtopping" negation="false" src="d0_s5" to="o18326" />
    </statement>
    <statement id="d0_s6">
      <text>peduncle scapelike, to 20 cm.</text>
      <biological_entity id="o18327" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals deciduous, orbiculate to ovate, to 3 mm;</text>
      <biological_entity id="o18328" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18329" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s7" to="ovate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals pale to bright rose or purplish red, elliptic to obovate, to 10 mm;</text>
      <biological_entity id="o18330" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18331" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="bright rose or purplish red" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 20–30;</text>
      <biological_entity id="o18332" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18333" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma 1, subcapitate.</text>
      <biological_entity id="o18334" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18335" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="subcapitate" value_original="subcapitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ovoid-globose, 5 mm.</text>
      <biological_entity id="o18336" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid-globose" value_original="ovoid-globose" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds without arcuate ridges, 1.2 mm.</text>
      <biological_entity id="o18337" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character name="some_measurement" notes="" src="d0_s12" unit="mm" value="1.2" value_original="1.2" />
      </biological_entity>
      <biological_entity id="o18338" name="ridge" name_original="ridges" src="d0_s12" type="structure">
        <character is_modifier="true" name="course_or_shape" src="d0_s12" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <relation from="o18337" id="r2644" name="without" negation="false" src="d0_s12" to="o18338" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs, ledges, and outcrops in basaltic soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="outcrops" constraint="in basaltic soils" />
        <character name="habitat" value="basaltic soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  
</bio:treatment>