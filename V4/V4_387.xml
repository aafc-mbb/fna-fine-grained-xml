<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Allan D. Zimmerman,Bruce D. Parfitt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">197</other_info_on_meta>
    <other_info_on_meta type="mention_page">198</other_info_on_meta>
    <other_info_on_meta type="treatment_page">207</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Backeberg" date="1938" rank="genus">GLANDULICACTUS</taxon_name>
    <place_of_publication>
      <publication_title>Blätter Kakteenf.</publication_title>
      <place_in_publication>1938(6): [18, 10, 13, 25]. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus glandulicactus;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin glandula, gland, and Cactus, an old genus name</other_info_on_name>
    <other_info_on_name type="fna_id">113596</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, usually unbranched, not deep-seated in substrate.</text>
      <biological_entity id="o8625" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character constraint="in substrate" constraintid="o8626" is_modifier="false" modifier="not" name="location" src="d0_s0" value="deep-seated" value_original="deep-seated" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8626" name="substrate" name_original="substrate" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse.</text>
      <biological_entity id="o8627" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unsegmented, usually bluish green or grayish green, spheric to cylindric, (5–) 7–15 (–30) × 5–7.5 (–10) cm, glabrous;</text>
      <biological_entity id="o8628" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s2" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish green" value_original="grayish green" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s2" to="cylindric" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s2" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s2" to="7.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ribs 9–13, protruding on sexually mature plants, crests deeply notched adaxial to each areole, making ribs ± crenate (ribs of young plants almost completely divided into tubercles);</text>
      <biological_entity id="o8629" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s3" to="13" />
        <character constraint="on plants" constraintid="o8630" is_modifier="false" name="prominence" src="d0_s3" value="protruding" value_original="protruding" />
      </biological_entity>
      <biological_entity id="o8630" name="plant" name_original="plants" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="sexually" name="life_cycle" src="d0_s3" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o8631" name="crest" name_original="crests" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s3" value="notched" value_original="notched" />
        <character constraint="to areole" constraintid="o8632" is_modifier="false" name="position" src="d0_s3" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o8632" name="areole" name_original="areole" src="d0_s3" type="structure" />
      <biological_entity id="o8633" name="rib" name_original="ribs" src="d0_s3" type="structure" />
      <relation from="o8631" id="r1210" name="making" negation="false" src="d0_s3" to="o8633" />
    </statement>
    <statement id="d0_s4">
      <text>tubercles 9–15 × 6–10 mm;</text>
      <biological_entity id="o8634" name="tubercle" name_original="tubercles" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>areoles spaced 20–25 mm apart along ribs, adaxially elongated into short, conspicuous areolar grooves extending into rib notches (axils of partially confluent tubercles), short woolly;</text>
      <biological_entity id="o8635" name="areole" name_original="areoles" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="spaced" value_original="spaced" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
        <character constraint="along ribs" constraintid="o8636" is_modifier="false" name="arrangement" src="d0_s5" value="apart" value_original="apart" />
        <character constraint="into areolar, grooves" constraintid="o8637, o8638" is_modifier="false" modifier="adaxially" name="length" notes="" src="d0_s5" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o8636" name="rib" name_original="ribs" src="d0_s5" type="structure" />
      <biological_entity id="o8637" name="areolar" name_original="areolar" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o8638" name="groove" name_original="grooves" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity constraint="rib" id="o8639" name="notch" name_original="notches" src="d0_s5" type="structure" />
      <relation from="o8637" id="r1211" name="extending into" negation="false" src="d0_s5" to="o8639" />
      <relation from="o8638" id="r1212" name="extending into" negation="false" src="d0_s5" to="o8639" />
    </statement>
    <statement id="d0_s6">
      <text>areolar glands 1 to several, usually conspicuous in groove, hemispheric;</text>
      <biological_entity constraint="areolar" id="o8640" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="several" value_original="several" />
        <character constraint="in groove" constraintid="o8641" is_modifier="false" modifier="usually" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o8641" name="groove" name_original="groove" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>cortex and pith not mucilaginous.</text>
      <biological_entity id="o8642" name="cortex" name_original="cortex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s7" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o8643" name="pith" name_original="pith" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s7" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spines 10–12 per areole, straw colored to pale gray or reddish, longest spines 50–90 (–130) × 1–1.5 mm, glabrous;</text>
      <biological_entity id="o8644" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o8645" from="10" name="quantity" src="d0_s8" to="12" />
        <character char_type="range_value" from="straw colored" name="coloration" notes="" src="d0_s8" to="pale gray or reddish" />
      </biological_entity>
      <biological_entity id="o8645" name="areole" name_original="areole" src="d0_s8" type="structure" />
      <biological_entity constraint="longest" id="o8646" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="130" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s8" to="90" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>radial spines (5–) 8–10 per areole, (11–) 20–35 (–47) mm, 3 abaxial spines hooked, sometimes 1 straight abaxial spine beneath hooked spines;</text>
      <biological_entity id="o8647" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="radial" value_original="radial" />
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s9" to="8" to_inclusive="false" />
        <character char_type="range_value" constraint="per areole" constraintid="o8648" from="8" name="quantity" src="d0_s9" to="10" />
        <character char_type="range_value" from="11" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="47" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="35" to_unit="mm" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o8648" name="areole" name_original="areole" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o8649" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="hooked" value_original="hooked" />
        <character modifier="sometimes" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8650" name="spine" name_original="spine" src="d0_s9" type="structure">
        <character is_modifier="true" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o8651" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="hooked" value_original="hooked" />
      </biological_entity>
      <relation from="o8650" id="r1213" name="beneath" negation="false" src="d0_s9" to="o8651" />
    </statement>
    <statement id="d0_s10">
      <text>lateral and adaxial spines not hooked, 2–3 in semicentral spine position;</text>
      <biological_entity constraint="lateral and adaxial" id="o8652" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="hooked" value_original="hooked" />
        <character char_type="range_value" from="2" name="position" src="d0_s10" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>central spines 1–4 per areole, principal 1 hooked, turned upward, terete to somewhat flattened.</text>
      <biological_entity constraint="central" id="o8653" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o8654" from="1" name="quantity" src="d0_s11" to="4" />
        <character is_modifier="false" name="structure_subtype" notes="" src="d0_s11" value="principal" value_original="principal" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s11" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="turned" value_original="turned" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete to somewhat" value_original="terete to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o8654" name="areole" name_original="areole" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers diurnal, at stem apex at adaxial edges of areoles or at axillary end of short areolar groove, campanulate or funnelform, 2–4 × 2–3 cm;</text>
      <biological_entity id="o8655" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="diurnal" value_original="diurnal" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="campanulate" value_original="campanulate" />
        <character name="shape" src="d0_s12" value="funnel" value_original="funnelform" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s12" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s12" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o8656" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity constraint="adaxial" id="o8657" name="edge" name_original="edges" src="d0_s12" type="structure" />
      <biological_entity id="o8658" name="areole" name_original="areoles" src="d0_s12" type="structure" />
      <biological_entity constraint="axillary" id="o8659" name="end" name_original="end" src="d0_s12" type="structure" />
      <biological_entity id="o8660" name="areolar" name_original="areolar" src="d0_s12" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o8661" name="groove" name_original="groove" src="d0_s12" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
      </biological_entity>
      <relation from="o8655" id="r1214" name="at" negation="false" src="d0_s12" to="o8656" />
      <relation from="o8656" id="r1215" name="at" negation="false" src="d0_s12" to="o8657" />
      <relation from="o8657" id="r1216" name="part_of" negation="false" src="d0_s12" to="o8658" />
      <relation from="o8657" id="r1217" name="part_of" negation="false" src="d0_s12" to="o8659" />
      <relation from="o8657" id="r1218" name="part_of" negation="false" src="d0_s12" to="o8660" />
      <relation from="o8657" id="r1219" name="part_of" negation="false" src="d0_s12" to="o8661" />
    </statement>
    <statement id="d0_s13">
      <text>outer tepals entire or crenate-serrate to densely and minutely fringed;</text>
      <biological_entity constraint="outer" id="o8662" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="crenate-serrate" value_original="crenate-serrate" />
        <character is_modifier="false" modifier="densely; minutely" name="shape" src="d0_s13" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>inner tepals brick-red or maroon, 12–22 × 3.5–6 mm, margins minutely toothed;</text>
      <biological_entity constraint="inner" id="o8663" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brick-red" value_original="brick-red" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="maroon" value_original="maroon" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s14" to="22" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8664" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s14" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary scaly, spineless;</text>
      <biological_entity id="o8665" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s15" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="spineless" value_original="spineless" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>scales broad, entire or crenate-serrate to densely and minutely fringed;</text>
      <biological_entity id="o8666" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="width" src="d0_s16" value="broad" value_original="broad" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="crenate-serrate" value_original="crenate-serrate" />
        <character is_modifier="false" modifier="densely; minutely" name="shape" src="d0_s16" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma lobes 10–14, pale-yellow to dull orange, 5–6 mm.</text>
      <biological_entity constraint="stigma" id="o8667" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s17" to="14" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s17" to="dull orange" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits indehiscent, brilliant red [green], ovoid, obovoid, or spheric, 15–25 × 10–20 mm, very succulent, conspicuously scaly, spineless;</text>
      <biological_entity id="o8668" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s18" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="reflectance" src="d0_s18" value="brilliant" value_original="brilliant" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s18" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s18" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s18" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="very" name="texture" src="d0_s18" value="succulent" value_original="succulent" />
        <character is_modifier="false" modifier="conspicuously" name="architecture_or_pubescence" src="d0_s18" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="spineless" value_original="spineless" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pulp brilliant red [white];</text>
      <biological_entity id="o8669" name="pulp" name_original="pulp" src="d0_s19" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s19" value="brilliant" value_original="brilliant" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>floral remnant strongly persistent.</text>
      <biological_entity constraint="floral" id="o8670" name="remnant" name_original="remnant" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="strongly" name="duration" src="d0_s20" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds black, often slightly curved, obovoid to pyriform, 1.3–1.5 × 0.8–1 mm;</text>
      <biological_entity id="o8671" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="black" value_original="black" />
        <character is_modifier="false" modifier="often slightly" name="course" src="d0_s21" value="curved" value_original="curved" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s21" to="pyriform" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s21" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s21" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>testa cells convex.</text>
    </statement>
    <statement id="d0_s23">
      <text>x = 11.</text>
      <biological_entity constraint="testa" id="o8672" name="cell" name_original="cells" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="x" id="o8673" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Arid regions, sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Arid regions" establishment_means="native" />
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <discussion>Species 1–3 (1 in the flora).</discussion>
  <discussion>Generic affinities of Glandulicactus have been the subject of much taxonomic disagreement. Glandulicactus has been associated with Echinocactus, Echinomastus, Ferocactus, Hamatocactus, Thelocactus, Ancistrocactus, and Sclerocactus. Chloroplast DNA evidence (C. A. Butterworth et al. 2002) supports a close relationship to Echinocactus, Ferocactus, and Thelocactus, but a quite distant relationship from Sclerocactus. Glandulicactus is the only genus of subfam. Cactoideae in the flora with strongly hooked, abaxial radial spines, conspicuous even on immature plants two centimeters in diameter.</discussion>
  <references>
    <reference>Unger, G. 1982. Glandulicactus Backeberg. Kakteen And. Sukk. 33: 246–248.</reference>
  </references>
  
</bio:treatment>