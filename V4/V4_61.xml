<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">34</other_info_on_meta>
    <other_info_on_meta type="treatment_page">35</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">acleisanthes</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">longiflora</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts, ser.</publication_title>
      <place_in_publication>2, 15: 260. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus acleisanthes;species longiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415033</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants herbaceous, often slightly woody at base, overall pubescence of white, capitate hairs 0.2–0.4 mm.</text>
      <biological_entity id="o18596" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s0" value="herbaceous" value_original="herbaceous" />
        <character constraint="at base" constraintid="o18597" is_modifier="false" modifier="often slightly" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18597" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o18598" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s0" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="pubescence" src="d0_s0" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to prostrate or sprawling, profusely branched, to 100 cm, puberulent to glabrate, occasionally hirtellous.</text>
      <biological_entity id="o18599" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" modifier="profusely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s1" to="glabrate" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s1" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves grayish green, petiolate, those of pair slightly unequal;</text>
      <biological_entity id="o18600" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish green" value_original="grayish green" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s2" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o18601" name="pair" name_original="pair" src="d0_s2" type="structure" />
      <relation from="o18600" id="r2686" name="part_of" negation="false" src="d0_s2" to="o18601" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–14 mm, puberulent to glabrate;</text>
      <biological_entity id="o18602" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="14" to_unit="mm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s3" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade lanceolate to linear-lanceolate or triangular-lanceolate to deltate, 3–40 × 1–30 mm, base cuneate and decurrent, margins undulate or crispate, apex acuminate to acute or long attenuate, glaucous, puberulent to glabrate.</text>
      <biological_entity id="o18603" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear-lanceolate or triangular-lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18604" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o18605" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crispate" value_original="crispate" />
      </biological_entity>
      <biological_entity id="o18606" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="acute" />
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s4" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers, rarely geminate, sessile or with pedicel to 7 mm;</text>
      <biological_entity id="o18607" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="rarely" name="arrangement" notes="" src="d0_s5" value="geminate" value_original="geminate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="with pedicel" value_original="with pedicel" />
      </biological_entity>
      <biological_entity id="o18608" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o18609" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
      <relation from="o18607" id="r2687" name="with" negation="false" src="d0_s5" to="o18609" />
    </statement>
    <statement id="d0_s6">
      <text>bracts linear-subulate, 1–7 mm, puberulent to sparsely so.</text>
      <biological_entity id="o18610" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: chasmogamous perianth with tubes 7–17 cm × 1–2 mm, puberulent to glabrate, limbs 10–20 mm diam., stamens 5;</text>
      <biological_entity id="o18611" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18612" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="chasmogamous" value_original="chasmogamous" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s7" to="glabrate" />
      </biological_entity>
      <biological_entity id="o18613" name="tube" name_original="tubes" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s7" to="17" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18614" name="limb" name_original="limbs" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18615" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
      <relation from="o18612" id="r2688" name="with" negation="false" src="d0_s7" to="o18613" />
    </statement>
    <statement id="d0_s8">
      <text>cleistogamous perianth 5–12 mm, puberulent, stamens 5.</text>
      <biological_entity id="o18616" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18617" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="cleistogamous" value_original="cleistogamous" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o18618" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits with 5 hyaline ridges and pair of shallow, parallel grooves between ridges, without resinous glands, narrowly oblong, truncated at both ends, constricted 1 mm both below apex and above base, 6–10 mm, hirtellous to puberulent with capitate hairs and many to few, minute, moniliform hairs 0.2–0.4 mm, or glabrate.</text>
      <biological_entity constraint="between ridges" constraintid="o18622" id="o18619" name="fruit" name_original="fruits" src="d0_s9" type="structure" constraint_original="between  ridges, ">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s9" value="oblong" value_original="oblong" />
        <character constraint="at ends" constraintid="o18624" is_modifier="false" name="shape" src="d0_s9" value="truncated" value_original="truncated" />
        <character constraint="below apex" constraintid="o18625" name="some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" constraint="with hairs" constraintid="o18627" from="hirtellous" name="pubescence" src="d0_s9" to="puberulent" />
        <character is_modifier="false" name="size" src="d0_s9" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o18620" name="ridge" name_original="ridges" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o18621" name="groove" name_original="grooves" src="d0_s9" type="structure">
        <character is_modifier="true" name="depth" src="d0_s9" value="shallow" value_original="shallow" />
        <character is_modifier="true" name="arrangement" src="d0_s9" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o18622" name="ridge" name_original="ridges" src="d0_s9" type="structure" />
      <biological_entity id="o18623" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="true" name="coating" src="d0_s9" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o18624" name="end" name_original="ends" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o18625" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o18626" name="base" name_original="base" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18627" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s9" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="many" name="quantity" notes="" src="d0_s9" to="few" />
      </biological_entity>
      <biological_entity id="o18628" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="moniliform" value_original="moniliform" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <relation from="o18619" id="r2689" name="with" negation="false" src="d0_s9" to="o18620" />
      <relation from="o18619" id="r2690" name="part_of" negation="false" src="d0_s9" to="o18621" />
      <relation from="o18619" id="r2691" name="without" negation="false" src="d0_s9" to="o18623" />
      <relation from="o18619" id="r2692" name="above" negation="false" src="d0_s9" to="o18626" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, gravelly, loamy, or sandy calcareous, gypseous, or igneous-derived soils in deserts, grasslands, shrublands, or woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" modifier="rocky" />
        <character name="habitat" value="sandy calcareous" modifier="loamy" />
        <character name="habitat" value="gypseous" />
        <character name="habitat" value="igneous-derived soils" constraint="in deserts , grasslands , shrublands , or woodlands" />
        <character name="habitat" value="deserts" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Durango, Nuevo León, Sonora, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Angel trumpets</other_name>
  <other_name type="common_name">yerba de la rabia</other_name>
  
</bio:treatment>