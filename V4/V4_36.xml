<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">boerhavia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">pterocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 376. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus boerhavia;species pterocarpa</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415015</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
      <biological_entity id="o18175" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot tapered, soft or ± woody.</text>
      <biological_entity id="o18176" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="texture" src="d0_s1" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems procumbent or decumbent to ascending, sparingly branched throughout, 1–4 dm, minutely puberulent with bent hairs throughout.</text>
      <biological_entity id="o18177" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="procumbent" value_original="procumbent" />
        <character name="growth_form" src="d0_s2" value="decumbent to ascending" value_original="decumbent to ascending" />
        <character is_modifier="false" modifier="sparingly; throughout" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="4" to_unit="dm" />
        <character constraint="with hairs" constraintid="o18178" is_modifier="false" modifier="minutely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o18178" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="bent" value_original="bent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ± throughout;</text>
      <biological_entity id="o18179" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>larger leaves with petiole 3–12 mm, blade rhombic-ovate to ovate or lanceolate, 15–25 × 9–15 mm (distal leaves smaller, proportionately narrower), adaxial surface glabrous or sparsely puberulent, abaxial surface paler than adaxial, glabrous, neither surface punctate, base round to obtuse, margins entire or slightly sinuate, apex acute.</text>
      <biological_entity constraint="larger" id="o18180" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o18181" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18182" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="rhombic-ovate" name="shape" src="d0_s4" to="ovate or lanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18183" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18184" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="than adaxial surface" constraintid="o18185" is_modifier="false" name="coloration" src="d0_s4" value="paler" value_original="paler" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18185" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o18186" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s4" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity id="o18187" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o18188" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o18189" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o18180" id="r2612" name="with" negation="false" src="d0_s4" to="o18181" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal or axillary, without sticky internodal bands;</text>
      <biological_entity id="o18190" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o18191" name="band" name_original="bands" src="d0_s5" type="structure">
        <character is_modifier="true" name="texture" src="d0_s5" value="sticky" value_original="sticky" />
      </biological_entity>
      <relation from="o18190" id="r2613" name="without" negation="false" src="d0_s5" to="o18191" />
    </statement>
    <statement id="d0_s6">
      <text>peduncle, 1–3 cm, bearing small capitate clusters of flowers.</text>
      <biological_entity id="o18192" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18193" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="small" value_original="small" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="capitate" value_original="capitate" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
      <relation from="o18192" id="r2614" name="bearing" negation="false" src="d0_s6" to="o18193" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: pedicel 0.3–0.6 mm;</text>
      <biological_entity id="o18194" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18195" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts at base of perianth quickly deciduous, lance-acuminate, 0.4–0.7 mm;</text>
      <biological_entity id="o18196" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18197" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="lance-acuminate" value_original="lance-acuminate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18198" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="quickly" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o18199" name="perianth" name_original="perianth" src="d0_s8" type="structure" />
      <relation from="o18197" id="r2615" name="at" negation="false" src="d0_s8" to="o18198" />
      <relation from="o18198" id="r2616" name="part_of" negation="false" src="d0_s8" to="o18199" />
    </statement>
    <statement id="d0_s9">
      <text>perianth white to pale pinkish, campanulate distal to constriction, 1–1.5 mm;</text>
      <biological_entity id="o18200" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18201" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pale pinkish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character constraint="to constriction" constraintid="o18202" is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18202" name="constriction" name_original="constriction" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 2, included or barely exserted.</text>
      <biological_entity id="o18203" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18204" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character is_modifier="false" modifier="barely" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits 2–8 per cluster, pale green to straw colored, broadly obpyramidal, base tapered to stipelike above pedicel, 2.9–3.4 × 2.8–3.2 mm (l/w: 1–1.4), apex truncate, glabrous;</text>
      <biological_entity id="o18205" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per base" constraintid="o18206" from="2" name="quantity" src="d0_s11" to="8" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="length" notes="" src="d0_s11" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" notes="" src="d0_s11" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18206" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="pale green" is_modifier="true" name="coloration" src="d0_s11" to="straw colored" />
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s11" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" constraint="above pedicel" constraintid="o18207" from="tapered" name="shape" src="d0_s11" to="stipelike" />
      </biological_entity>
      <biological_entity id="o18207" name="pedicel" name_original="pedicel" src="d0_s11" type="structure" />
      <biological_entity id="o18208" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ribs 3–4, winglike, smooth;</text>
      <biological_entity id="o18209" name="rib" name_original="ribs" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="4" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="winglike" value_original="winglike" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sulci 3–4 times as wide as base of ribs, coarsely transversely rugose, not papillate.</text>
      <biological_entity id="o18210" name="sulcus" name_original="sulci" src="d0_s13" type="structure">
        <character constraint="of ribs" constraintid="o18212" is_modifier="false" name="width" src="d0_s13" value="3-4 times as wide as base" />
        <character is_modifier="false" modifier="coarsely transversely" name="relief" notes="" src="d0_s13" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o18211" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o18212" name="rib" name_original="ribs" src="d0_s13" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy loam to clay soils, disturbed areas, occasionally a weed in ornamental beds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy loam" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="a weed" modifier="occasionally" constraint="in ornamental beds" />
        <character name="habitat" value="ornamental beds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="past_name">Boerhaavia</other_name>
  
</bio:treatment>