<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="treatment_page">115</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="(Engelmann) F. M. Knuth in C. Backeberg and F. M. Knuth" date="1935" rank="genus">cylindropuntia</taxon_name>
    <taxon_name authority="(C. B. Wolf) Backeberg" date="unknown" rank="species">munzii</taxon_name>
    <place_of_publication>
      <publication_title>Kakteenlexikon,</publication_title>
      <place_in_publication>113. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus cylindropuntia;species munzii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415163</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="C. B. Wolf" date="unknown" rank="species">munzii</taxon_name>
    <place_of_publication>
      <publication_title>Occas. Pap. Rancho Santa Ana Bot. Gard.</publication_title>
      <place_in_publication>2: 79, fig. 23. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Opuntia;species munzii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or treelike shrubs, with trunk (s) and main branches spreading, bearing terminal tufts of usually drooping branchlets, 2–4 m.</text>
      <biological_entity id="o875" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="tree" />
        <character is_modifier="true" name="growth_form" src="d0_s0" value="arboreous" value_original="treelike" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o877" name="trunk" name_original="trunk" src="d0_s0" type="structure" />
      <biological_entity constraint="main" id="o878" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o879" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <biological_entity id="o880" name="branchlet" name_original="branchlets" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="usually" name="orientation" src="d0_s0" value="drooping" value_original="drooping" />
      </biological_entity>
      <relation from="o875" id="r106" name="with" negation="false" src="d0_s0" to="o877" />
      <relation from="o875" id="r107" name="with" negation="false" src="d0_s0" to="o877" />
      <relation from="o875" id="r108" name="with" negation="false" src="d0_s0" to="o878" />
      <relation from="o875" id="r109" name="with" negation="false" src="d0_s0" to="o878" />
      <relation from="o875" id="r110" name="bearing" negation="false" src="d0_s0" to="o879" />
      <relation from="o875" id="r111" name="bearing" negation="false" src="d0_s0" to="o879" />
      <relation from="o875" id="r112" name="part_of" negation="false" src="d0_s0" to="o880" />
      <relation from="o875" id="r113" name="part_of" negation="false" src="d0_s0" to="o880" />
    </statement>
    <statement id="d0_s1">
      <text>Stem segments easily detached, gray-green, 4–16 × 1.2–2.5 cm;</text>
      <biological_entity constraint="stem" id="o881" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="easily" name="fusion" src="d0_s1" value="detached" value_original="detached" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s1" to="16" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tubercles prominent, narrowly oval, 1–2 cm;</text>
      <biological_entity id="o882" name="tubercle" name_original="tubercles" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="oval" value_original="oval" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles subcircular to obdeltate, 5–7 × 3–4.5 mm;</text>
      <biological_entity id="o883" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="subcircular" name="shape" src="d0_s3" to="obdeltate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>wool tawny to yellowbrown, aging gray.</text>
      <biological_entity id="o884" name="wool" name_original="wool" src="d0_s4" type="structure">
        <character char_type="range_value" from="tawny" name="coloration" src="d0_s4" to="yellowbrown" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spines (7–) 9–14 per areole, at most areoles, erect or spreading, yellow, aging red to gray-black with pale tips, terete or abaxial ones slightly flattened, the longest to 3 cm;</text>
      <biological_entity id="o885" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s5" to="9" to_inclusive="false" />
        <character char_type="range_value" constraint="per areole" constraintid="o886" from="9" name="quantity" src="d0_s5" to="14" />
      </biological_entity>
      <biological_entity id="o886" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity id="o887" name="0-areole" name_original="0-areoles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="aging" value_original="aging" />
        <character constraint="with tips" constraintid="o888" is_modifier="false" name="coloration" src="d0_s5" value="red to gray-black" value_original="red to gray-black" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o888" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o889" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="length" src="d0_s5" value="longest" value_original="longest" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>marginal bristlelike spines shorter, 0–4;</text>
      <biological_entity constraint="marginal" id="o890" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="bristlelike" value_original="bristlelike" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sheaths pale-yellow becoming golden apically, baggy.</text>
      <biological_entity id="o891" name="sheath" name_original="sheaths" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="becoming apically; apically" name="coloration" src="d0_s7" value="pale-yellow becoming golden" value_original="pale-yellow becoming golden" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glochids in adaxial crescent and sometimes small tuft, yellow, inconspicuous, 0.5–2 mm.</text>
      <biological_entity id="o892" name="glochid" name_original="glochids" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o893" name="tuft" name_original="tuft" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="adaxial" value_original="adaxial" />
        <character is_modifier="true" name="shape" src="d0_s8" value="crescent" value_original="crescent" />
        <character is_modifier="true" modifier="sometimes" name="size" src="d0_s8" value="small" value_original="small" />
      </biological_entity>
      <relation from="o892" id="r114" name="in" negation="false" src="d0_s8" to="o893" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: inner tepals pale reddish maroon-brown, elliptic, of irregular lengths, 8–15 mm, emarginate-apiculate;</text>
      <biological_entity id="o894" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="inner" id="o895" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale reddish" value_original="pale reddish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="maroon-brown" value_original="maroon-brown" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="8" from_unit="mm" modifier="of irregular lengths" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="emarginate-apiculate" value_original="emarginate-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments green;</text>
      <biological_entity id="o896" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o897" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o898" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o899" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style and stigma lobes cream.</text>
      <biological_entity id="o900" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o901" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="cream" value_original="cream" />
      </biological_entity>
      <biological_entity constraint="stigma" id="o902" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="cream" value_original="cream" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits easily detached, tan when mature and fertile, globose, 17–24 × 17–21 mm, dry, tuberculate, spineless but bearing numerous very long glochids;</text>
      <biological_entity id="o903" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="easily" name="fusion" src="d0_s13" value="detached" value_original="detached" />
        <character is_modifier="false" modifier="when mature and fertile" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s13" to="24" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="width" src="d0_s13" to="21" to_unit="mm" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s13" value="dry" value_original="dry" />
        <character is_modifier="false" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="spineless" value_original="spineless" />
      </biological_entity>
      <biological_entity id="o904" name="glochid" name_original="glochids" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="numerous" value_original="numerous" />
        <character is_modifier="true" modifier="very" name="length_or_size" src="d0_s13" value="long" value_original="long" />
      </biological_entity>
      <relation from="o903" id="r115" name="bearing" negation="false" src="d0_s13" to="o904" />
    </statement>
    <statement id="d0_s14">
      <text>basal tubercles not markedly longer than distal ones (except in sterile fruits);</text>
      <biological_entity constraint="basal" id="o905" name="tubercle" name_original="tubercles" src="d0_s14" type="structure">
        <character constraint="than distal tubercles" constraintid="o906" is_modifier="false" name="length_or_size" src="d0_s14" value="not markedly longer" value_original="not markedly longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o906" name="tubercle" name_original="tubercles" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>areoles 30–40.</text>
      <biological_entity id="o907" name="areole" name_original="areoles" src="d0_s15" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s15" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds pale-yellow, rounded-deltoid, slightly flattened, 3–4.5 × 3–4 mm, sides smooth;</text>
      <biological_entity id="o908" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="shape" src="d0_s16" value="rounded-deltoid" value_original="rounded-deltoid" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s16" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o909" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>girdle smooth, not protruding.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 22, 33.</text>
      <biological_entity id="o910" name="girdle" name_original="girdle" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s17" value="protruding" value_original="protruding" />
      </biological_entity>
      <biological_entity constraint="2n" id="o911" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="22" value_original="22" />
        <character name="quantity" src="d0_s18" value="33" value_original="33" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sonoran Desert, flats, hills, sandy to rocky soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="flats" />
        <character name="habitat" value="hills" />
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="desert" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="past_name">hybrid</other_name>
  <other_name type="common_name">Munz cholla</other_name>
  <discussion>Cylindropuntia munzii hybridizes with C. echinocarpa forming a shrub to 1.5 m, with shorter tubercles and baggy-sheathed spines obscuring the stem and bearing fruits with long glochids but no spines. The chromosome number for the hybrid is 2n = 22.</discussion>
  
</bio:treatment>