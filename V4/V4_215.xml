<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="treatment_page">113</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="(Engelmann) F. M. Knuth in C. Backeberg and F. M. Knuth" date="1935" rank="genus">cylindropuntia</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) F. M. Knuth in C. Backeberg and F. M. Knuth" date="1935" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>in C. Backeberg and F. M. Knuth, Kaktus-ABC,</publication_title>
      <place_in_publication>125. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus cylindropuntia;species californica;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415159</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cereus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">californicus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 555. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cereus;species californicus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Coville" date="unknown" rank="species">californica</taxon_name>
    <place_of_publication>
      <place_in_publication>1899</place_in_publication>
      <other_info_on_pub>not Engelmann 1848 (provisional name)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Opuntia;species californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, decumbent to erect, essentially trunkless, 0.3–2.5 m.</text>
      <biological_entity id="o14863" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s0" to="erect" />
        <character is_modifier="false" modifier="essentially" name="architecture" src="d0_s0" value="trunkless" value_original="trunkless" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem segments green to purplish green, 6–30 × 1.5–2.5 cm;</text>
      <biological_entity constraint="stem" id="o14864" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="purplish green" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tubercles prominent, oval to elongate-elliptic, 0.7–3 cm, sometimes appearing as ribs;</text>
      <biological_entity id="o14865" name="tubercle" name_original="tubercles" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s2" to="elongate-elliptic" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14866" name="rib" name_original="ribs" src="d0_s2" type="structure" />
      <relation from="o14865" id="r2157" modifier="sometimes" name="appearing as" negation="false" src="d0_s2" to="o14866" />
    </statement>
    <statement id="d0_s3">
      <text>areoles elliptic, 4.5–5 × 3–3.5 mm;</text>
      <biological_entity id="o14867" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>wool tan, aging dark gray.</text>
      <biological_entity id="o14868" name="wool" name_original="wool" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="tan" value_original="tan" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark gray" value_original="dark gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spines (0–) 7–13 (–20) per areole, at most areoles, rarely obscuring stems, yellow to orangebrown with yellow tips, aging dark gray, subequal or 1–6 (–7) much longer to 3 cm;</text>
      <biological_entity id="o14869" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s5" to="7" to_inclusive="false" />
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="20" />
        <character char_type="range_value" constraint="per areole" constraintid="o14870" from="7" name="quantity" src="d0_s5" to="13" />
      </biological_entity>
      <biological_entity id="o14870" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity id="o14871" name="0-areole" name_original="0-areoles" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="with tips" constraintid="o14873" from="yellow" modifier="rarely" name="coloration" src="d0_s5" to="orangebrown" />
        <character is_modifier="false" name="life_cycle" notes="" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark gray" value_original="dark gray" />
        <character is_modifier="false" name="size" src="d0_s5" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="7" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="false" modifier="much" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14872" name="stem" name_original="stems" src="d0_s5" type="structure" />
      <biological_entity id="o14873" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
      <relation from="o14871" id="r2158" modifier="rarely" name="obscuring" negation="false" src="d0_s5" to="o14872" />
    </statement>
    <statement id="d0_s6">
      <text>sheaths whitish or gold to brown, moderately loose.</text>
      <biological_entity id="o14874" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character char_type="range_value" from="gold" name="coloration" src="d0_s6" to="brown" />
        <character is_modifier="false" modifier="moderately" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Glochids yellow-tan to redbrown in dense adaxial crescent and scattered, stouter ones at basal margins.</text>
      <biological_entity id="o14875" name="glochid" name_original="glochids" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="at basal margins" constraintid="o14876" from="yellow-tan" name="coloration" src="d0_s7" to="redbrown" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14876" name="margin" name_original="margins" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: inner tepals yellow to greenish yellow;</text>
      <biological_entity id="o14877" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="inner" id="o14878" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="greenish yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer ones tipped red, 15–30 mm;</text>
      <biological_entity id="o14879" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="outer" value_original="outer" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="tipped red" value_original="tipped red" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments green or yellow;</text>
      <biological_entity id="o14880" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14881" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o14882" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14883" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style light green or cream-white (rarely pinkish) apically;</text>
      <biological_entity id="o14884" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14885" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="apically" name="coloration" src="d0_s12" value="cream-white" value_original="cream-white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma lobes cream-white to yellow.</text>
      <biological_entity id="o14886" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="stigma" id="o14887" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="cream-white" name="coloration" src="d0_s13" to="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits green to yellowish, drying gray-tan, short top-shaped to subspheric, 15–32 × 13–26 mm, leathery-dry, tuberculate, few-spined or spineless;</text>
      <biological_entity id="o14888" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s14" to="yellowish" />
        <character is_modifier="false" name="condition" src="d0_s14" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="gray-tan" value_original="gray-tan" />
        <character is_modifier="false" name="shape" src="d0_s14" value="short" value_original="short" />
        <character char_type="range_value" from="top-shaped" name="shape" src="d0_s14" to="subspheric" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s14" to="32" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s14" to="26" to_unit="mm" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s14" value="leathery-dry" value_original="leathery-dry" />
        <character is_modifier="false" name="relief" src="d0_s14" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="few-spined" value_original="few-spined" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="spineless" value_original="spineless" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>proximal tubercles longest;</text>
      <biological_entity constraint="proximal" id="o14889" name="tubercle" name_original="tubercles" src="d0_s15" type="structure">
        <character is_modifier="false" name="length" src="d0_s15" value="longest" value_original="longest" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>umbilicus deep;</text>
      <biological_entity id="o14890" name="umbilicus" name_original="umbilicus" src="d0_s16" type="structure">
        <character is_modifier="false" name="depth" src="d0_s16" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>rim slightly flanged;</text>
      <biological_entity id="o14891" name="rim" name_original="rim" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="flanged" value_original="flanged" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>areoles 13–30.</text>
      <biological_entity id="o14892" name="areole" name_original="areoles" src="d0_s18" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s18" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds tan, squarish to angular in outline, warped, (4–) 5–7 × 4–5 mm, sides with 2–4 deep depressions;</text>
      <biological_entity id="o14893" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="tan" value_original="tan" />
        <character char_type="range_value" constraint="in outline" constraintid="o14894" from="squarish" name="shape" src="d0_s19" to="angular" />
        <character is_modifier="false" name="shape" notes="" src="d0_s19" value="warped" value_original="warped" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s19" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s19" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s19" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14894" name="outline" name_original="outline" src="d0_s19" type="structure" />
      <biological_entity id="o14895" name="side" name_original="sides" src="d0_s19" type="structure" />
      <biological_entity id="o14896" name="depression" name_original="depressions" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s19" to="4" />
        <character is_modifier="true" name="depth" src="d0_s19" value="deep" value_original="deep" />
      </biological_entity>
      <relation from="o14895" id="r2159" name="with" negation="false" src="d0_s19" to="o14896" />
    </statement>
    <statement id="d0_s20">
      <text>girdle smooth.</text>
      <biological_entity id="o14897" name="girdle" name_original="girdle" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">California cholla</other_name>
  <discussion>Varieties 4 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs, suberect to decumbent; stem tubercles oval, shorter, usually 0.7-1.5 cm; spines/areole usually subequal</description>
      <determination>15a Cylindropuntia californica var. californ</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs, suberect to erect; stem tubercles elliptic, longer, usually 1.5-3 cm; spines/areole commonly with 1(-3) central spines much longer</description>
      <determination>15b Cylindropuntia californica var. parkeri</determination>
    </key_statement>
  </key>
</bio:treatment>