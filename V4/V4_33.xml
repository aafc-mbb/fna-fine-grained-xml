<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">boerhavia</taxon_name>
    <taxon_name authority="M. E. Jones" date="1902" rank="species">intermedia</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>10: 41, unnumbered fig. at end of is sue. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus boerhavia;species intermedia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415012</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Boerhavia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">erecta</taxon_name>
    <taxon_name authority="(M. E. Jones) Kearney &amp; Peebles" date="unknown" rank="variety">intermedia</taxon_name>
    <taxon_hierarchy>genus Boerhavia;species erecta;variety intermedia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
      <biological_entity id="o20443" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot tapered, soft or ± woody.</text>
      <biological_entity id="o20444" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="texture" src="d0_s1" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually erect or ascending, occasionally decumbent, moderately or profusely branched primarily distally, 2–6 (–8) dm, minutely puberulent with bent hairs basally, glabrous or minutely puberulent distally.</text>
      <biological_entity id="o20445" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="occasionally" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="moderately; profusely; primarily distally; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="8" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="6" to_unit="dm" />
        <character constraint="with hairs" constraintid="o20446" is_modifier="false" modifier="minutely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely; distally" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o20446" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="bent" value_original="bent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly in basal 1/2 of plant;</text>
      <biological_entity id="o20447" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o20448" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <biological_entity id="o20449" name="plant" name_original="plant" src="d0_s3" type="structure" />
      <relation from="o20447" id="r2978" name="in" negation="false" src="d0_s3" to="o20448" />
      <relation from="o20448" id="r2979" name="part_of" negation="false" src="d0_s3" to="o20449" />
    </statement>
    <statement id="d0_s4">
      <text>larger leaves with petiole 7–25 mm, blade broadly ovate or oval to lanceolate, 20–45 × 7–16 mm (distal leaves smaller, sometimes longer, proportionately narrower), adaxial surface usually glabrous, occasionally glandular-puberulent, often minutely punctate, abaxial surface paler than adaxial, glabrous or glabrate, usually punctate with small patches of reddish or brownish cells, base round, obtuse, or truncate, margins entire or slightly sinuate, apex acute, obtuse, or round.</text>
      <biological_entity constraint="larger" id="o20450" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o20451" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20452" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oval" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20453" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" modifier="often minutely" name="coloration_or_relief" src="d0_s4" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20454" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="than adaxial surface" constraintid="o20455" is_modifier="false" name="coloration" src="d0_s4" value="paler" value_original="paler" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character constraint="with patches" constraintid="o20456" is_modifier="false" modifier="usually" name="coloration_or_relief" src="d0_s4" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20455" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o20456" name="patch" name_original="patches" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o20457" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="reddish" value_original="reddish" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o20458" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o20459" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o20460" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
      </biological_entity>
      <relation from="o20450" id="r2980" name="with" negation="false" src="d0_s4" to="o20451" />
      <relation from="o20456" id="r2981" name="part_of" negation="false" src="d0_s4" to="o20457" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, forked ca. 3–6 times ± evenly (or clearly unevenly), diffuse, usually with sticky internodal bands;</text>
      <biological_entity id="o20461" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s5" value="forked" value_original="forked" />
        <character constraint="band" constraintid="o20462" is_modifier="false" modifier="more or less evenly; evenly" name="size_or_quantity" src="d0_s5" value="3-6 times diffuse with sticky internodal bands" />
        <character constraint="band" constraintid="o20463" is_modifier="false" name="size_or_quantity" src="d0_s5" value="3-6 times diffuse with sticky internodal bands" />
        <character constraint="band" constraintid="o20464" is_modifier="false" name="size_or_quantity" src="d0_s5" value="3-6 times diffuse with sticky internodal bands" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o20462" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="internodal" id="o20463" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="internodal" id="o20464" name="band" name_original="bands" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches strongly ascending, terminating in umbels or flowers borne singly, occasionally subumbellate (all pedicels not attaching at same point), rarely irregularly compound umbels.</text>
      <biological_entity id="o20465" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o20467" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o20468" name="umbel" name_original="umbels" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="true" modifier="occasionally" name="architecture" src="d0_s6" value="subumbellate" value_original="subumbellate" />
        <character is_modifier="true" modifier="rarely irregularly" name="architecture" src="d0_s6" value="compound" value_original="compound" />
      </biological_entity>
      <relation from="o20465" id="r2982" name="terminating in" negation="false" src="d0_s6" to="o20467" />
      <relation from="o20465" id="r2983" name="terminating in" negation="false" src="d0_s6" to="o20468" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: pedicel 0.5–3.2 mm;</text>
      <biological_entity id="o20469" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o20470" name="pedicel" name_original="pedicel" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts at base of perianth quickly deciduous, (1–) 2, narrowly lance-acuminate, 0.5–1 mm;</text>
      <biological_entity id="o20471" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o20472" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s8" value="lance-acuminate" value_original="lance-acuminate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20473" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="quickly" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s8" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o20474" name="perianth" name_original="perianth" src="d0_s8" type="structure" />
      <relation from="o20472" id="r2984" name="at" negation="false" src="d0_s8" to="o20473" />
      <relation from="o20473" id="r2985" name="part_of" negation="false" src="d0_s8" to="o20474" />
    </statement>
    <statement id="d0_s9">
      <text>perianth whitish to pale-pink or purplish, campanulate distal to constriction, 0.7–1.2 [–2] mm;</text>
      <biological_entity id="o20475" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o20476" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s9" to="pale-pink or purplish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character constraint="to constriction" constraintid="o20477" is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20477" name="constriction" name_original="constriction" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 2–3, included or barely exserted.</text>
      <biological_entity id="o20478" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o20479" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="3" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character is_modifier="false" modifier="barely" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits 1–15 per cluster, straw colored or gray-brown, obconic, broadly low conic, 2–2.8 (–3.2) × 0.7–1.3 mm (l/w: 1.7–3.2), apex nearly truncate, glabrous;</text>
      <biological_entity id="o20480" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per cluster" from="1" name="quantity" src="d0_s11" to="15" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="straw colored" value_original="straw colored" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obconic" value_original="obconic" />
        <character is_modifier="false" modifier="broadly" name="position" src="d0_s11" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s11" value="conic" value_original="conic" />
        <character char_type="range_value" from="2.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20481" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ribs (4–) 5, acute, slightly rugose or undulate near sulci;</text>
      <biological_entity id="o20482" name="rib" name_original="ribs" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s12" value="rugose" value_original="rugose" />
        <character constraint="near sulci" constraintid="o20483" is_modifier="false" name="shape" src="d0_s12" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o20483" name="sulcus" name_original="sulci" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>sulci 0.2–1 times as wide as base of ribs, coarsely transversely rugose, smooth or very faintly papillate.</text>
      <biological_entity id="o20485" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o20486" name="rib" name_original="ribs" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 52, ca. 54.</text>
      <biological_entity id="o20484" name="sulcus" name_original="sulci" src="d0_s13" type="structure">
        <character constraint="of ribs" constraintid="o20486" is_modifier="false" name="width" src="d0_s13" value="0.2-1 times as wide as base" />
        <character is_modifier="false" modifier="coarsely transversely" name="relief" notes="" src="d0_s13" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="very faintly" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20487" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="52" value_original="52" />
        <character name="quantity" src="d0_s14" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly areas in deserts and arid grasslands, disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" constraint="in deserts and arid grasslands , disturbed areas" />
        <character name="habitat" value="gravelly areas" constraint="in deserts and arid grasslands , disturbed areas" />
        <character name="habitat" value="deserts" />
        <character name="habitat" value="arid grasslands" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>[0-]100-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="100" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., N.Mex., Tex.; Mexico (Baja California, Baja California Sur, Chihuahua, Coahuila, Durango, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Boerhavia intermedia is a wide-ranging and variable species of the arid areas of southwestern North America. Ordinarily, the terminal inflorescence is an umbel of at least a few flowers. Plants with few-flowered umbels often have only one flower at some of the terminal inflorescences. Plants with predominantly or entirely one-flowered terminal inflorescences occasionally occur in the eastern part of the range. To the west, and especially on the Coloradan portion of the Sonoran Desert and on the Baja California peninsula, plants with one-flowered terminal inflorescences are more frequent, and even though those have five-ribbed fruits, they often have been identified as B. triquetra. In that region, such plants may have proportionately broader fruits as the ribs become more winglike. Some plants in southern California bear a few fruits with four angles, and in this respect are intermediate to B. triquetra.</discussion>
  
</bio:treatment>