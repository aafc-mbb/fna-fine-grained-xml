<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">252</other_info_on_meta>
    <other_info_on_meta type="treatment_page">253</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Haworth" date="unknown" rank="genus">mammillaria</taxon_name>
    <taxon_name authority="K. Brandegee" date="unknown" rank="species">mainiae</taxon_name>
    <place_of_publication>
      <publication_title>Zoë</publication_title>
      <place_in_publication>5: 31. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus mammillaria;species mainiae;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415359</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants branched or unbranched;</text>
      <biological_entity id="o12850" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches 0–several, not numerous, seldom rooting.</text>
      <biological_entity id="o12851" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character is_modifier="false" modifier="not" name="quantity" src="d0_s1" value="numerous" value_original="numerous" />
        <character is_modifier="false" modifier="seldom" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Roots diffuse, upper portion not enlarged.</text>
      <biological_entity id="o12852" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="density" src="d0_s2" value="diffuse" value_original="diffuse" />
      </biological_entity>
      <biological_entity constraint="upper" id="o12853" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s2" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems nearly spheric, 2.5–12 × 3.5–7 cm, firm;</text>
      <biological_entity id="o12854" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s3" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s3" to="7" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>tubercles 9–10.5 (–18) × 3–9 mm;</text>
      <biological_entity id="o12855" name="tubercle" name_original="tubercles" src="d0_s4" type="structure">
        <character char_type="range_value" from="10.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="18" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s4" to="10.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>axils appearing naked;</text>
      <biological_entity id="o12856" name="axil" name_original="axils" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cortex and pith not mucilaginous;</text>
      <biological_entity id="o12857" name="cortex" name_original="cortex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o12858" name="pith" name_original="pith" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>latex absent.</text>
      <biological_entity id="o12859" name="latex" name_original="latex" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spines ca. 9–16 per areole, brightly colored than M. grahamii, yellowish, pale pinkish tan, or brown (smaller spines paler), tipped dark chestnut-brown to blackish, glabrous, sometimes ± pubescent when young;</text>
      <biological_entity id="o12860" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o12861" from="9" name="quantity" src="d0_s8" to="16" />
        <character constraint="than m" constraintid="" is_modifier="false" name="coloration" notes="" src="d0_s8" value="brightly colored" value_original="brightly colored" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale pinkish" value_original="pale pinkish" />
        <character char_type="range_value" from="brown tipped dark chestnut-brown" name="coloration" src="d0_s8" to="blackish" />
        <character char_type="range_value" from="brown tipped dark chestnut-brown" name="coloration" src="d0_s8" to="blackish" />
        <character char_type="range_value" from="brown tipped dark chestnut-brown" name="coloration" src="d0_s8" to="blackish" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o12861" name="areole" name_original="areole" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>radial spines (8–) 10–15 per areole, bristlelike, 6–10 (–12) × 0.13–0.23 mm, stiff;</text>
      <biological_entity id="o12862" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="radial" value_original="radial" />
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s9" to="10" to_inclusive="false" />
        <character char_type="range_value" constraint="per areole" constraintid="o12863" from="10" name="quantity" src="d0_s9" to="15" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="bristlelike" value_original="bristlelike" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.13" from_unit="mm" name="width" src="d0_s9" to="0.23" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s9" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o12863" name="areole" name_original="areole" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>central spines 1 (–2) per areole, porrect, hooked, 11–20 × 0.2–0.4 mm;</text>
      <biological_entity constraint="central" id="o12864" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="2" />
        <character constraint="per areole" constraintid="o12865" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s10" value="porrect" value_original="porrect" />
        <character is_modifier="false" name="shape" src="d0_s10" value="hooked" value_original="hooked" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12865" name="areole" name_original="areole" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>subcentral spines 0 (–2) per areole, adaxial to central spines.</text>
      <biological_entity constraint="subcentral" id="o12866" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="2" />
        <character constraint="per areole" constraintid="o12867" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12867" name="areole" name_original="areole" src="d0_s11" type="structure" />
      <biological_entity id="o12868" name="spine" name_original="spines" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers (1–) 2–3 × 1.2–2 cm;</text>
      <biological_entity id="o12869" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s12" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s12" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s12" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>outermost tepal margins densely fringed, fringes 0.4 mm;</text>
      <biological_entity constraint="tepal" id="o12870" name="margin" name_original="margins" src="d0_s13" type="structure" constraint_original="outermost tepal">
        <character is_modifier="false" modifier="densely" name="shape" src="d0_s13" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o12871" name="fringe" name_original="fringes" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>inner tepals pinkish white with sharply defined magenta midstripes;</text>
      <biological_entity constraint="inner" id="o12872" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character constraint="with midstripes" constraintid="o12873" is_modifier="false" name="coloration" src="d0_s14" value="pinkish white" value_original="pinkish white" />
      </biological_entity>
      <biological_entity id="o12873" name="midstripe" name_original="midstripes" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="sharply" name="prominence" src="d0_s14" value="defined" value_original="defined" />
        <character is_modifier="true" name="coloration" src="d0_s14" value="magenta" value_original="magenta" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma lobes bright red to red-purple, orange, or magenta, (3–) 7–9 mm.</text>
      <biological_entity constraint="stigma" id="o12874" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="bright red" name="coloration" src="d0_s15" to="red-purple orange or magenta" />
        <character char_type="range_value" from="bright red" name="coloration" src="d0_s15" to="red-purple orange or magenta" />
        <character char_type="range_value" from="bright red" name="coloration" src="d0_s15" to="red-purple orange or magenta" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits bright-orange-red, spheric to obovoid, 5–7 × 4–4.5 mm, level with or beneath spines, juicy only in fruit walls;</text>
      <biological_entity id="o12875" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="bright-orange-red" value_original="bright-orange-red" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s16" to="obovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s16" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s16" to="4.5" to_unit="mm" />
        <character constraint="with or beneath spines" constraintid="o12876" is_modifier="false" name="position_relational" src="d0_s16" value="level" value_original="level" />
        <character constraint="in fruit walls" constraintid="o12877" is_modifier="false" name="texture" notes="" src="d0_s16" value="juicy" value_original="juicy" />
      </biological_entity>
      <biological_entity id="o12876" name="spine" name_original="spines" src="d0_s16" type="structure" />
      <biological_entity constraint="fruit" id="o12877" name="wall" name_original="walls" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>floral remnant weakly persistent.</text>
      <biological_entity constraint="floral" id="o12878" name="remnant" name_original="remnant" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="weakly" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds black, 1–1.2 × 0.8–1 mm, pitted;</text>
      <biological_entity id="o12879" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="black" value_original="black" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s18" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s18" to="1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s18" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>testa hard;</text>
      <biological_entity id="o12880" name="testa" name_original="testa" src="d0_s19" type="structure">
        <character is_modifier="false" name="texture" src="d0_s19" value="hard" value_original="hard" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anticlinal cell-walls straight;</text>
      <biological_entity id="o12881" name="cell-wall" name_original="cell-walls" src="d0_s20" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s20" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="course" src="d0_s20" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>interstices conspicuously wider than pit diameters;</text>
      <biological_entity id="o12882" name="interstice" name_original="interstices" src="d0_s21" type="structure">
        <character constraint="than pit" constraintid="o12883" is_modifier="false" name="width" src="d0_s21" value="conspicuously wider" value_original="conspicuously wider" />
      </biological_entity>
      <biological_entity id="o12883" name="pit" name_original="pit" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>pits bowl-shaped.</text>
    </statement>
    <statement id="d0_s23">
      <text>2n = 22.</text>
      <biological_entity id="o12884" name="pit" name_original="pits" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="bowl--shaped" value_original="bowl--shaped" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12885" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sonoran desert, grasslands, bajadas, valleys, washes, alluvial fans [subtropical woodlands, tropical deciduous forests]</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sonoran desert" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="bajadas" />
        <character name="habitat" value="valleys" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="alluvial fans" />
        <character name="habitat" value="subtropical woodlands" />
        <character name="habitat" value="tropical deciduous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>[200-]600-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="1200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="past_name">Mamillaria mainae</other_name>
  <other_name type="common_name">Counterclockwise fishhook cactus</other_name>
  <discussion>The tendency for all spine hooks on plants to be oriented in same direction is not unique to Mammillaria mainiae. This uncommon and poorly known species is restricted in the flora area to the relatively mesic eastern edge of the Sonoran Desert, in western bajadas of the Baboquivari Mountains, Arizona. Mammillaria mainiae is not known from Nogales, Arizona, contrary to L. D. Benson (1969, 1982); it was originally discovered in Mexico somewhere south of Nogales.</discussion>
  <discussion>Mammillaria wrightii var. wilcoxii, which grows all around Nogales, Arizona, is easily misidentified as M. mainiae.</discussion>
  
</bio:treatment>