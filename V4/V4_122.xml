<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">abronia</taxon_name>
    <taxon_name authority="A. Nelson" date="1899" rank="species">elliptica</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 7. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus abronia;species elliptica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415083</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Abronia</taxon_name>
    <taxon_name authority="Nuttall ex Hooker" date="unknown" rank="species">fragrans</taxon_name>
    <taxon_name authority="(A. Nelson) M. E. Jones" date="unknown" rank="variety">elliptica</taxon_name>
    <taxon_hierarchy>genus Abronia;species fragrans;variety elliptica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Abronia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">nana</taxon_name>
    <taxon_name authority="S. L. Welsh" date="unknown" rank="variety">harrisii</taxon_name>
    <taxon_hierarchy>genus Abronia;species nana;variety harrisii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, sometimes nearly acaulescent.</text>
      <biological_entity id="o13505" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="sometimes nearly" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect, elongate, glandular-pubescent, infrequently glabrous.</text>
      <biological_entity id="o13506" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1–4 cm;</text>
      <biological_entity id="o13507" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13508" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate elliptic-oblong to ovate, 1.5–6 × 0.5–3.5 cm, margins entire to sinuate, often undulate, adaxial surface glabrous or puberulent, abaxial surface thinly puberulent to pubescent.</text>
      <biological_entity id="o13509" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13510" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="elliptic-oblong" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13511" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s3" to="sinuate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13512" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13513" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character char_type="range_value" from="thinly puberulent" name="pubescence" src="d0_s3" to="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: peduncle longer than subtending petiole;</text>
      <biological_entity id="o13514" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o13515" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character constraint="than subtending petiole" constraintid="o13516" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o13516" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracts ovate to obovate, 5–20 × 3–10 mm, scarious, apex obtuse to acute, glandular-pubescent to villous;</text>
      <biological_entity id="o13517" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o13518" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o13519" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute" />
        <character char_type="range_value" from="glandular-pubescent" name="pubescence" src="d0_s5" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flowers 25–75.</text>
      <biological_entity id="o13520" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o13521" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s6" to="75" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perianth: tube rose to greenish, 10–20 mm, limb white, 5–8 mm diam.</text>
      <biological_entity id="o13522" name="perianth" name_original="perianth" src="d0_s7" type="structure" />
      <biological_entity id="o13523" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="rose" name="coloration" src="d0_s7" to="greenish" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13524" name="limb" name_original="limb" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits broadly turbinate, apex truncate or rounded and slightly beaked, or fruit ± rhombic in profile, 5–12 × 4–8.5 mm, scarious, tapered at both ends;</text>
      <biological_entity id="o13525" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="turbinate" value_original="turbinate" />
      </biological_entity>
      <biological_entity id="o13526" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s8" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o13527" name="fruit" name_original="fruit" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less; in profile" name="shape" src="d0_s8" value="rhombic" value_original="rhombic" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="8.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character constraint="at ends" constraintid="o13528" is_modifier="false" name="shape" src="d0_s8" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o13528" name="end" name_original="ends" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>wings (2–) 5 (often 2 on periphery of inflorescence and folded together), dilated distally and flattened perpendicular to plane of lamina, dilations longer than wide, thin walled, cavities extending throughout.</text>
      <biological_entity id="o13529" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s9" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="perpendicular" value_original="perpendicular" />
        <character constraint="of lamina" constraintid="o13530" is_modifier="false" name="shape" src="d0_s9" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o13530" name="lamina" name_original="lamina" src="d0_s9" type="structure" />
      <biological_entity id="o13531" name="dilation" name_original="dilations" src="d0_s9" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer than wide" value_original="longer than wide" />
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="walled" value_original="walled" />
      </biological_entity>
      <biological_entity id="o13532" name="cavity" name_original="cavities" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly soils, desert grasslands, scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Nev., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  
</bio:treatment>