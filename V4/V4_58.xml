<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jackie M. Poole</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">16</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="treatment_page">33</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">ACLEISANTHES</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts, ser.</publication_title>
      <place_in_publication>2, 15: 259. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus ACLEISANTHES</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek a, without, cleis, thing that closes, and anthos, flower; alluding to lack of involucre</other_info_on_name>
    <other_info_on_name type="fna_id">100274</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Standley" date="unknown" rank="genus">Ammocodon</taxon_name>
    <taxon_hierarchy>genus Ammocodon;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="A. Gray" date="unknown" rank="genus">Selinocarpus</taxon_name>
    <taxon_hierarchy>genus Selinocarpus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, finely pubescent, from stout taproots.</text>
      <biological_entity id="o2134" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2135" name="taproot" name_original="taproots" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
      <relation from="o2134" id="r269" name="from" negation="false" src="d0_s0" to="o2135" />
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to erect, often clambering through other vegetation, slender to ± stout, herbaceous or woody, unarmed, without glutinous bands on internodes.</text>
      <biological_entity id="o2136" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="erect" />
        <character constraint="through vegetation" constraintid="o2137" is_modifier="false" modifier="often" name="orientation" src="d0_s1" value="clambering" value_original="clambering" />
        <character is_modifier="false" name="texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <biological_entity id="o2137" name="vegetation" name_original="vegetation" src="d0_s1" type="structure">
        <character char_type="range_value" from="slender" name="size" notes="" src="d0_s1" to="more or less stout" />
      </biological_entity>
      <biological_entity id="o2138" name="band" name_original="bands" src="d0_s1" type="structure">
        <character is_modifier="true" name="coating" src="d0_s1" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <biological_entity id="o2139" name="internode" name_original="internodes" src="d0_s1" type="structure" />
      <relation from="o2136" id="r270" name="without" negation="false" src="d0_s1" to="o2138" />
      <relation from="o2138" id="r271" name="on" negation="false" src="d0_s1" to="o2139" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile to petiolate, subequal to greatly unequal in each pair, ± thick and succulent, base asymmetric or ± symmetric.</text>
      <biological_entity id="o2140" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s2" to="petiolate" />
        <character char_type="range_value" constraint="in pair" constraintid="o2141" from="subequal" name="size" src="d0_s2" to="greatly unequal" />
        <character is_modifier="false" modifier="more or less" name="width" notes="" src="d0_s2" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s2" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o2141" name="pair" name_original="pair" src="d0_s2" type="structure" />
      <biological_entity id="o2142" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal or axillary, solitary flowers or few-flowered cymes and nearly sessile, or short pedicellate in 3–25-flowered umbellate clusters in axils or forks of branches;</text>
      <biological_entity id="o2143" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o2144" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short" value_original="short" />
        <character constraint="in forks" constraintid="o2147" is_modifier="false" name="architecture" src="d0_s3" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o2145" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="few-flowered" value_original="few-flowered" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short" value_original="short" />
        <character constraint="in forks" constraintid="o2147" is_modifier="false" name="architecture" src="d0_s3" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o2146" name="axil" name_original="axils" src="d0_s3" type="structure" />
      <biological_entity id="o2147" name="fork" name_original="forks" src="d0_s3" type="structure" />
      <biological_entity id="o2148" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <relation from="o2147" id="r272" name="part_of" negation="false" src="d0_s3" to="o2148" />
    </statement>
    <statement id="d0_s4">
      <text>bracts persistent, not accrescent, 1–3 beneath each flower, distinct, narrowly lanceolate, small or minute, herbaceous.</text>
      <biological_entity id="o2149" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s4" value="accrescent" value_original="accrescent" />
        <character char_type="range_value" constraint="beneath flower" constraintid="o2150" from="1" name="quantity" src="d0_s4" to="3" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s4" value="small" value_original="small" />
        <character is_modifier="false" name="size" src="d0_s4" value="minute" value_original="minute" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o2150" name="flower" name_original="flower" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual, chasmogamous and/or cleistogamous;</text>
      <biological_entity id="o2151" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="chasmogamous" value_original="chasmogamous" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cleistogamous perianth narrow domelike tube atop basal portion;</text>
      <biological_entity id="o2152" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity id="o2153" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="shape" src="d0_s6" value="domelike" value_original="domelike" />
      </biological_entity>
      <biological_entity id="o2154" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="atop" name="position" src="d0_s6" value="basal" value_original="basal" />
      </biological_entity>
      <relation from="o2153" id="r273" name="atop basal" negation="false" src="d0_s6" to="o2154" />
    </statement>
    <statement id="d0_s7">
      <text>chasmogamous perianth radially symmetric, short-to-elongate funnelform, constricted beyond ovary, abruptly expanded to 5-lobed limb;</text>
      <biological_entity id="o2155" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="chasmogamous" value_original="chasmogamous" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="short" name="size" src="d0_s7" to="elongate" />
        <character constraint="beyond ovary" constraintid="o2156" is_modifier="false" name="size" src="d0_s7" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o2156" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" notes="" src="d0_s7" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o2157" name="limb" name_original="limb" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="5-lobed" value_original="5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 2 (–3) or 5–6 (2–5 in cleistogamous flowers), exserted;</text>
      <biological_entity id="o2158" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="3" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles exserted beyond anthers;</text>
      <biological_entity id="o2159" name="style" name_original="styles" src="d0_s9" type="structure" />
      <biological_entity id="o2160" name="anther" name_original="anthers" src="d0_s9" type="structure" />
      <relation from="o2159" id="r274" name="exserted beyond" negation="false" src="d0_s9" to="o2160" />
    </statement>
    <statement id="d0_s10">
      <text>stigmas peltate.</text>
      <biological_entity id="o2161" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="peltate" value_original="peltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits oblong or narrowly ellipsoid, coriaceous, smooth, glabrous or minutely puberulent;</text>
      <biological_entity id="o2162" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="texture" src="d0_s11" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ribs 5, rounded, often with large, dark, sticky gland near apex, or wings 3–5, coriaceous, hyaline, often between ribs or wings a rib or low sharp ridge.</text>
      <biological_entity id="o2163" name="rib" name_original="ribs" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o2164" name="gland" name_original="gland" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="large" value_original="large" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="dark" value_original="dark" />
        <character is_modifier="true" name="texture" src="d0_s12" value="sticky" value_original="sticky" />
      </biological_entity>
      <biological_entity id="o2165" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o2166" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="5" />
        <character is_modifier="false" name="texture" src="d0_s12" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o2167" name="rib" name_original="ribs" src="d0_s12" type="structure" />
      <biological_entity id="o2169.o2167-o2166." name="rib-wing" name_original="ribs and wings" src="d0_s12" type="structure" />
      <biological_entity id="o2170" name="rib" name_original="rib" src="d0_s12" type="structure" />
      <biological_entity id="o2171" name="ridge" name_original="ridge" src="d0_s12" type="structure">
        <character is_modifier="true" name="position" src="d0_s12" value="low" value_original="low" />
        <character is_modifier="true" name="shape" src="d0_s12" value="sharp" value_original="sharp" />
      </biological_entity>
      <relation from="o2163" id="r275" modifier="often" name="with" negation="false" src="d0_s12" to="o2164" />
      <relation from="o2164" id="r276" name="near" negation="false" src="d0_s12" to="o2165" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, n Mexico, ne Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
        <character name="distribution" value="ne Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Trumpets</other_name>
  <discussion>Species 17 (12 in the flora).</discussion>
  <discussion>R. A. Levin (2000) showed that Selinocarpus is paraphyletic and embedded within Acleisanthes. All species are pubescent on young stems and leaves. Older leaves may be glabrate or glabrous.</discussion>
  <references>
    <reference>Fowler, B. A. and B. L. Turner. 1977. Taxonomy of Selinocarpus and Ammocodon (Nyctaginaceae). Phytologia 37: 177–208.</reference>
    <reference>Levin, R. A. 2002. Taxonomic status of Acleisanthes, Selinocarpus, and Ammocodon (Nyctaginaceae). Novon 12: 58–63.</reference>
    <reference>Smith, J. M. 1976. A taxonomic study of Acleisanthes (Nyctaginaceae). Wrightia 5: 261–276.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits with rounded or broadly obtuse ribs</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits with thin, hyaline wings</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Resinous glands present at or near apex of fruits; overall glandular pubescence consisting of mixture of capitate hairs 0.2-0.3 mm, and shorter white, capitate hairs 0.1-0.2 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Resinous glands on fruits absent; overall pubescence consisting of white, capitate hairs 0.1-0.4 mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Glands at apex of mature fruits; ribs not extending past glands; fruits oblong, base and apex truncate</description>
      <determination>6 Acleisanthes wrightii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Glands below apex of mature fruits; ribs extending past glands; fruits constricted at level of glands, oval-oblong, base tapering apex truncate</description>
      <determination>5 Acleisanthes acutifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Fruits 10-ribbed; leaves in each pair very unequal</description>
      <determination>1 Acleisanthes anisophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Fruits 5-ribbed; leaves in each pair slightly unequal</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Ribs on fruit broad, flat, lacking grooves; leaf blades adaxially hirtellous on primary and secondary veins</description>
      <determination>4 Acleisanthes crassifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Ribs on fruit narrow, ridgelike, often with pair of parallel grooves between each rib or ridge; leaf blades uniformly puberulent to glabrate</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Inflorescences usually solitary flowers, rarely geminate; chasmogamous flowers 7-17 cm</description>
      <determination>3 Acleisanthes longiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Inflorescences usually 2-5-flowered cymes, occasionally solitary flowers; chasmogamous flowers 2.5-5.5 cm</description>
      <determination>2 Acleisanthes obtusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Perianths 4-15 mm, limbs pink or brownish orange; leaf blades from ovate or deltate to linear; at least young stems and leaves with minute, white, T-shaped hairs</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Perianths 17-52 mm, limbs white, cream, or greenish yellow; leaf blades lanceolate, ovate, ovate-oblong, or ± diamond-shaped; at least young stems and leaves with minute, white, hairs not T shaped</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Perianth limbs pink to lavender; perianth 4-6 mm; leaf blades ovate, ovate-oblong, or deltate; plants herbaceous; stamens usually 2</description>
      <determination>12 Acleisanthes chenopodioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Perianth limbs brownish orange; perianth 10-15 mm; leaf blades linear or linear- lanceolate; plants woody at base or throughout; stamens 5.</description>
      <determination>10 Acleisanthes angustifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Pubescence of minute, flat, white hairs only; petioles 0-3 mm; leaf margins entire</description>
      <determination>11 Acleisanthes lanceolata</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Pubescence of glandular hairs or multicellular conic hairs and minute, flat, white hairs; petioles 1-20 mm; leaf margins entire or undulate</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Stems ascending to erect, sparsely leafy; leaves abruptly reduced toward inflo- rescence; fruits 8-10 mm</description>
      <determination>9 Acleisanthes parvifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Stems commonly decumbent, sometimes erect, densely leafy; leaves gradually reduced toward inflorescence; fruits 5-7 mm</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blades ovate to ovate-oblong, leaves commonly drying brownish green; perianth limbs 15 mm wide</description>
      <determination>7 Acleisanthes diffusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blades ovate to orbiculate, leaves commonly drying yellowish green; perianth limbs 10 mm wide</description>
      <determination>8 Acleisanthes nevadensis</determination>
    </key_statement>
  </key>
</bio:treatment>