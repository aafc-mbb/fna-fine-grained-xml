<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Noel H. Holmgren</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">261</other_info_on_meta>
    <other_info_on_meta type="treatment_page">300</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Schrader" date="1830" rank="genus">MONOLEPIS</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (Göttingen)</publication_title>
      <place_in_publication>1830: 4. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus MONOLEPIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek monos, solitary, and lepis, scale, for the typically solitary sepal</other_info_on_name>
    <other_info_on_name type="fna_id">121060</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, polygamous, ± farinose or glabrous.</text>
      <biological_entity id="o10813" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamous" value_original="polygamous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="farinose" value_original="farinose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems arising from base, prostrate to ascending, not jointed, not armed, not fleshy;</text>
      <biological_entity id="o10814" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="from base" constraintid="o10815" is_modifier="false" name="orientation" src="d0_s1" value="arising" value_original="arising" />
        <character char_type="range_value" from="prostrate" name="orientation" notes="" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="jointed" value_original="jointed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="armed" value_original="armed" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o10815" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>ultimate branches not filiform.</text>
      <biological_entity constraint="ultimate" id="o10816" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate, succulent;</text>
      <biological_entity id="o10817" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="texture" src="d0_s3" value="succulent" value_original="succulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade triangular-lanceolate to oblanceolate or spatulate, base narrowly attenuate to cuneate, unlobed to hastate, margins sometimes with few teeth distally or completely entire, apex obtuse to rounded.</text>
      <biological_entity id="o10818" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="triangular-lanceolate" name="shape" src="d0_s4" to="oblanceolate or spatulate" />
      </biological_entity>
      <biological_entity id="o10819" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly attenuate" name="shape" src="d0_s4" to="cuneate unlobed" />
        <character char_type="range_value" from="narrowly attenuate" name="shape" src="d0_s4" to="cuneate unlobed" />
      </biological_entity>
      <biological_entity id="o10820" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="completely" name="architecture_or_shape" notes="" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10821" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o10822" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <relation from="o10820" id="r1580" name="with" negation="false" src="d0_s4" to="o10821" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–many-flowered glomerules in leaf-axils.</text>
      <biological_entity id="o10823" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o10824" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="1-many-flowered" value_original="1-many-flowered" />
      </biological_entity>
      <biological_entity id="o10825" name="leaf-axil" name_original="leaf-axils" src="d0_s5" type="structure" />
      <relation from="o10824" id="r1581" name="in" negation="false" src="d0_s5" to="o10825" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual or pistillate;</text>
      <biological_entity id="o10826" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth segment usually 1 (2–3 in central flowers) or absent, bractlike, greenish;</text>
      <biological_entity constraint="perianth" id="o10827" name="segment" name_original="segment" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s7" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 1 (–2) or absent (in pistillate flowers);</text>
      <biological_entity id="o10828" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="2" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary superior;</text>
      <biological_entity id="o10829" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="superior" value_original="superior" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 2, connate proximally.</text>
    </statement>
    <statement id="d0_s11">
      <text>Fruiting structures somewhat flattened utricles;</text>
      <biological_entity id="o10830" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o10831" name="structure" name_original="structures" src="d0_s11" type="structure" />
      <biological_entity id="o10832" name="utricle" name_original="utricles" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="somewhat" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o10830" id="r1582" name="fruiting" negation="false" src="d0_s11" to="o10831" />
    </statement>
    <statement id="d0_s12">
      <text>pericarp loose when dry.</text>
      <biological_entity id="o10833" name="pericarp" name_original="pericarp" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="when dry" name="architecture_or_fragility" src="d0_s12" value="loose" value_original="loose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds vertical, lenticular;</text>
      <biological_entity id="o10834" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lenticular" value_original="lenticular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat brown to black, smooth;</text>
      <biological_entity id="o10835" name="seed-coat" name_original="seed-coat" src="d0_s14" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s14" to="black" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>embryo annular;</text>
      <biological_entity id="o10836" name="embryo" name_original="embryo" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="annular" value_original="annular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>perisperm copious.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity id="o10837" name="perisperm" name_original="perisperm" src="d0_s16" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s16" value="copious" value_original="copious" />
      </biological_entity>
      <biological_entity constraint="x" id="o10838" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate regions of w North America, c and ne Asia, s South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate regions of w North America" establishment_means="native" />
        <character name="distribution" value="c and ne Asia" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Species 5 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves, at least some of them, hastately lobed; utricle 1.1-1.5 mm; pericarp whitish, cellular reticulate</description>
      <determination>1 Monolepis nuttalliana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves unlobed; utricle 0.5-0.7 mm; pericarp pale brown, turning black, tuberculate-papil- lose</description>
      <determination>2 Monolepis spathulata</determination>
    </key_statement>
  </key>
</bio:treatment>