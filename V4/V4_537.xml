<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="treatment_page">287</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">CHENOPODIUM</taxon_name>
    <taxon_name authority="Aellen &amp; Iljin ex Mosyakin &amp; Clemants" date="1996" rank="subsection">Undata</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">murale</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 219. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus chenopodium;subgenus chenopodium;section chenopodium;subsection undata;species murale;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242100042</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, branched, 1–6 (–10) dm, glabrous (to sparsely farinose when young);</text>
      <biological_entity id="o19662" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal branches decumbent.</text>
      <biological_entity constraint="proximal" id="o19663" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves nonaromatic;</text>
      <biological_entity id="o19664" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="odor" src="d0_s2" value="nonaromatic" value_original="nonaromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–2.5 cm;</text>
      <biological_entity id="o19665" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade triangular, ovate, or rhombic-ovate, 0.8–4 (–8) × 0.4–3 (–5) cm, base cuneate to rounded, margins irregularly dentate, apex acute to acuminate, glabrous (rarely indistinctly farinose when young).</text>
      <biological_entity id="o19666" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19667" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o19668" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o19669" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences glomerules in terminal and lateral panicles, 6–7 × 4–5 cm;</text>
      <biological_entity constraint="inflorescences" id="o19670" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" notes="" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" notes="" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="terminal and lateral" id="o19671" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <relation from="o19670" id="r2859" name="in" negation="false" src="d0_s5" to="o19671" />
    </statement>
    <statement id="d0_s6">
      <text>glomerules subglobose, 2–4 mm diam., or some flowers not in glomerules;</text>
      <biological_entity id="o19672" name="glomerule" name_original="glomerules" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19673" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19674" name="glomerule" name_original="glomerules" src="d0_s6" type="structure" />
      <relation from="o19673" id="r2860" name="in" negation="false" src="d0_s6" to="o19674" />
    </statement>
    <statement id="d0_s7">
      <text>bracts absent.</text>
      <biological_entity id="o19675" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth segments 5, distinct nearly to base;</text>
      <biological_entity id="o19676" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="perianth" id="o19677" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character constraint="to base" constraintid="o19678" is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o19678" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>lobes ovate, 0.5–0.8 × 0.6–0.7 mm, apex acute to obtuse, keeled abaxially, farinose, covering fruit at maturity;</text>
      <biological_entity id="o19679" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19680" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s9" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s9" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19681" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="obtuse" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="farinose" value_original="farinose" />
      </biological_entity>
      <biological_entity id="o19682" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <relation from="o19681" id="r2861" name="covering" negation="false" src="d0_s9" to="o19682" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 5;</text>
      <biological_entity id="o19683" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19684" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas 2, 0.2 mm.</text>
      <biological_entity id="o19685" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19686" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes depressed-ovoid;</text>
      <biological_entity id="o19687" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="depressed-ovoid" value_original="depressed-ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pericarp adherent, pustulate, becoming smooth with maturity.</text>
      <biological_entity id="o19688" name="pericarp" name_original="pericarp" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="adherent" value_original="adherent" />
        <character is_modifier="false" name="relief" src="d0_s13" value="pustulate" value_original="pustulate" />
        <character constraint="with maturity" is_modifier="false" modifier="becoming" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds lenticular, round, 1–1.5 mm diam.;</text>
      <biological_entity id="o19689" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s14" value="round" value_original="round" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>seed-coat black, minutely rugose to ± smooth.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 18.</text>
      <biological_entity id="o19690" name="seed-coat" name_original="seed-coat" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="black" value_original="black" />
        <character char_type="range_value" from="minutely rugose" name="relief" src="d0_s15" to="more or less smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19691" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, roadsides, clay mounds, open oak woods, prairies, rocky hillsides, along railways</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="clay mounds" />
        <character name="habitat" value="open oak woods" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="rocky hillsides" constraint="along railways" />
        <character name="habitat" value="railways" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., N.B., Ont., Que., Sask.; Ariz., Ark., Calif., Conn., Del., D.C., Fla., Ga., Ill., Ind., Ky., La., Maine, Md., Mass., Mich., Mo., Nev., N.J., N.Y., N.C., Ohio, Okla., Oreg., Pa., R.I., S.C., Tenn., Tex., Utah, Vt., Va., W.Va., Wis.; native to Europe, Asia, n Africa; introduced nearly worldwide, mostly in subtropics and warm-temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="native to Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="nearly worldwide" establishment_means="introduced" />
        <character name="distribution" value="mostly in subtropics and warm-temperate regions" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Sowbane</other_name>
  <other_name type="common_name">nettle-leaf goosefoot</other_name>
  <discussion>Chenopodium murale is distinctive and is one of the more common species of the genus in the world, especially in tropical and subtropical regions.</discussion>
  
</bio:treatment>