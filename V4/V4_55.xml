<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="treatment_page">32</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Standley" date="1911" rank="genus">cyphomeris</taxon_name>
    <taxon_name authority="(Standley) Standley" date="1911" rank="species">crassifolia</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>13: 428. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus cyphomeris;species crassifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415030</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senkenbergia</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="species">crassifolia</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>12: 373. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senkenbergia;species crassifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, finely pubescent with pale, curved hairs (rarely capitate-glandular).</text>
      <biological_entity id="o9932" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="with hairs" constraintid="o9933" is_modifier="false" modifier="finely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o9933" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="pale" value_original="pale" />
        <character is_modifier="true" name="course" src="d0_s0" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to trailing, 5–20 dm.</text>
      <biological_entity id="o9934" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="trailing" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s1" to="20" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades rhombic to ovate or broadly oblong, proximal blades 10–50 × 8–40 mm, proportionately about as wide as distal blades, margins usually sinuate, often undulate, pubescent.</text>
      <biological_entity id="o9935" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s2" to="ovate or broadly oblong" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9936" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="50" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9937" name="blade" name_original="blades" src="d0_s2" type="structure" />
      <biological_entity id="o9938" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proportionately; usually" name="shape" src="d0_s2" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Perianths pale-pink to deep pink or red-violet [greenish white], 6–8 mm.</text>
      <biological_entity id="o9939" name="perianth" name_original="perianths" src="d0_s3" type="structure">
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s3" to="deep pink or red-violet" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Fruits notably gibbous, 6–8 [–11] mm, usually prominently warty at least on gibbous side.</text>
      <biological_entity id="o9940" name="fruit" name_original="fruits" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="notably" name="shape" src="d0_s4" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
        <character constraint="on side" constraintid="o9941" is_modifier="false" modifier="usually prominently" name="pubescence_or_relief" src="d0_s4" value="warty" value_original="warty" />
      </biological_entity>
      <biological_entity id="o9941" name="side" name_original="side" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="gibbous" value_original="gibbous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fine or rocky soils, flats, washes, slopes, roadsides, desert to semiarid or subtropical scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fine" />
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="desert" />
        <character name="habitat" value="semiarid" />
        <character name="habitat" value="subtropical scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100[-2000] m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; ne Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="ne Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="ne Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="ne Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>