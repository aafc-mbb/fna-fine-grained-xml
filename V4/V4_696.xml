<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="treatment_page">358</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Gaertner) S. L. Welsh" date="2001" rank="subgenus">Obione</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Obione</taxon_name>
    <taxon_name authority="(Standley) S. L. Welsh" date="2001" rank="subsection">Pusillae</taxon_name>
    <taxon_name authority="Jepson" date="1892" rank="species">cordulata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">cordulata</taxon_name>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus obione;section obione;subsection pusillae;species cordulata;variety cordulata;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415547</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems highly branched from base, 1–5 dm;</text>
      <biological_entity id="o2755" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="from base" constraintid="o2756" is_modifier="false" modifier="highly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o2756" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>branches mainly steeply ascending.</text>
      <biological_entity id="o2757" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mainly steeply" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade ovate to cordate-ovate or subcordate, 5–15 (–20) × (3–) 5–12 mm wide, base rounded to cordate-clasping, margin typically entire, or sparingly toothed below middle.</text>
      <biological_entity id="o2758" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o2759" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="cordate-ovate or subcordate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2760" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s2" value="cordate-clasping" value_original="cordate-clasping" />
      </biological_entity>
      <biological_entity id="o2761" name="margin" name_original="margin" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="typically" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sparingly" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="typically" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character constraint="below middle leaves" constraintid="o2762" is_modifier="false" modifier="sparingly" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o2762" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s2" value="middle" value_original="middle" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Anthers red or purple.</text>
    </statement>
    <statement id="d0_s4">
      <text>Fruiting bracteoles semicircular to deltoid-ovate in profile, 4–5 mm and about as wide, central tooth largest, scarcely though sometimes tuberculate on faces.</text>
      <biological_entity id="o2764" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure" />
      <biological_entity constraint="central" id="o2765" name="tooth" name_original="tooth" src="d0_s4" type="structure">
        <character char_type="range_value" from="semicircular" modifier="in profile" name="shape" src="d0_s4" to="deltoid-ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character constraint="on faces" constraintid="o2766" is_modifier="false" modifier="scarcely though sometimes" name="relief" src="d0_s4" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o2766" name="face" name_original="faces" src="d0_s4" type="structure" />
      <relation from="o2763" id="r356" name="fruiting" negation="false" src="d0_s4" to="o2764" />
    </statement>
    <statement id="d0_s5">
      <text>2n = 36.</text>
      <biological_entity id="o2763" name="anther" name_original="anthers" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2767" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hard trampled, somewhat saline or alkaline soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hard trampled" />
        <character name="habitat" value="saline" modifier="somewhat" />
        <character name="habitat" value="alkaline soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>below 70 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="70" to_unit="m" from="any" from_unit="m" constraint="below " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32a.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Atriplex cordulata var. cordulata often occurs with Distichlis spicata, Centromadia pungens, Suaeda moquinii, Spergularia macrotheca, Frankenia salina, Sporobolus airoides, and other annual Atriplex species.</discussion>
  
</bio:treatment>