<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Michael W. Hawkes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">92</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="treatment_page">99</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Engelmann in W. H. Brewer et al." date="unknown" rank="subfamily">Pereskioideae</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>1: 243. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily Pereskioideae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20514</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, shrubs, or woody climbers, erect or scrambling, usually freely branching, not cactuslike, resembling other woody plants.</text>
      <biological_entity id="o17454" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="scrambling" value_original="scrambling" />
        <character is_modifier="false" modifier="usually freely" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s0" value="cactuslike" value_original="cactuslike" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o17456" name="climber" name_original="climbers" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="scrambling" value_original="scrambling" />
        <character is_modifier="false" modifier="usually freely" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s0" value="cactuslike" value_original="cactuslike" />
      </biological_entity>
      <biological_entity id="o17457" name="plant" name_original="plants" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o17454" id="r2527" name="resembling other" negation="false" src="d0_s0" to="o17457" />
      <relation from="o17456" id="r2528" name="resembling other" negation="false" src="d0_s0" to="o17457" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse or tuberlike.</text>
      <biological_entity id="o17458" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" name="shape" src="d0_s1" value="tuberlike" value_original="tuberlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unsegmented, not ribbed or tuberculate, not especially succulent, becoming woody with age;</text>
      <biological_entity id="o17459" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s2" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="relief" src="d0_s2" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="not especially" name="texture" src="d0_s2" value="succulent" value_original="succulent" />
        <character constraint="with age" constraintid="o17460" is_modifier="false" modifier="becoming" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o17460" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>areoles in leaf-axils, circular, bearing 1 to several spines, glochids absent.</text>
      <biological_entity id="o17461" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s3" value="circular" value_original="circular" />
        <character constraint="to spines" constraintid="o17463" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o17462" name="leaf-axil" name_original="leaf-axils" src="d0_s3" type="structure" />
      <biological_entity id="o17463" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o17464" name="glochid" name_original="glochids" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o17461" id="r2529" name="in" negation="false" src="d0_s3" to="o17462" />
    </statement>
    <statement id="d0_s4">
      <text>Spines persistent, usually acicular, straight, less often curving and catclaw-like, smooth, glabrous, epidermis intact, not separating as sheath.</text>
      <biological_entity id="o17465" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="acicular" value_original="acicular" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="less often" name="course" src="d0_s4" value="curving" value_original="curving" />
        <character is_modifier="false" name="shape" src="d0_s4" value="catclaw-like" value_original="catclaw-like" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17466" name="epidermis" name_original="epidermis" src="d0_s4" type="structure">
        <character is_modifier="false" name="condition" src="d0_s4" value="intact" value_original="intact" />
        <character constraint="as sheath" constraintid="o17467" is_modifier="false" modifier="not" name="arrangement" src="d0_s4" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o17467" name="sheath" name_original="sheath" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves deciduous, alternate, often petiolate [subsessile];</text>
      <biological_entity id="o17468" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade broad, flat, fleshy or ± succulent, resembling leaves of other broad-leaved plants.</text>
      <biological_entity id="o17469" name="blade" name_original="blade" src="d0_s6" type="structure" constraint="plant" constraint_original="plant; plant">
        <character is_modifier="false" name="width" src="d0_s6" value="broad" value_original="broad" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s6" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o17470" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o17471" name="plant" name_original="plants" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="broad-leaved" value_original="broad-leaved" />
      </biological_entity>
      <relation from="o17469" id="r2530" name="resembling" negation="false" src="d0_s6" to="o17470" />
      <relation from="o17469" id="r2531" name="part_of" negation="false" src="d0_s6" to="o17471" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers diurnal, bisexual [or unisexual], in axillary or terminal racemes, corymbs, panicles, or cymose-panicles of 1–70+ flowers [solitary or fasciculate], radially symmetric, stalked [subsessile to sessile], ± rotate [broadly campanulate–urceolate];</text>
      <biological_entity id="o17472" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="diurnal" value_original="diurnal" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o17473" name="raceme" name_original="racemes" src="d0_s7" type="structure" />
      <biological_entity id="o17474" name="corymb" name_original="corymbs" src="d0_s7" type="structure" />
      <biological_entity id="o17475" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity id="o17476" name="cymose-panicle" name_original="cymose-panicles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" notes="" src="d0_s7" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="stalked" value_original="stalked" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="rotate" value_original="rotate" />
      </biological_entity>
      <biological_entity id="o17477" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="70" upper_restricted="false" />
      </biological_entity>
      <relation from="o17472" id="r2532" name="in" negation="false" src="d0_s7" to="o17473" />
      <relation from="o17476" id="r2533" name="consist_of" negation="false" src="d0_s7" to="o17477" />
    </statement>
    <statement id="d0_s8">
      <text>tepals perigynous or epigynous, flower tube not apparent;</text>
      <biological_entity id="o17478" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="epigynous" value_original="epigynous" />
      </biological_entity>
      <biological_entity constraint="flower" id="o17479" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s8" value="apparent" value_original="apparent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary subtended by broad leaflike scaly bracts;</text>
      <biological_entity id="o17480" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
      <biological_entity id="o17481" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s9" value="leaflike" value_original="leaflike" />
        <character is_modifier="true" name="architecture_or_pubescence" src="d0_s9" value="scaly" value_original="scaly" />
      </biological_entity>
      <relation from="o17480" id="r2534" name="subtended by" negation="false" src="d0_s9" to="o17481" />
    </statement>
    <statement id="d0_s10">
      <text>nectar chamber not apparent.</text>
      <biological_entity constraint="nectar" id="o17482" name="chamber" name_original="chamber" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s10" value="apparent" value_original="apparent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits indehiscent, spheric, pyriform, or broadly depressed-obovate, fleshy;</text>
      <biological_entity id="o17483" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s11" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="depressed-obovate" value_original="depressed-obovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="depressed-obovate" value_original="depressed-obovate" />
        <character is_modifier="false" name="texture" src="d0_s11" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth and contained flower parts ± persistent.</text>
      <biological_entity id="o17484" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="flower" id="o17485" name="part" name_original="parts" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 2–150+, black, lenticular to obovate or subreniform, 1.7–7.5 mm, not strophiolate or arillate.</text>
      <biological_entity id="o17486" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="150" upper_restricted="false" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character char_type="range_value" from="lenticular" name="shape" src="d0_s13" to="obovate or subreniform" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s13" to="7.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s13" value="strophiolate" value_original="strophiolate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="arillate" value_original="arillate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; tropical and subtropical regions in the New World from central Mexico and the West Indies southward to northern Argentina.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="tropical and subtropical regions in the New World from central Mexico and the West Indies southward to northern Argentina" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>37a.</number>
  <other_name type="past_name">Peirescieae</other_name>
  <discussion>Genus 1, species 17 (2 species in the flora).</discussion>
  <discussion>The monogeneric Pereskioideae is the smallest North American subfamily of Cactaceae. Recently, subfam. Maihuenioideae P. Fearn, of South America, was segregated on the basis of DNA and morphologic data. It has long been generally accepted that the cactus family had its origin in a Pereskia-like ancestor, because species of Pereskia, with their broad leaves, woody, barely succulent habits, and relatively primitive-looking flowers, more closely resemble typical, leafy dicotyledonous plants than succulent cacti (A. C. Gibson and P. S. Nobel 1986; B. E. Leuenberger 1986). In addition, species of Pereskia are reported to have the C3 photosynthetic pathway, unlike stem-succulent cacti, which have crassulacean acid metabolism (A. C. Gibson and P. S. Nobel 1986). Recent analyses of DNA data confirm that Pereskia have DNA sequences that are primitive for the family (R. S. Wallace and A. C. Gibson 2002).</discussion>
  
</bio:treatment>