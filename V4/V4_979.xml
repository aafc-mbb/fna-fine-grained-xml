<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="treatment_page">484</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="genus">lewisia</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="species">rediviva</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 368. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus lewisia;species rediviva</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220007569</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Taproots gradually ramified distally.</text>
      <biological_entity id="o18834" name="taproot" name_original="taproots" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="architecture" src="d0_s0" value="ramified" value_original="ramified" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems procumbent to erect, 1–3 cm.</text>
      <biological_entity id="o18835" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves withering at or soon after anthesis, sessile, blade linear to clavate, subterete or grooved adaxially, 0.5–5 cm, margins entire, apex obtuse to subacute;</text>
      <biological_entity id="o18836" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o18837" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="after anthesis" is_modifier="false" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o18838" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="clavate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subterete" value_original="subterete" />
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s2" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18839" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18840" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves absent.</text>
      <biological_entity id="o18841" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o18842" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with flowers borne singly;</text>
      <biological_entity id="o18843" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o18844" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o18843" id="r2718" name="with" negation="false" src="d0_s4" to="o18844" />
    </statement>
    <statement id="d0_s5">
      <text>bracts 4–7 (–8), whorled, subulate to linear-lanceolate, 4–10 mm, margins entire, apex acuminate.</text>
      <biological_entity id="o18845" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="8" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="7" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="whorled" value_original="whorled" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18846" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18847" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers pedicellate, disarticulate in fruit;</text>
      <biological_entity id="o18848" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o18849" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <relation from="o18848" id="r2719" name="disarticulate in" negation="false" src="d0_s6" to="o18849" />
    </statement>
    <statement id="d0_s7">
      <text>sepals (4–) 6–9, broadly elliptic to ovate, 10–25 mm, scarious after anthesis, margins entire to somewhat erose, apex obtuse to rounded;</text>
      <biological_entity id="o18850" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="9" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s7" to="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character constraint="after anthesis" is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o18851" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="entire to somewhat" value_original="entire to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o18852" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 10–19, usually rose to pink, sometimes lavender, sometimes with paler or white centers, or wholly white, elliptic, oblong, or narrowly oblanceolate, 15–35 mm;</text>
      <biological_entity id="o18853" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="19" />
        <character char_type="range_value" from="usually rose" name="coloration" src="d0_s8" to="pink" />
        <character is_modifier="false" modifier="sometimes" name="coloration_or_odor" src="d0_s8" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="wholly" name="coloration" notes="" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18854" name="center" name_original="centers" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="paler" value_original="paler" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <relation from="o18853" id="r2720" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o18854" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 20–50;</text>
      <biological_entity id="o18855" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 4–9;</text>
      <biological_entity id="o18856" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicel (1–) 3–15 (–30) mm.</text>
      <biological_entity id="o18857" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 5–6 mm.</text>
      <biological_entity id="o18858" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 6–25, 2–2.5 mm, shiny, minutely papillate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 26, 28.</text>
      <biological_entity id="o18859" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s13" to="25" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18860" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="26" value_original="26" />
        <character name="quantity" src="d0_s14" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Ariz., Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Bitterroot</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Native Americans commonly ate the boiled roots of Lewisia rediviva.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaf blades clavate to narrowly oblanceolate, grooved adaxially; sepals 10-12(-15) mm; petals 15mm; stamens 20-30</description>
      <determination>14a Lewisia rediviva var. minor</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaf blades linear, subterete, not grooved adaxially; sepals 15-25 mm; petals 18-35 mm; stamens 30-50</description>
      <determination>14b Lewisia rediviva var. rediviva</determination>
    </key_statement>
  </key>
</bio:treatment>