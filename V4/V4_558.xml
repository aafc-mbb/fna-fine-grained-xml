<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">CHENOPODIUM</taxon_name>
    <taxon_name authority="(Aellen) Mosyakin &amp; Clemants" date="1996" rank="subsection">Favosa</taxon_name>
    <taxon_name authority="Standley in N. L. Britton et al." date="1916" rank="species">neomexicanum</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>21: 19. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus chenopodium;subgenus chenopodium;section chenopodium;subsection favosa;species neomexicanum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415440</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect to ascending, sparsely branched, 2.5–5 dm, sparsely farinose.</text>
      <biological_entity id="o3001" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="ascending" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="farinose" value_original="farinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves nonaromatic;</text>
      <biological_entity id="o3002" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="odor" src="d0_s1" value="nonaromatic" value_original="nonaromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.5–1.3 cm;</text>
      <biological_entity id="o3003" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="1.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade triangular to ovate or rhombic-ovate, 0.7–2.9 × 0.4–2.1 cm, apex apiculate, base broadly cuneate to truncate, subhastate with low, rounded or acutish lobes, margins entire above lobes, apex acute to obtuse, sparsely farinose abaxially.</text>
      <biological_entity id="o3004" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s3" to="ovate or rhombic-ovate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s3" to="2.9" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="2.1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3005" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o3006" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly cuneate" name="shape" src="d0_s3" to="truncate" />
        <character constraint="with lobes" constraintid="o3007" is_modifier="false" name="shape" src="d0_s3" value="subhastate" value_original="subhastate" />
      </biological_entity>
      <biological_entity id="o3007" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="low" value_original="low" />
        <character is_modifier="true" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s3" value="acutish" value_original="acutish" />
      </biological_entity>
      <biological_entity id="o3008" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="above lobes" constraintid="o3009" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3009" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <biological_entity id="o3010" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="pubescence" src="d0_s3" value="farinose" value_original="farinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences glomerules in paniculate spikes, 21–24 × 4–9 cm;</text>
      <biological_entity constraint="inflorescences" id="o3011" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character char_type="range_value" from="21" from_unit="cm" name="length" notes="" src="d0_s4" to="24" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" notes="" src="d0_s4" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3012" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <relation from="o3011" id="r387" name="in" negation="false" src="d0_s4" to="o3012" />
    </statement>
    <statement id="d0_s5">
      <text>glomerules maturing irregularly;</text>
      <biological_entity id="o3013" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="life_cycle" src="d0_s5" value="maturing" value_original="maturing" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts leaflike to linear.</text>
      <biological_entity id="o3014" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="leaflike" name="shape" src="d0_s6" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth segments 5, distinct nearly to base;</text>
      <biological_entity id="o3015" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="perianth" id="o3016" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character constraint="to base" constraintid="o3017" is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o3017" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>lobes ovate, 0.7–1 × 0.7–0.9 mm, apex obtuse, keeled, densely to sparsely farinose, partly covering seeds at maturity;</text>
      <biological_entity id="o3018" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3019" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s8" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s8" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3020" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="densely to sparsely" name="pubescence" src="d0_s8" value="farinose" value_original="farinose" />
      </biological_entity>
      <biological_entity id="o3021" name="seed" name_original="seeds" src="d0_s8" type="structure" />
      <relation from="o3020" id="r388" modifier="partly" name="covering" negation="false" src="d0_s8" to="o3021" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 5;</text>
      <biological_entity id="o3022" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3023" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 2, 0.2–0.3 mm.</text>
      <biological_entity id="o3024" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3025" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Achenes ovoid;</text>
      <biological_entity id="o3026" name="achene" name_original="achenes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pericarp adherent, black, honeycombed.</text>
      <biological_entity id="o3027" name="pericarp" name_original="pericarp" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="adherent" value_original="adherent" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character is_modifier="false" name="relief" src="d0_s12" value="honeycombed" value_original="honeycombed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds lenticular or round, 1–1.3 mm diam., margins acute;</text>
      <biological_entity id="o3028" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s13" value="round" value_original="round" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s13" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3029" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat coarsely honeycombed.</text>
      <biological_entity id="o3030" name="seed-coat" name_original="seed-coat" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s14" value="honeycombed" value_original="honeycombed" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist soils, roadsides, pinelands, igneous rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist soils" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="pinelands" />
        <character name="habitat" value="igneous rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  
</bio:treatment>