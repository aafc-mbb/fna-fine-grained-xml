<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">145</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">opuntia</taxon_name>
    <taxon_name authority="Haworth" date="1819" rank="species">polyacantha</taxon_name>
    <taxon_name authority="(Engelmann &amp; J. M. Bigelow) B. D. Parfitt" date="1998" rank="variety">erinacea</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>70: 188. 1998</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus opuntia;species polyacantha;variety erinacea;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415228</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Engelmann &amp; J. M. Bigelow" date="unknown" rank="species">erinacea</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>3: 301. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Opuntia;species erinacea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Engelmann &amp; Bigelow" date="unknown" rank="species">erinacea</taxon_name>
    <taxon_name authority="(F. A. C. Weber) Parish" date="unknown" rank="variety">ursina</taxon_name>
    <taxon_hierarchy>genus Opuntia;species erinacea;variety ursina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="F. A. C. Weber" date="unknown" rank="species">hystricina</taxon_name>
    <taxon_name authority="(F. A. C. Weber) Backeberg" date="unknown" rank="variety">ursina</taxon_name>
    <taxon_hierarchy>genus Opuntia;species hystricina;variety ursina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ursina</taxon_name>
    <taxon_hierarchy>genus Opuntia;species ursina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stem segments elliptic to obovate, (7–) 10–20 × 5–10 cm;</text>
      <biological_entity constraint="stem" id="o16657" name="segment" name_original="segments" src="d0_s0" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s0" to="obovate" />
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_length" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s0" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s0" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>areoles 8–14 per diagonal row across midstem segment, 12–17 mm apart.</text>
      <biological_entity id="o16658" name="areole" name_original="areoles" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="per diagonal row" constraintid="o16659" from="8" name="quantity" src="d0_s1" to="14" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" notes="" src="d0_s1" to="17" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o16659" name="row" name_original="row" src="d0_s1" type="structure" />
      <biological_entity constraint="midstem" id="o16660" name="segment" name_original="segment" src="d0_s1" type="structure" />
      <relation from="o16659" id="r2403" name="across" negation="false" src="d0_s1" to="o16660" />
    </statement>
    <statement id="d0_s2">
      <text>Spines 1–18 per areole, in essentially all areoles, grading in size and orientation, yellow to dark-brown, turning gray, pink-gray, or gray-brown, the longest spreading and curling in all directions especially on proximal stem segments, usually ascending and ± straight near stem segment apex, deflexed to reflexed near base, (35–) 40–90 (–185) mm, spines of older stem segments more numerous, less straight, and reflexed.</text>
      <biological_entity id="o16661" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o16662" from="1" name="quantity" src="d0_s2" to="18" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s2" to="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink-gray" value_original="pink-gray" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink-gray" value_original="pink-gray" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character constraint="on proximal stem segments" constraintid="o16664" is_modifier="false" name="course" src="d0_s2" value="curling" value_original="curling" />
        <character is_modifier="false" modifier="usually" name="orientation" notes="" src="d0_s2" value="ascending" value_original="ascending" />
        <character constraint="near stem segment apex" constraintid="o16665" is_modifier="false" modifier="more or less" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character char_type="range_value" constraint="near base" constraintid="o16666" from="deflexed" name="orientation" notes="" src="d0_s2" to="reflexed" />
        <character char_type="range_value" from="35" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s2" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s2" to="185" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="90" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16662" name="areole" name_original="areole" src="d0_s2" type="structure" />
      <biological_entity id="o16663" name="areole" name_original="areoles" src="d0_s2" type="structure" />
      <biological_entity constraint="stem" id="o16664" name="segment" name_original="segments" src="d0_s2" type="structure" constraint_original="proximal stem" />
      <biological_entity constraint="segment" id="o16665" name="apex" name_original="apex" src="d0_s2" type="structure" constraint_original="stem segment" />
      <biological_entity id="o16666" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o16667" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character is_modifier="false" modifier="less" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="stem" id="o16668" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
      </biological_entity>
      <relation from="o16661" id="r2404" name="in" negation="false" src="d0_s2" to="o16663" />
      <relation from="o16667" id="r2405" name="part_of" negation="false" src="d0_s2" to="o16668" />
    </statement>
    <statement id="d0_s3">
      <text>Fruits stout;</text>
      <biological_entity id="o16669" name="fruit" name_original="fruits" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>areoles 20–33, each bearing 7–13 spines, 8–20 mm. 2n = 44.</text>
      <biological_entity id="o16670" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s4" to="33" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16671" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s4" to="13" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16672" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="44" value_original="44" />
      </biological_entity>
      <relation from="o16670" id="r2406" name="bearing" negation="false" src="d0_s4" to="o16671" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deserts, desert grasslands, juniper woodlands, alluvial, sandy or gravelly soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deserts" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="juniper woodlands" />
        <character name="habitat" value="alluvial" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34e.</number>
  <other_name type="common_name">Mojave pricklypear</other_name>
  <other_name type="common_name">grizzly bear pricklypear</other_name>
  <discussion>Plants of Opuntia polyacantha var. erinacea with very long, wavy hairlike spines have been treated as O. ursina or O. erinacea var. ursina. Such plants, however, occur at the original (type) locality for var. erinaceae, and the characteristic spines may appear on the oldest stem of otherwise straight-spined plants. The name Opuntia erinacea has been widely misapplied to some plants of other varieties of O. polyacantha.</discussion>
  
</bio:treatment>