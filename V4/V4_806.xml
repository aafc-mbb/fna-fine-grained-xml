<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kenneth R. Robertson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="treatment_page">409</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Reichenbach" date="1828" rank="genus">HERMBSTAEDTIA</taxon_name>
    <place_of_publication>
      <publication_title>Consp. Regn. Veg.,</publication_title>
      <place_in_publication>164. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus HERMBSTAEDTIA</taxon_hierarchy>
    <other_info_on_name type="etymology">for Sigismund Friedrich Hermbstädt, 1760–1833, German botanist</other_info_on_name>
    <other_info_on_name type="fna_id">115167</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, perennial [annual].</text>
      <biological_entity id="o477" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o479" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, petiolate;</text>
      <biological_entity id="o480" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear.</text>
      <biological_entity id="o481" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, many-flowered spikes.</text>
      <biological_entity id="o482" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o483" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="many-flowered" value_original="many-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual;</text>
      <biological_entity id="o484" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals 5, distinct, scarious, usually glabrous;</text>
      <biological_entity id="o485" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments connate basally into tube;</text>
      <biological_entity id="o486" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character constraint="into tube" constraintid="o487" is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o487" name="tube" name_original="tube" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>anthers 2-locular or 4-locular;</text>
      <biological_entity id="o488" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s8" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s8" value="4-locular" value_original="4-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pseudostaminodes 2-lobed, alternating with filaments;</text>
      <biological_entity id="o489" name="pseudostaminode" name_original="pseudostaminodes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="2-lobed" value_original="2-lobed" />
        <character constraint="with filaments" constraintid="o490" is_modifier="false" name="arrangement" src="d0_s9" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o490" name="filament" name_original="filaments" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>ovules 2–several;</text>
      <biological_entity id="o491" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="false" name="quantity" src="d0_s10" to="several" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style persistent, 0.5 mm;</text>
      <biological_entity id="o492" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas [2–] 3, filiform.</text>
      <biological_entity id="o493" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s12" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Utricles cylindric-ovoid, membranous, dehiscence centrally circumscissile.</text>
      <biological_entity id="o494" name="utricle" name_original="utricles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric-ovoid" value_original="cylindric-ovoid" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="centrally" name="dehiscence" src="d0_s13" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 2–8, black, flattened.</text>
      <biological_entity id="o495" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; s Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Guineaflower</other_name>
  <discussion>Species ca. 5 (1 in the flora).</discussion>
  <references>
    <reference>Townsend, C. C. 1982. A new species of African Celosia and a new conspectus of Hermbstaedtia. Kew Bull. 37: 81–90.</reference>
  </references>
  
</bio:treatment>