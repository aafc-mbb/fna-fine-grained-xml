<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="treatment_page">227</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="(Engelmann) Lemaire" date="1868" rank="genus">coryphantha</taxon_name>
    <taxon_name authority="(Engelmann) Britton &amp; Rose" date="1923" rank="species">echinus</taxon_name>
    <place_of_publication>
      <publication_title>Cact.</publication_title>
      <place_in_publication>4: 42. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus coryphantha;species echinus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415321</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mammillaria</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">echinus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>3: 267. 1856 (as Mamillaria)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mammillaria;species echinus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coryphantha</taxon_name>
    <taxon_name authority="(de Candolle) Lemaire" date="unknown" rank="species">cornifera</taxon_name>
    <taxon_name authority="(Engelmann) L. D. Benson" date="unknown" rank="variety">echinus</taxon_name>
    <taxon_hierarchy>genus Coryphantha;species cornifera;variety echinus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coryphantha</taxon_name>
    <taxon_name authority="(Engelmann) Britton &amp; Rose" date="unknown" rank="species">pectinata</taxon_name>
    <taxon_hierarchy>genus Coryphantha;species pectinata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mammillaria</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">pectinata</taxon_name>
    <taxon_hierarchy>genus Mammillaria;species pectinata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually unbranched (branched with age, forming large clumps to 80 cm diam. at low elevation in Brewster County, Texas), usually relatively smooth except for protruding abaxial central spine, stem largely obscured by spines.</text>
      <biological_entity id="o11849" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character constraint="except-for abaxial central spine" constraintid="o11850" is_modifier="false" modifier="usually relatively" name="architecture_or_pubescence_or_relief" src="d0_s0" value="smooth" value_original="smooth" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="abaxial central" id="o11850" name="spine" name_original="spine" src="d0_s0" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s0" value="protruding" value_original="protruding" />
      </biological_entity>
      <biological_entity id="o11851" name="stem" name_original="stem" src="d0_s0" type="structure">
        <character constraint="by spines" constraintid="o11852" is_modifier="false" modifier="largely" name="prominence" src="d0_s0" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o11852" name="spine" name_original="spines" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse or short taproots.</text>
      <biological_entity id="o11853" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o11854" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="true" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems spheric (ovoid or conic with age) to cylindric, 3–15 (–20) × 3–7.6 cm;</text>
      <biological_entity id="o11855" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="spheric" name="shape" src="d0_s2" to="cylindric" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s2" to="7.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tubercles 8–12 × 6–11 mm, firm;</text>
      <biological_entity id="o11856" name="tubercle" name_original="tubercles" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="11" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>areolar glands seasonally conspicuous;</text>
      <biological_entity constraint="areolar" id="o11857" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="seasonally" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>parenchyma not mucilaginous;</text>
      <biological_entity id="o11858" name="parenchyma" name_original="parenchyma" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s5" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pith 1/4–1/3 of lesser stem diam.;</text>
      <biological_entity id="o11859" name="pith" name_original="pith" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="of stem" constraintid="o11860" from="1/4" name="quantity" src="d0_s6" to="1/3" />
      </biological_entity>
      <biological_entity id="o11860" name="stem" name_original="stem" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>medullary vascular system absent.</text>
      <biological_entity constraint="medullary" id="o11861" name="system" name_original="system" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="vascular" value_original="vascular" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spines 16–34 per areole, drab whitish, pale yellowish tan, or pale purplish gray, overlying relatively bright-yellow to dark yellowbrown inner layers, later gray with dark tips;</text>
      <biological_entity id="o11862" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o11863" from="16" name="quantity" src="d0_s8" to="34" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="drab whitish" value_original="drab whitish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale yellowish" value_original="pale yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale purplish" value_original="pale purplish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale purplish" value_original="pale purplish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="gray" value_original="gray" />
      </biological_entity>
      <biological_entity id="o11863" name="areole" name_original="areole" src="d0_s8" type="structure" />
      <biological_entity constraint="inner" id="o11864" name="layer" name_original="layers" src="d0_s8" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s8" value="overlying" value_original="overlying" />
        <character is_modifier="true" modifier="relatively" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="true" modifier="relatively" name="coloration" src="d0_s8" value="bright-yellow to dark yellowbrown" value_original="bright-yellow to dark yellowbrown" />
        <character constraint="with tips" constraintid="o11865" is_modifier="false" modifier="later" name="coloration" src="d0_s8" value="gray" value_original="gray" />
      </biological_entity>
      <biological_entity id="o11865" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>radial spines 15–25 (–29) per areole, 16–24 × 0.2–0.6 mm;</text>
      <biological_entity id="o11866" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="radial" value_original="radial" />
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="29" />
        <character char_type="range_value" constraint="per areole" constraintid="o11867" from="15" name="quantity" src="d0_s9" to="25" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" notes="" src="d0_s9" to="24" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" notes="" src="d0_s9" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11867" name="areole" name_original="areole" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>subcentral spines (0–) 2–3 (–4) per areole, erect;</text>
      <biological_entity constraint="subcentral" id="o11868" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s10" to="2" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="4" />
        <character char_type="range_value" constraint="per areole" constraintid="o11869" from="2" name="quantity" src="d0_s10" to="3" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s10" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o11869" name="areole" name_original="areole" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>central spines (0–) 1–4 (–11) per areole, larger spines and abaxial central spine porrect, straight or slightly curved downward (rarely strongly recurved), others appressed, abaxial (or only) central spine 11.5–25 × 0.3–0.9 mm, rigid, others slightly longer and thinner.</text>
      <biological_entity constraint="central" id="o11870" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s11" to="1" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="11" />
        <character char_type="range_value" constraint="per abaxial central spine" constraintid="o11874" from="1" name="quantity" src="d0_s11" to="4" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="downward" value_original="downward" />
      </biological_entity>
      <biological_entity id="o11871" name="areole" name_original="areole" src="d0_s11" type="structure" />
      <biological_entity id="o11872" name="spine" name_original="spines" src="d0_s11" type="structure" />
      <biological_entity constraint="larger" id="o11873" name="areole" name_original="areole" src="d0_s11" type="structure" />
      <biological_entity constraint="abaxial central" id="o11874" name="spine" name_original="spine" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="porrect" value_original="porrect" />
      </biological_entity>
      <biological_entity id="o11875" name="other" name_original="others" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="abaxial central" id="o11876" name="spine" name_original="spine" src="d0_s11" type="structure">
        <character char_type="range_value" from="11.5" from_unit="mm" name="length" src="d0_s11" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s11" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o11877" name="other" name_original="others" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="length_or_size" src="d0_s11" value="longer" value_original="longer" />
        <character is_modifier="false" name="width" src="d0_s11" value="thinner" value_original="thinner" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers nearly apical, 25–65 × 25–65 mm;</text>
      <biological_entity id="o11878" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="length" notes="" src="d0_s12" to="65" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" notes="" src="d0_s12" to="65" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11879" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="nearly" name="position" src="d0_s12" value="apical" value_original="apical" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>outer tepals entire;</text>
      <biological_entity constraint="outer" id="o11880" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>inner tepals 20–37 per flower, bright-yellow, sometimes proximally reddish, 22–34 × 4.5–12 mm;</text>
      <biological_entity constraint="inner" id="o11881" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per flower" constraintid="o11882" from="20" name="quantity" src="d0_s14" to="37" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s14" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" modifier="sometimes proximally" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="22" from_unit="mm" name="length" src="d0_s14" to="34" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11882" name="flower" name_original="flower" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>outer filaments reddish, reddish orange, or yellow;</text>
      <biological_entity constraint="outer" id="o11883" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers bright yellow-orange;</text>
      <biological_entity id="o11884" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="bright yellow-orange" value_original="bright yellow-orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma lobes 10–13, whitish or greenish yellow, 3–4 mm.</text>
      <biological_entity constraint="stigma" id="o11885" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s17" to="13" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="greenish yellow" value_original="greenish yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits green, ovoid, 12–28 × 10–19 mm, slimy;</text>
      <biological_entity id="o11886" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s18" to="28" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s18" to="19" to_unit="mm" />
        <character is_modifier="false" name="coating" src="d0_s18" value="slimy" value_original="slimy" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>floral remnant strongly persistent.</text>
      <biological_entity constraint="floral" id="o11887" name="remnant" name_original="remnant" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="strongly" name="duration" src="d0_s19" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds reddish-brown, somewhat comma-shaped, 1.7–1.9 mm, smooth, shiny.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 22 (as C. cornifera var. echinus).</text>
      <biological_entity id="o11888" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s20" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s20" value="comma--shaped" value_original="comma--shaped" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s20" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s20" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11889" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (Apr–Jul); fruiting 2-4 months after flowering.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Degraded grasslands, desert scrub, on and near limestone or igneous hills and benches, with Larrea</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="degraded grasslands" />
        <character name="habitat" value="desert scrub" constraint="on and near limestone or igneous hills and benches" />
        <character name="habitat" value="near limestone" />
        <character name="habitat" value="igneous hills" />
        <character name="habitat" value="benches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Sea-urchin cactus</other_name>
  <other_name type="common_name">rhinocerous cactus</other_name>
  <discussion>Mature plants of Coryphantha echinus are dimorphic with respect to presence/absence of porrect (inner or abaxial) central spines. Immature plants of most populations lack central spines, except in the southern and western populations where some individuals produce central spines even before sexual maturation. The name C. pectinata (Engelmann) Britton &amp; Rose was used for plants lacking central spines. Coryphantha echinus was recombined as variety of central Mexican C. cornifera (L. D. Benson 1969c); however, they belong to different species groups.</discussion>
  <discussion>The showy flowers of Coryphantha echinus are among the most ephemeral in the Cactaceae. They are fully expanded at high noon (if in brilliant sunlight) and wilt after only an hour or two. By mid afternoon, when most Chihuahuan Desert cacti are at the peak of anthesis, the flowers of C. echinus are tightly closed.</discussion>
  
</bio:treatment>