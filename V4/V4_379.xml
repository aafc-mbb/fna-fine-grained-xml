<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">198</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="treatment_page">203</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">sclerocactus</taxon_name>
    <taxon_name authority="(Boissevain &amp; C. Davidson) L. D. Benson" date="1966" rank="species">mesae-verdae</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>38: 54. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus sclerocactus;species mesae-verdae;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415290</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coloradoa</taxon_name>
    <taxon_name authority="Boissevain &amp; C. Davidson" date="unknown" rank="species">mesae-verdae</taxon_name>
    <place_of_publication>
      <publication_title>Colorado Cacti,</publication_title>
      <place_in_publication>55, figs. 38–40. 1940 (as mesae verdae)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Coloradoa;species mesae-verdae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocactus</taxon_name>
    <taxon_name authority="(Boissevain &amp; C. Davidson) L. D. Benson" date="unknown" rank="species">mesae-verdae</taxon_name>
    <taxon_hierarchy>genus Echinocactus;species mesae-verdae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ferocactus</taxon_name>
    <taxon_name authority="(Boissevain &amp; C. Davidson) N. P. Taylor" date="unknown" rank="species">mesae-verdae</taxon_name>
    <taxon_hierarchy>genus Ferocactus;species mesae-verdae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pediocactus</taxon_name>
    <taxon_name authority="(Boissevain &amp; C. Davidson) Arp" date="unknown" rank="species">mesae-verdae</taxon_name>
    <taxon_hierarchy>genus Pediocactus;species mesae-verdae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually unbranched (occasionally 2–3-branched), usually pale green, depressed-spheric to ovoid, usually 3.2–11 (–18) × 3.8–8 (–10) cm, somewhat glaucous;</text>
      <biological_entity id="o15211" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s0" value="pale green" value_original="pale green" />
        <character char_type="range_value" from="depressed-spheric" name="shape" src="d0_s0" to="ovoid" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="18" to_unit="cm" />
        <character char_type="range_value" from="3.2" from_unit="cm" name="length" src="d0_s0" to="11" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s0" to="10" to_unit="cm" />
        <character char_type="range_value" from="3.8" from_unit="cm" name="width" src="d0_s0" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="usually; somewhat" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>ribs 13–17, tubercles inconspicuous.</text>
      <biological_entity id="o15212" name="rib" name_original="ribs" src="d0_s1" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s1" to="17" />
      </biological_entity>
      <biological_entity id="o15213" name="tubercle" name_original="tubercles" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spines not obscuring stem, usually only radial;</text>
      <biological_entity id="o15214" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually only" name="arrangement" src="d0_s2" value="radial" value_original="radial" />
      </biological_entity>
      <biological_entity id="o15215" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o15214" id="r2202" name="obscuring" negation="false" src="d0_s2" to="o15215" />
    </statement>
    <statement id="d0_s3">
      <text>radial spines 7–14 per areole, spreading, straw colored, 6–13 mm, less than 1 mm diam.;</text>
      <biological_entity id="o15216" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o15217" from="7" name="quantity" src="d0_s3" to="14" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="straw colored" value_original="straw colored" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="13" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15217" name="areole" name_original="areole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>central spines 0 (–4) per areole (very rarely 1 abaxial central spine per areole hooked, 7–15 mm, brown).</text>
      <biological_entity constraint="central" id="o15218" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="4" />
        <character constraint="per areole" constraintid="o15219" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15219" name="areole" name_original="areole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers funnelform to campanulate, 1–3.5 × 1–3 cm;</text>
      <biological_entity id="o15220" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer tepals with purple midstripes and cream or gold (rarely pink) margins, oblanceolate, 10–25 × 5–8 mm;</text>
      <biological_entity constraint="outer" id="o15221" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="gold" value_original="gold" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15222" name="midstripe" name_original="midstripes" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o15223" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <relation from="o15221" id="r2203" name="with" negation="false" src="d0_s6" to="o15222" />
    </statement>
    <statement id="d0_s7">
      <text>inner tepals yellow to cream (rarely pink), oblanceolate, 15–30 × 5 mm;</text>
      <biological_entity constraint="inner" id="o15224" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s7" to="cream" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s7" to="30" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments pale-yellow or white;</text>
      <biological_entity id="o15225" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers yellow.</text>
      <biological_entity id="o15226" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits indehiscent to irregularly dehiscent, green, becoming tan at maturity, short cylindric, (4–) 8–10 × 7–9 mm.</text>
      <biological_entity id="o15227" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="indehiscent" name="dehiscence" src="d0_s10" to="irregularly dehiscent" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character constraint="at maturity" is_modifier="false" modifier="becoming" name="coloration" src="d0_s10" value="tan" value_original="tan" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s10" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds black, 2.5–3 × 3–4 mm;</text>
      <biological_entity id="o15228" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>testa cells convex but somewhat flattened apically.</text>
      <biological_entity constraint="testa" id="o15229" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="somewhat; apically" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Apr-early May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early May" from="late Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline, clay badlands, mat saltbush communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline" />
        <character name="habitat" value="clay badlands" />
        <character name="habitat" value="saltbush communities" modifier="mat" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Mesa Verde fishhook cactus</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Sclerocactus mesae-verdae is characterized by its relatively short radial spines and the general absence of central spines. In addition, no other Sclerocactus in Colorado or New Mexico has flowers that are cream to yellow (or pale pink).</discussion>
  <discussion>Sclerocactus mesae-verdae is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>