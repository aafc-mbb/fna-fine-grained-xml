<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">sclerocactus</taxon_name>
    <taxon_name authority="K. D. Heil &amp; J. M. Porter" date="unknown" rank="species">cloverae</taxon_name>
    <place_of_publication>
      <publication_title>Haseltonia</publication_title>
      <place_in_publication>2: 31. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus sclerocactus;species cloverae;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415289</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sclerocactus</taxon_name>
    <taxon_name authority="(Engelmann &amp; J. M. Bigelow) Britton &amp; Rose" date="unknown" rank="species">whipplei</taxon_name>
    <taxon_name authority="Castetter" date="unknown" rank="variety">heilii</taxon_name>
    <place_of_publication>
      <publication_title>P. Pierce &amp; K. H. Schwerin, Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>48: 79, figs. 3, 4. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sclerocactus;species whipplei;variety heilii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pediocactus</taxon_name>
    <taxon_name authority="(K. D. Heil &amp; J. M. Porter) Halda" date="unknown" rank="species">cloverae</taxon_name>
    <taxon_hierarchy>genus Pediocactus;species cloverae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sclerocactus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cloverae</taxon_name>
    <taxon_name authority="K. D. Heil &amp; J. M. Porter" date="unknown" rank="subspecies">brackii</taxon_name>
    <taxon_hierarchy>genus Sclerocactus;species cloverae;subspecies brackii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sclerocactus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">whipplei</taxon_name>
    <taxon_name authority="Castetter" date="unknown" rank="variety">reevesii</taxon_name>
    <place_of_publication>
      <publication_title>P. Pierce &amp; K. H. Schwerin</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus Sclerocactus;species whipplei;variety reevesii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems unbranched (occasionally 2–3-branched near base), green, ovoid to elongate-cylindric, 2.9–25 (–35) × 2.8–12.5 (–20) cm;</text>
      <biological_entity id="o15594" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s0" to="elongate-cylindric" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="2.9" from_unit="cm" name="length" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="12.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s0" to="20" to_unit="cm" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="width" src="d0_s0" to="12.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>ribs usually (11–) 13 (–15), well developed, tubercles evident on ribs.</text>
      <biological_entity id="o15595" name="rib" name_original="ribs" src="d0_s1" type="structure">
        <character char_type="range_value" from="11" name="atypical_quantity" src="d0_s1" to="13" to_inclusive="false" />
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="15" />
        <character name="quantity" src="d0_s1" value="13" value_original="13" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s1" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o15596" name="tubercle" name_original="tubercles" src="d0_s1" type="structure">
        <character constraint="on ribs" constraintid="o15597" is_modifier="false" name="prominence" src="d0_s1" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o15597" name="rib" name_original="ribs" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Spines obscuring stems;</text>
      <biological_entity id="o15598" name="spine" name_original="spines" src="d0_s2" type="structure" />
      <biological_entity id="o15599" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <relation from="o15598" id="r2270" name="obscuring" negation="false" src="d0_s2" to="o15599" />
    </statement>
    <statement id="d0_s3">
      <text>radial spines 4–6 per areole, acicular, elliptic or rhombic in cross-section, 19 × 1.3–2 mm;</text>
      <biological_entity id="o15600" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o15601" from="4" name="quantity" src="d0_s3" to="6" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="acicular" value_original="acicular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character constraint="in cross-section" constraintid="o15602" is_modifier="false" name="shape" src="d0_s3" value="rhombic" value_original="rhombic" />
        <character name="length" notes="" src="d0_s3" unit="mm" value="19" value_original="19" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" notes="" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15601" name="areole" name_original="areole" src="d0_s3" type="structure" />
      <biological_entity id="o15602" name="cross-section" name_original="cross-section" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>central spines 6–9 per areole, usually 8;</text>
      <biological_entity constraint="central" id="o15603" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o15604" from="6" name="quantity" src="d0_s4" to="9" />
      </biological_entity>
      <biological_entity id="o15604" name="areole" name_original="areole" src="d0_s4" type="structure">
        <character modifier="usually" name="quantity" src="d0_s4" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial central spine usually 1 per areole, porrect, straw colored to brown, highlighted with purple or red, terete or somewhat angled, hooked, (15–) 30–46 × 1.5 mm;</text>
      <biological_entity constraint="abaxial central" id="o15605" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character constraint="per areole" constraintid="o15606" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s5" value="porrect" value_original="porrect" />
        <character char_type="range_value" from="straw colored" name="coloration" src="d0_s5" to="brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="highlighted with purple or highlighted with red" />
        <character name="coloration" src="d0_s5" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s5" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s5" value="hooked" value_original="hooked" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s5" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s5" to="46" to_unit="mm" />
        <character name="width" src="d0_s5" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o15606" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>lateral central spines 5–8 per areole, similar to abaxial but slightly shorter and usually not hooked;</text>
      <biological_entity constraint="lateral central" id="o15607" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o15608" from="5" name="quantity" src="d0_s6" to="8" />
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s6" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity id="o15608" name="areole" name_original="areole" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial" id="o15609" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <relation from="o15607" id="r2271" name="to" negation="false" src="d0_s6" to="o15609" />
    </statement>
    <statement id="d0_s7">
      <text>adaxial central erect, white or straw colored, straight or curved, angled to flat, somewhat inconspicuous, triangular in cross-section, 25–55 × 1–2 mm.</text>
      <biological_entity constraint="adaxial central" id="o15610" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="straw colored" value_original="straw colored" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s7" value="angled to flat" value_original="angled to flat" />
        <character is_modifier="false" modifier="somewhat" name="prominence" src="d0_s7" value="inconspicuous" value_original="inconspicuous" />
        <character constraint="in cross-section" constraintid="o15611" is_modifier="false" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" notes="" src="d0_s7" to="55" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15611" name="cross-section" name_original="cross-section" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers narrowly funnelform to campanulate, 2.5–3.5 (–4) × 1.6–3.1 (–3.6) cm;</text>
      <biological_entity id="o15612" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s8" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="3.1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s8" to="3.6" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="width" src="d0_s8" to="3.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer tepals with greenish to purple with brownish midstripes, pink, purple, or whitish margins, larger ones oblanceolate, 10–18 × 4–7.5 mm, margins membranous and crisped or minutely toothed, apex mucronate;</text>
      <biological_entity constraint="outer" id="o15613" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="with midstripes" constraintid="o15614" from="greenish" name="coloration" src="d0_s9" to="purple" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="larger" value_original="larger" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s9" to="18" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15614" name="midstripe" name_original="midstripes" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o15615" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <biological_entity id="o15616" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s9" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o15617" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner tepals purple, sometimes suffused with brown, largest tepals oblanceolate, 15–22 (–30) × 4–6 mm, margins irregularly toothed, apex mucronate;</text>
      <biological_entity constraint="inner" id="o15618" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="suffused with brown" value_original="suffused with brown" />
      </biological_entity>
      <biological_entity constraint="largest" id="o15619" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="22" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15620" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s10" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o15621" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments white, tinged with pink to pink-purple;</text>
      <biological_entity id="o15622" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="punct" value_original="punct" />
        <character char_type="range_value" from="tinged with pink" name="coloration" src="d0_s11" to="pink-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers yellow;</text>
      <biological_entity id="o15623" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary minutely papillate, appearing smooth.</text>
      <biological_entity id="o15624" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits irregularly dehiscent or dehiscent through basal abscission pore, green to tan, sometimes suffused with pink, 7–15 × 5–12 mm, dry;</text>
      <biological_entity id="o15625" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
        <character constraint="through basal pore" constraintid="o15626" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s14" to="tan" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="suffused with pink" value_original="suffused with pink" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s14" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s14" to="12" to_unit="mm" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s14" value="dry" value_original="dry" />
      </biological_entity>
      <biological_entity id="o15626" name="pore" name_original="pore" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="through" name="position" src="d0_s14" value="basal" value_original="basal" />
        <character is_modifier="true" name="life_cycle" src="d0_s14" value="abscission" value_original="abscission" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>scales few, membranous, scarious-margined, minutely toothed or fringed.</text>
      <biological_entity id="o15627" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="few" value_original="few" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="scarious-margined" value_original="scarious-margined" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s15" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds brown or black, 1.2–2.5 × 1.9–3.5 mm;</text>
      <biological_entity id="o15628" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="black" value_original="black" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s16" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="width" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>testa with rounded papillae.</text>
      <biological_entity id="o15629" name="testa" name_original="testa" src="d0_s17" type="structure" />
      <biological_entity id="o15630" name="papilla" name_original="papillae" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o15629" id="r2272" name="with" negation="false" src="d0_s17" to="o15630" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Apr-early Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jun" from="late Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, gravelly, or clay hills, mesas, and washes, desert grasslands, saltbush, sagebrush, and rabbitbrush flats, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" modifier="sandy" />
        <character name="habitat" value="clay hills" />
        <character name="habitat" value="mesas" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="rabbitbrush flats" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="past_name">cloveriae</other_name>
  <other_name type="common_name">Clover eagle-claw cactus</other_name>
  <other_name type="common_name">Clover’s fishhook cactus</other_name>
  <discussion>Sclerocactus cloverae is characterized by its very dense spines and small (relative to S. parviflorus) purple flowers. Phylogenetic analyses of chloroplast DNA sequences support a close relationship among S. cloverae, S. whipplei, and S. parviflorus (J. M. Porter et al. 2000). Populations with all reproductive individuals maintaining juvenile morphology have been segregated as S. cloverae subsp. brackii K. D. Heil &amp; J. M. Porter.</discussion>
  
</bio:treatment>