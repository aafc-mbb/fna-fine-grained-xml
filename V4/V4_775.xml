<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">391</other_info_on_meta>
    <other_info_on_meta type="treatment_page">392</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Forsskål ex J. F. Gmelin" date="unknown" rank="genus">suaeda</taxon_name>
    <taxon_name authority="(Moquin-Tandon) Volkens in H. G. A. Engler and K. Prantl" date="1893" rank="section">Brezia</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>79[III,1a]: 80. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus suaeda;section Brezia</taxon_hierarchy>
    <other_info_on_name type="fna_id">304250</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Moquin-Tandon" date="unknown" rank="section">Brezia</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>13(2): 167. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>section Brezia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Suaeda</taxon_name>
    <taxon_name authority="Iljin" date="unknown" rank="section">Heterosperma</taxon_name>
    <taxon_hierarchy>genus Suaeda;section Heterosperma;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, annual or perennial, glabrous.</text>
      <biological_entity id="o13827" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves sessile, fleshy, cross-sections of fresh leaves ± uniformly green when seen at 10× magnification.</text>
      <biological_entity id="o13829" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o13830" name="cross-section" name_original="cross-sections" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when seen at 10×magnification" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o13831" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="condition" src="d0_s1" value="fresh" value_original="fresh" />
      </biological_entity>
      <relation from="o13830" id="r2000" name="part_of" negation="false" src="d0_s1" to="o13831" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences glomes, each glome in axis of one leaflike bract;</text>
      <biological_entity constraint="inflorescences" id="o13832" name="glome" name_original="glomes" src="d0_s2" type="structure" />
      <biological_entity id="o13833" name="glome" name_original="glome" src="d0_s2" type="structure" />
      <biological_entity id="o13834" name="axis" name_original="axis" src="d0_s2" type="structure" />
      <biological_entity id="o13835" name="bract" name_original="bract" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <relation from="o13833" id="r2001" name="in" negation="false" src="d0_s2" to="o13834" />
      <relation from="o13834" id="r2002" name="part_of" negation="false" src="d0_s2" to="o13835" />
    </statement>
    <statement id="d0_s3">
      <text>bracteoles subtending glomes without marginal hairs.</text>
      <biological_entity id="o13836" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure" />
      <biological_entity id="o13837" name="glome" name_original="glomes" src="d0_s3" type="structure" />
      <biological_entity constraint="marginal" id="o13838" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <relation from="o13836" id="r2003" name="subtending" negation="false" src="d0_s3" to="o13837" />
      <relation from="o13836" id="r2004" name="without" negation="false" src="d0_s3" to="o13838" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers bisexual or sometimes unisexual;</text>
      <biological_entity id="o13839" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s4" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>perianth irregular or zygomorphic, sometimes appearing ± actinomorphic, globose to wider than long;</text>
      <biological_entity id="o13840" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="zygomorphic" value_original="zygomorphic" />
        <character is_modifier="false" modifier="sometimes; more or less" name="architecture" src="d0_s5" value="actinomorphic" value_original="actinomorphic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="globose" value_original="globose" />
        <character is_modifier="false" name="width" src="d0_s5" value="wider than long" value_original="wider than long" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth segments connate proximally (sometimes forming fleshy disk), succulent or thin, abaxially flat or rounded, sometimes distally hooded, and/or with abaxial appendages (transverse proximal wings, keels, and/or distal horns), apex obtuse to acute;</text>
      <biological_entity constraint="perianth" id="o13841" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character is_modifier="false" name="texture" src="d0_s6" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes distally" name="architecture" src="d0_s6" value="hooded" value_original="hooded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13842" name="appendage" name_original="appendages" src="d0_s6" type="structure" />
      <biological_entity id="o13843" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
      <relation from="o13841" id="r2005" name="with" negation="false" src="d0_s6" to="o13842" />
    </statement>
    <statement id="d0_s7">
      <text>ovary ovoid, transversely ellipsoid, or pyriform;</text>
      <biological_entity id="o13844" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pyriform" value_original="pyriform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigmas 2–5, inserted on attenuated apex of ovary, linear, smooth or papillate.</text>
      <biological_entity id="o13845" name="stigma" name_original="stigmas" src="d0_s8" type="structure" constraint="ovary" constraint_original="ovary; ovary">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="5" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s8" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o13846" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="attenuated" value_original="attenuated" />
      </biological_entity>
      <biological_entity id="o13847" name="ovary" name_original="ovary" src="d0_s8" type="structure" />
      <relation from="o13845" id="r2006" name="inserted on" negation="false" src="d0_s8" to="o13846" />
      <relation from="o13845" id="r2007" name="part_of" negation="false" src="d0_s8" to="o13847" />
    </statement>
    <statement id="d0_s9">
      <text>Seeds horizontal, strongly dimorphic in some species and individuals;</text>
      <biological_entity id="o13850" name="individual" name_original="individuals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lenticular with seed-coat black, blackish brown, or blackish red, shiny, and easily removable from pericarp;</text>
      <biological_entity id="o13851" name="seed-coat" name_original="seed-coat" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blackish brown" value_original="blackish brown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blackish red" value_original="blackish red" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blackish brown" value_original="blackish brown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="blackish red" value_original="blackish red" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="shiny" value_original="shiny" />
        <character constraint="from pericarp" constraintid="o13852" is_modifier="false" modifier="easily" name="fixation" src="d0_s10" value="removable" value_original="removable" />
      </biological_entity>
      <biological_entity id="o13852" name="pericarp" name_original="pericarp" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>or flat to discoid and coiled with seed-coat brown, dull, and not easily removed from pericarp.</text>
      <biological_entity id="o13848" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="horizontal" value_original="horizontal" />
        <character constraint="in individuals" constraintid="o13850" is_modifier="false" modifier="strongly" name="growth_form" src="d0_s9" value="dimorphic" value_original="dimorphic" />
        <character constraint="with seed-coat" constraintid="o13851" is_modifier="false" name="shape" src="d0_s10" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="flat to discoid" value_original="flat to discoid" />
      </biological_entity>
      <biological_entity id="o13853" name="seed-coat" name_original="seed-coat" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="coiled" value_original="coiled" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="reflectance" src="d0_s11" value="dull" value_original="dull" />
      </biological_entity>
      <biological_entity id="o13854" name="pericarp" name_original="pericarp" src="d0_s11" type="structure" />
      <relation from="o13853" id="r2008" modifier="not easily; easily" name="removed from" negation="false" src="d0_s11" to="o13854" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25b.</number>
  <discussion>Species ca. 40 (6 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth segments thin to abaxially rounded, without appendages</description>
      <determination>2 Suaeda maritima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth segments abaxially rounded, one or more segments with abaxial appendages (transverse proximal wings, keels, and/or distal horns)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perianth segments with one or more segments transversely winged proximally and/or horned distally</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perianth segments not horned or winged</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Glomes 3-7-flowered; bracts usually broadest proximal to middle and thin- margined at base; branches usually ascending</description>
      <determination>3 Suaeda calceoliformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Glomes 1-3-flowered; bracts usually broadest at ± middle, not thin-margined at base; branches usually spreading</description>
      <determination>4 Suaeda occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades linear-lanceolate, usually not narrowed basally; glomes usually 3-5- flowered; Pacific coast</description>
      <determination>5 Suaeda esteroa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades linear, narrowed basally; glomes usually 1-3-flowered; Atlantic coast</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers usually crowded in compound spikes; perianth 1-2.5 mm diam.; len- ticular seeds 1-1.8 mm diam</description>
      <determination>6 Suaeda linearis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers usually distributed throughout plant; perianth 2-4 mm diam.; lenticu- lar seeds 1.5-2.3 mm diam</description>
      <determination>7 Suaeda rolandii</determination>
    </key_statement>
  </key>
</bio:treatment>