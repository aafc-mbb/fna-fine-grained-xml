<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">315</other_info_on_meta>
    <other_info_on_meta type="mention_page">316</other_info_on_meta>
    <other_info_on_meta type="treatment_page">317</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">corispermum</taxon_name>
    <taxon_name authority="Mosyakin" date="1995" rank="species">navicula</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>5: 349. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus corispermum;species navicula</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415484</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants branched from the base or nearly so, 5–15 (–25) cm, sparsely covered with dendroid or stellate hairs, or almost glabrous.</text>
      <biological_entity id="o4747" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="from " constraintid="o4749" is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="almost" name="pubescence" notes="" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4748" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o4749" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="dendroid" value_original="dendroid" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity id="o4750" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o4751" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="dendroid" value_original="dendroid" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" constraint="from " constraintid="o4753" from="5" from_unit="cm" name="location" src="d0_s0" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4752" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o4753" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="dendroid" value_original="dendroid" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o4749" id="r632" name="from" negation="false" src="d0_s0" to="o4750" />
      <relation from="o4749" id="r633" name="from" negation="false" src="d0_s0" to="o4751" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades linear-lanceolate, linear, occasionally narrowly lanceolate, usually plane, (1.5–) 2–4 × 0.1–0.5 cm.</text>
      <biological_entity id="o4754" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s1" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="occasionally narrowly" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s1" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences compact and dense, ovoid, ovate or oblong-obovate.</text>
      <biological_entity id="o4755" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="compact" value_original="compact" />
        <character is_modifier="false" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-obovate" value_original="oblong-obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bracts ovate or ovatelanceolate (occasionally proximal ones leaflike, narrowly ovatelanceolate or lanceolate), 0.5–2 × 0.2–0.6 cm.</text>
      <biological_entity id="o4756" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Perianth segment 1.</text>
      <biological_entity constraint="perianth" id="o4757" name="segment" name_original="segment" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits brown, dark-brown, or deep olive green, usually with numerous reddish-brown spots and whitish warts, strongly convex abaxially, usually strongly concave adaxially, elongate-obovate or obovate-elliptic, broadest beyond middle, (4.2–) 4.5–5 (–5.2) × 2.5–3 mm;</text>
      <biological_entity id="o4758" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="olive green" value_original="olive green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="olive green" value_original="olive green" />
        <character is_modifier="false" modifier="strongly; abaxially" name="shape" notes="" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="usually strongly; adaxially" name="shape" src="d0_s5" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate-obovate" value_original="elongate-obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate-elliptic" value_original="obovate-elliptic" />
        <character constraint="beyond middle fruits" constraintid="o4760" is_modifier="false" name="width" src="d0_s5" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity id="o4759" name="wart" name_original="warts" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="reddish-brown spots" value_original="reddish-brown spots" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity constraint="middle" id="o4760" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.2" from_unit="mm" name="atypical_length" notes="alterIDs:o4760" src="d0_s5" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" notes="alterIDs:o4760" src="d0_s5" to="5.2" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" notes="alterIDs:o4760" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" notes="alterIDs:o4760" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o4758" id="r634" modifier="usually" name="with" negation="false" src="d0_s5" to="o4759" />
    </statement>
    <statement id="d0_s6">
      <text>wing not translucent or translucent only at margin, thick, 0.1–0.2 (–0.3) mm wide (occasionally nearly absent), margins entire or irregularly erose, usually involute toward adaxial face of fruit, apex rostrate, triangular (wing long-adnate to style bases).</text>
      <biological_entity id="o4761" name="wing" name_original="wing" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="coloration_or_reflectance" src="d0_s6" value="translucent" value_original="translucent" />
        <character constraint="at margin" constraintid="o4762" is_modifier="false" name="coloration_or_reflectance" src="d0_s6" value="translucent" value_original="translucent" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="mm" name="width" src="d0_s6" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s6" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4762" name="margin" name_original="margin" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" notes="" src="d0_s6" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o4763" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s6" value="erose" value_original="erose" />
        <character constraint="toward adaxial face" constraintid="o4764" is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4764" name="face" name_original="face" src="d0_s6" type="structure" />
      <biological_entity id="o4765" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o4766" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rostrate" value_original="rostrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
      <relation from="o4764" id="r635" name="part_of" negation="false" src="d0_s6" to="o4765" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand dunes, probably also sandy and gravely shores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="sandy" modifier="probably also" />
        <character name="habitat" value="shores" modifier="gravely" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="2500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Boat-shaped bugseed</other_name>
  <discussion>Corispermum navicula is very similar in its fruit morphology to the Siberian species C. bardunovii M. Popov ex M. Lomonosova (M. N. Lomonosova 1992). Probably, the two taxa represent results of parallel evolution (or parallel variability?) within North American and Asian representatives of the same species aggregate. The most distinctive character of both C. navicula and C. bardunovii, an elongated fruit body with almost parallel margins in the middle portion and distinctly triangular apex, shows a transition toward representatives of Corispermum sect. Declinata Mosyakin. Additional study of C. navicula would help clarify its relationships with other species. Some specimens from Oklahoma may also belong to C. navicula.</discussion>
  
</bio:treatment>