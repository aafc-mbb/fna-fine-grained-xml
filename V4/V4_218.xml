<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="treatment_page">114</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="(Engelmann) F. M. Knuth in C. Backeberg and F. M. Knuth" date="1935" rank="genus">cylindropuntia</taxon_name>
    <taxon_name authority="(Hester) Backeberg" date="1958" rank="species">abyssi</taxon_name>
    <place_of_publication>
      <publication_title>Cactaceae</publication_title>
      <place_in_publication>1: 184. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus cylindropuntia;species abyssi;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415162</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Hester" date="unknown" rank="species">abyssi</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>15: 193, fig. 94 (upper left, bottom). 1943</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Opuntia;species abyssi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees and shrubs, openly branched, to 1 m.</text>
      <biological_entity id="o19004" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="openly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" modifier="openly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem segments somewhat detachable, 8–14 × 1.8–2.5 cm;</text>
      <biological_entity constraint="stem" id="o19006" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="somewhat" name="fragility" src="d0_s1" value="detachable" value_original="detachable" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s1" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tubercles prominent, 0.6–1.5 cm, moderately broad;</text>
      <biological_entity id="o19007" name="tubercle" name_original="tubercles" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s2" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s2" value="broad" value_original="broad" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles elliptic, 5–7 × 2.5–3.5 mm;</text>
      <biological_entity id="o19008" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>wool white to gray.</text>
      <biological_entity id="o19009" name="wool" name_original="wool" src="d0_s4" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s4" to="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spines in brushlike clusters of 10–15 per areole, flexible, whitish to yellowish tan, aging gray;</text>
      <biological_entity id="o19010" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o19011" from="10" modifier="of" name="quantity" src="d0_s5" to="15" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s5" value="pliable" value_original="flexible" />
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s5" to="yellowish tan" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="gray" value_original="gray" />
      </biological_entity>
      <biological_entity id="o19011" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>abaxial ones erect to descending, recurved, angularly flattened to flattened, sometimes twisted, the longest 23–38 mm;</text>
      <biological_entity constraint="abaxial" id="o19012" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="descending" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="angularly flattened" name="shape" src="d0_s6" to="flattened" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="length" src="d0_s6" value="longest" value_original="longest" />
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s6" to="38" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>adaxial ones ascending, erect, divergent, terete to angularly flattened basally, the longest 18–32 mm;</text>
      <biological_entity constraint="adaxial" id="o19013" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="terete" modifier="basally" name="shape" src="d0_s7" to="angularly flattened" />
        <character is_modifier="false" name="length" src="d0_s7" value="longest" value_original="longest" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s7" to="32" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sheaths silvery white.</text>
      <biological_entity id="o19014" name="sheath" name_original="sheaths" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="silvery white" value_original="silvery white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glochids in inconspicuous small adaxial tuft, pale-yellow, 0.5–1.5 mm, few much longer and scattered along periphery of areole.</text>
      <biological_entity id="o19015" name="glochid" name_original="glochids" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="few" value_original="few" />
        <character is_modifier="false" modifier="much" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
        <character constraint="along periphery" constraintid="o19017" is_modifier="false" name="arrangement" src="d0_s9" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19016" name="tuft" name_original="tuft" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="true" name="size" src="d0_s9" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o19017" name="periphery" name_original="periphery" src="d0_s9" type="structure" />
      <biological_entity id="o19018" name="areole" name_original="areole" src="d0_s9" type="structure" />
      <relation from="o19015" id="r2761" name="in" negation="false" src="d0_s9" to="o19016" />
      <relation from="o19017" id="r2762" name="part_of" negation="false" src="d0_s9" to="o19018" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: inner tepals pale-yellow to greenish yellow, spatulate, 15–20 mm, apiculate;</text>
      <biological_entity id="o19019" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="inner" id="o19020" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s10" to="greenish yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments yellow;</text>
      <biological_entity id="o19021" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19022" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers yellow;</text>
      <biological_entity id="o19023" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o19024" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style off-white;</text>
      <biological_entity id="o19025" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19026" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="off-white" value_original="off-white" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma lobes yellowish.</text>
      <biological_entity id="o19027" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="stigma" id="o19028" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits green becoming dull yellow, dry, tuberculate, spineless or with 1–2 short spines;</text>
      <biological_entity id="o19029" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" modifier="becoming" name="reflectance" src="d0_s15" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s15" value="dry" value_original="dry" />
        <character is_modifier="false" name="relief" src="d0_s15" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="spineless" value_original="spineless" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="with 1-2 short spines" value_original="with 1-2 short spines" />
      </biological_entity>
      <biological_entity id="o19030" name="spine" name_original="spines" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s15" to="2" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
      </biological_entity>
      <relation from="o19029" id="r2763" name="with" negation="false" src="d0_s15" to="o19030" />
    </statement>
    <statement id="d0_s16">
      <text>tubercles subequal in length;</text>
      <biological_entity id="o19031" name="tubercle" name_original="tubercles" src="d0_s16" type="structure">
        <character is_modifier="false" name="length" src="d0_s16" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>umbilicus deep, 7 × 15–18 mm;</text>
      <biological_entity id="o19032" name="umbilicus" name_original="umbilicus" src="d0_s17" type="structure">
        <character is_modifier="false" name="depth" src="d0_s17" value="deep" value_original="deep" />
        <character name="length" src="d0_s17" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s17" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>areoles 16–24.</text>
      <biological_entity id="o19033" name="areole" name_original="areoles" src="d0_s18" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s18" to="24" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds tan, slightly angular and warped, 3–4 × 3.2–3.5 mm;</text>
      <biological_entity id="o19034" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="slightly" name="arrangement_or_shape" src="d0_s19" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s19" value="warped" value_original="warped" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s19" to="4" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="width" src="d0_s19" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>girdle smooth.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 22.</text>
      <biological_entity id="o19035" name="girdle" name_original="girdle" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19036" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer (Mar–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert scrub, limestone ledges and crests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="limestone ledges" />
        <character name="habitat" value="crests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Peach Springs cholla</other_name>
  <discussion>Cylindropuntia abyssi is tentatively viewed as a narrow endemic relict in Peach Springs Canyon, Mojave County, Arizona, but may well be a persistent hybrid derivative, most likely involving C. bigelovii.</discussion>
  <discussion>Hybrids between Cylindropuntia abyssi and C. acanthocarpa have a shrub habit, long-divergent and loosely sheathed spines, and more elongate tubercles than C. abyssi.</discussion>
  
</bio:treatment>