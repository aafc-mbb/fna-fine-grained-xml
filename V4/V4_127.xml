<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="treatment_page">65</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">abronia</taxon_name>
    <taxon_name authority="Douglas ex Hooker" date="1829" rank="species">mellifera</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>56: plate 2879. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus abronia;species mellifera</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415088</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o22122" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending, much branched, elongate, glabrous or glandular-pubescent.</text>
      <biological_entity id="o22123" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1–6 cm;</text>
      <biological_entity id="o22124" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22125" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to lance-elliptic, 1–6 × 0.5–4 cm, margins entire to sinuate and ± undulate, surfaces glabrous or glandular-pubescent.</text>
      <biological_entity id="o22126" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22127" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="lance-elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22128" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s3" to="sinuate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o22129" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: peduncle longer than subtending petiole;</text>
      <biological_entity id="o22130" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o22131" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character constraint="than subtending petiole" constraintid="o22132" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o22132" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracts lanceolate to obovate, 5–12 × 1–5 mm, papery, glabrate to glandular-pubescent;</text>
      <biological_entity id="o22133" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o22134" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="papery" value_original="papery" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s5" to="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flowers 25–60.</text>
      <biological_entity id="o22135" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o22136" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s6" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perianth: tube pale rose proximally to greenish distally, 15–25 mm, limb white, 7–12 mm diam.</text>
      <biological_entity id="o22137" name="perianth" name_original="perianth" src="d0_s7" type="structure" />
      <biological_entity id="o22138" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="pale rose" value_original="pale rose" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22139" name="limb" name_original="limb" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits winged, broadly obdeltate or cordate in profile, 6–10 × 4–10 mm, thin, usually coriaceous, rarely indurate, base attenuate, apex prominently beaked;</text>
      <biological_entity id="o22140" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="obdeltate" value_original="obdeltate" />
        <character is_modifier="false" modifier="in profile" name="shape" src="d0_s8" value="cordate" value_original="cordate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s8" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" modifier="rarely" name="texture" src="d0_s8" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o22141" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o22142" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_shape" src="d0_s8" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>wings (2–) 5 (when 2, folded to form single deep groove), without dilations, broad, thin, without cavities.</text>
      <biological_entity id="o22143" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o22144" name="dilation" name_original="dilations" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" notes="" src="d0_s9" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o22145" name="cavity" name_original="cavities" src="d0_s9" type="structure" />
      <relation from="o22143" id="r3211" name="without" negation="false" src="d0_s9" to="o22144" />
      <relation from="o22143" id="r3212" name="without" negation="false" src="d0_s9" to="o22145" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, cold desert scrub, grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="cold desert scrub" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  
</bio:treatment>