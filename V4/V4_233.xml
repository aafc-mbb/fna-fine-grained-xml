<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="treatment_page">121</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="F. Reichenbach ex K. Schumann" date="1896" rank="genus">grusonia</taxon_name>
    <taxon_name authority="(Engelmann) H. Robinson" date="1973" rank="species">schottii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>26: 176. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus grusonia;species schottii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415176</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Opuntia</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">schottii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>3: 304. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Opuntia;species schottii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corynopuntia</taxon_name>
    <taxon_name authority="(Engelmann) F. M. Knuth" date="unknown" rank="species">schottii</taxon_name>
    <taxon_hierarchy>genus Corynopuntia;species schottii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, forming mats, 7.5–9 cm.</text>
      <biological_entity id="o9905" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7.5" from_unit="cm" name="some_measurement" src="d0_s0" to="9" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o9906" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o9905" id="r1438" name="forming" negation="false" src="d0_s0" to="o9906" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse.</text>
      <biological_entity id="o9907" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem segments clavate, (2–) 3.5–6.5 × (1.5–) 2–3 cm;</text>
      <biological_entity constraint="stem" id="o9908" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s2" to="3.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s2" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_width" src="d0_s2" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tubercles 8–20 mm, 1–3.5 times longer than wide, not or little obscured by spines;</text>
      <biological_entity id="o9909" name="tubercle" name_original="tubercles" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="1-3.5" value_original="1-3.5" />
        <character constraint="by spines" constraintid="o9910" is_modifier="false" modifier="not" name="prominence" src="d0_s3" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o9910" name="spine" name_original="spines" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>areoles 2.5–4 mm in diam.;</text>
      <biological_entity id="o9911" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diam" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>wool white to yellowish.</text>
      <biological_entity id="o9912" name="wool" name_original="wool" src="d0_s5" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s5" to="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spines 11–17 per areole, mostly in distal areoles, spreading to deflexed, white, tan, or redbrown, longest 3–7 cm;</text>
      <biological_entity id="o9914" name="areole" name_original="areole" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o9915" name="areole" name_original="areoles" src="d0_s6" type="structure" />
      <relation from="o9913" id="r1439" modifier="mostly" name="in" negation="false" src="d0_s6" to="o9915" />
    </statement>
    <statement id="d0_s7">
      <text>major 3 abaxial spines divergent or deflexed, tan to brown, flattened to angular-flattened, sharp-edged;</text>
      <biological_entity id="o9913" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o9914" from="11" name="quantity" src="d0_s6" to="17" />
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s6" to="deflexed" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="length" src="d0_s6" value="longest" value_original="longest" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="7" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s7" value="major" value_original="major" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>major 2–3 adaxial spines ascending-divergent, redbrown, subterete to angular-flattened.</text>
      <biological_entity constraint="abaxial" id="o9916" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="deflexed" value_original="deflexed" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s7" to="brown" />
        <character char_type="range_value" from="flattened" name="shape" src="d0_s7" to="angular-flattened" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sharp-edged" value_original="sharp-edged" />
        <character is_modifier="false" name="size" src="d0_s8" value="major" value_original="major" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9917" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="3" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="ascending-divergent" value_original="ascending-divergent" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s8" to="angular-flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glochids adaxial in areole, yellowish white to yellow, to 7 mm.</text>
      <biological_entity id="o9918" name="glochid" name_original="glochids" src="d0_s9" type="structure">
        <character constraint="in areole" constraintid="o9919" is_modifier="false" name="position" src="d0_s9" value="adaxial" value_original="adaxial" />
        <character char_type="range_value" from="yellowish white" name="coloration" notes="" src="d0_s9" to="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9919" name="areole" name_original="areole" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: inner tepals bright-yellow, ± 20 mm;</text>
      <biological_entity id="o9920" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="inner" id="o9921" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="bright-yellow" value_original="bright-yellow" />
        <character modifier="more or less" name="some_measurement" src="d0_s10" unit="mm" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments yellow;</text>
      <biological_entity id="o9922" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9923" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style cream;</text>
      <biological_entity id="o9924" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9925" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="cream" value_original="cream" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma lobes cream or tinged pink.</text>
      <biological_entity id="o9926" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="stigma" id="o9927" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="tinged pink" value_original="tinged pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits yellow, cylindric to ellipsoid, 30–50 × 10–30 mm, fleshy, spineless (sometimes with 3–6 white or red spines to 6 mm), yellow glochidiate;</text>
      <biological_entity id="o9928" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s14" to="ellipsoid" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s14" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s14" to="30" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s14" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="spineless" value_original="spineless" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glochidiate" value_original="glochidiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>areoles 25–35.</text>
      <biological_entity id="o9929" name="areole" name_original="areoles" src="d0_s15" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s15" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds yellowish white to yellow, 4.5–5 × 3.5 mm, smooth.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 44.</text>
      <biological_entity id="o9930" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="yellowish white" name="coloration" src="d0_s16" to="yellow" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s16" to="5" to_unit="mm" />
        <character name="width" src="d0_s16" unit="mm" value="3.5" value_original="3.5" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9931" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer (Jun–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chihuahuan Desert, sandy or gravelly flats, low hills</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly flats" />
        <character name="habitat" value="low hills" />
        <character name="habitat" value="desert" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Clav ellina</other_name>
  <discussion>Plants restricted to clay soils in Big Bend National Park, Brewster County, Texas, with more spines per areole than Grusonia schottii, have been named G. densispina (Ralston &amp; Hilsenbeck) Pinkava. Much additional study is required to determine the relationship between such plants and G. schottii.</discussion>
  
</bio:treatment>