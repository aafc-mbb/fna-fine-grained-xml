<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">391</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Forsskål ex J. F. Gmelin" date="unknown" rank="genus">suaeda</taxon_name>
    <taxon_name authority="(C. A. Meyer) Volkens in H. G. A. Engler and K. Prantl" date="1893" rank="section">Schanginia</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>79[III,1a]: 80. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus suaeda;section Schanginia</taxon_hierarchy>
    <other_info_on_name type="fna_id">304249</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="C. A. Meyer" date="unknown" rank="section">Schanginia</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. von Ledebour, Fl. Altaica</publication_title>
      <place_in_publication>1: 370, 394. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>section Schanginia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, glabrous.</text>
      <biological_entity id="o20377" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves sessile, ± flat, cross-sections of fresh leaves ± uniformly green when seen at 10× magnification.</text>
      <biological_entity id="o20378" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o20379" name="cross-section" name_original="cross-sections" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when seen at 10×magnification" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o20380" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="condition" src="d0_s1" value="fresh" value_original="fresh" />
      </biological_entity>
      <relation from="o20379" id="r2966" name="part_of" negation="false" src="d0_s1" to="o20380" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: glomes on short branchlets, partially fused to and appearing to arise from bracts;</text>
      <biological_entity id="o20381" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o20382" name="glome" name_original="glomes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="partially" name="fusion" notes="" src="d0_s2" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o20383" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o20384" name="bract" name_original="bracts" src="d0_s2" type="structure" />
      <relation from="o20382" id="r2967" name="on" negation="false" src="d0_s2" to="o20383" />
      <relation from="o20382" id="r2968" name="appearing to" negation="false" src="d0_s2" to="o20384" />
    </statement>
    <statement id="d0_s3">
      <text>bracteoles subtending glomes without marginal hairs.</text>
      <biological_entity id="o20385" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o20386" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure" />
      <biological_entity id="o20387" name="glome" name_original="glomes" src="d0_s3" type="structure" />
      <biological_entity constraint="marginal" id="o20388" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <relation from="o20386" id="r2969" name="subtending" negation="false" src="d0_s3" to="o20387" />
      <relation from="o20386" id="r2970" name="without" negation="false" src="d0_s3" to="o20388" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers bisexual or pistillate;</text>
      <biological_entity id="o20389" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>perianth actinomorphic, to ± irregular, globose to longer than wide;</text>
      <biological_entity id="o20390" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character char_type="range_value" from="actinomorphic" name="architecture" src="d0_s5" to="more or less irregular" />
        <character char_type="range_value" from="actinomorphic" name="architecture" src="d0_s5" to="more or less irregular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="globose" value_original="globose" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="longer than wide" value_original="longer than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth segments connate proximally to ± completely, succulent;</text>
      <biological_entity constraint="perianth" id="o20391" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="proximally to more or less; completely" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="more or less completely" name="texture" src="d0_s6" value="succulent" value_original="succulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovary subglobose;</text>
      <biological_entity id="o20392" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subglobose" value_original="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigmas 2–3, arising from apex of ovary, filiform, ± smooth.</text>
      <biological_entity id="o20393" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="3" />
        <character constraint="from apex" constraintid="o20394" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o20394" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o20395" name="ovary" name_original="ovary" src="d0_s8" type="structure" />
      <relation from="o20394" id="r2971" name="part_of" negation="false" src="d0_s8" to="o20395" />
    </statement>
    <statement id="d0_s9">
      <text>Seeds vertical, dimorphic, subglobose with black, hard, papillate seed-coat, or strongly flattened and coiled with brown, membranaceous, dull seed-coat.</text>
      <biological_entity id="o20396" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="growth_form" src="d0_s9" value="dimorphic" value_original="dimorphic" />
        <character constraint="with seed-coat" constraintid="o20397" is_modifier="false" name="shape" src="d0_s9" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="strongly" name="shape" notes="" src="d0_s9" value="flattened" value_original="flattened" />
        <character constraint="with seed-coat" constraintid="o20398" is_modifier="false" name="architecture" src="d0_s9" value="coiled" value_original="coiled" />
      </biological_entity>
      <biological_entity id="o20397" name="seed-coat" name_original="seed-coat" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character is_modifier="true" name="texture" src="d0_s9" value="hard" value_original="hard" />
        <character is_modifier="true" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o20398" name="seed-coat" name_original="seed-coat" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="true" name="texture" src="d0_s9" value="membranaceous" value_original="membranaceous" />
        <character is_modifier="true" name="reflectance" src="d0_s9" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia; also introduced in Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="also  in Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>25a.</number>
  <discussion>Species ca. 3 (1 in the flora).</discussion>
  
</bio:treatment>