<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">426</other_info_on_meta>
    <other_info_on_meta type="treatment_page">425</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">amaranthus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Amaranthus</taxon_name>
    <taxon_name authority="Martius ex Thellung" date="1912" rank="species">dubius</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Adv. Montpellier</publication_title>
      <place_in_publication>38: 203. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus amaranthus;subgenus amaranthus;species dubius;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242415663</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glabrous or sparsely pubescent in distal parts.</text>
      <biological_entity id="o19037" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="in distal parts" constraintid="o19038" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19038" name="part" name_original="parts" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, green, branched, 0.3–1 m.</text>
      <biological_entity id="o19039" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s1" to="1" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole of proximal leaves equaling or longer than blade, becoming shorter distally;</text>
      <biological_entity id="o19040" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19041" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="variability" src="d0_s2" value="equaling" value_original="equaling" />
        <character constraint="than blade" constraintid="o19043" is_modifier="false" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="becoming; distally" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o19042" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19043" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <relation from="o19041" id="r2764" name="part_of" negation="false" src="d0_s2" to="o19042" />
    </statement>
    <statement id="d0_s3">
      <text>blade rhombic-ovate or ovate to elliptic, 3–12 × 2–8 cm, base broadly cuneate, margins entire, apex slightly acuminate to obtuse and faintly emarginate, mucronate.</text>
      <biological_entity id="o19044" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19045" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19046" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19047" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19048" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="slightly acuminate" name="shape" src="d0_s3" to="obtuse" />
        <character is_modifier="false" modifier="faintly" name="architecture_or_shape" src="d0_s3" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal panicles and axillary spikes;</text>
      <biological_entity id="o19049" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="terminal" id="o19050" name="panicle" name_original="panicles" src="d0_s4" type="structure" />
      <biological_entity constraint="terminal axillary" id="o19051" name="spike" name_original="spikes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>panicles erect or often drooping, green, dense, branched, leafless at least distally.</text>
      <biological_entity id="o19052" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s5" value="drooping" value_original="drooping" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="at-least distally; distally" name="architecture" src="d0_s5" value="leafless" value_original="leafless" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Bracts lanceolate, shorter than 2 mm, shorter than tepals, apex spinescent.</text>
      <biological_entity id="o19053" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character modifier="shorter than" name="some_measurement" src="d0_s6" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o19054" name="tepal" name_original="tepals" src="d0_s6" type="structure" />
      <biological_entity id="o19055" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="spinescent" value_original="spinescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate flowers: tepals 5, oblong-spatulate to oblong, not clawed, 1.5–2 mm, apex acute, often very shortly mucronate;</text>
      <biological_entity id="o19056" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19057" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character char_type="range_value" from="oblong-spatulate" name="shape" src="d0_s7" to="oblong" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19058" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="often very shortly" name="shape" src="d0_s7" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style-branches strongly spreading, shorter than body of fruit;</text>
      <biological_entity id="o19059" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19060" name="style-branch" name_original="style-branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character constraint="than body" constraintid="o19061" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o19061" name="body" name_original="body" src="d0_s8" type="structure" />
      <biological_entity id="o19062" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <relation from="o19061" id="r2765" name="part_of" negation="false" src="d0_s8" to="o19062" />
    </statement>
    <statement id="d0_s9">
      <text>stigmas 3.</text>
      <biological_entity id="o19063" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19064" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers usually clustered at tips of inflorescence branches, sometimes gathered in proximal glomerules (as in A. spinosus);</text>
      <biological_entity id="o19065" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character constraint="at tips" constraintid="o19066" is_modifier="false" modifier="usually" name="arrangement_or_growth_form" src="d0_s10" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o19066" name="tip" name_original="tips" src="d0_s10" type="structure" />
      <biological_entity constraint="inflorescence" id="o19067" name="branch" name_original="branches" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o19068" name="glomerule" name_original="glomerules" src="d0_s10" type="structure" />
      <relation from="o19066" id="r2766" name="part_of" negation="false" src="d0_s10" to="o19067" />
      <relation from="o19065" id="r2767" modifier="sometimes" name="gathered in" negation="false" src="d0_s10" to="o19068" />
    </statement>
    <statement id="d0_s11">
      <text>tepals 5, equal or subequal;</text>
      <biological_entity id="o19069" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 5.</text>
      <biological_entity id="o19070" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Utricles ovoid or subglobose, 1.5–2 mm, slightly shorter than tepals, smooth to irregularly wrinkled, dehiscence regularly circumscissile.</text>
      <biological_entity id="o19071" name="utricle" name_original="utricles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character constraint="than tepals" constraintid="o19072" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="slightly shorter" value_original="slightly shorter" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s13" to="irregularly wrinkled" />
        <character is_modifier="false" modifier="regularly" name="dehiscence" src="d0_s13" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o19072" name="tepal" name_original="tepals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds dark reddish-brown to black, subglobose or lenticular, 0.8–1 mm diam., shiny, smooth.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 64.</text>
      <biological_entity id="o19073" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="dark reddish-brown" name="coloration" src="d0_s14" to="black" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="diameter" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19074" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall in tropics, various seasons in subtropics.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="in tropics, various seasons in subtropics" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, disturbed habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="disturbed habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; West Indies; South America; introduced and locally naturalized Europe, Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="and locally naturalized Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Spleen amaranth</other_name>
  <discussion>Amaranthus dubius, a morphologically deviant allopolyploid, is very close genetically to both A. spinosus (sect. Centrusa) and members of sect. Amaranthus. This species most probably originated as a result of ancient hybridization between A. spinosus and either A. hybridus or A. quitensis (W. F. Grant 1959; T. N. Khoshoo and M. Pal 1972; M. Pal and T. N. Khoshoo 1965; J. D. Sauer 1967b; V. Srivastava et al. 1977). Amaranthus nothosect. Dubia Mosyakin &amp; K. R. Robertson (A. sect. Amaranthus × A. sect. Centrusa), was proposed to accommodate A. dubius (S. L. Mosyakin and K. R. Robertson 1996).</discussion>
  
</bio:treatment>