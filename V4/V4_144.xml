<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="treatment_page">69</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">abronia</taxon_name>
    <taxon_name authority="Brandegee" date="1899" rank="species">alpina</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>27: 456. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus abronia;species alpina</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415105</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o3758" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, well branched, forming small mats, elongate, viscid-pubescent.</text>
      <biological_entity id="o3759" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="well" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="viscid-pubescent" value_original="viscid-pubescent" />
      </biological_entity>
      <biological_entity id="o3760" name="mat" name_original="mats" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
      <relation from="o3759" id="r496" name="forming" negation="false" src="d0_s1" to="o3760" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1–2 cm;</text>
      <biological_entity id="o3761" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3762" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade orbiculate-oval, 0.4–1 × 0.3–0.5 cm, margins entire, plane, surfaces glandular-pubescent.</text>
      <biological_entity id="o3763" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3764" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="orbiculate-oval" value_original="orbiculate-oval" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s3" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3765" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o3766" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: peduncle shorter than subtending petiole;</text>
      <biological_entity id="o3767" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o3768" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character constraint="than subtending petiole" constraintid="o3769" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o3769" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracts lanceolate to ovate, 2–3 × 1–2 mm, papery, glandular-pubescent;</text>
      <biological_entity id="o3770" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o3771" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="papery" value_original="papery" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flowers 1–5.</text>
      <biological_entity id="o3772" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o3773" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perianth: tube whitish, 10–18 mm, limb white to lavender-pink, 6–8 mm diam.</text>
      <biological_entity id="o3774" name="perianth" name_original="perianth" src="d0_s7" type="structure" />
      <biological_entity id="o3775" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3776" name="limb" name_original="limb" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="lavender-pink" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits narrowly obovate in profile, 3–4 × 2–3 mm, thin, coriaceous, apex broadly conic;</text>
      <biological_entity id="o3777" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly; in profile" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s8" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o3778" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>wings absent or 5-angled.</text>
      <biological_entity id="o3779" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s9" value="5-angled" value_original="5-angled" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, alpine meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="alpine meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2600-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>