<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Allan D. Zimmerman,Bruce D. Parfitt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Lemaire" date="1839" rank="genus">ASTROPHYTUM</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Gen. Sp. Nov.,</publication_title>
      <place_in_publication>3. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus astrophytum;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek asteros, star, in reference to the star-shaped stem cross section of the type species, and phyton, plant</other_info_on_name>
    <other_info_on_name type="fna_id">103011</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants stem succulents, unbranched [to several branched from base], mostly low and deep-seated in substrate [taller in Mexican species].</text>
      <biological_entity constraint="stem" id="o11312" name="succulent" name_original="succulents" src="d0_s0" type="structure" constraint_original="plants stem">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s0" value="low" value_original="low" />
        <character constraint="in substrate" constraintid="o11313" is_modifier="false" name="location" src="d0_s0" value="deep-seated" value_original="deep-seated" />
      </biological_entity>
      <biological_entity id="o11313" name="substrate" name_original="substrate" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots diffuse.</text>
      <biological_entity id="o11314" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem unsegmented, dark green or gray-green, hemispheric or depressed-spheric [to spheric or short cylindric], (2.5–) 6–15 (–60) [–100] × 6–10 (–20) cm, speckled [or entirely hidden] by numerous tufts of dense, whitish, multicellular hairs less than 0.5 mm;</text>
      <biological_entity id="o11315" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="depressed-spheric" value_original="depressed-spheric" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_length" src="d0_s2" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="60" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s2" to="10" to_unit="cm" />
        <character constraint="by tufts" constraintid="o11316" is_modifier="false" name="coloration" src="d0_s2" value="speckled" value_original="speckled" />
      </biological_entity>
      <biological_entity id="o11316" name="tuft" name_original="tufts" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11317" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <relation from="o11316" id="r1649" name="part_of" negation="false" src="d0_s2" to="o11317" />
    </statement>
    <statement id="d0_s3">
      <text>ribs [4–] 8–10, crests uninterrupted, straight [to sinuous and/or helically curving around stem], broad and nearly flat or rounded [sharp or keeled];</text>
      <biological_entity id="o11318" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s3" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <biological_entity id="o11319" name="crest" name_original="crests" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="uninterrupted" value_original="uninterrupted" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="width" src="d0_s3" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>areoles distinct, [2–] 6–9 [–20] mm apart along ribs, circular;</text>
      <biological_entity id="o11320" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
        <character constraint="along ribs" constraintid="o11321" is_modifier="false" name="arrangement" src="d0_s4" value="apart" value_original="apart" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s4" value="circular" value_original="circular" />
      </biological_entity>
      <biological_entity id="o11321" name="rib" name_original="ribs" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>areolar glands absent;</text>
      <biological_entity constraint="areolar" id="o11322" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cortex and pith hard, not mucilaginous.</text>
      <biological_entity id="o11323" name="cortex" name_original="cortex" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o11324" name="pith" name_original="pith" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spines absent [1–25 per areole in some Mexican species].</text>
      <biological_entity id="o11325" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers diurnal, near stem apex, at adaxial edge of areoles, funnelform, 4.5–5.4 [–8] × 3.8–5.2 [–6] cm;</text>
      <biological_entity id="o11326" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="diurnal" value_original="diurnal" />
        <character char_type="range_value" from="5.4" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s8" to="8" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" notes="" src="d0_s8" to="5.4" to_unit="cm" />
        <character char_type="range_value" from="5.2" from_inclusive="false" from_unit="cm" name="atypical_width" notes="" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="3.8" from_unit="cm" name="width" notes="" src="d0_s8" to="5.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o11327" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity constraint="adaxial" id="o11328" name="edge" name_original="edge" src="d0_s8" type="structure" />
      <biological_entity id="o11329" name="areole" name_original="areoles" src="d0_s8" type="structure" />
      <relation from="o11326" id="r1650" name="near" negation="false" src="d0_s8" to="o11327" />
      <relation from="o11326" id="r1651" name="at" negation="false" src="d0_s8" to="o11328" />
      <relation from="o11328" id="r1652" name="part_of" negation="false" src="d0_s8" to="o11329" />
    </statement>
    <statement id="d0_s9">
      <text>inner tepals yellow, proximally red [all yellow], 25 × 6–10 [–12] mm, margins entire;</text>
      <biological_entity constraint="inner" id="o11330" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character name="length" src="d0_s9" unit="mm" value="25" value_original="25" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11331" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary sparsely to densely scaly, axils spineless long hairy with arachnoid trichomes, distal scales spine-tipped;</text>
      <biological_entity id="o11332" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="architecture_or_pubescence" src="d0_s10" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o11333" name="axil" name_original="axils" src="d0_s10" type="structure">
        <character is_modifier="false" name="length" src="d0_s10" value="spineless" value_original="spineless" />
        <character constraint="with trichomes" constraintid="o11334" is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o11334" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s10" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11335" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigma lobes [8–] 10–12, yellow [pale yellowish], 4 mm.</text>
      <biological_entity constraint="stigma" id="o11336" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s11" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="12" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits indehiscent or splitting irregularly, green or pinkish to red, ovoid to spheric, 15–20 × 12 mm, initially fleshy, drying immediately after ripening, sparsely to densely scaly with spine-tipped scales;</text>
      <biological_entity id="o11337" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s12" value="splitting" value_original="splitting" />
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s12" to="red" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s12" to="spheric" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s12" to="20" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="12" value_original="12" />
        <character is_modifier="false" modifier="initially" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
        <character constraint="after ripening" is_modifier="false" name="condition" src="d0_s12" value="drying" value_original="drying" />
        <character constraint="with scales" constraintid="o11338" is_modifier="false" modifier="sparsely to densely" name="architecture_or_pubescence" src="d0_s12" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o11338" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>axils of scales long woolly, spineless;</text>
      <biological_entity id="o11339" name="axil" name_original="axils" src="d0_s13" type="structure" constraint="scale" constraint_original="scale; scale">
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="spineless" value_original="spineless" />
      </biological_entity>
      <biological_entity id="o11340" name="scale" name_original="scales" src="d0_s13" type="structure" />
      <relation from="o11339" id="r1653" name="part_of" negation="false" src="d0_s13" to="o11340" />
    </statement>
    <statement id="d0_s14">
      <text>floral remnant persistent.</text>
      <biological_entity constraint="floral" id="o11341" name="remnant" name_original="remnant" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds dark-brown to blackish, appearing hollow or bowl-shaped from strongly expanded, inrolled rim around sunken hilum, 2–3 mm in greatest dimension, nearly smooth;</text>
      <biological_entity id="o11342" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s15" to="blackish" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="hollow" value_original="hollow" />
        <character constraint="from rim" constraintid="o11343" is_modifier="false" name="shape" src="d0_s15" value="bowl--shaped" value_original="bowl--shaped" />
        <character char_type="range_value" from="2" from_unit="mm" name="dimension" notes="" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o11343" name="rim" name_original="rim" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="strongly" name="size" src="d0_s15" value="expanded" value_original="expanded" />
        <character is_modifier="true" name="shape_or_vernation" src="d0_s15" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <biological_entity id="o11344" name="hilum" name_original="hilum" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="around" name="prominence" src="d0_s15" value="sunken" value_original="sunken" />
      </biological_entity>
      <relation from="o11343" id="r1654" name="around sunken" negation="false" src="d0_s15" to="o11344" />
    </statement>
    <statement id="d0_s16">
      <text>testa cells very slightly convex.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 11.</text>
      <biological_entity constraint="testa" id="o11345" name="cell" name_original="cells" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="very slightly" name="shape" src="d0_s16" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="x" id="o11346" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion>Species 4–5 (1 in the flora).</discussion>
  <discussion>Astrophytum is most often recognized as a distinct genus. Chloroplast DNA evidence (C. A. Butterworth et al. 2002), however, confirms that it is among the closest relatives of Echinocactus. Unlike Echinocactus, the stem surfaces of Astrophytum species are speckled by numerous tiny white tufts of minute, matted hairs in addition to the regularly spaced woolly areoles.</discussion>
  
</bio:treatment>