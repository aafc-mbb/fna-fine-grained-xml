<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="treatment_page">253</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Haworth" date="unknown" rank="genus">mammillaria</taxon_name>
    <taxon_name authority="K. Brandegee" date="unknown" rank="species">dioica</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>5: 115. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus mammillaria;species dioica;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415360</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants unbranched or branched;</text>
      <biological_entity id="o16732" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches 0–50.</text>
      <biological_entity id="o16733" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Roots diffuse, upper portion not enlarged.</text>
      <biological_entity id="o16734" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="density" src="d0_s2" value="diffuse" value_original="diffuse" />
      </biological_entity>
      <biological_entity constraint="upper" id="o16735" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s2" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems nearly spheric to more often cylindric or long cylindric, 5–30 × 5–7 cm, firm;</text>
      <biological_entity id="o16736" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s3" value="spheric" value_original="spheric" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s3" to="7" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>tubercles 5–12 × 3–7 mm;</text>
      <biological_entity id="o16737" name="tubercle" name_original="tubercles" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>axils woolly, bearing 4–15 bristles (0 in young growth) as long as tubercles;</text>
      <biological_entity id="o16738" name="axil" name_original="axils" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o16739" name="bristle" name_original="bristles" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s5" to="15" />
      </biological_entity>
      <biological_entity id="o16740" name="tubercle" name_original="tubercles" src="d0_s5" type="structure" />
      <relation from="o16738" id="r2418" name="bearing" negation="false" src="d0_s5" to="o16739" />
      <relation from="o16738" id="r2419" name="as long as" negation="false" src="d0_s5" to="o16740" />
    </statement>
    <statement id="d0_s6">
      <text>cortex and pith not mucilaginous;</text>
      <biological_entity id="o16741" name="cortex" name_original="cortex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o16742" name="pith" name_original="pith" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>latex absent.</text>
      <biological_entity id="o16743" name="latex" name_original="latex" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spines 14–26 per areole, pinkish or reddish-brown to black, glabrous;</text>
      <biological_entity id="o16744" name="spine" name_original="spines" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o16745" from="14" name="quantity" src="d0_s8" to="26" />
        <character char_type="range_value" from="reddish-brown" name="coloration" notes="" src="d0_s8" to="black" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16745" name="areole" name_original="areole" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>radial spines 11–22 per areole, usually white, bristlelike, 5–7 mm, stiff;</text>
      <biological_entity id="o16746" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o16747" from="11" name="quantity" src="d0_s9" to="22" />
        <character is_modifier="false" modifier="usually" name="coloration" notes="" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="bristlelike" value_original="bristlelike" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s9" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o16747" name="areole" name_original="areole" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>central spines (1–) 3–4 per areole, abaxial 1 porrect, hooked, longer, stouter, adaxial central spines ascending with radial spines;</text>
      <biological_entity constraint="central" id="o16748" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s10" to="3" to_inclusive="false" />
        <character char_type="range_value" constraint="per areole" constraintid="o16749" from="3" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity id="o16749" name="areole" name_original="areole" src="d0_s10" type="structure" />
      <biological_entity constraint="abaxial central" id="o16750" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="porrect" value_original="porrect" />
        <character is_modifier="false" name="shape" src="d0_s10" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="stouter" value_original="stouter" />
      </biological_entity>
      <biological_entity constraint="adaxial central" id="o16751" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character constraint="with spines" constraintid="o16752" is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o16752" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="radial" value_original="radial" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>subcentral spines 0.</text>
      <biological_entity constraint="subcentral" id="o16753" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 10–22 mm;</text>
      <biological_entity id="o16754" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>outermost tepals entire or short fringed;</text>
      <biological_entity constraint="outermost" id="o16755" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s13" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s13" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>inner tepals cream, usually with pinkish or reddish midstripes, longer in bisexual flowers, 5.4 mm diam.;</text>
      <biological_entity constraint="inner" id="o16756" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="cream" value_original="cream" />
        <character constraint="in flowers" constraintid="o16758" is_modifier="false" name="length_or_size" notes="" src="d0_s14" value="longer" value_original="longer" />
        <character name="diameter" notes="" src="d0_s14" unit="mm" value="5.4" value_original="5.4" />
      </biological_entity>
      <biological_entity id="o16757" name="midstripe" name_original="midstripes" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s14" value="pinkish" value_original="pinkish" />
        <character is_modifier="true" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o16758" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o16756" id="r2420" modifier="usually" name="with" negation="false" src="d0_s14" to="o16757" />
    </statement>
    <statement id="d0_s15">
      <text>stigma lobes yellow to greenish yellow or brownish green, 8 mm.</text>
      <biological_entity constraint="stigma" id="o16759" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s15" to="greenish yellow or brownish green" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits bright scarlet, clavate or ovoid, 10–25 (–35) × 10 mm, juicy only in fruit walls;</text>
      <biological_entity id="o16760" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="bright scarlet" value_original="bright scarlet" />
        <character is_modifier="false" name="shape" src="d0_s16" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s16" to="25" to_unit="mm" />
        <character name="width" src="d0_s16" unit="mm" value="10" value_original="10" />
        <character constraint="in fruit walls" constraintid="o16761" is_modifier="false" name="texture" src="d0_s16" value="juicy" value_original="juicy" />
      </biological_entity>
      <biological_entity constraint="fruit" id="o16761" name="wall" name_original="walls" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>floral remnant persistent.</text>
      <biological_entity constraint="floral" id="o16762" name="remnant" name_original="remnant" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds black, 0.8 × 0.6 mm, pitted;</text>
      <biological_entity id="o16763" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="black" value_original="black" />
        <character name="length" src="d0_s18" unit="mm" value="0.8" value_original="0.8" />
        <character name="width" src="d0_s18" unit="mm" value="0.6" value_original="0.6" />
        <character is_modifier="false" name="relief" src="d0_s18" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>testa hard;</text>
      <biological_entity id="o16764" name="testa" name_original="testa" src="d0_s19" type="structure">
        <character is_modifier="false" name="texture" src="d0_s19" value="hard" value_original="hard" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anticlinal cell-walls straight (not undulate);</text>
      <biological_entity id="o16765" name="cell-wall" name_original="cell-walls" src="d0_s20" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s20" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="course" src="d0_s20" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>interstices conspicuously wider than pit diameters;</text>
      <biological_entity id="o16766" name="interstice" name_original="interstices" src="d0_s21" type="structure">
        <character constraint="than pit" constraintid="o16767" is_modifier="false" name="width" src="d0_s21" value="conspicuously wider" value_original="conspicuously wider" />
      </biological_entity>
      <biological_entity id="o16767" name="pit" name_original="pit" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>pits bowl-shaped.</text>
    </statement>
    <statement id="d0_s23">
      <text>2n = 66.</text>
      <biological_entity id="o16768" name="pit" name_original="pits" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="bowl--shaped" value_original="bowl--shaped" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16769" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="66" value_original="66" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–May); fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-May" />
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>California coastal scrub, Colorado subdivision of Sonoran desert scrub, rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="california coastal scrub" />
        <character name="habitat" value="colorado subdivision" constraint="of sonoran" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="past_name">Mamillaria</other_name>
  <other_name type="common_name">California fishhook cactus</other_name>
  <other_name type="common_name">strawberry cactus</other_name>
  <discussion>In an inland population in California, Mammillaria dioica was found to be functionally gynodioecious (F. R. Ganders and H. Kennedy 1978), with flowers of some plants bisexual while those of other individuals bear only functionally female flowers with sterile anthers. Coastal populations of the species were not studied and might be “trioecious” with staminate, pistillate, and bisexual flowers on different plants (B. D. Parfitt 1985).</discussion>
  <discussion>Plants of Mammillaria dioica in Mexico are both tetraploid and hexaploid (M. A. T. Johnson 1978).</discussion>
  
</bio:treatment>