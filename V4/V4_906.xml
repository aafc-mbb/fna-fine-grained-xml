<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John G. Packer</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="mention_page">505</other_info_on_meta>
    <other_info_on_meta type="treatment_page">457</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">PORTULACACEAE</taxon_name>
    <taxon_hierarchy>family PORTULACACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10724</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs [shrubs] or herbs, annual, biennial, or perennial, often succulent or fleshy.</text>
      <biological_entity id="o21126" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="often" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="often" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves opposite, subopposite, or alternate and sometimes secund, sometimes rosulate or subrosulate, exstipulate (except Portulaca and Talinopsis, with nodal or axillary hairs regarded as stipular);</text>
      <biological_entity id="o21128" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s1" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="subrosulate" value_original="subrosulate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="exstipulate" value_original="exstipulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade margins mostly entire, occasionally dentate to crisped.</text>
      <biological_entity constraint="blade" id="o21129" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character char_type="range_value" from="occasionally dentate" name="shape" src="d0_s2" to="crisped" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences axillary or terminal, cymose, racemose, paniculate, or umbellate, sometimes glomerate, spikelike, or with flowers solitary, open to congested.</text>
      <biological_entity id="o21130" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="glomerate" value_original="glomerate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spikelike" value_original="spikelike" />
        <character char_type="range_value" from="open" name="architecture" notes="" src="d0_s3" to="congested" />
      </biological_entity>
      <biological_entity id="o21131" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o21130" id="r3055" name="with" negation="false" src="d0_s3" to="o21131" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers mostly radially symmetric, sometimes slightly irregular (in Montia);</text>
      <biological_entity id="o21132" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly radially" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="sometimes slightly" name="architecture_or_course" src="d0_s4" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 2–9;</text>
      <biological_entity id="o21133" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals (1–) 2–19 or sometimes absent, distinct or connate basally;</text>
      <biological_entity id="o21134" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="19" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 1–many, opposite and sometimes basally adnate to petals;</text>
      <biological_entity id="o21135" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s7" to="many" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="opposite" value_original="opposite" />
        <character constraint="to petals" constraintid="o21136" is_modifier="false" modifier="sometimes basally" name="fusion" src="d0_s7" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o21136" name="petal" name_original="petals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>gynoecium 2–9-carpelled;</text>
      <biological_entity id="o21137" name="gynoecium" name_original="gynoecium" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-9-carpelled" value_original="2-9-carpelled" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 1, superior (half-inferior to inferior in Portulaca), 1-locular throughout or initially plurilocular and becoming 1-locular distally (in Portulaca), placentation basal or free-central, ovules 1-many;</text>
      <biological_entity id="o21138" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s9" value="superior" value_original="superior" />
        <character is_modifier="false" modifier="throughout" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" modifier="initially; initially" name="architecture" src="d0_s9" value="plurilocular" value_original="plurilocular" />
        <character is_modifier="false" modifier="becoming; distally" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="1-locular" value_original="1-locular" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21139" name="free-central" name_original="free-central" src="d0_s9" type="structure" />
      <biological_entity id="o21140" name="ovule" name_original="ovules" src="d0_s9" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s9" value="1-many" value_original="1-many" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style present, sometimes branched, or absent;</text>
      <biological_entity id="o21141" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s10" value="branched" value_original="branched" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas 1–9.</text>
      <biological_entity id="o21142" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits capsular.</text>
      <biological_entity id="o21143" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="capsular" value_original="capsular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds smooth or sculptured, with or without strophioles or elaiosomes.</text>
      <biological_entity id="o21145" name="strophiole" name_original="strophioles" src="d0_s13" type="structure" />
      <biological_entity id="o21146" name="elaiosome" name_original="elaiosomes" src="d0_s13" type="structure" />
      <relation from="o21144" id="r3056" name="with or without" negation="false" src="d0_s13" to="o21145" />
      <relation from="o21144" id="r3057" name="with or without" negation="false" src="d0_s13" to="o21146" />
    </statement>
    <statement id="d0_s14">
      <text>x = 4–9, 11, 13, 15, 23.</text>
      <biological_entity id="o21144" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s13" value="sculptured" value_original="sculptured" />
      </biological_entity>
      <biological_entity constraint="x" id="o21147" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s14" to="9" />
        <character name="quantity" src="d0_s14" value="11" value_original="11" />
        <character name="quantity" src="d0_s14" value="13" value_original="13" />
        <character name="quantity" src="d0_s14" value="15" value_original="15" />
        <character name="quantity" src="d0_s14" value="23" value_original="23" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Primarily Southern Hemisphere, poorly represented in Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Primarily Southern Hemisphere" establishment_means="native" />
        <character name="distribution" value="poorly represented in Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40.</number>
  <other_name type="common_name">Purslane Family</other_name>
  <discussion>Genera 20–30, species ca. 500 (9 genera, 91 species in the flora).</discussion>
  <discussion>The eastern New World species of Portulacaceae seem to have a closer relationship with the African species, and the western New World species a closer one with the Australian species, than the two New World groups have with each other to each other.</discussion>
  <discussion>The outer perianth segments, referred to herein as sepals, are held by some (e.g., T. Eckardt 1976) to be modified bracteoles, the petals then representing the true sepals. However, the traditional interpretation, adopted here and in most North American floras, still finds current support (R. C. Carolin 1987). A comparable situation prevails with respect to the cauline leaves in Claytonia and other genera, which are widely interpreted to be foliaceous bracts (R. C. Carolin 1987); here again, as is appropriate in a descriptive context, the traditional terminology is employed. In Talinopsis and Portulaca, the stipular nature of the nodal or axillary hairs also has been a matter of discussion. The question was revisited by R. Geesink (1969), who denied their stipular origin.</discussion>
  <discussion>The relationships of the family are not a matter of dispute (A. Cronquist 1981; R. C. Carolin 1987); the same cannot be said for the relationships and delimitations of the genera, which have always been labile. They are, at present, the subject of active research, which has led to the current acceptance of Phemeranthus and Cistanthe. Changes in the generic classification are discussed in the treatments of the genera concerned.</discussion>
  <discussion>Because of the uncertain relationships, the genera and species are listed alphabetically.</discussion>
  <references>
    <reference>Applequist, W. L. and R. S. Wallace. 2001. Phylogeny of the portulacaceous cohort based on ndhF sequence data. Syst. Bot. 26: 406–419.</reference>
    <reference>Bogle, A. L. 1969. The genera of Portulacaceae and Basellaceae in the southeastern United States. J. Arnold Arbor. 50: 566–598.</reference>
    <reference>Carolin, R. C. 1987. A review of the family Portulacaceae. Austral. J. Bot. 35: 383–412.</reference>
    <reference>Carolin, R. C. 1993. Portulacaceae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 4+ vols. Berlin etc. Vol. 2, pp. 544–555.</reference>
    <reference>Hershkovitz, M. A. and E. A. Zimmer. 2000. Ribosomal DNA evidence and disjunctions of western American Portulacaceae. Molec. Phylogen. Evol. 15: 419–439.</reference>
    <reference>McNeill, J. 1974. Synopsis of a revised classification of the Portulacaceae. Taxon 23: 725–728.</reference>
    <reference>McNeill, J. 1975. A generic revision of Portulacaceae tribe Montieae using techniques of numerical taxonomy. Canad. J. Bot. 53: 789–809.</reference>
    <reference>Nilsson, Ö. 1967. Studies in Montia L. and Claytonia L. and allied genera III. Pollen morphology. Grana Palynol., n. s. 7: 279–353.</reference>
    <reference>Nyananyo, B. L. 1986. Taxonomic significance of the stomatal complex in the Portulacaceae. Feddes Repert. 97: 763–766.</reference>
    <reference>Nyananyo, B. L. 1990. Tribal and generic relationships in the Portulacaceae. Feddes Repert. 101: 237–241.</reference>
    <reference>Rydberg, P. A. and P. Wilson. 1932. Portulacaceae. In: N. L. Britton et al., eds. 1905+. North American Flora.... 47+ vols. New York. Vol. 21, pp. 279–336.</reference>
    <reference>Swanson, J. R. 1966. A synopsis of relationships in Montioideae (Portulacaceae). Brittonia 18: 229–241.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovary half-inferior to inferior</description>
      <determination>7 Portulaca</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovary superior</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Subshrubs; stem nodes pubescent</description>
      <determination>8 Talinopsis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Herbs; stem nodes glabrous</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsule dehiscence circumscissile, valves longitudinally dehiscent from base</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsule dehiscence not circumscissile, valves longitudinally dehiscent from apex</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals 25-40 mm; seeds strophiolate; nc Washington to extreme sc British Columbia</description>
      <determination>2 Cistanthe</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals 4-26 mm (15-35 mm in Lewisia rediviva); seeds estrophiolate; more widespread</description>
      <determination>4 Lewisia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stigmas 2; capsule valves 2</description>
      <determination>2 Cistanthe</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stigmas 1 or 3; capsule valves 2-3</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sepals mostly deciduous; inflorescences not appearing secund; leaves articulate at base, attachment points round, not clasping; capsule valves wholly or partly deciduous</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sepals persistent; inflorescences somewhat to markedly secund (at least distally); leaves not articulate at base, attachment points linear, somewhat to markedly clasping; capsule valves not deciduous</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades broadly planate, 1-7 cm wide; capsules tardily dehiscent, valves or portions of them sometimes persistent; exocarp and endocarp distinctly differentiated, sometimes separating; seeds minutely tuberculate or striolate, strophiolate, not covered by membrane</description>
      <determination>9 Talinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades terete or semiterete, 1-3 mm wide, (narrowly planate, 1[-2] cm wide in Phemeranthus aurantiacus); capsules promptly dehiscent, valves deciduous; exocarp and endocarp not evidently differentiated and not separating; seeds smooth, rugulose, or distinctly ridged, estrophiolate, covered by thin, fleshy to chartaceous membrane</description>
      <determination>6 Phemeranthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Ovules and seeds (1-)7-40</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Ovules 3 or 6; seeds 1-6</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves and sepals sometimes with elongate, unicellular hairs; sepals distinctly angular or keeled; capsule valves reflexed after dehiscence, margins markedly involute</description>
      <determination>1 Calandrinia</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves and sepals without elongate, unicellular hairs; sepals not distinctly angled or keeled; capsule valves not reflexed after dehiscence, margins not markedly involute</description>
      <determination>2 Cistanthe</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Cauline leaves 2 (rarely 3 in whorl), distinct or partially or completely connate; ovules 3 or 6</description>
      <determination>3 Claytonia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Cauline leaves more than 2, distinct; ovules 3</description>
      <determination>5 Montia</determination>
    </key_statement>
  </key>
</bio:treatment>