<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">34</other_info_on_meta>
    <other_info_on_meta type="treatment_page">36</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">acleisanthes</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">crassifolia</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts, ser.</publication_title>
      <place_in_publication>2, 15: 260. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus acleisanthes;species crassifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">220000116</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants herbaceous, often slightly woody at base, overall pubescence of white, capitate hairs 0.1–0.2 mm.</text>
      <biological_entity id="o99" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s0" value="herbaceous" value_original="herbaceous" />
        <character constraint="at base" constraintid="o100" is_modifier="false" modifier="often slightly" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o100" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o101" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s0" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="pubescence" src="d0_s0" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems procumbent, sparsely branched, 15–50 cm, hirtellous to glabrate.</text>
      <biological_entity id="o102" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s1" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves grayish green (major veins prominently paler), petiolate, those of pair slightly unequal;</text>
      <biological_entity id="o103" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish green" value_original="grayish green" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s2" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o104" name="pair" name_original="pair" src="d0_s2" type="structure" />
      <relation from="o103" id="r13" name="part_of" negation="false" src="d0_s2" to="o104" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–15 (–20) mm, hirtellous;</text>
      <biological_entity id="o105" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to deltate-ovate or oblong-ovate, (5–) 10–35 (–45) × 2–20 (–30) mm, base usually rounded, margins undulate, apex apiculate and acute or less frequently obtuse, sparsely puberulent abaxially and adaxially, adaxial primary and secondary-veins hirtellous.</text>
      <biological_entity id="o106" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="deltate-ovate or oblong-ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o107" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o108" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o109" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="less frequently" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="adaxial primary" id="o110" name="secondary-vein" name_original="secondary-veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers, sessile or nearly so;</text>
      <biological_entity id="o111" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s5" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o112" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts linear-lanceolate, 2–7 mm, long attenuate, usually puberulent.</text>
      <biological_entity id="o113" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: chasmogamous perianth 3–5 cm, puberulent, tube 1–1.5 mm diam., limbs 8–25 mm diam., stamens 5;</text>
      <biological_entity id="o114" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o115" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="chasmogamous" value_original="chasmogamous" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o116" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o117" name="limb" name_original="limbs" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o118" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cleistogamous perianth 2–6 mm, hirtellous, stamens 2.</text>
      <biological_entity id="o119" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o120" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="cleistogamous" value_original="cleistogamous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o121" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits with 5 broad, flat ribs, lacking grooves and resinous glands, 5-angled and shallowly 5-sulcate, oval-oblong, truncate at both ends, 6–9 mm, hirtellous.</text>
      <biological_entity id="o122" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="5-angled" value_original="5-angled" />
        <character is_modifier="false" modifier="shallowly" name="architecture" src="d0_s9" value="5-sulcate" value_original="5-sulcate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oval-oblong" value_original="oval-oblong" />
        <character constraint="at ends" constraintid="o126" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o123" name="rib" name_original="ribs" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="true" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="coating" src="d0_s9" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o124" name="groove" name_original="grooves" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="true" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="coating" src="d0_s9" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o125" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="true" name="width" src="d0_s9" value="broad" value_original="broad" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="coating" src="d0_s9" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o126" name="end" name_original="ends" src="d0_s9" type="structure" />
      <relation from="o122" id="r14" name="with" negation="false" src="d0_s9" to="o123" />
      <relation from="o122" id="r15" name="with" negation="false" src="d0_s9" to="o124" />
      <relation from="o122" id="r16" name="with" negation="false" src="d0_s9" to="o125" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, calcareous soils in cenizo (Leucophyllum frutescens) shrublands and grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" constraint="in cenizo" />
        <character name="habitat" value="calcareous soils" constraint="in cenizo" />
        <character name="habitat" value="cenizo" />
        <character name="habitat" value="leucophyllum frutescens" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Texas trumpets</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>