<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Chenopodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">CHENOPODIUM</taxon_name>
    <taxon_name authority="(Standley) Clemants &amp; Mosyakin" date="1996" rank="subsection">Leptophylla</taxon_name>
    <taxon_name authority="Rydberg" date="1912" rank="species">pratericola</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>39: 310. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus chenopodium;subgenus chenopodium;section chenopodium;subsection leptophylla;species pratericola;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415421</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chenopodium</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">desiccatum</taxon_name>
    <taxon_name authority="(Murr) Wahl" date="unknown" rank="variety">leptophylloides</taxon_name>
    <taxon_hierarchy>genus Chenopodium;species desiccatum;variety leptophylloides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems strictly erect, simple or branching above, 2–8 dm, moderately to densely farinose.</text>
      <biological_entity id="o10929" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="strictly" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s0" value="farinose" value_original="farinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves nonaromatic;</text>
      <biological_entity id="o10930" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="odor" src="d0_s1" value="nonaromatic" value_original="nonaromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.4–1 cm;</text>
      <biological_entity id="o10931" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s2" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear to narrowly lanceolate, or oblongelliptic, (1-veined or) 3-veined, 1.5–4.2 (–6) × 0.4–1 (–1.4) cm, thick and somewhat fleshy, base cuneate, margins entire or with pair of lobes near base, apex acute, abaxial surface densely to sparingly white-mealy.</text>
      <biological_entity id="o10932" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="narrowly lanceolate or oblongelliptic" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="narrowly lanceolate or oblongelliptic" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="4.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="4.2" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o10933" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o10934" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="with pair" value_original="with pair" />
      </biological_entity>
      <biological_entity id="o10935" name="pair" name_original="pair" src="d0_s3" type="structure" />
      <biological_entity id="o10936" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <biological_entity id="o10937" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o10938" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10939" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely to sparingly" name="pubescence_or_texture" src="d0_s3" value="white-mealy" value_original="white-mealy" />
      </biological_entity>
      <relation from="o10934" id="r1595" name="with" negation="false" src="d0_s3" to="o10935" />
      <relation from="o10935" id="r1596" name="part_of" negation="false" src="d0_s3" to="o10936" />
      <relation from="o10935" id="r1597" name="near" negation="false" src="d0_s3" to="o10937" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences glomerules in terminal and axillary panicles, 1–13 × 0.15–0.5 cm;</text>
      <biological_entity constraint="inflorescences" id="o10940" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" notes="" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="0.15" from_unit="cm" name="width" notes="" src="d0_s4" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="terminal and axillary" id="o10941" name="panicle" name_original="panicles" src="d0_s4" type="structure" />
      <relation from="o10940" id="r1598" name="in" negation="false" src="d0_s4" to="o10941" />
    </statement>
    <statement id="d0_s5">
      <text>glomerules usually densely disposed, maturing irregularly;</text>
      <biological_entity id="o10942" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually densely" name="arrangement" src="d0_s5" value="disposed" value_original="disposed" />
        <character is_modifier="false" modifier="irregularly" name="life_cycle" src="d0_s5" value="maturing" value_original="maturing" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts leaflike.</text>
      <biological_entity id="o10943" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth segments (4–) 5, distinct nearly to base;</text>
      <biological_entity id="o10944" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="perianth" id="o10945" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character constraint="to base" constraintid="o10946" is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o10946" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>lobes oblong-ovate, 0.8–1 × 0.5–0.7 mm, apex obtuse, rounded or emarginate, strongly keeled along midvein, densely farinose, usually spreading from fruit;</text>
      <biological_entity id="o10947" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10948" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s8" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10949" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="emarginate" value_original="emarginate" />
        <character constraint="along midvein" constraintid="o10950" is_modifier="false" modifier="strongly" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s8" value="farinose" value_original="farinose" />
        <character constraint="from fruit" constraintid="o10951" is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o10950" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o10951" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens (4–) 5;</text>
      <biological_entity id="o10952" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10953" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 2, 0.2 mm.</text>
      <biological_entity id="o10954" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10955" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Utricles ovoid;</text>
      <biological_entity id="o10956" name="utricle" name_original="utricles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pericarp nonadherent, smooth.</text>
      <biological_entity id="o10957" name="pericarp" name_original="pericarp" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="nonadherent" value_original="nonadherent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds round, 0.9–1.3 mm diam., margins rounded;</text>
      <biological_entity id="o10958" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="round" value_original="round" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="diameter" src="d0_s13" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10959" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat black, rugulate.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o10960" name="seed-coat" name_original="seed-coat" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character is_modifier="false" name="relief" src="d0_s14" value="rugulate" value_original="rugulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10961" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting early summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy soils, pinyon woodlands, sagebrush, often in saline or alkaline habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sandy soils" />
        <character name="habitat" value="pinyon woodlands" />
        <character name="habitat" value="saline" modifier="sagebrush often in" />
        <character name="habitat" value="alkaline habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Okla., Oreg., Pa., R.I., S.C., S.Dak., Tex., Utah, Vt., Va., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">Narr owleaf goosefoot</other_name>
  
</bio:treatment>