<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">126</other_info_on_meta>
    <other_info_on_meta type="mention_page">128</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">136</other_info_on_meta>
    <other_info_on_meta type="mention_page">139</other_info_on_meta>
    <other_info_on_meta type="mention_page">141</other_info_on_meta>
    <other_info_on_meta type="treatment_page">135</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">opuntia</taxon_name>
    <taxon_name authority="Salm-Dyck ex Engelmann" date="unknown" rank="species">engelmannii</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 207. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus opuntia;species engelmannii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415194</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, with short trunk, spreading to sometimes decumbent, 1–3 m.</text>
      <biological_entity id="o23178" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s0" value="spreading to sometimes" value_original="spreading to sometimes" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s0" value="spreading to sometimes" value_original="spreading to sometimes" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o23180" name="trunk" name_original="trunk" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <relation from="o23178" id="r3365" name="with" negation="false" src="d0_s0" to="o23180" />
      <relation from="o23178" id="r3366" name="with" negation="false" src="d0_s0" to="o23180" />
    </statement>
    <statement id="d0_s1">
      <text>Stem segments not disarticulating, yellow-green to blue-green, flattened, circular to obovate to rhombic, or apex tapering, elongate, 15–40 (–120) × 10–40 cm, ± tuberculate, glabrous, often glaucous;</text>
      <biological_entity constraint="stem" id="o23181" name="segment" name_original="segments" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="disarticulating" value_original="disarticulating" />
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s1" to="blue-green" />
        <character is_modifier="false" name="shape" src="d0_s1" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="circular" name="shape" src="d0_s1" to="obovate" />
      </biological_entity>
      <biological_entity id="o23182" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="120" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s1" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>areoles 5–8 per diagonal row across midstem segments, evenly distributed on stem segment to absent, subcircular to obovate, 4–7 × 4–6 mm;</text>
      <biological_entity id="o23183" name="areole" name_original="areoles" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per diagonal row" constraintid="o23184" from="5" name="quantity" src="d0_s2" to="8" />
        <character constraint="on stem segment" constraintid="o23186" is_modifier="false" modifier="evenly" name="arrangement" notes="" src="d0_s2" value="distributed" value_original="distributed" />
        <character char_type="range_value" from="subcircular" name="shape" notes="" src="d0_s2" to="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o23184" name="row" name_original="row" src="d0_s2" type="structure" />
      <biological_entity constraint="midstem" id="o23185" name="segment" name_original="segments" src="d0_s2" type="structure" />
      <biological_entity constraint="stem" id="o23186" name="segment" name_original="segment" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o23184" id="r3367" name="across" negation="false" src="d0_s2" to="o23185" />
    </statement>
    <statement id="d0_s3">
      <text>wool tawny, aging blackish.</text>
      <biological_entity id="o23187" name="wool" name_original="wool" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="blackish" value_original="blackish" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines (0–) 1–6 (–12) per areole, white to yellow, usually red to dark-brown at extreme bases, aging gray to ± black, subulate, straight to curved, flattened to angular at least near base, the longest spreading to strongly reflexed, 10–30 (–50) mm.</text>
      <biological_entity id="o23188" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s4" to="1" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="12" />
        <character char_type="range_value" constraint="per areole" constraintid="o23189" from="1" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s4" to="yellow usually red" />
        <character char_type="range_value" constraint="at extreme bases" constraintid="o23190" from="white" name="coloration" src="d0_s4" to="yellow usually red" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="aging" value_original="aging" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s4" to="more or less black" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="straight" name="course" src="d0_s4" to="curved" />
        <character char_type="range_value" constraint="near base" constraintid="o23191" from="flattened" name="shape" src="d0_s4" to="angular" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s4" value="longest" value_original="longest" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="strongly reflexed" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23189" name="areole" name_original="areole" src="d0_s4" type="structure" />
      <biological_entity constraint="extreme" id="o23190" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o23191" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Glochids widely spaced, sparse in crescent at adaxial edge, encircling areole or nearly so, and scattered in subapical tuft, yellow to redbrown, aging gray to blackish, of irregular lengths, to 10 mm.</text>
      <biological_entity id="o23192" name="glochid" name_original="glochids" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s5" value="spaced" value_original="spaced" />
        <character constraint="at adaxial edge" constraintid="o23193" is_modifier="false" name="count_or_density" src="d0_s5" value="sparse" value_original="sparse" />
        <character constraint="in subapical tuft" constraintid="o23195" is_modifier="false" modifier="nearly" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
        <character char_type="range_value" from="yellow" name="coloration" notes="" src="d0_s5" to="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="aging" value_original="aging" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s5" to="blackish" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="of irregular lengths" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23193" name="edge" name_original="edge" src="d0_s5" type="structure" />
      <biological_entity id="o23194" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity constraint="subapical" id="o23195" name="tuft" name_original="tuft" src="d0_s5" type="structure" />
      <relation from="o23192" id="r3368" name="encircling" negation="false" src="d0_s5" to="o23194" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: inner tepals uniformly yellow to buff, sometimes orange to pink to red (rarely whitish), 30–40 mm;</text>
      <biological_entity id="o23196" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="inner" id="o23197" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="uniformly yellow" name="coloration" src="d0_s6" to="buff" />
        <character char_type="range_value" from="orange" modifier="sometimes" name="coloration" src="d0_s6" to="pink" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s6" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments, anthers, and style whitish to cream;</text>
      <biological_entity id="o23198" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o23199" name="filament" name_original="filaments" src="d0_s7" type="structure" />
      <biological_entity id="o23200" name="anther" name_original="anthers" src="d0_s7" type="structure" />
      <biological_entity id="o23201" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s7" to="cream" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigma lobes yellow-green to green.</text>
      <biological_entity id="o23202" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="stigma" id="o23203" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s8" to="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits dark red to purple throughout, sometimes stipitate, ovate-elongate to barrel-shaped, 35–90 × 20–40 mm, juicy (bleeding and staining), glabrous, spineless;</text>
      <biological_entity id="o23204" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character char_type="range_value" from="dark red" modifier="throughout" name="coloration" src="d0_s9" to="purple" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s9" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="ovate-elongate" name="shape" src="d0_s9" to="barrel-shaped" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s9" to="90" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s9" to="40" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s9" value="juicy" value_original="juicy" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="spineless" value_original="spineless" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>areoles 20–32 usually toward apex.</text>
      <biological_entity id="o23205" name="areole" name_original="areoles" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="toward apex" constraintid="o23206" from="20" name="quantity" src="d0_s10" to="32" />
      </biological_entity>
      <biological_entity id="o23206" name="apex" name_original="apex" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds tan to grayish, subcircular to deltoid, flattened, 2.5–6 × 2–5 mm;</text>
      <biological_entity id="o23207" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s11" to="grayish" />
        <character char_type="range_value" from="subcircular" name="shape" src="d0_s11" to="deltoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>girdle protruding 0.3–0.5 mm.</text>
      <biological_entity id="o23208" name="girdle" name_original="girdle" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="protruding" value_original="protruding" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., La., N.Mex., Nev., Tex., Utah; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="past_name">engelmanni</other_name>
  <other_name type="common_name">Engelmann’s pricklypear</other_name>
  <other_name type="common_name">cactus apple</other_name>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <discussion>The basal portions of stems seedlings of Opuntia engelmannii bear long hairlike spines.</discussion>
  <discussion>The name Opuntia dillei Griffiths has been used for a spineless or nearly spineless morphotype of O. engelmannii.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem segments becoming very elongate, 2+ times longer than wide, lanceolate to linear-lanceolate, often falcate</description>
      <determination>12b Opuntia engelmannii var. linguiformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem segments to 1.5 times longer than wide, commonly circular to obovate to rhomboid</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Spines chalky white, yellow when wetted, with dark red-brown extreme bases.</description>
      <determination>12a Opuntia engelmannii var. engelmannii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Spines pale to dark yellow, not chalky white, at times with red-brown extreme bases</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem segments with spines straight or slightly curving, spreading</description>
      <determination>12c Opuntia engelmannii var. lindheim</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem segments with spines strongly deflexed or arching downward near bases</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Spines arching downward near bases; sc Ari- zona</description>
      <determination>12d Opuntia engelmannii var. flavispi</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Spines strongly deflexed; s Texas.</description>
      <determination>12e Opuntia engelmannii var. flexispi</determination>
    </key_statement>
  </key>
</bio:treatment>