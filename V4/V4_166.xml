<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="treatment_page">81</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">aizoaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">sesuvium</taxon_name>
    <taxon_name authority="(Linnaeus) Linnaeus" date="1759" rank="species">portulacastrum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1058. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aizoaceae;genus sesuvium;species portulacastrum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200007017</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Portulaca</taxon_name>
    <taxon_name authority=" Linnaeus" date="unknown" rank="species">portulacastrum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 446. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Portulaca;species portulacastrum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Halimus</taxon_name>
    <taxon_name authority="(Linnaeus) Kuntze" date="unknown" rank="species">portulacastrum</taxon_name>
    <taxon_hierarchy>genus Halimus;species portulacastrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous.</text>
      <biological_entity id="o11036" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, forming mats to 2 m diam., branched;</text>
      <biological_entity id="o11038" name="mat" name_original="mats" src="d0_s1" type="structure" />
      <relation from="o11037" id="r1614" name="forming" negation="false" src="d0_s1" to="o11038" />
    </statement>
    <statement id="d0_s2">
      <text>rooting at nodes.</text>
      <biological_entity id="o11037" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="2" to_unit="m" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="at nodes" constraintid="o11039" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o11039" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole ± absent;</text>
      <biological_entity id="o11040" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11041" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate to elliptic-ovate, to 6 × 2.5 cm, tapered to clasping base.</text>
      <biological_entity id="o11042" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o11043" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="elliptic-ovate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s4" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o11044" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: flowers solitary;</text>
      <biological_entity id="o11045" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o11046" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pedicel to 20 mm.</text>
      <biological_entity id="o11047" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o11048" name="pedicel" name_original="pedicel" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes pink-purple adaxially, with subapical abaxial appendages, ovate to lanceolate, 3–10 mm;</text>
      <biological_entity id="o11049" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o11050" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration_or_density" src="d0_s7" value="pink-purple" value_original="pink-purple" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="subapical abaxial" id="o11051" name="appendage" name_original="appendages" src="d0_s7" type="structure" />
      <relation from="o11050" id="r1615" name="with" negation="false" src="d0_s7" to="o11051" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 30;</text>
      <biological_entity id="o11052" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o11053" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="30" value_original="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pistil 5-carpellate;</text>
      <biological_entity id="o11054" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o11055" name="pistil" name_original="pistil" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-carpellate" value_original="5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary 5-loculed;</text>
      <biological_entity id="o11056" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11057" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="5-loculed" value_original="5-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 5.</text>
      <biological_entity id="o11058" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11059" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules conic, 10 mm.</text>
      <biological_entity id="o11060" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="conic" value_original="conic" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 30–60, black, 1.2–1.5 mm, shiny, smooth.</text>
      <biological_entity id="o11061" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s13" to="60" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet or desiccated soils, beaches, dunes, margins of coastal wetlands, waste grounds, ballast</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="desiccated soils" />
        <character name="habitat" value="beaches" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="margins" constraint="of coastal wetlands" />
        <character name="habitat" value="coastal wetlands" />
        <character name="habitat" value="waste grounds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-5 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="5" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., Pa., S.C., Tex.; Mexico; South America; Europe; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Cencilla</other_name>
  <other_name type="common_name">shoreline sea-purslane</other_name>
  <discussion>Sesuvium portulacastrum is a widespread and variable subtropical and tropical species to which many names have been applied, particularly to material collected beyond North America (e.g., Argentina and Brazil). Although S. portulacastrum occurs or has been reported in natural habitats on the east coast of North America north to North Carolina, and from ballast north to the Delaware River in Pennsylvania, there are no verified records for this species occurring in western North America north of Mexico, where it occurs northward along the coasts of Sonora and Baja California. All records or collections of S. portulacastrum from desert wetlands of the United States are included in S. verrucosum.</discussion>
  
</bio:treatment>