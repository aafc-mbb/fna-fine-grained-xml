<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="treatment_page">212</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose in N. L. Britton and A. Brown" date="1913" rank="genus">pediocactus</taxon_name>
    <taxon_name authority="(Engelmann ex J. M. Coulter) L. D. Benson" date="1961" rank="species">sileri</taxon_name>
    <place_of_publication>
      <publication_title>Cact. Succ. J. (Los Angeles)</publication_title>
      <place_in_publication>33: 53. 1961</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus pediocactus;species sileri;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242415302</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocactus</taxon_name>
    <taxon_name authority="Engelmann ex J. M. Coulter" date="unknown" rank="species">sileri</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>3: 376. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Echinocactus;species sileri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Utahia</taxon_name>
    <taxon_name authority="(Engelmann ex J. M. Coulter) Britton &amp; Rose" date="unknown" rank="species">sileri</taxon_name>
    <taxon_hierarchy>genus Utahia;species sileri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants few branched or unbranched.</text>
      <biological_entity id="o12153" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems depressed-ovoid or occasionally elongate-cylindric, 5–15 (–25) × 6–11.5 cm;</text>
      <biological_entity id="o12154" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="depressed-ovoid" value_original="depressed-ovoid" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s1" value="elongate-cylindric" value_original="elongate-cylindric" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s1" to="11.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>areoles circular to pyriform, villous to lanate.</text>
      <biological_entity id="o12155" name="areole" name_original="areoles" src="d0_s2" type="structure">
        <character char_type="range_value" from="circular" name="shape" src="d0_s2" to="pyriform" />
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s2" to="lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spines distinguishable as radial and central: radial spines 11–15 per areole, white, 11–21 mm;</text>
      <biological_entity id="o12156" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character constraint="as central spines" constraintid="o12157" is_modifier="false" name="prominence" src="d0_s3" value="distinguishable" value_original="distinguishable" />
      </biological_entity>
      <biological_entity constraint="central" id="o12157" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="radial" value_original="radial" />
      </biological_entity>
      <biological_entity id="o12158" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="radial" value_original="radial" />
        <character char_type="range_value" constraint="per areole" constraintid="o12159" from="11" name="quantity" src="d0_s3" to="15" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="white" value_original="white" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s3" to="21" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12159" name="areole" name_original="areole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>central spines 3–7 per areole, nearly porrect, brownish black aging to pale gray or white, straight or slightly curving at tips, 13–30 × 1 mm at base.</text>
      <biological_entity id="o12160" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character constraint="as central spines" constraintid="o12161" is_modifier="false" name="prominence" src="d0_s4" value="distinguishable" value_original="distinguishable" />
      </biological_entity>
      <biological_entity constraint="central" id="o12161" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="radial" value_original="radial" />
      </biological_entity>
      <biological_entity constraint="central" id="o12162" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o12163" from="3" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" modifier="nearly" name="orientation" notes="" src="d0_s4" value="porrect" value_original="porrect" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brownish black" value_original="brownish black" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale gray" value_original="pale gray" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character constraint="at tips" constraintid="o12164" is_modifier="false" modifier="slightly" name="course" src="d0_s4" value="curving" value_original="curving" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" notes="" src="d0_s4" to="30" to_unit="mm" />
        <character constraint="at base" constraintid="o12165" name="width" notes="" src="d0_s4" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12163" name="areole" name_original="areole" src="d0_s4" type="structure" />
      <biological_entity id="o12164" name="tip" name_original="tips" src="d0_s4" type="structure" />
      <biological_entity id="o12165" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers 0.8–2.2 × 2–3 cm;</text>
      <biological_entity id="o12166" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s5" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scales long fringed;</text>
      <biological_entity id="o12167" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s6" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>outer tepals brown with white margins, long fringed, 9–15 × 3–4.5 mm;</text>
      <biological_entity constraint="outer" id="o12168" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character constraint="with margins" constraintid="o12169" is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s7" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s7" value="fringed" value_original="fringed" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12169" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>inner tepals yellow with purple veins, 15–20 × 4.5–6 mm.</text>
      <biological_entity constraint="inner" id="o12170" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character constraint="with veins" constraintid="o12171" is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" notes="" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" notes="" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12171" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits greenish yellow, short cylindric, 12–15 × 6–9 mm.</text>
      <biological_entity id="o12172" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds gray to nearly black, 3.5–5 × 3–3.5 mm, papillate and rugose.</text>
      <biological_entity id="o12173" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s10" to="nearly black" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="relief" src="d0_s10" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rounded hills in gypsum clay and sandy soils of Moenkopi Formation, Great Basin desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rounded hills" constraint="in gypsum clay" />
        <character name="habitat" value="gypsum clay" />
        <character name="habitat" value="sandy soils" constraint="of moenkopi formation , great basin desert scrub" />
        <character name="habitat" value="great basin desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Gypsum cactus</other_name>
  <other_name type="common_name">Siler’s pincushion cactus</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Pediocactus sileri stems tend to develop a basal thatch of spines that anchor it to the fine, gypsum-rich soil. This species is endemic to a narrow strip along the Arizona-Utah border.</discussion>
  
</bio:treatment>