<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">371</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">atriplex</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) S. L. Welsh" date="2001" rank="subgenus">Pterochiton</taxon_name>
    <taxon_name authority="(Moquin-Tandon) D. Dietrich" date="1852" rank="species">gardneri</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">gardneri</taxon_name>
    <taxon_hierarchy>family chenopodiaceae;genus atriplex;subgenus pterochiton;species gardneri;variety gardneri;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415583</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, dioecious or sparingly monoecious, mostly 1–4 × 2–15 dm.</text>
      <biological_entity id="o5074" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" modifier="sparingly" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character char_type="range_value" from="1" from_unit="dm" modifier="mostly" name="length" src="d0_s0" to="4" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" modifier="mostly" name="width" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending.</text>
      <biological_entity id="o5075" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent or subpersistent, opposite to subalternate proximally, alternate distally;</text>
      <biological_entity id="o5076" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="subpersistent" value_original="subpersistent" />
        <character char_type="range_value" from="opposite" modifier="proximally" name="arrangement" src="d0_s2" to="subalternate" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 4 mm;</text>
      <biological_entity id="o5077" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade green, spatulate to oblanceolate or obovate, 12–55 × 5–12 mm, 2–5 times longer than wide.</text>
      <biological_entity id="o5078" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate or obovate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s4" to="55" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="2-5" value_original="2-5" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Staminate flowers dark-brown or yellow (especially in the northern Great Plains), in glomerules 2–4 mm thick, in terminal panicles 3–18 cm.</text>
      <biological_entity id="o5079" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o5080" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o5081" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="18" to_unit="cm" />
      </biological_entity>
      <relation from="o5079" id="r686" name="in" negation="false" src="d0_s5" to="o5080" />
      <relation from="o5079" id="r687" name="in" negation="false" src="d0_s5" to="o5081" />
    </statement>
    <statement id="d0_s6">
      <text>Pistillate flowers in rather leafy unbranched spikes or panicles 10–25 cm.</text>
      <biological_entity id="o5083" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="rather" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o5084" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="25" to_unit="cm" />
      </biological_entity>
      <relation from="o5082" id="r688" name="in" negation="false" src="d0_s6" to="o5083" />
      <relation from="o5082" id="r689" name="in" negation="false" src="d0_s6" to="o5084" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting bracteoles sessile or stipe 2–5 mm, body globose to flattened, 3–6 × 2–4 (–5) mm, terminal tooth to 1.5 mm, subtended with 2–4 (–7) lateral teeth, surface densely, shortly tuberculate to smooth and free of tubercles.</text>
      <biological_entity id="o5082" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o5085" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure" />
      <biological_entity id="o5086" name="stipe" name_original="stipe" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5087" name="body" name_original="body" src="d0_s7" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s7" to="flattened" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o5088" name="tooth" name_original="tooth" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o5089" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="7" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o5090" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="7" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o5091" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="shortly" name="relief" src="d0_s7" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="true" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" value_original="free" />
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="7" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o5082" id="r690" name="fruiting" negation="false" src="d0_s7" to="o5085" />
      <relation from="o5088" id="r691" name="subtended with" negation="false" src="d0_s7" to="o5089" />
      <relation from="o5088" id="r692" name="subtended with" negation="false" src="d0_s7" to="o5090" />
      <relation from="o5088" id="r693" name="subtended with" negation="false" src="d0_s7" to="o5091" />
    </statement>
    <statement id="d0_s8">
      <text>Seeds 1.5–2 mm wide.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 18, 36.</text>
      <biological_entity id="o5092" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5093" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
        <character name="quantity" src="d0_s9" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Greasewood and sagebrush-saltbush communities, mainly on fine-textured saline substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="greasewood" />
        <character name="habitat" value="sagebrush-saltbush communities" />
        <character name="habitat" value="fine-textured saline substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Colo., Idaho, Mont., Nebr., N.Dak., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>51a.</number>
  
</bio:treatment>