<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="treatment_page">219</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1922" rank="genus">hamatocactus</taxon_name>
    <taxon_name authority="(Terán &amp; Berlandier) I. M. Johnston" date="1924" rank="species">bicolor</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>70: 88. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus hamatocactus;species bicolor;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415315</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cactus</taxon_name>
    <taxon_name authority="Terán &amp; Berlandier" date="unknown" rank="species">bicolor</taxon_name>
    <place_of_publication>
      <publication_title>in J. L. Berlandier, Mem. Comis. Limites,</publication_title>
      <place_in_publication>4. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cactus;species bicolor;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ferocactus</taxon_name>
    <taxon_name authority="(Engelma nn) L. D. Benson" date="unknown" rank="species">setispinus</taxon_name>
    <taxon_hierarchy>genus Ferocactus;species setispinus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hamatocactus</taxon_name>
    <taxon_name authority="(Englemann) Britton &amp; Rose" date="unknown" rank="species">setispinus</taxon_name>
    <taxon_hierarchy>genus Hamatocactus;species setispinus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelocactus</taxon_name>
    <taxon_name authority="(Engelmann) E. F. Anderson" date="unknown" rank="species">setispinus</taxon_name>
    <taxon_hierarchy>genus Thelocactus;species setispinus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Spines: central spine yellowish, becoming ashy gray or reddish-brown, 1–3.8 cm, minutely scabrous (use 30× lens).</text>
      <biological_entity id="o17333" name="spine" name_original="spines" src="d0_s0" type="structure" />
      <biological_entity constraint="central" id="o17334" name="spine" name_original="spine" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s0" value="ashy gray" value_original="ashy gray" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="3.8" to_unit="cm" />
        <character is_modifier="false" modifier="minutely" name="pubescence_or_relief" src="d0_s0" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers yellow with red centers, leaving obvious, concave abscission scars after fall of flower or fruit;</text>
      <biological_entity id="o17335" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character constraint="with centers" constraintid="o17336" is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o17336" name="center" name_original="centers" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o17337" name="scar" name_original="scars" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="obvious" value_original="obvious" />
        <character is_modifier="true" name="shape" src="d0_s1" value="concave" value_original="concave" />
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="abscission" value_original="abscission" />
      </biological_entity>
      <biological_entity id="o17338" name="fall" name_original="fall" src="d0_s1" type="structure" />
      <biological_entity id="o17339" name="flower" name_original="flower" src="d0_s1" type="structure" />
      <biological_entity id="o17340" name="fruit" name_original="fruit" src="d0_s1" type="structure" />
      <relation from="o17335" id="r2500" name="leaving" negation="false" src="d0_s1" to="o17337" />
      <relation from="o17335" id="r2501" name="after" negation="false" src="d0_s1" to="o17338" />
      <relation from="o17338" id="r2502" name="part_of" negation="false" src="d0_s1" to="o17339" />
      <relation from="o17338" id="r2503" name="part_of" negation="false" src="d0_s1" to="o17340" />
    </statement>
    <statement id="d0_s2">
      <text>proximal outer tepals obtuse or with broad ± auriculate distal portions;</text>
      <biological_entity constraint="distal" id="o17342" name="portion" name_original="portions" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="broad" value_original="broad" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s2" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <relation from="o17341" id="r2504" name="with" negation="false" src="d0_s2" to="o17342" />
    </statement>
    <statement id="d0_s3">
      <text>distal ones minutely fringed, acute;</text>
      <biological_entity constraint="proximal outer" id="o17341" name="tepal" name_original="tepals" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s2" value="with broad more or less auriculate distal portions" />
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s3" value="fringed" value_original="fringed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>inner tepals oblanceolate;</text>
      <biological_entity constraint="inner" id="o17343" name="tepal" name_original="tepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments reddish, pale-yellow, or whitish, collectively spiraled around style;</text>
      <biological_entity id="o17344" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
        <character constraint="around style" constraintid="o17345" is_modifier="false" modifier="collectively" name="arrangement" src="d0_s5" value="spiraled" value_original="spiraled" />
      </biological_entity>
      <biological_entity id="o17345" name="style" name_original="style" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ovary 8–13 mm diam.</text>
      <biological_entity id="o17346" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s6" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Fruits displaced away from stem apex by apical growth subsequent to flowering.</text>
      <biological_entity constraint="stem" id="o17348" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity constraint="apical" id="o17349" name="growth" name_original="growth" src="d0_s7" type="structure">
        <character is_modifier="false" name="growth_order" src="d0_s7" value="subsequent" value_original="subsequent" />
      </biological_entity>
      <relation from="o17347" id="r2505" name="displaced away from" negation="false" src="d0_s7" to="o17348" />
      <relation from="o17348" id="r2506" name="by" negation="false" src="d0_s7" to="o17349" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 22.</text>
      <biological_entity id="o17347" name="fruit" name_original="fruits" src="d0_s7" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s7" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17350" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, shrublands, especially mesquite thickets, heavy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="mesquite thickets" modifier="especially" />
        <character name="habitat" value="heavy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Twisted-rib cactus</other_name>
  <discussion>Hamatocactus bicolor is taxonomically isolated in the flora. It is perhaps most similar to Thelocactus bicolor, but H. bicolor is dramatically different in its juicy, scarlet fruits, yellow flowers, very narrowly and sharply ribbed stem surface, and hooked central spines. Hamatocactus bicolor is superficially similar to Ferocactus hamatacanthus var. sinuatus, which is larger and green-fruited with pitted seed coat and much larger yellow flowers lacking red centers. Glandulicactus uncinatus has hooked radial spines, rounded birbs, and small, maroon flowers.</discussion>
  
</bio:treatment>