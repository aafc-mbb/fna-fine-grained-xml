<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="treatment_page">430</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">amaranthaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">amaranthus</taxon_name>
    <taxon_name authority="(Kunth) Grenier &amp; Godron" date="1855" rank="subgenus">Albersia</taxon_name>
    <taxon_name authority="Linnaeus" date="1771" rank="species">deflexus</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.</publication_title>
      <place_in_publication>2: 295. 1771</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amaranthaceae;genus amaranthus;subgenus albersia;species deflexus;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242415670</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants short-lived perennial or annual, pubescent in distal parts of plant or becoming glabrescent at maturity.</text>
      <biological_entity id="o19305" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character constraint="in distal parts" constraintid="o19306" is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character constraint="at maturity" is_modifier="false" modifier="becoming" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19306" name="part" name_original="parts" src="d0_s0" type="structure" />
      <biological_entity id="o19307" name="plant" name_original="plant" src="d0_s0" type="structure" />
      <relation from="o19306" id="r2811" name="part_of" negation="false" src="d0_s0" to="o19307" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or prostrate, profusely branched basally, radiating from rootstock, mostly 0.2–0.5 m.</text>
      <biological_entity id="o19308" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="profusely; basally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="from rootstock" constraintid="o19309" is_modifier="false" name="arrangement" src="d0_s1" value="radiating" value_original="radiating" />
        <character char_type="range_value" from="0.2" from_unit="m" modifier="mostly" name="some_measurement" notes="" src="d0_s1" to="0.5" to_unit="m" />
      </biological_entity>
      <biological_entity id="o19309" name="rootstock" name_original="rootstock" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1/2 as long as to equaling blade;</text>
      <biological_entity id="o19310" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19311" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character constraint="as-long-as " constraintid="o19312" name="quantity" src="d0_s2" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o19312" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="true" name="variability" src="d0_s2" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade rhombic-ovate or ovate to lanceolate, 1–2 × 0.5–1 cm, base tapering or cuneate, margins entire, plane or slightly undulate, apex subacute, obtuse, or retuse or shallowly emarginate, mucronulate.</text>
      <biological_entity id="o19313" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19314" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19315" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19316" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o19317" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subacute" value_original="subacute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="retuse" value_original="retuse" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="retuse" value_original="retuse" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, erect, compact, pyramidal panicles and also some axillary clusters, green or silvery green, occasionally tinged with red, leafless at least distally.</text>
      <biological_entity id="o19318" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o19319" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="silvery green" value_original="silvery green" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s4" value="tinged with red" value_original="tinged with red" />
        <character is_modifier="false" modifier="at-least distally; distally" name="architecture" src="d0_s4" value="leafless" value_original="leafless" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Bracts of pistillate flowers linear, 0.5–1 mm, 1/2 as long as tepals.</text>
      <biological_entity id="o19320" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character constraint="as-long-as tepals" constraintid="o19322" name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o19321" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19322" name="tepal" name_original="tepals" src="d0_s5" type="structure" />
      <relation from="o19320" id="r2812" name="part_of" negation="false" src="d0_s5" to="o19321" />
    </statement>
    <statement id="d0_s6">
      <text>Pistillate flowers: tepals 2–3, narrowly elliptic or oblanceolate, not clawed, equal or subequal, 1.2–2 mm, apex broadly acute;</text>
      <biological_entity id="o19323" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19324" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="clawed" value_original="clawed" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19325" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style-branches erect;</text>
      <biological_entity id="o19326" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19327" name="style-branch" name_original="style-branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigmas 3.</text>
      <biological_entity id="o19328" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19329" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate flowers clustered at tips of inflorescences;</text>
      <biological_entity id="o19330" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character constraint="at tips" constraintid="o19331" is_modifier="false" name="arrangement_or_growth_form" src="d0_s9" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o19331" name="tip" name_original="tips" src="d0_s9" type="structure" />
      <biological_entity id="o19332" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <relation from="o19331" id="r2813" name="part_of" negation="false" src="d0_s9" to="o19332" />
    </statement>
    <statement id="d0_s10">
      <text>tepals 2–3;</text>
      <biological_entity id="o19333" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 2–3.</text>
      <biological_entity id="o19334" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Utricles marked with 2 (–3) green lines that intersect at apex and divide fruit into halves or quarters, slightly to distinctly inflated, ellipsoid, 2–3 mm, distinctly longer than tepals, smooth (in dry plants wrinkled or rugose), indehiscent.</text>
      <biological_entity id="o19335" name="utricle" name_original="utricles" src="d0_s12" type="structure" />
      <biological_entity id="o19336" name="line" name_original="lines" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s12" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" modifier="slightly to distinctly" name="shape" notes="" src="d0_s12" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character constraint="than tepals" constraintid="o19340" is_modifier="false" name="length_or_size" src="d0_s12" value="distinctly longer" value_original="distinctly longer" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o19337" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o19338" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o19339" name="half" name_original="halves" src="d0_s12" type="structure" />
      <biological_entity id="o19340" name="tepal" name_original="tepals" src="d0_s12" type="structure" />
      <relation from="o19335" id="r2814" name="marked with" negation="false" src="d0_s12" to="o19336" />
      <relation from="o19336" id="r2815" name="intersect at" negation="false" src="d0_s12" to="o19337" />
      <relation from="o19336" id="r2816" name="intersect at" negation="false" src="d0_s12" to="o19338" />
      <relation from="o19336" id="r2817" name="into" negation="false" src="d0_s12" to="o19339" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds very dark-brown to black, 1–1.2 mm diam., shiny, filling only proximal portion of fruit.</text>
      <biological_entity id="o19341" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s13" to="black" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s13" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity id="o19342" name="portion" name_original="portion" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="only" name="position" src="d0_s13" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o19343" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
      <relation from="o19341" id="r2818" name="filling" negation="false" src="d0_s13" to="o19342" />
      <relation from="o19341" id="r2819" name="part_of" negation="false" src="d0_s13" to="o19343" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Weedy areas, ballast heaps, railroads, other disturbed habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="weedy areas" />
        <character name="habitat" value="ballast heaps" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="other disturbed habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Calif., Fla., Ga., La., Mass., N.J., N.Y., Oreg., Pa., Tenn., Va.; native to South America; locally introduced or naturalized in tropical to warm-temperate regions of the globe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="native to South America" establishment_means="native" />
        <character name="distribution" value="locally  or naturalized in tropical to warm-temperate regions of the globe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <other_name type="common_name">Large-fruit amaranth</other_name>
  <other_name type="common_name">Argentina amaranth</other_name>
  <other_name type="common_name">deflexed amaranth</other_name>
  <other_name type="common_name">low amaranth</other_name>
  <discussion>The hybrid between Amaranthus deflexus and A. muricatus was described from Europe as A. ×tarraconensis Sennen &amp; Pau (see J. L. Carretero 1979) and may be expected in North America in the future in places of possible co-occurrence of the parental species.</discussion>
  
</bio:treatment>