<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="treatment_page">68</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">nyctaginaceae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">abronia</taxon_name>
    <taxon_name authority="Eschscholtz" date="1826" rank="species">latifolia</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Acad. Imp. Sci. St. Pétersbourg Hist. Acad.</publication_title>
      <place_in_publication>10: 281. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family nyctaginaceae;genus abronia;species latifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415100</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o7633" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, often buried in sand, usually much branched, forming large mats, succulent, densely glandular-pubescent to glabrous.</text>
      <biological_entity id="o7634" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character constraint="in sand" is_modifier="false" modifier="often" name="location" src="d0_s1" value="buried" value_original="buried" />
        <character is_modifier="false" modifier="usually much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="texture" src="d0_s1" value="succulent" value_original="succulent" />
        <character char_type="range_value" from="densely glandular-pubescent" name="pubescence" src="d0_s1" to="glabrous" />
      </biological_entity>
      <biological_entity id="o7635" name="mat" name_original="mats" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="large" value_original="large" />
      </biological_entity>
      <relation from="o7634" id="r1051" name="forming" negation="false" src="d0_s1" to="o7635" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: 1–6 cm;</text>
      <biological_entity id="o7636" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade deltate-ovate to reniform, 2.2–4.8 × 2.7–5.2 cm, margins usually entire, sometimes slightly repand and undulate, surfaces glabrous or viscid-puberulent.</text>
      <biological_entity id="o7637" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7638" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="deltate-ovate" name="shape" src="d0_s3" to="reniform" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="length" src="d0_s3" to="4.8" to_unit="cm" />
        <character char_type="range_value" from="2.7" from_unit="cm" name="width" src="d0_s3" to="5.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7639" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s3" value="repand" value_original="repand" />
        <character is_modifier="false" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o7640" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="viscid-puberulent" value_original="viscid-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: peduncle longer than subtending petiole;</text>
      <biological_entity id="o7641" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o7642" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character constraint="than subtending petiole" constraintid="o7643" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o7643" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>bracts ovate, 5–9 × 3–5 mm, thin, yellowish green, glandular-pubescent;</text>
      <biological_entity id="o7644" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o7645" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flowers 17–35.</text>
      <biological_entity id="o7646" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o7647" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="17" name="quantity" src="d0_s6" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perianth: tube yellowish green, 6.5–18 mm, limb yellow, 8–13 mm diam., lobes slightly to moderately reflexed.</text>
      <biological_entity id="o7648" name="perianth" name_original="perianth" src="d0_s7" type="structure" />
      <biological_entity id="o7649" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7650" name="limb" name_original="limb" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7651" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly to moderately" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits winged, ± rhombic in profile, attenuate at both ends, 8–15 × 6–14 mm, scarious;</text>
      <biological_entity id="o7652" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="more or less; in profile" name="shape" src="d0_s8" value="rhombic" value_original="rhombic" />
        <character constraint="at ends" constraintid="o7653" is_modifier="false" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" notes="" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" notes="" src="d0_s8" to="14" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o7653" name="end" name_original="ends" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>wings 4–5, thin walled, small, cavities extending into wing.</text>
      <biological_entity id="o7654" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="walled" value_original="walled" />
        <character is_modifier="false" name="size" src="d0_s9" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o7655" name="cavity" name_original="cavities" src="d0_s9" type="structure" />
      <biological_entity id="o7656" name="wing" name_original="wing" src="d0_s9" type="structure" />
      <relation from="o7655" id="r1052" name="extending into" negation="false" src="d0_s9" to="o7656" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, coastal scrub, lees of dunes adjacent to strand</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="lees" constraint="of dunes adjacent to strand" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="strand" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Yellow sand-verbena</other_name>
  <discussion>S. S. Tillett (1967) considered plants of Abronia umbellata var. minor (Standley) Munz to be introgressants between A. latifolia and A. umbellata.</discussion>
  
</bio:treatment>