<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="treatment_page">101</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Engelmann in W. H. Brewer et al." date="unknown" rank="subfamily">Pereskioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">pereskia</taxon_name>
    <taxon_name authority="Miller" date="1768" rank="species">aculeata</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily pereskioideae;genus pereskia;species aculeata;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242413988</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cactus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pereskia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 469. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cactus;species pereskia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or vines, clambering, 3–10 m.</text>
      <biological_entity id="o9224" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="clambering" value_original="clambering" />
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="clambering" value_original="clambering" />
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 3 cm diam., spiny;</text>
      <biological_entity id="o9226" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>areoles to 15 mm diam., largest on basal portion of stem.</text>
      <biological_entity id="o9227" name="areole" name_original="areoles" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s2" to="15" to_unit="mm" />
        <character constraint="on basal portion" constraintid="o9228" is_modifier="false" name="size" src="d0_s2" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9228" name="portion" name_original="portion" src="d0_s2" type="structure" />
      <biological_entity id="o9229" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o9228" id="r1315" name="part_of" negation="false" src="d0_s2" to="o9229" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves lanceolate to ovate or oblong, 4.5–11 × 1.5–5 cm, 0.5–1 mm thick.</text>
      <biological_entity id="o9230" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovate or oblong" />
        <character name="thickness" src="d0_s3" unit="cm" value="4.5-11×1.5-5" value_original="4.5-11×1.5-5" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines of 2 kinds;</text>
      <biological_entity id="o9231" name="spine" name_original="spines" src="d0_s4" type="structure" />
      <biological_entity id="o9232" name="kind" name_original="kinds" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <relation from="o9231" id="r1316" name="consist_of" negation="false" src="d0_s4" to="o9232" />
    </statement>
    <statement id="d0_s5">
      <text>primary spines (= first-formed) 2 per areole, recurved, clawlike, 4–8 mm;</text>
      <biological_entity constraint="primary" id="o9233" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character constraint="per areole" constraintid="o9234" name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s5" value="clawlike" value_original="clawlike" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9234" name="areole" name_original="areole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>secondary spines to 25 per older areole, straight, 10–35 mm.</text>
      <biological_entity constraint="secondary" id="o9235" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character constraint="per areole" constraintid="o9236" name="quantity" src="d0_s6" value="25" value_original="25" />
        <character is_modifier="false" name="course" notes="" src="d0_s6" value="straight" value_original="straight" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9236" name="areole" name_original="areole" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="older" value_original="older" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers to 70 in terminal or lateral inflorescences, fragrant, 3 × 2.5–5 cm;</text>
      <biological_entity id="o9237" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in inflorescences" constraintid="o9238" from="0" name="quantity" src="d0_s7" to="70" />
        <character is_modifier="false" name="odor" notes="" src="d0_s7" value="fragrant" value_original="fragrant" />
        <character name="length" src="d0_s7" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s7" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9238" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels 5–15 mm;</text>
      <biological_entity id="o9239" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals perigynous;</text>
      <biological_entity id="o9240" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>scales and areoles on prominent to inconspicuous tubercles;</text>
      <biological_entity id="o9241" name="scale" name_original="scales" src="d0_s10" type="structure" />
      <biological_entity id="o9242" name="areole" name_original="areoles" src="d0_s10" type="structure" />
      <biological_entity id="o9243" name="tubercle" name_original="tubercles" src="d0_s10" type="structure">
        <character char_type="range_value" from="prominent" is_modifier="true" name="prominence" src="d0_s10" to="inconspicuous" />
      </biological_entity>
      <relation from="o9241" id="r1317" name="on" negation="false" src="d0_s10" to="o9243" />
      <relation from="o9242" id="r1318" name="on" negation="false" src="d0_s10" to="o9243" />
    </statement>
    <statement id="d0_s11">
      <text>perianth whitish to light pink.</text>
      <biological_entity id="o9244" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s11" to="light pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits yellow to orange, spheric, not angled, 40 × 15–25 mm, never proliferating.</text>
      <biological_entity id="o9245" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s12" to="orange" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spheric" value_original="spheric" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="angled" value_original="angled" />
        <character name="length" src="d0_s12" unit="mm" value="40" value_original="40" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s12" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="never" name="life_cycle" src="d0_s12" value="proliferating" value_original="proliferating" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds lenticular, 4.5–5 mm diam., glossy.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 22.</text>
      <biological_entity id="o9246" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="diameter" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9247" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (Aug–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed shell middens (Fla), riparian woodlands with fine, sandy loam (Tex.)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed shell middens" />
        <character name="habitat" value="riparian woodlands" constraint="with fine , sandy loam" />
        <character name="habitat" value="fine" />
        <character name="habitat" value="sandy loam" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., Tex.; West Indies; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Lemon vine</other_name>
  <other_name type="common_name">blade apple cactus</other_name>
  <other_name type="common_name">Barbados gooseberry</other_name>
  <discussion>Pereskia aculeata is cultivated as an ornamental, both for its edible fruits and fragrant flowers (though the scent is considered unpleasant by some). The species has escaped from cultivation in seven counties in Florida: Brevard, Dade, Indian River, Manatee, Highlands, St. Lucie, and Palm Beach, and it is established in Willacy County, near the southernmost tip of Texas (J. Ideker 1996).</discussion>
  
</bio:treatment>