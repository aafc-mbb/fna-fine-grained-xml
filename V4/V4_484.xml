<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Leila M. Shultz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">261</other_info_on_meta>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">chenopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">BETA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 222. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 103. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family chenopodiaceae;genus BETA</taxon_hierarchy>
    <other_info_on_name type="etymology">derivation uncertain, possibly from Celtic name for red root</other_info_on_name>
    <other_info_on_name type="fna_id">103882</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, biennial, or perennial, often with fleshy, thickened roots, glabrous throughout.</text>
      <biological_entity id="o4964" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="throughout" name="pubescence" notes="" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o4965" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o4964" id="r673" modifier="often" name="with" negation="false" src="d0_s0" to="o4965" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or procumbent, not jointed, not armed, not fleshy.</text>
      <biological_entity id="o4966" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="jointed" value_original="jointed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="armed" value_original="armed" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, petiolate or sessile;</text>
      <biological_entity id="o4967" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate-cordate to rhombic-cuneate, margins ± entire, apex obtuse.</text>
      <biological_entity id="o4968" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate-cordate" name="shape" src="d0_s3" to="rhombic-cuneate" />
      </biological_entity>
      <biological_entity id="o4969" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4970" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences spikelike cymes or glomerules, ebracteate at least in distal 1/2.</text>
      <biological_entity id="o4971" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character constraint="in distal 1/2" constraintid="o4974" is_modifier="false" name="architecture" src="d0_s4" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
      <biological_entity id="o4972" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character constraint="in distal 1/2" constraintid="o4974" is_modifier="false" name="architecture" src="d0_s4" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
      <biological_entity id="o4973" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character constraint="in distal 1/2" constraintid="o4974" is_modifier="false" name="architecture" src="d0_s4" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4974" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers bisexual, bracteate;</text>
      <biological_entity id="o4975" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth segments 3–5, distinct, sometimes petaloid, rounded or keeled abaxially, wings and spines absent;</text>
      <biological_entity constraint="perianth" id="o4976" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s6" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o4977" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4978" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 5;</text>
      <biological_entity id="o4979" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ovary semi-inferior;</text>
      <biological_entity id="o4980" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="semi-inferior" value_original="semi-inferior" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigmas usually 2–3 (–5), connate basally.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting structures achenes, connate with receptacle, often enclosed by swollen perianth.</text>
      <biological_entity id="o4981" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="structures" id="o4982" name="achene" name_original="achenes" src="d0_s10" type="structure" />
      <biological_entity id="o4983" name="receptacle" name_original="receptacle" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o4984" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o4981" id="r674" name="fruiting" negation="false" src="d0_s10" to="o4982" />
      <relation from="o4983" id="r675" modifier="often" name="enclosed by" negation="false" src="d0_s10" to="o4984" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds horizontal, orbicular or reniform;</text>
      <biological_entity id="o4985" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="shape" src="d0_s11" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="reniform" value_original="reniform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>seed-coat dark-brown, smooth;</text>
      <biological_entity id="o4986" name="seed-coat" name_original="seed-coat" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>embryo ± annular, perisperm copious.</text>
      <biological_entity id="o4987" name="embryo" name_original="embryo" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="annular" value_original="annular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>x = 9.</text>
      <biological_entity id="o4988" name="perisperm" name_original="perisperm" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="copious" value_original="copious" />
      </biological_entity>
      <biological_entity constraint="x" id="o4989" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Beet</other_name>
  <other_name type="common_name">chard</other_name>
  <discussion>Species ca. 6 (1 in the flora).</discussion>
  <discussion>Beta is widely distributed and is known especially for the economically important Beta vulgaris subsp. vulgaris, the commonly cultivated beet. The forms of the beets introduced in North America and established in the wild occupy both inland and maritime habitats.</discussion>
  <discussion>The taxonomy of the genus is complicated by a long history of cultivation in which selective breeding has caused a bewildering array of diverse morphologies. In looking at the differences between the two forms of Beta that occasionally become established in waste places in North America, it is tempting to segregate the clearly distinct forms as different species. However, a number of researchers documenting the variation within the complex (B. V. Ford-Lloyd and J. T. Williams 1975; H. Van Kijk and B. Desplanque 1999) agree that the range of variation stretches along a continuum between the two extreme types defined by the maritima and vulgaris groups. The morphological lineage assigned here to subsp. maritima is usually considered ancestral to the cultivated forms of beet included within subsp. vulgaris.</discussion>
  <references>
    <reference>Ford-Lloyd, B. V. and J. T. Williams. 1975. A revision of Beta section Vulgares (Chenopodiaceae), with new light on the origin of cultivated herbs. Bot. J. Linn. Soc. 71: 89–102.</reference>
  </references>
  
</bio:treatment>