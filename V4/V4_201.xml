<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Opuntioideae</taxon_name>
    <taxon_name authority="(Engelmann) F. M. Knuth in C. Backeberg and F. M. Knuth" date="1935" rank="genus">cylindropuntia</taxon_name>
    <taxon_name authority="(Engelmann) F. M. Knuth in C. Backeberg and F. M. Knuth" date="1935" rank="species">fulgida</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">fulgida</taxon_name>
    <taxon_hierarchy>family cactaceae;subfamily opuntioideae;genus cylindropuntia;species fulgida;variety fulgida;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415145</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stem segments appearing spiny from afar, obscuring strongly mammillate tubercles beneath.</text>
      <biological_entity constraint="stem" id="o9133" name="segment" name_original="segments" src="d0_s0" type="structure">
        <character constraint="from afar" is_modifier="false" name="architecture_or_shape" src="d0_s0" value="spiny" value_original="spiny" />
      </biological_entity>
      <biological_entity id="o9134" name="tubercle" name_original="tubercles" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="strongly" name="shape" src="d0_s0" value="mammillate" value_original="mammillate" />
      </biological_entity>
      <relation from="o9133" id="r1295" name="obscuring" negation="false" src="d0_s0" to="o9134" />
    </statement>
    <statement id="d0_s1">
      <text>Spines of stems interlaced with spines of adjacent areoles, longest usually 2.5–3 cm;</text>
      <biological_entity id="o9135" name="spine" name_original="spines" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="length" src="d0_s1" value="longest" value_original="longest" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9136" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o9137" name="spine" name_original="spines" src="d0_s1" type="structure" />
      <biological_entity id="o9138" name="areole" name_original="areoles" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o9135" id="r1296" name="part_of" negation="false" src="d0_s1" to="o9136" />
      <relation from="o9135" id="r1297" name="interlaced with" negation="false" src="d0_s1" to="o9137" />
      <relation from="o9135" id="r1298" name="part_of" negation="false" src="d0_s1" to="o9138" />
    </statement>
    <statement id="d0_s2">
      <text>sheaths baggy.</text>
    </statement>
    <statement id="d0_s3">
      <text>2n = 22, 33.</text>
      <biological_entity id="o9139" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <biological_entity constraint="2n" id="o9140" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="22" value_original="22" />
        <character name="quantity" src="d0_s3" value="33" value_original="33" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall (Apr–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sonoran desert scrub, sandy flats, rocky slopes, rolling hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sonoran desert scrub" />
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="rolling hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sonora); escaped in South Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="escaped in South Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5a.</number>
  
</bio:treatment>