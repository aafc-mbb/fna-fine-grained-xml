<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="treatment_page">162</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Engelmann in F. A.Wislizenus" date="1848" rank="genus">echinocereus</taxon_name>
    <taxon_name authority="Engelmann in F. A. Wislizenus" date="1848" rank="species">enneacanthus</taxon_name>
    <place_of_publication>
      <publication_title>in F. A. Wislizenus, Mem. Tour N. Mexico,</publication_title>
      <place_in_publication>111. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinocereus;species enneacanthus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415237</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants branched forming dense or lax clumps with 20–100 (–500) branches, usually branching before flowering.</text>
      <biological_entity id="o18568" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character constraint="before flowering" is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s0" value="branching" value_original="branching" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18569" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o18570" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s0" to="500" />
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s0" to="100" />
      </biological_entity>
      <relation from="o18568" id="r2684" name="forming" negation="false" src="d0_s0" to="o18569" />
      <relation from="o18568" id="r2685" name="with" negation="false" src="d0_s0" to="o18570" />
    </statement>
    <statement id="d0_s1">
      <text>Stems somewhat lax often sprawling, longest stems sometimes prostrate, cylindric, 8–40 (–100?) × 3.2–15 cm;</text>
      <biological_entity id="o18571" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s1" value="sprawling" value_original="sprawling" />
      </biological_entity>
      <biological_entity constraint="longest" id="o18572" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="3.2" from_unit="cm" name="width" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>ribs (6–) 7–10 (–12), crests essentially uninterrupted;</text>
      <biological_entity id="o18573" name="rib" name_original="ribs" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s2" to="7" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="12" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s2" to="10" />
      </biological_entity>
      <biological_entity id="o18574" name="crest" name_original="crests" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="essentially" name="architecture" src="d0_s2" value="uninterrupted" value_original="uninterrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>areoles (11–) 14–52 mm apart.</text>
      <biological_entity id="o18575" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="14" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s3" to="52" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spines 6–14 per areole, straight or central spines slightly curved throughout their lengths, ± opaque, white, pale tan, or purplish gray, often extensively tipped or banded with brown;</text>
      <biological_entity id="o18576" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per areole" constraintid="o18577" from="6" name="quantity" src="d0_s4" to="14" />
      </biological_entity>
      <biological_entity id="o18577" name="areole" name_original="areole" src="d0_s4" type="structure" />
      <biological_entity constraint="central" id="o18578" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="true" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character constraint="throughout their lengths" is_modifier="false" modifier="slightly" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s4" value="opaque" value_original="opaque" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale tan" value_original="pale tan" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purplish gray" value_original="purplish gray" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale tan" value_original="pale tan" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purplish gray" value_original="purplish gray" />
        <character is_modifier="false" modifier="often extensively" name="architecture" src="d0_s4" value="tipped" value_original="tipped" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="banded with brown" value_original="banded with brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>radial spines 5–10 (–13) per areole, 9.5–40 (–47) mm, usually less than 1/2 as long as central spines;</text>
      <biological_entity id="o18579" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="radial" value_original="radial" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="13" />
        <character char_type="range_value" constraint="per areole" constraintid="o18580" from="5" name="quantity" src="d0_s5" to="10" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s5" to="47" to_unit="mm" />
        <character char_type="range_value" from="9.5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as central spines" constraintid="o18581" from="0" modifier="usually" name="quantity" src="d0_s5" to="1/2" />
      </biological_entity>
      <biological_entity id="o18580" name="areole" name_original="areole" src="d0_s5" type="structure" />
      <biological_entity constraint="central" id="o18581" name="spine" name_original="spines" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>central spines 1–4 (–5) per areole, all or mostly projecting, abaxial spine porrect or descending, frequently compressed or angular in cross-section (sometimes sulcate, keeled, or striate), (12–) 20–84 (–96) mm.</text>
      <biological_entity constraint="central" id="o18582" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" />
        <character char_type="range_value" constraint="per areole" constraintid="o18583" from="1" name="quantity" src="d0_s6" to="4" />
        <character is_modifier="false" modifier="mostly" name="orientation" notes="" src="d0_s6" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o18583" name="areole" name_original="areole" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial" id="o18584" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="porrect" value_original="porrect" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="descending" value_original="descending" />
        <character is_modifier="false" modifier="frequently" name="shape" src="d0_s6" value="compressed" value_original="compressed" />
        <character constraint="in cross-section" constraintid="o18585" is_modifier="false" name="shape" src="d0_s6" value="angular" value_original="angular" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s6" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="84" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s6" to="96" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="84" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18585" name="cross-section" name_original="cross-section" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers (4.5–) 5–7.5 × 5–5.6 (–9) cm;</text>
      <biological_entity id="o18586" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="atypical_length" src="d0_s7" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="5.6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s7" to="9" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s7" to="5.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>flower tube 10–30 × 10–22 (–40) mm;</text>
      <biological_entity constraint="flower" id="o18587" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="30" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s8" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>flower tube hairs 1–2 mm;</text>
      <biological_entity constraint="tube" id="o18588" name="hair" name_original="hairs" src="d0_s9" type="structure" constraint_original="flower tube">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner tepals pink or magenta, darkest proximally, 28–55 × 8–14 (–20) mm, tips relatively thin and delicate;</text>
      <biological_entity constraint="inner" id="o18589" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="magenta" value_original="magenta" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s10" value="darkest" value_original="darkest" />
        <character char_type="range_value" from="28" from_unit="mm" name="length" src="d0_s10" to="55" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18590" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="delicate" value_original="delicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o18591" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectar chamber 4–6 mm.</text>
      <biological_entity constraint="nectar" id="o18592" name="chamber" name_original="chamber" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits pale yellow-green or dull reddish, 20–30 mm, pulp white or pale-pink.</text>
      <biological_entity id="o18593" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale yellow-green" value_original="pale yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 22.</text>
      <biological_entity id="o18594" name="pulp" name_original="pulp" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale-pink" value_original="pale-pink" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18595" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Strawberry cactus</other_name>
  <other_name type="common_name">pitaya</other_name>
  <other_name type="common_name">alicoche</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The commonly recognized concept of Echinocereus enneacanthus var. enneacanthus (W. O. Moore 1967; D. Weniger 1970; L. D. Benson 1982) pertained to the small eastern var. brevispinus.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems (5-)8-14(-15) cm diam.; radial spines 5-8(-9) per areole; central spines 56-84(-96) mm</description>
      <determination>1a Echinocereus enneacanthus var. enneacant</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems slender 3.2-4.5(-7.5) cm diam.; radial spines 8-10(-13) per areole; central spines (12-)20-44(-50) mm</description>
      <determination>1b Echinocereus enneacanthus var. brevispin</determination>
    </key_statement>
  </key>
</bio:treatment>