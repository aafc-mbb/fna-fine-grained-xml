<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">190</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cactaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Cactoideae</taxon_name>
    <taxon_name authority="Link &amp; Otto" date="1827" rank="genus">echinocactus</taxon_name>
    <taxon_name authority="Engelmann &amp; J. M. Bigelow" date="1856" rank="species">polycephalus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">polycephalus</taxon_name>
    <taxon_hierarchy>family cactaceae;subfamily cactoideae;genus echinocactus;species polycephalus;variety polycephalus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242415271</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems gray-green.</text>
      <biological_entity id="o20407" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray-green" value_original="gray-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Spines red to gray, infrequently straw colored, canescent.</text>
      <biological_entity id="o20408" name="spine" name_original="spines" src="d0_s1" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s1" to="gray" />
        <character is_modifier="false" modifier="infrequently" name="coloration" src="d0_s1" value="straw colored" value_original="straw colored" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Fruit scales reddish to maroon, aging tan or black, 10–14 mm, usually shorter than dried tepals at fruit apex, spine tips canescent with strap-shaped, unicellular trichomes.</text>
      <biological_entity constraint="fruit" id="o20409" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s2" to="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="black" value_original="black" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="14" to_unit="mm" />
        <character constraint="than dried tepals" constraintid="o20410" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o20410" name="tepal" name_original="tepals" src="d0_s2" type="structure">
        <character is_modifier="true" name="condition" src="d0_s2" value="dried" value_original="dried" />
      </biological_entity>
      <biological_entity constraint="fruit" id="o20411" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity constraint="spine" id="o20412" name="tip" name_original="tips" src="d0_s2" type="structure">
        <character constraint="with trichomes" constraintid="o20413" is_modifier="false" name="pubescence" src="d0_s2" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity id="o20413" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="strap--shaped" value_original="strap--shaped" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <relation from="o20410" id="r2974" name="at" negation="false" src="d0_s2" to="o20411" />
    </statement>
    <statement id="d0_s3">
      <text>Seeds rounded or faceted with flat planes transversing testa, 2.8–4.7 mm, papillate-roughened (exposed surfaces of testa cells protruding, hemispheric to hexagonally faceted, appearing dull except for microscopically sparkling individual facets).</text>
      <biological_entity id="o20415" name="plane" name_original="planes" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o20416" name="testa" name_original="testa" src="d0_s3" type="structure" />
      <relation from="o20415" id="r2975" name="transversing" negation="false" src="d0_s3" to="o20416" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 22.</text>
      <biological_entity id="o20414" name="seed" name_original="seeds" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character constraint="with planes" constraintid="o20415" is_modifier="false" name="shape" src="d0_s3" value="faceted" value_original="faceted" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s3" to="4.7" to_unit="mm" />
        <character is_modifier="false" name="relief_or_texture" src="d0_s3" value="papillate-roughened" value_original="papillate-roughened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20417" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky flats and washes, bajadas, rock ledges, Mojave and Sonoran desert scrub, igneous and calcareous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky flats" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="bajadas" />
        <character name="habitat" value="rock ledges" />
        <character name="habitat" value="mojave" />
        <character name="habitat" value="sonoran desert scrub" />
        <character name="habitat" value="igneous" />
        <character name="habitat" value="calcareous substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>[10-]30-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="30" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="1700" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.</number>
  
</bio:treatment>