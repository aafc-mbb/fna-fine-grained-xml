<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/08 17:13:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">490</other_info_on_meta>
    <other_info_on_meta type="treatment_page">492</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Adanson" date="unknown" rank="family">portulacaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1814" rank="genus">phemeranthus</taxon_name>
    <taxon_name authority="(S. Ware) Kiger" date="2001" rank="species">calcaricus</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>11: 320. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family portulacaceae;genus phemeranthus;species calcaricus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242415793</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Talinum</taxon_name>
    <taxon_name authority="S. Ware" date="unknown" rank="species">calcaricum</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>69: 466. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Talinum;species calcaricum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 2.5 dm;</text>
      <biological_entity id="o9509" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots tuberous, fleshy.</text>
      <biological_entity id="o9510" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, sometimes branching, ± tufted.</text>
      <biological_entity id="o9511" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_pubescence" src="d0_s2" value="tufted" value_original="tufted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile;</text>
      <biological_entity id="o9512" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade terete, to 5 cm.</text>
      <biological_entity id="o9513" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymose, overtopping leaves;</text>
      <biological_entity id="o9514" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymose" value_original="cymose" />
      </biological_entity>
      <biological_entity id="o9515" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o9514" id="r1354" name="overtopping" negation="false" src="d0_s5" to="o9515" />
    </statement>
    <statement id="d0_s6">
      <text>peduncle scapelike, to 15 cm.</text>
      <biological_entity id="o9516" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals persistent, ovate, 3–4 mm;</text>
      <biological_entity id="o9517" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o9518" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals rose-purple, elliptic to obovate, 8–10 mm;</text>
      <biological_entity id="o9519" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9520" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="rose-purple" value_original="rose-purple" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 25–45;</text>
      <biological_entity id="o9521" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9522" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s9" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma 1, distinctly 3-lobed.</text>
      <biological_entity id="o9523" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9524" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s10" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ovoid to obovoid, 4–6 mm.</text>
      <biological_entity id="o9525" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s11" to="obovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds without arcuate ridges, 1.2 mm. 2n = 48.</text>
      <biological_entity id="o9526" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character name="some_measurement" notes="" src="d0_s12" unit="mm" value="1.2" value_original="1.2" />
      </biological_entity>
      <biological_entity id="o9527" name="ridge" name_original="ridges" src="d0_s12" type="structure">
        <character is_modifier="true" name="course_or_shape" src="d0_s12" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9528" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="48" value_original="48" />
      </biological_entity>
      <relation from="o9526" id="r1355" name="without" negation="false" src="d0_s12" to="o9527" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cedar glades in shallow soil on limestone outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cedar" />
        <character name="habitat" value="shallow soil" modifier="glades in" constraint="on limestone outcrops" />
        <character name="habitat" value="limestone outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ky., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>A recent study strongly suggests that Phemeranthus calcaricus is a derivative of autotetraploid P. calycinus (W. H. Murdy and M. E. B. Carter 2001). Congruent with that hypothesis, one collection from a glade in Izard County, Arkansas (B. L. Lipscomb 1577, NCU), which is within the range of P. calycinus, appears to belong to P. calcaricus, which is known otherwise only from well east of the Mississippi River and outside the range of P. calycinus.</discussion>
  <references>
    <reference>Baskin, J. M. and C. C. Baskin. 1989. Cedar glade endemics in Tennessee, and a review of their autecology. J. Tennessee Acad. Sci. 64(3): 63–74.</reference>
    <reference>Krebs, S. M. 1971. A Systematic Study of Talinum calcaricum Ware. M.S. thesis. Emory University.</reference>
  </references>
  
</bio:treatment>