<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Endlicher" date="1830" rank="tribe">Epilobieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EPILOBIUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Epilobium</taxon_name>
    <taxon_name authority="Reichenbach" date="1824" rank="species">hornemannii</taxon_name>
    <place_of_publication>
      <publication_title>Iconogr. Bot. Pl. Crit.</publication_title>
      <place_in_publication>2: 73.  1824</place_in_publication>
      <other_info_on_pub>(as hornemanni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section epilobium;species hornemannii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epilobium</taxon_name>
    <taxon_name authority="Hornemann in G. C. Oeder et al." date="1387" rank="species">nutans</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Dan.</publication_title>
      <place_in_publication>8: plate 1387.  1810</place_in_publication>
      <other_info_on_pub>not F. W. Schmidt 1794</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus epilobium;species nutans</taxon_hierarchy>
  </taxon_identification>
  <number>30.</number>
  <other_name type="common_name">Hornemann’s willowherb</other_name>
  <other_name type="common_name">épilobe de Hornemann</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with short, scaly hypo­geous or leafy epigeous soboles.</text>
      <biological_entity id="o5042" name="herb" name_original="herbs" src="d0_s0" type="structure" />
      <biological_entity id="o5043" name="sobole" name_original="soboles" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" />
        <character is_modifier="true" name="architecture_or_pubescence" src="d0_s0" value="scaly" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="leafy" />
        <character is_modifier="true" name="location" src="d0_s0" value="epigeous" />
      </biological_entity>
      <relation from="o5042" id="r909" name="with" negation="false" src="d0_s0" to="o5043" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, clumped, terete, 10–45 cm, usually simple, rarely branched proximally, subglabrous proximal to inflorescence with sparsely strigillose lines decurrent from margins of petioles, ± sparsely mixed strigillose and glandular puberulent distally.</text>
      <biological_entity id="o5044" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="ascending" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="erect" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="clumped" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" />
        <character is_modifier="false" modifier="rarely; proximally" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="subglabrous" />
        <character constraint="to inflorescence" constraintid="o5045" is_modifier="false" name="position" src="d0_s1" value="proximal" />
        <character is_modifier="false" modifier="more or less sparsely" name="arrangement" notes="" src="d0_s1" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="puberulent" />
      </biological_entity>
      <biological_entity id="o5045" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
      <biological_entity id="o5046" name="line" name_original="lines" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s1" value="strigillose" />
        <character constraint="from margins" constraintid="o5047" is_modifier="false" name="shape" src="d0_s1" value="decurrent" />
      </biological_entity>
      <biological_entity id="o5047" name="margin" name_original="margins" src="d0_s1" type="structure" />
      <biological_entity id="o5048" name="petiole" name_original="petioles" src="d0_s1" type="structure" />
      <relation from="o5045" id="r910" name="with" negation="false" src="d0_s1" to="o5046" />
      <relation from="o5047" id="r911" name="part_of" negation="false" src="d0_s1" to="o5048" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite proximal to inflo­rescence, usually alternate distally, petioles 3–9 mm proximally to subsessile distally;</text>
      <biological_entity id="o5049" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character constraint="to inflo" constraintid="o5050" is_modifier="false" name="position" src="d0_s2" value="proximal" />
        <character is_modifier="false" modifier="usually; distally" name="arrangement" notes="" src="d0_s2" value="alternate" />
      </biological_entity>
      <biological_entity id="o5050" name="inflo" name_original="inflo" src="d0_s2" type="structure" />
      <biological_entity id="o5051" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" modifier="proximally" name="some_measurement" src="d0_s2" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly elliptic to spatulate proximally, ovate to lanceolate distally, ± coriaceous or not, 1.5–6.2 × 0.7–2.9 cm, base attenuate to cuneate or rounded, margins subentire proximally, denticulate distally with 10–25 teeth per side, veins often inconspicuous, 4–7 per side, apex obtuse to subacute, surfaces glabrous or, sometimes, strigillose along margins;</text>
      <biological_entity id="o5052" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="spatulate" />
        <character is_modifier="false" modifier="proximally" name="shape" notes="[duplicate value]" src="d0_s3" value="," />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character char_type="range_value" from="broadly elliptic" modifier="distally" name="shape" src="d0_s3" to="spatulate proximally" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="coriaceous" />
        <character name="texture" src="d0_s3" value="not" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="6.2" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s3" to="2.9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5053" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="attenuate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="cuneate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="rounded" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s3" to="cuneate or rounded" />
      </biological_entity>
      <biological_entity id="o5054" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s3" value="subentire" />
        <character constraint="with teeth" constraintid="o5055" is_modifier="false" name="shape" src="d0_s3" value="denticulate" />
      </biological_entity>
      <biological_entity id="o5055" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s3" to="25" />
      </biological_entity>
      <biological_entity id="o5056" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o5057" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s3" value="inconspicuous" />
        <character char_type="range_value" constraint="per side" constraintid="o5058" from="4" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity id="o5058" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o5059" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="obtuse" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="subacute" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="subacute" />
      </biological_entity>
      <biological_entity id="o5060" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" />
        <character name="pubescence" src="d0_s3" value="," />
        <character constraint="along margins" constraintid="o5061" is_modifier="false" name="pubescence" src="d0_s3" value="strigillose" />
      </biological_entity>
      <biological_entity id="o5061" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <relation from="o5055" id="r912" name="per" negation="false" src="d0_s3" to="o5056" />
    </statement>
    <statement id="d0_s4">
      <text>bracts reduced.</text>
      <biological_entity id="o5062" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences erect or nodding, open racemes, mixed strigillose and glandular puberulent.</text>
      <biological_entity id="o5063" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" />
      </biological_entity>
      <biological_entity id="o5064" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="open" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o5065" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>buds 2–5.5 × 2–4 mm;</text>
      <biological_entity id="o5066" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 2–5 mm;</text>
      <biological_entity id="o5067" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>floral-tube 1–2.2 × 1.3–2.8 mm, sparse ring of hairs at mouth inside or ring absent;</text>
      <biological_entity id="o5068" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s9" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5069" name="ring" name_original="ring" src="d0_s9" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s9" value="sparse" />
      </biological_entity>
      <biological_entity id="o5070" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o5071" name="mouth" name_original="mouth" src="d0_s9" type="structure" />
      <biological_entity id="o5072" name="ring" name_original="ring" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" />
      </biological_entity>
      <relation from="o5069" id="r913" name="part_of" negation="false" src="d0_s9" to="o5070" />
      <relation from="o5069" id="r914" name="at" negation="false" src="d0_s9" to="o5071" />
    </statement>
    <statement id="d0_s10">
      <text>sepals sometimes red-tipped or bright red, 2–7 × 1–2.2 mm, abaxial surface sparsely strigillose and glandular puberulent;</text>
      <biological_entity id="o5073" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="red-tipped" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="bright red" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5074" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals usually rose-purple or magenta to light pink, rarely white, 3–10 (–11) × 2–6 mm, apical notch 0.7–2.4 mm;</text>
      <biological_entity id="o5075" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" notes="[duplicate value]" src="d0_s11" value="rose-purple" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="magenta" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="light pink" />
        <character char_type="range_value" from="magenta" name="coloration" src="d0_s11" to="light pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="white" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="11" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5076" name="notch" name_original="notch" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments cream to light pink, those of longer stamens 1.4–5 (–6) mm, those of shorter ones 1.2–4 mm;</text>
      <biological_entity id="o5077" name="filament" name_original="filaments" src="d0_s12" type="structure" constraint="stamen">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s12" value="cream" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s12" value="light pink" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s12" to="light pink" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o5078" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity constraint="shorter" id="o5079" name="one" name_original="ones" src="d0_s12" type="structure" />
      <relation from="o5077" id="r915" name="part_of" negation="false" src="d0_s12" to="o5078" />
      <relation from="o5077" id="r916" name="part_of" negation="false" src="d0_s12" to="o5079" />
    </statement>
    <statement id="d0_s13">
      <text>anthers light yellow, 0.4–1.2 × 0.3–0.6 mm;</text>
      <biological_entity id="o5080" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="light yellow" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s13" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 15–25 mm, glandular puberulent, sometimes mixed strigillose;</text>
      <biological_entity id="o5081" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s14" to="25" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s14" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s14" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style white or cream, 2–8 mm, stigma cream, clavate or cylindrical, entire, 1.2–3 × 0.5–1 mm, usually surrounded by, rarely exserted beyond, anthers.</text>
      <biological_entity id="o5082" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="cream" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5083" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="cream" />
        <character is_modifier="false" name="shape" src="d0_s15" value="clavate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindrical" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5084" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="rarely" name="position" src="d0_s15" value="exserted" />
      </biological_entity>
      <relation from="o5083" id="r917" modifier="usually" name="surrounded by" negation="false" src="d0_s15" to="o5084" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules 35–65 mm, surfaces glandular puberulent, sometimes mixed strigillose;</text>
      <biological_entity id="o5085" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s16" to="65" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5086" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s16" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s16" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 5–15 (–25) mm.</text>
      <biological_entity id="o5087" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds narrowly fusiform or oblanceoloid, 0.9–1.6 × 0.3–0.5 mm, chalazal collar short, 0.05–0.1 mm, blond to brown, surface distinctly papillose or reticulate/smooth;</text>
      <biological_entity id="o5088" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s18" value="oblanceoloid" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s18" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s18" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="chalazal" id="o5089" name="collar" name_original="collar" src="d0_s18" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="short" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s18" to="0.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" />
      </biological_entity>
      <biological_entity id="o5090" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="distinctly" name="relief" src="d0_s18" value="papillose" />
        <character is_modifier="false" name="relief" src="d0_s18" value="smooth" value_original="reticulate/smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>coma readily detached, dingy white, 6–11 mm.</text>
      <biological_entity id="o5091" name="coma" name_original="coma" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="readily" name="fusion" src="d0_s19" value="detached" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="dingy white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s19" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Epilobium hornemannii occurs widely in montane and boreal regions in North America and western Eurasia, and also in Japan and the Russian Far East.  It is characterized by having the CC chromosome arrangement and is included in the Alpinae alliance with E. anagallidifolium, E. lactiflorum, and others (I. Kytövuori 1972).</discussion>
  <discussion>W. Trelease (1891) discussed eastern and western forms of Epilobium hornemannii, the latter divided into two variations; however, he did not formally recognize any of these variants.</discussion>
  <discussion>P. A. Munz (1965) included the Eurasian Epilobium alsinifolium Villars in his North American treatment, noting that it occurred in Greenland.  However, B. Fredskild (1984) suggested that, for the most part, these determinations represent misidentifications of E. hornemannii.</discussion>
  <discussion>The two subspecies recognized here intergrade throughout much of their shared range, but whereas subsp. hornemannii is commonly found in high mon­tane to alpine regions, in the northern part of its range it grows at much lower elevations, and in maritime areasis replaced by coriaceous-leaved forms here designated as subsp. behringianum.  The situation is rather analo­gous to the pattern seen in E. ciliatum in which subsp. ciliatum is wide-ranging and variable, but replaced in Pacific maritime areas by subsp. watsonii, from which it differs consistently in most specimens, but some­times intergrades.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves not coriaceous, petioles 3–7 mm proximally; sepals 2–4.5 mm; petals 3–9 mm; capsules 40–65 mm; seeds 0.9–1.2 mm, surface papillose.</description>
      <determination>30a. Epilobium hornemannii subsp. hornemannii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves ± coriaceous, petioles 4–9 mm proximally; sepals 5–7 mm; petals 8–10(–11) mm; capsules 35–55 mm; seeds 0.9–1.6 mm, surface reticulate.</description>
      <determination>30b. Epilobium hornemannii subsp. behringianum</determination>
    </key_statement>
  </key>
</bio:treatment>