<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) Walpers" date="1843" rank="section">Kneiffia</taxon_name>
    <taxon_name authority="Rafinesque" date="1820" rank="species">pilosella</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Nat.</publication_title>
      <place_in_publication>1: 15.  1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section kneiffia;species pilosella</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kneiffia</taxon_name>
    <taxon_name authority="(Linnaeus) Spach ex Raimann" date="unknown" rank="species">fruticosa</taxon_name>
    <taxon_name authority="(Rafinesque) Britton" date="unknown" rank="variety">pilosella</taxon_name>
    <taxon_hierarchy>genus kneiffia;species fruticosa;variety pilosella</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">K.</taxon_name>
    <taxon_name authority="(Rafinesque) A. Heller" date="unknown" rank="species">pilosella</taxon_name>
    <taxon_hierarchy>genus k.;species pilosella</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">K.</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">pratensis</taxon_name>
    <taxon_hierarchy>genus k.;species pratensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">K.</taxon_name>
    <taxon_name authority="Jennings" date="unknown" rank="species">sumstinei</taxon_name>
    <taxon_hierarchy>genus k.;species sumstinei</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">fruticosa</taxon_name>
    <taxon_name authority="Nuttall ex Torrey &amp; A. Gray" date="unknown" rank="variety">hirsuta</taxon_name>
    <taxon_hierarchy>genus oenothera;species fruticosa;variety hirsuta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fruticosa</taxon_name>
    <taxon_name authority="(Rafinesque) Small &amp; A. Heller" date="unknown" rank="variety">pilosella</taxon_name>
    <taxon_hierarchy>genus o.;species fruticosa;variety pilosella</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="(Small) B. L. Robinson" date="unknown" rank="species">pratensis</taxon_name>
    <taxon_hierarchy>genus o.;species pratensis</taxon_hierarchy>
  </taxon_identification>
  <number>32.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, usually densely to sparsely hirsute, rarely glabrous;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a thickened base, rhizomatous.</text>
      <biological_entity id="o5190" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" modifier="usually densely; densely to sparsely" name="pubescence" src="d0_s0" value="hirsute" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems spreading or ascending, unbranched or few-branched distally, 20–80 cm.</text>
      <biological_entity id="o5191" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="few-branched" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, basal 4–8 × 2–5 cm, petiole (0.5–) 1–3 (–4) cm, blade oblanceolate to ovate, margins entire;</text>
      <biological_entity id="o5192" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5193" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o5194" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5195" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5196" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="ovate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="ovate" />
      </biological_entity>
      <relation from="o5192" id="r1017" name="in" negation="false" src="d0_s3" to="o5193" />
    </statement>
    <statement id="d0_s4">
      <text>cauline 3–10 (–13) × 1–2 (–4) cm, petiole 0–0.5 (–2) cm, blade lanceolate to ovate, abruptly narrowed to base, margins subentire or coarsely dentate.</text>
      <biological_entity id="o5197" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" />
      </biological_entity>
      <biological_entity id="o5198" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5199" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character constraint="to base" constraintid="o5200" is_modifier="false" modifier="abruptly" name="shape" src="d0_s4" value="narrowed" />
      </biological_entity>
      <biological_entity id="o5200" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o5201" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences erect, flowers in axils of distalmost few nodes.</text>
      <biological_entity id="o5202" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" />
      </biological_entity>
      <biological_entity id="o5203" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o5204" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity constraint="distalmost" id="o5205" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" />
      </biological_entity>
      <relation from="o5203" id="r1018" name="in" negation="false" src="d0_s5" to="o5204" />
      <relation from="o5204" id="r1019" name="part_of" negation="false" src="d0_s5" to="o5205" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers opening near sunrise;</text>
      <biological_entity id="o5206" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o5207" name="sunrise" name_original="sunrise" src="d0_s6" type="structure" />
      <relation from="o5206" id="r1020" name="opening near" negation="false" src="d0_s6" to="o5207" />
    </statement>
    <statement id="d0_s7">
      <text>buds with free tips 1–3 mm, spreading;</text>
      <biological_entity id="o5208" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="spreading" />
      </biological_entity>
      <biological_entity id="o5209" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o5208" id="r1021" name="with" negation="false" src="d0_s7" to="o5209" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube 10–25 mm;</text>
      <biological_entity id="o5210" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 10–20 mm;</text>
      <biological_entity id="o5211" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals bright-yellow, fading pale-pink or pale-yellow, 15–30 mm;</text>
      <biological_entity id="o5212" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="bright-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-yellow" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 7–15 mm, anthers 4–8 mm, pollen 85–100% fertile;</text>
      <biological_entity id="o5213" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5214" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5215" name="pollen" name_original="pollen" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="85-100%" name="reproduction" src="d0_s11" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 10–20 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o5216" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5217" name="stigma" name_original="stigma" src="d0_s12" type="structure" />
      <biological_entity id="o5218" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o5217" id="r1022" name="exserted beyond" negation="false" src="d0_s12" to="o5218" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules usually oblong-clavate to oblong-ellipsoid or ellipsoid, 4-angled or weakly 4-winged, (5–) 10–15 (–28) × 2–4 (–5) mm, stipe (1–) 3–5 (–9) mm;</text>
      <biological_entity id="o5219" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" notes="[duplicate value]" src="d0_s13" value="oblong-clavate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="oblong-ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ellipsoid" />
        <character char_type="range_value" from="usually oblong-clavate" name="shape" src="d0_s13" to="oblong-ellipsoid or ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="4-angled" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s13" value="4-winged" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s13" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="28" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s13" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sessile.</text>
      <biological_entity id="o5220" name="stipe" name_original="stipe" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1 × 0.5 mm. 2n = 56.</text>
      <biological_entity id="o5221" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="length" src="d0_s15" unit="mm" value="1" />
        <character name="width" src="d0_s15" unit="mm" value="0.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5222" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera pilosella is widespread in cultivation in gardens and frequently escapes and becomes naturalized; the northern and eastern natural limits of O. pilosella are not clear.  According to G. B. Straley (1977) the natural limits are from Wayne County, West Virginia, along the Ohio River and Erie County, New York, for the eastern limits, and Tuscola County, Michigan, and Manitowoc County, Wisconsin, for the northern limits.  K. N. Krakos (2014), based on new field studies and phylogenetic data, found that O. pilosella does not form a monophyletic group with plants previously treated by Straley as O. pilosella subsp. sessilis in molecular analyses, and thus is here reinstated as the distinct species O. sessilis.  Straley determined that O. pilosella is self-incompatible and an octoploid, one of the few in the genus.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open fields, edge of woods, marshes and bottomland prairies, open dis­turbed sites, ditches, old fields, railroads, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open fields" />
        <character name="habitat" value="edge" constraint="of woods , marshes and bottomland prairies" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="bottomland prairies" />
        <character name="habitat" value="sites" modifier="open disturbed" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Ill., Ind., Iowa, Ky., La., Maine, Mass., Mich., Miss., Mo., N.H., N.Y., Ohio, Okla., Pa., R.I., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>