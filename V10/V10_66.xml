<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Pennell) Munz" date="1937" rank="section">Peniophyllum</taxon_name>
    <taxon_name authority="Nuttall" date="1821" rank="species">linifolia</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>2: 120.  1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section peniophyllum;species linifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kneiffia</taxon_name>
    <taxon_name authority="(Nuttall) Spach" date="unknown" rank="species">linifolia</taxon_name>
    <taxon_hierarchy>genus kneiffia;species linifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">linifolia</taxon_name>
    <taxon_name authority="Munz" date="unknown" rank="variety">glandulosa</taxon_name>
    <taxon_hierarchy>genus oenothera;species linifolia;variety glandulosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Peniophyllum</taxon_name>
    <taxon_name authority="(Nuttall) Pennell" date="unknown" rank="species">linifolium</taxon_name>
    <taxon_hierarchy>genus peniophyllum;species linifolium</taxon_hierarchy>
  </taxon_identification>
  <number>27.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, caulescent, strig­illose or glabrous, also often glandular puberulent, especially distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a sparsely branched taproot.</text>
      <biological_entity id="o4916" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" modifier="often" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems un­branched or with many ascending branches arising near base, erect, 10–50 cm.</text>
      <biological_entity id="o4917" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="with many ascending branches" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4918" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="many" />
        <character is_modifier="true" name="orientation" src="d0_s2" value="ascending" />
        <character constraint="near base" constraintid="o4919" is_modifier="false" name="orientation" src="d0_s2" value="arising" />
      </biological_entity>
      <biological_entity id="o4919" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o4917" id="r964" name="with" negation="false" src="d0_s2" to="o4918" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, basal 1–2 (–4) × 0.2–0.6 cm, petiole 0.2–1 (–1.5) cm, blade ovate to obovate or narrowly elliptic;</text>
      <biological_entity id="o4920" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4921" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o4922" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4923" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
      <relation from="o4920" id="r965" name="in" negation="false" src="d0_s3" to="o4921" />
    </statement>
    <statement id="d0_s4">
      <text>cauline 1–4 × less than 0.1 cm, sessile, blade linear or filiform.</text>
      <biological_entity id="o4924" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="obovate or narrowly elliptic" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" />
      </biological_entity>
      <biological_entity id="o4925" name="whole_organism" name_original="" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
      </biological_entity>
      <biological_entity id="o4926" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers usually 1–3 opening per day near sunrise;</text>
      <biological_entity id="o4927" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o4928" name="day" name_original="day" src="d0_s5" type="structure" />
      <biological_entity id="o4929" name="sunrise" name_original="sunrise" src="d0_s5" type="structure" />
      <relation from="o4927" id="r966" name="opening per" negation="false" src="d0_s5" to="o4928" />
      <relation from="o4927" id="r967" name="near" negation="false" src="d0_s5" to="o4929" />
    </statement>
    <statement id="d0_s6">
      <text>buds without free tips;</text>
      <biological_entity id="o4930" name="bud" name_original="buds" src="d0_s6" type="structure" />
      <biological_entity id="o4931" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="free" />
      </biological_entity>
      <relation from="o4930" id="r968" name="without" negation="false" src="d0_s6" to="o4931" />
    </statement>
    <statement id="d0_s7">
      <text>sepals 1.5–2 mm;</text>
      <biological_entity id="o4932" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals bright-yellow, fading pink, obcordate to obovate, 3–5 (–7) mm;</text>
      <biological_entity id="o4933" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="fading pink" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="obcordate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="obovate" />
        <character char_type="range_value" from="obcordate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 1–2 mm, anthers 0.5–1 mm;</text>
      <biological_entity id="o4934" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4935" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 1–2 mm, stigma surrounded by anthers at anthesis.</text>
      <biological_entity id="o4936" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4937" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o4938" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o4937" id="r969" name="surrounded by" negation="false" src="d0_s10" to="o4938" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules ellipsoid-rhombic to subglobose, 4-angled, 4–6 (–10) × 1.5–3 mm, stipe 0–4 mm, valve midrib raised at distal end, indehiscent or tardily dehiscent distally;</text>
      <biological_entity id="o4939" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="ellipsoid-rhombic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="subglobose" />
        <character char_type="range_value" from="ellipsoid-rhombic" name="shape" src="d0_s11" to="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s11" value="4-angled" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4940" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4942" name="end" name_original="end" src="d0_s11" type="structure" />
      <relation from="o4941" id="r970" name="raised at" negation="false" src="d0_s11" to="o4942" />
    </statement>
    <statement id="d0_s12">
      <text>sessile.</text>
      <biological_entity constraint="valve" id="o4941" name="midrib" name_original="midrib" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="indehiscent" />
        <character is_modifier="false" modifier="tardily; distally" name="dehiscence" src="d0_s11" value="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds clustered in each locule, ovoid, surface minutely papillose, 1 × 0.5 mm. 2n = 14.</text>
      <biological_entity id="o4943" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character constraint="in locule" constraintid="o4944" is_modifier="false" name="arrangement_or_growth_form" src="d0_s13" value="clustered" />
        <character is_modifier="false" name="shape" notes="" src="d0_s13" value="ovoid" />
      </biological_entity>
      <biological_entity id="o4944" name="locule" name_original="locule" src="d0_s13" type="structure" />
      <biological_entity id="o4945" name="surface" name_original="surface" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s13" value="papillose" />
        <character name="length" src="d0_s13" unit="mm" value="1" />
        <character name="width" src="d0_s13" unit="mm" value="0.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4946" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Kneiffia linearifolia Spach (1835) is an illegitimate name based on Oenothera linifolia.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, open woodlands, open rocky and sandy sites, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="open rocky" />
        <character name="habitat" value="sandy sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ill., Kans., Ky., La., Miss., Mo., N.C., Okla., S.C., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>