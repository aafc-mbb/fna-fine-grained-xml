<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POLYGALA</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">hemipterocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 31.  1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species hemipterocarpa</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Winged milkwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, single or multistemmed, 1.2–4 (–5.6) [–7.5] dm, unbranched or branched distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>from thickened caudex.</text>
      <biological_entity id="o1782" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" />
        <character is_modifier="false" name="architecture_or_growth_form" src="d0_s0" value="multistemmed" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5.6" to_unit="dm" />
        <character char_type="range_value" from="1.2" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s0" value="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sometimes slightly glaucous, usually glabrous, rarely with few scattered, minute hairs, those appressed to incurved.</text>
      <biological_entity id="o1783" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" modifier="sometimes slightly" name="pubescence" src="d0_s2" value="glaucous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="appressed" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="incurved" />
        <character char_type="range_value" from="appressed" name="orientation" notes="" src="d0_s2" to="incurved" />
      </biological_entity>
      <biological_entity id="o1784" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" />
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" />
        <character is_modifier="true" name="size" src="d0_s2" value="minute" />
      </biological_entity>
      <relation from="o1783" id="r224" modifier="rarely" name="with" negation="false" src="d0_s2" to="o1784" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or subsessile;</text>
      <biological_entity id="o1785" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear to linear-lanceolate, (3–) 6–25 × 0.6–1 mm, base obtuse, apex acute to subacuminate, surfaces glabrous.</text>
      <biological_entity id="o1786" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="linear-lanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s5" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1787" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" />
      </biological_entity>
      <biological_entity id="o1788" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="subacuminate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="subacuminate" />
      </biological_entity>
      <biological_entity id="o1789" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes loosely cylindric, 3–15 (–21) × 0.4–0.8 cm;</text>
      <biological_entity id="o1790" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="loosely" name="shape" src="d0_s6" value="cylindric" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="21" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle 0.5–2.5 cm;</text>
      <biological_entity id="o1791" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts deciduous, ovate to lanceolate-ovate.</text>
      <biological_entity id="o1792" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="lanceolate-ovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0.5–1 mm, usually glabrous, rarely puberulent.</text>
      <biological_entity id="o1793" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s9" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers white, greenish veined, 3–4.5 mm;</text>
      <biological_entity id="o1794" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="veined" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals ovate, 1.6–2.3 mm;</text>
      <biological_entity id="o1795" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s11" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>wings obovate or elliptic-ovate, 3–4.3 × 1.6–1.8 mm, apex obtuse (rarely to bluntly rounded);</text>
      <biological_entity id="o1796" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elliptic-ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="4.3" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1797" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keel 3.4 mm, crest 2-parted, with 3 lobes on each side, each 2–4-lobed.</text>
      <biological_entity id="o1798" name="keel" name_original="keel" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="3.4" />
      </biological_entity>
      <biological_entity id="o1799" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="2-parted" />
        <character is_modifier="false" name="shape" notes="" src="d0_s13" value="2-4-lobed" />
      </biological_entity>
      <biological_entity id="o1800" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="3" />
      </biological_entity>
      <biological_entity id="o1801" name="side" name_original="side" src="d0_s13" type="structure" />
      <relation from="o1799" id="r225" name="with" negation="false" src="d0_s13" to="o1800" />
      <relation from="o1800" id="r226" name="on" negation="false" src="d0_s13" to="o1801" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules oblong, 4–6.3 × 2.3 mm, abaxial locule not winged, adaxial locule longer, winged, wing broadly scarious, erose-crenulate.</text>
      <biological_entity id="o1802" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s14" to="6.3" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="2.3" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1803" name="locule" name_original="locule" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1804" name="locule" name_original="locule" src="d0_s14" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s14" value="longer" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
      <biological_entity id="o1805" name="wing" name_original="wing" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="broadly" name="texture" src="d0_s14" value="scarious" />
        <character is_modifier="false" name="shape" src="d0_s14" value="erose-crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2.7 mm, pubescent, coat with rows of pits 0.05 mm wide;</text>
      <biological_entity id="o1806" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="2.7" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" />
      </biological_entity>
      <biological_entity id="o1807" name="coat" name_original="coat" src="d0_s15" type="structure" />
      <biological_entity id="o1808" name="row" name_original="rows" src="d0_s15" type="structure">
        <character name="width" src="d0_s15" unit="mm" value="0.05" />
      </biological_entity>
      <biological_entity id="o1809" name="pit" name_original="pits" src="d0_s15" type="structure" />
      <relation from="o1807" id="r227" name="with" negation="false" src="d0_s15" to="o1808" />
      <relation from="o1808" id="r228" name="part_of" negation="false" src="d0_s15" to="o1809" />
    </statement>
    <statement id="d0_s16">
      <text>arils 2 mm, lobes 1/3–2/3 length of seed, vestigial, or to 1/3 length of seed in wingless abaxial locule.</text>
      <biological_entity id="o1810" name="aril" name_original="arils" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="2" />
      </biological_entity>
      <biological_entity id="o1811" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character char_type="range_value" from="1/3 length of seed" name="length" src="d0_s16" to="2/3 length of seed" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="vestigial" />
        <character char_type="range_value" constraint="in abaxial locule" constraintid="o1812" from="0 length of seed" name="length" src="d0_s16" to="1/3 length of seed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1812" name="locule" name_original="locule" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="wingless" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Polygala hemipterocarpa is known only from the Chihuahuan and eastern Sonoran deserts.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes in grasslands, open woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" constraint="in grasslands , open woodlands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Durango, eastern Sonora, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (eastern Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>