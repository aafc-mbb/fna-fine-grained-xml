<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Gordon C. Tucker</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Meisner" date="unknown" rank="family">GUNNERACEAE</taxon_name>
    <taxon_hierarchy>family gunneraceae</taxon_hierarchy>
  </taxon_identification>
  <number />
  <other_name type="common_name">Chilean Rhubarb Family</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, monoecious or polygamomonoecious [dioecious], terrestrial or amphibious, acaulous, armed, clonal or not.</text>
      <biological_entity id="o1439" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamomonoecious" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="terrestrial" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="amphibious" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="armed" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="clonal" />
        <character name="growth_form" src="d0_s0" value="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fibrous, rhizomes with [without] triangular scales (cataphylls).</text>
      <biological_entity id="o1440" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" />
      </biological_entity>
      <biological_entity id="o1441" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
      <biological_entity id="o1442" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="triangular" />
      </biological_entity>
      <relation from="o1441" id="r296" name="with" negation="false" src="d0_s1" to="o1442" />
    </statement>
    <statement id="d0_s2">
      <text>Stems absent.</text>
      <biological_entity id="o1443" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, alternate, simple;</text>
      <biological_entity id="o1444" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules scalelike or absent;</text>
    </statement>
    <statement id="d0_s5">
      <text>petio­late;</text>
      <biological_entity id="o1445" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="scalelike" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade palmately lobed [toothed], venation prominent, margins irregularly toothed, abaxial surface with stiff, hard prickles.</text>
      <biological_entity id="o1446" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s6" value="lobed" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="prominent" />
      </biological_entity>
      <biological_entity id="o1447" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s6" value="toothed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1448" name="surface" name_original="surface" src="d0_s6" type="structure" />
      <biological_entity id="o1449" name="prickle" name_original="prickles" src="d0_s6" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s6" value="stiff" />
        <character is_modifier="true" name="texture" src="d0_s6" value="hard" />
      </biological_entity>
      <relation from="o1448" id="r297" name="with" negation="false" src="d0_s6" to="o1449" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences ± from rhizome, terminal [from axils of distal leaves], panicles of spikes;</text>
      <biological_entity id="o1450" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" notes="" src="d0_s7" value="terminal" />
      </biological_entity>
      <biological_entity id="o1451" name="rhizome" name_original="rhizome" src="d0_s7" type="structure" />
      <biological_entity id="o1452" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity id="o1453" name="spike" name_original="spikes" src="d0_s7" type="structure" />
      <relation from="o1450" id="r298" name="from" negation="false" src="d0_s7" to="o1451" />
      <relation from="o1452" id="r299" name="part_of" negation="false" src="d0_s7" to="o1453" />
    </statement>
    <statement id="d0_s8">
      <text>bracts absent.</text>
      <biological_entity id="o1454" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers often pistillate proximally, staminate distally, bisexual between, sessile or pedicellate;</text>
      <biological_entity id="o1455" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often; proximally" name="architecture" src="d0_s9" value="pistillate" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s9" value="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth and androecium perigynous;</text>
      <biological_entity id="o1456" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="perigynous" />
      </biological_entity>
      <biological_entity id="o1457" name="androecium" name_original="androecium" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 2 [0];</text>
      <biological_entity id="o1458" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" />
        <character name="atypical_quantity" src="d0_s11" value="0" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 0 [2];</text>
      <biological_entity id="o1459" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" />
        <character name="atypical_quantity" src="d0_s12" value="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 2 [1] epipetalous;</text>
      <biological_entity id="o1460" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" />
        <character name="atypical_quantity" src="d0_s13" value="1" />
        <character is_modifier="false" name="position" src="d0_s13" value="epipetalous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers basifixed, extrorse, pollen-grains 3 (–5) -aperturate, colpate, 2-nucleate;</text>
      <biological_entity id="o1461" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s14" value="basifixed" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s14" value="extrorse" />
      </biological_entity>
      <biological_entity id="o1462" name="pollen-grain" name_original="pollen-grains" src="d0_s14" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="3(-5)-aperturate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="colpate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistil 2-carpellate;</text>
      <biological_entity id="o1463" name="pistil" name_original="pistil" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary inferior, 1-locular;</text>
    </statement>
    <statement id="d0_s17">
      <text>placentation apical;</text>
      <biological_entity id="o1464" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="inferior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="1-locular" />
        <character is_modifier="false" name="placentation" src="d0_s17" value="apical" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles 2;</text>
      <biological_entity id="o1465" name="style" name_original="styles" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 2, papillate;</text>
      <biological_entity id="o1466" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" />
        <character is_modifier="false" name="relief" src="d0_s19" value="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovule 1, anatropous, bitegmic, crassinucellate.</text>
      <biological_entity id="o1467" name="ovule" name_original="ovule" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="1" />
        <character is_modifier="false" name="orientation" src="d0_s20" value="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="bitegmic" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="crassinucellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits drupes.</text>
      <biological_entity constraint="fruits" id="o1468" name="drupe" name_original="drupes" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>Seeds with oily endosperm, embryo straight.</text>
      <biological_entity id="o1469" name="seed" name_original="seeds" src="d0_s22" type="structure" />
      <biological_entity id="o1470" name="endosperm" name_original="endosperm" src="d0_s22" type="structure">
        <character is_modifier="true" name="coating" src="d0_s22" value="oily" />
      </biological_entity>
      <biological_entity id="o1471" name="embryo" name_original="embryo" src="d0_s22" type="structure">
        <character is_modifier="false" name="course" src="d0_s22" value="straight" />
      </biological_entity>
      <relation from="o1469" id="r300" name="with" negation="false" src="d0_s22" to="o1470" />
    </statement>
  </description>
  <discussion>Genus 1, species 35–50 (1 in the flora): introduced, California; Mexico, Central America, South America, w Europe, se Asia, s, e Africa, Pacific Islands (Hawaii), Australia.</discussion>
  <discussion>The systematic position of Gunneraceae has been uncertain for a long time.  Gunnera has sometimes been included in the mostly aquatic Haloragaceae, which also have reduced flowers with an inferior ovary.  Molecular studies (combining rbcL, atpB, and 18S data) suggest that Gunnera occupies an isolated position among the core “eudicots.”  Some of these studies have also identified the African shrub Myrothamnus Welwitsch as the closest relative to Gunnera.  Other work, based on leaf morphology and pollen, suggests a close relationship with Saxifragaceae (D. G. Fuller and L. J. Hickey 2005).  Gunnera is remarkable for its symbiosis with cyanobacteria of the genus Nostoc (B. Osborne et al. 1991).</discussion>
  <discussion>Species of Gunnera subg. Panke (Molina) Schindler are unique in having large, triangular scales between the leaves on the rhizomes.  The morphological significance of these scales has been debated, and they have been interpreted as stipules, ligules, or cataphylls.  Comparisons with features of the rhizomes in other subgenera support the interpretation of these structures as cataphylls (L. Wanntorp et al. 2003).</discussion>
  <references>
    <reference>Fuller, D. G. and L. J. Hickey.  2005.  Systematics and leaf architecture of the Gunneraceae.  Bot. Rev. (Lancaster) 71: 295–353.</reference>
    <reference>Osborne, B. et al.  1991.  Gunnera tinctoria: An unusual nitrogen-fixing invader.  BioScience 41: 224–234.</reference>
    <reference>Wanntorp, L., H.-E. Wanntorp, and M. Källersjö.  2002.  Phylogenetic relationships of Gunnera based on nuclear ribosomal DNA ITS region, rbcL and rps16 intron sequences.  Syst. Bot. 27: 512–521.</reference>
    <reference>Wilkinson, H. P. and L. Wanntorp.  2007.  Gunneraceae.  In: K. Kubitzki et al., eds.  1990+.  The Families and Genera of Vascular Plants.  15+ vols.  Berlin etc.  Vol. 9, pp. 177–183.</reference>
  </references>
  
</bio:treatment>