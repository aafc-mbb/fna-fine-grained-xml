<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MELASTOMATACEAE</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">RHEXIA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">mariana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 346.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family melastomataceae;genus rhexia;species mariana</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Maryland meadow beauty</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices not developed;</text>
      <biological_entity id="o2499" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="development" src="d0_s0" value="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots often long and rhizomelike, non-tuberiferous.</text>
      <biological_entity id="o2500" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="length_or_size" src="d0_s1" value="long" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomelike" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="non-tuberiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually few to several-branched distally, sometimes unbranched, 20–80 cm, faces strongly unequal, 1 pair of opposite faces rounded to convex, the other narrower, flat or concave, without wings or very narrowly winged, internodes and nodes usually hirsute-villous, rarely glabrous or glabrate, hairs gland-tipped.</text>
      <biological_entity id="o2501" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s2" value="few" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="several-branched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="unbranched" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2502" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s2" value="unequal" />
        <character constraint="of faces" constraintid="o2503" name="quantity" src="d0_s2" value="1" />
        <character is_modifier="false" name="width" notes="" src="d0_s2" value="narrower" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flat" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" />
      </biological_entity>
      <biological_entity id="o2503" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="rounded" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="convex" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="convex" />
      </biological_entity>
      <biological_entity id="o2504" name="wing" name_original="wings" src="d0_s2" type="structure" />
      <biological_entity id="o2505" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="very narrowly" name="architecture" src="d0_s2" value="winged" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="hirsute-villous" />
      </biological_entity>
      <biological_entity id="o2506" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="very narrowly" name="architecture" src="d0_s2" value="winged" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="hirsute-villous" />
      </biological_entity>
      <biological_entity id="o2507" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="gland-tipped" />
      </biological_entity>
      <relation from="o2502" id="r503" name="without" negation="false" src="d0_s2" to="o2504" />
      <relation from="o2502" id="r504" name="without" negation="false" src="d0_s2" to="o2505" />
      <relation from="o2502" id="r505" name="without" negation="false" src="d0_s2" to="o2506" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile, subsessile, or petioles 0.5–1.5 mm;</text>
      <biological_entity id="o2508" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" />
      </biological_entity>
      <biological_entity id="o2509" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, lanceolate, elliptic, or narrowly ovate, rarely linear-filiform, 2–4 cm × (5–) 8–15 (–20) mm, margins serrate, surfaces loosely strigose to strigose-hirsute or villous.</text>
      <biological_entity id="o2510" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="ovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="linear-filiform" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s4" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2511" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" />
      </biological_entity>
      <biological_entity id="o2512" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" notes="[duplicate value]" src="d0_s4" value="strigose" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s4" value="strigose-hirsute" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s4" value="villous" />
        <character char_type="range_value" from="loosely strigose" name="pubescence" src="d0_s4" to="strigose-hirsute or villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences diffuse, not obscured by bracts.</text>
      <biological_entity id="o2513" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="density" src="d0_s5" value="diffuse" />
        <character constraint="by bracts" constraintid="o2514" is_modifier="false" modifier="not" name="prominence" src="d0_s5" value="obscured" />
      </biological_entity>
      <biological_entity id="o2514" name="bract" name_original="bracts" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium ovoid to subglobose, about as long as the constricted neck, 6–10 mm, hirsute-villous, glabrous, or glabrate, hairs gland-tipped;</text>
      <biological_entity id="o2515" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2516" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="subglobose" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="subglobose" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hirsute-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" />
      </biological_entity>
      <biological_entity id="o2517" name="neck" name_original="neck" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="constricted" />
      </biological_entity>
      <biological_entity id="o2518" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" />
      </biological_entity>
      <relation from="o2516" id="r506" name="as long as" negation="false" src="d0_s6" to="o2517" />
    </statement>
    <statement id="d0_s7">
      <text>calyx lobes narrowly triangular, apices acute to acuminate, narrowed to linear-oblong extensions;</text>
      <biological_entity id="o2519" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o2520" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="triangular" />
      </biological_entity>
      <biological_entity id="o2521" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="acuminate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="narrowed" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="linear-oblong" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate narrowed" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="acuminate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="narrowed" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="linear-oblong" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate narrowed" />
      </biological_entity>
      <biological_entity id="o2522" name="extension" name_original="extensions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>petals spreading, white or pale to dull lavender, 1.2–1.5 cm;</text>
      <biological_entity id="o2523" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2524" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="white" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="pale" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="dull" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="lavender" />
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="dull lavender" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers curved, 5–8 mm.</text>
      <biological_entity id="o2525" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2526" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="curved" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 0.7 mm, surfaces longitudinally ridged with contiguous tubercles, papil­lae, or laterally flattened domes.</text>
      <biological_entity id="o2527" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.7" />
      </biological_entity>
      <biological_entity id="o2529" name="tubercle" name_original="tubercles" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="contiguous" />
      </biological_entity>
      <biological_entity id="o2530" name="dome" name_original="domes" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="laterally" name="shape" src="d0_s10" value="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 22.</text>
      <biological_entity id="o2528" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character constraint="with tubercles" constraintid="o2529" is_modifier="false" modifier="longitudinally" name="shape" src="d0_s10" value="ridged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2531" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="22" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sc, e United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sc" establishment_means="native" />
        <character name="distribution" value="e United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As noted by R. Kral and P. E. Bostick (1969), Rhexia mariana is the most abundant and wide-ranging species of the genus.  It is sympatric with every other species and hybridizes with, and often takes on characteristics of, other species.</discussion>
  <discussion>Variety exalbida was formally recognized by C. W. James (1956) as distinct in its white petals and linear leaves and distribution mostly from southern Mississippi to Florida and along the coastal plain to the Carolinas; James noted that differences are quantitative and intergrading.  R. Kral and P. E. Bostick (1969) observed that its distinct distribution might support its taxonomic recognition but that intergradation with var. mariana, especially in the Florida panhandle across the outer coastal plain to Texas, suggested that only a single entity should be recognized.  The geography of chromosome counts reported by Kral and Bostick indicates that vars. exalbida and mariana are diploid.  With the caveat that the decision is subjective, var. exalbida is treated here as distinct, emphasizing its geographic concentration in the southeastern corner of the species range.</discussion>
  <discussion>In the concept of R. Kral and P. E. Bostick (1969), Rhexia mariana also includes vars. interior and ventricosa.  The geographic ranges of each of these varieties lie almost completely within that of var. mariana; each of the varieties is tetraploid; var. mariana is diploid.  The morphological differences that separate these entities are subtle but they appear to be consistent, and the ploidal differences probably act as isolating mechanisms.  Of the alternatives, to treat all as a single species without subdivisions disregards the biology; to treat them as three varieties disregards the apparent isolation, which usually is a significant feature of a species concept.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals dull lavender to lavender-rose; leaf blades lanceolate to elliptic or narrowly ovate.</description>
      <determination>11a. Rhexia mariana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals white or pale lavender; leaf blades mostly linear.</description>
      <determination>11b. Rhexia mariana var. exalbida</determination>
    </key_statement>
  </key>
</bio:treatment>