<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POLYGALA</taxon_name>
    <taxon_name authority="Chodat" date="1893" rank="species">watsonii</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Soc. Phys. Genève</publication_title>
      <place_in_publication>31(2): 285, plate 26, figs. 8, 9.  1893</place_in_publication>
      <other_info_on_pub>(as watsoni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species watsonii</taxon_hierarchy>
  </taxon_identification>
  <number>31.</number>
  <other_name type="common_name">Watson’s milkwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, multistemmed, 0.5–5 dm, branched throughout;</text>
    </statement>
    <statement id="d0_s1">
      <text>from thickened caudex.</text>
      <biological_entity id="o2356" name="subshrub" name_original="subshrubs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_growth_form" src="d0_s0" value="multistemmed" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s0" value="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sometimes laxly so, puberulent or subglabrous, hairs incurved.</text>
      <biological_entity id="o2357" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" modifier="sometimes laxly; laxly" name="pubescence" src="d0_s2" value="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="subglabrous" />
      </biological_entity>
      <biological_entity id="o2358" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or subsessile;</text>
      <biological_entity id="o2359" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually linear-lanceolate to linear, sometimes falcate-linear with reflexed tip, 4–15 × 0.3–1 mm, base obtuse or acute, apex acute to acuminate, surfaces usually puberulent, rarely subglabrous, hairs incurved.</text>
      <biological_entity id="o2360" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate to linear" />
        <character constraint="with tip" constraintid="o2361" is_modifier="false" modifier="sometimes" name="arrangement_or_course_or_shape" src="d0_s5" value="falcate-linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" notes="" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2361" name="tip" name_original="tip" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="reflexed" />
      </biological_entity>
      <biological_entity id="o2362" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" />
      </biological_entity>
      <biological_entity id="o2363" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acuminate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o2364" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate to linear" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="puberulent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s5" value="subglabrous" />
      </biological_entity>
      <biological_entity id="o2365" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes subcapitate, 0.5–1.4 (–2.5) × 1 cm;</text>
      <biological_entity id="o2366" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="subcapitate" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s6" to="1.4" to_unit="cm" />
        <character name="width" src="d0_s6" unit="cm" value="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle 0–0.5 cm;</text>
      <biological_entity id="o2367" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts deciduous, ovate to lanceolate-ovate.</text>
      <biological_entity id="o2368" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="lanceolate-ovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0.5 mm, glabrous.</text>
      <biological_entity id="o2369" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.5" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers white, greenish or purplish veined, keel pink to purplish brown, often yellow-green distally, (3.7–) 4–6 mm;</text>
      <biological_entity id="o2370" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="veined" />
      </biological_entity>
      <biological_entity id="o2371" name="keel" name_original="keel" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="purplish brown" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="purplish brown" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s10" value="yellow-green" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals oblong-ovate, 1.3 mm;</text>
      <biological_entity id="o2372" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong-ovate" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>wings spatulate-obovate, (3.5–) 4–6 × 1.3–1.6 mm, apex obtuse or acute, sometimes minutely apiculate;</text>
      <biological_entity id="o2373" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="spatulate-obovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s12" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s12" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2374" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_shape" src="d0_s12" value="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keel 2.8 mm, crest 2-parted, with 3 divided lobes on each side.</text>
      <biological_entity id="o2375" name="keel" name_original="keel" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="2.8" />
      </biological_entity>
      <biological_entity id="o2376" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="2-parted" />
      </biological_entity>
      <biological_entity id="o2377" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="3" />
        <character is_modifier="true" name="shape" src="d0_s13" value="divided" />
      </biological_entity>
      <biological_entity id="o2378" name="side" name_original="side" src="d0_s13" type="structure" />
      <relation from="o2376" id="r287" name="with" negation="false" src="d0_s13" to="o2377" />
      <relation from="o2377" id="r288" name="on" negation="false" src="d0_s13" to="o2378" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules oblong-ellipsoid to oblong, 2.3–3.5 × 1.6 mm, abaxial locule not winged, adaxial locule slightly longer, not or obscurely winged.</text>
      <biological_entity id="o2379" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="oblong-ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="oblong" />
        <character char_type="range_value" from="oblong-ellipsoid" name="shape" src="d0_s14" to="oblong" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" src="d0_s14" to="3.5" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="1.6" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2380" name="locule" name_original="locule" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2381" name="locule" name_original="locule" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly" name="length_or_size" src="d0_s14" value="longer" />
        <character is_modifier="false" modifier="not; obscurely" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2.5–3 mm, puberulent, coat with rows of pits 0.05 mm wide;</text>
      <biological_entity id="o2382" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="puberulent" />
      </biological_entity>
      <biological_entity id="o2383" name="coat" name_original="coat" src="d0_s15" type="structure" />
      <biological_entity id="o2384" name="row" name_original="rows" src="d0_s15" type="structure">
        <character name="width" src="d0_s15" unit="mm" value="0.05" />
      </biological_entity>
      <biological_entity id="o2385" name="pit" name_original="pits" src="d0_s15" type="structure" />
      <relation from="o2383" id="r289" name="with" negation="false" src="d0_s15" to="o2384" />
      <relation from="o2384" id="r290" name="part_of" negation="false" src="d0_s15" to="o2385" />
    </statement>
    <statement id="d0_s16">
      <text>aril 1–1.9 mm, lobes 1/2 to subequal to length of seed.</text>
      <biological_entity id="o2386" name="aril" name_original="aril" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2388" name="seed" name_original="seed" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 16.</text>
      <biological_entity id="o2387" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1/2" />
        <character constraint="to " constraintid="o2388" is_modifier="false" name="size" src="d0_s16" value="subequal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2389" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Polygala watsonii is known in the flora area from a single collection from the Glass Mountains in Brewster County (T. L. Wendt 1979).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stony limestone slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stony limestone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>