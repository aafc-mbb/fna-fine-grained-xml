<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TRAPA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">natans</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 120.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus trapa;species natans</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems slender, young growth and flowering parts velutinous.</text>
      <biological_entity id="o4109" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" />
      </biological_entity>
      <biological_entity id="o4110" name="growth" name_original="growth" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="young" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="velutinous" />
      </biological_entity>
      <biological_entity id="o4111" name="part" name_original="parts" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="young" />
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="flowering" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="velutinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves of floating rosettes bearing successively longer pet­ioles toward outer edges of rosette, to 20 cm;</text>
      <biological_entity id="o4112" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="toward outer edges" constraintid="o4114" is_modifier="false" modifier="successively" name="length_or_size" src="d0_s1" value="longer" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4113" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form_or_location" src="d0_s1" value="floating" />
      </biological_entity>
      <biological_entity constraint="outer" id="o4114" name="edge" name_original="edges" src="d0_s1" type="structure" />
      <biological_entity id="o4115" name="rosette" name_original="rosette" src="d0_s1" type="structure" />
      <relation from="o4112" id="r504" name="part_of" negation="false" src="d0_s1" to="o4113" />
      <relation from="o4114" id="r505" name="part_of" negation="false" src="d0_s1" to="o4115" />
    </statement>
    <statement id="d0_s2">
      <text>blade 20–40 × 25–60 mm, width greater than length, surfaces velutinous abax­ially, glabrous adaxially.</text>
      <biological_entity id="o4116" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s2" to="60" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s2" value="greater than length" />
      </biological_entity>
      <biological_entity id="o4117" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="velutinous" />
        <character is_modifier="false" modifier="ially; adaxially" name="pubescence" src="d0_s2" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Floral-tube 2 mm;</text>
      <biological_entity id="o4118" name="floral-tube" name_original="floral-tube" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals 4–7 mm, keeled;</text>
      <biological_entity id="o4119" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals obovate, 8–15 mm.</text>
      <biological_entity id="o4120" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Drupes 20–25 mm diam., excluding spines;</text>
      <biological_entity id="o4121" name="drupe" name_original="drupes" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4122" name="spine" name_original="spines" src="d0_s6" type="structure" />
      <relation from="o4121" id="r506" name="excluding" negation="false" src="d0_s6" to="o4122" />
    </statement>
    <statement id="d0_s7">
      <text>horns 2–4, to ca. 10 mm. 2n = 48 (Poland, Japan), 96 (Japan).</text>
      <biological_entity id="o4123" name="horn" name_original="horns" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" unit=",0-10 mm" />
        <character char_type="range_value" from="2" name="some_measurement" src="d0_s7" to="4" unit=",0-10 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4124" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="48" />
        <character name="quantity" src="d0_s7" value="96" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Trapa natans was first noted in Massachusetts in 1859 from an unknown origin and was recorded from the Charles River, Cambridge, in 1879.  The hard, spiny fruits can cause severe puncture wounds and are slow to decay in lake and river bottoms.  The species propagates by seed and by detached floating rosettes to form extensive floating mats that reduce oxygen, restrict light, crowd out native plants, and make navigation difficult.  Populations grow rapidly and are difficult to eradicate except by sustained efforts over multiple seasons.  Federal regulations now prohibit interstate sale and transport of T. natans.  The species is considered rare and threatened in Europe.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Relatively neutral, nutrient-rich, flowing or still waters, rivers, ponds, lakes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="neutral" modifier="relatively" />
        <character name="habitat" value="nutrient-rich" />
        <character name="habitat" value="flowing" />
        <character name="habitat" value="waters" modifier="still" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Que.; Conn., Del., D.C., Maine, Md., Mass., N.H., N.J., N.Y., Pa., R.I., Vt., Va.; Europe; Asia; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>