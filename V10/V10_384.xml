<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Spach) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Godetia</taxon_name>
    <taxon_name authority="(Durand &amp; Hilgard) H. Lewis &amp; M. E. Lewis" date="1953" rank="species">williamsonii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>12: 34.  1953</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section godetia;species williamsonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Godetia</taxon_name>
    <taxon_name authority="Durand &amp; Hilgard in War Department [U.S.]" date="1857" rank="species">williamsonii</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Railr. Rep.</publication_title>
      <place_in_publication>5(3): 7, plate 5.  1857</place_in_publication>
      <other_info_on_pub>(as williamsoni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus godetia;species williamsonii</taxon_hierarchy>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Fort Miller clarkia or fairyfan</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, to 100 cm, puber­ulent.</text>
      <biological_entity id="o6728" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 0–10 mm;</text>
      <biological_entity id="o6729" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o6730" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear to narrowly lanceolate, 2–7 cm.</text>
      <biological_entity id="o6731" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflores­cences open racemes, axis straight;</text>
      <biological_entity id="o6732" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="narrowly lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6733" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="open" />
      </biological_entity>
      <biological_entity id="o6734" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>buds erect, mucronate.</text>
      <biological_entity id="o6735" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" />
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: floral-tube 7–13 mm;</text>
      <biological_entity id="o6736" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6737" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed individually or in pairs, tips distinct in bud;</text>
      <biological_entity id="o6738" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6739" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="in pairs" is_modifier="false" modifier="individually" name="orientation" src="d0_s6" value="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="in pairs" />
      </biological_entity>
      <biological_entity id="o6740" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character constraint="in bud" constraintid="o6741" is_modifier="false" name="fusion" src="d0_s6" value="distinct" />
      </biological_entity>
      <biological_entity id="o6741" name="bud" name_original="bud" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>corolla bowl-shaped, petals usually lavender, white near middle with purple spot distally, rarely uniformly wine-red, 10–30 mm;</text>
      <biological_entity id="o6742" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6743" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="bowl--shaped" value_original="bowl-shaped" />
      </biological_entity>
      <biological_entity id="o6744" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration_or_odor" src="d0_s7" value="lavender" />
        <character constraint="near middle" constraintid="o6745" is_modifier="false" name="coloration" src="d0_s7" value="white" />
        <character is_modifier="false" modifier="rarely uniformly" name="coloration" notes="" src="d0_s7" value="wine-red" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6745" name="middle" name_original="middle" src="d0_s7" type="structure">
        <character constraint="with" is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="purple spot" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 8, subequal;</text>
      <biological_entity id="o6746" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6747" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 8-grooved, shorter than adjacent internode;</text>
      <biological_entity id="o6748" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6749" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="8-grooved" />
        <character constraint="than adjacent internode" constraintid="o6750" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" />
      </biological_entity>
      <biological_entity id="o6750" name="internode" name_original="internode" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma usually exserted beyond anthers.</text>
      <biological_entity id="o6751" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6752" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o6753" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o6752" id="r1189" modifier="usually" name="exserted beyond" negation="false" src="d0_s10" to="o6753" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules 10–30 mm.</text>
      <biological_entity id="o6754" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds brown or gray, 1–1.5 mm, scaly, crest 0.1 mm. 2n = 18.</text>
      <biological_entity id="o6755" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="gray" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s12" value="scaly" />
      </biological_entity>
      <biological_entity id="o6756" name="crest" name_original="crest" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6757" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Clarkia williamsonii occurs widely along the western slope of the Sierra Nevada from Nevada to Kern counties, and the Tehachapi Mountains barely to Los Angeles and Santa Barbara counties (one collection each).  There are unverified reports from Riverside and Shasta counties.</discussion>
  <discussion>Clarkia williamsonii is similar to C. speciosa and some populations of the hexaploid C. purpurea but can be distinguished from the former by petal color pattern and from both by having sepals that have distinctly free tips in bud, a trait most obvious in pressed specimens when the tips tend to spread apart.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Foothill woodlands, yellow-pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="foothill woodlands" />
        <character name="habitat" value="yellow-pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>