<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) W. L. Wagner &amp; Hoch" date="2007" rank="section">Anogra</taxon_name>
    <taxon_name authority="(Small) Munz" date="1931" rank="species">engelmannii</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>18: 316.  1931</place_in_publication>
      <other_info_on_pub>(as engelmanni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section anogra;species engelmannii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anogra</taxon_name>
    <taxon_name authority="(Lindley) Britton" date="unknown" rank="species">pallida</taxon_name>
    <taxon_name authority="Small" date="1896" rank="variety">engelmannii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 176.  1896</place_in_publication>
      <other_info_on_pub>(as engelmanni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus anogra;species pallida;variety engelmannii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="(Small) Wooton &amp; Standley" date="unknown" rank="species">engelmannii</taxon_name>
    <taxon_hierarchy>genus a.;species engelmannii</taxon_hierarchy>
  </taxon_identification>
  <number>62.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs winter-annual, conspic­uously villous throughout, also strigillose on leaves and distal parts;</text>
      <biological_entity id="o662" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="distal" id="o663" name="part" name_original="parts" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>from a taproot.</text>
      <biological_entity id="o661" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" />
        <character is_modifier="false" modifier="uously; throughout" name="pubescence" src="d0_s0" value="villous" />
        <character constraint="on distal parts" constraintid="o663" is_modifier="false" name="pubescence" src="d0_s0" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, unbranched or with few, spreading branches, 30–50 (–80) cm.</text>
      <biological_entity id="o664" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="with few , spreading branches" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="80" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o665" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" />
        <character is_modifier="true" name="orientation" src="d0_s2" value="spreading" />
      </biological_entity>
      <relation from="o664" id="r165" name="with" negation="false" src="d0_s2" to="o665" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, rosette weakly developed or absent, at least during flowering, (1–) 2–6 (–8) × 1–2 (–3) cm;</text>
      <biological_entity id="o666" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o667" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o666" id="r166" name="in" negation="false" src="d0_s3" to="o667" />
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o668" name="rosette" name_original="rosette" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="weakly" name="development" src="d0_s3" value="developed" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="during flowering" name="atypical_length" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" modifier="during flowering" name="atypical_length" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" modifier="during flowering" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" modifier="during flowering" name="atypical_width" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="during flowering" name="width" src="d0_s3" to="2" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade lanceolate to oblong-lanceolate, proximal ones sometimes oblanceolate, margins coarsely repand-dentate or pinnatifid.</text>
      <biological_entity id="o669" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="oblong-lanceolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblong-lanceolate" />
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="oblanceolate" />
      </biological_entity>
      <biological_entity id="o670" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s5" value="repand-dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1–several opening per day near sunset;</text>
      <biological_entity id="o671" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s6" to="several" />
      </biological_entity>
      <biological_entity id="o672" name="day" name_original="day" src="d0_s6" type="structure" />
      <biological_entity id="o673" name="sunset" name_original="sunset" src="d0_s6" type="structure" />
      <relation from="o671" id="r167" name="opening per" negation="false" src="d0_s6" to="o672" />
      <relation from="o671" id="r168" name="near" negation="false" src="d0_s6" to="o673" />
    </statement>
    <statement id="d0_s7">
      <text>buds nodding, weakly quadran­gular, without free tips;</text>
      <biological_entity id="o674" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="nodding" />
      </biological_entity>
      <biological_entity id="o675" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
      </biological_entity>
      <relation from="o674" id="r169" modifier="weakly" name="without" negation="false" src="d0_s7" to="o675" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube 20–30 mm;</text>
      <biological_entity id="o676" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 13–21 mm, not spotted or with scattered small, maroon spots;</text>
      <biological_entity id="o677" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s9" to="21" to_unit="mm" />
        <character constraint="with scattered small" is_modifier="false" modifier="not" name="coloration" src="d0_s9" value="spotted" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="with scattered small" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="maroon spots" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, fading pink, broadly obovate or obcordate, 15–30 mm;</text>
      <biological_entity id="o678" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading pink" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obcordate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 14–16 mm, anthers 6–8 mm;</text>
      <biological_entity id="o679" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s11" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o680" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 40–50 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o681" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s12" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o682" name="stigma" name_original="stigma" src="d0_s12" type="structure" />
      <biological_entity id="o683" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o682" id="r170" name="exserted beyond" negation="false" src="d0_s12" to="o683" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules widely spreading, woody in age, straight or slightly curved, cylindrical, obtusely 4-angled, especially toward base, tapering gradually from base to apex, 30–60 × 2–3 mm;</text>
      <biological_entity id="o685" name="age" name_original="age" src="d0_s13" type="structure" />
      <biological_entity id="o686" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o687" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o688" name="apex" name_original="apex" src="d0_s13" type="structure" />
      <relation from="o684" id="r171" modifier="especially" name="toward" negation="false" src="d0_s13" to="o686" />
      <relation from="o687" id="r172" name="to" negation="false" src="d0_s13" to="o688" />
    </statement>
    <statement id="d0_s14">
      <text>sessile.</text>
      <biological_entity id="o684" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s13" value="spreading" />
        <character constraint="in age" constraintid="o685" is_modifier="false" name="texture" src="d0_s13" value="woody" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s13" value="curved" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindrical" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s13" value="4-angled" />
        <character constraint="from base" constraintid="o687" is_modifier="false" name="shape" notes="" src="d0_s13" value="tapering" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" notes="" src="d0_s13" to="60" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds numerous, in 1 row per locule, brown, narrowly obovoid, 1–1.5 mm. 2n = 14.</text>
      <biological_entity id="o689" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="numerous" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="obovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o690" name="row" name_original="row" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" />
      </biological_entity>
      <biological_entity id="o691" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <biological_entity constraint="2n" id="o692" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" />
      </biological_entity>
      <relation from="o689" id="r173" name="in" negation="false" src="d0_s15" to="o690" />
      <relation from="o690" id="r174" name="per" negation="false" src="d0_s15" to="o691" />
    </statement>
  </description>
  <discussion>Oenothera engelmannii is self-incompatible (W. L. Wagner et al. 2007; K. E. Theiss et al. 2010).  It has a relatively narrow distribution in sandy areas of eastern New Mexico and western Texas, extending to southeastern Colorado, western Oklahoma, and southwestern Kansas.  The flower size seems to vary, with larger flowers in eastern New Mexico and considerably smaller flowers in the eastern part of its range.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy prairies, dunes, disturbed areas, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy prairies" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., N.Mex., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>