<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Fibula</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>20: 333. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section fibula</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clarkia</taxon_name>
    <taxon_name authority="K. E. Holsinger" date="unknown" rank="subgenus">Xantianae</taxon_name>
    <taxon_hierarchy>genus clarkia;subgenus xantianae</taxon_hierarchy>
  </taxon_identification>
  <number>6g.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences: axis erect;</text>
      <biological_entity id="o6990" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
      <biological_entity id="o6991" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>buds pendent.</text>
      <biological_entity id="o6992" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o6993" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: floral-tube obconic to campanulate, 2–5 mm;</text>
      <biological_entity id="o6994" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o6995" name="floral-tube" name_original="floral-tube" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="obconic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="campanulate" />
        <character char_type="range_value" from="obconic" name="shape" src="d0_s2" to="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals reflexed together to 1 side;</text>
      <biological_entity id="o6996" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o6997" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="together" name="orientation" src="d0_s3" value="reflexed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="1" />
      </biological_entity>
      <biological_entity id="o6998" name="side" name_original="side" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petals pale to pink-lavender or reddish purple, white near base and red-flecked or upper petals with a white-bordered dark spot (C. xantiana), fan-shaped, unlobed or shallowly 2-lobed, with subulate tooth in sinus, claw inconspicuous (conspicuous in C. xantiana);</text>
      <biological_entity id="o6999" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o7000" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="pale" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="pink-lavender" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="reddish purple" />
        <character char_type="range_value" from="pale" name="coloration" src="d0_s4" to="pink-lavender or reddish purple" />
        <character constraint="near base" constraintid="o7001" is_modifier="false" name="coloration" src="d0_s4" value="white" />
      </biological_entity>
      <biological_entity id="o7001" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="upper" id="o7002" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="red-flecked" />
        <character is_modifier="false" modifier="with a white-bordered dark spot" name="shape" src="d0_s4" value="fan--shaped" value_original="fan-shaped" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="2-lobed" />
      </biological_entity>
      <biological_entity id="o7003" name="tooth" name_original="tooth" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="subulate" />
      </biological_entity>
      <biological_entity id="o7004" name="sinus" name_original="sinus" src="d0_s4" type="structure" />
      <biological_entity id="o7005" name="claw" name_original="claw" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" />
      </biological_entity>
      <relation from="o7002" id="r1219" name="with" negation="false" src="d0_s4" to="o7003" />
      <relation from="o7003" id="r1220" name="in" negation="false" src="d0_s4" to="o7004" />
    </statement>
    <statement id="d0_s5">
      <text>stamens 8, in 2 unequal sets, outer anthers with lavender to purple pollen, inner shorter, with cream pollen.</text>
      <biological_entity id="o7006" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o7007" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="8" />
      </biological_entity>
      <biological_entity id="o7008" name="set" name_original="sets" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" />
        <character is_modifier="true" name="size" src="d0_s5" value="unequal" />
      </biological_entity>
      <biological_entity constraint="outer" id="o7009" name="anther" name_original="anthers" src="d0_s5" type="structure" />
      <biological_entity id="o7010" name="pollen" name_original="pollen" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" notes="[duplicate value]" src="d0_s5" value="lavender" />
        <character is_modifier="true" name="coloration" notes="[duplicate value]" src="d0_s5" value="purple" />
        <character char_type="range_value" from="lavender" is_modifier="true" name="coloration" src="d0_s5" to="purple" />
      </biological_entity>
      <biological_entity constraint="inner" id="o7011" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" />
      </biological_entity>
      <biological_entity id="o7012" name="pollen" name_original="pollen" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="cream" />
      </biological_entity>
      <relation from="o7007" id="r1221" name="in" negation="false" src="d0_s5" to="o7008" />
      <relation from="o7009" id="r1222" name="with" negation="false" src="d0_s5" to="o7010" />
      <relation from="o7011" id="r1223" name="with" negation="false" src="d0_s5" to="o7012" />
    </statement>
    <statement id="d0_s6">
      <text>Capsules terete, 4-grooved, or conspicuously 8-ribbed (C. xantiana);</text>
    </statement>
    <statement id="d0_s7">
      <text>sessile or subsessile to long-pedicellate.</text>
      <biological_entity id="o7013" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="4-grooved" />
        <character is_modifier="false" modifier="conspicuously" name="architecture" src="d0_s6" value="8-ribbed" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="4-grooved" />
        <character is_modifier="false" modifier="conspicuously" name="architecture" src="d0_s6" value="8-ribbed" />
        <character is_modifier="false" name="architecture" notes="[duplicate value]" src="d0_s7" value="sessile" />
        <character is_modifier="false" name="architecture" notes="[duplicate value]" src="d0_s7" value="subsessile" />
        <character is_modifier="false" name="architecture" notes="[duplicate value]" src="d0_s7" value="long-pedicellate" />
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s7" to="long-pedicellate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="California." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Sytsma, K. J. and L. D. Gottlieb.  1986.  Chloroplast DNA evolution and phylogenetic relationships in Clarkia sect. Peripetasma (Onagraceae).  Evolution 40: 1248–1261.</reference>
  </references>
  
</bio:treatment>