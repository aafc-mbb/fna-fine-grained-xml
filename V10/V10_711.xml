<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="L’Heritier" date="1792" rank="genus">EUCALYPTUS</taxon_name>
    <taxon_name authority="F. Mueller" date="1853" rank="species">cladocalyx</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>25: 388.  1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus eucalyptus;species cladocalyx</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Sugar gum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, 20 m;</text>
      <biological_entity id="o2889" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character name="some_measurement" src="d0_s0" unit="m" value="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk white, often mottled gray, orange, or tan, mostly straight, graceful, ± smooth;</text>
      <biological_entity id="o2890" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="mottled gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="orange" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="orange" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan" />
        <character is_modifier="false" modifier="mostly" name="course" src="d0_s1" value="straight" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark shed in large, irregular patches.</text>
      <biological_entity id="o2891" name="bark" name_original="bark" src="d0_s2" type="structure" />
      <biological_entity id="o2892" name="patch" name_original="patches" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="large" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s2" value="irregular" />
      </biological_entity>
      <relation from="o2891" id="r551" name="shed in" negation="false" src="d0_s2" to="o2892" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 0.1–0.2 cm;</text>
      <biological_entity id="o2893" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2894" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s3" to="0.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade light green abaxially, ± widely lanceolate, 8–15 × 2–3 cm.</text>
      <biological_entity id="o2895" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2896" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s4" value="light green" />
        <character is_modifier="false" modifier="more or less widely" name="shape" src="d0_s4" value="lanceolate" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 1–3 cm.</text>
      <biological_entity id="o2897" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences mostly 7–11-flowered, umbels, usually on leafless branches.</text>
      <biological_entity id="o2898" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s6" value="7-11-flowered" />
      </biological_entity>
      <biological_entity id="o2899" name="umbel" name_original="umbels" src="d0_s6" type="structure" />
      <biological_entity id="o2900" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafless" />
      </biological_entity>
      <relation from="o2899" id="r552" modifier="usually" name="on" negation="false" src="d0_s6" to="o2900" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium cylindrical or urn-shaped, ± ribbed, less than 10 mm, length 3–4 times calyptra;</text>
      <biological_entity id="o2901" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2902" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s7" value="urn--shaped" value_original="urn-shaped" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s7" value="ribbed" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character constraint="calyptra" constraintid="o2903" is_modifier="false" name="length" src="d0_s7" value="3-4 times calyptra" />
      </biological_entity>
      <biological_entity id="o2903" name="calyptra" name_original="calyptra" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>calyptra cylindric to urn-shaped, abruptly pointed;</text>
      <biological_entity id="o2904" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2905" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="cylindric" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="urn--shaped" value_original="urn-shaped" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s8" to="urn-shaped" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s8" value="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens white.</text>
      <biological_entity id="o2906" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2907" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules ± urn-shaped, ribbed, 10–15 mm, not glaucous;</text>
      <biological_entity id="o2908" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="urn--shaped" value_original="urn-shaped" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="ribbed" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s10" value="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves 3 or 4, included.</text>
      <biological_entity id="o2909" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" unit="or" value="3" />
        <character name="quantity" src="d0_s11" unit="or" value="4" />
        <character is_modifier="false" name="position" src="d0_s11" value="included" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eucalyptus cladocalyx is commonly cultivated in southern California.</discussion>
  <discussion>Eucalyptus corynocalyx F. Mueller is an illegitimate name based on the same type as E. cladocalyx.  Mueller may have thought corynocalyx (club-calyx) was a more appropriate name than cladocalyx (branch-calyx) and intended to change the name.  The closed bud of this species is clublike.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; s Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="s Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>