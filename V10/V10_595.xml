<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(P. H. Raven) W. L. Wagner &amp; Hoch" date="2007" rank="genus">EREMOTHERA</taxon_name>
    <taxon_name authority="(P. H. Raven) W. L. Wagner &amp; Hoch" date="2007" rank="species">gouldii</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 210.  2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus eremothera;species gouldii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="P. H. Raven" date="1969" rank="species">gouldii</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>37: 368, fig. 70.  1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus camissonia;species gouldii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(P. H. Raven) S. L. Welsh &amp; N. D. Atwood" date="unknown" rank="species">gouldii</taxon_name>
    <taxon_hierarchy>genus oenothera;species gouldii</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs glandular puberulent, sometimes also moderately or sparsely villous.</text>
      <biological_entity id="o4966" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
        <character is_modifier="false" modifier="sometimes; moderately; sparsely" name="pubescence" src="d0_s0" value="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or loosely branched, 6–20 cm, usually flowering only distally.</text>
      <biological_entity id="o4967" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s1" value="branched" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="usually; only distally; distally" name="life_cycle" src="d0_s1" value="flowering" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, sometimes with lower ones clustered near base, 0.5–3.5 × 0.5–1 cm;</text>
      <biological_entity id="o4968" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" notes="" src="d0_s2" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" notes="" src="d0_s2" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o4969" name="one" name_original="ones" src="d0_s2" type="structure">
        <character constraint="near base" constraintid="o4970" is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" />
      </biological_entity>
      <biological_entity id="o4970" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o4968" id="r638" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o4969" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 0–0.6 cm;</text>
      <biological_entity id="o4971" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="0.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic-lanceolate to elliptic, margins crenate-dentate or serrulate.</text>
      <biological_entity id="o4972" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="elliptic-lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="elliptic" />
        <character char_type="range_value" from="elliptic-lanceolate" name="shape" src="d0_s4" to="elliptic" />
      </biological_entity>
      <biological_entity id="o4973" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences only at terminal nodes, nodding at anthesis.</text>
      <biological_entity id="o4974" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" notes="" src="d0_s5" value="nodding" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o4975" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o4974" id="r639" name="at" negation="false" src="d0_s5" to="o4975" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers opening at sunset;</text>
      <biological_entity id="o4976" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4977" name="sunset" name_original="sunset" src="d0_s6" type="structure" />
      <relation from="o4976" id="r640" name="opening at" negation="false" src="d0_s6" to="o4977" />
    </statement>
    <statement id="d0_s7">
      <text>floral-tube 1.5–3 mm, villous in proximal 1/2 inside;</text>
      <biological_entity id="o4978" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character constraint="in inside" constraintid="o4979" is_modifier="false" name="pubescence" src="d0_s7" value="villous" />
      </biological_entity>
      <biological_entity id="o4979" name="inside" name_original="inside" src="d0_s7" type="structure">
        <character is_modifier="true" name="position" src="d0_s7" value="proximal" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 1–1.5 mm;</text>
      <biological_entity id="o4980" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white, fading pinkish, 1.5–2.5 mm;</text>
      <biological_entity id="o4981" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="fading pinkish" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>episepalous filaments 1–2 mm, epipetalous filaments slightly shorter, anthers 0.4–0.9 mm;</text>
      <biological_entity constraint="episepalous" id="o4982" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="epipetalous" id="o4983" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="height_or_length_or_size" src="d0_s10" value="shorter" />
      </biological_entity>
      <biological_entity id="o4984" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 3–4.5 mm, villous near base, stigma 0.5–0.8 mm diam., surrounded by anthers at anthesis.</text>
      <biological_entity id="o4985" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
        <character constraint="near base" constraintid="o4986" is_modifier="false" name="pubescence" src="d0_s11" value="villous" />
      </biological_entity>
      <biological_entity id="o4986" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o4987" name="stigma" name_original="stigma" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4988" name="anther" name_original="anthers" src="d0_s11" type="structure" />
      <relation from="o4987" id="r641" name="surrounded by" negation="false" src="d0_s11" to="o4988" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules cylindrical and thickened proximally, spreading, straight to arcuate or weakly sigmoid, terete, thickened near base, tapering distally, 8–12 × 2–3 mm, regularly but tardily dehiscent.</text>
      <biological_entity id="o4989" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindrical" />
        <character is_modifier="false" modifier="proximally" name="size_or_width" src="d0_s12" value="thickened" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight to arcuate" />
        <character is_modifier="false" modifier="weakly" name="course_or_shape" src="d0_s12" value="sigmoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" />
        <character constraint="near base" constraintid="o4990" is_modifier="false" name="size_or_width" src="d0_s12" value="thickened" />
        <character is_modifier="false" modifier="distally" name="shape" notes="" src="d0_s12" value="tapering" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="regularly; tardily" name="dehiscence" src="d0_s12" value="dehiscent" />
      </biological_entity>
      <biological_entity id="o4990" name="base" name_original="base" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds monomorphic, light-brown, ca. 1 mm, finely reticulate.</text>
      <biological_entity id="o4991" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="light-brown" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="1" />
        <character is_modifier="false" modifier="finely" name="architecture_or_coloration_or_relief" src="d0_s13" value="reticulate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eremothera gouldii is known from Coconino and Mohave counties in Arizona and Washington County in Utah.</discussion>
  <discussion>P. H. Raven (1969) determined Eremothera gouldii to be self-compatible and autogamous.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Volcanic scree or cinder flats.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cinder flats" modifier="volcanic scree or" />
        <character name="habitat" value="volcanic" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>