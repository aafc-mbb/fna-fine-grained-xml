<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Endlicher" date="1830" rank="tribe">Epilobieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EPILOBIUM</taxon_name>
    <taxon_name authority="Hoch &amp; W. L. Wagner" date="2007" rank="section">Macrocarpa</taxon_name>
    <taxon_name authority="Haussknecht" date="1879" rank="species">rigidum</taxon_name>
    <place_of_publication>
      <publication_title>Oesterr. Bot. Z.</publication_title>
      <place_in_publication>29: 51.  1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section macrocarpa;species rigidum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epilobium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rigidum</taxon_name>
    <taxon_name authority="Trelease" date="unknown" rank="variety">canescens</taxon_name>
    <taxon_hierarchy>genus epilobium;species rigidum;variety canescens</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Stiff willowherb</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs from woody caudex forming hypogeal shoots with barklike periderm.</text>
      <biological_entity id="o3249" name="herb" name_original="herbs" src="d0_s0" type="structure" />
      <biological_entity id="o3250" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" />
      </biological_entity>
      <biological_entity id="o3251" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="hypogeal" />
      </biological_entity>
      <biological_entity id="o3252" name="periderm" name_original="periderm" src="d0_s0" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s0" value="barklike" />
      </biological_entity>
      <relation from="o3249" id="r537" name="from" negation="false" src="d0_s0" to="o3250" />
      <relation from="o3250" id="r538" name="forming" negation="false" src="d0_s0" to="o3251" />
      <relation from="o3249" id="r539" name="with" negation="false" src="d0_s0" to="o3252" />
    </statement>
    <statement id="d0_s1">
      <text>Stems several to many, suberect or ascending, terete, 10–40 cm, simple or sparsely branched, usually glabrous and ± glaucous proximal to inflorescence, strigillose distally, sometimes densely strigillose throughout.</text>
      <biological_entity id="o3253" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" notes="[duplicate value]" src="d0_s1" value="several" />
        <character is_modifier="false" name="quantity" notes="[duplicate value]" src="d0_s1" value="many" />
        <character char_type="range_value" from="several" name="quantity" src="d0_s1" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="suberect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="glaucous" />
        <character constraint="to inflorescence" constraintid="o3254" is_modifier="false" name="position" src="d0_s1" value="proximal" />
        <character is_modifier="false" modifier="distally" name="pubescence" notes="" src="d0_s1" value="strigillose" />
        <character is_modifier="false" modifier="sometimes densely; throughout" name="pubescence" src="d0_s1" value="strigillose" />
      </biological_entity>
      <biological_entity id="o3254" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves crowded distally, petiole 2–6 mm, blade narrowly ovate to ovate or broadly elliptic, often obovate in proximal pairs;</text>
      <biological_entity id="o3255" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s2" value="crowded" />
      </biological_entity>
      <biological_entity id="o3256" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3257" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s2" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s2" value="elliptic" />
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s2" to="ovate or broadly elliptic" />
        <character constraint="in proximal leaves" constraintid="o3258" is_modifier="false" modifier="often" name="shape" src="d0_s2" value="obovate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3258" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>cauline 2–4.5 × 0.8–2 cm, base rounded to attenuate, margins subentire or finely denticulate, 8–12 teeth per side, lateral-veins inconspicuous, 3–5 per side, apex obtuse proximally to subacute distally, surfaces glaucous and subglabrous to densely strigillose;</text>
      <biological_entity constraint="cauline" id="o3259" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3260" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="rounded" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="attenuate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="attenuate" />
      </biological_entity>
      <biological_entity id="o3261" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subentire" />
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s3" value="denticulate" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s3" to="12" />
      </biological_entity>
      <biological_entity id="o3262" name="tooth" name_original="teeth" src="d0_s3" type="structure" />
      <biological_entity id="o3263" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o3264" name="lateral-vein" name_original="lateral-veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" />
        <character char_type="range_value" constraint="per side" constraintid="o3265" from="3" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o3265" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o3266" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s3" value="obtuse" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="subacute" />
      </biological_entity>
      <biological_entity id="o3267" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s3" value="subglabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" notes="[duplicate value]" src="d0_s3" value="strigillose" />
        <character char_type="range_value" from="subglabrous" name="pubescence" src="d0_s3" to="densely strigillose" />
      </biological_entity>
      <relation from="o3262" id="r540" name="per" negation="false" src="d0_s3" to="o3263" />
    </statement>
    <statement id="d0_s4">
      <text>bracts narrower and much smaller.</text>
      <biological_entity id="o3268" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="narrower" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s4" value="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences erect racemes, simple, ± densely strigillose, rarely mixed sparsely glandular puberulent.</text>
      <biological_entity id="o3269" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="simple" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s5" value="strigillose" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s5" value="mixed" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" />
      </biological_entity>
      <biological_entity id="o3270" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o3271" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>buds 6–11 × 4–5 mm, apiculate;</text>
      <biological_entity id="o3272" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="11" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 4–8 mm;</text>
      <biological_entity id="o3273" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>floral-tube 1–1.8 × 2–3 mm, with raised ring of tissue edged with spreading hairs at mouth inside;</text>
      <biological_entity id="o3274" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3275" name="ring" name_original="ring" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="raised" />
        <character constraint="with hairs" constraintid="o3277" is_modifier="false" name="architecture" src="d0_s9" value="edged" />
      </biological_entity>
      <biological_entity id="o3276" name="tissue" name_original="tissue" src="d0_s9" type="structure" />
      <biological_entity id="o3277" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="spreading" />
      </biological_entity>
      <biological_entity id="o3278" name="mouth" name_original="mouth" src="d0_s9" type="structure" />
      <relation from="o3274" id="r541" name="with" negation="false" src="d0_s9" to="o3275" />
      <relation from="o3275" id="r542" name="part_of" negation="false" src="d0_s9" to="o3276" />
      <relation from="o3277" id="r543" name="at" negation="false" src="d0_s9" to="o3278" />
    </statement>
    <statement id="d0_s10">
      <text>sepals often reddish green, lanceolate, 9.5–14.5 × 2.5–3.5 mm, apex acuminate, abaxial surface densely strigillose;</text>
      <biological_entity id="o3279" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="reddish green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" />
        <character char_type="range_value" from="9.5" from_unit="mm" name="length" src="d0_s10" to="14.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3280" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3281" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals pink to rose-purple, obcordate, 16–20 × 13–16 mm, apical notch 3.4–5.5 mm;</text>
      <biological_entity id="o3282" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="rose-purple" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="rose-purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obcordate" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s11" to="20" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s11" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o3283" name="notch" name_original="notch" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s11" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments light pink, those of longer stamens 9–14 mm, those of shorter ones 6.5–10 mm;</text>
      <biological_entity id="o3284" name="filament" name_original="filaments" src="d0_s12" type="structure" constraint="stamen">
        <character is_modifier="false" name="coloration" src="d0_s12" value="light pink" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o3285" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity constraint="shorter" id="o3286" name="one" name_original="ones" src="d0_s12" type="structure" />
      <relation from="o3284" id="r544" name="part_of" negation="false" src="d0_s12" to="o3285" />
      <relation from="o3284" id="r545" name="part_of" negation="false" src="d0_s12" to="o3286" />
    </statement>
    <statement id="d0_s13">
      <text>anthers cream, 1.8–3.5 × 1–1.9 mm;</text>
      <biological_entity id="o3287" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="cream" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s13" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="1.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 6–12 mm, densely strigillose;</text>
      <biological_entity id="o3288" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s14" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style cream to light pink, 14.5–18.5 mm, stigma broadly 4-lobed, 1–1.5 × 3–3.5 mm, exserted beyond anthers.</text>
      <biological_entity id="o3289" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s15" value="cream" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s15" value="light pink" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s15" to="light pink" />
        <character char_type="range_value" from="14.5" from_unit="mm" name="some_measurement" src="d0_s15" to="18.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3290" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="4-lobed" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s15" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3291" name="anther" name_original="anthers" src="d0_s15" type="structure" />
      <relation from="o3290" id="r546" name="exserted beyond" negation="false" src="d0_s15" to="o3291" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules 20–35 mm, surfaces strigillose;</text>
      <biological_entity id="o3292" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s16" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3293" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 9–13 mm, bracts often attached 2–3 mm from base.</text>
      <biological_entity id="o3294" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s17" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3295" name="bract" name_original="bracts" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="often" name="fixation" src="d0_s17" value="attached" />
        <character char_type="range_value" constraint="from base" constraintid="o3296" from="2" from_unit="mm" name="location" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3296" name="base" name_original="base" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Seeds narrowly obovoid, constriction 0.6–0.8 mm from micropylar end, 2.5–3.4 × 0.9–1.4 mm, chalazal collar obscure, light-brown, surface papillose;</text>
      <biological_entity id="o3297" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="obovoid" />
      </biological_entity>
      <biological_entity id="o3298" name="constriction" name_original="constriction" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="from micropylar, end" constraintid="o3299, o3300" from="0.6" from_unit="mm" name="location" src="d0_s18" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" notes="" src="d0_s18" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" notes="" src="d0_s18" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3299" name="micropylar" name_original="micropylar" src="d0_s18" type="structure" />
      <biological_entity id="o3300" name="end" name_original="end" src="d0_s18" type="structure" />
      <biological_entity constraint="chalazal" id="o3301" name="collar" name_original="collar" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="obscure" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" />
      </biological_entity>
      <biological_entity id="o3302" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="false" name="relief" src="d0_s18" value="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>coma easily detached, white, 6–8 mm. 2n = 36.</text>
      <biological_entity id="o3303" name="coma" name_original="coma" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="easily" name="fusion" src="d0_s19" value="detached" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s19" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3304" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Within its range in northwestern California and southwestern Oregon, Epilobium rigidum is restricted to unusually dry habitats compared to most species in sect. Epilobium, but is not unlike taxa in the non-n = 18 clade, which are both perennial and annual.  It is self-compatible, but with strongly protandrous flowers and an exserted stigma, and is modally outcrossing, pollinated by bees and flies.</discussion>
  <discussion>Plant vestiture varies from subglabrous to densely strigillose throughout (var. canescens), but plants with these differences can be in the same population, and no other morphological differences between them have been found.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry rocky or sandy benches, rocky hillsides, dry streambeds in coniferous forests, on seasonally moist serpentine slopes, rarely along disturbed roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry rocky" />
        <character name="habitat" value="sandy benches" />
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="dry streambeds" constraint="in coniferous forests" />
        <character name="habitat" value="coniferous forests" />
        <character name="habitat" value="moist serpentine slopes" modifier="seasonally" />
        <character name="habitat" value="disturbed roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1200(–1500) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>