<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MELASTOMATACEAE</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">RHEXIA</taxon_name>
    <taxon_name authority="Pennell" date="1918" rank="species">interior</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>45: 480.  1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family melastomataceae;genus rhexia;species interior</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhexia</taxon_name>
    <taxon_name authority="Bush" date="1911" rank="species">latifolia</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>13: 167.  1911</place_in_publication>
      <other_info_on_pub>not Aublet 1775</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus rhexia;species latifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">mariana</taxon_name>
    <taxon_name authority="(Pennell) Kral &amp; Bostick" date="unknown" rank="variety">interior</taxon_name>
    <taxon_hierarchy>genus r.;species mariana;variety interior</taxon_hierarchy>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Ozark meadow beauty</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices not developed;</text>
      <biological_entity id="o2571" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="development" src="d0_s0" value="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots often long and rhizomelike, lignes­cent, non-tuberiferous.</text>
      <biological_entity id="o2572" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="length_or_size" src="d0_s1" value="long" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomelike" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="non-tuberiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unbranched or few to several-branched distally, 40–60 cm, faces subequal, angles sharp, without wings or very narrowly winged, internodes and nodes hirsute-villous, hairs gland-tipped.</text>
      <biological_entity id="o2573" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="few" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="several-branched" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2574" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="subequal" />
      </biological_entity>
      <biological_entity id="o2575" name="angle" name_original="angles" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="sharp" />
      </biological_entity>
      <biological_entity id="o2576" name="wing" name_original="wings" src="d0_s2" type="structure" />
      <biological_entity id="o2577" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="very narrowly" name="architecture" src="d0_s2" value="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute-villous" />
      </biological_entity>
      <biological_entity id="o2578" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="very narrowly" name="architecture" src="d0_s2" value="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute-villous" />
      </biological_entity>
      <biological_entity id="o2579" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="gland-tipped" />
      </biological_entity>
      <relation from="o2575" id="r513" name="without" negation="false" src="d0_s2" to="o2576" />
      <relation from="o2575" id="r514" name="without" negation="false" src="d0_s2" to="o2577" />
      <relation from="o2575" id="r515" name="without" negation="false" src="d0_s2" to="o2578" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile, subsessile, or petiole 0.5–1.5 mm;</text>
      <biological_entity id="o2580" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" />
      </biological_entity>
      <biological_entity id="o2581" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly to broadly elliptic, 2–6 (–7) cm × 10–25 mm, margins serrate to serrulate, surfaces loosely strigose to strigose-hirsute or villous.</text>
      <biological_entity id="o2582" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="arrangement_or_shape" src="d0_s4" value="elliptic" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2583" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" notes="[duplicate value]" src="d0_s4" value="serrate" />
        <character is_modifier="false" name="architecture_or_shape" notes="[duplicate value]" src="d0_s4" value="serrulate" />
        <character char_type="range_value" from="serrate" name="architecture_or_shape" src="d0_s4" to="serrulate" />
      </biological_entity>
      <biological_entity id="o2584" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" notes="[duplicate value]" src="d0_s4" value="strigose" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s4" value="strigose-hirsute" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s4" value="villous" />
        <character char_type="range_value" from="loosely strigose" name="pubescence" src="d0_s4" to="strigose-hirsute or villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences diffuse, not obscured by bracts.</text>
      <biological_entity id="o2585" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="density" src="d0_s5" value="diffuse" />
        <character constraint="by bracts" constraintid="o2586" is_modifier="false" modifier="not" name="prominence" src="d0_s5" value="obscured" />
      </biological_entity>
      <biological_entity id="o2586" name="bract" name_original="bracts" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium ovoid to subglobose, about as long as the constricted neck, 6–10 mm, hirsute-villous, glabrous, or glabrate, hairs gland-tipped;</text>
      <biological_entity id="o2587" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2588" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="subglobose" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="subglobose" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hirsute-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" />
      </biological_entity>
      <biological_entity id="o2589" name="neck" name_original="neck" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="constricted" />
      </biological_entity>
      <biological_entity id="o2590" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" />
      </biological_entity>
      <relation from="o2588" id="r516" name="as long as" negation="false" src="d0_s6" to="o2589" />
    </statement>
    <statement id="d0_s7">
      <text>calyx lobes narrowly triangular, apices acute to acuminate, narrowed to linear-oblong extensions;</text>
      <biological_entity id="o2591" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o2592" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="triangular" />
      </biological_entity>
      <biological_entity id="o2593" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="acuminate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="narrowed" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="linear-oblong" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate narrowed" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="acuminate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="narrowed" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="linear-oblong" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate narrowed" />
      </biological_entity>
      <biological_entity id="o2594" name="extension" name_original="extensions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>petals spreading, bright lavender-rose, 1.2–1.5 cm;</text>
      <biological_entity id="o2595" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2596" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright lavender-rose" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers curved, 5–8 mm.</text>
      <biological_entity id="o2597" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2598" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="curved" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 0.5–0.7 mm, surfaces irregularly ridged in concentric rows or with laterally flattened, domelike processes.</text>
      <biological_entity id="o2599" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2601" name="process" name_original="processes" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="laterally" name="shape" src="d0_s10" value="flattened" />
        <character is_modifier="true" name="shape" src="d0_s10" value="domelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 44.</text>
      <biological_entity id="o2600" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character constraint="in concentric rows or with processes" constraintid="o2601" is_modifier="false" modifier="irregularly" name="shape" src="d0_s10" value="ridged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2602" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet areas, ditches, prairies.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet areas" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ill., Ind., Kans., Ky., La., Miss., Mo., Okla., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>