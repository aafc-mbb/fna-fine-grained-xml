<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="A. Jussieu" date="1832" rank="genus">GAYOPHYTUM</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1840" rank="species">ramosissimum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 513.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus gayophytum;species ramosissimum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gayophytum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ramosissimum</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="variety">deflexum</taxon_name>
    <taxon_hierarchy>genus gayophytum;species ramosissimum;variety deflexum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ramosissimum</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">obtusum</taxon_name>
    <taxon_hierarchy>genus g.;species ramosissimum;variety obtusum</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs glabrous or sparsely strigillose distally.</text>
      <biological_entity id="o806" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s0" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, profusely branched throughout, usually at every other node, branching dichotomous except near base, 10–50 cm.</text>
      <biological_entity id="o807" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="profusely; throughout" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="branching" />
        <character constraint="except base" constraintid="o809" is_modifier="false" name="architecture" src="d0_s1" value="dichotomous" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o808" name="node" name_original="node" src="d0_s1" type="structure" />
      <biological_entity id="o809" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o807" id="r107" modifier="usually" name="at" negation="false" src="d0_s1" to="o808" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves reduced distally, 10–40 × 1–5 mm;</text>
      <biological_entity id="o810" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s2" value="reduced" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0–3 (–10) mm;</text>
      <biological_entity id="o811" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade very narrowly lanceolate to sublinear.</text>
    </statement>
    <statement id="d0_s5">
      <text>Inflores­cences with flowers arising as proximally as first 5–15 nodes from base.</text>
      <biological_entity id="o812" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="very narrowly" name="shape" notes="[duplicate value]" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="sublinear" />
        <character char_type="range_value" from="very narrowly lanceolate" name="shape" src="d0_s4" to="sublinear" />
      </biological_entity>
      <biological_entity id="o813" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="as-proximally-as nodes" constraintid="o814" is_modifier="false" name="orientation" src="d0_s5" value="arising" />
      </biological_entity>
      <biological_entity id="o814" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="15" />
      </biological_entity>
      <biological_entity id="o815" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o812" id="r108" name="with" negation="false" src="d0_s5" to="o813" />
      <relation from="o814" id="r109" name="from" negation="false" src="d0_s5" to="o815" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 0.4–0.8 mm, reflexed singly;</text>
      <biological_entity id="o816" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o817" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s6" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 0.7–1.2 (–1.5) mm;</text>
      <biological_entity id="o818" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o819" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pollen 90–100% fertile;</text>
      <biological_entity id="o820" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o821" name="pollen" name_original="pollen" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="90-100%" name="reproduction" src="d0_s8" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigma subglobose, surrounded by anthers at anthesis.</text>
      <biological_entity id="o822" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o823" name="stigma" name_original="stigma" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobose" />
      </biological_entity>
      <biological_entity id="o824" name="anther" name_original="anthers" src="d0_s9" type="structure" />
      <relation from="o823" id="r110" name="surrounded by" negation="false" src="d0_s9" to="o824" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules ascending to reflexed, subterete, 3–9 × 0.8–1.2 mm, with inconspicuous constrictions between seeds, valve margins entire or weakly undulate, all valves free from septum after dehiscence, septum straight;</text>
      <biological_entity id="o825" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s10" value="ascending" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s10" value="reflexed" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subterete" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="between seeds" constraintid="o827" id="o826" name="constriction" name_original="constrictions" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="inconspicuous" />
      </biological_entity>
      <biological_entity id="o827" name="seed" name_original="seeds" src="d0_s10" type="structure" />
      <biological_entity constraint="valve" id="o828" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="entire" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s10" value="undulate" />
      </biological_entity>
      <biological_entity id="o829" name="valve" name_original="valves" src="d0_s10" type="structure">
        <character constraint="from septum" constraintid="o830" is_modifier="false" name="fusion" src="d0_s10" value="free" />
      </biological_entity>
      <biological_entity id="o830" name="septum" name_original="septum" src="d0_s10" type="structure" />
      <biological_entity id="o831" name="septum" name_original="septum" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" />
      </biological_entity>
      <relation from="o825" id="r111" name="with" negation="false" src="d0_s10" to="o826" />
    </statement>
    <statement id="d0_s11">
      <text>pedicel (3–) 5–12 mm.</text>
      <biological_entity id="o832" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 10–30, all developing, arranged ± parallel to septum and in alternating pattern between locules, crowded, overlapping, often appearing to form 2 irregular rows in each locule, brown or gray mottled with brown, 1–1.5 × 0.5–0.7 mm, glabrous.</text>
      <biological_entity id="o834" name="septum" name_original="septum" src="d0_s12" type="structure" />
      <biological_entity constraint="between locules" constraintid="o836" id="o835" name="pattern" name_original="pattern" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="alternating" />
      </biological_entity>
      <biological_entity id="o836" name="locule" name_original="locules" src="d0_s12" type="structure" />
      <biological_entity id="o837" name="row" name_original="rows" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s12" value="irregular" />
      </biological_entity>
      <biological_entity id="o838" name="locule" name_original="locule" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s12" value="irregular" />
      </biological_entity>
      <relation from="o833" id="r112" name="in" negation="false" src="d0_s12" to="o835" />
      <relation from="o833" id="r113" modifier="often" name="appearing to form" negation="false" src="d0_s12" to="o837" />
      <relation from="o833" id="r114" modifier="often" name="appearing to form" negation="false" src="d0_s12" to="o838" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 14.</text>
      <biological_entity id="o833" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="30" />
        <character is_modifier="false" name="development" src="d0_s12" value="developing" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="arranged" />
        <character constraint="to septum" constraintid="o834" is_modifier="false" modifier="more or less" name="arrangement" src="d0_s12" value="parallel" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s12" value="crowded" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="overlapping" />
        <character is_modifier="false" modifier="often; often" name="coloration" src="d0_s12" value="brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="gray mottled with brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s12" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o839" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Gayophytum ramosissimum is similar to some individuals of G. diffusum subsp. parviflorum and has probably contributed to the extensive variation of the G. diffusum tetraploid complex.  Gayophytum ramosissimum is most readily distinguished from G. diffusum subsp. parviflorum by its capsule being shorter than the pedicel.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush communities.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="communities" modifier="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–3000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>