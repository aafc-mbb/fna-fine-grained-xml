<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">CHYLISMIA</taxon_name>
    <taxon_name authority="(P. H. Raven) W. L. Wagner &amp; Hoch" date="2007" rank="section">Lignothera</taxon_name>
    <taxon_name authority="(Torrey) Small" date="1896" rank="species">cardiophylla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">cardiophylla</taxon_name>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus chylismia;section lignothera;species cardiophylla;subspecies cardiophylla</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">cardiophylla</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="variety">petiolaris</taxon_name>
    <taxon_hierarchy>genus oenothera;species cardiophylla;variety petiolaris</taxon_hierarchy>
  </taxon_identification>
  <number>15a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs villous, sometimes also glandular puberulent.</text>
      <biological_entity id="o1393" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: floral-tube 4.5–12 mm;</text>
      <biological_entity id="o1394" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o1395" name="floral-tube" name_original="floral-tube" src="d0_s1" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s1" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petals 3–12 mm;</text>
      <biological_entity id="o1396" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o1397" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>style 8–23 mm, stigma often exserted beyond anthers at anthesis.</text>
      <biological_entity id="o1398" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o1399" name="style" name_original="style" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1401" name="anther" name_original="anthers" src="d0_s3" type="structure" />
      <relation from="o1400" id="r292" modifier="often" name="exserted beyond" negation="false" src="d0_s3" to="o1401" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 14.</text>
      <biological_entity id="o1400" name="stigma" name_original="stigma" src="d0_s3" type="structure" />
      <biological_entity constraint="2n" id="o1402" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies cardiophylla is known from southern San Bernardino County south to eastern San Diego County, California, and Yuma County (and possibly western Pinal County), Arizona, and south in northeastern and central Baja California, Mexico; it is also found on Isla Ángel de la Guarda, Isla San Esteban, Isla San Luis, Isla San Marcos, and Isla San Pedro Mártir in the Gulf of California.  Isla San Esteban is the only locality for this subspecies in Sonora.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky walls, sandy alluvial flats, with Ambrosia dumosa, Hyptis emoryi, and Larrea tridentata.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky walls" />
        <character name="habitat" value="sandy alluvial flats" constraint="with ambrosia dumosa , hyptis emoryi , and larrea tridentata" />
        <character name="habitat" value="ambrosia dumosa" />
        <character name="habitat" value="hyptis emoryi" />
        <character name="habitat" value="larrea tridentata" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>