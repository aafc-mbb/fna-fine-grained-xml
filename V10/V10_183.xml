<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">HALORAGACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MYRIOPHYLLUM</taxon_name>
    <taxon_name authority="Bigelow" date="1824" rank="species">tenellum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Boston. ed.</publication_title>
      <place_in_publication>2, 346.  1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family haloragaceae;genus myriophyllum;species tenellum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Dwarf or slender water-milfoil</other_name>
  <other_name type="common_name">myriophylle grêle</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs monoecious, aquatic or semiaquatic, often forming dense mats.</text>
      <biological_entity id="o2655" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="aquatic" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="semiaquatic" />
      </biological_entity>
      <biological_entity id="o2656" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" />
      </biological_entity>
      <relation from="o2655" id="r591" modifier="often" name="forming" negation="false" src="d0_s0" to="o2656" />
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched or 1-branched, to 0.7 m.</text>
      <biological_entity id="o2657" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-branched" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="0.7" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Turions absent.</text>
      <biological_entity id="o2658" name="turion" name_original="turions" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate, homo­morphic, scalelike;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
    </statement>
    <statement id="d0_s5">
      <text>sub­mersed leaves ovate, 0.3–1 (–1.5) × 0.1–0.7 mm, margins entire;</text>
      <biological_entity id="o2659" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="morphic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="scalelike" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
      </biological_entity>
      <biological_entity id="o2660" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o2661" name="whole_organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" />
      </biological_entity>
      <biological_entity id="o2662" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" />
      </biological_entity>
      <relation from="o2659" id="r592" name="sub ­ mersed" negation="false" src="d0_s5" to="o2660" />
    </statement>
    <statement id="d0_s6">
      <text>emersed leaves ovate to obovate, (0.5–) 0.8–2.5 (–3.3) × (0.2–) 0.3–1.2 (–3) mm, margins entire.</text>
      <biological_entity id="o2663" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="location" src="d0_s6" value="emersed" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="obovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_length" src="d0_s6" to="0.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s6" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_width" src="d0_s6" to="0.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s6" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2664" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences to 9 cm;</text>
      <biological_entity id="o2665" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>flowers proximally pistillate, medially bisexual, distally staminate;</text>
      <biological_entity id="o2666" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s8" value="pistillate" />
        <character is_modifier="false" modifier="medially" name="reproduction" src="d0_s8" value="bisexual" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s8" value="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles cream, narrowly elliptic to ovate, (0.4–) 0.5–0.8 (–1) × (0.1–) 0.2–0.4 (–0.5) mm, margins usually entire, sometimes serrate.</text>
      <biological_entity id="o2667" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="cream" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s9" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s9" value="ovate" />
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="atypical_length" src="d0_s9" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s9" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="atypical_width" src="d0_s9" to="0.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s9" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2668" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s9" value="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s9" value="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers: sepals green to cream, lanceolate, 0.1–0.5 × 0.1–0.2 mm;</text>
      <biological_entity id="o2669" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" />
      </biological_entity>
      <biological_entity id="o2670" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="green" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="cream" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="cream" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s10" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s10" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals persistent, cream to pink, obovate, (0.6–) 1–2 (–2.3) × (0.3–) 0.6–1.4 (–1.6) mm;</text>
      <biological_entity id="o2671" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" />
      </biological_entity>
      <biological_entity id="o2672" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="cream" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="pink" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="atypical_length" src="d0_s11" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_width" src="d0_s11" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s11" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, filaments to 1.2 mm, anthers yellow, narrowly elliptic, (0.3–) 0.7–1.6 × 0.1–0.5 (–0.7) mm.</text>
      <biological_entity id="o2673" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" />
      </biological_entity>
      <biological_entity id="o2674" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" />
      </biological_entity>
      <biological_entity id="o2675" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2676" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s12" value="elliptic" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_length" src="d0_s12" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s12" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: sepals green to cream, lanceolate, 0.1–0.6 (–1) × 0.1–0.2 (–0.6) mm;</text>
      <biological_entity id="o2677" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" />
      </biological_entity>
      <biological_entity id="o2678" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s13" value="green" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s13" value="cream" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s13" to="cream" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s13" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s13" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s13" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals often persistent, cream to pink, obovate, 0.5–1.7 (–2.2) × (0.2–) 0.4–1.2 (–1.5) mm;</text>
      <biological_entity id="o2679" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" />
      </biological_entity>
      <biological_entity id="o2680" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="often" name="duration" src="d0_s14" value="persistent" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s14" value="cream" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s14" value="pink" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s14" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovate" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s14" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_width" src="d0_s14" to="0.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s14" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistils to 1.3 mm, stigmas white, to 0.7 mm.</text>
      <biological_entity id="o2681" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" />
      </biological_entity>
      <biological_entity id="o2682" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2683" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits globose, distinctly 4-lobed.</text>
      <biological_entity id="o2684" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s16" value="4-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Mericarps olive-brown, cylindric to ± globose, 0.6–1.3 × (0.2–) 0.4–1.4 mm, transversely ovate, abaxial surface rounded, smooth or minutely papillate, rarely with a single obscure, longi­tudinal ridge, wings absent.</text>
      <biological_entity id="o2685" name="mericarp" name_original="mericarps" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="olive-brown" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s17" value="cylindric" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="[duplicate value]" src="d0_s17" value="globose" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s17" to="more or less globose" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s17" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_width" src="d0_s17" to="0.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s17" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s17" value="ovate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2686" name="surface" name_original="surface" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="rounded" />
        <character is_modifier="false" name="relief" src="d0_s17" value="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s17" value="papillate" />
      </biological_entity>
      <biological_entity id="o2687" name="ridge" name_original="ridge" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="single" />
        <character is_modifier="true" name="prominence" src="d0_s17" value="obscure" />
      </biological_entity>
      <relation from="o2686" id="r593" modifier="rarely" name="with" negation="false" src="d0_s17" to="o2687" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 14.</text>
      <biological_entity id="o2688" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2689" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Myriophyllum tenellum is easily recognized; it is the only water-milfoil in the flora area with homomorphic scalelike leaves whether it is growing submersed or as a shoreline emergent.  It is often associated with acidic to circumneutral waters.  E. G. Voss (1972–1996, vol. 2) noted that it tends to be overlooked.  Although it has been recently recorded from Indiana (R. W. Scribailo and M. S. Alix 2006), we have seen no specimens from Illinois or Ohio.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oligotrophic to mesotrophic waters, lakes, sandy substrates.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oligotrophic to mesotrophic waters" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="sandy substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que.; Conn., Ind., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Pa., R.I., Vt., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>