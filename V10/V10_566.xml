<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Link" date="1818" rank="genus">CAMISSONIA</taxon_name>
    <taxon_name authority="(Munz) P. H. Raven" date="1964" rank="species">kernensis</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>16: 284.  1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus camissonia;species kernensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Munz" date="1931" rank="species">kernensis</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>18: 737.  1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species kernensis</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs sparsely or densely villous and glandular puberulent, espe­cially distally, sometimes glabrate or sparsely glandular puberulent.</text>
      <biological_entity id="o4332" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="villous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
        <character is_modifier="false" modifier="cially distally; distally; sometimes" name="pubescence" src="d0_s0" value="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, often many-branched, 5–30 cm.</text>
      <biological_entity id="o4333" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="many-branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximalmost some­times clustered near base;</text>
      <biological_entity id="o4334" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximalmost" id="o4335" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="near base" constraintid="o4336" is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" />
      </biological_entity>
      <biological_entity id="o4336" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade usually very narrowly elliptic to narrowly so, rarely lanceolate, 1–3.8 (–5.5) × 0.2–0.5 cm, base narrowly cuneate, margins sparsely serrate, apex acuminate.</text>
      <biological_entity id="o4337" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4338" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually very narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" />
        <character is_modifier="false" modifier="narrowly; rarely" name="shape" src="d0_s3" value="lanceolate" />
        <character char_type="range_value" from="3.8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="3.8" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4339" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="cuneate" />
      </biological_entity>
      <biological_entity id="o4340" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s3" value="serrate" />
      </biological_entity>
      <biological_entity id="o4341" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers opening near sun­rise;</text>
      <biological_entity id="o4342" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o4343" name="sun" name_original="sun" src="d0_s4" type="structure" />
      <relation from="o4342" id="r555" name="opening near" negation="false" src="d0_s4" to="o4343" />
    </statement>
    <statement id="d0_s5">
      <text>floral-tube 2.2–3.8 (–5.5) mm, villous inside;</text>
      <biological_entity id="o4344" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s5" to="3.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals 5–9 (–11) mm, reflexed separately;</text>
      <biological_entity id="o4345" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="separately" name="orientation" src="d0_s6" value="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 8–15 (–18) mm, each with 2 large red dots basally;</text>
    </statement>
    <statement id="d0_s8">
      <text>episepalous fila­ments 3.5–5.5 (–7) mm, epipetalous filaments 1.3–2 (–4.5) mm, anthers 1.8–2 (–3) mm, pollen with less than 5% of grains 4-pored or 5-pored;</text>
      <biological_entity id="o4346" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="18" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character modifier="with" name="quantity" src="d0_s7" value="2" />
        <character is_modifier="false" name="size" src="d0_s7" value="large" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s7" value="red dots" />
        <character is_modifier="false" name="position" src="d0_s8" value="episepalous" />
      </biological_entity>
      <biological_entity id="o4347" name="whole_organism" name_original="" src="d0_s8" type="structure">
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="epipetalous" id="o4348" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4349" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4350" name="pollen" name_original="pollen" src="d0_s8" type="structure" />
      <biological_entity id="o4351" name="grain" name_original="grains" src="d0_s8" type="structure" />
      <relation from="o4350" id="r556" modifier="0-5%" name="part_of" negation="false" src="d0_s8" to="o4351" />
    </statement>
    <statement id="d0_s9">
      <text>style 7–10 (–14) mm, stig­ma well exserted beyond anthers at anthesis.</text>
      <biological_entity id="o4352" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="14" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4353" name="anther" name_original="anthers" src="d0_s9" type="structure" />
      <relation from="o4352" id="r557" modifier="well" name="exserted beyond" negation="false" src="d0_s9" to="o4353" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules 22–37 × 1.5–1.7 mm;</text>
      <biological_entity id="o4354" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="22" from_unit="mm" name="length" src="d0_s10" to="37" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicel 0–15 mm.</text>
      <biological_entity id="o4355" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 1.1–1.2 × 0.5–0.6 mm.</text>
      <biological_entity id="o4356" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s12" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Camissonia kernensis occurs in sagebrush scrub, and Joshua-tree and pinyon-juniper woodlands at eleva­tions of 700–1900 m in southern and central California and southern Nevada.  The species is self-incompatible and is apparently pollinated by oligolectic bees of Andrena subg. Onagrandrena (P. H. Raven 1969); Raven subdivided the species into two intergrading subspecies.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs compact, sparsely glandular puberulent and villous throughout; leaves: proximalmost often clustered near base; pedicels 3–15 mm in fruit.</description>
      <determination>1a. Camissonia kernensis subsp. kernensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs with open habit, glandular puberulent throughout, usually also sparsely villous, or glabrate, with few glandular hairs; leaves: none clustered near base; pedicels 0–5(–15) mm in fruit.</description>
      <determination>1b. Camissonia kernensis subsp. gilmanii</determination>
    </key_statement>
  </key>
</bio:treatment>