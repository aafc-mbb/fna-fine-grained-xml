<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">AMMANNIA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">latifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 119.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus ammannia;species latifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ammannia</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">koehnei</taxon_name>
    <taxon_hierarchy>genus ammannia;species koehnei</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">koehnei</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">exauriculata</taxon_name>
    <taxon_hierarchy>genus a.;species koehnei;variety exauriculata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">teres</taxon_name>
    <taxon_name authority="(Fernald) Fernald" date="unknown" rank="variety">exauriculata</taxon_name>
    <taxon_hierarchy>genus a.;species teres;variety exauriculata</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, robust, 4–10 dm.</text>
      <biological_entity id="o3152" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sparsely branched, or branched proximally.</text>
      <biological_entity id="o3153" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades lanceolate-linear to oblong, elliptic, or spatulate, 15–70 (–100) × 4–15 (–21) mm.</text>
      <biological_entity id="o3154" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate-linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="oblong" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="spatulate" />
        <character char_type="range_value" from="lanceolate-linear" name="shape" src="d0_s2" to="oblong elliptic or spatulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate-linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="oblong" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="spatulate" />
        <character char_type="range_value" from="lanceolate-linear" name="shape" src="d0_s2" to="oblong elliptic or spatulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate-linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="oblong" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="spatulate" />
        <character char_type="range_value" from="lanceolate-linear" name="shape" src="d0_s2" to="oblong elliptic or spatulate" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="100" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="70" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="21" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences simple cymes, (1–) 3–10-flowered mid-stem;</text>
      <biological_entity id="o3155" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o3156" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="simple" />
      </biological_entity>
      <biological_entity id="o3157" name="mid-stem" name_original="mid-stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="(1-)3-10-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>peduncle stout, 3 mm.</text>
      <biological_entity id="o3158" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" />
        <character name="some_measurement" src="d0_s4" unit="mm" value="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0–1 mm.</text>
      <biological_entity id="o3159" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Floral-tube urceolate, 3–4 mm;</text>
      <biological_entity id="o3160" name="floral-tube" name_original="floral-tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="urceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx segments as long as or slightly longer than sepals;</text>
      <biological_entity constraint="epicalyx" id="o3161" name="segment" name_original="segments" src="d0_s7" type="structure" />
      <biological_entity id="o3163" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s7" value="longer" />
      </biological_entity>
      <biological_entity id="o3162" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s7" value="longer" />
      </biological_entity>
      <relation from="o3161" id="r378" name="as long as" negation="false" src="d0_s7" to="o3163" />
    </statement>
    <statement id="d0_s8">
      <text>sepals 4 (–6), shallowly deltate, with mucronate apex;</text>
      <biological_entity id="o3164" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="6" />
        <character name="quantity" src="d0_s8" value="4" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s8" value="deltate" />
      </biological_entity>
      <biological_entity id="o3165" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="mucronate" />
      </biological_entity>
      <relation from="o3164" id="r379" name="with" negation="false" src="d0_s8" to="o3165" />
    </statement>
    <statement id="d0_s9">
      <text>petals 0–4 (–6), pale-pink to white, 1 × 1 mm;</text>
      <biological_entity id="o3166" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="6" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="4" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s9" value="pale-pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s9" value="white" />
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s9" to="white" />
        <character name="length" src="d0_s9" unit="mm" value="1" />
        <character name="width" src="d0_s9" unit="mm" value="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 4 (–8);</text>
      <biological_entity id="o3167" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="8" />
        <character name="quantity" src="d0_s10" value="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow;</text>
      <biological_entity id="o3168" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style stout, included, ca. 0.5 mm;</text>
      <biological_entity id="o3169" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s12" value="stout" />
        <character is_modifier="false" name="position" src="d0_s12" value="included" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma capitate.</text>
      <biological_entity id="o3170" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules 3.5–5.5 mm diam., included to scarcely surpassing sepals, splitting irregularly.</text>
      <biological_entity id="o3172" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <relation from="o3171" id="r380" modifier="scarcely" name="surpassing" negation="false" src="d0_s14" to="o3172" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 48.</text>
      <biological_entity id="o3171" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s14" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="included" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_dehiscence" src="d0_s14" value="splitting" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3173" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ammannia latifolia flowers spring through fall in the northern part of the range; year-round from southern Florida southward.</discussion>
  <discussion>Petals in Ammannia latifolia are consistently present from the Gulf Coast and from Georgia northward along the Atlantic Coast, and are commonly reduced in number or absent along the southern Atlantic and Gulf coasts and southward.  Presence of petals is best determined in the bud.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal plains in brackish to freshwater marshes, ditches.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal plains" constraint="in brackish to freshwater marshes , ditches" />
        <character name="habitat" value="brackish" />
        <character name="habitat" value="freshwater marshes" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., La., Md., Miss., N.J., N.C., S.C., Tex., Va.; Mexico (Yucatán); West Indies; Central America (Panama); South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico (Yucatán)" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America (Panama)" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>