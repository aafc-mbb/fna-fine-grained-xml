<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) Walpers" date="1843" rank="section">Megapterium</taxon_name>
    <taxon_name authority="Nuttall" date="1813" rank="species">macrocarpa</taxon_name>
    <taxon_name authority="(S. Watson) W. L. Wagner" date="1983" rank="subspecies">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>70: 194. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section megapterium;species macrocarpa;subspecies fremontii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="S. Watson" date="1873" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 587.  1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species fremontii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Megapterium</taxon_name>
    <taxon_name authority="(S. Watson) Britton" date="unknown" rank="species">fremontii</taxon_name>
    <taxon_hierarchy>genus megapterium;species fremontii</taxon_hierarchy>
  </taxon_identification>
  <number>26d.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs densely strigillose.</text>
      <biological_entity id="o4862" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems numerous, with numerous short secondary branches, 3–30 cm.</text>
      <biological_entity id="o4863" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="numerous" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o4864" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="numerous" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" />
      </biological_entity>
      <relation from="o4863" id="r956" name="with" negation="false" src="d0_s1" to="o4864" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves gray, (2.8–) 3.7–11 × 0.1–0.6 (–1.5) cm;</text>
      <biological_entity id="o4865" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="atypical_length" src="d0_s2" to="3.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.7" from_unit="cm" name="length" src="d0_s2" to="11" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s2" to="0.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear to narrowly elliptic to narrowly elliptic-lanceolate or narrowly oblanceolate, margins flat, entire or inconspicuously denticulate, apex acute.</text>
      <biological_entity id="o4866" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic-lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s3" to="narrowly elliptic-lanceolate or narrowly oblanceolate" />
      </biological_entity>
      <biological_entity id="o4867" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" />
        <character is_modifier="false" modifier="inconspicuously" name="shape" src="d0_s3" value="denticulate" />
      </biological_entity>
      <biological_entity id="o4868" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: buds with unequal free tips 1–2 (–5) mm;</text>
      <biological_entity id="o4869" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o4870" name="bud" name_original="buds" src="d0_s4" type="structure" />
      <biological_entity id="o4871" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="unequal" />
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o4870" id="r957" name="with" negation="false" src="d0_s4" to="o4871" />
    </statement>
    <statement id="d0_s5">
      <text>floral-tube (21–) 35–65 (–80) mm;</text>
      <biological_entity id="o4872" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o4873" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="21" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="65" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s5" to="65" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals (20–) 25–30 (–37) mm;</text>
      <biological_entity id="o4874" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4875" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="37" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals (17–) 25–33 (–37) mm;</text>
      <biological_entity id="o4876" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4877" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="33" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="37" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="33" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 13–18 mm, anthers 10–12 mm;</text>
      <biological_entity id="o4878" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4879" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4880" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style (45–) 55–80 (–98) mm.</text>
      <biological_entity id="o4881" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4882" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="45" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="55" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="98" to_unit="mm" />
        <character char_type="range_value" from="55" from_unit="mm" name="some_measurement" src="d0_s9" to="80" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules ellip­soid to narrowly ellipsoid, often twisted, wings 2–5 (–9) mm wide, body 13–30 (–65) × 2–6 mm. 2n = 14.</text>
      <biological_entity id="o4883" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="ellipsoid" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s10" value="twisted" />
      </biological_entity>
      <biological_entity id="o4884" name="wing" name_original="wings" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="width" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4885" name="body" name_original="body" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="65" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s10" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4886" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies fremontii occurs from Franklin and Webster counties in south-central Nebraska south into Kansas to Ellsworth, Hodgeman, and Logan counties; also with disjunct locations in Antelope and Cedar counties in northeastern Nebraska, and Barber County in south-central Kansas.  Some specimens from the eastern part of the range, where subsp. fremontii and subsp. macrocarpa are sympatric, appear intermediate between the two subspecies and are difficult to assign.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soil derived from fine-textured sandstone, shale or chalk on rocky hillsides, bluffs, badlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soil" />
        <character name="habitat" value="fine-textured sandstone" />
        <character name="habitat" value="shale" />
        <character name="habitat" value="chalk" />
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans., Nebr.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>