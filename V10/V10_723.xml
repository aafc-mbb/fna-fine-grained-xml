<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="L’Heritier" date="1792" rank="genus">EUCALYPTUS</taxon_name>
    <taxon_name authority="Labillardière" date="1806" rank="species">viminalis</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Holl. Pl.</publication_title>
      <place_in_publication>2: 12, plate 151.  1806</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus eucalyptus;species viminalis</taxon_hierarchy>
  </taxon_identification>
  <number>15.</number>
  <other_name type="common_name">Manna or ribbon gum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 50 m;</text>
      <biological_entity id="o3166" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="50" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk whitish, gray, or tan, straight, ± smooth;</text>
      <biological_entity id="o3167" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark usually shed in relatively long, narrow, irregular strips distally, sometimes persistent toward trunk base.</text>
      <biological_entity id="o3168" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character constraint="toward trunk base" constraintid="o3170" is_modifier="false" modifier="sometimes" name="duration" notes="" src="d0_s2" value="persistent" />
      </biological_entity>
      <biological_entity id="o3169" name="strip" name_original="strips" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="relatively" name="length_or_size" src="d0_s2" value="long" />
        <character is_modifier="true" name="size_or_width" src="d0_s2" value="narrow" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s2" value="irregular" />
      </biological_entity>
      <biological_entity constraint="trunk" id="o3170" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o3168" id="r566" name="shed in" negation="false" src="d0_s2" to="o3169" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 1–1.5 cm;</text>
      <biological_entity id="o3171" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3172" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade green, lanceolate to narrowly lan­ceolate, 10–15 × 1–2.5 cm.</text>
      <biological_entity id="o3173" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3174" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" modifier="narrowly" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="narrowly" name="width" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles to ca. 1 cm.</text>
      <biological_entity id="o3175" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences usually 3-flowered, umbels.</text>
      <biological_entity id="o3176" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="3-flowered" />
      </biological_entity>
      <biological_entity id="o3177" name="umbel" name_original="umbels" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium ovoid, 2–3 mm, length ± equaling calyptra;</text>
      <biological_entity id="o3178" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3179" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3180" name="calyptra" name_original="calyptra" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calyptra conic to rounded or, rarely, rostrate;</text>
      <biological_entity id="o3181" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3182" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="conic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="rounded" />
        <character char_type="range_value" from="conic" name="shape" src="d0_s8" to="rounded" />
        <character name="shape" src="d0_s8" value="," />
        <character is_modifier="false" name="shape" src="d0_s8" value="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens white.</text>
      <biological_entity id="o3183" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3184" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules ± hemispheric, 5–10 mm, not glaucous;</text>
      <biological_entity id="o3185" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="hemispheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s10" value="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves 3 or 4, exserted.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 22, 90.</text>
      <biological_entity id="o3186" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" unit="or" value="3" />
        <character name="quantity" src="d0_s11" unit="or" value="4" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3187" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="22" />
        <character name="quantity" src="d0_s12" value="90" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eucalyptus viminalis is known from the Outer North Coast Ranges, Central Coast, Outer South Coast Ranges, South Coast, and Peninsular Ranges.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer, fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed urban areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="urban areas" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; se Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="se Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>