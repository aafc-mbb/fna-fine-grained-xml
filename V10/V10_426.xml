<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Spach) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Phaeostoma</taxon_name>
    <taxon_name authority="(Spach) K. E. Holsinger" date="1985" rank="subsection">Phaeostoma</taxon_name>
    <taxon_name authority="Lindley" date="1837" rank="species">unguiculata</taxon_name>
    <place_of_publication>
      <publication_title>Edwards’s Bot. Reg.</publication_title>
      <place_in_publication>23: sub plate 1981.  1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section phaeostoma;subsection phaeostoma;species unguiculata</taxon_hierarchy>
  </taxon_identification>
  <number>37.</number>
  <other_name type="common_name">Elegant or woodland clarkia</other_name>
  <other_name type="common_name">mountain garland</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 30–100 cm, glabrous, glaucous.</text>
      <biological_entity id="o465" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 0–10 mm;</text>
      <biological_entity id="o466" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o467" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate to elliptic or ovate, 1–6 cm.</text>
      <biological_entity id="o468" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o469" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="elliptic or ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences open racemes, sometimes branched, axis erect;</text>
      <biological_entity id="o470" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="branched" />
      </biological_entity>
      <biological_entity id="o471" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o472" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" />
      </biological_entity>
      <relation from="o470" id="r45" name="open" negation="false" src="d0_s3" to="o471" />
    </statement>
    <statement id="d0_s4">
      <text>buds pendent.</text>
      <biological_entity id="o473" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: floral-tube 2–5 mm;</text>
      <biological_entity id="o474" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o475" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed together to 1 side, green to dark red, sparsely to densely puberulent abaxially, with longer, straight, spreading hairs to 3 mm;</text>
      <biological_entity id="o476" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o477" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="together" name="orientation" src="d0_s6" value="reflexed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="1" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s6" value="green" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s6" value="dark red" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s6" to="dark red" />
        <character is_modifier="false" modifier="sparsely to densely; abaxially" name="pubescence" src="d0_s6" value="puberulent" />
      </biological_entity>
      <biological_entity id="o478" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o479" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s6" value="longer" />
        <character is_modifier="true" name="course" src="d0_s6" value="straight" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o477" id="r46" name="with" negation="false" src="d0_s6" to="o479" />
    </statement>
    <statement id="d0_s7">
      <text>corolla rotate, petals lavender-pink to salmon or dark reddish purple, triangular or diamond-shaped to suborbiculate, 10–25 mm, claw slender, equal to or longer than blade, entire, rarely somewhat expanded at base;</text>
      <biological_entity id="o480" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o481" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rotate" />
      </biological_entity>
      <biological_entity id="o482" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="lavender-pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="salmon" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="dark reddish" />
        <character char_type="range_value" from="lavender-pink" name="coloration" src="d0_s7" to="salmon or dark reddish" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s7" value="purple" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="triangular" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="diamond--shaped" value_original="diamond-shaped" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="suborbiculate" />
        <character char_type="range_value" from="diamond-shaped" name="shape" src="d0_s7" to="suborbiculate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o483" name="claw" name_original="claw" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="slender" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" />
        <character constraint="than blade" constraintid="o484" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" />
        <character constraint="at base" constraintid="o485" is_modifier="false" modifier="rarely somewhat" name="size" src="d0_s7" value="expanded" />
      </biological_entity>
      <biological_entity id="o484" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o485" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 8, unequal, outer anthers red, inner smaller, paler;</text>
      <biological_entity id="o486" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o487" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" />
      </biological_entity>
      <biological_entity constraint="outer" id="o488" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red" />
      </biological_entity>
      <biological_entity constraint="inner" id="o489" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="smaller" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="paler" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary with hairs as on sepals;</text>
      <biological_entity id="o490" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o491" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
      <biological_entity id="o492" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o493" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <relation from="o491" id="r47" name="with" negation="false" src="d0_s9" to="o492" />
      <relation from="o492" id="r48" name="on" negation="false" src="d0_s9" to="o493" />
    </statement>
    <statement id="d0_s10">
      <text>stigma exserted beyond anthers.</text>
      <biological_entity id="o494" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o495" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o496" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o495" id="r49" name="exserted beyond" negation="false" src="d0_s10" to="o496" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules 15–30 mm.</text>
      <biological_entity id="o497" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds brown, 1–1.5 mm, tuberculate, crest inconspicuous.</text>
      <biological_entity id="o498" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s12" value="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o499" name="crest" name_original="crest" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="inconspicuous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o500" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Clarkia unguiculata is a widely distributed species in California, and occurs throughout much of the south­ern two-thirds of the state in appropriate woodland habitats.</discussion>
  <discussion>Clarkia unguiculata is ancestral to C. exilis, C. springvillensis, and C. tembloriensis.  It is one of the parents of the tetraploid species C. delicata and may have been involved in the origin of C. heterandra.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands." />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>