<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="A. Jussieu" date="1832" rank="genus">GAYOPHYTUM</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1840" rank="species">diffusum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 513.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus gayophytum;species diffusum</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs usually glabrous to strigillose, sometimes villous.</text>
      <biological_entity id="o955" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" notes="[duplicate value]" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s0" value="strigillose" />
        <character char_type="range_value" from="usually glabrous" name="pubescence" src="d0_s0" to="strigillose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched or unbranched near base, much branched distally, usually with 1 or 2 nodes between branches, distal branching dichotomous or lateral branches shortened, 5–60 cm.</text>
      <biological_entity constraint="between branches" constraintid="o958" id="o956" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" />
        <character constraint="near base" constraintid="o957" is_modifier="false" name="architecture" src="d0_s1" value="unbranched" />
        <character is_modifier="false" modifier="much; distally" name="architecture" notes="" src="d0_s1" value="branched" />
      </biological_entity>
      <biological_entity id="o957" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o958" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity constraint="distal" id="o959" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="dichotomous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o960" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="length" src="d0_s1" value="shortened" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves reduced distally, 10–60 × 1–5 mm;</text>
      <biological_entity id="o961" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s2" value="reduced" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0–10 mm;</text>
      <biological_entity id="o962" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade very narrowly lanceolate.</text>
      <biological_entity id="o963" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="very narrowly" name="shape" src="d0_s4" value="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences with flowers arising usually as proximally as first 1–20 nodes from base.</text>
      <biological_entity id="o964" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o965" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="as-proximally-as nodes" constraintid="o966" is_modifier="false" name="orientation" src="d0_s5" value="arising" />
      </biological_entity>
      <biological_entity id="o966" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o967" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o964" id="r139" name="with" negation="false" src="d0_s5" to="o965" />
      <relation from="o966" id="r140" name="from" negation="false" src="d0_s5" to="o967" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 0.9–3 (–5) mm, reflexed singly or in pairs;</text>
      <biological_entity id="o968" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o969" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in pairs" />
      </biological_entity>
      <biological_entity id="o970" name="pair" name_original="pairs" src="d0_s6" type="structure" />
      <relation from="o969" id="r141" name="in" negation="false" src="d0_s6" to="o970" />
    </statement>
    <statement id="d0_s7">
      <text>petals 1.2–5 (–7) mm;</text>
      <biological_entity id="o971" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o972" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pollen 90–100% fertile;</text>
      <biological_entity id="o973" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o974" name="pollen" name_original="pollen" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="90-100%" name="reproduction" src="d0_s8" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigma hemispheric to subglobose, exserted beyond anthers of longer stamens or surrounded by them at anthesis.</text>
      <biological_entity id="o975" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o976" name="stigma" name_original="stigma" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s9" value="hemispheric" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s9" value="subglobose" />
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s9" to="subglobose" />
        <character constraint="at by them anthesis" is_modifier="false" name="position_relational" src="d0_s9" value="surrounded" />
      </biological_entity>
      <biological_entity id="o977" name="anther" name_original="anthers" src="d0_s9" type="structure" />
      <biological_entity constraint="longer" id="o978" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <relation from="o976" id="r142" name="exserted beyond" negation="false" src="d0_s9" to="o977" />
      <relation from="o976" id="r143" name="part_of" negation="false" src="d0_s9" to="o978" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules ascending to reflexed, subterete, 3–15 × 1–1.5 mm, with inconspicuous or conspicuous constrictions between seeds, valve margins somewhat undulate, all valves free from septum after dehiscence, septum straight or sinuous;</text>
      <biological_entity id="o979" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s10" value="ascending" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s10" value="reflexed" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subterete" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" constraint="with constrictions" constraintid="o980" from="1" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="between seeds" constraintid="o981" id="o980" name="constriction" name_original="constrictions" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="inconspicuous" />
        <character is_modifier="true" name="prominence" src="d0_s10" value="conspicuous" />
      </biological_entity>
      <biological_entity id="o981" name="seed" name_original="seeds" src="d0_s10" type="structure" />
      <biological_entity constraint="valve" id="o982" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s10" value="undulate" />
      </biological_entity>
      <biological_entity id="o983" name="valve" name_original="valves" src="d0_s10" type="structure">
        <character constraint="from septum" constraintid="o984" is_modifier="false" name="fusion" src="d0_s10" value="free" />
      </biological_entity>
      <biological_entity id="o984" name="septum" name_original="septum" src="d0_s10" type="structure" />
      <biological_entity id="o985" name="septum" name_original="septum" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" />
        <character is_modifier="false" name="course" src="d0_s10" value="sinuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicel 2–10 (–15) mm, usually shorter than capsule.</text>
      <biological_entity id="o986" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character constraint="than capsule" constraintid="o987" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="usually shorter" />
      </biological_entity>
      <biological_entity id="o987" name="capsule" name_original="capsule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds (3–) 6–18, all or most developing, arranged ± parallel to septum and in alternating pattern between locules, crowded, overlapping, often appearing to form 2 irregular rows in each locule, or well spaced, forming a single row in capsule, brown, sometimes mottled with gray, 1–1.6 × 0.5–0.8 mm, glabrous or puberulent.</text>
      <biological_entity id="o989" name="septum" name_original="septum" src="d0_s12" type="structure" />
      <biological_entity constraint="between locules" constraintid="o991" id="o990" name="pattern" name_original="pattern" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="alternating" />
      </biological_entity>
      <biological_entity id="o991" name="locule" name_original="locules" src="d0_s12" type="structure" />
      <biological_entity id="o992" name="row" name_original="rows" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s12" value="irregular" />
      </biological_entity>
      <biological_entity id="o993" name="locule" name_original="locule" src="d0_s12" type="structure" />
      <biological_entity id="o994" name="row" name_original="row" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="single" />
      </biological_entity>
      <biological_entity id="o995" name="capsule" name_original="capsule" src="d0_s12" type="structure" />
      <relation from="o988" id="r144" name="in" negation="false" src="d0_s12" to="o990" />
      <relation from="o988" id="r145" modifier="often" name="appearing to form" negation="false" src="d0_s12" to="o992" />
      <relation from="o988" id="r146" modifier="often; often" name="in" negation="false" src="d0_s12" to="o993" />
      <relation from="o988" id="r147" name="forming a" negation="false" src="d0_s12" to="o994" />
      <relation from="o988" id="r148" name="in" negation="false" src="d0_s12" to="o995" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 28.</text>
      <biological_entity id="o988" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s12" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s12" to="18" />
        <character is_modifier="false" name="development" src="d0_s12" value="developing" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="arranged" />
        <character constraint="to septum" constraintid="o989" is_modifier="false" modifier="more or less" name="arrangement" src="d0_s12" value="parallel" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s12" value="crowded" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="overlapping" />
        <character is_modifier="false" modifier="well" name="arrangement" notes="" src="d0_s12" value="spaced" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="mottled with gray" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s12" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o996" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Gayophytumdiffusum consists of a diverse assemblage of tetraploid populations, some of which are similar to every known diploid species except G. humile.  The combination of characteristics of at least five diploid species in various ways suggests that the complex is derived from several independently formed allopolyploids that subsequently hybridized and segregated to produce the observed diversity.</discussion>
  <discussion>Populations of Gayophytum diffusum differ in breeding behavior.  Populations with relatively large flowers and stigmas that extend beyond the anthers are obviously outcrossing, whereas most populations are small-flowered and modally self-pollinated.  It is among the latter that the greatest morphological diversity is found.  Often two or more morphologically different, apparently true-breeding strains can be found growing together.  In such a variable complex, recognition of infraspecific taxa becomes arbitrary.  In this treatment the striking morphological differences associated with breeding behavior have been used as a basis for subspecies recognition.  At some localities the two subspecies intergrade.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 3–5(–7) mm; sepals 2–3(–5) mm; stigma usually exserted beyond anthers of longer stamens at anthesis.</description>
      <determination>8a. Gayophytum diffusum subsp. diffusum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 1.2–3 mm; sepals 0.9–2 mm; stigma surrounded by anthers at anthesis.</description>
      <determination>8b. Gayophytum diffusum subsp. parviflorum</determination>
    </key_statement>
  </key>
</bio:treatment>