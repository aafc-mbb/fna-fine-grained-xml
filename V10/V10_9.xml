<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">BUXACEAE</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">PACHYSANDRA</taxon_name>
    <taxon_name authority="Siebold &amp; Zuccarini" date="1845" rank="species">terminalis</taxon_name>
    <place_of_publication>
      <publication_title>Abh. Math.-Phys. Cl. Königl. Bayer. Akad. Wiss.</publication_title>
      <place_in_publication>4(2): 142.  1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family buxaceae;genus pachysandra;species terminalis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pachysandra</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">terminalis</taxon_name>
    <taxon_name authority="Norton" date="unknown" rank="variety">variegata</taxon_name>
    <taxon_hierarchy>genus pachysandra;species terminalis;variety variegata</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Japanese mountain spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs 10–30 cm, glabrous or glabrate.</text>
      <biological_entity id="o5626" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves crowded distally and in clusters at middle and often at proximal part of stem;</text>
      <biological_entity id="o5627" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="at middle part" constraintid="o5628" is_modifier="false" modifier="distally" name="arrangement" src="d0_s1" value="crowded" />
      </biological_entity>
      <biological_entity constraint="middle" id="o5628" name="part" name_original="part" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o5629" name="part" name_original="part" src="d0_s1" type="structure" />
      <biological_entity id="o5630" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <relation from="o5627" id="r1090" modifier="often" name="at" negation="false" src="d0_s1" to="o5629" />
      <relation from="o5629" id="r1091" name="part_of" negation="false" src="d0_s1" to="o5630" />
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–3 cm;</text>
      <biological_entity id="o5631" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade slightly darker green adaxially, without mottling along veins, elliptic to widely elliptic or ovate, broadly ovate, or obovate, 5–8 × 2–4 cm, base cuneate to broadly cune­ate, margins coarsely dentate distal to middle, apex (terminal tooth) acute or obtuse, abaxial surface puberulent along veins, adaxial surface glabrous, shiny (not evident when dried).</text>
      <biological_entity id="o5632" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="darker green" />
        <character constraint="along veins" constraintid="o5633" is_modifier="false" name="coloration" src="d0_s3" value="mottling" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s3" value="elliptic" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s3" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s3" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s3" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5633" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <biological_entity id="o5634" name="base" name_original="base" src="d0_s3" type="structure">
        <character constraint="to ­" constraintid="o5635" is_modifier="false" name="shape" src="d0_s3" value="cuneate" />
      </biological_entity>
      <biological_entity id="o5635" name="­" name_original="­" src="d0_s3" type="structure" />
      <biological_entity id="o5636" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="coarsely" name="position" src="d0_s3" value="dentate" />
        <character is_modifier="false" name="position" notes="[duplicate value]" src="d0_s3" value="distal" />
        <character is_modifier="false" name="position" notes="[duplicate value]" src="d0_s3" value="middle" />
        <character char_type="range_value" from="distal" name="position" src="d0_s3" to="middle" />
      </biological_entity>
      <biological_entity id="o5637" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5638" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character constraint="along veins" constraintid="o5639" is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" />
      </biological_entity>
      <biological_entity id="o5639" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial" id="o5640" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescence 1, terminal.</text>
      <biological_entity id="o5641" name="inflorescence" name_original="inflorescence" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Staminate flowers 15–20, sessile, each subtended by 1 bract and 2 sepal-like bracteoles;</text>
      <biological_entity id="o5642" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s5" to="20" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" />
      </biological_entity>
      <biological_entity id="o5643" name="bract" name_original="bract" src="d0_s5" type="structure" />
      <biological_entity id="o5644" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="sepal-like" />
      </biological_entity>
      <relation from="o5642" id="r1092" name="subtended by" negation="false" src="d0_s5" to="o5643" />
      <relation from="o5642" id="r1093" name="subtended by" negation="false" src="d0_s5" to="o5644" />
    </statement>
    <statement id="d0_s6">
      <text>tepals 2, ovate, 2.5–4 mm, margins ciliate, apex rounded.</text>
      <biological_entity id="o5645" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5646" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" />
      </biological_entity>
      <biological_entity id="o5647" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate flowers 2–7, pedicellate;</text>
      <biological_entity id="o5648" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals 2.5–4 mm, margins ciliate, apex rounded;</text>
      <biological_entity id="o5649" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5650" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" />
      </biological_entity>
      <biological_entity id="o5651" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 2 (or 3) -carpellate, apical lobes 2 (or 3), locules 1 (or 2) per carpel;</text>
      <biological_entity id="o5652" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="2-carpellate" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5653" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" />
      </biological_entity>
      <biological_entity id="o5654" name="locule" name_original="locules" src="d0_s9" type="structure">
        <character constraint="per carpel" constraintid="o5655" name="quantity" src="d0_s9" value="1" />
      </biological_entity>
      <biological_entity id="o5655" name="carpel" name_original="carpel" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>styles 2;</text>
      <biological_entity id="o5656" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovules 1 or 2 per locule.</text>
      <biological_entity id="o5657" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" unit="or per" value="1" />
        <character name="quantity" src="d0_s11" unit="or per" value="2" />
      </biological_entity>
      <biological_entity id="o5658" name="locule" name_original="locule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits berries, to 15 mm diam., apex 2-lobed, glabrous.</text>
      <biological_entity constraint="fruits" id="o5659" name="berry" name_original="berries" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5660" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="2-lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1–3, brown or black, 4–6 × 2–3 mm;</text>
    </statement>
    <statement id="d0_s14">
      <text>ecarunculate.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 48.</text>
      <biological_entity id="o5661" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="3" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="ecarunculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5662" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>&lt;i&gt;Pachysandra terminalis&lt;/i&gt;, a native of China and Japan, is widely cultivated as an ornamental groundcover, usu­ally in shaded situations, in temperate North America.  The plants are more likely to spread vegetatively by rhizome pieces rather than by seeds.  Many natural-appearing occurrences may be remnants of cultivation.  The two sepal-like bracteoles of the staminate flowers are sometimes interpreted as tepals (R. B. Channelland C. E. Wood Jr. 1987).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, railroad embankments, moist woods, along streams, near old homesites.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad embankments" />
        <character name="habitat" value="woods" modifier="moist" constraint="along streams" />
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="old homesites" modifier="near" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont.; Conn., Del., Ill., Ind., Md., Mass., N.H., Ohio, Pa., R.I., Va., Wis.; e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>