<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="L’Heritier" date="1792" rank="genus">EUCALYPTUS</taxon_name>
    <taxon_name authority="Smith" date="1795" rank="species">tereticornis</taxon_name>
    <place_of_publication>
      <publication_title>Spec. Bot. New Holland,</publication_title>
      <place_in_publication>41.  1795</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus eucalyptus;species tereticornis</taxon_hierarchy>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Forest red gum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 40 m;</text>
      <biological_entity id="o3117" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="40" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk gray or tan, nearly straight, ± smooth;</text>
      <biological_entity id="o3118" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s1" value="straight" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark usually shed in irregular strips distally, sometimes per­sistent toward trunk base.</text>
      <biological_entity id="o3119" name="bark" name_original="bark" src="d0_s2" type="structure" />
      <biological_entity id="o3120" name="strip" name_original="strips" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s2" value="irregular" />
      </biological_entity>
      <biological_entity constraint="trunk" id="o3121" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o3119" id="r562" modifier="distally" name="shed in" negation="false" src="d0_s2" to="o3120" />
      <relation from="o3119" id="r563" modifier="sometimes" name="toward" negation="false" src="d0_s2" to="o3121" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: (juvenile alternate, petiolate);</text>
      <biological_entity id="o3122" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.3–2 cm;</text>
      <biological_entity id="o3123" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3124" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade green, lanceolate, 6–20 × 1.5–2.5 cm.</text>
      <biological_entity id="o3125" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o3126" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 0.8–1.2 cm.</text>
      <biological_entity id="o3127" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s6" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences ca. 7-flowered, umbels.</text>
      <biological_entity id="o3128" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="7-flowered" />
      </biological_entity>
      <biological_entity id="o3129" name="umbel" name_original="umbels" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: hypanthium hemispheric, 2–3 mm, calyptra 2–3 times as long as hypanthium;</text>
      <biological_entity id="o3130" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3131" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3132" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character constraint="hypanthium" constraintid="o3133" is_modifier="false" name="length" src="d0_s8" value="2-3 times as long as hypanthium" />
      </biological_entity>
      <biological_entity id="o3133" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>calyptra mostly conic-acuminate or horn-shaped;</text>
      <biological_entity id="o3134" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3135" name="calyptra" name_original="calyptra" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s9" value="conic-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="horn--shaped" value_original="horn-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens white.</text>
      <biological_entity id="o3136" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3137" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules hemispheric, 5–9 mm, not glaucous;</text>
      <biological_entity id="o3138" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="hemispheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s11" value="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves 3–5, exserted.</text>
      <biological_entity id="o3139" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="5" />
        <character is_modifier="false" name="position" src="d0_s12" value="exserted" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eucalyptus tereticornis is known from the San Joaquin Valley, Outer South Coast Ranges, South Coast, northern Channel Islands, and Transverse and Peninsular ranges.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed coastal, urban areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal" modifier="disturbed" />
        <character name="habitat" value="urban areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; e Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="e Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>