<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Ludwigioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LUDWIGIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Ludwigia</taxon_name>
    <taxon_name authority="R. M. Harper" date="1904" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Torreya</publication_title>
      <place_in_publication>4: 163, fig. 2.  1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily ludwigioideae;genus ludwigia;section ludwigia;species maritima</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Seaside primrose-willow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots fibrous or fusiform, sometimes fascicled.</text>
      <biological_entity id="o1311" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="texture" src="d0_s0" value="fibrous" />
        <character is_modifier="false" name="shape" src="d0_s0" value="fusiform" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_arrangement" src="d0_s0" value="fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems subterete to scarcely angled, with narrow raised lines or wings de­current from leaf-axils, 30–90 cm, simple or sparsely branched distally, strigillose to some­times glabrate.</text>
      <biological_entity id="o1312" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s1" value="subterete" />
        <character is_modifier="false" modifier="scarcely" name="shape" notes="[duplicate value]" src="d0_s1" value="angled" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s1" to="scarcely angled" />
        <character constraint="with" is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" />
        <character constraint="from leaf-axils" constraintid="o1315" is_modifier="false" name="structure_subtype" src="d0_s1" value="current" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" />
        <character is_modifier="false" modifier="sparsely; distally" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" />
      </biological_entity>
      <biological_entity id="o1313" name="line" name_original="lines" src="d0_s1" type="structure" />
      <biological_entity id="o1314" name="wing" name_original="wings" src="d0_s1" type="structure" />
      <biological_entity id="o1315" name="leaf-axil" name_original="leaf-axils" src="d0_s1" type="structure" />
      <relation from="o1312" id="r223" name="raised" negation="false" src="d0_s1" to="o1313" />
      <relation from="o1312" id="r224" name="raised" negation="false" src="d0_s1" to="o1314" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules nar­rowly deltate, 0.05–0.2 × 0.05–0.1 mm;</text>
      <biological_entity id="o1316" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1317" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="rowly" name="shape" src="d0_s2" value="deltate" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="length" src="d0_s2" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s2" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity id="o1318" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate proxi­mally, lanceolate to lanceolate-linear distally, (2–) 3–8 × 0.3–1.5 cm, base cuneate, margins entire, apex acute, surfaces glabrate to strigillose or hirsute;</text>
      <biological_entity id="o1319" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1320" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" />
        <character is_modifier="false" modifier="mally" name="shape" notes="[duplicate value]" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="lanceolate-linear" />
        <character char_type="range_value" from="lanceolate" modifier="mally; distally" name="shape" src="d0_s4" to="lanceolate-linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s4" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s4" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1321" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" />
      </biological_entity>
      <biological_entity id="o1322" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" />
      </biological_entity>
      <biological_entity id="o1323" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" />
      </biological_entity>
      <biological_entity id="o1324" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s4" value="glabrate" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s4" value="strigillose" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s4" value="hirsute" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s4" to="strigillose or hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts usually much reduced, sublinear.</text>
      <biological_entity id="o1325" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o1326" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually much" name="size" src="d0_s5" value="reduced" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sublinear" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences sparse racemes, flowers solitary in leaf-axils;</text>
      <biological_entity id="o1327" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o1328" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s6" value="sparse" />
      </biological_entity>
      <biological_entity id="o1329" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="in leaf-axils" constraintid="o1330" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" />
      </biological_entity>
      <biological_entity id="o1330" name="leaf-axil" name_original="leaf-axils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracteoles attached in subopposite pairs on distal 1/3 of pedicel, lanceolate-linear, 0.7–3.2 (–5) × 0.2–0.5 mm, margins entire, apex acute, surfaces strigillose.</text>
      <biological_entity id="o1331" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character constraint="in pairs" constraintid="o1332" is_modifier="false" name="fixation" src="d0_s7" value="attached" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="lanceolate-linear" />
        <character char_type="range_value" from="3.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s7" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1332" name="pair" name_original="pairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="subopposite" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1333" name="1/3" name_original="1/3" src="d0_s7" type="structure" />
      <biological_entity id="o1334" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <biological_entity id="o1335" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" />
      </biological_entity>
      <biological_entity id="o1336" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" />
      </biological_entity>
      <biological_entity id="o1337" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="strigillose" />
      </biological_entity>
      <relation from="o1332" id="r225" name="on" negation="false" src="d0_s7" to="o1333" />
      <relation from="o1333" id="r226" name="part_of" negation="false" src="d0_s7" to="o1334" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals often spreading, ovate-deltate, (4.5–) 5.5–8 (–9) × 3–5 mm, margins entire, apex acute to obtuse, sur­faces strigillose;</text>
      <biological_entity id="o1338" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1339" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s8" value="spreading" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate-deltate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_length" src="d0_s8" to="5.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1340" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" />
      </biological_entity>
      <biological_entity id="o1341" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="obtuse" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
      <biological_entity id="o1342" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals cordate, 9–12 × 8–10 mm, base attenuate, apex emarginate;</text>
      <biological_entity id="o1343" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1344" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cordate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1345" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="attenuate" />
      </biological_entity>
      <biological_entity id="o1346" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments yellow, awl-shaped, 1.9–3.2 mm, anthers 1.2–2.5 × 0.4–0.7 mm;</text>
      <biological_entity id="o1347" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1348" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="awl--shaped" value_original="awl-shaped" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s10" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1349" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary subcuboid or globose, 3–4.5 × 3–4 mm;</text>
      <biological_entity id="o1350" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1351" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc elevated, domed, 0.9–1.3 mm diam., prominently 4-lobed, ringed with sparse, spreading hairs;</text>
      <biological_entity id="o1352" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o1353" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="elevated" />
        <character is_modifier="false" name="shape" src="d0_s12" value="domed" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="diameter" src="d0_s12" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="prominently" name="shape" src="d0_s12" value="4-lobed" />
        <character constraint="with hairs" constraintid="o1354" is_modifier="false" name="relief" src="d0_s12" value="ringed" />
      </biological_entity>
      <biological_entity id="o1354" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s12" value="sparse" />
        <character is_modifier="true" name="orientation" src="d0_s12" value="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 1.5–3.3 mm, glabrous, stigma capitate to hemi­spher­ical, 0.6–1.2 × 1.4–1.9 mm, shallowly 4-lobed, not exserted beyond anthers.</text>
      <biological_entity id="o1355" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1356" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" />
      </biological_entity>
      <biological_entity id="o1357" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character constraint="to ­" constraintid="o1358" is_modifier="false" name="architecture_or_shape" src="d0_s13" value="capitate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" notes="" src="d0_s13" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" notes="" src="d0_s13" to="1.9" to_unit="mm" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s13" value="4-lobed" />
      </biological_entity>
      <biological_entity id="o1358" name="­" name_original="­" src="d0_s13" type="structure" />
      <biological_entity id="o1359" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <relation from="o1357" id="r227" name="exserted beyond" negation="true" src="d0_s13" to="o1359" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules subcuboid to squarish globose, 4–7 ×4–5 mm, 4-angled, often also 4-winged, wings 0.3–1.2 mm wide, pedicel 5–17 mm.</text>
      <biological_entity id="o1360" name="capsule" name_original="capsules" src="d0_s14" type="structure" />
      <biological_entity id="o1361" name="wing" name_original="wings" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="squarish" />
        <character is_modifier="true" name="shape" src="d0_s14" value="globose" />
        <character char_type="range_value" from="4" from_unit="mm" is_modifier="true" name="length" src="d0_s14" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" is_modifier="true" name="width" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="true" name="shape" src="d0_s14" value="4-angled" />
        <character is_modifier="true" modifier="often" name="architecture" src="d0_s14" value="4-winged" />
        <character char_type="range_value" from="0.3" from_unit="mm" modifier="often" name="width" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1362" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="17" to_unit="mm" />
      </biological_entity>
      <relation from="o1360" id="r228" name="to" negation="false" src="d0_s14" to="o1361" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds light-brown, oblong to reni­form, 0.4–0.6 ×0.2–0.4 mm, surface cells elongate trans­versely to seed length, except may parallel to seed length near raphe.</text>
      <biological_entity id="o1363" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="light-brown" />
        <character constraint="to form" constraintid="o1364" is_modifier="false" name="shape" src="d0_s15" value="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" notes="" src="d0_s15" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" notes="" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1364" name="form" name_original="form" src="d0_s15" type="structure" />
      <biological_entity id="o1366" name="seed" name_original="seed" src="d0_s15" type="structure">
        <character is_modifier="true" name="length" src="d0_s15" value="parallel" />
      </biological_entity>
      <biological_entity id="o1367" name="raphe" name_original="raphe" src="d0_s15" type="structure" />
      <relation from="o1365" id="r229" modifier="versely" name="except may" negation="false" src="d0_s15" to="o1366" />
      <relation from="o1365" id="r230" modifier="versely" name="near" negation="false" src="d0_s15" to="o1367" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 16.</text>
      <biological_entity constraint="surface" id="o1365" name="cel" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" name="length" src="d0_s15" value="elongate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1368" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, sandy, or peaty habitats, roadside ditches, margins of bogs or fields, usually within 75 miles of sea coast.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="peaty habitats" modifier="damp sandy or" />
        <character name="habitat" value="roadside ditches" />
        <character name="habitat" value="margins" constraint="of bogs or fields" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="75 miles" modifier="usually within" />
        <character name="habitat" value="sea coast" modifier="of" />
        <character name="habitat" value="damp" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>