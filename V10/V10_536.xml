<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">LAGERSTROEMIA</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="species">indica</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1076.  1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus lagerstroemia;species indica</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 20–70 × 15–30 mm.</text>
      <biological_entity id="o3587" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s0" to="70" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s0" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Panicles showy, 5–25 cm.</text>
      <biological_entity id="o3588" name="panicle" name_original="panicles" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="showy" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: floral-tube 7–10 × 6–10 mm;</text>
      <biological_entity id="o3589" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o3590" name="floral-tube" name_original="floral-tube" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals erect;</text>
      <biological_entity id="o3591" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o3592" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals 10–12 × 7–15 mm, slender claw 7–8 mm;</text>
      <biological_entity id="o3593" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o3594" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="slender" id="o3595" name="claw" name_original="claw" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>antisepalous stamens 6, stout, anthers green, relatively large, antipetalous stamens 30+, in clusters of 5 or 6, [rarely antipetalous stamens solitary], slender, anthers yellow, relatively small.</text>
      <biological_entity id="o3596" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="antisepalous" id="o3597" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="6" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s5" value="stout" />
      </biological_entity>
      <biological_entity id="o3598" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s5" value="large" />
      </biological_entity>
      <biological_entity constraint="antipetalous" id="o3599" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s5" upper_restricted="false" />
        <character is_modifier="false" modifier="of 5 or 6" name="size" src="d0_s5" value="slender" />
      </biological_entity>
      <biological_entity id="o3600" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s5" value="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules globose to oblong, 9–15 × 8–11 mm.</text>
      <biological_entity id="o3601" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="globose" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="oblong" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s6" to="oblong" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds 7–9 mm, including unilateral wing.</text>
      <biological_entity constraint="unilateral" id="o3603" name="wing" name_original="wing" src="d0_s7" type="structure" />
      <relation from="o3602" id="r432" name="including" negation="false" src="d0_s7" to="o3603" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 48, 50 (China, India).</text>
      <biological_entity id="o3602" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3604" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="48" />
        <character name="quantity" src="d0_s8" value="50" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lagerstroemia indica is popular in the southern and western United States as a landscape and street tree for its showy, densely flowered inflorescences.  Numerous cultivars varying in habit and flower color have been developed.  The dimorphic anthers of L. indica fill different reproductive roles.  The larger, green anthers produce fertile pollen and the smaller, yellow anthers produce abundant, sterile pollen that attracts pollinators.  A larger tree species, L. speciosa (Linnaeus) Persoon, is cultivated in southern Florida and southward; it has not become naturalized.  It is distinguished from L. indica by oblong leaves to 28 cm and a yellow-brown puberulent, deeply ridged floral tube.  Hybrid cultivars of the Japanese L. subcostata Koehne var. fauriei Hatusima × L. indica show a range of flower colors and habit and are increasingly popular in landscape use.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>introduced also widely in tropical and subtropical areas..</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tropical" modifier="introduced also widely in" />
        <character name="habitat" value="subtropical areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_1">
      <text>introduced also widely in tropical and subtropical areas..</text>
      <biological_entity id="hab_o1" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tropical" modifier="introduced also widely in" />
        <character name="habitat" value="subtropical areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., Ga., La., Miss., S.C., Tex., Utah; Asia; introduced also widely in tropical and subtropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="also widely in tropical and subtropical areas" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>