<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(P. H. Raven) W. L. Wagner &amp; Hoch" date="2007" rank="genus">EREMOTHERA</taxon_name>
    <taxon_name authority="(Douglas) W. L. Wagner &amp; Hoch" date="2007" rank="species">boothii</taxon_name>
    <taxon_name authority="(Munz) W. L. Wagner &amp; Hoch" date="2007" rank="subspecies">condensata</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 209. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus eremothera;species boothii;subspecies condensata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) Greene" date="unknown" rank="species">decorticans</taxon_name>
    <taxon_name authority="Munz" date="1928" rank="variety">condensata</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>85: 247.  1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species decorticans;variety condensata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(Douglas) P. H. Raven" date="unknown" rank="species">boothii</taxon_name>
    <taxon_name authority="(Munz) P. H. Raven" date="unknown" rank="subspecies">condensata</taxon_name>
    <taxon_hierarchy>genus camissonia;species boothii;subspecies condensata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">boothii</taxon_name>
    <taxon_name authority="(Munz) Cronquist" date="unknown" rank="variety">condensata</taxon_name>
    <taxon_hierarchy>genus c.;species boothii;variety condensata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="Douglas" date="unknown" rank="species">boothii</taxon_name>
    <taxon_name authority="(Munz) Munz" date="unknown" rank="subspecies">condensata</taxon_name>
    <taxon_hierarchy>genus o.;species boothii;subspecies condensata</taxon_hierarchy>
  </taxon_identification>
  <number>4c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs glabrate to strigillose, sometimes also glandular puberulent in inflorescence.</text>
      <biological_entity id="o4872" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s0" value="glabrate" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s0" value="strigillose" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s0" to="strigillose" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character constraint="in inflorescence" constraintid="o4873" is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
      <biological_entity id="o4873" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 5–20 (–30) cm.</text>
      <biological_entity id="o4874" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves primarily clustered toward base, 2.5–10 (–13) × 0.3–2 (–2.5) cm;</text>
      <biological_entity id="o4875" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="toward base" constraintid="o4876" is_modifier="false" modifier="primarily" name="arrangement_or_growth_form" src="d0_s2" value="clustered" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s2" to="13" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" notes="" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" notes="" src="d0_s2" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" notes="" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4876" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 0–6 cm;</text>
      <biological_entity id="o4877" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade usually lanceolate to oblanceolate, margins subentire to denticulate.</text>
      <biological_entity id="o4878" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" notes="[duplicate value]" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="oblanceolate" />
        <character char_type="range_value" from="usually lanceolate" name="shape" src="d0_s4" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o4879" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="subentire" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="denticulate" />
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences very dense.</text>
      <biological_entity id="o4880" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="very" name="density" src="d0_s5" value="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: floral-tube 3.5–8 mm;</text>
      <biological_entity id="o4881" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4882" name="floral-tube" name_original="floral-tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white, 4–5 mm.</text>
      <biological_entity id="o4883" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4884" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules curved outward, not downward, sometimes ± contorted, tapering abruptly distally, 4-angled, much thickened along angles, 2–3.8 mm diam. near base.</text>
      <biological_entity id="o4885" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="curved" />
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s8" value="downward" />
        <character is_modifier="false" modifier="sometimes more or less" name="arrangement_or_shape" src="d0_s8" value="contorted" />
        <character is_modifier="false" modifier="abruptly distally; distally" name="shape" src="d0_s8" value="tapering" />
        <character is_modifier="false" name="shape" src="d0_s8" value="4-angled" />
        <character constraint="along angles" constraintid="o4886" is_modifier="false" modifier="much" name="size_or_width" src="d0_s8" value="thickened" />
        <character char_type="range_value" constraint="near base" constraintid="o4887" from="2" from_unit="mm" name="diameter" notes="" src="d0_s8" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4886" name="angle" name_original="angles" src="d0_s8" type="structure" />
      <biological_entity id="o4887" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Seeds dimorphic.</text>
      <biological_entity id="o4888" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s9" value="dimorphic" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies condensata intergrades with subsp. desertorum.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy slopes and washes, desert scrublands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy slopes" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="desert scrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>80–1300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="80" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>