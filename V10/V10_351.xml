<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) P. H. Raven" date="1964" rank="section">Rhodanthos</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>16: 287. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section rhodanthos</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Fischer &amp; C. A. Meyer" date="1836" rank="section">Rhodanthos</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (St. Petersburg)</publication_title>
      <place_in_publication>2: 45.  1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;section rhodanthos</taxon_hierarchy>
  </taxon_identification>
  <number>6c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences: axis recurved or straight and erect;</text>
      <biological_entity id="o6072" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
      <biological_entity id="o6073" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="recurved" />
        <character is_modifier="false" name="course" src="d0_s0" value="straight" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>buds pendent or erect.</text>
      <biological_entity id="o6074" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o6075" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="pendent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: floral-tube obconic, 1–10 mm;</text>
      <biological_entity id="o6076" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o6077" name="floral-tube" name_original="floral-tube" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obconic" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals reflexed together to 1 side;</text>
      <biological_entity id="o6078" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o6079" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="together" name="orientation" src="d0_s3" value="reflexed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="1" />
      </biological_entity>
      <biological_entity id="o6080" name="side" name_original="side" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petals usually pink to lavender or light purple, rarely white, usually with red spot near middle or a red zone at base, obovate to fan-shaped, unlobed, claw inconspicuous or absent;</text>
      <biological_entity id="o6081" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o6082" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" notes="[duplicate value]" src="d0_s4" value="pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="lavender" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="light purple" />
        <character char_type="range_value" from="usually pink" name="coloration" src="d0_s4" to="lavender or light purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s4" value="white" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="obovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="fan--shaped" value_original="fan-shaped" />
        <character char_type="range_value" from="obovate" name="shape" notes="" src="d0_s4" to="fan-shaped" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" />
      </biological_entity>
      <biological_entity id="o6083" name="middle" name_original="middle" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="red spot" />
      </biological_entity>
      <biological_entity id="o6084" name="zone" name_original="zone" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="red" />
      </biological_entity>
      <biological_entity id="o6085" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o6086" name="claw" name_original="claw" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" />
      </biological_entity>
      <relation from="o6082" id="r1113" modifier="usually" name="with" negation="false" src="d0_s4" to="o6083" />
      <relation from="o6082" id="r1114" modifier="usually" name="with" negation="false" src="d0_s4" to="o6084" />
      <relation from="o6084" id="r1115" name="at" negation="false" src="d0_s4" to="o6085" />
    </statement>
    <statement id="d0_s5">
      <text>stamens 8, subequal.</text>
      <biological_entity id="o6087" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6088" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="8" />
        <character is_modifier="false" name="size" src="d0_s5" value="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules 4-angled, 4-grooved or 8-ribbed;</text>
    </statement>
    <statement id="d0_s7">
      <text>sessile or pedicellate.</text>
      <biological_entity id="o6089" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="4-angled" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="4-grooved" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="8-ribbed" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 6 (6 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>