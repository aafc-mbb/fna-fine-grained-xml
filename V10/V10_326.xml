<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Endlicher" date="1830" rank="tribe">Epilobieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EPILOBIUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Epilobium</taxon_name>
    <taxon_name authority="Lamarck in J. Lamarck et al." date="1786" rank="species">anagallidifolium</taxon_name>
    <place_of_publication>
      <publication_title>Encycl.</publication_title>
      <place_in_publication>2: 376.  1786</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section epilobium;species anagallidifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epilobium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">alpinum</taxon_name>
    <other_info_on_name>name rejected</other_info_on_name>
    <taxon_hierarchy>genus epilobium;species alpinum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="Haussknecht" date="unknown" rank="species">pseudoscaposum</taxon_name>
    <taxon_hierarchy>genus e.;species pseudoscaposum</taxon_hierarchy>
  </taxon_identification>
  <number>32.</number>
  <other_name type="common_name">Alpine or pimpernel willowherb</other_name>
  <other_name type="common_name">épilobe à feuilles de mouron</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with spreading thin, small-leafed epigeous soboles to 5 cm.</text>
      <biological_entity id="o5169" name="herb" name_original="herbs" src="d0_s0" type="structure" />
      <biological_entity id="o5170" name="sobole" name_original="soboles" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" />
        <character is_modifier="true" name="width" src="d0_s0" value="thin" />
        <character is_modifier="true" name="location" src="d0_s0" value="epigeous" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="5" to_unit="cm" />
      </biological_entity>
      <relation from="o5169" id="r928" name="with" negation="false" src="d0_s0" to="o5170" />
    </statement>
    <statement id="d0_s1">
      <text>Stems many, ascending, often sigmoidally bent, nod­ding distally, later erect, clumped or mat-forming, terete, 3–20 (–25) cm, simple, subglabrous, sometimes with faint raised strigillose lines decurrent from margins of petioles, rarely mixed strigillose and sparsely glandular puber­ulent distally.</text>
      <biological_entity id="o5171" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="many" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" />
        <character is_modifier="false" modifier="often sigmoidally" name="shape" src="d0_s1" value="bent" />
        <character is_modifier="false" modifier="distally; later" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="clumped" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="mat-forming" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="subglabrous" />
        <character is_modifier="false" modifier="rarely" name="arrangement" notes="" src="d0_s1" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigillose" />
        <character is_modifier="false" modifier="sparsely; distally" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" />
      </biological_entity>
      <biological_entity id="o5172" name="line" name_original="lines" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="faint" />
        <character is_modifier="true" name="prominence" src="d0_s1" value="raised" />
        <character is_modifier="true" name="pubescence" src="d0_s1" value="strigillose" />
        <character constraint="from margins" constraintid="o5173" is_modifier="false" name="shape" src="d0_s1" value="decurrent" />
      </biological_entity>
      <biological_entity id="o5173" name="margin" name_original="margins" src="d0_s1" type="structure" />
      <biological_entity id="o5174" name="petiole" name_original="petioles" src="d0_s1" type="structure" />
      <relation from="o5171" id="r929" modifier="sometimes" name="with" negation="false" src="d0_s1" to="o5172" />
      <relation from="o5173" id="r930" name="part_of" negation="false" src="d0_s1" to="o5174" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite and crowded proximal to inflorescence, alternate distally, petioles 1–6 mm, rarely subsessile distally;</text>
      <biological_entity id="o5175" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" />
        <character is_modifier="false" modifier="distally" name="arrangement" notes="" src="d0_s2" value="alternate" />
      </biological_entity>
      <biological_entity id="o5176" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
      <biological_entity id="o5177" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="rarely; distally" name="architecture" src="d0_s2" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade spatulate to oblong proxi­mally, elliptic to narrowly lanceolate or sublinear distally, (0.5–) 0.8–2.5 × 0.3–1 cm, base attenuate to cuneate, margins subentire proximally, sparsely denticulate distally with 2–5 low teeth per side, veins obscure, 2–4 per side, apex obtuse or rounded proximally to subacute distally, surfaces subglabrous;</text>
      <biological_entity id="o5178" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="spatulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="oblong" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblong" />
        <character is_modifier="false" modifier="mally" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="sublinear" />
        <character char_type="range_value" from="elliptic" modifier="mally; distally" name="shape" src="d0_s3" to="narrowly lanceolate or sublinear" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s3" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5179" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="attenuate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="cuneate" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s3" to="cuneate" />
      </biological_entity>
      <biological_entity id="o5180" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s3" value="subentire" />
        <character constraint="with teeth" constraintid="o5181" is_modifier="false" modifier="sparsely" name="shape" src="d0_s3" value="denticulate" />
      </biological_entity>
      <biological_entity id="o5181" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="true" name="position" src="d0_s3" value="low" />
      </biological_entity>
      <biological_entity id="o5182" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o5183" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="obscure" />
        <character char_type="range_value" constraint="per side" constraintid="o5184" from="2" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o5184" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o5185" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="obtuse" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="rounded" />
        <character is_modifier="false" modifier="proximally" name="shape" notes="[duplicate value]" src="d0_s3" value="subacute" />
        <character char_type="range_value" from="rounded" modifier="distally" name="shape" src="d0_s3" to="proximally subacute" />
      </biological_entity>
      <biological_entity id="o5186" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="subglabrous" />
      </biological_entity>
      <relation from="o5181" id="r931" name="per" negation="false" src="d0_s3" to="o5182" />
    </statement>
    <statement id="d0_s4">
      <text>bracts reduced, usually much narrower.</text>
      <biological_entity id="o5187" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="reduced" />
        <character is_modifier="false" modifier="usually much" name="width" src="d0_s4" value="narrower" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences nodding in bud, later suberect, few-flowered racemes, subglabrous to sparsely strigillose and/or glandular puberulent.</text>
      <biological_entity id="o5188" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character constraint="in bud" constraintid="o5189" is_modifier="false" name="orientation" src="d0_s5" value="nodding" />
        <character is_modifier="false" modifier="later" name="orientation" notes="" src="d0_s5" value="suberect" />
      </biological_entity>
      <biological_entity id="o5189" name="bud" name_original="bud" src="d0_s5" type="structure" />
      <biological_entity id="o5190" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="few-flowered" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s5" value="subglabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="[duplicate value]" src="d0_s5" value="strigillose" />
        <character char_type="range_value" from="subglabrous" name="pubescence" src="d0_s5" to="sparsely strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers sub­erect;</text>
      <biological_entity id="o5191" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>buds 2–5 × 1–2 mm;</text>
      <biological_entity id="o5192" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 1–6 (–15) mm;</text>
      <biological_entity id="o5193" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>floral-tube 0.6–1.2 × 0.8–1.8 mm, slightly raised subglabrous ring at mouth inside;</text>
      <biological_entity id="o5194" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s9" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s9" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5195" name="ring" name_original="ring" src="d0_s9" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s9" value="subglabrous" />
      </biological_entity>
      <biological_entity id="o5196" name="mouth" name_original="mouth" src="d0_s9" type="structure" />
      <relation from="o5194" id="r932" modifier="slightly" name="raised" negation="false" src="d0_s9" to="o5195" />
      <relation from="o5194" id="r933" name="at" negation="false" src="d0_s9" to="o5196" />
    </statement>
    <statement id="d0_s10">
      <text>sepals green to reddish purple, 1.5–5 × 0.6–1.5 mm, abaxial surface subglabrous to sparsely glandular;</text>
      <biological_entity id="o5197" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="green" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="reddish purple" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="reddish purple" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5198" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s10" value="subglabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="[duplicate value]" src="d0_s10" value="glandular" />
        <character char_type="range_value" from="subglabrous" name="pubescence" src="d0_s10" to="sparsely glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals usually pink to rose-purple, rarely white, narrowly obcordate, (1.7–) 2.5–6.5 (–8) × 1.6–3.5 mm, apical notch 0.5–1.2 mm;</text>
      <biological_entity id="o5199" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" notes="[duplicate value]" src="d0_s11" value="pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="rose-purple" />
        <character char_type="range_value" from="usually pink" name="coloration" src="d0_s11" to="rose-purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="white" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="obcordate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="atypical_length" src="d0_s11" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5200" name="notch" name_original="notch" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments cream to light pink, those of longer stamens 1.4–3.2 mm, those of shorter ones 0.7–2 mm;</text>
      <biological_entity id="o5201" name="filament" name_original="filaments" src="d0_s12" type="structure" constraint="stamen">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s12" value="cream" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s12" value="light pink" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s12" to="light pink" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o5202" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity constraint="shorter" id="o5203" name="one" name_original="ones" src="d0_s12" type="structure" />
      <relation from="o5201" id="r934" name="part_of" negation="false" src="d0_s12" to="o5202" />
      <relation from="o5201" id="r935" name="part_of" negation="false" src="d0_s12" to="o5203" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.3–0.6 × 0.2–0.4 mm;</text>
      <biological_entity id="o5204" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s13" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s13" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary often reddish purple, 6–20 mm, subgla­brous or sparsely strigillose and glandular puberulent;</text>
      <biological_entity id="o5205" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish purple" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s14" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style white, 1.2–2.5 mm, glabrous, stigma broadly clavate to subcapitate, entire, 0.9–1.5 × 0.4–0.7 mm, surrounded by longer anthers.</text>
      <biological_entity id="o5206" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" />
      </biological_entity>
      <biological_entity id="o5207" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="clavate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="clavate to subcapitate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s15" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s15" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o5208" name="anther" name_original="anthers" src="d0_s15" type="structure" />
      <relation from="o5207" id="r936" name="surrounded by" negation="false" src="d0_s15" to="o5208" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules slender, often reddish purple, 17–40 (–55) mm, surfaces subglabrous or with scattered hairs;</text>
      <biological_entity id="o5209" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="slender" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s16" value="reddish purple" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="55" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s16" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5210" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="subglabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="with scattered hairs" />
      </biological_entity>
      <biological_entity id="o5211" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s16" value="scattered" />
      </biological_entity>
      <relation from="o5210" id="r937" name="with" negation="false" src="d0_s16" to="o5211" />
    </statement>
    <statement id="d0_s17">
      <text>pedicel 5–35 (–68) mm.</text>
      <biological_entity id="o5212" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="68" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds narrowly obovoid, 0.7–1.4 × 0.3–0.5 mm, inconspicuous chalazal collar 0.1–0.2 mm wide, light-brown, surface reticulate (smooth);</text>
      <biological_entity id="o5213" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="obovoid" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s18" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s18" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="chalazal" id="o5214" name="collar" name_original="collar" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="inconspicuous" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s18" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" />
      </biological_entity>
      <biological_entity id="o5215" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s18" value="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>coma persistent, dull white, 2–4 mm. 2n = 36.</text>
      <biological_entity id="o5216" name="coma" name_original="coma" src="d0_s19" type="structure">
        <character is_modifier="false" name="duration" src="d0_s19" value="persistent" />
        <character is_modifier="false" name="reflectance" src="d0_s19" value="dull" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="white" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s19" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5217" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Epilobium anagallidifolium is widely but sparsely distributed in high montane-alpine and subarctic Eurasia, including Europe, Russia, China, and Japan.</discussion>
  <discussion>Epilobium anagallidifolium usually forms low clumps or mats, with stems nodding in bud and usually subglabrous below the inflorescence.  Many collections of E. anagallidifolium from eastern Canada and Greenland tend to be unusually tall (to 25 cm) and robust for the species, with somewhat larger, thicker leaves, and longer pedicels (to 60 mm).  Similarly large and robust specimens occur scattered in Yukon and Washington, and may result from occasional hybridization and introgression with sympatric species such as E. hornemannii or E. lactiflorum, which also have the CC chromosomal arrangement.  In an analysis of Fennoscandian populations of the Alpinae group, I. Kytövuori (1972) found a similar pattern of mostly smaller, sigmoidal plants of E. anagallidifolium with a small proportion of larger ones, and he also suggested the possibility of hybridization and/or introgression.</discussion>
  <discussion>Plants of Epilobium anagallidifolium, and indeed of the whole Alpinae group, from Haida Gwaii (the Queen Charlotte Islands) of British Columbia (J. A. Calder and R. L. Taylor 1968), are particularly distinctive com­pared to those on the mainland, and difficult to interpret.  The observed differences may be the result of hybridization with other sympatric species or a response to unique ecological conditions on the islands, reinforced by relative isolation from mainland British Columbia.</discussion>
  <discussion>The Linnaean name Epilobium alpinum has long been a source of nomenclatural confusion and instability, since it circumscribed at least four distinct species, especially E. anagallidifolium.  A proposal by P. C. Hoch et al. (1995) to permanently reject the name E. alpinum Linnaeus was approved.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist flats, stream banks, subarctic coastal marsh edges, high montane and alpine meadows and seeps.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist flats" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="subarctic coastal marsh edges" />
        <character name="habitat" value="high montane" />
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="seeps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–4500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Nfld. and Labr., N.W.T., Nunavut, Que., Yukon; Alaska, Calif., Colo., Idaho, Maine, Mont., Nev., N.H., Oreg., Utah, Wash., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>