<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Eucharidium</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) Greene" date="1887" rank="species">concinna</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 140.  1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section eucharidium;species concinna</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eucharidium</taxon_name>
    <taxon_name authority="Fischer &amp; C. A. Meyer" date="1836" rank="species">concinnum</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (St. Petersburg)</publication_title>
      <place_in_publication>2: 37.  1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus eucharidium;species concinnum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Red ribbons</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, to 40 cm, glabrous or puberulent.</text>
      <biological_entity id="o5917" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 5–25 mm;</text>
      <biological_entity id="o5918" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o5919" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate to elliptic or ovate, 1–4.5 cm.</text>
      <biological_entity id="o5920" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o5921" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="elliptic or ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemes, axis sub­erect or slightly recurved;</text>
      <biological_entity constraint="inflorescences" id="o5922" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o5923" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s3" value="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>buds pendent.</text>
      <biological_entity id="o5924" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: floral-tube 13–25 mm;</text>
      <biological_entity id="o5925" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o5926" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed together to 1 side, sometimes nearly separating, petallike, pink or red;</text>
      <biological_entity id="o5927" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o5928" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="together" name="orientation" src="d0_s6" value="reflexed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="1" />
        <character is_modifier="false" modifier="sometimes nearly" name="arrangement" notes="" src="d0_s6" value="separating" />
        <character is_modifier="false" name="shape" src="d0_s6" value="petallike" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red" />
      </biological_entity>
      <biological_entity id="o5929" name="side" name_original="side" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>corolla rotate, petals bright pink, usually white-streaked, narrowly fan-shaped, 10–30 mm, length 2 times width, tapered to a ± distinct claw, lobes ± equal or middle lobe wider, usually oblanceo­late;</text>
      <biological_entity id="o5930" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5931" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rotate" />
      </biological_entity>
      <biological_entity id="o5932" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="bright pink" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="white-streaked" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="fan--shaped" value_original="fan-shaped" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s7" value="2" />
        <character constraint="to claw" constraintid="o5933" is_modifier="false" name="shape" src="d0_s7" value="tapered" />
      </biological_entity>
      <biological_entity id="o5933" name="claw" name_original="claw" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="fusion" src="d0_s7" value="distinct" />
      </biological_entity>
      <biological_entity id="o5934" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" />
      </biological_entity>
      <biological_entity constraint="middle" id="o5935" name="lobe" name_original="lobe" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="wider" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 4, filaments not wider distally;</text>
      <biological_entity id="o5936" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5937" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="4" />
      </biological_entity>
      <biological_entity id="o5938" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not; distally" name="width" src="d0_s8" value="wider" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 8-grooved;</text>
      <biological_entity id="o5939" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5940" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="8-grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma exserted or not beyond anthers.</text>
      <biological_entity id="o5941" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5942" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" />
        <character name="position" src="d0_s10" value="not" />
      </biological_entity>
      <biological_entity id="o5943" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o5942" id="r1098" name="beyond" negation="false" src="d0_s10" to="o5943" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules 15–20 mm;</text>
    </statement>
    <statement id="d0_s12">
      <text>sessile.</text>
      <biological_entity id="o5944" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds reddish-brown, 2–3 mm, scaly, crest to 0.8 mm, conspicuous.</text>
      <biological_entity id="o5945" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s13" value="scaly" />
      </biological_entity>
      <biological_entity id="o5946" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="conspicuous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="California." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The two self-pollinating subspecies with smaller flowers probably arose independently from the outcrossing subsp. concinna.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stigmas exserted beyond anthers; petals 15–30 mm.</description>
      <determination>1a. Clarkia concinna subsp. concinna</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stigmas not exserted beyond anthers; petals 10–20 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals remaining connate only near tip; petal lobes prominent, blades gradually tapered to distinct claw.</description>
      <determination>1b. Clarkia concinna subsp. automixa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals remaining connate in distal 2/3; petal lobes short, blades abruptly tapered to inconspicuous claw.</description>
      <determination>1c. Clarkia concinna subsp. raichei</determination>
    </key_statement>
  </key>
</bio:treatment>