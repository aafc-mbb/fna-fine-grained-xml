<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="(S. F. Blake) J. R. Abbott" date="2011" rank="genus">RHINOTROPIS</taxon_name>
    <taxon_name authority="(Steyermark) J. R. Abbott" date="2011" rank="species">rimulicola</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">rimulicola</taxon_name>
    <taxon_hierarchy>family polygalaceae;genus rhinotropis;species rimulicola;variety rimulicola</taxon_hierarchy>
  </taxon_identification>
  <number>10a.</number>
  <other_name type="common_name">Steyermark’s milkwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowers (2.4–) 3–5.1 (–5.4) mm;</text>
      <biological_entity id="o2927" name="flower" name_original="flowers" src="d0_s0" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="5.4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s0" to="5.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sepals eciliate or infrequently with a few proximal cilia;</text>
      <biological_entity id="o2928" name="sepal" name_original="sepals" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eciliate" />
        <character name="pubescence" src="d0_s1" value="infrequently" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o2929" name="cilium" name_original="cilia" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="few" />
      </biological_entity>
      <relation from="o2928" id="r356" name="with" negation="false" src="d0_s1" to="o2929" />
    </statement>
    <statement id="d0_s2">
      <text>keel beak deltate or rounded, often obscure, (0–) 0.1–0.3 (–0.5) mm, base width equal to or greater than length;</text>
      <biological_entity constraint="keel" id="o2930" name="beak" name_original="beak" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="deltate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s2" value="obscure" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="0.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s2" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2931" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" src="d0_s2" value="equal" />
        <character is_modifier="false" name="size" src="d0_s2" value="greater than length" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>staminal column glabrous adaxially, except marginally.</text>
      <biological_entity constraint="staminal" id="o2932" name="column" name_original="column" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Seeds (1.7–) 1.9–2.7 mm, body (excluding aril and pubescence) (1–) 1.1–1.5 (–1.7) mm.</text>
      <biological_entity id="o2933" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="1.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s4" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>2n = 18.</text>
      <biological_entity id="o2934" name="body" name_original="body" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="1.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2935" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety rimulicola occurs in the Guadalupe Mountains in New Mexico (Eddy County) and Texas (Culberson County).  It is also known from the Sierra Diablo Mountains of Texas (Culberson and Hudspeth counties).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices of limestone boulders and cliffs in mesic oak-maple canyons, chaparral, open coniferous forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="of limestone boulders and cliffs" />
        <character name="habitat" value="limestone boulders" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="mesic oak-maple canyons" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="open coniferous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>