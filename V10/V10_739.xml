<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUGENIA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">uniflora</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 470.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus eugenia;species uniflora</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Surinam cherry</other_name>
  <other_name type="common_name">pitanga</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, to 10 m, glabrous except for few simple coppery hairs on buds, bracts, and bracteoles.</text>
      <biological_entity id="o3721" name="shrub" name_original="shrubs" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character constraint="except-for hairs" constraintid="o3723" is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
      </biological_entity>
      <biological_entity id="o3722" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character constraint="except-for hairs" constraintid="o3723" is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
      </biological_entity>
      <biological_entity id="o3723" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="few" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="simple" />
        <character is_modifier="true" name="coloration" src="d0_s0" value="coppery" />
      </biological_entity>
      <biological_entity id="o3724" name="bud" name_original="buds" src="d0_s0" type="structure" />
      <biological_entity id="o3725" name="bract" name_original="bracts" src="d0_s0" type="structure" />
      <biological_entity id="o3726" name="bracteole" name_original="bracteoles" src="d0_s0" type="structure" />
      <relation from="o3723" id="r636" name="on" negation="false" src="d0_s0" to="o3724" />
      <relation from="o3723" id="r637" name="on" negation="false" src="d0_s0" to="o3725" />
      <relation from="o3723" id="r638" name="on" negation="false" src="d0_s0" to="o3726" />
    </statement>
    <statement id="d0_s1">
      <text>Twigs slender, compressed distally;</text>
      <biological_entity id="o3727" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s1" value="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark red­dish, shredding, glandular.</text>
      <biological_entity id="o3728" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="red" />
        <character is_modifier="false" name="shape" src="d0_s2" value="dish" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves drying glossy pale green abaxially, darker adaxially;</text>
      <biological_entity id="o3729" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="condition" src="d0_s3" value="drying" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="glossy" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s3" value="pale green" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="darker" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole channeled, 1–3 mm;</text>
      <biological_entity id="o3730" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="channeled" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate, 3–6 × 1.5–3 cm, papery, base rounded, margins merging abruptly into edge of petiole, apex acute to acuminate, surfaces with numerous, small, raised glands, becoming punctate adaxially on older leaves.</text>
      <biological_entity id="o3731" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="papery" />
      </biological_entity>
      <biological_entity id="o3732" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" />
      </biological_entity>
      <biological_entity id="o3733" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o3734" name="edge" name_original="edge" src="d0_s5" type="structure" />
      <biological_entity id="o3735" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
      <biological_entity id="o3736" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acuminate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o3737" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="with numerous" name="size" src="d0_s5" value="small" />
        <character constraint="on leaves" constraintid="o3739" is_modifier="false" modifier="becoming" name="coloration_or_relief" src="d0_s5" value="punctate" />
      </biological_entity>
      <biological_entity id="o3738" name="gland" name_original="glands" src="d0_s5" type="structure" />
      <biological_entity id="o3739" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="older" />
      </biological_entity>
      <relation from="o3733" id="r639" name="merging" negation="false" src="d0_s5" to="o3734" />
      <relation from="o3733" id="r640" name="part_of" negation="false" src="d0_s5" to="o3735" />
      <relation from="o3737" id="r641" name="raised" negation="false" src="d0_s5" to="o3738" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences (1 or) 2–6-flowered, short racemes, often appearing fasciculate, flowers rarely solitary;</text>
      <biological_entity id="o3740" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-6-flowered" />
      </biological_entity>
      <biological_entity id="o3741" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" />
        <character is_modifier="false" modifier="often; rarely" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" />
      </biological_entity>
      <biological_entity id="o3742" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s6" value="fasciculate" />
      </biological_entity>
      <relation from="o3741" id="r642" modifier="often" name="appearing" negation="false" src="d0_s6" to="o3742" />
    </statement>
    <statement id="d0_s7">
      <text>axis 1–2 mm;</text>
      <biological_entity id="o3743" name="axis" name_original="axis" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bud obovoid, 3–5 mm;</text>
      <biological_entity id="o3744" name="bud" name_original="bud" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles caducous, oblong-lanceolate, 1 × 0.5 mm, base distinct, margins ciliate, apex acute.</text>
      <biological_entity id="o3745" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="caducous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong-lanceolate" />
        <character name="length" src="d0_s9" unit="mm" value="1" />
        <character name="width" src="d0_s9" unit="mm" value="0.5" />
      </biological_entity>
      <biological_entity id="o3746" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" />
      </biological_entity>
      <biological_entity id="o3747" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" />
      </biological_entity>
      <biological_entity id="o3748" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels gracile, 15–25 mm.</text>
      <biological_entity id="o3749" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="gracile" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: hypanthium campanulate, 8–ribbed, 1–1.5 mm;</text>
      <biological_entity id="o3750" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3751" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="8-ribbed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calyx lobes oblong, subequal, 2.5–4 × 1.5–2 mm, margins ciliate, apex rounded or acute;</text>
      <biological_entity id="o3752" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="calyx" id="o3753" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" />
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3754" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" />
      </biological_entity>
      <biological_entity id="o3755" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals obovate, 4–6 × 2.5–4 mm, margins ciliate, apex rounded;</text>
      <biological_entity id="o3756" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o3757" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3758" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s13" value="ciliate" />
      </biological_entity>
      <biological_entity id="o3759" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>disc 2–2.5 mm diam.;</text>
      <biological_entity id="o3760" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o3761" name="disc" name_original="disc" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens 40–70, 4–6 mm;</text>
      <biological_entity id="o3762" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o3763" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s15" to="70" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style 4–7 mm.</text>
      <biological_entity id="o3764" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o3765" name="style" name_original="style" src="d0_s16" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Berries deep bright red, globose, 12–15 mm diam., 8-costate;</text>
      <biological_entity id="o3766" name="berry" name_original="berries" src="d0_s17" type="structure">
        <character is_modifier="false" name="depth" src="d0_s17" value="deep" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="bright red" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s17" to="15" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="8-costate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>calyx persistent, erect.</text>
      <biological_entity id="o3767" name="calyx" name_original="calyx" src="d0_s18" type="structure">
        <character is_modifier="false" name="duration" src="d0_s18" value="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eugenia uniflora has escaped from cultivation in the flora area and is known from the central and southern parts of the peninsula.</discussion>
  <discussion>Eugenia uniflora has been widely cultivated since pre-Columbian times.  Its native range is unknown, but it is generally assumed to have originated in Brazil or, possibly, northern South America; R. McVaugh (1969) thought that southern Brazil was most likely.  The species is prized for its fruit and is also grown as a specimen tree or trained as a formal hedge.</discussion>
  <discussion>The Florida Exotic Pest Plant Council has listed Eugenia uniflora as a Class 1 invasive, a taxon that displaces native species or disrupts native habitats.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hammocks, distrubed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="distrubed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>