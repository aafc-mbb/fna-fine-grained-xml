<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="A. Jussieu" date="1832" rank="genus">GAYOPHYTUM</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1840" rank="species">diffusum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">diffusum</taxon_name>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus gayophytum;species diffusum;subspecies diffusum</taxon_hierarchy>
  </taxon_identification>
  <number>8a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowers: sepals 2–3 (–5) mm;</text>
      <biological_entity id="o997" name="flower" name_original="flowers" src="d0_s0" type="structure" />
      <biological_entity id="o998" name="sepal" name_original="sepals" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>petals 3–5 (–7) mm;</text>
      <biological_entity id="o999" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o1000" name="petal" name_original="petals" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stigma hemispheric, usually exserted beyond anthers of longer stamens at anthesis.</text>
      <biological_entity id="o1001" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o1003" name="anther" name_original="anthers" src="d0_s2" type="structure" />
      <biological_entity constraint="longer" id="o1004" name="stamen" name_original="stamens" src="d0_s2" type="structure" />
      <relation from="o1002" id="r149" modifier="usually" name="exserted beyond" negation="false" src="d0_s2" to="o1003" />
      <relation from="o1002" id="r150" name="part_of" negation="false" src="d0_s2" to="o1004" />
    </statement>
    <statement id="d0_s3">
      <text>2n = 28.</text>
      <biological_entity id="o1002" name="stigma" name_original="stigma" src="d0_s2" type="structure" constraint="stamen">
        <character is_modifier="false" name="shape" src="d0_s2" value="hemispheric" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1005" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies diffusum is very similar to Gayophytum eriospermum but is not known from the area of distribution of the latter.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open pine forests, sagebrush slopes, dry margins of meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open pine forests" />
        <character name="habitat" value="sagebrush slopes" />
        <character name="habitat" value="dry margins" constraint="of meadows" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Nev., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>