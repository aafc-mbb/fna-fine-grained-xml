<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Eucharidium</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1887" rank="species">breweri</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 141.  1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section eucharidium;species breweri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eucharidium</taxon_name>
    <taxon_name authority="A. Gray" date="1865" rank="species">breweri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 532.  1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus eucharidium;species breweri</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Brewer’s clarkia</other_name>
  <other_name type="common_name">fairy fans</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or decumbent, to 20 cm, glabrous or sparsely puberulent.</text>
      <biological_entity id="o5991" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole to 20 mm;</text>
      <biological_entity id="o5992" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o5993" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear to lance­olate, 2–5 cm.</text>
      <biological_entity id="o5994" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o5995" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemes, axis straight or re­curved;</text>
      <biological_entity constraint="inflorescences" id="o5996" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o5997" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" />
        <character name="course" src="d0_s3" value="re" />
        <character is_modifier="false" name="course" src="d0_s3" value="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>buds pendent.</text>
      <biological_entity id="o5998" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: floral-tube 20–35 mm;</text>
      <biological_entity id="o5999" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6000" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s5" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed together to 1 side, not petallike, green to magenta;</text>
      <biological_entity id="o6001" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6002" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="together" name="orientation" src="d0_s6" value="reflexed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="1" />
        <character is_modifier="false" modifier="not" name="shape" notes="" src="d0_s6" value="petallike" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s6" value="green" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s6" value="magenta" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s6" to="magenta" />
      </biological_entity>
      <biological_entity id="o6003" name="side" name_original="side" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>corolla rotate, petals pink, broadly fan-shaped, 15–25 mm, length equal to width, with­out claw, 3-lobed, middle lobe longer and much nar­rower, linear to oblanceolate;</text>
      <biological_entity id="o6004" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6005" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rotate" />
      </biological_entity>
      <biological_entity id="o6006" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="fan--shaped" value_original="fan-shaped" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s7" value="equal" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="3-lobed" />
      </biological_entity>
      <biological_entity id="o6007" name="claw" name_original="claw" src="d0_s7" type="structure" />
      <biological_entity constraint="middle" id="o6008" name="lobe" name_original="lobe" src="d0_s7" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="longer" />
        <character is_modifier="false" modifier="much" name="shape" notes="[duplicate value]" src="d0_s7" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="oblanceolate" />
        <character char_type="range_value" from="linear" modifier="much" name="shape" src="d0_s7" to="oblanceolate" />
      </biological_entity>
      <relation from="o6006" id="r1107" modifier="with" name="out" negation="false" src="d0_s7" to="o6007" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 4, filaments wider distally;</text>
      <biological_entity id="o6009" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6010" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="4" />
      </biological_entity>
      <biological_entity id="o6011" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="width" src="d0_s8" value="wider" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary inconspicuously grooved;</text>
      <biological_entity id="o6012" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6013" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="inconspicuously" name="architecture" src="d0_s9" value="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma exserted beyond anthers.</text>
      <biological_entity id="o6014" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6015" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o6016" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o6015" id="r1108" name="exserted beyond" negation="false" src="d0_s10" to="o6016" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules 15–40 mm;</text>
    </statement>
    <statement id="d0_s12">
      <text>subsessile.</text>
      <biological_entity id="o6017" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="40" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds reddish-brown, 2–3 mm, scaly-tuberculate, crest to 0.8 mm, conspicuous.</text>
      <biological_entity id="o6018" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="scaly-tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o6019" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="conspicuous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6020" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Clarkia breweri is restricted to dry woodlands and chaparral west of the Central Valley from the San Francisco Bay area into the southern Coast Ranges in Fresno, Monterey, and San Benito counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodlands, chaparral.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>