<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Warren L. Wagner</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">TARAXIA</taxon_name>
    <place_of_publication>
      <publication_title>Nat. Pflanzenfam.</publication_title>
      <place_in_publication>96[III,7]: 216.  1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus taraxia</taxon_hierarchy>
    <other_info_on_name type="etymology">Species Leontodon taraxacoides, alluding to similar leaves</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus [unranked] Taraxia Torrey &amp; A. Gray" date=" 1840" rank="genus">Oenothera</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 506.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(Nuttall) Munz" date="unknown" rank="section">Heterostemon</taxon_name>
    <taxon_hierarchy>genus oenothera;section heterostemon</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Heterostemon</taxon_name>
    <taxon_hierarchy>genus oenothera;subgenus heterostemon</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="[unranked] Primulopsis Torrey &amp; A. Gray" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_hierarchy>genus oenothera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Jepson" date="unknown" rank="subgenus">Taraxia</taxon_name>
    <taxon_hierarchy>genus oenothera;subgenus taraxia</taxon_hierarchy>
  </taxon_identification>
  <number>9.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, fleshy perennial, acaulescent;</text>
    </statement>
    <statement id="d0_s1">
      <text>with thick or slender, sometimes woody taproot, some­times branched and then usually producing new rosettes.</text>
      <biological_entity id="o4125" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="texture" src="d0_s0" value="fleshy" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" />
      </biological_entity>
      <biological_entity id="o4126" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="thick" />
        <character is_modifier="true" name="size" src="d0_s1" value="slender" />
        <character is_modifier="true" modifier="sometimes" name="texture" src="d0_s1" value="woody" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" />
      </biological_entity>
      <biological_entity id="o4127" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" />
      </biological_entity>
      <relation from="o4125" id="r507" name="with" negation="false" src="d0_s1" to="o4126" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in a basal rosette;</text>
      <biological_entity id="o4128" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o4129" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <relation from="o4128" id="r508" name="in" negation="false" src="d0_s2" to="o4129" />
    </statement>
    <statement id="d0_s3">
      <text>stipules absent;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o4130" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade margins subentire to deeply sinuate or pinnatifid.</text>
      <biological_entity constraint="blade" id="o4131" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="subentire" />
        <character is_modifier="false" modifier="deeply" name="shape" notes="[duplicate value]" src="d0_s5" value="sinuate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="pinnatifid" />
        <character char_type="range_value" from="subentire" name="shape" src="d0_s5" to="deeply sinuate or pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences solitary flowers in leaf-axils.</text>
      <biological_entity id="o4132" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o4133" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" />
      </biological_entity>
      <biological_entity id="o4134" name="leaf-axil" name_original="leaf-axils" src="d0_s6" type="structure" />
      <relation from="o4133" id="r509" name="in" negation="false" src="d0_s6" to="o4134" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual, actinomorphic, buds erect;</text>
      <biological_entity id="o4135" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="actinomorphic" />
      </biological_entity>
      <biological_entity id="o4136" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral-tube deciduous (with sepals, petals, and stamens) after anthesis, with fleshy basal nectary;</text>
      <biological_entity id="o4137" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character constraint="after anthesis" is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4138" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character is_modifier="true" name="texture" src="d0_s8" value="fleshy" />
      </biological_entity>
      <relation from="o4137" id="r510" name="with" negation="false" src="d0_s8" to="o4138" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 4, reflexed separately;</text>
      <biological_entity id="o4139" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" />
        <character is_modifier="false" modifier="separately" name="orientation" src="d0_s9" value="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 4, usually yellow, rarely white, without spots, usually fading orange, strongly ultraviolet reflective, or sometimes not reflective near base;</text>
      <biological_entity id="o4140" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" modifier="usually" name="coloration" notes="" src="d0_s10" value="fading orange" />
        <character is_modifier="false" modifier="strongly" name="reflectance" src="d0_s10" value="reflective" />
        <character constraint="near base" constraintid="o4142" is_modifier="false" modifier="sometimes not" name="reflectance" src="d0_s10" value="reflective" />
      </biological_entity>
      <biological_entity id="o4141" name="spot" name_original="spots" src="d0_s10" type="structure" />
      <biological_entity id="o4142" name="base" name_original="base" src="d0_s10" type="structure" />
      <relation from="o4140" id="r511" name="without" negation="false" src="d0_s10" to="o4141" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 8, in 2 unequal series, anthers basi­fixed, pollen shed singly;</text>
      <biological_entity id="o4143" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="8" />
      </biological_entity>
      <biological_entity id="o4144" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" />
        <character is_modifier="true" name="size" src="d0_s11" value="unequal" />
      </biological_entity>
      <biological_entity id="o4145" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s11" value="fixed" />
      </biological_entity>
      <biological_entity id="o4146" name="pollen" name_original="pollen" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="singly" />
      </biological_entity>
      <relation from="o4143" id="r512" name="in" negation="false" src="d0_s11" to="o4144" />
    </statement>
    <statement id="d0_s12">
      <text>ovary 4-locular, with a long, slender, sterile apical projection proximal to opening of floral-tube, projection without visible abscission lines at its junctures with floral-tube or fertile part of ovary, stigma entire or irregularly lobed, globose, surface unknown, probably wet and non-papillate.</text>
      <biological_entity id="o4147" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="4-locular" />
      </biological_entity>
      <biological_entity constraint="apical" id="o4148" name="projection" name_original="projection" src="d0_s12" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s12" value="long" />
        <character is_modifier="true" name="size" src="d0_s12" value="slender" />
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" />
        <character constraint="to opening" constraintid="o4149" is_modifier="false" name="position" src="d0_s12" value="proximal" />
      </biological_entity>
      <biological_entity id="o4149" name="opening" name_original="opening" src="d0_s12" type="structure" />
      <biological_entity id="o4150" name="floral-tube" name_original="floral-tube" src="d0_s12" type="structure" />
      <biological_entity id="o4151" name="projection" name_original="projection" src="d0_s12" type="structure" />
      <biological_entity id="o4152" name="line" name_original="lines" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="visible" />
        <character is_modifier="true" name="life_cycle" src="d0_s12" value="abscission" />
      </biological_entity>
      <biological_entity id="o4153" name="juncture" name_original="junctures" src="d0_s12" type="structure" />
      <biological_entity id="o4154" name="floral-tube" name_original="floral-tube" src="d0_s12" type="structure" />
      <biological_entity id="o4155" name="part" name_original="part" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="fertile" />
      </biological_entity>
      <biological_entity id="o4156" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <biological_entity id="o4157" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="entire" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s12" value="lobed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" />
      </biological_entity>
      <biological_entity id="o4158" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="probably" name="condition" src="d0_s12" value="wet" />
        <character is_modifier="false" name="relief" src="d0_s12" value="non-papillate" />
      </biological_entity>
      <relation from="o4147" id="r513" name="with" negation="false" src="d0_s12" to="o4148" />
      <relation from="o4149" id="r514" name="part_of" negation="false" src="d0_s12" to="o4150" />
      <relation from="o4151" id="r515" name="without" negation="false" src="d0_s12" to="o4152" />
      <relation from="o4152" id="r516" name="at" negation="false" src="d0_s12" to="o4153" />
      <relation from="o4153" id="r517" name="with" negation="false" src="d0_s12" to="o4154" />
      <relation from="o4153" id="r518" name="with" negation="false" src="d0_s12" to="o4155" />
      <relation from="o4155" id="r519" name="part_of" negation="false" src="d0_s12" to="o4156" />
    </statement>
    <statement id="d0_s13">
      <text>Fruit a capsule, straight or slightly irregularly curved, subterete to 4-angled, cylindric-lanceoloid or ovoid, or oblong-ellipsoid, irregularly locu­licidal, gradually tapering into a slender, sterile portion (4–) 15–180 mm, sometimes persistent 1+ years, often blackened, thin or thick-walled;</text>
      <biological_entity id="o4161" name="portion" name_original="portion" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="slender" />
        <character is_modifier="true" name="reproduction" src="d0_s13" value="sterile" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s13" to="180" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>subsessile.</text>
      <biological_entity id="o4159" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" />
        <character is_modifier="false" modifier="slightly irregularly" name="course" src="d0_s13" value="curved" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="subterete" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="4-angled" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="cylindric-lanceoloid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="oblong-ellipsoid" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s13" to="4-angled cylindric-lanceoloid or ovoid or oblong-ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="subterete" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="4-angled" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="cylindric-lanceoloid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="oblong-ellipsoid" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s13" to="4-angled cylindric-lanceoloid or ovoid or oblong-ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="subterete" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="4-angled" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="cylindric-lanceoloid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="oblong-ellipsoid" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s13" to="4-angled cylindric-lanceoloid or ovoid or oblong-ellipsoid" />
        <character constraint="into portion" constraintid="o4161" is_modifier="false" modifier="irregularly; gradually" name="shape" src="d0_s13" value="tapering" />
        <character is_modifier="false" modifier="sometimes" name="duration" notes="" src="d0_s13" value="persistent" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s13" value="blackened" />
        <character is_modifier="false" name="width" src="d0_s13" value="thin" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="thick-walled" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="subsessile" />
      </biological_entity>
      <biological_entity id="o4160" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" />
        <character is_modifier="false" modifier="slightly irregularly" name="course" src="d0_s13" value="curved" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="subterete" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="4-angled" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="cylindric-lanceoloid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="oblong-ellipsoid" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s13" to="4-angled cylindric-lanceoloid or ovoid or oblong-ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="subterete" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="4-angled" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="cylindric-lanceoloid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="oblong-ellipsoid" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s13" to="4-angled cylindric-lanceoloid or ovoid or oblong-ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="subterete" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="4-angled" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="cylindric-lanceoloid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="oblong-ellipsoid" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s13" to="4-angled cylindric-lanceoloid or ovoid or oblong-ellipsoid" />
        <character constraint="into portion" constraintid="o4161" is_modifier="false" modifier="irregularly; gradually" name="shape" src="d0_s13" value="tapering" />
        <character is_modifier="false" modifier="sometimes" name="duration" notes="" src="d0_s13" value="persistent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" upper_restricted="false" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s13" value="blackened" />
        <character is_modifier="false" name="width" src="d0_s13" value="thin" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="thick-walled" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds numerous, in 2 rows per locule, pitted or coarsely papillose.</text>
      <biological_entity id="o4163" name="row" name_original="rows" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" />
      </biological_entity>
      <biological_entity id="o4164" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <relation from="o4162" id="r520" name="in" negation="false" src="d0_s15" to="o4163" />
      <relation from="o4163" id="r521" name="per" negation="false" src="d0_s15" to="o4164" />
    </statement>
    <statement id="d0_s16">
      <text>= 7.</text>
      <biological_entity id="o4162" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="numerous" />
        <character is_modifier="false" name="relief" src="d0_s15" value="pitted" />
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s15" value="papillose" />
        <character name="quantity" src="d0_s16" value="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 4 (4 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Taraxia is known from the western United States and southwestern Canada in open, moist clay or sandy sites, usually at low to middle elevations.  Taraxia is characterized by its acaulescent habit, seeds in two rows per locule in unwinged, irregularly dehiscent capsules, and notably by having a relatively long, slender, sterile projection at the apex of the ovary that persists on the mature capsule after the floral tube and perianth detach.</discussion>
  <discussion>This distinctive group of species has been treated variously as a subgenus or section of Oenothera (J. Torrey and A. Gray 1838–1843, vol. 1; P. A. Munz 1965), as a section of Camissonia (P. H. Raven 1969), or as a separate genus (J. K. Small 1896).  Traditionally, the two acaulescent annual species now viewed as composing the genus Tetrapteron, which share with Taraxia a sterile apical projection on the ovary, have been included in this group (R. Raimann 1893; Raven 1969).  Munz (1965) included the six species in his Oenothera subg. Heterostemon, but separated the four perennials (as sect. Heterostemon) from the two annuals (as sect. Tetrapteron).  On the basis of additional information, W. L. Wagner et al. (2007) recognized the two annual species as the genus Tetrapteron.  R. A. Levin et al. (2004) found strong molecular support for Taraxia on a weakly supported branch sister to Clarkia + Gayophytum + Chylismiella, whereas the two annual species are strongly monophyletic on a weakly supported branch with Camissoniopsis and Neoholmgrenia.  Even though the molecular support for the clade of Clarkia + Gayophytum + Chylismiella + Taraxia is weak, this group of genera shares the feature of basifixed anthers, unlike the versatile anthers of all other genera of tribe Onagreae.  P. H. Raven (1964) first pointed out that the basifixed anthers in Taraxia are similar to those found in Clarkia.  Species of Taraxia are sometimes grown as ornamentals in rock gardens.  Reproductive features include: self-incompatible, flowers diurnal, outcrossing, and pollinated by small bees [T. ovata (E. G. Linsley et al. 1973), T. tanacetifolia (Linsley et al. 1963b)] or facultatively autogamous [T. breviflora, T. subacaulis (Raven 1969)].</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade margins usually subentire to sinuate or crisped, sometimes irregularly sinuate-lobed toward base, rarely pinnatifid; herbs sparsely to densely short-hirsute or strigillose, especially on leaf blade margins and ± veins.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades usually densely short-hirsute on margins and ± veins, sometimes sparsely so; capsules subterete, walls much distended by seeds.</description>
      <determination>1. Taraxia ovata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades glabrate, veins and margins rarely sparsely strigillose; capsules 4-angled, walls nearly flat, not noticeably distended by seeds.</description>
      <determination>2. Taraxia subacaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade margins pinnatifid; herbs usually sparsely to densely strigillose or short-hirtellous, hairs spreading or appressed.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Styles 9.5–20(–25) mm; stigmas exserted beyond anthers at anthesis; petals (8–)10–23 mm; herbs densely or, sometimes, sparsely short-hirtellous and/or strigillose.</description>
      <determination>3. Taraxia tanacetifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Styles 3–6.5 mm; stigmas surrounded by anthers at anthesis; petals 5–7(–9) mm; herbs densely to sparsely strigillose, sometimes also appressed-hirtellous, hairs spreading or appressed.</description>
      <determination>4. Taraxia breviflora</determination>
    </key_statement>
  </key>
</bio:treatment>