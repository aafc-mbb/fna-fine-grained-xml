<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007" rank="section">Gaura</taxon_name>
    <taxon_name authority="(Spach) W. L. Wagner &amp; Hoch" date="2007" rank="subsection">Schizocarya</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 168. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section gaura;subsection schizocarya</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schizocarya</taxon_name>
    <taxon_name authority="unknown" date="1835" rank="species">Spach</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Sci. Nat., Bot., sér.</publication_title>
      <place_in_publication>2, 4: 170.  1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus schizocarya;species spach</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaura</taxon_name>
    <taxon_name authority="Schizocarya (Spach) Endlicher" date="unknown" rank="species">[unranked]</taxon_name>
    <taxon_hierarchy>genus gaura;species [unranked]</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaura</taxon_name>
    <taxon_name authority="Schizocarya (Spach) P. H. Raven &amp; D. P. Gregory" date="unknown" rank="species">sect.</taxon_name>
    <taxon_hierarchy>genus gaura;species sect.</taxon_hierarchy>
  </taxon_identification>
  <number>17k.2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>from enlarged taproot.</text>
      <biological_entity id="o5330" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually unbranched, sometimes branched.</text>
      <biological_entity id="o5331" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="unbranched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences slender, nodding.</text>
      <biological_entity id="o5332" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="slender" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 4-merous, nearly actinomorphic, opening near sunset;</text>
      <biological_entity id="o5333" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="4-merous" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s4" value="actinomorphic" />
      </biological_entity>
      <biological_entity id="o5334" name="sunset" name_original="sunset" src="d0_s4" type="structure" />
      <relation from="o5333" id="r1045" name="opening near" negation="false" src="d0_s4" to="o5334" />
    </statement>
    <statement id="d0_s5">
      <text>floral-tube 1.5–5 mm;</text>
      <biological_entity id="o5335" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white, slightly unequal;</text>
      <biological_entity id="o5336" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s6" value="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments with minute basal scale.</text>
      <biological_entity id="o5337" name="filament" name_original="filaments" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o5338" name="scale" name_original="scale" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="minute" />
      </biological_entity>
      <relation from="o5337" id="r1046" name="with" negation="false" src="d0_s7" to="o5338" />
    </statement>
    <statement id="d0_s8">
      <text>Capsules reflexed in age, fusiform, terete, weakly 4-angled in distal 1/3, angles becoming broad and rounded in proximal part, tapering abruptly toward base;</text>
      <biological_entity id="o5339" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character constraint="in age" constraintid="o5340" is_modifier="false" name="orientation" src="d0_s8" value="reflexed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s8" value="terete" />
        <character constraint="in distal 1/3" constraintid="o5341" is_modifier="false" modifier="weakly" name="shape" src="d0_s8" value="4-angled" />
      </biological_entity>
      <biological_entity id="o5340" name="age" name_original="age" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o5341" name="1/3" name_original="1/3" src="d0_s8" type="structure" />
      <biological_entity constraint="proximal" id="o5343" name="part" name_original="part" src="d0_s8" type="structure" />
      <biological_entity id="o5344" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sessile.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 14.</text>
      <biological_entity id="o5342" name="angle" name_original="angles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="becoming" name="width" src="d0_s8" value="broad" />
        <character constraint="in proximal part" constraintid="o5343" is_modifier="false" name="shape" src="d0_s8" value="rounded" />
        <character constraint="toward base" constraintid="o5344" is_modifier="false" name="shape" notes="" src="d0_s8" value="tapering" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5345" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w, c United States, n, c Mexico; introduced in s South America, Asia (China, Japan), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="c United States" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c Mexico" establishment_means="native" />
        <character name="distribution" value="in s South America" establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Japan)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>