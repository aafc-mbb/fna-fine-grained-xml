<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Ludwigioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LUDWIGIA</taxon_name>
    <taxon_name authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007" rank="section">Isnardia</taxon_name>
    <taxon_name authority="Short &amp; R. Peter" date="1835" rank="species">polycarpa</taxon_name>
    <place_of_publication>
      <publication_title>Transylvania J. Med. Assoc. Sci.</publication_title>
      <place_in_publication>8: 581.  1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily ludwigioideae;genus ludwigia;section isnardia;species polycarpa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isnardia</taxon_name>
    <taxon_name authority="(Short &amp; R. Peter) Kuntze" date="unknown" rank="species">polycarpa</taxon_name>
    <taxon_hierarchy>genus isnardia;species polycarpa</taxon_hierarchy>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Manyfruit primrose-willow</other_name>
  <other_name type="common_name">false loosestrife</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs slender, with well-developed aerenchyma on sub­merged stems, forming stolons 2.5–15 (–22) cm, 1–2.3 mm thick, well branched.</text>
      <biological_entity id="o1817" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="22" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s0" to="2.3" to_unit="mm" />
        <character is_modifier="false" modifier="well" name="architecture" src="d0_s0" value="branched" />
      </biological_entity>
      <biological_entity id="o1818" name="aerenchymum" name_original="aerenchyma" src="d0_s0" type="structure">
        <character is_modifier="true" name="development" src="d0_s0" value="well-developed" />
      </biological_entity>
      <biological_entity id="o1819" name="sub" name_original="sub" src="d0_s0" type="structure" />
      <biological_entity id="o1820" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o1821" name="stolon" name_original="stolons" src="d0_s0" type="structure" />
      <relation from="o1817" id="r295" name="with" negation="false" src="d0_s0" to="o1818" />
      <relation from="o1818" id="r296" name="on" negation="false" src="d0_s0" to="o1819" />
      <relation from="o1819" id="r297" name="merged" negation="false" src="d0_s0" to="o1820" />
      <relation from="o1817" id="r298" name="forming" negation="false" src="d0_s0" to="o1821" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, slightly ridged, well branched, (10–) 25–60 (–85) cm, glabrate with raised ± strigillose lines decurrent from leaf-axils.</text>
      <biological_entity id="o1822" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s1" value="ridged" />
        <character is_modifier="false" modifier="well" name="architecture" src="d0_s1" value="branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="85" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character constraint="with lines" constraintid="o1823" is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" />
      </biological_entity>
      <biological_entity id="o1823" name="line" name_original="lines" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="raised" />
        <character is_modifier="true" modifier="more or less" name="pubescence" src="d0_s1" value="strigillose" />
        <character constraint="from leaf-axils" constraintid="o1824" is_modifier="false" name="shape" src="d0_s1" value="decurrent" />
      </biological_entity>
      <biological_entity id="o1824" name="leaf-axil" name_original="leaf-axils" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o1825" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules narrowly to broadly ovate, 0.1–0.4 × 0.1–0.3 mm;</text>
      <biological_entity id="o1826" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s3" value="ovate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s3" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s3" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stolons: leaves often clustered near apex of stolon, petiole 0–0.5 cm, blade narrowly elliptic or oblanceolate, 0.8–2 (–3.2) × 0.2–0.8 (–1.2) cm, base attenuate, margins entire or remotely denticulate, apex acute, surfaces glabrous;</text>
      <biological_entity id="o1827" name="stolon" name_original="stolons" src="d0_s4" type="structure" />
      <biological_entity id="o1828" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="near apex" constraintid="o1829" is_modifier="false" modifier="often" name="arrangement_or_growth_form" src="d0_s4" value="clustered" />
      </biological_entity>
      <biological_entity id="o1829" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o1830" name="stolon" name_original="stolon" src="d0_s4" type="structure" />
      <biological_entity id="o1831" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1832" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="3.2" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s4" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1833" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" />
      </biological_entity>
      <biological_entity id="o1834" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" />
        <character is_modifier="false" modifier="remotely" name="shape" src="d0_s4" value="denticulate" />
      </biological_entity>
      <biological_entity id="o1835" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" />
      </biological_entity>
      <biological_entity id="o1836" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" />
      </biological_entity>
      <relation from="o1829" id="r299" name="part_of" negation="false" src="d0_s4" to="o1830" />
    </statement>
    <statement id="d0_s5">
      <text>stems: petiole winged, 0.1–1 cm, blade very narrowly oblongelliptic, 3.5–11 × 0.4–1 (–1.7) cm, base very narrowly cuneate or long-attenuate, margins entire and densely, minutely papillose-serrulate with obscure hydathodal glands, apex narrowly acute or acuminate, surfaces glabrous;</text>
      <biological_entity id="o1837" name="stem" name_original="stems" src="d0_s5" type="structure" />
      <biological_entity id="o1838" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s5" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1839" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="very narrowly" name="shape" src="d0_s5" value="oblongelliptic" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s5" to="11" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1840" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="very narrowly" name="shape" src="d0_s5" value="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="long-attenuate" />
      </biological_entity>
      <biological_entity id="o1841" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" />
        <character constraint="with glands" constraintid="o1842" is_modifier="false" modifier="densely; minutely" name="architecture_or_shape" src="d0_s5" value="papillose-serrulate" />
      </biological_entity>
      <biological_entity id="o1842" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="obscure" />
      </biological_entity>
      <biological_entity id="o1843" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" />
      </biological_entity>
      <biological_entity id="o1844" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts not much reduced.</text>
      <biological_entity id="o1845" name="stem" name_original="stems" src="d0_s6" type="structure" />
      <biological_entity id="o1846" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not much" name="size" src="d0_s6" value="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences elongated, leafy spikes, flowers solitary in leaf-axils, sometimes borne almost to base of stems;</text>
      <biological_entity id="o1847" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="length" src="d0_s7" value="elongated" />
      </biological_entity>
      <biological_entity id="o1848" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="leafy" />
      </biological_entity>
      <biological_entity id="o1849" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character constraint="in leaf-axils" constraintid="o1850" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" />
      </biological_entity>
      <biological_entity id="o1850" name="leaf-axil" name_original="leaf-axils" src="d0_s7" type="structure" />
      <biological_entity id="o1851" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o1852" name="stem" name_original="stems" src="d0_s7" type="structure" />
      <relation from="o1849" id="r300" modifier="sometimes" name="borne" negation="false" src="d0_s7" to="o1851" />
      <relation from="o1849" id="r301" modifier="sometimes; sometimes" name="part_of" negation="false" src="d0_s7" to="o1852" />
    </statement>
    <statement id="d0_s8">
      <text>bracteoles attached 0.5–2.5 (–3) mm distal to base of ovary, linear-lanceolate, 3.5–6.5 (–8) × 0.4–1 (–1.3) mm, with a swollen base, margins minutely papillose-serrulate.</text>
      <biological_entity id="o1853" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s8" value="attached" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character constraint="to base" constraintid="o1854" is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="linear-lanceolate" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s8" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1854" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o1855" name="ovary" name_original="ovary" src="d0_s8" type="structure" />
      <biological_entity id="o1856" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="swollen" />
      </biological_entity>
      <biological_entity id="o1857" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s8" value="papillose-serrulate" />
      </biological_entity>
      <relation from="o1854" id="r302" name="part_of" negation="false" src="d0_s8" to="o1855" />
      <relation from="o1853" id="r303" name="with" negation="false" src="d0_s8" to="o1856" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals spreading horizontally with reflexed tips, pale green, narrowly ovate-deltate, 2.5–4.5 × 1.5–3.2 mm, margins entire, minutely papillose-serrulate, apex elongate-acuminate, surfaces glabrous;</text>
      <biological_entity id="o1858" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1859" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character constraint="with tips" constraintid="o1860" is_modifier="false" name="orientation" src="d0_s9" value="spreading" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="pale green" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="ovate-deltate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1860" name="tip" name_original="tips" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="reflexed" />
      </biological_entity>
      <biological_entity id="o1861" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s9" value="papillose-serrulate" />
      </biological_entity>
      <biological_entity id="o1862" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate-acuminate" />
      </biological_entity>
      <biological_entity id="o1863" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 0;</text>
      <biological_entity id="o1864" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1865" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments yellowish green, 0.7–1.5 mm, base dilated, anthers 0.5–0.9 × 0.5–0.7 mm;</text>
      <biological_entity id="o1866" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1867" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish green" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1868" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="dilated" />
      </biological_entity>
      <biological_entity id="o1869" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s11" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen shed in tetrads;</text>
      <biological_entity id="o1870" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1871" name="pollen" name_original="pollen" src="d0_s12" type="structure" />
      <biological_entity id="o1872" name="tetrad" name_original="tetrads" src="d0_s12" type="structure" />
      <relation from="o1871" id="r304" name="shed in" negation="false" src="d0_s12" to="o1872" />
    </statement>
    <statement id="d0_s13">
      <text>ovary oblong, barely 4-angled, 3–4.5 × 2–3.5 mm;</text>
      <biological_entity id="o1873" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1874" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" />
        <character is_modifier="false" modifier="barely" name="shape" src="d0_s13" value="4-angled" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>nectary disc elevated 0.5–0.8 mm on ovary apex, yellowish green, 1.8–3 mm diam., 4-lobed, glabrous;</text>
      <biological_entity id="o1875" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="nectary" id="o1876" name="disc" name_original="disc" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="elevated" />
        <character char_type="range_value" constraint="on ovary apex" constraintid="o1877" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s14" value="yellowish green" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="diameter" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" />
      </biological_entity>
      <biological_entity constraint="ovary" id="o1877" name="apex" name_original="apex" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style yellowish green, 0.5–0.8 mm, glabrous, stigma broadly clavate to subglobose, 0.4–0.8 × 0.3–0.6 mm, usually 4-lobed, not exserted beyond anthers.</text>
      <biological_entity id="o1878" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o1879" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellowish green" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" />
      </biological_entity>
      <biological_entity id="o1880" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s15" value="clavate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s15" value="subglobose" />
        <character char_type="range_value" from="broadly clavate" name="shape" src="d0_s15" to="subglobose" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s15" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s15" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s15" value="4-lobed" />
      </biological_entity>
      <biological_entity id="o1881" name="anther" name_original="anthers" src="d0_s15" type="structure" />
      <relation from="o1880" id="r305" name="exserted beyond" negation="true" src="d0_s15" to="o1881" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules oblong-obovoid, obscurely 4-angled, 4–7 × 2.5–5 mm, hard-walled, irregularly dehiscent, pedicel 0.1–0.3 mm.</text>
      <biological_entity id="o1882" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong-obovoid" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s16" value="4-angled" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s16" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="hard-walled" />
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s16" value="dehiscent" />
      </biological_entity>
      <biological_entity id="o1883" name="pedicel" name_original="pedicel" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds light-brown, narrowly oblong with curved ends, 0.5–0.6 × 0.2–0.3 mm, surface cells elongate parallel to seed length.</text>
      <biological_entity id="o1884" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="light-brown" />
        <character constraint="with ends" constraintid="o1885" is_modifier="false" modifier="narrowly" name="shape" src="d0_s17" value="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" notes="" src="d0_s17" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" notes="" src="d0_s17" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1885" name="end" name_original="ends" src="d0_s17" type="structure">
        <character is_modifier="true" name="course" src="d0_s17" value="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>2n = 32.</text>
      <biological_entity constraint="surface" id="o1886" name="cel" name_original="cells" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="elongate" />
        <character is_modifier="false" name="length" src="d0_s17" value="parallel" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1887" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ludwigia polycarpa, unlike all other species in sect. Isnardia, is distributed primarily in the central Midwest and Great Lakes regions, with one highly disjunct population recorded from Kootenai County, Idaho, which is presumably introduced.  This species has also been found scattered as far east as Connecticut and Massachusetts, and reports of it from Arkansas, Maine, Tennessee, and Vermont cannot be confirmed.  As indicated by C. I. Peng (1989), a report of this species from Alabama involved a natural hybrid between L. glandulosa and L. pilosa.</discussion>
  <discussion>The basal stolons formed by Ludwigia polycarpa tend to be shorter, more condensed, and more branched than those found in other species, and may be a morphological adaptation to perennial survival in the colder areas in which it grows.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ditches, moist prairies, alluvial ground of ponds, lakes, and rivers, marshes, swales, edges of lagoons, low fallow fields.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ditches" />
        <character name="habitat" value="moist prairies" />
        <character name="habitat" value="alluvial ground" constraint="of ponds , lakes , and rivers" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="edges" constraint="of lagoons , low fallow fields" />
        <character name="habitat" value="lagoons" />
        <character name="habitat" value="low fallow fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Conn., Idaho, Ill., Ind., Iowa, Kans., Ky., Mass., Mich., Minn., Mo., Nebr., Ohio, Pa., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>