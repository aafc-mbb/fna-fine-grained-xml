<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007" rank="section">Gaura</taxon_name>
    <taxon_name authority="(P. H. Raven &amp; D. P. Gregory) W. L. Wagner &amp; Hoch" date="2007" rank="subsection">Xenogaura</taxon_name>
    <taxon_name authority="(Bentham) W. L. Wagner, Hoch &amp; Zarucchi" date="2015" rank="species">hispida</taxon_name>
    <place_of_publication>
      <publication_title>PhytoKeys</publication_title>
      <place_in_publication>50: 26.  2015</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section gaura;subsection xenogaura;species hispida</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaura</taxon_name>
    <taxon_name authority="Bentham" date="1849" rank="species">hispida</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>288.  1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gaura;species hispida</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="(Spach) D. Dietrich" date="unknown" rank="species">crispa</taxon_name>
    <taxon_hierarchy>genus g.;species crispa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="(Spach) Torrey &amp; A. Gray" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_hierarchy>genus g.;species drummondii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="Scheele 1848" date="unknown" rank="species">roemeriana</taxon_name>
    <other_info_on_name>not Oenothera roemeriana Scheele 1849</other_info_on_name>
    <taxon_hierarchy>genus g.;species roemeriana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="unknown" rank="species">xenogaura</taxon_name>
    <taxon_hierarchy>genus o.;species xenogaura</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schizocarya</taxon_name>
    <taxon_name authority="Spach 1839" date="unknown" rank="species">crispa</taxon_name>
    <other_info_on_name>not O. crispa Schultes 1809</other_info_on_name>
    <taxon_hierarchy>genus schizocarya;species crispa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="Spach 1836" date="unknown" rank="species">drummondii</taxon_name>
    <other_info_on_name>not O. drummondii Hooker 1834</other_info_on_name>
    <taxon_hierarchy>genus s.;species drummondii</taxon_hierarchy>
  </taxon_identification>
  <number>44.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, spreading by rhizomes (forming colonies), strigillose, often also villous;</text>
      <biological_entity id="o5766" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>from taproot.</text>
      <biological_entity id="o5765" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character constraint="by rhizomes" constraintid="o5766" is_modifier="false" name="orientation" src="d0_s0" value="spreading" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s0" value="strigillose" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s0" value="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to decumbent, several-branched from base, usually also irregularly branched distally, sometimes with a single, unbranched stem, 20–60 (–120) cm.</text>
      <biological_entity id="o5767" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="ascending" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="decumbent" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="decumbent" />
        <character constraint="from base" constraintid="o5768" is_modifier="false" name="architecture" src="d0_s2" value="several-branched" />
        <character is_modifier="false" modifier="usually; irregularly; distally" name="architecture" notes="" src="d0_s2" value="branched" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="120" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5768" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o5769" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="single" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="unbranched" />
      </biological_entity>
      <relation from="o5767" id="r1107" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o5769" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, 0.5–7.5 (–9.5) × 0.1–2.2 cm, blade narrowly lanceolate to elliptic, margins subentire or shallowly sinuate-dentate.</text>
      <biological_entity id="o5770" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="2.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5771" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity id="o5772" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s3" to="elliptic" />
      </biological_entity>
      <biological_entity id="o5773" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subentire" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="sinuate-dentate" />
      </biological_entity>
      <relation from="o5770" id="r1108" name="in" negation="false" src="d0_s3" to="o5771" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers 4-merous, zygomorphic, opening near sunset;</text>
      <biological_entity id="o5774" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="4-merous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>floral-tube 4–14 mm;</text>
      <biological_entity id="o5775" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals 7–11 (–14) mm;</text>
      <biological_entity id="o5776" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="14" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white, fading red, slightly unequal, elliptic, 6–10 mm, clawed;</text>
      <biological_entity id="o5777" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="fading red" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s7" value="unequal" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="elliptic" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 4–8.5 mm, anthers 3–6 mm, pollen 90–100% fertile;</text>
      <biological_entity id="o5778" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5779" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5780" name="pollen" name_original="pollen" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="90-100%" name="reproduction" src="d0_s8" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 12–26 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o5781" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5782" name="stigma" name_original="stigma" src="d0_s9" type="structure" />
      <biological_entity id="o5783" name="anther" name_original="anthers" src="d0_s9" type="structure" />
      <relation from="o5782" id="r1109" name="exserted beyond" negation="false" src="d0_s9" to="o5783" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules erect, pyra­midal in distal 1/2, conspicuously bulging at base of distal pyramidal part, strongly 4-angled, conspicu­ously bulging at base, abruptly constricted to terete proximal part, 7–13 × 3–5 mm;</text>
      <biological_entity constraint="distal" id="o5785" name="1/2" name_original="1/2" src="d0_s10" type="structure" />
      <biological_entity id="o5786" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity constraint="distal" id="o5787" name="part" name_original="part" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="pyramidal" />
      </biological_entity>
      <biological_entity id="o5788" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o5789" name="part" name_original="part" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="terete" />
      </biological_entity>
      <relation from="o5784" id="r1110" name="in" negation="false" src="d0_s10" to="o5785" />
      <relation from="o5786" id="r1111" name="part_of" negation="false" src="d0_s10" to="o5787" />
    </statement>
    <statement id="d0_s11">
      <text>sessile.</text>
      <biological_entity id="o5784" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" />
        <character constraint="at base" constraintid="o5786" is_modifier="false" modifier="conspicuously" name="pubescence_or_shape" notes="" src="d0_s10" value="bulging" />
        <character is_modifier="false" modifier="strongly" name="shape" notes="" src="d0_s10" value="4-angled" />
        <character constraint="at base" constraintid="o5788" is_modifier="false" modifier="ously" name="pubescence_or_shape" src="d0_s10" value="bulging" />
        <character is_modifier="false" modifier="abruptly" name="size" notes="" src="d0_s10" value="constricted" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" notes="" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds (2 or) 3 or 4 (–8), reddish-brown, 2–2.5 × 1–1.3 mm. 2n = 28.</text>
      <biological_entity id="o5790" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="8" />
        <character name="quantity" src="d0_s12" value="4" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5791" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera hispida is native across the eastern half of Texas, south through Mexico to Oaxaca and Puebla; it is naturalized in Sevier County, Arkansas, coastal southern California, and Glynn County Georgia.</discussion>
  <discussion>P. H. Raven and D. P. Gregory (1972[1973]) reported Oenothera hispida to be self-incompatible.  It occasionally forms hybrids with O. suffrutescens.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy loam.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy loam" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Calif., Ga., Tex.; c Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="c Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>