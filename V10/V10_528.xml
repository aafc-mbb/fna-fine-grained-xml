<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="P. Browne" date="1756" rank="genus">CUPHEA</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 56.  1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus cuphea;species wrightii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cuphea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_name authority="Bacigalupi" date="unknown" rank="variety">nematopetala</taxon_name>
    <taxon_hierarchy>genus cuphea;species wrightii;variety nematopetala</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parsonsia</taxon_name>
    <taxon_name authority="(A. Gray) Kearney" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_hierarchy>genus parsonsia;species wrightii</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, 1–4 dm, with fibrous-roots.</text>
      <biological_entity id="o3415" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o3416" name="fibrou-root" name_original="fibrous-roots" src="d0_s0" type="structure" />
      <relation from="o3415" id="r423" name="with" negation="false" src="d0_s0" to="o3416" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sparsely branched, dark purple-red-glandular-setose, glandular-viscid.</text>
      <biological_entity id="o3417" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark purple-red-glandular-setose" />
        <character is_modifier="false" name="coating" src="d0_s1" value="glandular-viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite, petio­late proximally, sessile distally;</text>
      <biological_entity id="o3418" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" modifier="proximally; distally" name="architecture" src="d0_s2" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–10 [–22] mm;</text>
      <biological_entity id="o3419" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to lanceolate, 10–35 [–50] × 3–15 [–20] mm, base rounded to cuneate.</text>
      <biological_entity id="o3420" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="lanceolate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3421" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="rounded" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="cuneate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes leafy.</text>
      <biological_entity id="o3422" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels (1–) 4–6 mm.</text>
      <biological_entity id="o3423" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers alternate, solitary and interpetiolar, (with 2 or 3 axillary);</text>
      <biological_entity id="o3424" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral-tube pale abaxially, purple-red to purple-black with dark veins adaxially, 5–11 × 1 mm, purple-red-glandular-setose;</text>
      <biological_entity id="o3425" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s8" value="pale" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="purple-red" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="purple-black" />
        <character char_type="range_value" constraint="with veins" constraintid="o3426" from="purple-red" name="coloration" src="d0_s8" to="purple-black" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s8" to="11" to_unit="mm" />
        <character name="width" notes="" src="d0_s8" unit="mm" value="1" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="purple-red-glandular-setose" />
      </biological_entity>
      <biological_entity id="o3426" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>base rounded [or a descending spur], to 0.5 mm;</text>
      <biological_entity id="o3427" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner surface glabrous proximally, lightly villous to glabrous distal to stamens;</text>
      <biological_entity constraint="inner" id="o3428" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s10" value="glabrous" />
        <character is_modifier="false" modifier="lightly" name="pubescence" notes="[duplicate value]" src="d0_s10" value="villous" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s10" value="glabrous" />
        <character char_type="range_value" from="lightly villous" name="pubescence" src="d0_s10" to="glabrous" />
        <character constraint="to stamens" constraintid="o3429" is_modifier="false" name="position_or_shape" src="d0_s10" value="distal" />
      </biological_entity>
      <biological_entity id="o3429" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>epicalyx segments thick, 2 flanking the adaxialmost sepal terminated by a bristle;</text>
      <biological_entity constraint="epicalyx" id="o3430" name="segment" name_original="segments" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thick" />
        <character name="quantity" src="d0_s11" value="2" />
      </biological_entity>
      <biological_entity id="o3431" name="sepal" name_original="sepal" src="d0_s11" type="structure" />
      <biological_entity id="o3432" name="bristle" name_original="bristle" src="d0_s11" type="structure" />
      <relation from="o3430" id="r424" name="flanking the adaxialmost" negation="false" src="d0_s11" to="o3431" />
      <relation from="o3430" id="r425" name="terminated by a" negation="false" src="d0_s11" to="o3432" />
    </statement>
    <statement id="d0_s12">
      <text>sepals unequal, adaxialmost longer;</text>
      <biological_entity id="o3433" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" />
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 6, purple or rose [bicolor, with white abaxial petals], obovate to orbiculate, unequal, 4 abaxial 0.5–1 [–2.5] × 0.2 mm, 2 adaxial 1–2 [–5] × 0.7 mm;</text>
      <biological_entity id="o3434" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="rose" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="obovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="orbiculate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s13" to="orbiculate" />
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" />
        <character name="quantity" src="d0_s13" value="4" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3435" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s13" to="1" to_unit="mm" />
        <character name="width" src="d0_s13" unit="mm" value="0.2" />
        <character name="quantity" src="d0_s13" value="2" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3436" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s13" to="2" to_unit="mm" />
        <character name="width" src="d0_s13" unit="mm" value="0.7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 11, reaching or surpassing sinus of sepals.</text>
      <biological_entity id="o3437" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="11" />
        <character is_modifier="false" name="position_relational" src="d0_s14" value="reaching" />
        <character is_modifier="false" name="position_relational" src="d0_s14" value="surpassing" />
      </biological_entity>
      <biological_entity id="o3438" name="sinus" name_original="sinus" src="d0_s14" type="structure" />
      <biological_entity id="o3439" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <relation from="o3438" id="r426" name="part_of" negation="false" src="d0_s14" to="o3439" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 3–6 [or 7], oblongelliptic in outline, 2–2.5 × 1.8–2 mm, margin rounded.</text>
      <biological_entity id="o3440" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="6" />
        <character constraint="in outline" constraintid="o3441" is_modifier="false" name="shape" src="d0_s15" value="oblongelliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" notes="" src="d0_s15" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" notes="" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3441" name="outline" name_original="outline" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 20 (Mexico), 44 (Mexico).</text>
      <biological_entity id="o3442" name="margin" name_original="margin" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3443" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" />
        <character name="quantity" src="d0_s16" value="44" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cuphea wrightii reaches its northernmost distribution in the southeastern corner of Arizona.  Plants from Arizona with filiform petals (var. nematopetala) have been reported growing mixed with plants having normal obovate petals.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Local in moist, open hab­itats, pastures, roadsides, rocky washes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" modifier="local in" />
        <character name="habitat" value="open habitats" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="rocky washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1800[–2900] m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1000" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="2900" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>