<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007" rank="section">Gaura</taxon_name>
    <taxon_name authority="(P. H. Raven &amp; D. P. Gregory) W. L. Wagner &amp; Hoch" date="2007" rank="subsection">Stipogaura</taxon_name>
    <taxon_name authority="(Munz) W. L. Wagner &amp; Hoch" date="2007" rank="species">mckelveyae</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 213.  2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section gaura;subsection stipogaura;species mckelveyae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaura</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">villosa</taxon_name>
    <taxon_name authority="Munz" date="1938" rank="variety">mckelveyae</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>65: 214.  1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gaura;species villosa;variety mckelveyae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaura</taxon_name>
    <taxon_name authority="(Munz) P. H. Raven &amp; D. P. Gregory" date="unknown" rank="species">mckelveyae</taxon_name>
    <taxon_hierarchy>genus gaura;species mckelveyae</taxon_hierarchy>
  </taxon_identification>
  <number>39.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, clumped, long-villous, more sparsely so distally, hairs erect, 2–4 mm, also strigillose, sometimes glabrate distally or also sparsely glandular puberulent;</text>
      <biological_entity id="o5538" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="clumped" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="long-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>from twisted, woody rootstock.</text>
      <biological_entity id="o5539" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sparsely; distally" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s0" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigillose" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" src="d0_s0" value="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending, branched below or just above ground, branched also proximal to inflorescences, 30–70 (–120) cm.</text>
      <biological_entity id="o5540" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" />
        <character constraint="above ground" constraintid="o5541" is_modifier="false" modifier="below" name="architecture" src="d0_s2" value="branched" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="branched" />
        <character constraint="to inflorescences" constraintid="o5542" is_modifier="false" name="position" src="d0_s2" value="proximal" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="120" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="70" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5541" name="ground" name_original="ground" src="d0_s2" type="structure" />
      <biological_entity id="o5542" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, basal 3–17 × 0.8–2 cm, blade oblanceolate, cauline 1–6.5 × 0.1–1.5 cm, sessile, blade narrowly oblanceolate to elliptic, margins conspicu­ously sinuate-dentate, often undulate.</text>
      <biological_entity id="o5543" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5544" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o5545" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="17" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5546" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o5547" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
      </biological_entity>
      <biological_entity id="o5548" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s3" to="elliptic" />
      </biological_entity>
      <biological_entity id="o5549" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="ously" name="architecture_or_shape" src="d0_s3" value="sinuate-dentate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="undulate" />
      </biological_entity>
      <relation from="o5543" id="r1078" name="in" negation="false" src="d0_s3" to="o5544" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences slender.</text>
      <biological_entity id="o5550" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 4-merous, zygomorphic, opening near sunset;</text>
      <biological_entity id="o5551" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-merous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral-tube 2–3.5 mm;</text>
      <biological_entity id="o5552" name="floral-tube" name_original="floral-tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals 6–12 mm;</text>
      <biological_entity id="o5553" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, fading dark-pink to red, slightly unequal, elliptic-obovate, 7–11 mm, long-clawed;</text>
      <biological_entity id="o5554" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="fading dark-pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="red" />
        <character char_type="range_value" from="fading dark-pink" name="coloration" src="d0_s8" to="red" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s8" value="unequal" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic-obovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="long-clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens presented in lower 1/2 of flower, filaments 5–9 mm, lanate at very base, anthers 2–4 mm, pollen 90–100% fertile;</text>
      <biological_entity id="o5555" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity constraint="lower" id="o5556" name="1/2" name_original="1/2" src="d0_s9" type="structure" />
      <biological_entity id="o5557" name="flower" name_original="flower" src="d0_s9" type="structure" />
      <biological_entity id="o5558" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character constraint="at base" constraintid="o5559" is_modifier="false" name="pubescence" src="d0_s9" value="lanate" />
      </biological_entity>
      <biological_entity id="o5559" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o5560" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5561" name="pollen" name_original="pollen" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="90-100%" name="reproduction" src="d0_s9" value="fertile" />
      </biological_entity>
      <relation from="o5555" id="r1079" name="presented in" negation="false" src="d0_s9" to="o5556" />
      <relation from="o5556" id="r1080" name="part_of" negation="false" src="d0_s9" to="o5557" />
    </statement>
    <statement id="d0_s10">
      <text>style 9–16 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o5562" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5563" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o5564" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o5563" id="r1081" name="exserted beyond" negation="false" src="d0_s10" to="o5564" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules reflexed, lanceoloid to narrowly ovoid, narrowly 4-winged, 8–19 × 1.5–2 mm, tapering to a sterile stipe 3–9 mm.</text>
      <biological_entity id="o5565" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="lanceoloid" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s11" value="ovoid" />
        <character char_type="range_value" from="lanceoloid" name="shape" src="d0_s11" to="narrowly ovoid" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s11" value="4-winged" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="19" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
        <character constraint="to stipe" constraintid="o5566" is_modifier="false" name="shape" src="d0_s11" value="tapering" />
      </biological_entity>
      <biological_entity id="o5566" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="sterile" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds (1 or) 2–4, 2–3 × 1 mm, yellowish to reddish-brown.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 14.</text>
      <biological_entity id="o5567" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="4" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="1" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s12" value="yellowish" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s12" value="reddish-brown" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s12" to="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5568" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera mckelveyae, on the Rio Grande Plain, is found in an area bounded by from Dimmit and LaSalle counties east to Karnes and Refugio counties in the north, southward through south Texas, extending to northeastern Tamaulipas and adjacent Nuevo León.  P. H. Raven and D. P. Gregory (1972[1973]) found Oenothera mckelveyae to be self-incompatible.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soil.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>