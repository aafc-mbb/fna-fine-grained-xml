<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POLYGALA</taxon_name>
    <taxon_name authority="C. W. James" date="1957" rank="species">crenata</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>59: 53.  1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species crenata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygala</taxon_name>
    <taxon_name authority="Walter forma obovata S. F. Blake" date="1915" rank="species">polygama</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>17: 201.  1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus polygala;species polygama</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Scalloped milkwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs short-lived perennial (rarely biennial or annual), single or multistemmed, (1.2–) 2–3 (–3.5) dm, usually unbranched, rarely branched distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>from taproot or fibrous-root cluster.</text>
      <biological_entity id="o1636" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" />
        <character is_modifier="false" name="architecture_or_growth_form" src="d0_s0" value="multistemmed" />
        <character char_type="range_value" from="1.2" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="unbranched" />
        <character is_modifier="false" modifier="rarely; distally" name="architecture" src="d0_s0" value="branched" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sometimes laxly so, to nearly decumbent, glabrous.</text>
      <biological_entity id="o1637" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" modifier="sometimes laxly; laxly; nearly" name="growth_form_or_orientation" src="d0_s2" value="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or subsessile, or with narrowed petiolelike base to 2 mm;</text>
      <biological_entity id="o1638" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" />
      </biological_entity>
      <biological_entity id="o1639" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="narrowed" />
        <character is_modifier="true" name="shape" src="d0_s4" value="petiolelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade obovate or elliptic, sometimes becoming scalelike proximally, (5–) 8–23 × (2–) 3–8 (–9) mm, base cuneate, apex rounded, obtuse, or acute, surfaces glabrous.</text>
      <biological_entity id="o1640" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" />
        <character is_modifier="false" modifier="sometimes becoming; proximally" name="shape" src="d0_s5" value="scalelike" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s5" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="23" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s5" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1641" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" />
      </biological_entity>
      <biological_entity id="o1642" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" />
      </biological_entity>
      <biological_entity id="o1643" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes loosely cylindric, open, elongate, 7–10 (–15) × 1–1.5 cm;</text>
      <biological_entity id="o1644" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="loosely" name="shape" src="d0_s6" value="cylindric" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle 1–2 (–2.5) cm;</text>
      <biological_entity id="o1645" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts deciduous, ovate.</text>
      <biological_entity id="o1646" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 3–4 (–5) mm, glabrous.</text>
      <biological_entity id="o1647" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cleistogamous flowers present in racemes usually below, rarely just above, soil surface, sometimes in proximal leaf-axils later in season.</text>
      <biological_entity id="o1648" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="cleistogamous" />
        <character constraint="in racemes" constraintid="o1649" is_modifier="false" name="presence" src="d0_s10" value="absent" />
      </biological_entity>
      <biological_entity id="o1649" name="raceme" name_original="racemes" src="d0_s10" type="structure" />
      <biological_entity id="o1650" name="surface" name_original="surface" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o1651" name="leaf-axil" name_original="leaf-axils" src="d0_s10" type="structure">
        <character constraint="in season" constraintid="o1652" is_modifier="false" name="condition" src="d0_s10" value="later" />
      </biological_entity>
      <biological_entity id="o1652" name="season" name_original="season" src="d0_s10" type="structure" />
      <relation from="o1648" id="r207" modifier="rarely just; just; sometimes" name="in" negation="false" src="d0_s10" to="o1651" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers pink to pale-purple, 4–6 mm;</text>
      <biological_entity id="o1653" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="pale-purple" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="pale-purple" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals ovate or elliptic, 1.1–2 mm;</text>
      <biological_entity id="o1654" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elliptic" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>wings elliptic, ovate, or obovate, (3.3–) 4–5 × (1.5–) 2–2.7 mm, apex obtuse to bluntly rounded;</text>
      <biological_entity id="o1655" name="wing" name_original="wings" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovate" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="atypical_length" src="d0_s13" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s13" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1656" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="obtuse" />
        <character is_modifier="false" modifier="bluntly" name="shape" notes="[duplicate value]" src="d0_s13" value="rounded" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s13" to="bluntly rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>keel 3–4 mm, crest 2-parted, with 2–4 entire or divided lobes on each side.</text>
      <biological_entity id="o1657" name="keel" name_original="keel" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1658" name="crest" name_original="crest" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="2-parted" />
      </biological_entity>
      <biological_entity id="o1659" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s14" to="4" />
        <character is_modifier="true" name="shape" src="d0_s14" value="entire" />
        <character is_modifier="true" name="shape" src="d0_s14" value="divided" />
      </biological_entity>
      <biological_entity id="o1660" name="side" name_original="side" src="d0_s14" type="structure" />
      <relation from="o1658" id="r208" name="with" negation="false" src="d0_s14" to="o1659" />
      <relation from="o1659" id="r209" name="on" negation="false" src="d0_s14" to="o1660" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules broadly ellipsoid, 2–3.5 × 2–3 mm, margins narrowly winged, wings equal, crenate.</text>
      <biological_entity id="o1661" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="ellipsoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s15" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1662" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s15" value="winged" />
      </biological_entity>
      <biological_entity id="o1663" name="wing" name_original="wings" src="d0_s15" type="structure">
        <character is_modifier="false" name="variability" src="d0_s15" value="equal" />
        <character is_modifier="false" name="shape" src="d0_s15" value="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1.5–2 mm, pubescent;</text>
      <biological_entity id="o1664" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>aril 1 mm, somewhat helmetlike, lobes to 1/4 length of seed.</text>
      <biological_entity id="o1665" name="aril" name_original="aril" src="d0_s17" type="structure">
        <character name="some_measurement" src="d0_s17" unit="mm" value="1" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s17" value="helmetlike" />
      </biological_entity>
      <biological_entity id="o1666" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character char_type="range_value" from="0 length of seed" name="length" src="d0_s17" to="1/4 length of seed" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Polygala crenata is known only from the East Gulf Coastal Plain.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet flatwoods and savannas, sandy mesic hills, bogs, acidic swamps.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet flatwoods" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="sandy mesic hills" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="acidic swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>