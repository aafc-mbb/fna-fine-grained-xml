<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POLYGALA</taxon_name>
    <taxon_name authority="Shuttleworth ex A. Gray" date="1852" rank="species">leptostachys</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 41.  1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species leptostachys</taxon_hierarchy>
  </taxon_identification>
  <number>15.</number>
  <other_name type="common_name">Threadleaf milkwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, single-stemmed, 1.5–3.5 (–5) dm, unbranched or branched distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>from slender taproot, sometimes becoming fibrous-root cluster.</text>
      <biological_entity id="o1869" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="single-stemmed" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="3.5" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s0" value="branched" />
      </biological_entity>
      <biological_entity id="o1870" name="fibrou-root" name_original="fibrous-root" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, glabrous.</text>
      <biological_entity id="o1871" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves whorled, at least proximally, to alternate distally;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or subsessile;</text>
      <biological_entity id="o1872" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" />
        <character is_modifier="false" modifier="at-least proximally; proximally; distally" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly obovate to linear-spatulate proximally, linear to filiform distally, 3–30 × 0.5–1 (–2.5) mm, base cuneate, apex acute, surfaces glabrous.</text>
      <biological_entity id="o1873" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s5" value="obovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="linear-spatulate" />
        <character is_modifier="false" modifier="proximally" name="shape" notes="[duplicate value]" src="d0_s5" value="," />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="filiform" />
        <character char_type="range_value" from="narrowly obovate" modifier="distally" name="shape" src="d0_s5" to="linear-spatulate proximally" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1874" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" />
      </biological_entity>
      <biological_entity id="o1875" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" />
      </biological_entity>
      <biological_entity id="o1876" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes spikelike, narrowly cylin­dric, 0.5–6 × 0.2–0.4 cm;</text>
      <biological_entity id="o1877" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="spikelike" />
        <character char_type="range_value" from="0.5" from_unit="cm" modifier="narrowly" name="length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" modifier="narrowly" name="width" src="d0_s6" to="0.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle 0.7–3 cm;</text>
      <biological_entity id="o1878" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts deciduous, ovate to lanceolate-ovate.</text>
      <biological_entity id="o1879" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="lanceolate-ovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0–0.5 mm, glabrous.</text>
      <biological_entity id="o1880" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers white or greenish white, 1.5–2.8 mm;</text>
      <biological_entity id="o1881" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish white" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals ovate to elliptic, 0.6–1.1 mm;</text>
      <biological_entity id="o1882" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="elliptic" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="elliptic" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>wings elliptic or obovate, 1.5–2.6 × 0.8–1 mm, apex obtuse to bluntly rounded;</text>
      <biological_entity id="o1883" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1884" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="obtuse" />
        <character is_modifier="false" modifier="bluntly" name="shape" notes="[duplicate value]" src="d0_s12" value="rounded" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="bluntly rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keel 1.3–2.5 mm, crest 2-lobed, with 2 or 3 lobes on each side.</text>
      <biological_entity id="o1885" name="keel" name_original="keel" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1886" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="2-lobed" />
      </biological_entity>
      <biological_entity id="o1887" name="side" name_original="side" src="d0_s13" type="structure" />
      <relation from="o1886" id="r235" modifier="with 2 or 3lobes" name="on" negation="false" src="d0_s13" to="o1887" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules ellipsoid to oblong, 1.7–2.2 × 1–1.5 mm, margins not winged.</text>
      <biological_entity id="o1888" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="oblong" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s14" to="oblong" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s14" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1889" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.5–2 mm, glabrous;</text>
      <biological_entity id="o1890" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>aril 1 mm, lobes 1/2 length of seed.</text>
      <biological_entity id="o1891" name="aril" name_original="aril" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="1" />
      </biological_entity>
      <biological_entity id="o1892" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character name="length" src="d0_s16" value="1/2 length of seed" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Polygala leptostachys is most common in Florida and rare elsewhere in its range.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandhills, dry to xeric pine-oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandhills" />
        <character name="habitat" value="dry to xeric pine-oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>