<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POLYGALA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">sanguinea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 705.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species sanguinea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygala</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">viridescens</taxon_name>
    <taxon_hierarchy>genus polygala;species viridescens</taxon_hierarchy>
  </taxon_identification>
  <number>24.</number>
  <other_name type="common_name">Blood or purple or field milkwort</other_name>
  <other_name type="common_name">polygale sanguin</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, single-stemmed, (0.5–) 1–4 dm, usually branched distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>from taproot (or rarely fibrous-root cluster).</text>
      <biological_entity id="o2142" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="single-stemmed" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s0" value="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, glabrous.</text>
      <biological_entity id="o2143" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or subsessile;</text>
      <biological_entity id="o2144" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade spatulate proximally to linear or narrowly elliptic distally, (5–) 10–20 (–40) × (0.5–) 1–3 (–5) mm, base acute or obtuse, apex acute to acuminate, surfaces glabrous.</text>
      <biological_entity id="o2145" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="spatulate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" />
        <character is_modifier="false" modifier="narrowly; distally" name="arrangement_or_shape" src="d0_s5" value="elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_width" src="d0_s5" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2146" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" />
      </biological_entity>
      <biological_entity id="o2147" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acuminate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o2148" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes capi­tate to densely cylindric, (0.5–) 1–2 (–4) × 0.5–1.4 cm;</text>
      <biological_entity id="o2149" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="shape" src="d0_s6" value="cylindric" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s6" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s6" to="1.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle 0.3–2.5 (–3) cm;</text>
      <biological_entity id="o2150" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts subpersistent to tardily deciduous, subulate.</text>
      <biological_entity id="o2151" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" notes="[duplicate value]" src="d0_s8" value="subpersistent" />
        <character is_modifier="false" modifier="tardily" name="duration" notes="[duplicate value]" src="d0_s8" value="deciduous" />
        <character char_type="range_value" from="subpersistent" name="duration" src="d0_s8" to="tardily deciduous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0.4–1.5 mm, glabrous.</text>
      <biological_entity id="o2152" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers usually pink, purple, or reddish purple, rarely white, sometimes greenish tinged, sepals sometimes pink or white, 4–6 mm;</text>
      <biological_entity id="o2153" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="pink" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="reddish purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="greenish tinged" />
      </biological_entity>
      <biological_entity id="o2154" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals oval, elliptic-ovate, or lanceolate, 1–3 mm;</text>
      <biological_entity id="o2155" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oval" />
        <character is_modifier="false" name="shape" src="d0_s11" value="elliptic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="elliptic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>wings ovate to broadly elliptic, (2.6–) 4.5–6.3 × (1–) 2.5–3.5 mm, apex obtuse to broadly rounded, sometimes minutely apiculate, rarely acute;</text>
      <biological_entity id="o2156" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s12" value="elliptic" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="broadly elliptic" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="atypical_length" src="d0_s12" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s12" to="6.3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s12" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2157" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="obtuse" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s12" value="rounded" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="broadly rounded" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_shape" src="d0_s12" value="apiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s12" value="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keel 2.5–3 mm, crest 2-parted, with 2–4 lobes on each side.</text>
      <biological_entity id="o2158" name="keel" name_original="keel" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2159" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="2-parted" />
      </biological_entity>
      <biological_entity id="o2160" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s13" to="4" />
      </biological_entity>
      <biological_entity id="o2161" name="side" name_original="side" src="d0_s13" type="structure" />
      <relation from="o2159" id="r262" name="with" negation="false" src="d0_s13" to="o2160" />
      <relation from="o2160" id="r263" name="on" negation="false" src="d0_s13" to="o2161" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules usually with flattened, sterile base, cuneate-subglobose, 2.5–3 × 2–2.5 mm, margins not winged (sometimes with raised rim).</text>
      <biological_entity id="o2162" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="cuneate-subglobose" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2163" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="flattened" />
        <character is_modifier="true" name="reproduction" src="d0_s14" value="sterile" />
      </biological_entity>
      <biological_entity id="o2164" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
      <relation from="o2162" id="r264" name="with" negation="false" src="d0_s14" to="o2163" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.3–1.7 mm, pubescent;</text>
      <biological_entity id="o2165" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s15" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>aril 1–1.3 mm, lobes usually (1/2–) 2/3 to ± length of seed, rarely minute.</text>
      <biological_entity id="o2166" name="aril" name_original="aril" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2167" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s16" to="2/3" />
        <character is_modifier="false" modifier="rarely" name="more_or_les_length" notes="" src="d0_s16" value="minute" />
      </biological_entity>
      <biological_entity id="o2168" name="seed" name_original="seed" src="d0_s16" type="structure" />
    </statement>
  </description>
  <discussion>Polygala sanguinea is the only species of the genus in the flora area with the wings to twice the length of the keel.  Late season flowers can have much smaller wings, some as small as 2.6 × 1 mm.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, old fields, gravelly logging road margins, meadows, glades, bogs, flatwoods, open woods.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="road margins" modifier="gravelly logging" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>