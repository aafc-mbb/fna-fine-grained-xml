<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">CHYLISMIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Chylismia</taxon_name>
    <taxon_name authority="(A. Gray) Small" date="1896" rank="species">brevipes</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 194.  1896</place_in_publication>
      <other_info_on_pub>(as Chylisma)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus chylismia;section chylismia;species brevipes</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="A. Gray in War Department [U.S.]" date="1857" rank="species">brevipes</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Rail. Rep.</publication_title>
      <place_in_publication>4(5): 87.  1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species brevipes</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(A. Gray) P. H. Raven" date="unknown" rank="species">brevipes</taxon_name>
    <taxon_hierarchy>genus camissonia;species brevipes</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, moderately to densely villous, sometimes strig­illose.</text>
      <biological_entity id="o638" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s0" value="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched, 3–75 cm.</text>
      <biological_entity id="o639" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="75" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves primarily in basal rosette, cauline greatly reduced when present, 6–14 × 1.5–3.5 cm;</text>
      <biological_entity id="o640" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s2" value="cauline" />
        <character is_modifier="false" modifier="when present" name="size" src="d0_s2" value="reduced" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o641" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <relation from="o640" id="r152" name="in" negation="false" src="d0_s2" to="o641" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1.5–4 (–11) cm;</text>
      <biological_entity id="o642" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="11" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade pinnately lobed or lateral lobes greatly reduced or absent, often mixed on same plant, terminal lobe usually ovate, rarely elliptic, 2.5–6.9 × 1.5–7 cm, margins irregularly dentate, oil cells on abaxial surface inconspicuous.</text>
      <biological_entity id="o643" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o644" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s4" value="reduced" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" />
        <character constraint="on plant" constraintid="o645" is_modifier="false" modifier="often" name="arrangement" src="d0_s4" value="mixed" />
      </biological_entity>
      <biological_entity id="o645" name="plant" name_original="plant" src="d0_s4" type="structure" />
      <biological_entity constraint="terminal" id="o646" name="lobe" name_original="lobe" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="ovate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_shape" src="d0_s4" value="elliptic" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="6.9" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o647" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="dentate" />
      </biological_entity>
      <biological_entity constraint="oil" id="o648" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o649" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" />
      </biological_entity>
      <relation from="o648" id="r153" name="on" negation="false" src="d0_s4" to="o649" />
    </statement>
    <statement id="d0_s5">
      <text>Racemes nodding, mostly elongating after flowers.</text>
      <biological_entity id="o650" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" />
        <character constraint="after flowers" constraintid="o651" is_modifier="false" modifier="mostly" name="length" src="d0_s5" value="elongating" />
      </biological_entity>
      <biological_entity id="o651" name="flower" name_original="flowers" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers opening at sunrise;</text>
      <biological_entity id="o652" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o653" name="sunrise" name_original="sunrise" src="d0_s6" type="structure" />
      <relation from="o652" id="r154" name="opening at" negation="false" src="d0_s6" to="o653" />
    </statement>
    <statement id="d0_s7">
      <text>buds some­times individually reflexed, without free tips or with subapical free tips 1–2 mm, or with minute, apical free tips less than 1 mm;</text>
      <biological_entity id="o654" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="individually" name="orientation" src="d0_s7" value="reflexed" />
      </biological_entity>
      <biological_entity id="o655" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o656" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o657" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="minute" />
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o654" id="r155" name="without" negation="false" src="d0_s7" to="o655" />
      <relation from="o654" id="r156" name="without" negation="false" src="d0_s7" to="o656" />
      <relation from="o654" id="r157" name="with" negation="false" src="d0_s7" to="o657" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube 3–8 mm, densely short-villous inside proximally;</text>
      <biological_entity id="o658" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s8" value="short-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 5–9 mm;</text>
      <biological_entity id="o659" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals bright-yellow, sometimes with red dots at base, fading yellow to orange or reddish, 3–18 mm;</text>
      <biological_entity id="o660" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="bright-yellow" />
        <character constraint="at base" constraintid="o661" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="red dots" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="fading yellow" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="orange" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="reddish" />
        <character char_type="range_value" from="fading yellow" name="coloration" notes="" src="d0_s10" to="orange or reddish" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o661" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens sub­equal, filaments 3–6 mm, anthers 2.5–6 mm, ciliate;</text>
      <biological_entity id="o662" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o663" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equal" />
      </biological_entity>
      <biological_entity id="o664" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" />
      </biological_entity>
      <relation from="o662" id="r158" name="sub ­" negation="false" src="d0_s11" to="o663" />
    </statement>
    <statement id="d0_s12">
      <text>style 10–18 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o665" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o666" name="stigma" name_original="stigma" src="d0_s12" type="structure" />
      <biological_entity id="o667" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o666" id="r159" name="exserted beyond" negation="false" src="d0_s12" to="o667" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules ascending or spreading, oblong-cylindrical, 18–92 mm;</text>
      <biological_entity id="o668" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-cylindrical" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s13" to="92" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel 2–20 mm.</text>
      <biological_entity id="o669" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1–1.5 mm.</text>
      <biological_entity id="o670" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>P. H. Raven (1962, 1969) determined this species to be self-incompatible.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flower buds individually reflexed; petals often fading reddish, 3–8 mm.</description>
      <determination>1c. Chylismia brevipes subsp. arizonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flower buds not individually reflexed; petals fading yellow to orange, 6–18 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants stout, villous; buds with subapical free tips 1–2 mm; petals usually without red dots at base.</description>
      <determination>1a. Chylismia brevipes subsp. brevipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants slender, usually strigillose, sometimes also villous proximally; buds with apical free tips 0–1 mm; petals often with red dots near base.</description>
      <determination>1b. Chylismia brevipes subsp. pallidula</determination>
    </key_statement>
  </key>
</bio:treatment>