<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Spach) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Godetia</taxon_name>
    <taxon_name authority="(Curtis) A. Nelson &amp; J. F. Macbride" date="1918" rank="species">purpurea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">purpurea</taxon_name>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section godetia;species purpurea;subspecies purpurea</taxon_hierarchy>
  </taxon_identification>
  <number>22a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades broadly lanceolate to elliptic or ovate, 1.5–4.5 cm, length usually less than 5 times width.</text>
      <biological_entity id="o6918" name="blade-leaf" name_original="leaf-blades" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s0" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s0" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s0" value="ovate" />
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s0" to="elliptic or ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="distance" src="d0_s0" to="4.5" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="l_w_ratio" src="d0_s0" value="0-5" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences dense ra­cemes.</text>
      <biological_entity id="o6919" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: petals lavender to purple or purplish red, often with darker spot near tip, 10–25 mm;</text>
      <biological_entity id="o6920" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o6921" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s2" value="lavender" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s2" value="purple" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s2" value="purplish red" />
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s2" to="purple or purplish red" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6922" name="tip" name_original="tip" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="darker spot" />
      </biological_entity>
      <relation from="o6921" id="r1211" modifier="often" name="with" negation="false" src="d0_s2" to="o6922" />
    </statement>
    <statement id="d0_s3">
      <text>stigma exserted beyond anthers.</text>
      <biological_entity id="o6923" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o6925" name="anther" name_original="anthers" src="d0_s3" type="structure" />
      <relation from="o6924" id="r1212" name="exserted beyond" negation="false" src="d0_s3" to="o6925" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 52.</text>
      <biological_entity id="o6924" name="stigma" name_original="stigma" src="d0_s3" type="structure" />
      <biological_entity constraint="2n" id="o6926" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="52" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies purpurea is widely distributed but uncommon in California and southern Oregon.</discussion>
  <discussion>Collections matching the original description and illustration are rare, probably because the grassland habitat in and around the Central Valley where it grew is very desirable for development and, therefore, much altered.  Intermediates with the two other subspecies are now more frequent than the typical subspecies.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, often in moist conditions.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist conditions" modifier="grasslands often in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>