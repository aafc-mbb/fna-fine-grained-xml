<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Spach) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Godetia</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>20: 283. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section godetia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Spach" date=" 1835" rank="genus">Godetia</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Nat. Vég.</publication_title>
      <place_in_publication>4: 386.  1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus godetia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clarkia</taxon_name>
    <taxon_hierarchy>genus clarkia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Reiche" date="unknown" rank="genus">Oenotheridium</taxon_name>
    <taxon_hierarchy>genus oenotheridium</taxon_hierarchy>
  </taxon_identification>
  <number>6e.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences: axis erect, or prostrate to decumbent (C. davyi, C. prostrata);</text>
      <biological_entity id="o6682" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
      <biological_entity id="o6683" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character is_modifier="false" name="growth_form_or_orientation" notes="[duplicate value]" src="d0_s0" value="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" notes="[duplicate value]" src="d0_s0" value="decumbent" />
        <character char_type="range_value" from="prostrate" name="growth_form_or_orientation" src="d0_s0" to="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>buds erect.</text>
      <biological_entity id="o6684" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o6685" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: floral-tube funnelform to obconical, 2–15 mm;</text>
      <biological_entity id="o6686" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o6687" name="floral-tube" name_original="floral-tube" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obconical" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals reflexed individually or in pairs;</text>
      <biological_entity id="o6688" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o6689" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character constraint="in pairs" is_modifier="false" modifier="individually" name="orientation" src="d0_s3" value="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="in pairs" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals lavender-pink to dark wine-red, shading white or yellow near middle or base, usually purplish red-spotted, obovate to obdeltate, unlobed, claw inconspicuous or absent;</text>
      <biological_entity id="o6690" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o6691" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="lavender-pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="dark wine-red" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="shading white" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="yellow" />
        <character char_type="range_value" from="lavender-pink" name="coloration" src="d0_s4" to="dark wine-red shading white or yellow" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="lavender-pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="dark wine-red" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="shading white" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s4" value="yellow" />
        <character char_type="range_value" constraint="near " constraintid="o6693" from="lavender-pink" name="coloration" src="d0_s4" to="dark wine-red shading white or yellow" />
        <character is_modifier="false" modifier="usually" name="coloration" notes="" src="d0_s4" value="purplish red-spotted" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="obovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="obdeltate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="obdeltate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" />
      </biological_entity>
      <biological_entity id="o6692" name="middle" name_original="middle" src="d0_s4" type="structure" />
      <biological_entity id="o6693" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o6694" name="claw" name_original="claw" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens 8, subequal.</text>
      <biological_entity id="o6695" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6696" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="8" />
        <character is_modifier="false" name="size" src="d0_s5" value="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules conspicuously 8-ribbed;</text>
    </statement>
    <statement id="d0_s7">
      <text>subsessile.</text>
      <biological_entity id="o6697" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="architecture_or_shape" src="d0_s6" value="8-ribbed" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 7 (6 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, nw Mexico, w South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
        <character name="distribution" value="w South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Clarkia tenella (Cavanilles) H. Lewis &amp; M. E. Lewis, with three subspecies, is known from Argentina and Chile, and is the only species of Clarkia not known to occur naturally in North America.</discussion>
  <discussion>Some of the common cultivars of Clarkia in the horticulture trade are members of sect. Godetia, with their relatively large bowl-shaped flowers, and some cultivars (particularly of C. speciosa) are still on the market as Godetia.</discussion>
  
</bio:treatment>