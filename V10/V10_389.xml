<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Spach) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Godetia</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1953" rank="species">speciosa</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1955" rank="subspecies">polyantha</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>20: 291. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section godetia;species speciosa;subspecies polyantha</taxon_hierarchy>
  </taxon_identification>
  <number>19d.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, branches on well-developed plants few, virgate, many-flowered.</text>
      <biological_entity id="o6817" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
      </biological_entity>
      <biological_entity id="o6818" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s0" value="virgate" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="many-flowered" />
      </biological_entity>
      <biological_entity id="o6819" name="plant" name_original="plants" src="d0_s0" type="structure">
        <character is_modifier="true" name="development" src="d0_s0" value="well-developed" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="few" />
      </biological_entity>
      <relation from="o6818" id="r1199" name="on" negation="false" src="d0_s0" to="o6819" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades linear to narrowly lanceolate.</text>
      <biological_entity id="o6820" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s1" value="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s1" value="lanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="narrowly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences open racemes or panicles.</text>
      <biological_entity id="o6821" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o6822" name="raceme" name_original="racemes" src="d0_s2" type="structure" />
      <biological_entity id="o6823" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
      <relation from="o6821" id="r1200" name="open" negation="false" src="d0_s2" to="o6822" />
      <relation from="o6821" id="r1201" name="open" negation="false" src="d0_s2" to="o6823" />
    </statement>
    <statement id="d0_s3">
      <text>Petals purple or laven­der, usually lighter toward base, with a purplish red spot near middle.</text>
      <biological_entity id="o6825" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o6826" name="spot" name_original="spot" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="purplish red" />
      </biological_entity>
      <biological_entity id="o6827" name="middle" name_original="middle" src="d0_s3" type="structure" />
      <relation from="o6824" id="r1202" name="with" negation="false" src="d0_s3" to="o6826" />
      <relation from="o6826" id="r1203" name="near" negation="false" src="d0_s3" to="o6827" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 18.</text>
      <biological_entity id="o6824" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s3" value="purple" />
        <character constraint="toward base" constraintid="o6825" is_modifier="false" modifier="usually" name="coloration" src="d0_s3" value="lighter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6828" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies polyantha is relatively common in Fresno, Kern, Madera, and Tulare counties in the foothills of the southern Sierra Nevada, and is uncommon in the Tehachapi Mountains in Kern and Los Angeles counties.  A report from Riverside County (cited in Calflora) is erroneous.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands." />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>