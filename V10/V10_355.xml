<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) P. H. Raven" date="1964" rank="section">Rhodanthos</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1955" rank="subsection">Primigenia</taxon_name>
    <taxon_name authority="(Lehmann) A. Nelson &amp; J. F. Macbride" date="1918" rank="species">amoena</taxon_name>
    <taxon_name authority="(Abrams ex Piper) H. Lewis &amp; M. E. Lewis" date="1955" rank="subspecies">caurina</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>20: 268. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section rhodanthos;subsection primigenia;species amoena;subspecies caurina</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Godetia</taxon_name>
    <taxon_name authority="Abrams ex Piper" date="1906" rank="species">caurina</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>11: 410.  1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus godetia;species caurina</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clarkia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">amoena</taxon_name>
    <taxon_name authority="(Abrams ex Piper) C. L. Hitchcock" date="unknown" rank="variety">caurina</taxon_name>
    <taxon_hierarchy>genus clarkia;species amoena;variety caurina</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">amoena</taxon_name>
    <taxon_name authority="(M. Peck) C. L. Hitchcock" date="unknown" rank="variety">pacifica</taxon_name>
    <taxon_hierarchy>genus c.;species amoena;variety pacifica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="M. Peck" date="unknown" rank="species">pacifica</taxon_name>
    <taxon_hierarchy>genus g.;species pacifica</taxon_hierarchy>
  </taxon_identification>
  <number>4b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, to 50 cm.</text>
      <biological_entity id="o6145" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences open racemes;</text>
      <biological_entity id="o6146" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o6147" name="raceme" name_original="racemes" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bracts ± narrowly lanceolate;</text>
      <biological_entity id="o6148" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less narrowly" name="shape" src="d0_s2" value="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes as long as or longer than subtending flowers.</text>
      <biological_entity id="o6149" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <biological_entity constraint="subtending" id="o6151" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o6150" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="longer" />
      </biological_entity>
      <relation from="o6149" id="r1123" name="as long as" negation="false" src="d0_s3" to="o6151" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: petals usually with bright red spot mid-blade or on distal part, less than 20 mm;</text>
      <biological_entity id="o6152" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o6153" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="spot" id="o6154" name="mid-blade" name_original="mid-blade" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="bright red" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6155" name="part" name_original="part" src="d0_s4" type="structure" />
      <relation from="o6153" id="r1124" name="with" negation="false" src="d0_s4" to="o6154" />
      <relation from="o6153" id="r1125" name="with" negation="false" src="d0_s4" to="o6155" />
    </statement>
    <statement id="d0_s5">
      <text>ovary cylindrical, 2–3 mm wide, 4-grooved, grooves sometimes inconspicuous;</text>
      <biological_entity id="o6156" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6157" name="ovary" name_original="ovary" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindrical" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-grooved" />
      </biological_entity>
      <biological_entity id="o6158" name="groove" name_original="grooves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s5" value="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stigma not exserted beyond anthers.</text>
      <biological_entity id="o6159" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6161" name="anther" name_original="anthers" src="d0_s6" type="structure" />
      <relation from="o6160" id="r1126" name="exserted beyond" negation="true" src="d0_s6" to="o6161" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 14.</text>
      <biological_entity id="o6160" name="stigma" name_original="stigma" src="d0_s6" type="structure" />
      <biological_entity constraint="2n" id="o6162" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies caurina, the only subspecies that does not occur in California, is found along the coast and along the Columbia River in Oregon and Washington to British Columbia (only on Vancouver Island).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sea bluffs, coastal slopes, along Columbia River.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sea bluffs" />
        <character name="habitat" value="coastal slopes" constraint="along columbia river" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>