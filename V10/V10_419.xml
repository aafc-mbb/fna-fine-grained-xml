<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Spach) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Phaeostoma</taxon_name>
    <taxon_name authority="K. E. Holsinger &amp; H. Lewis" date="1986" rank="subsection">Sympherica</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>73: 493. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section phaeostoma;subsection sympherica</taxon_hierarchy>
  </taxon_identification>
  <number>6h.5.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences open racemes, axis recurved at tip in bud;</text>
      <biological_entity id="o328" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
      <biological_entity id="o329" name="raceme" name_original="racemes" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" />
      </biological_entity>
      <biological_entity id="o330" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character constraint="at tip" constraintid="o331" is_modifier="false" name="orientation" src="d0_s0" value="recurved" />
      </biological_entity>
      <biological_entity id="o331" name="tip" name_original="tip" src="d0_s0" type="structure" />
      <biological_entity id="o332" name="bud" name_original="bud" src="d0_s0" type="structure" />
      <relation from="o331" id="r28" name="in" negation="false" src="d0_s0" to="o332" />
    </statement>
    <statement id="d0_s1">
      <text>buds pendent.</text>
      <biological_entity id="o333" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers protandrous;</text>
      <biological_entity id="o334" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s2" value="protandrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petals pink or purplish red fading white in middle, base purplish red, sometimes reddish or purple-flecked, 10–35 mm;</text>
      <biological_entity id="o335" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pink" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="purplish red" />
        <character constraint="in middle" constraintid="o336" is_modifier="false" name="coloration" src="d0_s3" value="fading white" />
      </biological_entity>
      <biological_entity id="o336" name="middle" name_original="middle" src="d0_s3" type="structure" />
      <biological_entity id="o337" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="purplish red" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s3" value="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="purple-flecked" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ovary 4-grooved;</text>
      <biological_entity id="o338" name="ovary" name_original="ovary" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="4-grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stigma exserted beyond anthers.</text>
      <biological_entity id="o339" name="stigma" name_original="stigma" src="d0_s5" type="structure" />
      <biological_entity id="o340" name="anther" name_original="anthers" src="d0_s5" type="structure" />
      <relation from="o339" id="r29" name="exserted beyond" negation="false" src="d0_s5" to="o340" />
    </statement>
  </description>
  <discussion>Species 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="California." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Ford, V. S. and L. D. Gottlieb.  2003.  Reassessment of phylogenetic relationships in Clarkia sect. Sympherica.  Amer. J. Bot. 90: 167–174.</reference>
  </references>
  
</bio:treatment>