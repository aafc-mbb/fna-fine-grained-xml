<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) Walpers" date="1843" rank="section">Megapterium</taxon_name>
    <taxon_name authority="W. L. Wagner" date="1986" rank="species">coryi</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>73: 475.  1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section megapterium;species coryi</taxon_hierarchy>
  </taxon_identification>
  <number>25.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs acaulescent or caulescent, densely strigillose and glandular puberulent distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a taproot.</text>
      <biological_entity id="o4704" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems densely leafy, 4–20 cm.</text>
      <biological_entity id="o4705" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s2" value="leafy" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette, sometimes also cauline, 5–16 × (0.2–) 0.3–0.5 (–0.7) cm;</text>
      <biological_entity id="o4706" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="position" notes="" src="d0_s3" value="cauline" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="16" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s3" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4707" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o4706" id="r934" name="in" negation="false" src="d0_s3" to="o4707" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.6–3.5 cm;</text>
      <biological_entity id="o4708" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear to narrowly lanceolate, margins entire or sometimes proximal 1/2 of blade remotely lobed, apex long-attenuate, acute to rounded.</text>
      <biological_entity id="o4709" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s5" value="lanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o4710" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s5" value="proximal" />
        <character constraint="of blade" constraintid="o4711" name="quantity" src="d0_s5" value="1/2" />
      </biological_entity>
      <biological_entity id="o4711" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="remotely" name="shape" src="d0_s5" value="lobed" />
      </biological_entity>
      <biological_entity id="o4712" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="long-attenuate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="rounded" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers usually 1–3, rarely more, opening per day near sunset, weakly scented;</text>
      <biological_entity id="o4713" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" modifier="rarely; rarely; weakly" name="odor" src="d0_s6" value="scented" />
      </biological_entity>
      <biological_entity id="o4714" name="day" name_original="day" src="d0_s6" type="structure" />
      <relation from="o4713" id="r935" modifier="rarely" name="opening per" negation="false" src="d0_s6" to="o4714" />
    </statement>
    <statement id="d0_s7">
      <text>buds with unequal free tips 0.7–1.2 mm;</text>
      <biological_entity id="o4715" name="bud" name_original="buds" src="d0_s7" type="structure" />
      <biological_entity id="o4716" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="unequal" />
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
      <relation from="o4715" id="r936" name="with" negation="false" src="d0_s7" to="o4716" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube (55–) 75–100 (–125) mm;</text>
      <biological_entity id="o4717" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="55" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="75" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="125" to_unit="mm" />
        <character char_type="range_value" from="75" from_unit="mm" name="some_measurement" src="d0_s8" to="100" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 34–40 mm;</text>
      <biological_entity id="o4718" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="34" from_unit="mm" name="some_measurement" src="d0_s9" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals lemon-yellow, fading orange, drying lavender to purple, broadly obovate, 35–43 mm, sometimes with terminal tooth;</text>
      <biological_entity id="o4719" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="lemon-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading orange" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="drying" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="lavender" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="purple" />
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s10" to="purple" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="obovate" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s10" to="43" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o4720" name="tooth" name_original="tooth" src="d0_s10" type="structure" />
      <relation from="o4719" id="r937" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o4720" />
    </statement>
    <statement id="d0_s11">
      <text>filaments 17–25 mm, anthers 14–17 mm;</text>
      <biological_entity id="o4721" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4722" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s11" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style (85–) 105–135 (–143) mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o4723" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="85" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="105" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="135" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="143" to_unit="mm" />
        <character char_type="range_value" from="105" from_unit="mm" name="some_measurement" src="d0_s12" to="135" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4724" name="stigma" name_original="stigma" src="d0_s12" type="structure" />
      <biological_entity id="o4725" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o4724" id="r938" name="exserted beyond" negation="false" src="d0_s12" to="o4725" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules leathery, lanceoloid to ovoid, winged, wings 4–6 mm wide, body 25–30 × 8 mm, dehiscent 1/4–1/3 their length;</text>
      <biological_entity id="o4726" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="texture" src="d0_s13" value="leathery" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="lanceoloid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ovoid" />
        <character char_type="range_value" from="lanceoloid" name="shape" src="d0_s13" to="ovoid" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="winged" />
      </biological_entity>
      <biological_entity id="o4727" name="wing" name_original="wings" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4728" name="body" name_original="body" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s13" to="30" to_unit="mm" />
        <character name="width" src="d0_s13" unit="mm" value="8" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="dehiscent" />
        <character char_type="range_value" from="1/4" name="length" src="d0_s13" to="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel 1–2 (–3) mm.</text>
      <biological_entity id="o4729" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds numerous, usually in 2 distinct rows per locule, often reduced to 1 row near apex, rarely 1 row throughout, obovoid to subcuboid, 2.5–4 × 2.5–3.5 mm. 2n = 42.</text>
      <biological_entity id="o4730" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="numerous" />
        <character constraint="to row" constraintid="o4733" is_modifier="false" modifier="often" name="size" notes="" src="d0_s15" value="reduced" />
      </biological_entity>
      <biological_entity id="o4731" name="row" name_original="rows" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" />
        <character is_modifier="true" name="fusion" src="d0_s15" value="distinct" />
      </biological_entity>
      <biological_entity id="o4732" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <biological_entity id="o4733" name="row" name_original="row" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" />
      </biological_entity>
      <biological_entity id="o4734" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character modifier="rarely" name="quantity" src="d0_s15" value="1" />
      </biological_entity>
      <biological_entity id="o4735" name="row" name_original="row" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s15" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4736" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="42" />
      </biological_entity>
      <relation from="o4730" id="r939" modifier="usually" name="in" negation="false" src="d0_s15" to="o4731" />
      <relation from="o4731" id="r940" name="per" negation="false" src="d0_s15" to="o4732" />
      <relation from="o4733" id="r941" name="near" negation="false" src="d0_s15" to="o4734" />
    </statement>
  </description>
  <discussion>Oenothera coryi is known only from Baylor, Callahan, Knox, Nolan, Taylor, and Throckmorton counties in north-central Texas and Crosby and Garza counties in the Texas Panhandle.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open grasslands, disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open grasslands" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>