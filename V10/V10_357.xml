<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) P. H. Raven" date="1964" rank="section">Rhodanthos</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1955" rank="subsection">Primigenia</taxon_name>
    <taxon_name authority="(Lehmann) A. Nelson &amp; J. F. Macbride" date="1918" rank="species">amoena</taxon_name>
    <taxon_name authority="(Douglas) H. Lewis &amp; M. E. Lewis" date="1955" rank="subspecies">lindleyi</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>20: 267. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section rhodanthos;subsection primigenia;species amoena;subspecies lindleyi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Douglas" date="2832" rank="species">lindleyi</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>55: plate 2832.  1828</place_in_publication>
      <other_info_on_pub>(as lindleyii)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species lindleyi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clarkia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">amoena</taxon_name>
    <taxon_name authority="(Douglas) C. L. Hitchcock" date="unknown" rank="variety">lindleyi</taxon_name>
    <taxon_hierarchy>genus clarkia;species amoena;variety lindleyi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Godetia</taxon_name>
    <taxon_name authority="(Lehmann) G. Don" date="unknown" rank="species">amoena</taxon_name>
    <taxon_name authority="(Douglas) Jepson" date="unknown" rank="variety">lindleyi</taxon_name>
    <taxon_hierarchy>genus godetia;species amoena;variety lindleyi</taxon_hierarchy>
  </taxon_identification>
  <number>4d.</number>
  <other_name type="common_name">Lindley’s clarkia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, to 200 cm.</text>
      <biological_entity id="o6178" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences congested ra­cemes;</text>
      <biological_entity id="o6179" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bracts narrowly lanceolate to lanceolate;</text>
      <biological_entity id="o6180" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes shorter than subtending flowers.</text>
      <biological_entity id="o6181" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character constraint="than subtending flowers" constraintid="o6182" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o6182" name="flower" name_original="flowers" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: petals without red spot near middle of blade or with very small spot or streak, 30–40 mm;</text>
      <biological_entity id="o6183" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o6184" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6185" name="streak" name_original="streak" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="very" name="coloration" src="d0_s4" value="small spot" />
      </biological_entity>
      <relation from="o6184" id="r1129" name="without red spot near middle of blade or with" negation="false" src="d0_s4" to="o6185" />
    </statement>
    <statement id="d0_s5">
      <text>ovary cylindrical, 2–3 mm wide, 4-grooved;</text>
      <biological_entity id="o6186" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6187" name="ovary" name_original="ovary" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindrical" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stigma exserted beyond anthers.</text>
      <biological_entity id="o6188" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6190" name="anther" name_original="anthers" src="d0_s6" type="structure" />
      <relation from="o6189" id="r1130" name="exserted beyond" negation="false" src="d0_s6" to="o6190" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 14.</text>
      <biological_entity id="o6189" name="stigma" name_original="stigma" src="d0_s6" type="structure" />
      <biological_entity constraint="2n" id="o6191" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies lindleyi is found in west-central Oregon and southwestern Washington in the Coast Ranges.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodlands, margins of fields, along railroad tracks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="margins" constraint="of fields" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="railroad tracks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>