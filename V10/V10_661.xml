<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">CHYLISMIA</taxon_name>
    <taxon_name authority="(P. H. Raven) W. L. Wagner &amp; Hoch" date="2007" rank="section">Lignothera</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 136. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus chylismia;section lignothera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="P. H. Raven" date="1962" rank="section">Lignothera</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>34: 76.  1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;section lignothera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Link" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(P. H. Raven) P. H. Raven" date="unknown" rank="section">Lignothera</taxon_name>
    <taxon_hierarchy>genus camissonia;section lignothera</taxon_hierarchy>
  </taxon_identification>
  <number>16b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual or perennial.</text>
      <biological_entity id="o1357" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
      <biological_entity id="o1358" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade unlobed, cordate-orbicular or deltate.</text>
      <biological_entity id="o1359" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate-orbicular" />
        <character is_modifier="false" name="shape" src="d0_s2" value="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers opening at sunset;</text>
      <biological_entity id="o1360" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o1361" name="sunset" name_original="sunset" src="d0_s3" type="structure" />
      <relation from="o1360" id="r285" name="opening at" negation="false" src="d0_s3" to="o1361" />
    </statement>
    <statement id="d0_s4">
      <text>floral-tube 4.5–40 mm;</text>
      <biological_entity id="o1362" name="floral-tube" name_original="floral-tube" src="d0_s4" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals yellow, without dots or flecks, fading brick-red or orange;</text>
      <biological_entity id="o1363" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="fading brick-red" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="orange" />
      </biological_entity>
      <biological_entity id="o1364" name="fleck" name_original="flecks" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration_or_relief" src="d0_s5" value="dots" />
      </biological_entity>
      <relation from="o1363" id="r286" name="without" negation="false" src="d0_s5" to="o1364" />
    </statement>
    <statement id="d0_s6">
      <text>pollen shed in tetrads.</text>
      <biological_entity id="o1365" name="pollen" name_original="pollen" src="d0_s6" type="structure" />
      <biological_entity id="o1366" name="tetrad" name_original="tetrads" src="d0_s6" type="structure" />
      <relation from="o1365" id="r287" name="shed in" negation="false" src="d0_s6" to="o1366" />
    </statement>
  </description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Section Lignothera consists of two diploid (2n = 14) species (four taxa) that occur on rocky slopes and in washes in the Mojave and western Sonoran Deserts.  Chylismia arenaria occurs from southeastern California into adjacent southwestern Arizona and barely to northern Sonora, Mexico; the more widespread C. cardiophylla occurs in that same region but also reaches to south-central Baja California, Mexico, farther east in Arizona, and north to the western and southern margins of Death Valley in Inyo County, California.  P. H. Raven (1962) considered this group to be an early evolutionary offshoot within Camissonia.  He revised his position (Raven 1969) to regard the late afternoon-opening flowers, pollen shed in tetrads, and semi-woody habit as specializations within Onagreae and in Camissonia, and, consequently, to regard sect. Lignothera as a derivative of sect. Chylismia and its long floral tubes an adaptation for hawkmoth pollination.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Floral tubes 4.5–14 mm; racemes congested; sepals 3–9 mm.</description>
      <determination>15. Chylismia cardiophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Floral tubes 18–40 mm; racemes open; sepals 8–15 mm.</description>
      <determination>16. Chylismia arenaria</determination>
    </key_statement>
  </key>
</bio:treatment>