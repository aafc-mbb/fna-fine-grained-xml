<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Leslie R. Landrum</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">LUMA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>3: 52.  1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus luma</taxon_hierarchy>
    <other_info_on_name type="etymology">Chilean Native American (Mapuche) name for hardwood of Amomyrtus luma</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Arrayán</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, glabrous or pubescent, hairs simple.</text>
      <biological_entity id="o3768" name="shrub" name_original="shrubs" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" />
      </biological_entity>
      <biological_entity id="o3769" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="small" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" />
      </biological_entity>
      <biological_entity id="o3770" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves opposite;</text>
      <biological_entity id="o3771" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade venation brochidodromous.</text>
      <biological_entity id="o3772" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="brochidodromous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 1 or 3-flowered, axillary, solitary flowers or dichasia.</text>
      <biological_entity id="o3773" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-flowered" value_original="1-or-3-flowered" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" />
      </biological_entity>
      <biological_entity id="o3774" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" />
      </biological_entity>
      <biological_entity id="o3775" name="dichasium" name_original="dichasia" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 4-merous, sessile or pedicellate;</text>
      <biological_entity id="o3776" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="4-merous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>hypanthium obconic to campanulate;</text>
      <biological_entity id="o3777" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="obconic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="campanulate" />
        <character char_type="range_value" from="obconic" name="shape" src="d0_s5" to="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>calyx green, lobes distinct;</text>
      <biological_entity id="o3778" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" />
      </biological_entity>
      <biological_entity id="o3779" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white;</text>
      <biological_entity id="o3780" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 170–300;</text>
      <biological_entity id="o3781" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="170" name="quantity" src="d0_s8" to="300" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 2-locular;</text>
      <biological_entity id="o3782" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="2-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovules 6–14 per locule.</text>
      <biological_entity id="o3783" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o3784" from="6" name="quantity" src="d0_s10" to="14" />
      </biological_entity>
      <biological_entity id="o3784" name="locule" name_original="locule" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruits berries, dark purple, subglobose, crowned by calyx lobes.</text>
      <biological_entity constraint="fruits" id="o3785" name="berry" name_original="berries" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" />
        <character constraint="by calyx lobes" constraintid="o3786" is_modifier="false" name="architecture" src="d0_s11" value="crowned" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o3786" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds 1–16, lenticular;</text>
      <biological_entity id="o3787" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="16" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lenticular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>seed-coat membranous;</text>
      <biological_entity id="o3788" name="seed-coat" name_original="seed-coat" src="d0_s13" type="structure">
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>embryo lenticular;</text>
      <biological_entity id="o3789" name="embryo" name_original="embryo" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lenticular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>cotyledons suborbicular, separate, thinly planoconvex;</text>
      <biological_entity id="o3790" name="cotyledon" name_original="cotyledons" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="suborbicular" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="separate" />
        <character is_modifier="false" modifier="thinly" name="shape" src="d0_s15" value="planoconvex" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>hypocotyls cylindri­cal, about as long as cotyledons.</text>
      <biological_entity id="o3791" name="hypocotyl" name_original="hypocotyls" src="d0_s16" type="structure" />
      <biological_entity id="o3792" name="cotyledon" name_original="cotyledons" src="d0_s16" type="structure" />
    </statement>
  </description>
  <discussion>Species 2 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced, California; South America (Argentina, Chile, Peru); introduced also in temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="California" establishment_means="introduced" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
        <character name="distribution" value="also in temperate regions" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>