<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Ludwigioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LUDWIGIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Ludwigia</taxon_name>
    <taxon_hierarchy>family onagraceae;subfamily ludwigioideae;genus ludwigia;section ludwigia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isnardia</taxon_name>
    <taxon_name authority="(de Candolle) Reichenbach" date="unknown" rank="subgenus">Ludwigiaria</taxon_name>
    <taxon_hierarchy>genus isnardia;subgenus ludwigiaria</taxon_hierarchy>
  </taxon_identification>
  <number>1f.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, roots often fusiform, thickened, and fascicled.</text>
      <biological_entity id="o1179" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
      </biological_entity>
      <biological_entity id="o1180" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s0" value="fusiform" />
        <character is_modifier="false" name="size_or_width" src="d0_s0" value="thickened" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, subterete to angled.</text>
      <biological_entity id="o1181" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s1" value="subterete" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s1" value="angled" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s1" to="angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate.</text>
      <biological_entity id="o1182" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 4-merous;</text>
      <biological_entity id="o1183" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="4-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals present, yellow;</text>
      <biological_entity id="o1184" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens as many as sepals;</text>
      <biological_entity id="o1185" name="stamen" name_original="stamens" src="d0_s5" type="structure" />
      <biological_entity id="o1186" name="sepal" name_original="sepals" src="d0_s5" type="structure" />
      <relation from="o1185" id="r209" name="as many as" negation="false" src="d0_s5" to="o1186" />
    </statement>
    <statement id="d0_s6">
      <text>pollen usually shed in tetrads.</text>
      <biological_entity id="o1187" name="pollen" name_original="pollen" src="d0_s6" type="structure" />
      <biological_entity id="o1188" name="tetrad" name_original="tetrads" src="d0_s6" type="structure" />
      <relation from="o1187" id="r210" name="shed in" negation="false" src="d0_s6" to="o1188" />
    </statement>
    <statement id="d0_s7">
      <text>Capsules globose to cuboid, 4-angled or terete, with hard walls, dehiscent by an apical pore.</text>
      <biological_entity id="o1189" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="globose" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="cuboid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="4-angled" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="terete" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s7" to="cuboid 4-angled or terete" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="globose" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="cuboid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="4-angled" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="terete" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s7" to="cuboid 4-angled or terete" />
        <character constraint="by apical pore" constraintid="o1191" is_modifier="false" name="dehiscence" notes="" src="d0_s7" value="dehiscent" />
      </biological_entity>
      <biological_entity id="o1190" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="true" name="texture" src="d0_s7" value="hard" />
      </biological_entity>
      <biological_entity constraint="apical" id="o1191" name="pore" name_original="pore" src="d0_s7" type="structure" />
      <relation from="o1189" id="r211" name="with" negation="false" src="d0_s7" to="o1190" />
    </statement>
    <statement id="d0_s8">
      <text>Seeds in several rows per locule, free, raphe an inconspicuous, longitudinal ridge.</text>
      <biological_entity id="o1192" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="free" />
      </biological_entity>
      <biological_entity id="o1193" name="row" name_original="rows" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="several" />
      </biological_entity>
      <biological_entity id="o1194" name="locule" name_original="locule" src="d0_s8" type="structure" />
      <biological_entity id="o1195" name="raphe" name_original="raphe" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" />
      </biological_entity>
      <relation from="o1192" id="r212" name="in" negation="false" src="d0_s8" to="o1193" />
      <relation from="o1193" id="r213" name="per" negation="false" src="d0_s8" to="o1194" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 16.</text>
      <biological_entity id="o1196" name="ridge" name_original="ridge" src="d0_s8" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s8" value="longitudinal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1197" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 4 (4 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c, e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species of sect. Ludwigia are found mainly along the coastal plain of southeastern United States.</discussion>
  <discussion>Section Ludwigia consists of four perennial diploid species native to the southeastern United States; L. hirtella extends north to Rhode Island and L. alternifolia to Ontario and into the Great Plains of central North America (P. A. Munz 1965).  Ludwigia virgata is outcrossing, the other three are autogamous.  This strongly supported monophyletic section (P. H. Raven 1963[1964]; Liu S. H. et al. 2017) differs from the other sections found in North America by having fusiform, tuberous roots that may have an adaptive function in wet habitats, and capsules regularly dehiscing by an apical pore.  The free seeds are loose in the globose capsules until they dehisce through the pore, giving rise to the common names seedbox or rattlebox.  All species of sect. Ludwigia consistently shed their pollen in tetrads; J. Praglowski et al. (1983) reported for all taxa in this section that sometimes the tetrads appeared to be linked, suggesting polyads.</discussion>
  
</bio:treatment>