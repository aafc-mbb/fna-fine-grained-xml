<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) P. H. Raven" date="1964" rank="section">Rhodanthos</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1955" rank="subsection">Jugales</taxon_name>
    <taxon_name authority="(Piper) A. Nelson &amp; J. F. Macbride" date="1918" rank="species">gracilis</taxon_name>
    <taxon_name authority="(Jepson) Abdel-Hameed &amp; R. Snow" date="1968" rank="subspecies">tracyi</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>55: 1048. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section rhodanthos;subsection jugales;species gracilis;subspecies tracyi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Godetia</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="species">cylindrica</taxon_name>
    <taxon_name authority="Jepson" date="1936" rank="variety">tracyi</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Calif.</publication_title>
      <place_in_publication>2: 584.  1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus godetia;species cylindrica;variety tracyi</taxon_hierarchy>
  </taxon_identification>
  <number>9c.</number>
  <other_name type="common_name">Tracy’s clarkia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowers: petals pinkish laven­der, base red, unspotted, 20–30 mm;</text>
      <biological_entity id="o6389" name="flower" name_original="flowers" src="d0_s0" type="structure" />
      <biological_entity id="o6390" name="petal" name_original="petals" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="pinkish" />
      </biological_entity>
      <biological_entity id="o6391" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="unspotted" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s0" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stigma exserted beyond anthers.</text>
      <biological_entity id="o6392" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o6394" name="anther" name_original="anthers" src="d0_s1" type="structure" />
      <relation from="o6393" id="r1151" name="exserted beyond" negation="false" src="d0_s1" to="o6394" />
    </statement>
    <statement id="d0_s2">
      <text>2n = 28.</text>
      <biological_entity id="o6393" name="stigma" name_original="stigma" src="d0_s1" type="structure" />
      <biological_entity constraint="2n" id="o6395" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies tracyi is known from the Inner North Coast Ranges, mainly in Colusa, Lake, and Napa counties, less commonly in Humboldt, Mendocino, Sonoma, Tehama, and Yolo counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wood­lands, serpentine soil.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine soil" modifier="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>