<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) Walpers" date="1843" rank="section">Kneiffia</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Bot. Syst.</publication_title>
      <place_in_publication>2: 84. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section kneiffia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Spach" date=" 1835" rank="genus">Kneiffia</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Nat. Vég.</publication_title>
      <place_in_publication>4: 373.  1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus kneiffia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Spach" date="unknown" rank="genus">Blennoderma</taxon_name>
    <taxon_hierarchy>genus blennoderma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="[unranked] Blennoderma (Spach) Endlicher" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_hierarchy>genus oenothera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(Spach) Walpers" date="unknown" rank="section">Blennoderma</taxon_name>
    <taxon_hierarchy>genus oenothera;section blennoderma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(Spach) Reichenbach" date="unknown" rank="subgenus">Blennoderma</taxon_name>
    <taxon_hierarchy>genus oenothera;subgenus blennoderma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="[unranked] Kneiffia (Spach) Endlicher" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_hierarchy>genus oenothera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(Spach) Munz" date="unknown" rank="subgenus">Kneiffia</taxon_name>
    <taxon_hierarchy>genus oenothera;subgenus kneiffia</taxon_hierarchy>
  </taxon_identification>
  <number>17j.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual or perennial, caulescent;</text>
    </statement>
    <statement id="d0_s1">
      <text>from fibrous-roots or a taproot, sometimes somewhat fleshy, or sometimes producing rhizomes.</text>
      <biological_entity id="o4947" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" />
        <character is_modifier="false" modifier="sometimes somewhat" name="texture" src="d0_s1" value="fleshy" />
      </biological_entity>
      <biological_entity id="o4948" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
      <relation from="o4947" id="r971" modifier="sometimes" name="producing" negation="false" src="d0_s1" to="o4948" />
    </statement>
    <statement id="d0_s2">
      <text>Stems usually erect or ascending, sometimes decumbent, branched or unbranched.</text>
      <biological_entity id="o4949" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s2" value="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, cauline 3–10 (–13) cm;</text>
      <biological_entity id="o4950" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="13" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4951" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o4950" id="r972" name="in" negation="false" src="d0_s3" to="o4951" />
    </statement>
    <statement id="d0_s4">
      <text>blade margins entire, subentire, denticulate, or coarsely dentate.</text>
      <biological_entity constraint="blade" id="o4952" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers in axils of distal leaves.</text>
      <biological_entity id="o4953" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o4954" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" />
      </biological_entity>
      <biological_entity id="o4955" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o4956" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o4954" id="r973" name="in" negation="false" src="d0_s5" to="o4955" />
      <relation from="o4955" id="r974" name="part_of" negation="false" src="d0_s5" to="o4956" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers opening near sunrise, faintly scented;</text>
      <biological_entity id="o4957" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="faintly" name="odor" src="d0_s6" value="scented" />
      </biological_entity>
      <biological_entity id="o4958" name="sunrise" name_original="sunrise" src="d0_s6" type="structure" />
      <relation from="o4957" id="r975" name="opening near" negation="false" src="d0_s6" to="o4958" />
    </statement>
    <statement id="d0_s7">
      <text>buds erect, terete, with free tips;</text>
      <biological_entity id="o4959" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" />
        <character is_modifier="false" name="shape" src="d0_s7" value="terete" />
      </biological_entity>
      <biological_entity id="o4960" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
      </biological_entity>
      <relation from="o4959" id="r976" name="with" negation="false" src="d0_s7" to="o4960" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube 3–25 mm;</text>
      <biological_entity id="o4961" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals splitting along one suture, remaining coherent and reflexed as a unit at anthesis, or separating in pairs, or all separating individually;</text>
      <biological_entity id="o4962" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character constraint="along suture" constraintid="o4963" is_modifier="false" name="architecture_or_dehiscence" src="d0_s9" value="splitting" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s9" value="coherent" />
        <character constraint="as unit" constraintid="o4964" is_modifier="false" name="orientation" src="d0_s9" value="reflexed" />
        <character constraint="in pairs" constraintid="o4965" is_modifier="false" modifier="at anthesis" name="arrangement" notes="" src="d0_s9" value="separating" />
        <character is_modifier="false" modifier="individually" name="arrangement" notes="" src="d0_s9" value="separating" />
      </biological_entity>
      <biological_entity id="o4963" name="suture" name_original="suture" src="d0_s9" type="structure" />
      <biological_entity id="o4964" name="unit" name_original="unit" src="d0_s9" type="structure" />
      <biological_entity id="o4965" name="pair" name_original="pairs" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, fading pale-pink or lavender, or orangish pink or yellow, or not changing color, obcordate to obovate;</text>
      <biological_entity id="o4966" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orangish pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orangish pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" />
        <character is_modifier="false" modifier="not" name="character" src="d0_s10" value="color" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obcordate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obovate" />
        <character char_type="range_value" from="obcordate" name="shape" src="d0_s10" to="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigma deeply divided into 4 linear lobes.</text>
      <biological_entity id="o4967" name="stigma" name_original="stigma" src="d0_s11" type="structure">
        <character constraint="into lobes" constraintid="o4968" is_modifier="false" modifier="deeply" name="shape" src="d0_s11" value="divided" />
      </biological_entity>
      <biological_entity id="o4968" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="4" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules leathery, straight, usually clavate or oblong, sometimes ellipsoid, angled or winged, apex rounded to truncate or weakly emarginate, tapering to a sterile, pedicel-like base (stipe), valve midrib raised, initially apically dehiscent, eventually dehiscent nearly throughout;</text>
      <biological_entity id="o4969" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="leathery" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s12" value="clavate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s12" value="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="angled" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="winged" />
      </biological_entity>
      <biological_entity id="o4970" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded to truncate" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s12" value="emarginate" />
        <character constraint="to base" constraintid="o4971" is_modifier="false" name="shape" src="d0_s12" value="tapering" />
      </biological_entity>
      <biological_entity id="o4971" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" />
        <character is_modifier="true" name="shape" src="d0_s12" value="pedicel-like" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sessile.</text>
      <biological_entity constraint="valve" id="o4972" name="midrib" name_original="midrib" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="raised" />
        <character is_modifier="false" modifier="initially apically" name="dehiscence" src="d0_s12" value="dehiscent" />
        <character is_modifier="false" modifier="eventually; nearly throughout; throughout" name="dehiscence" src="d0_s12" value="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds usually numerous, clustered in each locule, ovoid, surface minutely papillose.</text>
      <biological_entity id="o4973" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s14" value="numerous" />
        <character constraint="in locule" constraintid="o4974" is_modifier="false" name="arrangement_or_growth_form" src="d0_s14" value="clustered" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="ovoid" />
      </biological_entity>
      <biological_entity id="o4974" name="locule" name_original="locule" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 14, 28, 42, 56.</text>
      <biological_entity id="o4975" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s14" value="papillose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4976" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" />
        <character name="quantity" src="d0_s15" value="28" />
        <character name="quantity" src="d0_s15" value="42" />
        <character name="quantity" src="d0_s15" value="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 6 (6 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Section Kneiffia consists of six species (seven taxa) widely distributed in the eastern half of the United States and adjacent Canada, at 0–1900 m elevation (G. B. Straley 1977).  Oenothera spachiana is the only annual species and occurs in open fields, prairies, rocky or sandy sites, and along roadsides; the remaining species are all perennial and occupy diverse habitats, including fresh and brackish swampy areas, wood margins, meadows, prairies, and sandy sites.  Three species are self-incompatible (O. fruticosa, O. pilosella, and O. riparia) and the other three are self-compatible (O. perennis, O. sessilis, and O. spachiana).  The flowers are diurnal, opening near sunrise and closing near sunset; in some populations of outcrossing species they may reopen for several days.  In the outcrossing taxa, the flowers are pollinated by bees (Halictidae and Bombus).  Oenothera spachiana and O. perennis are autogamous, the former often cleistogamous and the latter a PTH species.  Taxonomy of the section here differs somewhat from Straley in that molecular data (K. N. Krakos et al. 2014) indicate that O. sessilis should be recognized at the species level since the closest relative is O. fruticosa, not O. pilosella as previously thought.  Similarly, O. riparia is separated from O. fruticosa since it apparently is most closely related to O. perennis despite the morphological resemblance to O. fruticosa.</discussion>
  <references>
    <reference>Krakos, K. N., J. S. Reece, and P. H. Raven.  2014.  Molecular phylogenetics and reproductive biology of Oenothera section Kneiffia (Onagraceae).  Syst. Bot. 39: 523–532.</reference>
    <reference>Munz, P. A.  1937.  Studies in Onagraceae X.  The subgenus Kneiffia.  Bull. Torrey Bot. Club 64: 287–306.</reference>
    <reference>Straley, G. B.  1977.  Systematics of Oenothera sect. Kneiffia (Onagraceae).  Ann. Missouri Bot. Gard. 64: 381–424.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs annual, from a taproot; flowers in leaf axils in distal 1/2 of plant.</description>
      <determination>28. Oenothera spachiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs perennial, from fibrous roots, sometimes producing rhizomes; flowers in axils of distalmost few nodes of plant.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stigmas surrounded by anthers at anthesis; petals 5–10 mm; inflorescences nodding; pollen 40–70% fertile.</description>
      <determination>33. Oenothera perennis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stigmas exserted beyond anthers at anthesis; petals (8–)15–30 mm; inflorescences usually erect, sometimes nodding; pollen 85–100% fertile.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants usually exclusively hirsute, rarely glabrous, from fibrous roots and producing rhizomes.</description>
      <determination>32. Oenothera pilosella</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants usually strigillose, glandular puberulent, glabrate, or glabrous, sometimes villous, usually with fibrous or fleshy roots, not or rarely producing rhizomes.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules ellipsoid, 8–10 mm; plants densely strigillose, glabrate proximally.</description>
      <determination>29. Oenothera sessilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules clavate to oblong-clavate or oblong-ellipsoid, (5–)10–17(–20) mm; plants usually strigillose, and/or glandular puberulent, or glabrous, sometimes villous.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants usually moderately to densely strigillose throughout, sometimes and/or glandular puberulent or villous; cauline leaf blades 0.1–1(–1.7) cm wide, margins entire or weakly and remotely denticulate.</description>
      <determination>30. Oenothera fruticosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants glabrous or sparsely strigillose, sometimes also glandular puberulent distally; cauline leaf blades (0.2–)0.8–3(–5) cm wide, margins dentate to remotely denticulate.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Roots fibrous; leaf blade margins dentate to remotely denticulate; petioles 0.1–2(–6) cm; inland upland meadows, stream margins, edges of woods.</description>
      <determination>30. Oenothera fruticosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Roots somewhat fleshy, with adventitious roots where submerged; petioles 0–1.5 cm; leaf blade margins remotely denticulate; coastal marshes, slow-running rivers.</description>
      <determination>31. Oenothera riparia</determination>
    </key_statement>
  </key>
</bio:treatment>