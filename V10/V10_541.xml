<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LYTHRUM</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1840" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 482.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus lythrum;species californicum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lythrum</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">alatum</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">breviflorum</taxon_name>
    <taxon_hierarchy>genus lythrum;species alatum;variety breviflorum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">L.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alatum</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">linearifolium</taxon_name>
    <taxon_hierarchy>genus l.;species alatum;variety linearifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">L.</taxon_name>
    <taxon_name authority="(A. Gray) S. Watson" date="unknown" rank="species">breviflorum</taxon_name>
    <taxon_hierarchy>genus l.;species breviflorum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">L.</taxon_name>
    <taxon_name authority="(A. Gray) Small" date="unknown" rank="species">linearifolium</taxon_name>
    <taxon_hierarchy>genus l.;species linearifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">L.</taxon_name>
    <taxon_name authority="Nieuwland" date="unknown" rank="species">parvulum</taxon_name>
    <taxon_hierarchy>genus l.;species parvulum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">L.</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">sanfordii</taxon_name>
    <taxon_hierarchy>genus l.;species sanfordii</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, slender, 5–10 (–15) dm, whitish gray glaucous, glabrous.</text>
      <biological_entity id="o3670" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="whitish gray" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, much-branched distally.</text>
      <biological_entity id="o3671" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="much-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly alternate, sometimes opposite proximally, branch leaves gradually smaller than those on main-stem;</text>
      <biological_entity id="o3672" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s2" value="alternate" />
        <character is_modifier="false" modifier="sometimes; proximally" name="arrangement" src="d0_s2" value="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity constraint="branch" id="o3673" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblong-lanceolate proximally, mostly linear or linear-oblong distally, 7–60 × 1–7 mm, base rounded.</text>
      <biological_entity id="o3674" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s4" value="oblong-lanceolate" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="linear" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="linear-oblong" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3675" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemes.</text>
      <biological_entity constraint="inflorescences" id="o3676" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers alternate, subsessile, pedicel stout, distylous;</text>
      <biological_entity id="o3677" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" />
      </biological_entity>
      <biological_entity id="o3678" name="pedicel" name_original="pedicel" src="d0_s6" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s6" value="stout" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="distylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral-tube cylindrical, 5–7 × 1–1.5 mm;</text>
      <biological_entity id="o3679" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindrical" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>epicalyx segments equal or to 2 times length of sepals;</text>
      <biological_entity constraint="epicalyx" id="o3680" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" />
        <character name="variability" src="d0_s8" value="0-2 times length of sepals" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals bright rose-purple, obovate, 4–6 × 2–4.5 mm;</text>
      <biological_entity id="o3681" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright rose-purple" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nectary encircling base of ovary;</text>
      <biological_entity id="o3682" name="nectary" name_original="nectary" src="d0_s10" type="structure" constraint="ovary" />
      <biological_entity id="o3683" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o3684" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
      <relation from="o3682" id="r442" name="encircling" negation="false" src="d0_s10" to="o3683" />
      <relation from="o3682" id="r443" name="part_of" negation="false" src="d0_s10" to="o3684" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 6.</text>
      <biological_entity id="o3685" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules septicidal or septifragal.</text>
      <biological_entity id="o3686" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="septifragal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds ca. 50, obovoid to fusiform.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 20.</text>
      <biological_entity id="o3687" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="50" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="obovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="fusiform" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s13" to="fusiform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3688" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lythrum californicum sometimes is difficult to distinguish from L. alatum; it generally has a more open vegetative habit with narrowly linear leaves.  Problematic intermediates between L. californicum and L. alatum var. alatum occur in Kansas and Oklahoma, and between L. alatum var. lanceolatum and L. californicum in Oklahoma and eastern Texas.  Prior to the recent recognition of L. junceum in California, older collections were identified as L. californicum.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet or moist soil, margins of ponds, streams, in ditches, on salt flats.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="moist soil" />
        <character name="habitat" value="margins" constraint="of ponds , streams" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="salt flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Kans., Nev., N.Mex., Okla., Utah, Tex.; Mexico (Baja California, Chihuahua, Coahuila, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>