<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="Munz in N. L. Britton et al." date="1965" rank="section">Kleinia</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Fl., ser.</publication_title>
      <place_in_publication>2, 5: 110. 1965</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section kleinia</taxon_hierarchy>
  </taxon_identification>
  <number>17n.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs winter-annual or perennial, caulescent;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a taproot or lateral roots producing adventitious shoots.</text>
      <biological_entity id="o495" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" />
      </biological_entity>
      <biological_entity id="o496" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s1" value="adventitious" />
      </biological_entity>
      <relation from="o495" id="r116" name="producing" negation="false" src="d0_s1" to="o496" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several arising from rosette, decumbent to ascending or erect, unbranched or with short, lateral branches, epidermis white or pink, not exfoliating.</text>
      <biological_entity id="o497" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character constraint="from rosette" constraintid="o498" is_modifier="false" name="orientation" src="d0_s2" value="arising" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="decumbent" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="ascending" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="erect" />
        <character char_type="range_value" from="decumbent" name="orientation" notes="" src="d0_s2" to="ascending or erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="with short , lateral branches" />
      </biological_entity>
      <biological_entity id="o498" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity constraint="lateral" id="o499" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" />
      </biological_entity>
      <biological_entity id="o500" name="epidermis" name_original="epidermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="exfoliating" />
      </biological_entity>
      <relation from="o497" id="r117" name="with" negation="false" src="d0_s2" to="o499" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, cauline 1–10 cm;</text>
      <biological_entity id="o501" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o502" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o501" id="r118" name="in" negation="false" src="d0_s3" to="o502" />
    </statement>
    <statement id="d0_s4">
      <text>blade margins entire, with few coarse teeth, lobed or pinnatifid.</text>
      <biological_entity constraint="blade" id="o503" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" />
      </biological_entity>
      <biological_entity id="o504" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" />
        <character is_modifier="true" name="relief" src="d0_s4" value="coarse" />
      </biological_entity>
      <relation from="o503" id="r119" name="with" negation="false" src="d0_s4" to="o504" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers in axils of distal leaves.</text>
      <biological_entity id="o505" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o506" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" />
      </biological_entity>
      <biological_entity id="o507" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o508" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o506" id="r120" name="in" negation="false" src="d0_s5" to="o507" />
      <relation from="o507" id="r121" name="part_of" negation="false" src="d0_s5" to="o508" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers opening near sunset, with a sweet scent or nearly unscented;</text>
      <biological_entity id="o509" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="nearly" name="odor" src="d0_s6" value="unscented" />
      </biological_entity>
      <biological_entity id="o510" name="sunset" name_original="sunset" src="d0_s6" type="structure" />
      <relation from="o509" id="r122" name="opening near" negation="false" src="d0_s6" to="o510" />
    </statement>
    <statement id="d0_s7">
      <text>buds nodding by recurved floral-tube, weakly quadrangular, without free tips;</text>
      <biological_entity id="o511" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character constraint="by floral-tube" constraintid="o512" is_modifier="false" name="orientation" src="d0_s7" value="nodding" />
        <character is_modifier="false" modifier="weakly" name="shape" notes="" src="d0_s7" value="quadrangular" />
      </biological_entity>
      <biological_entity id="o512" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="recurved" />
      </biological_entity>
      <biological_entity id="o513" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
      </biological_entity>
      <relation from="o511" id="r123" name="without" negation="false" src="d0_s7" to="o513" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube 10–30 mm;</text>
      <biological_entity id="o514" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals separating in pairs or individually;</text>
      <biological_entity id="o515" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character constraint="in pairs" constraintid="o516" is_modifier="false" name="arrangement" src="d0_s9" value="separating" />
      </biological_entity>
      <biological_entity id="o516" name="pair" name_original="pairs" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals white, fading pink, obcordate to obovate;</text>
      <biological_entity id="o517" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading pink" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obcordate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obovate" />
        <character char_type="range_value" from="obcordate" name="shape" src="d0_s10" to="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigma deeply divided into 4 linear lobes.</text>
      <biological_entity id="o518" name="stigma" name_original="stigma" src="d0_s11" type="structure">
        <character constraint="into lobes" constraintid="o519" is_modifier="false" modifier="deeply" name="shape" src="d0_s11" value="divided" />
      </biological_entity>
      <biological_entity id="o519" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="4" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules straight or sometimes curved upward, cylindrical or fusiform, obtusely 4-angled, tapering toward base and apex, dehiscent 1/2 their length;</text>
      <biological_entity id="o521" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o522" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>sessile.</text>
      <biological_entity id="o520" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s12" value="curved" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s12" value="fusiform" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s12" value="4-angled" />
        <character constraint="toward apex" constraintid="o522" is_modifier="false" name="shape" src="d0_s12" value="tapering" />
        <character is_modifier="false" name="dehiscence" notes="" src="d0_s12" value="dehiscent" />
        <character name="length" src="d0_s12" value="1/2" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds numerous, in 2 rows per locule, ellipsoid to subglobose, surface regularly pitted, pits in longitudinal lines.</text>
      <biological_entity id="o523" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="numerous" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="subglobose" />
        <character char_type="range_value" from="ellipsoid" name="shape" notes="" src="d0_s14" to="subglobose" />
      </biological_entity>
      <biological_entity id="o524" name="row" name_original="rows" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="2" />
      </biological_entity>
      <biological_entity id="o525" name="locule" name_original="locule" src="d0_s14" type="structure" />
      <biological_entity id="o526" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="regularly" name="relief" src="d0_s14" value="pitted" />
      </biological_entity>
      <biological_entity id="o528" name="line" name_original="lines" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" />
      </biological_entity>
      <relation from="o523" id="r124" name="in" negation="false" src="d0_s14" to="o524" />
      <relation from="o524" id="r125" name="per" negation="false" src="d0_s14" to="o525" />
      <relation from="o527" id="r126" name="in" negation="false" src="d0_s14" to="o528" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 14, 28.</text>
      <biological_entity id="o527" name="pit" name_original="pits" src="d0_s14" type="structure" />
      <biological_entity constraint="2n" id="o529" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" />
        <character name="quantity" src="d0_s15" value="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Section Kleinia consists of two species of usually open sandy or rocky sites from the Chihuahuan, Sonoran, and southern portions of the Great Basin deserts to the Great Plains, from southern Utah to southeastern Montana and western North Dakota, south to northern Mexico (Chihuahua and Sonora) at 1000 to 3000 m; Oenothera coronopifolia generally occurs at higher elevations than O. albicaulis.  As summarized by W. L. Wagner et al. (2007; see also K. E. Theiss et al. 2010), O. albicaulis is diploid (2n = 14) and O. coronopifolia has both diploid and tetraploid (2n = 14, 28) populations.  Both species are self-incompatible and the vespertine flowers are pollinated by hawkmoths, especially Hyles and Manduca.  P. H. Raven (1979) reported that O. coronopifolia had both self-incompatible and self-compatible populations.  Theiss et al. found only self-incompatible plants in one population examined.</discussion>
  <discussion>Section Kleinia is included within a strongly supported clade with members of sect. Anogra in recent molecular studies (R. A. Levin et al. 2004; M. E. K. Evans et al. 2005, 2009); placement of the subclade of the two sect. Kleinia species within the overall clade is not strongly resolved.  For the present, the classification, based on morphology, is maintained until further resolution can be obtained.  Section Kleinia can be distinguished by a number of morphological characteristics, including capsule shape, seeds in two rows per locule, and seeds with anatomy similar to that found in sect. Oenothera subsect. Raimannia but unlike that in sect. Anogra (W. L. Wagner et al. 2007; H. Tobe et al. 1987).  P. A. Munz (1965) described sect. Kleinia as part of his subg. Raimannia, thus including these two white-flowered species in an otherwise yellow-flowered group because of similarities of the capsules and seeds.  This group has been viewed as intermediate between sect. Oenothera subsect. Raimannia and sect. Anogra (Wagner et al.).  Although both sects. Anogra and Kleinia have morphological synapomorphies that define them, a section combining them would be united by nodding buds and white petals; both characters are not unique morphological synapomorphies.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs annual, from a taproot; floral tube mouth glabrous.</description>
      <determination>59. Oenothera albicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs perennial, lateral roots producing adventitious shoots; floral tube mouth conspicuously pubescent.</description>
      <determination>60. Oenothera coronopifolia</determination>
    </key_statement>
  </key>
</bio:treatment>