<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">CHYLISMIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Chylismia</taxon_name>
    <taxon_name authority="(Munz) W. L. Wagner &amp; Hoch" date="2007" rank="species">megalantha</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 207.  2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus chylismia;section chylismia;species megalantha</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Watson" date="unknown" rank="species">heterochromaS.</taxon_name>
    <taxon_name authority="Munz" date="1941" rank="variety">megalantha</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>3: 52.  1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species heterochromas.;variety megalantha</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(Munz) P. H. Raven" date="unknown" rank="species">megalantha</taxon_name>
    <taxon_hierarchy>genus camissonia;species megalantha</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="(Munz) P. H. Raven" date="unknown" rank="species">megalantha</taxon_name>
    <taxon_hierarchy>genus o.;species megalantha</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, glandular pubes­cent throughout.</text>
      <biological_entity id="o1267" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" modifier="throughout" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems several, 10–200 cm.</text>
      <biological_entity id="o1268" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="several" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="200" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in poorly defined basal rosette and cauline;</text>
      <biological_entity id="o1269" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s2" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1270" name="rosette" name_original="rosette" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="poorly" name="prominence" src="d0_s2" value="defined" />
      </biological_entity>
      <relation from="o1269" id="r263" name="in" negation="false" src="d0_s2" to="o1270" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1.8–5.5 cm;</text>
      <biological_entity id="o1271" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s3" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade unlobed, broadly cordate to ovate, 2.4–8 × 7 cm, margins sinuate-dentate, yellowish oil cells prominently lining veins abaxially.</text>
      <biological_entity id="o1272" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s4" value="cordate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="ovate" />
        <character char_type="range_value" from="broadly cordate" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="2.4" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character name="width" src="d0_s4" unit="cm" value="7" />
      </biological_entity>
      <biological_entity id="o1273" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sinuate-dentate" />
      </biological_entity>
      <biological_entity constraint="oil" id="o1274" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="yellowish" />
      </biological_entity>
      <biological_entity id="o1275" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <relation from="o1274" id="r264" modifier="prominently; abaxially" name="lining" negation="false" src="d0_s4" to="o1275" />
    </statement>
    <statement id="d0_s5">
      <text>Racemes erect, elongating in flower.</text>
      <biological_entity id="o1276" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" />
        <character constraint="in flower" constraintid="o1277" is_modifier="false" name="length" src="d0_s5" value="elongating" />
      </biological_entity>
      <biological_entity id="o1277" name="flower" name_original="flower" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers opening at sunrise;</text>
      <biological_entity id="o1278" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1279" name="sunrise" name_original="sunrise" src="d0_s6" type="structure" />
      <relation from="o1278" id="r265" name="opening at" negation="false" src="d0_s6" to="o1279" />
    </statement>
    <statement id="d0_s7">
      <text>buds without free tips;</text>
      <biological_entity id="o1280" name="bud" name_original="buds" src="d0_s7" type="structure" />
      <biological_entity id="o1281" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
      </biological_entity>
      <relation from="o1280" id="r266" name="without" negation="false" src="d0_s7" to="o1281" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube 4–9 mm, with matted, villous hairs inside;</text>
      <biological_entity id="o1282" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1283" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s8" value="matted" />
        <character is_modifier="true" name="pubescence" src="d0_s8" value="villous" />
      </biological_entity>
      <relation from="o1282" id="r267" name="with" negation="false" src="d0_s8" to="o1283" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 4.5–9 mm;</text>
      <biological_entity id="o1284" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals pale to dark lavender, diffusely purplish-flecked near base, white at very base, fading darker lavender, 9–14 mm;</text>
      <biological_entity id="o1285" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="pale" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="dark lavender" />
        <character char_type="range_value" from="pale" name="coloration" src="d0_s10" to="dark lavender" />
        <character constraint="near base" constraintid="o1286" is_modifier="false" modifier="diffusely" name="coloration" src="d0_s10" value="purplish-flecked" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="fading darker" />
        <character is_modifier="false" name="coloration_or_odor" src="d0_s10" value="lavender" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1286" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o1287" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens unequal, filaments of antisepalous stamens 6–12 mm, of antipetalous ones 3.5–8 mm, anthers 2 mm, glabrous;</text>
      <biological_entity id="o1288" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" />
      </biological_entity>
      <biological_entity id="o1289" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="antisepalous" id="o1290" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity constraint="antipetalous" id="o1291" name="one" name_original="ones" src="d0_s11" type="structure" />
      <biological_entity id="o1292" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" />
      </biological_entity>
      <relation from="o1289" id="r268" name="part_of" negation="false" src="d0_s11" to="o1290" />
      <relation from="o1289" id="r269" name="part_of" negation="false" src="d0_s11" to="o1291" />
    </statement>
    <statement id="d0_s12">
      <text>style 14–22.5 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o1293" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s12" to="22.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1294" name="stigma" name_original="stigma" src="d0_s12" type="structure" />
      <biological_entity id="o1295" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o1294" id="r270" name="exserted beyond" negation="false" src="d0_s12" to="o1295" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules erect or ascending, clavate, 8–14 mm;</text>
      <biological_entity id="o1296" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="ascending" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel 2–3.5 mm.</text>
      <biological_entity id="o1297" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1–1.3 mm. 2n = 14.</text>
      <biological_entity id="o1298" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1299" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chylismia megalantha is known from around Frenchman Drainage to French Peak and Skull Mountain in southern Nye County.  P. H. Raven (1962, 1969) determined this species to be self-compatible, but outcrossing.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rubble derived from volcanic tuff, partly on moist soil along springs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rubble" />
        <character name="habitat" value="volcanic tuff" modifier="derived from" />
        <character name="habitat" value="moist soil" modifier="partly on" constraint="along springs" />
        <character name="habitat" value="springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>