<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) W. L. Wagner &amp; Hoch" date="2007" rank="section">Calylophus</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) W. L. Wagner &amp; Hoch" date="2007" rank="subsection">Salpingia</taxon_name>
    <taxon_name authority="Bentham" date="1839" rank="species">hartwegii</taxon_name>
    <taxon_name authority="(Shinners) W. L. Wagner &amp; Hoch" date="2007" rank="subspecies">maccartii</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 212. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section calylophus;subsection salpingia;species hartwegii;subspecies maccartii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calylophus</taxon_name>
    <taxon_name authority="(Bentham) P. H. Raven" date="unknown" rank="species">hartwegii</taxon_name>
    <taxon_name authority="Shinners" date="1964" rank="variety">maccartii</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>1: 343.  1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus calylophus;species hartwegii;variety maccartii</taxon_hierarchy>
  </taxon_identification>
  <number>8b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs usually sparsely strig­illose, sometimes glandular puberulent.</text>
      <biological_entity id="o3788" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 0.6–3.5 × 0.1–0.6 cm, fascicles of small leaves to 1.5 cm usually present in axils;</text>
      <biological_entity id="o3789" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s1" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s1" to="0.6" to_unit="cm" />
        <character constraint="of leaves" constraintid="o3790" is_modifier="false" name="arrangement" src="d0_s1" value="fascicles" />
      </biological_entity>
      <biological_entity id="o3790" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" />
        <character constraint="in axils" constraintid="o3791" is_modifier="false" modifier="usually" name="presence" src="d0_s1" value="absent" />
      </biological_entity>
      <biological_entity id="o3791" name="axil" name_original="axils" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade usually narrowly lanceolate to lanceolate or oblanceolate, rarely linear, base attenuate, margins subentire or serrulate, usually crinkled-undulate or undulate.</text>
      <biological_entity id="o3792" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually narrowly" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="oblanceolate" />
        <character char_type="range_value" from="usually narrowly lanceolate" name="shape" src="d0_s2" to="lanceolate or oblanceolate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" />
      </biological_entity>
      <biological_entity id="o3793" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="attenuate" />
      </biological_entity>
      <biological_entity id="o3794" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="subentire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="serrulate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="crinkled-undulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subentire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="serrulate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="crinkled-undulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: buds with free tips 1–6 mm;</text>
      <biological_entity id="o3795" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o3796" name="bud" name_original="buds" src="d0_s3" type="structure" />
      <biological_entity id="o3797" name="tip" name_original="tips" src="d0_s3" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s3" value="free" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o3796" id="r751" name="with" negation="false" src="d0_s3" to="o3797" />
    </statement>
    <statement id="d0_s4">
      <text>floral-tube 17–45 mm;</text>
      <biological_entity id="o3798" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o3799" name="floral-tube" name_original="floral-tube" src="d0_s4" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s4" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 11–27 mm;</text>
      <biological_entity id="o3800" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o3801" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s5" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 10–30 mm;</text>
      <biological_entity id="o3802" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3803" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 6–12 mm, anthers 5–9 mm;</text>
      <biological_entity id="o3804" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3805" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3806" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style 25–60 mm. 2n = 14, 28.</text>
      <biological_entity id="o3807" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3808" name="style" name_original="style" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s8" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3809" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="14" />
        <character name="quantity" src="d0_s8" value="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies maccartii occurs on the south Texas Plains and along the Rio Grande from Kinney, Milam, Uvalde, and Val Verde counties south to southeastern Coahuila, central Nuevo León, and northwestern Tamaulipas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, sandy to gravelly soil, limestone, with Acacia, Larrea, Opuntia, Prosopis, and Yucca.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="sandy to gravelly soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>