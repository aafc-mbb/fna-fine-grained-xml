<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1767" rank="genus">MELALEUCA</taxon_name>
    <taxon_name authority="(Cavanilles) S. T. Blake" date="1958" rank="species">quinquenervia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Roy. Soc. Queensland</publication_title>
      <place_in_publication>69: 76.  1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus melaleuca;species quinquenervia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Metrosideros</taxon_name>
    <taxon_name authority="Cavanilles" date="1797" rank="species">quinquenervia</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>4: 19, plate 333.  1797</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus metrosideros;species quinquenervia</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Punk tree</other_name>
  <other_name type="common_name">broad-leaved paperbark</other_name>
  <other_name type="common_name">niauoli</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, 1–18 m;</text>
      <biological_entity id="o3274" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="18" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bark papery.</text>
      <biological_entity id="o3275" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o3276" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually narrowly elliptic to elliptic, rarely somewhat falcate, 5.5–12 × 1–3.1 cm, veins 5–7, longitudinal, surfaces glabres­cent.</text>
      <biological_entity id="o3277" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic to elliptic" />
        <character is_modifier="false" modifier="rarely somewhat" name="shape" src="d0_s3" value="falcate" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="3.1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3278" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s3" value="longitudinal" />
      </biological_entity>
      <biological_entity id="o3279" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 15–54-flowered, flowers in triads, pseudoterminal, sometimes also axillary distally, to 40 mm wide.</text>
      <biological_entity id="o3280" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="15-54-flowered" />
      </biological_entity>
      <biological_entity id="o3281" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s4" value="pseudoterminal" />
        <character is_modifier="false" modifier="sometimes; distally" name="position" src="d0_s4" value="axillary" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3282" name="triad" name_original="triads" src="d0_s4" type="structure" />
      <relation from="o3281" id="r583" name="in" negation="false" src="d0_s4" to="o3282" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx lobes glabrous abaxially, margins scarious, 0.3–0.4 mm wide;</text>
      <biological_entity id="o3283" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="calyx" id="o3284" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" />
      </biological_entity>
      <biological_entity id="o3285" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals deciduous, 2.5–3.5 mm;</text>
      <biological_entity id="o3286" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3287" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments connate in bundles of 5–10, white, cream, greenish white, green, creamy white, or creamy yellow, 10.5–20 mm, bundle claw 0.9–2.5 mm;</text>
      <biological_entity id="o3288" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3289" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character constraint="in bundles" constraintid="o3290" is_modifier="false" name="fusion" src="d0_s7" value="connate" />
        <character is_modifier="false" modifier="of 5-10 , white , cream" name="coloration" notes="" src="d0_s7" value="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="creamy white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="creamy yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="creamy white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="creamy yellow" />
        <character char_type="range_value" from="10.5" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3290" name="bundle" name_original="bundles" src="d0_s7" type="structure" />
      <biological_entity constraint="bundle" id="o3291" name="claw" name_original="claw" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style 11–18 mm;</text>
      <biological_entity id="o3292" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3293" name="style" name_original="style" src="d0_s8" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovules ca. 50–65 per locule.</text>
      <biological_entity id="o3294" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3295" name="ovule" name_original="ovules" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o3296" from="50" name="quantity" src="d0_s9" to="65" />
      </biological_entity>
      <biological_entity id="o3296" name="locule" name_original="locule" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules 2.7–4 mm.</text>
      <biological_entity id="o3297" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cotyledons obvolute.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 22.</text>
      <biological_entity id="o3298" name="cotyledon" name_original="cotyledons" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s11" value="obvolute" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3299" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round (commonly in fall).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="commonly" to="" from="" constraint=" year round" />
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., La.; Asia (Malesia); Pacific Islands (New Caledonia); Australia; introduced also elsewhere in Pacific Islands (Hawaii), widely elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" value="Asia (Malesia)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Caledonia)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="also elsewhere in Pacific Islands (Hawaii)" establishment_means="introduced" />
        <character name="distribution" value="widely elsewhere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Melaleuca quinquenervia is a serious woody weed of wetland habitats in Florida and Louisiana.  Mechanical control has not been successful and research in recent years has been focused upon biological control.</discussion>
  
</bio:treatment>