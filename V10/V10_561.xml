<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">TARAXIA</taxon_name>
    <taxon_name authority="(Nuttall) Small" date="1896" rank="species">ovata</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 185.  1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus taraxia;species ovata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1840" rank="species">ovata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 507.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species ovata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(Nuttall) P. H. Raven" date="unknown" rank="species">ovata</taxon_name>
    <taxon_hierarchy>genus camissonia;species ovata</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs densely, sometimes sparsely, short-hirsute, espe­cially on leaf-blade margins and ± veins;</text>
      <biological_entity id="o4165" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely; sparsely" name="pubescence" src="d0_s0" value="short-hirsute" />
      </biological_entity>
      <biological_entity constraint="leaf-blade" id="o4166" name="margin" name_original="margins" src="d0_s0" type="structure" />
      <biological_entity id="o4167" name="vein" name_original="veins" src="d0_s0" type="structure" />
      <relation from="o4165" id="r522" modifier="cially" name="on" negation="false" src="d0_s0" to="o4166" />
    </statement>
    <statement id="d0_s1">
      <text>taproot thick, often branched in age, producing new rosettes.</text>
      <biological_entity id="o4168" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thick" />
        <character constraint="in age" constraintid="o4169" is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="branched" />
      </biological_entity>
      <biological_entity id="o4169" name="age" name_original="age" src="d0_s1" type="structure" />
      <biological_entity id="o4170" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" />
      </biological_entity>
      <relation from="o4168" id="r523" name="producing" negation="false" src="d0_s1" to="o4170" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–15 × 1.6–5 cm;</text>
      <biological_entity id="o4171" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole narrowly winged, 0.8–15 cm, base slightly dilated;</text>
      <biological_entity id="o4172" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s3" value="winged" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4173" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to very narrowly elliptic, base attenuate, margins usually subentire to shallowly sinuate or crisped, rarely deeply sinuate, apex acute to acuminate.</text>
      <biological_entity id="o4174" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate to very" />
        <character is_modifier="false" modifier="very; narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" />
      </biological_entity>
      <biological_entity id="o4175" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" />
      </biological_entity>
      <biological_entity id="o4176" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" notes="[duplicate value]" src="d0_s4" value="subentire" />
        <character is_modifier="false" modifier="shallowly" name="shape" notes="[duplicate value]" src="d0_s4" value="sinuate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="crisped" />
        <character char_type="range_value" from="usually subentire" name="shape" src="d0_s4" to="shallowly sinuate or crisped" />
        <character is_modifier="false" modifier="rarely deeply" name="shape" src="d0_s4" value="sinuate" />
      </biological_entity>
      <biological_entity id="o4177" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="acuminate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers opening near sunrise;</text>
      <biological_entity id="o4178" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o4179" name="sunrise" name_original="sunrise" src="d0_s5" type="structure" />
      <relation from="o4178" id="r524" name="opening near" negation="false" src="d0_s5" to="o4179" />
    </statement>
    <statement id="d0_s6">
      <text>floral-tube 2–3 mm, with short, matted hairs inside near base;</text>
      <biological_entity id="o4180" name="floral-tube" name_original="floral-tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4181" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" />
        <character is_modifier="true" name="growth_form" src="d0_s6" value="matted" />
      </biological_entity>
      <biological_entity id="o4182" name="base" name_original="base" src="d0_s6" type="structure" />
      <relation from="o4180" id="r525" name="with" negation="false" src="d0_s6" to="o4181" />
      <relation from="o4181" id="r526" name="inside" negation="false" src="d0_s6" to="o4182" />
    </statement>
    <statement id="d0_s7">
      <text>sepals 11–19 mm;</text>
      <biological_entity id="o4183" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s7" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals usually yellow, rarely white, 8–23 mm;</text>
      <biological_entity id="o4184" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="white" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>episepalous staminal filaments 3.5–8 mm, epipetalous ones 2–6 mm, anthers 3–5 mm;</text>
      <biological_entity constraint="episepalous staminal" id="o4185" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="epipetalous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sterile prolongation of ovary 25–180 mm, style 4.5–11 mm, shortly pubescent near base, stigma exserted slightly beyond anthers at anthesis.</text>
      <biological_entity id="o4186" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character constraint="of ovary" constraintid="o4187" is_modifier="false" name="reproduction" src="d0_s10" value="sterile" />
      </biological_entity>
      <biological_entity id="o4187" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s10" to="180" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4188" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character constraint="near base" constraintid="o4189" is_modifier="false" modifier="shortly" name="pubescence" src="d0_s10" value="pubescent" />
      </biological_entity>
      <biological_entity id="o4189" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o4190" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character constraint="beyond anthers" constraintid="o4191" is_modifier="false" name="position" src="d0_s10" value="exserted" />
      </biological_entity>
      <biological_entity id="o4191" name="anther" name_original="anthers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules subterete, cylindric-lanceoloid, 11–30 × 3–5 mm, walls thin, much distended by seeds;</text>
      <biological_entity id="o4192" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cylindric-lanceoloid" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4194" name="seed" name_original="seeds" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>rarely with pedicel to 0.4 mm.</text>
      <biological_entity id="o4193" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" />
        <character constraint="by seeds" constraintid="o4194" is_modifier="false" modifier="much" name="shape" src="d0_s11" value="distended" />
      </biological_entity>
      <biological_entity id="o4195" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="0.4" to_unit="mm" />
      </biological_entity>
      <relation from="o4193" id="r527" name="with" negation="false" src="d0_s12" to="o4195" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds uniformly brown, elongate-ovoid, 1.8–2.2 × 1.2–1.4 mm, densely and coarsely papillose.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o4196" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s13" value="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elongate-ovoid" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s13" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s13" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="densely; coarsely" name="relief" src="d0_s13" value="papillose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4197" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Taraxia ovata occurs in counties near the coast and is found in Humboldt, Lake, and Mendocino counties south to the vicinity of Monterey Bay, Monterey County, and again south of the Santa Lucia Mountains in north­ern San Luis Obispo County, California, and Douglas and Josephine counties in Oregon.  It has no close relatives in the genus (P. H. Raven 1969), an assertion supported by its early branching in molecular analyses (R. A. Levin et al. 2004; W. L. Wagner et al. 2007).  The species is self-incompatible and pollinated by the oligolectic bee Andrena (Diandrena) chalybea (Cresson) (Raven).</discussion>
  <discussion>Oenothera primuloidea H. Léveillé is an illegitimate, superfluous name that pertains here.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy fields, clay soil, usually near coast.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy fields" />
        <character name="habitat" value="clay soil" />
        <character name="habitat" value="coast" modifier="usually" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>