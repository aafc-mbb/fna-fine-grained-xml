<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 41.  2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae</taxon_hierarchy>
  </taxon_identification>
  <number>b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: stipules present or absent.</text>
      <biological_entity id="o2643" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o2644" name="stipule" name_original="stipules" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: floral-tube present or, rarely, absent;</text>
      <biological_entity id="o2645" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o2646" name="floral-tube" name_original="floral-tube" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" />
        <character name="quantity" src="d0_s1" value="," />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>sepals 2 or 4 (very rarely 3), deciduous with floral-tube, petals, and stamens;</text>
      <biological_entity id="o2647" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o2648" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" unit="or" value="2" />
        <character name="quantity" src="d0_s2" unit="or" value="4" />
        <character constraint="with stamens" constraintid="o2651" is_modifier="false" name="duration" src="d0_s2" value="deciduous" />
      </biological_entity>
      <biological_entity id="o2649" name="floral-tube" name_original="floral-tube" src="d0_s2" type="structure" />
      <biological_entity id="o2650" name="petal" name_original="petals" src="d0_s2" type="structure" />
      <biological_entity id="o2651" name="stamen" name_original="stamens" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petals yellow, white, pink, red, rarely in combination.</text>
      <biological_entity id="o2652" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o2654" name="combination" name_original="combination" src="d0_s3" type="structure" />
      <relation from="o2653" id="r397" modifier="rarely" name="in" negation="false" src="d0_s3" to="o2654" />
    </statement>
    <statement id="d0_s4">
      <text>= 7, 10, 11, 15, 18.</text>
      <biological_entity id="o2653" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pink" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" />
        <character name="quantity" src="d0_s4" value="7" />
        <character name="quantity" src="d0_s4" value="10" />
        <character name="quantity" src="d0_s4" value="11" />
        <character name="quantity" src="d0_s4" value="15" />
        <character name="quantity" src="d0_s4" value="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 21, species 582 (16 genera, 246 species in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Eurasia, Pacific Islands (New Zealand, Society Islands), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Society Islands)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Onagroideae encompass the main lineage of the family, after the early branching of Ludwigia (R. A. Levin et al. 2003, 2004).  This large and diverse lineage is distinguished by the presence of a floral tube beyond the apex of the ovary; sepals deciduous with the floral tube, petals, and stamens; pollen shed in monads (or tetrads in Chylismia sect. Lignothera and all but one species of Epilobium); ovular vascular system exclusively transseptal (R. H. Eyde 1981); ovule archesporium multicellular (H. Tobe and P. H. Raven 1996); and change in base chromosome number from x = 8 in Ludwigia to x = 10 or x = 11 at the base of Onagroideae (Raven 1979; Levin et al. 2003).  Molecular work (Levin et al. 2003, 2004) substantially supports the traditional tribal classification (P. A. Munz 1965; Raven 1979, 1988); tribes are recognized to delimit major branches within the phylogeny of Onagroideae, where the branches comprise strongly supported monophyletic groups of one or more genera.</discussion>
  
</bio:treatment>