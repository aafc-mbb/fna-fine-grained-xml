<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Oenothera</taxon_name>
    <taxon_name authority="(Rose ex Britton &amp; A. Brown) W. Dietrich" date="1978" rank="subsection">Raimannia</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="species">drummondii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">drummondii</taxon_name>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section oenothera;subsection raimannia;species drummondii;subspecies drummondii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_name authority="H. Léveillé" date="unknown" rank="variety">helleriana</taxon_name>
    <taxon_hierarchy>genus oenothera;species drummondii;variety helleriana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="Schlechtendal" date="unknown" rank="species">littoralis</taxon_name>
    <taxon_hierarchy>genus o.;species littoralis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Raimannia</taxon_name>
    <taxon_name authority="(Schlechtendal) Rose ex Sprague &amp; L. Riley" date="unknown" rank="species">littoralis</taxon_name>
    <taxon_hierarchy>genus raimannia;species littoralis</taxon_hierarchy>
  </taxon_identification>
  <number>90a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, densely strigil­lose, sometimes also villous, also glandular puberulent distally.</text>
      <biological_entity id="o2286" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" modifier="densely; sometimes" name="pubescence" src="d0_s0" value="villous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to decum­bent, with nonflowering lateral branches, these often with terminal rosette of crowded, small leaves, 10–50 cm.</text>
      <biological_entity id="o2287" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="to decum" constraintid="o2288" is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2288" name="decum" name_original="decum" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="bent" />
      </biological_entity>
      <biological_entity constraint="nonflowering lateral" id="o2289" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity constraint="terminal" id="o2290" name="rosette" name_original="rosette" src="d0_s1" type="structure" />
      <biological_entity id="o2291" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="crowded" />
        <character is_modifier="true" name="size" src="d0_s1" value="small" />
      </biological_entity>
      <relation from="o2287" id="r519" name="with" negation="false" src="d0_s1" to="o2289" />
      <relation from="o2287" id="r520" modifier="often" name="with" negation="false" src="d0_s1" to="o2290" />
      <relation from="o2290" id="r521" name="part_of" negation="false" src="d0_s1" to="o2291" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in a basal rosette and cauline, basal 5–14 × 1–2 cm, cauline 1–8 × 0.5–2.5 cm;</text>
      <biological_entity id="o2292" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s2" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2293" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o2294" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="14" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o2295" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="2.5" to_unit="cm" />
      </biological_entity>
      <relation from="o2292" id="r522" name="in" negation="false" src="d0_s2" to="o2293" />
    </statement>
    <statement id="d0_s3">
      <text>blade grayish green, narrowly oblanceolate to elliptic, becoming elliptic to narrowly obovate to obovate distally, margins subentire or shallowly dentate;</text>
      <biological_entity id="o2296" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="grayish green" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate to elliptic" />
        <character is_modifier="false" modifier="becoming" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="obovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="obovate" />
        <character char_type="range_value" from="elliptic" modifier="becoming; distally" name="shape" src="d0_s3" to="narrowly obovate" />
      </biological_entity>
      <biological_entity id="o2297" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subentire" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts spreading, flat.</text>
      <biological_entity id="o2298" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 1–few opening per day near sunset;</text>
      <biological_entity id="o2299" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s5" to="few" />
      </biological_entity>
      <biological_entity id="o2300" name="day" name_original="day" src="d0_s5" type="structure" />
      <biological_entity id="o2301" name="sunset" name_original="sunset" src="d0_s5" type="structure" />
      <relation from="o2299" id="r523" name="opening per" negation="false" src="d0_s5" to="o2300" />
      <relation from="o2299" id="r524" name="near" negation="false" src="d0_s5" to="o2301" />
    </statement>
    <statement id="d0_s6">
      <text>buds erect, with free tips erect, 1–3 mm;</text>
      <biological_entity id="o2302" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2303" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="free" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
      </biological_entity>
      <relation from="o2302" id="r525" name="with" negation="false" src="d0_s6" to="o2303" />
    </statement>
    <statement id="d0_s7">
      <text>floral-tube 25–50 mm;</text>
      <biological_entity id="o2304" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 20–30 mm;</text>
      <biological_entity id="o2305" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals yellow, very broadly obovate or obcordate, 25–45 mm;</text>
      <biological_entity id="o2306" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" />
        <character is_modifier="false" modifier="very broadly" name="shape" src="d0_s9" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obcordate" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s9" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 10–23 mm, anthers 4–12 mm, pollen 85–100% fertile;</text>
      <biological_entity id="o2307" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2308" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2309" name="pollen" name_original="pollen" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="85-100%" name="reproduction" src="d0_s10" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 35–75 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o2310" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s11" to="75" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2311" name="stigma" name_original="stigma" src="d0_s11" type="structure" />
      <biological_entity id="o2312" name="anther" name_original="anthers" src="d0_s11" type="structure" />
      <relation from="o2311" id="r526" name="exserted beyond" negation="false" src="d0_s11" to="o2312" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules cylindrical, sometimes slightly enlarged toward apex, 25–55 × 2–3 mm.</text>
      <biological_entity id="o2313" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindrical" />
        <character constraint="toward apex" constraintid="o2314" is_modifier="false" modifier="sometimes slightly" name="size" src="d0_s12" value="enlarged" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" notes="" src="d0_s12" to="55" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2314" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds usually ellipsoid to broadly ellipsoid, rarely subglobose, 1–2 ×0.5–0.9 mm. 2n = 14.</text>
      <biological_entity id="o2315" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" notes="[duplicate value]" src="d0_s13" value="ellipsoid" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s13" value="ellipsoid" />
        <character char_type="range_value" from="usually ellipsoid" name="shape" src="d0_s13" to="broadly ellipsoid" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s13" value="subglobose" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s13" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2316" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Collections of subsp. drummondii at inland localities in Bexar and Dallas counties, Texas, and Henderson County, North Carolina, presumably represent introductions; it is also widely naturalized and is known from Africa, Asia, Australia, southwestern Europe, South America, and Taiwan (W. Dietrich and W. L. Wagner 1988; Wagner et al. 2007).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along or near Atlantic coast on dunes and open sandy places.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="atlantic coast" modifier="along or near" constraint="on dunes and open sandy places" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="open sandy places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Along or near Atlantic coast on dunes and open sandy places; Fla., La., N.C., S.C., Tex.; Mexico (Tamaulipas, Veracruz); introduced in South America, sw Europe, Asia (including Taiwan), Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Along or near Atlantic coast on dunes and open sandy places" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Veracruz)" establishment_means="native" />
        <character name="distribution" value="in South America" establishment_means="introduced" />
        <character name="distribution" value="sw Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia (including Taiwan)" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>