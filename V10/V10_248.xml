<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Ludwigioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LUDWIGIA</taxon_name>
    <taxon_name authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007" rank="section">Isnardia</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">glandulosa</taxon_name>
    <taxon_name authority="I. Peng" date="2020" rank="subspecies">brachycarpaC.</taxon_name>
    <place_of_publication>
      <publication_title>PhytoKeys</publication_title>
      <place_in_publication>145: 58. 2020</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily ludwigioideae;genus ludwigia;section isnardia;species glandulosa;subspecies brachycarpac.</taxon_hierarchy>
  </taxon_identification>
  <number>18b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems rarely reddish green, 10–55 (–90) cm.</text>
      <biological_entity id="o1794" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish green" />
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="55" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 0–1 cm, blades linear-elliptic to linear, sometimes very narrowly elliptic, those on main axis 3–5 (–7) × 0.3–0.5 (–1) cm, those on branches 0.8–3.6 × 0.2–0.3 (–0.8) cm.</text>
      <biological_entity id="o1795" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o1796" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1797" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" notes="[duplicate value]" src="d0_s1" value="linear-elliptic" />
        <character is_modifier="false" name="arrangement" notes="[duplicate value]" src="d0_s1" value="linear" />
        <character char_type="range_value" from="linear-elliptic" name="arrangement" src="d0_s1" to="linear" />
        <character is_modifier="false" modifier="sometimes very narrowly" name="arrangement_or_shape" src="d0_s1" value="elliptic" />
      </biological_entity>
      <biological_entity constraint="main" id="o1798" name="axis" name_original="axis" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s1" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1799" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s1" to="3.6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s1" to="0.3" to_unit="cm" />
      </biological_entity>
      <relation from="o1797" id="r292" name="on" negation="false" src="d0_s1" to="o1798" />
      <relation from="o1797" id="r293" name="on" negation="false" src="d0_s1" to="o1799" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: bracteoles attached at base of ovary, 0.4–0.8 × 0.1–0.2 mm.</text>
      <biological_entity id="o1800" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o1801" name="bracteole" name_original="bracteoles" src="d0_s2" type="structure">
        <character constraint="at base" constraintid="o1802" is_modifier="false" name="fixation" src="d0_s2" value="attached" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" notes="" src="d0_s2" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" notes="" src="d0_s2" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1802" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o1803" name="ovary" name_original="ovary" src="d0_s2" type="structure" />
      <relation from="o1802" id="r294" name="part_of" negation="false" src="d0_s2" to="o1803" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals 1.1–1.9 × 1–1.8 mm, apex acute or short-acuminate;</text>
      <biological_entity id="o1804" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o1805" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s3" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1806" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="short-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nectary disc obscurely, minutely papillose;</text>
      <biological_entity id="o1807" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="nectary" id="o1808" name="disc" name_original="disc" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s4" value="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>style 0.4–0.8 mm, stigma 0.2–0.3 mm diam.</text>
      <biological_entity id="o1809" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1810" name="style" name_original="style" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1811" name="stigma" name_original="stigma" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s5" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules obscurely 4-angled, 2–5 × 1.3–2 mm, pedicel 0–0.2 mm.</text>
      <biological_entity id="o1812" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s6" value="4-angled" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1813" name="pedicel" name_original="pedicel" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds 0.6–0.8 × 0.3–0.4 mm, surface cells elongate transversely to seed length.</text>
      <biological_entity id="o1814" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s7" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s7" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="surface" id="o1815" name="cel" name_original="cells" src="d0_s7" type="structure">
        <character constraint="to seed" constraintid="o1816" is_modifier="false" name="shape" src="d0_s7" value="elongate" />
      </biological_entity>
      <biological_entity id="o1816" name="seed" name_original="seed" src="d0_s7" type="structure" />
    </statement>
  </description>
  <discussion>Subspecies brachycarpa grows along the Gulf Coast from southwestern Louisiana to Nueces County, Texas, and more sporadically northward in eastern Texas to south-central Oklahoma.</discussion>
  <discussion>Subspecies brachycarpa was published initially by Peng as a new combination based on Ludwigia cylindrica var. brachycarpa Torrey &amp; A. Gray, not realizing that the variety was based on Jussiaea brachycarpa Lamarck, which Peng considered to be a synonym of L. glandulosa subsp. glandulosa.  Therefore, subsp. brachycarpa is a new subspecies but was invalid when published by Peng since it was not accompanied by a Latin description in 1986; that situation has since been remedied.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ditches, low meadows, coastal prairies, seeps in sandy woods, moist sinkholes in granite outcrops, old clay fields.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ditches" />
        <character name="habitat" value="low meadows" />
        <character name="habitat" value="coastal prairies" />
        <character name="habitat" value="sandy woods" modifier="seeps in" />
        <character name="habitat" value="moist sinkholes" constraint="in granite outcrops , old clay fields" />
        <character name="habitat" value="granite outcrops" />
        <character name="habitat" value="old clay fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>