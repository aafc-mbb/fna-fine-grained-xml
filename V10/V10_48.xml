<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) Walpers" date="1843" rank="section">Hartmannia</taxon_name>
    <taxon_name authority="P. H. Raven &amp; D. R. Parnell" date="1970" rank="species">platanorum</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>20: 246.  1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section hartmannia;species platanorum</taxon_hierarchy>
  </taxon_identification>
  <number>17.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, caulescent, strigillose, often densely so;</text>
    </statement>
    <statement id="d0_s1">
      <text>from slender taproot.</text>
      <biological_entity id="o4303" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several, ascending, 5–60 cm.</text>
      <biological_entity id="o4304" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, basal 2–7 × 0.3–1.4 cm, blade narrowly elliptic to narrowly ovate, margins weakly serrulate to sinuate-pinnatifid;</text>
      <biological_entity id="o4305" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4306" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o4307" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4308" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="ovate" />
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s3" to="narrowly ovate" />
      </biological_entity>
      <relation from="o4305" id="r845" name="in" negation="false" src="d0_s3" to="o4306" />
    </statement>
    <statement id="d0_s4">
      <text>cauline 1.2–6 × 0.3–1 cm, blade narrowly elliptic to elliptic or ovate, proximal ones sinuate-pinnatifid, margins subentire or weakly serrulate.</text>
      <biological_entity id="o4309" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" notes="[duplicate value]" src="d0_s3" value="serrulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="sinuate-pinnatifid" />
        <character char_type="range_value" from="weakly serrulate" name="shape" src="d0_s3" to="sinuate-pinnatifid" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" />
      </biological_entity>
      <biological_entity id="o4310" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character constraint="to proximal ones" constraintid="o4311" is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4311" name="one" name_original="ones" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="elliptic" />
        <character is_modifier="true" name="shape" src="d0_s4" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sinuate-pinnatifid" />
      </biological_entity>
      <biological_entity id="o4312" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s4" value="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences erect.</text>
      <biological_entity id="o4313" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1–3 opening per day near sunrise;</text>
      <biological_entity id="o4314" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o4315" name="day" name_original="day" src="d0_s6" type="structure" />
      <biological_entity id="o4316" name="sunrise" name_original="sunrise" src="d0_s6" type="structure" />
      <relation from="o4314" id="r846" name="opening per" negation="false" src="d0_s6" to="o4315" />
      <relation from="o4314" id="r847" name="near" negation="false" src="d0_s6" to="o4316" />
    </statement>
    <statement id="d0_s7">
      <text>buds with free tips 0–0.1 mm;</text>
      <biological_entity id="o4317" name="bud" name_original="buds" src="d0_s7" type="structure" />
      <biological_entity id="o4318" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.1" to_unit="mm" />
      </biological_entity>
      <relation from="o4317" id="r848" name="with" negation="false" src="d0_s7" to="o4318" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube 9–14 mm;</text>
      <biological_entity id="o4319" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 7.5–13 mm;</text>
      <biological_entity id="o4320" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals rose-purple, fading darker, 8–15 mm;</text>
      <biological_entity id="o4321" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="rose-purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading darker" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 4–9 mm, anthers 2.5–4 mm, pollen 85–100% fertile;</text>
      <biological_entity id="o4322" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4323" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4324" name="pollen" name_original="pollen" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="85-100%" name="reproduction" src="d0_s11" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 12–19 mm, stigma surrounded by anthers at anthesis.</text>
      <biological_entity id="o4325" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4326" name="stigma" name_original="stigma" src="d0_s12" type="structure" />
      <biological_entity id="o4327" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o4326" id="r849" name="surrounded by" negation="false" src="d0_s12" to="o4327" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules clavate or narrowly obovoid, 9–14 × 3–4 mm, apex attenuate to a sterile beak, valve midrib prominent in distal part, proximal stipe 4–15 mm, gradually tapering to base;</text>
      <biological_entity id="o4328" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="obovoid" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s13" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4329" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="attenuate" />
      </biological_entity>
      <biological_entity id="o4330" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="sterile" />
      </biological_entity>
      <biological_entity constraint="valve" id="o4331" name="midrib" name_original="midrib" src="d0_s13" type="structure">
        <character constraint="in distal part" constraintid="o4332" is_modifier="false" name="prominence" src="d0_s13" value="prominent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4332" name="part" name_original="part" src="d0_s13" type="structure" />
      <biological_entity id="o4334" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>sessile.</text>
      <biological_entity constraint="proximal" id="o4333" name="stipe" name_original="stipe" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="15" to_unit="mm" />
        <character constraint="to base" constraintid="o4334" is_modifier="false" modifier="gradually" name="shape" src="d0_s13" value="tapering" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds narrowly obovoid, 0.7–0.9 × 0.3–0.5 mm. 2n = 14.</text>
      <biological_entity id="o4335" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="obovoid" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s15" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4336" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera platanorum is known only from the southeastern counties of Cochise, Pinal, and Santa Cruz in Arizona.  It was recently collected in Sonora, Mexico.  The species is very similar to both O. texensis, from which it differs in its smaller flowers, and the widespread O. rosea, from which it differs in the somewhat larger flowers and in forming seven bivalents in meiosis and  fully fertile pollen, whereas O. rosea is a PTH species.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Streambeds and near springs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streambeds" />
        <character name="habitat" value="near springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>