<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POLYGALA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">cruciata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 706.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species cruciata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygala</taxon_name>
    <taxon_name authority="(Fernald &amp; B. G. Schubert) Sorrie &amp; Weakley" date="unknown" rank="species">aquilonia</taxon_name>
    <taxon_hierarchy>genus polygala;species aquilonia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cruciata</taxon_name>
    <taxon_name authority="(Fernald &amp; B. G. Schubert) A. Haines" date="unknown" rank="subspecies">aquilonia</taxon_name>
    <taxon_hierarchy>genus p.;species cruciata;subspecies aquilonia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cruciata</taxon_name>
    <taxon_name authority="Fernald &amp; B. G. Schubert" date="unknown" rank="variety">aquilonia</taxon_name>
    <taxon_hierarchy>genus p.;species cruciata;variety aquilonia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="(Nash ex B. L. Robinson) Small" date="unknown" rank="species">ramosior</taxon_name>
    <taxon_hierarchy>genus p.;species ramosior</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Drumheads</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, single-stemmed, (0.5–) 1–3 (–5) dm, usually branched distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>from slender taproot.</text>
      <biological_entity id="o1667" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="single-stemmed" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s0" value="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, usually glabrous, rarely subglabrous.</text>
      <biological_entity id="o1668" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves whorled, sometimes alternate distally;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or subsessile, or with narrow petiolelike base to 2 mm;</text>
      <biological_entity id="o1669" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" />
        <character is_modifier="false" modifier="sometimes; distally" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" />
      </biological_entity>
      <biological_entity id="o1670" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s4" value="narrow" />
        <character is_modifier="true" name="shape" src="d0_s4" value="petiolelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade mostly linear, to oblanceolate, spatulate, obovate, or narrowly elliptic, especially proximally, 8–35 (–50) × 1–5 (–7) mm, base cuneate to acute, apex rounded to obtuse or acute, surfaces glabrous.</text>
      <biological_entity id="o1671" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" notes="[duplicate value]" src="d0_s5" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="spatulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s5" value="elliptic" />
        <character char_type="range_value" from="mostly linear" name="shape" src="d0_s5" to="oblanceolate spatulate obovate or narrowly elliptic" />
        <character is_modifier="false" modifier="mostly" name="shape" notes="[duplicate value]" src="d0_s5" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="spatulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s5" value="elliptic" />
        <character char_type="range_value" from="mostly linear" name="shape" src="d0_s5" to="oblanceolate spatulate obovate or narrowly elliptic" />
        <character is_modifier="false" modifier="mostly" name="shape" notes="[duplicate value]" src="d0_s5" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="spatulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s5" value="elliptic" />
        <character char_type="range_value" from="mostly linear" name="shape" src="d0_s5" to="oblanceolate spatulate obovate or narrowly elliptic" />
        <character is_modifier="false" modifier="mostly" name="shape" notes="[duplicate value]" src="d0_s5" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="spatulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s5" value="elliptic" />
        <character char_type="range_value" from="mostly linear" name="shape" src="d0_s5" to="oblanceolate spatulate obovate or narrowly elliptic" />
        <character is_modifier="false" modifier="mostly" name="shape" notes="[duplicate value]" src="d0_s5" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="spatulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s5" value="elliptic" />
        <character char_type="range_value" from="mostly linear" name="shape" src="d0_s5" to="oblanceolate spatulate obovate or narrowly elliptic" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1672" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="especially proximally; proximally" name="shape" notes="[duplicate value]" src="d0_s5" value="cuneate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acute" />
        <character char_type="range_value" from="cuneate" modifier="especially proximally; proximally" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity id="o1673" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="rounded" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="obtuse" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acute" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="obtuse or acute" />
      </biological_entity>
      <biological_entity id="o1674" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes capitate to densely cylindric, 1–3.5 (–6) × 1–1.7 cm;</text>
      <biological_entity id="o1675" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="capitate" />
        <character is_modifier="false" modifier="densely" name="shape" notes="[duplicate value]" src="d0_s6" value="cylindric" />
        <character char_type="range_value" from="capitate" name="shape" src="d0_s6" to="densely cylindric" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="1.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle 0.5–3 (–5) cm;</text>
      <biological_entity id="o1676" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts persistent, narrowly lanceolate-ovate.</text>
      <biological_entity id="o1677" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="lanceolate-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 2–2.4 mm, glabrous.</text>
      <biological_entity id="o1678" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers usually purple or pink, rarely white, wings and distal keel sometimes green-tinged, sepals often pink, 4–6 mm;</text>
      <biological_entity id="o1679" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="white" />
      </biological_entity>
      <biological_entity id="o1680" name="wing" name_original="wings" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="green-tinged" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1681" name="keel" name_original="keel" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="green-tinged" />
      </biological_entity>
      <biological_entity id="o1682" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="pink" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals ovate, 0.8–1.5 mm, sometimes ciliolate;</text>
      <biological_entity id="o1683" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s11" value="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>wings ovate to deltate, 3.5–6 × 2.7–4 mm, apex acuminate, often strongly cuspidate;</text>
      <biological_entity id="o1684" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="deltate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="deltate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="width" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1685" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" />
        <character is_modifier="false" modifier="often strongly" name="architecture_or_shape" src="d0_s12" value="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keel 2.8–3.5 mm, crest 2-parted, with 2 or 3 entire or 2-fid lobes on each side.</text>
      <biological_entity id="o1686" name="keel" name_original="keel" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1687" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="2-parted" />
      </biological_entity>
      <biological_entity id="o1688" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" />
        <character is_modifier="true" name="shape" src="d0_s13" value="2-fid" />
      </biological_entity>
      <biological_entity id="o1689" name="side" name_original="side" src="d0_s13" type="structure" />
      <relation from="o1687" id="r210" name="with" negation="false" src="d0_s13" to="o1688" />
      <relation from="o1688" id="r211" name="on" negation="false" src="d0_s13" to="o1689" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules with winged, stipelike base, strongly oblique, subglobose, 2–2.5 × 1.8–2.1 mm, margins not winged.</text>
      <biological_entity id="o1690" name="capsule" name_original="capsules" src="d0_s14" type="structure" />
      <biological_entity id="o1691" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="winged" />
        <character is_modifier="true" name="shape" src="d0_s14" value="stipelike" />
        <character is_modifier="false" modifier="strongly" name="orientation_or_shape" src="d0_s14" value="oblique" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subglobose" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s14" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s14" to="2.1" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
      <biological_entity id="o1692" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="winged" />
        <character is_modifier="true" name="shape" src="d0_s14" value="stipelike" />
        <character is_modifier="false" modifier="strongly" name="orientation_or_shape" src="d0_s14" value="oblique" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subglobose" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s14" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s14" to="2.1" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
      <relation from="o1690" id="r212" name="with" negation="false" src="d0_s14" to="o1691" />
      <relation from="o1690" id="r213" name="with" negation="false" src="d0_s14" to="o1692" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.1–1.5 mm, short-pubescent;</text>
      <biological_entity id="o1693" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="short-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>aril 0.9–1.1 mm, lobes usually 2/3+ length of seed, rarely shorter or absent.</text>
      <biological_entity id="o1694" name="aril" name_original="aril" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s16" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>2n = 36, 40.</text>
      <biological_entity id="o1695" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character char_type="range_value" from="2/3 length of seed" name="length" src="d0_s16" upper_restricted="false" />
        <character is_modifier="false" modifier="rarely" name="height_or_length_or_size" src="d0_s16" value="shorter" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1696" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="36" />
        <character name="quantity" src="d0_s17" value="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Polygalacruciata var. aquilonia is usually a more northern form with broader leaf blades, shorter peduncles, narrower racemes, and more abruptly short-acuminate wing apices (versus strongly cuspidate and acuminate) than the more southern var. cruciata.  Extreme forms can appear distinctive.  However, in the absence of detailed populational study, these traditionally recognized varieties of P. cruciata do not merit taxonomical recognition at any rank.  This species was known from Ontario but appears to be extirpated from Canada.</discussion>
  <references>
    <reference>Sorrie, B. A. and A. S. Weakley.  2017.  Reassessment of variation within Polygala cruciata sensu lato (Polygalaceae).  Phytoneuron 2017-37: 1–9.</reference>
  </references>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows, marshes, savannas, bogs, pocosins, sand dunes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="pocosins" />
        <character name="habitat" value="sand dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Ky., La., Maine, Md., Mass., Mich., Minn., Miss., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>