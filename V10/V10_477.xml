<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POLYGALA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">lutea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 705.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species lutea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pilostaxis</taxon_name>
    <taxon_name authority="(Linnaeus) Small" date="unknown" rank="species">lutea</taxon_name>
    <taxon_hierarchy>genus pilostaxis;species lutea</taxon_hierarchy>
  </taxon_identification>
  <number>17.</number>
  <other_name type="common_name">Orange milkwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs biennial or short-lived perennial, single or multistemmed, 0.6–5 dm, unbranched or branched distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>from taproot or fibrous-root cluster.</text>
      <biological_entity id="o1925" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" />
        <character is_modifier="false" name="architecture_or_growth_form" src="d0_s0" value="multistemmed" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s0" value="branched" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sometimes laxly so, to nearly decumbent, glabrous.</text>
      <biological_entity id="o1926" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" modifier="sometimes laxly; laxly; nearly" name="growth_form_or_orientation" src="d0_s2" value="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually with basal rosette;</text>
      <biological_entity constraint="basal" id="o1928" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o1927" id="r238" name="with" negation="false" src="d0_s3" to="o1928" />
    </statement>
    <statement id="d0_s4">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s5">
      <text>sessile or subsessile, or with narrow petiolelike region to 1–2 mm;</text>
      <biological_entity id="o1927" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subsessile" />
      </biological_entity>
      <biological_entity id="o1929" name="region" name_original="region" src="d0_s5" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s5" value="narrow" />
        <character is_modifier="true" name="shape" src="d0_s5" value="petiolelike" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal blade obovate, oblanceolate, or spatulate, cauline becoming narrowly ovate or nearly linear distally, basal to 60 × 20 mm, cauline to 40 × 10 mm, succulent, base cuneate, apex bluntly rounded to obtuse or acute, especially distally, surfaces glabrous.</text>
      <biological_entity constraint="basal" id="o1930" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o1931" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="becoming narrowly" name="shape" src="d0_s6" value="ovate" />
        <character is_modifier="false" modifier="nearly; distally" name="shape" src="d0_s6" value="linear" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1932" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s6" to="60" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o1933" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s6" to="40" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="succulent" />
      </biological_entity>
      <biological_entity id="o1934" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" />
      </biological_entity>
      <biological_entity id="o1935" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="bluntly" name="shape" notes="[duplicate value]" src="d0_s6" value="rounded" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="obtuse" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="acute" />
        <character char_type="range_value" from="bluntly rounded" name="shape" src="d0_s6" to="obtuse or acute" />
      </biological_entity>
      <biological_entity id="o1936" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="especially distally; distally" name="pubescence" src="d0_s6" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes capitate, 0.8–3.5 (–4) × (0.8–) 1.2–2 cm;</text>
      <biological_entity id="o1937" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s7" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_width" src="d0_s7" to="1.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncle 3–10 cm;</text>
      <biological_entity id="o1938" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts deciduous, narrowly lanceolate.</text>
      <biological_entity id="o1939" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels winged, 1.5–2.8 mm, glabrous.</text>
      <biological_entity id="o1940" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="winged" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers usually bright orange, rarely yellow-orange, usually drying pale-yellow, 4.5–6 mm;</text>
      <biological_entity id="o1941" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="bright orange" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="yellow-orange" />
        <character is_modifier="false" modifier="usually" name="condition" src="d0_s11" value="drying" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-yellow" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals decurrent on pedicel, ovate, 1.2–2 mm, ciliolate;</text>
      <biological_entity id="o1942" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character constraint="on pedicel" constraintid="o1943" is_modifier="false" name="shape" src="d0_s12" value="decurrent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="ovate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="ciliolate" />
      </biological_entity>
      <biological_entity id="o1943" name="pedicel" name_original="pedicel" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>wings elliptic, 5–7.5 × 2.7–3.6 mm, apex acuminate to abruptly cuspidate, partially involute;</text>
      <biological_entity id="o1944" name="wing" name_original="wings" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="width" src="d0_s13" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1945" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="acuminate" />
        <character is_modifier="false" modifier="abruptly" name="shape" notes="[duplicate value]" src="d0_s13" value="cuspidate" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s13" to="abruptly cuspidate" />
        <character is_modifier="false" modifier="partially" name="shape_or_vernation" src="d0_s13" value="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>keel 3.5–6 mm, crest 2-parted, with 2–4 lobes on each side, each lobe entire or divided.</text>
      <biological_entity id="o1946" name="keel" name_original="keel" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1947" name="crest" name_original="crest" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="2-parted" />
      </biological_entity>
      <biological_entity id="o1948" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s14" to="4" />
      </biological_entity>
      <biological_entity id="o1949" name="side" name_original="side" src="d0_s14" type="structure" />
      <biological_entity id="o1950" name="lobe" name_original="lobe" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="entire" />
        <character is_modifier="false" name="shape" src="d0_s14" value="divided" />
      </biological_entity>
      <relation from="o1947" id="r239" name="with" negation="false" src="d0_s14" to="o1948" />
      <relation from="o1948" id="r240" name="on" negation="false" src="d0_s14" to="o1949" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules broadly ellipsoid to obovoid, 1.2–2.3 mm, margins not winged.</text>
      <biological_entity id="o1951" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s15" value="ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s15" value="obovoid" />
        <character char_type="range_value" from="broadly ellipsoid" name="shape" src="d0_s15" to="obovoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1952" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1–1.6 mm, pubescent;</text>
      <biological_entity id="o1953" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>aril 0.5–1.6 mm, lobes 1/2 to subequal length of seed.</text>
      <biological_entity id="o1954" name="aril" name_original="aril" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1956" name="seed" name_original="seed" src="d0_s17" type="structure">
        <character is_modifier="true" name="length" src="d0_s17" value="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>2n = 64, 68.</text>
      <biological_entity id="o1955" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character constraint="to " constraintid="o1956" name="quantity" src="d0_s17" value="1/2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1957" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="64" />
        <character name="quantity" src="d0_s18" value="68" />
      </biological_entity>
    </statement>
  </description>
  <discussion>A single lemon-yellow flowered plant of Polygala lutea has been reported from Brunswick County, North Carolina (R. R. Smith and D. B. Ward 1976); populations elsewhere may also produce yellow or yellow-orange flowers.  Smith and Ward also reported that a possible hybrid with P. rugelii had over 65% apparently non-functional pollen grains.  DNA analysis of the nrITS region (J. R. Abbott, unpubl.) found the hybrids to be polymorphic at all of the bases that differed between the parents; coupled with their rarity in the landscape despite common co-occurrence with the parents, this supports the hypothesis that they are F1 hybrids rather than established introgressives.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall (nearly year-round).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="nearly" to="fall" from="spring" />
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet soils (at least seasonally), open fields, savannas, pine flatwoods, sandy mixed pine-hardwoods, bogs, poco­sins, pond margins.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet soils" />
        <character name="habitat" value="open fields" modifier="( at least seasonally" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="sandy mixed pine-hardwoods" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="pocosins" />
        <character name="habitat" value="margins" modifier="pond" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200(–300) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Del., Fla., Ga., La., Md., Miss., N.J., N.Y., N.C., Pa., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>