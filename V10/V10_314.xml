<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Endlicher" date="1830" rank="tribe">Epilobieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EPILOBIUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Epilobium</taxon_name>
    <taxon_name authority="Haussknecht" date="1884" rank="species">oregonense</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Epilobium,</publication_title>
      <place_in_publication>276, plate 14, fig. 66.  1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section epilobium;species oregonense</taxon_hierarchy>
  </taxon_identification>
  <number>22.</number>
  <other_name type="common_name">Oregon willowherb</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with slender stolons to 18 cm with minute, rounded leaves.</text>
      <biological_entity id="o4620" name="herb" name_original="herbs" src="d0_s0" type="structure" />
      <biological_entity constraint="slender" id="o4621" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character char_type="range_value" constraint="with leaves" constraintid="o4622" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4622" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="minute" />
        <character is_modifier="true" name="shape" src="d0_s0" value="rounded" />
      </biological_entity>
      <relation from="o4620" id="r822" name="with" negation="false" src="d0_s0" to="o4621" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, often loosely matted, often flushed purple distally, terete, 8–30 (–40) cm, simple or sparsely branched from base, sub­glabrous.</text>
      <biological_entity id="o4623" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" />
        <character is_modifier="false" modifier="often loosely" name="growth_form" src="d0_s1" value="matted" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s1" value="flushed purple" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" />
        <character constraint="from base" constraintid="o4624" is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="glabrous" />
      </biological_entity>
      <biological_entity id="o4624" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite prox­imal to inflorescence, alternate distally, sub­sessile;</text>
      <biological_entity id="o4625" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="to inflorescence" constraintid="o4626" is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" modifier="distally" name="arrangement" notes="" src="d0_s2" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" />
      </biological_entity>
      <biological_entity id="o4626" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade broadly elliptic proximally, narrowly elliptic or lanceolate to sublinear distally, 5–25 × 1–7 mm, longer than internodes proximally to much shorter distally, base cuneate to rounded, margins subentire, veins extremely faint, 3–5 per side, apex obtuse, surfaces subglabrous;</text>
      <biological_entity id="o4627" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly; proximally" name="arrangement_or_shape" src="d0_s3" value="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="sublinear" />
        <character char_type="range_value" from="lanceolate" modifier="distally" name="shape" src="d0_s3" to="sublinear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
        <character constraint="than internodes" constraintid="o4628" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" />
      </biological_entity>
      <biological_entity id="o4628" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally to much; distally" name="height_or_length_or_size" src="d0_s3" value="shorter" />
      </biological_entity>
      <biological_entity id="o4629" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="cuneate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="rounded" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o4630" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subentire" />
      </biological_entity>
      <biological_entity id="o4631" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="extremely" name="prominence" src="d0_s3" value="faint" />
        <character char_type="range_value" constraint="per side" constraintid="o4632" from="3" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o4632" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o4633" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" />
      </biological_entity>
      <biological_entity id="o4634" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts extremely reduced and linear.</text>
      <biological_entity id="o4635" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="extremely" name="size" src="d0_s4" value="reduced" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences usually erect, sometimes nodding in bud, racemes, open, unbranched, sparsely strigillose and glandular puberulent.</text>
      <biological_entity id="o4636" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s5" value="erect" />
        <character constraint="in bud" constraintid="o4637" is_modifier="false" modifier="sometimes" name="orientation" src="d0_s5" value="nodding" />
      </biological_entity>
      <biological_entity id="o4637" name="bud" name_original="bud" src="d0_s5" type="structure" />
      <biological_entity id="o4638" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="unbranched" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers suberect or nodding;</text>
      <biological_entity id="o4639" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="suberect" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>buds 2–3.5 × 1–1.5 mm, apex blunt;</text>
      <biological_entity id="o4640" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4641" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 2–7 mm;</text>
      <biological_entity id="o4642" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>floral-tube 0.8–1.8 × 1–2.1 mm, with faint ring of hairs at mouth inside;</text>
      <biological_entity id="o4643" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s9" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4644" name="ring" name_original="ring" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="faint" />
      </biological_entity>
      <biological_entity id="o4645" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o4646" name="mouth" name_original="mouth" src="d0_s9" type="structure" />
      <relation from="o4643" id="r823" name="with" negation="false" src="d0_s9" to="o4644" />
      <relation from="o4644" id="r824" name="part_of" negation="false" src="d0_s9" to="o4645" />
      <relation from="o4644" id="r825" name="at" negation="false" src="d0_s9" to="o4646" />
    </statement>
    <statement id="d0_s10">
      <text>sepals often flushed purple, 2.5–4.5 × 1–1.6 mm;</text>
      <biological_entity id="o4647" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="flushed purple" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s10" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white to pink, 5–8 × 2.8–4 mm, apical notch 0.8–1.5 mm;</text>
      <biological_entity id="o4648" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="white" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s11" value="pink" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o4649" name="notch" name_original="notch" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments white, those of longer stamens 2.8–4.5 mm, those of shorter ones 2–3.8 mm;</text>
      <biological_entity id="o4650" name="filament" name_original="filaments" src="d0_s12" type="structure" constraint="stamen">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o4651" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity constraint="shorter" id="o4652" name="one" name_original="ones" src="d0_s12" type="structure" />
      <relation from="o4650" id="r826" name="part_of" negation="false" src="d0_s12" to="o4651" />
      <relation from="o4650" id="r827" name="part_of" negation="false" src="d0_s12" to="o4652" />
    </statement>
    <statement id="d0_s13">
      <text>anthers yellow-cream, 0.8–1.2 × 0.4–0.5 mm;</text>
      <biological_entity id="o4653" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow-cream" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s13" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary green to purple, 8–14 mm, sparsely strigillose and glandular puberulent;</text>
      <biological_entity id="o4654" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s14" value="green" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s14" value="purple" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s14" to="purple" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s14" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style white, 3.8–4.8 mm, glabrous, stigma subcapitate, 1–1.4 × 1–1.2 mm, surrounded by longer anthers.</text>
      <biological_entity id="o4655" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s15" to="4.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" />
      </biological_entity>
      <biological_entity id="o4656" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="subcapitate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s15" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o4657" name="anther" name_original="anthers" src="d0_s15" type="structure" />
      <relation from="o4656" id="r828" name="surrounded by" negation="false" src="d0_s15" to="o4657" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules slender, often purplish green, 21–40 (–52) mm, surfaces subglabrous;</text>
      <biological_entity id="o4658" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="slender" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s16" value="purplish green" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="52" to_unit="mm" />
        <character char_type="range_value" from="21" from_unit="mm" name="some_measurement" src="d0_s16" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4659" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 20–65 mm.</text>
      <biological_entity id="o4660" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s17" to="65" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds narrowlyoblanceoloid or subfusiform, 1–1.4 × 0.4–0.6 mm, chalazal collar 0.1–0.2 mm, light-brown, surface low papillose;</text>
      <biological_entity id="o4661" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="subfusiform" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s18" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s18" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="chalazal" id="o4662" name="collar" name_original="collar" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s18" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" />
      </biological_entity>
      <biological_entity id="o4663" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="low" />
        <character is_modifier="false" name="relief" src="d0_s18" value="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>coma persistent, whitish, 3–4 mm. 2n = 36.</text>
      <biological_entity id="o4664" name="coma" name_original="coma" src="d0_s19" type="structure">
        <character is_modifier="false" name="duration" src="d0_s19" value="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="whitish" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s19" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4665" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Epilobium oregonense is a distinctive western North American endemic, found primarily throughout the Cascade–Sierra mountain complex barely into the Transverse Ranges of southern California, and very scattered through the Rocky Mountains.  It is exceed­ingly rare in Arizona, Colorado, and New Mexico.</discussion>
  <discussion>Even though Epilobium oregonense bears some similarity to E. anagallidifolium and other members of the Alpinae group, and often grows in close proximity to them, this species does not share the derived CC chromosomal arrangement with that group, instead having the more globally widespread BB arrangement.  The similarities with E. anagallidifolium include the small stature, small, obtuse, and subentire leaves, and long pedicels in fruit; however, E. oregonense differs by its long, threadlike stolons, distal leaves extremely narrow and reduced in size relative to the internodes, and near complete absence of pubescence on the plant, including a lack of raised lines of hairs on the stems.</discussion>
  <discussion>Another species with which Epilobium oregonense has been confused is E. hallianum, but that species al­ways forms condensed basal turions, is more strictly erect, and generally has larger and more denticulate leaves.  The distinctive and diagnostic stolons of E. oregonense are similar to those found in E. palustre and related species (all of which also have the BB chromosome arrangement), except that those of E. oregonense never terminate in a condensed turion, as found in those other species.  The exact affinities of E. oregonense remain uncertain, but it appears to be most closely related to the E. palustre complex.</discussion>
  <discussion>Some specimens of Epilobium oregonense grow as floating mats in cold streams; these specimens are notably large, with particularly strong development of basal stolons and larger, more lanceolate leaves.  As evi­denced by mixed herbarium collections, E. oregonense grows sympatrically with several congeners, including E. anagallidifolium, E. ciliatum subspp. ciliatum and glandulosum, E. hallianum, and E. hornemannii, and hybridizes occasionally, at least with E. ciliatum subsp. ciliatum and E. hornemannii.</discussion>
  <discussion>Epilobium oregonense var. gracillimum Trelease, which pertains here, was not validly published, and other names based on it are also invalid.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane to subalpine boggy or mossy areas, wet meadows, protected, semi-shaded stream banks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane to subalpine" modifier="boggy" />
        <character name="habitat" value="areas" modifier="mossy" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="semi-shaded stream banks" modifier="protected" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–3000(–3500) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>