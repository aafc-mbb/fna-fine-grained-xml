<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Ludwigioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LUDWIGIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Ludwigia</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">virgata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 89.  1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily ludwigioideae;genus ludwigia;section ludwigia;species virgata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isnardia</taxon_name>
    <taxon_name authority="(Michaux) de Candolle" date="unknown" rank="species">virgata</taxon_name>
    <taxon_hierarchy>genus isnardia;species virgata</taxon_hierarchy>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Savannah primrose-willow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots fascicled, often fusiform, or spreading horizontally.</text>
      <biological_entity id="o1369" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="fascicled" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s0" value="fusiform" />
        <character is_modifier="false" modifier="horizontally" name="orientation" src="d0_s0" value="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems subterete to scarcely angled, with narrow raised lines or wings decurrent from leaf-axils, 45–85 cm, branched mainly near base, glabrate with strigillose raised lines or strigillose.</text>
      <biological_entity id="o1370" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s1" value="subterete" />
        <character is_modifier="false" modifier="scarcely" name="shape" notes="[duplicate value]" src="d0_s1" value="angled" />
        <character char_type="range_value" from="subterete" name="shape" src="d0_s1" to="scarcely angled" />
        <character constraint="with" is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" />
        <character constraint="from leaf-axils" constraintid="o1373" is_modifier="false" name="shape" src="d0_s1" value="decurrent" />
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="85" to_unit="cm" />
        <character constraint="near base" constraintid="o1374" is_modifier="false" name="architecture" src="d0_s1" value="branched" />
        <character constraint="with lines" constraintid="o1375" is_modifier="false" name="pubescence" notes="" src="d0_s1" value="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigillose" />
      </biological_entity>
      <biological_entity id="o1371" name="line" name_original="lines" src="d0_s1" type="structure" />
      <biological_entity id="o1372" name="wing" name_original="wings" src="d0_s1" type="structure" />
      <biological_entity id="o1373" name="leaf-axil" name_original="leaf-axils" src="d0_s1" type="structure" />
      <biological_entity id="o1374" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o1375" name="line" name_original="lines" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s1" value="strigillose" />
        <character is_modifier="true" name="prominence" src="d0_s1" value="raised" />
      </biological_entity>
      <relation from="o1370" id="r231" name="raised" negation="false" src="d0_s1" to="o1371" />
      <relation from="o1370" id="r232" name="raised" negation="false" src="d0_s1" to="o1372" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules narrowly deltate, 0.1–0.2 × 0.1–0.15 mm;</text>
      <biological_entity id="o1376" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1377" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="deltate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s2" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s2" to="0.15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity id="o1378" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to obovate proximally, lanceolate-linear to linear distally, 2–7 × 0.2–1 (–1.5) cm, base cuneate, margins entire, apex acute to rounded, surfaces strigillose, densely so particularly along veins;</text>
      <biological_entity id="o1379" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1380" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="obovate" />
        <character is_modifier="false" modifier="proximally" name="shape" notes="[duplicate value]" src="d0_s4" value="," />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="lanceolate-linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="linear" />
        <character char_type="range_value" from="ovate" modifier="distally" name="shape" src="d0_s4" to="obovate proximally" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s4" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1381" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" />
      </biological_entity>
      <biological_entity id="o1382" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" />
      </biological_entity>
      <biological_entity id="o1383" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="rounded" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o1384" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigillose" />
      </biological_entity>
      <biological_entity id="o1385" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <relation from="o1384" id="r233" modifier="densely; particularly" name="along" negation="false" src="d0_s4" to="o1385" />
    </statement>
    <statement id="d0_s5">
      <text>bracts usually very reduced in size, sub­linear.</text>
      <biological_entity id="o1386" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o1387" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually very" name="size" src="d0_s5" value="reduced" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences sparse racemes, flowers solitary in leaf-axils;</text>
      <biological_entity id="o1388" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o1389" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s6" value="sparse" />
      </biological_entity>
      <biological_entity id="o1390" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="in leaf-axils" constraintid="o1391" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" />
      </biological_entity>
      <biological_entity id="o1391" name="leaf-axil" name_original="leaf-axils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracteoles attached in subopposite pairs on distal 1/3 of pedicel, lanceolate-linear, 0.7–3.2 (–5) ×0.2–0.5 mm, margins entire, apex acute, surfaces strigillose.</text>
      <biological_entity id="o1392" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character constraint="in pairs" constraintid="o1393" is_modifier="false" name="fixation" src="d0_s7" value="attached" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="lanceolate-linear" />
        <character char_type="range_value" from="3.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s7" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1393" name="pair" name_original="pairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="subopposite" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1394" name="1/3" name_original="1/3" src="d0_s7" type="structure" />
      <biological_entity id="o1395" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <biological_entity id="o1396" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" />
      </biological_entity>
      <biological_entity id="o1397" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" />
      </biological_entity>
      <biological_entity id="o1398" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="strigillose" />
      </biological_entity>
      <relation from="o1393" id="r234" name="on" negation="false" src="d0_s7" to="o1394" />
      <relation from="o1394" id="r235" name="part_of" negation="false" src="d0_s7" to="o1395" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals strongly reflexed, narrowly ovate-deltate, 6–10 × 2.5–4.5 mm, margins entire, apex acute, surfaces finely strigillose to glabrate;</text>
      <biological_entity id="o1399" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1400" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s8" value="reflexed" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="ovate-deltate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1401" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" />
      </biological_entity>
      <biological_entity id="o1402" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" />
      </biological_entity>
      <biological_entity id="o1403" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" notes="[duplicate value]" src="d0_s8" value="strigillose" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s8" value="glabrate" />
        <character char_type="range_value" from="finely strigillose" name="pubescence" src="d0_s8" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals cor­date, 14–19 × 13–15 mm, base attenuate, apex emarginate;</text>
      <biological_entity id="o1404" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1405" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s9" to="19" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1406" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="attenuate" />
      </biological_entity>
      <biological_entity id="o1407" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments opaque white, awl-shaped, 1.9–4.2 mm, anthers 2–4 × 0.6–1 mm;</text>
      <biological_entity id="o1408" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1409" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="opaque white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="awl--shaped" value_original="awl-shaped" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s10" to="4.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1410" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary subcuboid to globose, 3–4 × 3–4 mm;</text>
      <biological_entity id="o1411" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1412" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc elevated, domed, 0.8–1.4 mm diam., prominently 4-lobed, ringed with silky-curly hairs;</text>
      <biological_entity id="o1413" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o1414" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="elevated" />
        <character is_modifier="false" name="shape" src="d0_s12" value="domed" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="diameter" src="d0_s12" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="prominently" name="shape" src="d0_s12" value="4-lobed" />
        <character constraint="with hairs" constraintid="o1415" is_modifier="false" name="relief" src="d0_s12" value="ringed" />
      </biological_entity>
      <biological_entity id="o1415" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="silky-curly" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 5–9.5 mm, gla­brous, stigma capitate to hemispherical, 0.6–1.3 × 1.3–2.6 mm, shallowly 4-lobed, as long as or exserted beyond anthers.</text>
      <biological_entity id="o1416" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1417" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="9.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1418" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="capitate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="hemispherical" />
        <character char_type="range_value" from="capitate" name="shape" src="d0_s13" to="hemispherical" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s13" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s13" to="2.6" to_unit="mm" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s13" value="4-lobed" />
      </biological_entity>
      <biological_entity id="o1419" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <relation from="o1418" id="r236" name="exserted beyond" negation="false" src="d0_s13" to="o1419" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules subglobose to ellipsoid, 3.5–6.8 × 3.3–4.3 mm, 4-angled, angles not developed into wings, pedicel 6.5–17 mm.</text>
      <biological_entity id="o1420" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="subglobose" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="ellipsoid" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s14" to="ellipsoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s14" to="6.8" to_unit="mm" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="width" src="d0_s14" to="4.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-angled" />
      </biological_entity>
      <biological_entity id="o1421" name="angle" name_original="angles" src="d0_s14" type="structure">
        <character constraint="into wings" constraintid="o1422" is_modifier="false" modifier="not" name="development" src="d0_s14" value="developed" />
      </biological_entity>
      <biological_entity id="o1422" name="wing" name_original="wings" src="d0_s14" type="structure" />
      <biological_entity id="o1423" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s14" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds light-brown, elliptic-oblong to reniform, 0.5–0.7 × 0.3–0.4 mm, surface cells elongate transversely to seed length or elongate parallel to length near raphe.</text>
      <biological_entity id="o1424" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="light-brown" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s15" value="elliptic-oblong" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s15" value="reniform" />
        <character char_type="range_value" from="elliptic-oblong" name="shape" src="d0_s15" to="reniform" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s15" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1426" name="seed" name_original="seed" src="d0_s15" type="structure" />
      <biological_entity id="o1427" name="raphe" name_original="raphe" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="elongate" />
        <character is_modifier="true" name="arrangement" src="d0_s15" value="parallel" />
        <character is_modifier="true" name="character" src="d0_s15" value="length" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 16.</text>
      <biological_entity constraint="surface" id="o1425" name="cel" name_original="cells" src="d0_s15" type="structure">
        <character constraint="to " constraintid="o1427" is_modifier="false" name="shape" src="d0_s15" value="elongate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1428" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy savannas, pinelands, damp roadside ditches, margins of ponds, bogs, irrigated fields, usually within 75 miles of sea coast.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy savannas" />
        <character name="habitat" value="pinelands" />
        <character name="habitat" value="roadside ditches" modifier="damp" />
        <character name="habitat" value="margins" constraint="of ponds , bogs , irrigated fields ," />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="irrigated fields" />
        <character name="habitat" value="75 miles" modifier="usually within" constraint="of sea coast" />
        <character name="habitat" value="sea coast" />
        <character name="habitat" value="damp" modifier="usually" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>