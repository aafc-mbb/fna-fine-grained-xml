<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Haller" date="1768" rank="genus">POLYGALOIDES</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Stirp. Helv.</publication_title>
      <place_in_publication>1: 149.  1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygaloides</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Polygala and Latin -oides, resembling</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(de Candolle) Spach" date="unknown" rank="genus">Chamaebuxus</taxon_name>
    <taxon_hierarchy>genus chamaebuxus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Polygala</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="section">Chamaebuxus</taxon_name>
    <taxon_hierarchy>genus polygala;section chamaebuxus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygala</taxon_name>
    <taxon_name authority="(de Candolle) Duchesne" date="unknown" rank="subgenus">Chamaebuxus</taxon_name>
    <taxon_hierarchy>genus polygala;subgenus chamaebuxus</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, usually multistemmed.</text>
      <biological_entity id="o2390" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" modifier="usually" name="architecture_or_growth_form" src="d0_s0" value="multistemmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, with short erect shoots, or decumbent, rhizomatous or stoloniferous, usually glabrous.</text>
      <biological_entity id="o2391" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" />
        <character is_modifier="false" name="growth_form_or_orientation" notes="" src="d0_s1" value="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" />
      </biological_entity>
      <biological_entity id="o2392" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" />
      </biological_entity>
      <relation from="o2391" id="r291" name="with" negation="false" src="d0_s1" to="o2392" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate or subsessile;</text>
    </statement>
    <statement id="d0_s4">
      <text>dimorphic, bractlike proximally, well-developed, uniform and somewhat clustered distally;</text>
      <biological_entity id="o2393" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" />
        <character is_modifier="false" name="growth_form" src="d0_s4" value="dimorphic" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s4" value="bractlike" />
        <character is_modifier="false" name="development" src="d0_s4" value="well-developed" />
        <character is_modifier="false" name="variability" src="d0_s4" value="uniform" />
        <character is_modifier="false" modifier="somewhat; distally" name="arrangement_or_growth_form" src="d0_s4" value="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade surfaces glabrous or pubescent.</text>
      <biological_entity constraint="blade" id="o2394" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, racemes or appearing corymblike or 1–4 (or 5) -flowered (from poorly developed peduncle);</text>
      <biological_entity id="o2395" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" />
      </biological_entity>
      <biological_entity id="o2396" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="corymblike" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle usually present;</text>
      <biological_entity id="o2397" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s7" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts deciduous.</text>
      <biological_entity id="o2398" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels present.</text>
      <biological_entity id="o2399" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers usually pink or rose-purple, rarely white, sepals pale-pink or whitish, crest often yellowish, (2–) 15–23 mm, cleistogamous sometimes present;</text>
      <biological_entity id="o2400" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="rose-purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="white" />
      </biological_entity>
      <biological_entity id="o2401" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="whitish" />
      </biological_entity>
      <biological_entity id="o2402" name="crest" name_original="crest" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="yellowish" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="23" to_unit="mm" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="cleistogamous" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s10" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals deciduous, glabrous;</text>
      <biological_entity id="o2403" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="deciduous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>wings deciduous, (10–) 13–20 mm, glabrous;</text>
      <biological_entity id="o2404" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keel crested, crest 2-parted, with 2–4 lobes on each side, glabrous;</text>
      <biological_entity id="o2405" name="keel" name_original="keel" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="crested" />
      </biological_entity>
      <biological_entity id="o2406" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="2-parted" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s13" value="glabrous" />
      </biological_entity>
      <biological_entity id="o2407" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s13" to="4" />
      </biological_entity>
      <biological_entity id="o2408" name="side" name_original="side" src="d0_s13" type="structure" />
      <relation from="o2406" id="r292" name="with" negation="false" src="d0_s13" to="o2407" />
      <relation from="o2407" id="r293" name="on" negation="false" src="d0_s13" to="o2408" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 6 (–8) in chasmogamous flowers, fewer in cleistogamous flowers, not grouped;</text>
      <biological_entity id="o2409" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="8" />
        <character constraint="in flowers" constraintid="o2410" name="quantity" src="d0_s14" value="6" />
      </biological_entity>
      <biological_entity id="o2410" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="chasmogamous" />
        <character constraint="in flowers" constraintid="o2411" is_modifier="false" name="quantity" notes="" src="d0_s14" value="fewer" />
      </biological_entity>
      <biological_entity id="o2411" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="cleistogamous" />
        <character is_modifier="false" modifier="not" name="arrangement" notes="" src="d0_s14" value="grouped" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 2-loculed.</text>
      <biological_entity id="o2412" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, dehiscent, margins narrowly winged apically, glabrous.</text>
      <biological_entity constraint="fruits" id="o2413" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="dehiscent" />
      </biological_entity>
      <biological_entity id="o2414" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="narrowly; apically" name="architecture" src="d0_s16" value="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds pubescent, arillate.</text>
      <biological_entity id="o2415" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="arillate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 6 or 7 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>