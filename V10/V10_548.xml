<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LYTHRUM</taxon_name>
    <taxon_name authority="(Linnaeus) D. A. Webb" date="1967" rank="species">portula</taxon_name>
    <place_of_publication>
      <publication_title>Feddes Repert.</publication_title>
      <place_in_publication>74: 13.  1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus lythrum;species portula</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Peplis</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">portula</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 332.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus peplis;species portula</taxon_hierarchy>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Water purslane</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, slender, delicate, 0.5–2.5 dm, green, glabrous.</text>
      <biological_entity id="o3813" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="delicate" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems frequently creeping and rooting at nodes, procumbent, decumbent, or weakly erect, often branched near base.</text>
      <biological_entity id="o3814" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="frequently" name="growth_form_or_orientation" src="d0_s1" value="creeping" />
        <character constraint="at nodes" constraintid="o3815" is_modifier="false" name="architecture" src="d0_s1" value="rooting" />
        <character is_modifier="false" name="growth_form" notes="" src="d0_s1" value="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="erect" />
        <character constraint="near base" constraintid="o3816" is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="branched" />
      </biological_entity>
      <biological_entity id="o3815" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o3816" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity id="o3817" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade spatulate or oblong to broadly obovate or orbiculate, 5–15 × 3–8 mm, base narrowly attenuate.</text>
      <biological_entity id="o3818" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="spatulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="oblong" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s4" value="obovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="orbiculate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="broadly obovate or orbiculate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflo­res­cences spikelike.</text>
      <biological_entity id="o3819" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers opposite or alternate, along most of stem, sessile to subsessile, monostylous;</text>
      <biological_entity id="o3820" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="alternate" />
        <character is_modifier="false" name="architecture" notes="[duplicate value]" src="d0_s6" value="sessile" />
        <character is_modifier="false" name="architecture" notes="[duplicate value]" src="d0_s6" value="subsessile" />
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s6" to="subsessile" />
      </biological_entity>
      <biological_entity id="o3821" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o3820" id="r459" modifier="along" name="part_of" negation="false" src="d0_s6" to="o3821" />
    </statement>
    <statement id="d0_s7">
      <text>floral-tube broadly campanulate, 1 × 1.5 mm;</text>
      <biological_entity id="o3822" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="campanulate" />
        <character name="length" src="d0_s7" unit="mm" value="1" />
        <character name="width" src="d0_s7" unit="mm" value="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>epicalyx segments equal to or to 2 times longer than sepals;</text>
      <biological_entity constraint="epicalyx" id="o3823" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" />
        <character constraint="sepal" constraintid="o3824" is_modifier="false" name="length_or_size" src="d0_s8" value="0-2 times longer than sepals" />
      </biological_entity>
      <biological_entity id="o3824" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 1/3–1/2 floral-tube length, apex dark red;</text>
      <biological_entity id="o3825" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s9" to="1/2" />
      </biological_entity>
      <biological_entity id="o3826" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure" />
      <biological_entity id="o3827" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="length" src="d0_s9" value="dark red" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals early caducous, 0 or 6, white to pink or rose, 1 × 0.7 mm;</text>
      <biological_entity id="o3828" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="caducous" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="count" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="list" />
        <character name="presence" src="d0_s10" unit="or punct white to pink or rose" value="absent" />
        <character name="quantity" src="d0_s10" unit="or punct white to pink or rose" value="6" />
        <character name="length" src="d0_s10" unit="mm" value="1" />
        <character name="width" src="d0_s10" unit="mm" value="0.7" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>nectary ab­sent;</text>
      <biological_entity id="o3829" name="nectary" name_original="nectary" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 6.</text>
      <biological_entity id="o3830" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules surpassing floral-tube, indehiscent, splitting irregularly.</text>
      <biological_entity id="o3831" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="indehiscent" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_dehiscence" src="d0_s13" value="splitting" />
      </biological_entity>
      <biological_entity id="o3832" name="floral-tube" name_original="floral-tube" src="d0_s13" type="structure" />
      <relation from="o3831" id="r460" name="surpassing" negation="false" src="d0_s13" to="o3832" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds ca. 10–25, subglobose.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 10 (Europe).</text>
      <biological_entity id="o3833" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="25" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subglobose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3834" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lythrum portula was long regarded as belonging to Peplis and is still accepted in that genus in some floras (D. A. Webb 1967).  It is widespread in western Asia and Europe and has become established in the northwestern United States and adjacent Canada.  It may be expected occasionally elsewhere in cool temperate regions in the flora area, as suggested by a 1999 introduction in Lake County, Ohio, presumably by seeds in soil accompanying plants purchased from a nursery on the West Coast.  The Ohio population was recognized as non-native and destroyed (J. K. Bissell, pers. comm.).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Drying ponds, lake margins, shallow water.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="drying ponds" />
        <character name="habitat" value="lake margins" />
        <character name="habitat" value="shallow water" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C.; Calif., Oreg., Wash.; Europe; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>