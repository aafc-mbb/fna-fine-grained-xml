<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Endlicher" date="1830" rank="tribe">Epilobieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EPILOBIUM</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) P. H. Raven" date="1977" rank="section">Cordylophorum</taxon_name>
    <taxon_name authority="P. H. Raven" date="1977" rank="subsection">Petrolobium</taxon_name>
    <taxon_name authority="Munz" date="1929" rank="species">nevadense</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>56: 166.  1929</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section cordylophorum;subsection petrolobium;species nevadense</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Nevada willowherb</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with many shoots from thick, woody caudex.</text>
      <biological_entity id="o3529" name="herb" name_original="herbs" src="d0_s0" type="structure" />
      <biological_entity id="o3530" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="many" />
      </biological_entity>
      <biological_entity id="o3531" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thick" />
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" />
      </biological_entity>
      <relation from="o3529" id="r596" name="with" negation="false" src="d0_s0" to="o3530" />
      <relation from="o3530" id="r597" name="from" negation="false" src="d0_s0" to="o3531" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, terete, 10–50 cm, branched at base and apically, densely strigillose throughout, sometimes mixed villous distally.</text>
      <biological_entity id="o3532" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character constraint="at base" constraintid="o3533" is_modifier="false" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" modifier="apically; densely; throughout" name="pubescence" notes="" src="d0_s1" value="strigillose" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s1" value="mixed" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="villous" />
      </biological_entity>
      <biological_entity id="o3533" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal pairs often early-deciduous, petiole 1–4 mm, blade lanceolate-elliptic to narrowly so, ± folded along midrib, 0.9–1.7 × 0.2–0.6 cm, shorter than internodes, base attenuate or narrowly cuneate, margins denticulate, 6–10 low teeth per side, lateral-veins inconspicuous or absent, apex acute with deciduous, rigid mucronate gland, surfaces usually glabrescent with scattered hairs on abaxial midrib, rarely strigillose-villous throughout;</text>
      <biological_entity id="o3534" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o3535" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="duration" src="d0_s2" value="early-deciduous" />
      </biological_entity>
      <biological_entity id="o3536" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3537" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="lanceolate-elliptic" />
        <character constraint="along midrib" constraintid="o3538" is_modifier="false" modifier="narrowly; more or less" name="architecture_or_shape" src="d0_s2" value="folded" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="length" notes="" src="d0_s2" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" notes="" src="d0_s2" to="0.6" to_unit="cm" />
        <character constraint="than internodes" constraintid="o3539" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" />
      </biological_entity>
      <biological_entity id="o3538" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
      <biological_entity id="o3539" name="internode" name_original="internodes" src="d0_s2" type="structure" />
      <biological_entity id="o3540" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="attenuate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="cuneate" />
      </biological_entity>
      <biological_entity id="o3541" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="denticulate" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="10" />
      </biological_entity>
      <biological_entity id="o3542" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="true" name="position" src="d0_s2" value="low" />
      </biological_entity>
      <biological_entity id="o3543" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o3544" name="lateral-vein" name_original="lateral-veins" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" />
      </biological_entity>
      <biological_entity id="o3545" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character constraint="with gland" constraintid="o3546" is_modifier="false" name="shape" src="d0_s2" value="acute" />
      </biological_entity>
      <biological_entity id="o3546" name="gland" name_original="gland" src="d0_s2" type="structure">
        <character is_modifier="true" name="duration" src="d0_s2" value="deciduous" />
        <character is_modifier="true" name="texture" src="d0_s2" value="rigid" />
        <character is_modifier="true" name="shape" src="d0_s2" value="mucronate" />
      </biological_entity>
      <biological_entity id="o3547" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character constraint="with hairs" constraintid="o3548" is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrescent" />
        <character is_modifier="false" modifier="rarely; throughout" name="pubescence" notes="" src="d0_s2" value="strigillose-villous" />
      </biological_entity>
      <biological_entity id="o3548" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3549" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
      <relation from="o3542" id="r598" name="per" negation="false" src="d0_s2" to="o3543" />
      <relation from="o3548" id="r599" name="on" negation="false" src="d0_s2" to="o3549" />
    </statement>
    <statement id="d0_s3">
      <text>bracts much reduced, sublinear, often attached to pedicel.</text>
      <biological_entity id="o3550" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3551" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s3" value="reduced" />
        <character is_modifier="false" name="shape" src="d0_s3" value="sublinear" />
        <character constraint="to pedicel" constraintid="o3552" is_modifier="false" modifier="often" name="fixation" src="d0_s3" value="attached" />
      </biological_entity>
      <biological_entity id="o3552" name="pedicel" name_original="pedicel" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences erect, open racemes or panicles, strigillose, often mixed glandular puberulent.</text>
      <biological_entity id="o3553" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" />
      </biological_entity>
      <biological_entity id="o3554" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="open" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigillose" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s4" value="mixed" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" />
      </biological_entity>
      <biological_entity id="o3555" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="open" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigillose" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s4" value="mixed" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers erect to ± nodding;</text>
      <biological_entity id="o3556" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s5" value="erect" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="[duplicate value]" src="d0_s5" value="nodding" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s5" to="more or less nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>buds rounded-obovoid, 5–6 × 3–4 mm;</text>
      <biological_entity id="o3557" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded-obovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral-tube with slight constriction 2–3 mm distal to base, 2.7–3.2 (–5) × 1.8–2.5 (–3.1) mm, without ring or scales inside, glabrous;</text>
      <biological_entity id="o3558" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character constraint="to base" constraintid="o3560" is_modifier="false" name="position_or_shape" src="d0_s7" value="distal" />
        <character char_type="range_value" from="3.2" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" notes="" src="d0_s7" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s7" to="3.1" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" notes="" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="glabrous" />
      </biological_entity>
      <biological_entity id="o3559" name="constriction" name_original="constriction" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="slight" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3560" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o3561" name="ring" name_original="ring" src="d0_s7" type="structure" />
      <biological_entity id="o3562" name="scale" name_original="scales" src="d0_s7" type="structure" />
      <relation from="o3558" id="r600" name="with" negation="false" src="d0_s7" to="o3559" />
      <relation from="o3558" id="r601" name="without" negation="false" src="d0_s7" to="o3561" />
      <relation from="o3558" id="r602" name="without" negation="false" src="d0_s7" to="o3562" />
    </statement>
    <statement id="d0_s8">
      <text>sepals erect or sometimes deflexed in late anthesis, green or reddish green, lanceolate, 2.6–4.2 × 0.9–1.3 mm, apex acute;</text>
      <biological_entity id="o3563" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" />
        <character constraint="in apex" constraintid="o3564" is_modifier="false" modifier="sometimes" name="orientation" src="d0_s8" value="deflexed" />
      </biological_entity>
      <biological_entity id="o3564" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="anthesis" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish green" />
        <character is_modifier="true" name="shape" src="d0_s8" value="lanceolate" />
        <character char_type="range_value" from="2.6" from_unit="mm" is_modifier="true" name="length" src="d0_s8" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" is_modifier="true" name="width" src="d0_s8" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals deep rose-purple, obcordate, 5–7.2 × 3.2–4.1 mm, apical notch 2–3 mm;</text>
      <biological_entity id="o3565" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="rose-purple" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obcordate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="7.2" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="width" src="d0_s9" to="4.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o3566" name="notch" name_original="notch" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments cream or white, those of longer stamens 5–7.5 mm, those of shorter ones 3.5–5.5 mm;</text>
      <biological_entity id="o3567" name="filament" name_original="filaments" src="d0_s10" type="structure" constraint="stamen">
        <character is_modifier="false" name="coloration" src="d0_s10" value="cream" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o3568" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity constraint="shorter" id="o3569" name="one" name_original="ones" src="d0_s10" type="structure" />
      <relation from="o3567" id="r603" name="part_of" negation="false" src="d0_s10" to="o3568" />
      <relation from="o3567" id="r604" name="part_of" negation="false" src="d0_s10" to="o3569" />
    </statement>
    <statement id="d0_s11">
      <text>anthers cream, 1–1.8 × 0.5–0.8 mm, scarcely apiculate;</text>
      <biological_entity id="o3570" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="scarcely" name="architecture_or_shape" src="d0_s11" value="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary 2.5–3.8 mm, densely strigillose and/or glandular puberulent;</text>
      <biological_entity id="o3571" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3.8" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style cream, 6–9.5 mm, glabrous, stigma 4-lobed, 0.8–1.2 × 1–1.5 mm, lobes reflexed or sometimes incompletely spread, then forming cuplike structure, exserted beyond longer anthers.</text>
      <biological_entity id="o3572" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="cream" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="9.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" />
      </biological_entity>
      <biological_entity id="o3573" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="4-lobed" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s13" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3574" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="reflexed" />
        <character is_modifier="false" modifier="sometimes incompletely" name="orientation" src="d0_s13" value="spread" />
      </biological_entity>
      <biological_entity id="o3575" name="structure" name_original="structure" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="cuplike" />
      </biological_entity>
      <biological_entity constraint="longer" id="o3576" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <relation from="o3574" id="r605" name="forming" negation="false" src="d0_s13" to="o3575" />
      <relation from="o3574" id="r606" name="exserted beyond" negation="false" src="d0_s13" to="o3576" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules erect, subfusiform, 8–12 mm, surfaces strigillose and/or glandular puberulent;</text>
      <biological_entity id="o3577" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subfusiform" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3578" name="surface" name_original="surfaces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pedicel 1–1.8 mm.</text>
      <biological_entity id="o3579" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds obovoid, with constriction 0.6–1 mm from micropylar end, 2.1–2.9 × 1.2–1.5 mm, very inconspicuous chalazal collar 0.05–0.06 mm wide, dark-brown, surface low papillose, papillae often with central pit;</text>
      <biological_entity id="o3580" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="length" notes="" src="d0_s16" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" notes="" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3581" name="constriction" name_original="constriction" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="from micropylar, end" constraintid="o3582, o3583" from="0.6" from_unit="mm" name="location" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3582" name="micropylar" name_original="micropylar" src="d0_s16" type="structure" />
      <biological_entity id="o3583" name="end" name_original="end" src="d0_s16" type="structure" />
      <biological_entity constraint="chalazal" id="o3584" name="collar" name_original="collar" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="very" name="prominence" src="d0_s16" value="inconspicuous" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s16" to="0.06" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" />
      </biological_entity>
      <biological_entity id="o3585" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="low" />
        <character is_modifier="false" name="relief" src="d0_s16" value="papillose" />
      </biological_entity>
      <biological_entity id="o3586" name="papilla" name_original="papillae" src="d0_s16" type="structure" />
      <biological_entity constraint="central" id="o3587" name="pit" name_original="pit" src="d0_s16" type="structure" />
      <relation from="o3580" id="r607" name="with" negation="false" src="d0_s16" to="o3581" />
      <relation from="o3586" id="r608" name="with" negation="false" src="d0_s16" to="o3587" />
    </statement>
    <statement id="d0_s17">
      <text>coma easily detached, white, 6–7.5 mm. 2n = 30.</text>
      <biological_entity id="o3588" name="coma" name_original="coma" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="easily" name="fusion" src="d0_s17" value="detached" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s17" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3589" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="30" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In his description of Epilobium nevadense, Munz clearly recognized its affinity to E. nivium and suggested a close relationship between these two species and E. brachycarpum, based on similarities in seed and floral morphology.  S. R. Seavey and P. H. Raven (1977c) demonstrated the close affinity between E. nivium and E. nevadense by forming fully fertile (99%) hybrids.  However, compared to E. nivium, E. nevadense has denticulate, subglabrous leaves (versus subentire, densely pubescent leaves) and shorter floral tube [2.7–3.2(–5) mm] versus longer (5.2–9.5 mm) in E. nivium; furthermore, the two have com­pletely non-overlapping geographical ranges.  In overall morphology and cytology, these two species (and the somewhat more distantly related E. suffruticosum) are quite distinct from the rest of the genus.</discussion>
  <discussion>Originally known only from the Charleston Mountains in southern Nevada, Epilobium nevadense has since been collected in northern Arizona, Eureka and Lincoln counties in Nevada, and in three counties of southwestern Utah.  It may be more widespread in this region, much of which (especially in southern Nevada) consists of military reserves that are inaccessible to collectors.  Although it was at one time considered endangered (S. D. Ripley 1975) due to the relatively low number of collections and threats from increased recreational use in its area of occurrence, it is no longer considered a candidate for listing (http://endangered.fws.gov).  Several collections of this species show evidence of seed predation, apparently by moth larvae (H. N. Mozingo and Margaret Williams 1980), and S. R. Seavey and P. H. Raven (1977c) reported that larvae found in capsules from the locality in the Charleston Mountains were identified as Mompha (Momphidae, Gelechioidea).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Loose scree slopes, limestone talus, sandy soils at base of steep rock faces in pinyon pine-juniper-mountain brush communities.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="loose scree slopes" />
        <character name="habitat" value="limestone talus" />
        <character name="habitat" value="sandy soils" constraint="at base of steep rock" />
        <character name="habitat" value="base" constraint="of steep rock" />
        <character name="habitat" value="steep rock" />
        <character name="habitat" value="pinyon pine-juniper-mountain brush communities" modifier="faces in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>