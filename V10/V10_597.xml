<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Warren L. Wagner</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="genus">CAMISSONIOPSIS</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 123.  2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus camissoniopsis</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Camissonia and Greek -opsis, resemblance</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Spach" date=" 1835" rank="genus">Agassizia</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Nat Vég.</publication_title>
      <place_in_publication>4: 347.  1835</place_in_publication>
      <other_info_on_pub>not Chavennes 1833 [Plantaginaceae]</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus agassizia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Link" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="P. H. Raven" date="unknown" rank="section">Holostigma</taxon_name>
    <taxon_hierarchy>genus camissonia;section holostigma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Spach 1835" date="unknown" rank="genus">Holostigma</taxon_name>
    <other_info_on_name>not G. Don 1834 [Campanulaceae]</other_info_on_name>
    <taxon_hierarchy>genus holostigma</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs,usually annual, rarely short-lived perennial, caulescent.</text>
      <biological_entity id="o5019" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to ascending or erect, often with reddish-brown or white exfoliating epidermis.</text>
      <biological_entity id="o5020" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="prostrate" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="ascending" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="erect" />
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="ascending or erect" />
      </biological_entity>
      <biological_entity id="o5021" name="epidermis" name_original="epidermis" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="reddish-brown" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="white" />
        <character is_modifier="true" name="relief" src="d0_s1" value="exfoliating" />
      </biological_entity>
      <relation from="o5020" id="r644" modifier="often" name="with" negation="false" src="d0_s1" to="o5021" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline and often in a basal rosette, alternate;</text>
      <biological_entity id="o5022" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s2" value="alternate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5023" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <relation from="o5022" id="r645" modifier="often" name="in" negation="false" src="d0_s2" to="o5023" />
    </statement>
    <statement id="d0_s3">
      <text>stipules absent;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or petiolate;</text>
      <biological_entity id="o5024" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade margins dentate, denticulate, or serrulate.</text>
      <biological_entity constraint="blade" id="o5025" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences spikes, erect or nodding at anthesis.</text>
      <biological_entity constraint="inflorescences" id="o5026" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s6" value="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual, actinomorphic, buds erect;</text>
      <biological_entity id="o5027" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="actinomorphic" />
      </biological_entity>
      <biological_entity id="o5028" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral-tube deciduous (with sepals, petals, and stamens) after anthesis, with basal nectary;</text>
      <biological_entity id="o5029" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character constraint="after anthesis" is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5030" name="nectary" name_original="nectary" src="d0_s8" type="structure" />
      <relation from="o5029" id="r646" name="with" negation="false" src="d0_s8" to="o5030" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 4, usually reflexed in pairs, sometimes separately;</text>
      <biological_entity id="o5031" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" />
        <character constraint="in pairs" constraintid="o5032" is_modifier="false" modifier="usually" name="orientation" src="d0_s9" value="reflexed" />
      </biological_entity>
      <biological_entity id="o5032" name="pair" name_original="pairs" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals 4, yellow, fading red, with 1+ red dots basally;</text>
      <biological_entity id="o5033" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading red" />
        <character char_type="range_value" from="1" modifier="with" name="quantity" src="d0_s10" upper_restricted="false" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s10" value="red dots" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 8, in 2 unequal series, anthers versatile, pollen shed singly;</text>
      <biological_entity id="o5034" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="8" />
      </biological_entity>
      <biological_entity id="o5035" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" />
        <character is_modifier="true" name="size" src="d0_s11" value="unequal" />
      </biological_entity>
      <biological_entity id="o5036" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s11" value="versatile" />
      </biological_entity>
      <biological_entity id="o5037" name="pollen" name_original="pollen" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="singly" />
      </biological_entity>
      <relation from="o5034" id="r647" name="in" negation="false" src="d0_s11" to="o5035" />
    </statement>
    <statement id="d0_s12">
      <text>ovary 4-locular, without apical projection, style glabrous or pubescent distally, stigma entire, subcapitate to subglobose, surface unknown, probably wet and non-papillate.</text>
      <biological_entity id="o5038" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="4-locular" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5039" name="projection" name_original="projection" src="d0_s12" type="structure" />
      <biological_entity id="o5040" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s12" value="pubescent" />
      </biological_entity>
      <biological_entity id="o5041" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="subcapitate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="subglobose" />
        <character char_type="range_value" from="subcapitate" name="shape" src="d0_s12" to="subglobose" />
      </biological_entity>
      <biological_entity id="o5042" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="probably" name="condition" src="d0_s12" value="wet" />
        <character is_modifier="false" name="relief" src="d0_s12" value="non-papillate" />
      </biological_entity>
      <relation from="o5038" id="r648" name="without" negation="false" src="d0_s12" to="o5039" />
    </statement>
    <statement id="d0_s13">
      <text>Fruit a capsule, contorted or curled 1 to 5 times, or straight, narrowly cylindrical and thickened proximally, 4-angled (at least when dry), regularly but tardily loculicidally dehiscent, not swollen by seeds;</text>
      <biological_entity id="o5045" name="seed" name_original="seeds" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>sessile.</text>
      <biological_entity id="o5043" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="contorted" />
        <character is_modifier="false" name="shape" src="d0_s13" value="curled" />
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="1-5 times or straight narrowly cylindrical and thickened" />
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="1-5 times or straight narrowly cylindrical and thickened" />
        <character is_modifier="false" modifier="proximally" name="size_or_width" src="d0_s13" value="1-5 times or straight narrowly cylindrical and thickened" />
        <character is_modifier="false" name="shape" src="d0_s13" value="4-angled" />
        <character is_modifier="false" modifier="regularly; tardily loculicidally" name="dehiscence" src="d0_s13" value="dehiscent" />
        <character constraint="by seeds" constraintid="o5045" is_modifier="false" modifier="not" name="shape" src="d0_s13" value="swollen" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" />
      </biological_entity>
      <biological_entity id="o5044" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="contorted" />
        <character is_modifier="false" name="shape" src="d0_s13" value="curled" />
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="1-5 times or straight narrowly cylindrical and thickened" />
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="1-5 times or straight narrowly cylindrical and thickened" />
        <character is_modifier="false" modifier="proximally" name="size_or_width" src="d0_s13" value="1-5 times or straight narrowly cylindrical and thickened" />
        <character is_modifier="false" name="shape" src="d0_s13" value="4-angled" />
        <character is_modifier="false" modifier="regularly; tardily loculicidally" name="dehiscence" src="d0_s13" value="dehiscent" />
        <character constraint="by seeds" constraintid="o5045" is_modifier="false" modifier="not" name="shape" src="d0_s13" value="swollen" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds numerous, in 1 row per locule, flattened, narrowly obovoid, dull.</text>
      <biological_entity id="o5047" name="row" name_original="row" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" />
      </biological_entity>
      <biological_entity id="o5048" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <relation from="o5046" id="r649" name="in" negation="false" src="d0_s15" to="o5047" />
      <relation from="o5047" id="r650" name="per" negation="false" src="d0_s15" to="o5048" />
    </statement>
    <statement id="d0_s16">
      <text>= 7.</text>
      <biological_entity id="o5046" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="numerous" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="flattened" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="obovoid" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="dull" />
        <character name="quantity" src="d0_s16" value="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 14 (13 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Camissoniopsis proavita (P. H. Raven) W. L. Wagner &amp; Hoch is known from northern Baja California, Mexico.  It is a diploid, closely related to C. micrantha but differing in having numerous flowers in the basal rosette, which is densely leafy.</discussion>
  <discussion>All species of Camissoniopsis occur near coasts or on dry slopes or desert flats inland from 0–2500 m.  R. A. Levin et al. (2004) found strong molecular support for Camissoniopsis in a clade with Neoholmgrenia and Tetrapteron.  Camissoniopsis was segregated from Camissonia as delimited by P. H. Raven (1969).  Camissoniopsis is distinguished by having 4-angled fruits, at least when dry, and not swollen by seeds, dull seeds usually smaller than 1 mm, and by flowering from both basal and distal nodes (Raven).  Relationships within Camissoniopsis are complex and reticulate.  Several diploids (especially C. hirtella) appear to have contributed to the formation of the tetraploids and, in turn, the hexaploids (Raven), and, as a result, are very similar morphologically to each other.  Identification of the polyploid species of Camissoniopsis is aided by their pollen having a high proportion of grains with higher number of pores than typical Onagraceae 3-pored pollen, usually 4- or 5-pored.  This can be observed under low magnification (for example, 10\×) since the 3-pored pollen is triangular while the 4-pored is quadrangular and 5-pored is pentangular.  Raven proposed Camissonia sect. Holostigma as a new combination based on Spach’s generic name.  He was unaware that Holostigma Spach, like Agassizia Spach, is a later homonym and thus illegitimate; however, he satisfied all requirements for valid publication of a new sectional name in Camissonia.  Reproductive features include: self-incompatible (C. cheiranthifolia and C. bistorta) or self-compatible; flowers diurnal; outcrossing and pollinated by bees (E. G. Linsley et al. 1963, 1964, 1973) or autogamous (Raven).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs perennial; coastal habitats.</description>
      <determination>1. Camissoniopsis cheiranthifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs usually annual, rarely short-lived perennial (in C. bistorta); primarily inland habitats.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stigma exserted beyond anthers at anthesis; sepals (2.3–)5–8(–11) mm; petals (4.2–)7–15 mm.</description>
      <determination>2. Camissoniopsis bistorta</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stigma surrounded by all anthers, or at least those of longer filaments, at anthesis; sepals 1–6(–8.5) mm; petals 1.5–10.5(–13) mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules 2.8–3.5 mm diam. near base, straight or slightly curved outward, deeply grooved along lines of dehiscence.</description>
      <determination>4. Camissoniopsis guadalupensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules 0.7–2.2 mm diam. near base, straight or curved into 1+-coiled spirals, not deeply grooved.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pollen with 25–100% of grains 4- or 5-pored.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Inflorescences exclusively villous; 25–60% of pollen grains 4- or 5-pored.</description>
      <determination>12. Camissoniopsis luciae</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Inflorescences villous and glandular puberulent; 70–100 % of pollen grains 4- or 5-pored.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Capsules 1.3–1.6 mm diam., subterete in living material (obscurely 4-angled when dry); southernmost Monterey County to central San Luis Obispo County, California.</description>
      <determination>11. Camissoniopsis hardhamiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Capsules 1.5–2 mm diam., 4-angled in living material; San Diego County, California, adjacent Baja California, and offshore islands.</description>
      <determination>13. Camissoniopsis robusta</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pollen with less than 5% of grains 4-pored (rarely more in C. intermedia).</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Capsules 1.8–2.2 mm diam., conspicuously 4-angled in living material.</description>
      <determination>3. Camissoniopsis lewisii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Capsules 0.7–1.2(–1.8) mm diam., terete, subterete, or obscurely 4-angled, at least in living material.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Distal leaves petiolate, blade base attenuate; capsules usually much contorted, irregularly to 5-coiled; herbs moderately to sparsely strigillose, sometimes also sparsely villous.</description>
      <determination>5. Camissoniopsis ignota</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Distal leaves usually subsessile, blade base rounded, cuneate, or truncate; capsules straight to 1–2-coiled; herbs strigillose to villous.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Herbs conspicuously grayish in appearance, densely strigillose; lateral stems usually decumbent; plants of the deserts.</description>
      <determination>8. Camissoniopsis pallida</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Herbs not conspicuously gray in appearance, mostly villous; lateral stems erect to decumbent; plants not of deserts or only at desert margins (except C. confusa in central Arizona).</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Capsules 0.7–0.9 mm diam.; distal leaf blades elliptic-ovate or ovate; stems ascending to erect.</description>
      <determination>7. Camissoniopsis hirtella</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Capsules 0.9–1.2(–1.8) mm diam.; distal leaf blades narrowly lanceolate to narrowly ovate; stems decumbent or erect.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Stems decumbent; inflorescences usually densely villous, rarely also glandular puberulent.</description>
      <determination>6. Camissoniopsis micrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Stems erect; inflorescences usually moderately to densely villous, also glandular puberulent.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Floral tube (1.8–)2–3.8 mm; petals (2.5–)5–10.5 mm; styles (2.5–)4.5–7.5 mm; herbs densely villous, often also stigillose.</description>
      <determination>9. Camissoniopsis confusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Floral tube 1.2–2 mm; petals 1.5–3.5(–4.5) mm; styles 2–3.5 mm; herbs moderately villous.</description>
      <determination>10. Camissoniopsis intermedia</determination>
    </key_statement>
  </key>
</bio:treatment>