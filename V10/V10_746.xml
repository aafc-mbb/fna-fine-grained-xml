<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Leslie R. Landrum</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MYRTUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 471.  1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 212.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus myrtus</taxon_hierarchy>
    <other_info_on_name type="etymology">Classical name for a species of myrtle</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Myrtle</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, usually glabrous or glabrate, hairs simple, whitish.</text>
      <biological_entity id="o3960" name="shrub" name_original="shrubs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" />
      </biological_entity>
      <biological_entity id="o3961" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" />
      </biological_entity>
      <biological_entity id="o3962" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually opposite or whorled;</text>
      <biological_entity id="o3963" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s1" value="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade venation brochidodromous, obscure.</text>
      <biological_entity id="o3964" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="brochidodromous" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 1-flowered, axillary, flowers solitary.</text>
      <biological_entity id="o3965" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-flowered" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" />
      </biological_entity>
      <biological_entity id="o3966" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 5-merous, pedicellate;</text>
      <biological_entity id="o3967" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-merous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>hypanthium obconic;</text>
      <biological_entity id="o3968" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>calyx lobes persisting after anthesis, distinct, small tears sometimes forming between lobes;</text>
      <biological_entity constraint="calyx" id="o3969" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character constraint="after tears" constraintid="o3970" is_modifier="false" name="duration" src="d0_s6" value="persisting" />
      </biological_entity>
      <biological_entity id="o3970" name="tear" name_original="tears" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="anthesis" />
        <character is_modifier="true" name="fusion" src="d0_s6" value="distinct" />
        <character is_modifier="true" name="size" src="d0_s6" value="small" />
      </biological_entity>
      <biological_entity id="o3971" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
      <relation from="o3970" id="r658" name="forming between" negation="false" src="d0_s6" to="o3971" />
    </statement>
    <statement id="d0_s7">
      <text>petals whitish;</text>
      <biological_entity id="o3972" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 100–200;</text>
      <biological_entity id="o3973" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s8" to="200" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 2-locular or 3-locular, septum often incomplete centrally to apically;</text>
      <biological_entity id="o3974" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="2-locular" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="3-locular" />
      </biological_entity>
      <biological_entity id="o3975" name="septum" name_original="septum" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often; centrally to apically; apically" name="architecture" src="d0_s9" value="incomplete" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>placenta axile, not protruding, - to -shaped;</text>
      <biological_entity id="o3976" name="placenta" name_original="placenta" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="axile" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s10" value="protruding" />
        <character char_type="range_value" from="" name="shape" src="d0_s10" to="shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovules 22–34 per locule, 2-seriate.</text>
      <biological_entity id="o3977" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o3978" from="22" name="quantity" src="d0_s11" to="34" />
        <character is_modifier="false" name="architecture_or_arrangement" notes="" src="d0_s11" value="2-seriate" />
      </biological_entity>
      <biological_entity id="o3978" name="locule" name_original="locule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits berries, bluish purple (pulp whitish), subglobose.</text>
      <biological_entity constraint="fruits" id="o3979" name="berry" name_original="berries" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="bluish purple" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 8–20, somewhat flattened, -shaped to coiled;</text>
      <biological_entity id="o3980" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s13" to="20" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s13" value="flattened" />
        <character is_modifier="false" name="shape" src="d0_s13" value=",--shaped" value_original=",-shaped" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>outer rim of seed-coat hard and shiny, central portion often soft, external portion a few cells thick, not notably dense, easily broken;</text>
      <biological_entity constraint="outer; seed-coat" id="o3981" name="rim" name_original="rim" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="hard" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" />
      </biological_entity>
      <biological_entity id="o3982" name="seed-coat" name_original="seed-coat" src="d0_s14" type="structure" />
      <biological_entity constraint="central" id="o3983" name="portion" name_original="portion" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence_or_texture" src="d0_s14" value="soft" />
      </biological_entity>
      <biological_entity constraint="external" id="o3984" name="portion" name_original="portion" src="d0_s14" type="structure" />
      <relation from="o3981" id="r659" name="part_of" negation="false" src="d0_s14" to="o3982" />
    </statement>
    <statement id="d0_s15">
      <text>embryo -shaped, cylindrical;</text>
      <biological_entity id="o3985" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="few" />
        <character is_modifier="false" name="width" src="d0_s14" value="thick" />
        <character is_modifier="false" modifier="not notably" name="density" src="d0_s14" value="dense" />
        <character is_modifier="false" modifier="easily" name="condition_or_fragility" src="d0_s14" value="broken" />
        <character is_modifier="false" name="shape" src="d0_s15" value="embryo--shaped" value_original="embryo-shaped" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindrical" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>cotyledons linear, ca. 1/2 as long as embryo.</text>
      <biological_entity id="o3987" name="embryo" name_original="embryo" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>x = 11.</text>
      <biological_entity id="o3986" name="cotyledon" name_original="cotyledons" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" />
        <character constraint="as-long-as embryo" constraintid="o3987" name="quantity" src="d0_s16" value="1/2" />
      </biological_entity>
      <biological_entity constraint="x" id="o3988" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="11" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1 or 2 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; s Europe, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s Europe" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>