<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Myxocarpa</taxon_name>
    <taxon_name authority="Greene" date="1895" rank="species">virgata</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>3: 123.  1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section myxocarpa;species virgata</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Sierra clarkia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, to 100 cm, puberulent.</text>
      <biological_entity id="o6578" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 15–50 mm;</text>
      <biological_entity id="o6579" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o6580" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s1" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade elliptic to ovate, 2–5 cm.</text>
      <biological_entity id="o6581" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6582" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="ovate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences open racemes, axis recurved only at tip in bud, straight 4+ nodes distal to open flowers;</text>
      <biological_entity id="o6583" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o6584" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o6585" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character constraint="at tip" constraintid="o6586" is_modifier="false" name="orientation" src="d0_s3" value="recurved" />
        <character is_modifier="false" name="course" notes="" src="d0_s3" value="straight" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6586" name="tip" name_original="tip" src="d0_s3" type="structure" />
      <biological_entity id="o6587" name="bud" name_original="bud" src="d0_s3" type="structure" />
      <biological_entity id="o6588" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" />
      </biological_entity>
      <biological_entity id="o6589" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <relation from="o6583" id="r1172" name="open" negation="false" src="d0_s3" to="o6584" />
      <relation from="o6586" id="r1173" name="in" negation="false" src="d0_s3" to="o6587" />
      <relation from="o6588" id="r1174" name="open" negation="false" src="d0_s3" to="o6589" />
    </statement>
    <statement id="d0_s4">
      <text>buds pendent, narrowly obo­void, tip obtuse.</text>
      <biological_entity id="o6590" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="pendent" />
      </biological_entity>
      <biological_entity id="o6591" name="tip" name_original="tip" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: floral-tube 2–4 mm;</text>
      <biological_entity id="o6592" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6593" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed individually;</text>
      <biological_entity id="o6594" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6595" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="individually" name="orientation" src="d0_s6" value="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla rotate, petals lavender-purple, mottled or spotted with reddish purple, ± rhombic, unlobed, 7–14 × 3–7 mm, length 1.9–3 times width;</text>
      <biological_entity id="o6596" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6597" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rotate" />
      </biological_entity>
      <biological_entity id="o6598" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender-purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="mottled" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="spotted with reddish purple" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="unlobed" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s7" value="1.9-3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 8, subequal, subtended by ciliate scales, pollen blue-gray;</text>
      <biological_entity id="o6599" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6600" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" />
      </biological_entity>
      <biological_entity id="o6601" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" />
      </biological_entity>
      <biological_entity id="o6602" name="pollen" name_original="pollen" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue-gray" />
      </biological_entity>
      <relation from="o6600" id="r1175" name="subtended by" negation="false" src="d0_s8" to="o6601" />
    </statement>
    <statement id="d0_s9">
      <text>ovary shallowly 4-grooved;</text>
      <biological_entity id="o6603" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6604" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture" src="d0_s9" value="4-grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma not or rarely exserted beyond anthers.</text>
      <biological_entity id="o6605" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6606" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o6607" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o6606" id="r1176" modifier="rarely" name="exserted beyond" negation="false" src="d0_s10" to="o6607" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules 10–20 mm;</text>
      <biological_entity id="o6608" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicel 1–4 mm.</text>
      <biological_entity id="o6609" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds brown or gray, 1–1.5 mm, scaly-echinate, crest 0.1 mm. 2n = 10.</text>
      <biological_entity id="o6610" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="gray" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="scaly-echinate" />
      </biological_entity>
      <biological_entity id="o6611" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6612" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Clarkia virgata is known primarily from El Dorado to Tuolumne counties in the north-central Sierra Nevada range, with scattered collections to Mariposa and Yuba counties.</discussion>
  <discussion>Clarkia virgata is very similar to C. mosquinii and C. australis and is probably derived from the former through chromosome reduction in number and rearrangement and may be ancestral to the latter, which differs in chromosome arrangement.  Experimental hybrids in all combinations have very low fertility.  The three species are difficult to distinguish morphologically but replace one another ecogeographically with C. australis in the south and C. virgata in the middle with non-overlapping distributions.  Other than geographical distribution, C. virgata is usually distinguishable from C. mosquinii by having narrower petal blades and from C. australis by having broader leaves.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Yellow-pine forests, foothill woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="yellow-pine forests" />
        <character name="habitat" value="foothill woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>