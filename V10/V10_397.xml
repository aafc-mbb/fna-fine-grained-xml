<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Biortis</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1953" rank="species">affinis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>12: 34.  1953</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section biortis;species affinis</taxon_hierarchy>
  </taxon_identification>
  <number>23.</number>
  <other_name type="common_name">Chaparral clarkia or fairyfan</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, to 80 cm, puber­ulent.</text>
      <biological_entity id="o6962" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 0–3 mm;</text>
      <biological_entity id="o6963" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o6964" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear to narrowly lanceo­late, 1.5–7 cm.</text>
      <biological_entity id="o6965" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflo­rescences dense spikes, axis straight;</text>
      <biological_entity id="o6966" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" />
        <character char_type="range_value" from="1.5" from_unit="cm" modifier="narrowly" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6967" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="true" name="density" src="d0_s3" value="dense" />
      </biological_entity>
      <biological_entity id="o6968" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>buds erect.</text>
      <biological_entity id="o6969" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: floral tube1.5–4 mm;</text>
      <biological_entity id="o6970" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_position" src="d0_s5" value="floral" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed together to 1 side;</text>
      <biological_entity id="o6971" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6972" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="together" name="orientation" src="d0_s6" value="reflexed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="1" />
      </biological_entity>
      <biological_entity id="o6973" name="side" name_original="side" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>corolla bowl-shaped, petals 5–15 mm;</text>
      <biological_entity id="o6974" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6975" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="bowl--shaped" value_original="bowl-shaped" />
      </biological_entity>
      <biological_entity id="o6976" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 8, subequal;</text>
      <biological_entity id="o6977" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6978" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary cylindrical, 8-grooved, length at least 10 times width;</text>
      <biological_entity id="o6979" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6980" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindrical" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="8-grooved" />
        <character is_modifier="false" modifier="at-least" name="l_w_ratio" src="d0_s9" value="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma not exserted beyond anthers.</text>
      <biological_entity id="o6981" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6982" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o6983" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o6982" id="r1218" name="exserted beyond" negation="true" src="d0_s10" to="o6983" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules 15–30 mm, beak 3–7 mm;</text>
      <biological_entity id="o6984" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6985" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicel 0–5 mm.</text>
      <biological_entity id="o6986" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds brown or gray, 1–1.5 mm, scaly, crest 0.1 mm. 2n = 52.</text>
      <biological_entity id="o6987" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="gray" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s13" value="scaly" />
      </biological_entity>
      <biological_entity id="o6988" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6989" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="52" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Clarkia affinis is known primarily from west-central California and the North Coast Ranges, and more scattered in the Sierra Nevada Foothills and Western Transverse Ranges.</discussion>
  <discussion>Clarkia affinis is a hexaploid most closely related to C. purpurea; both have 2n = 52.  Chromosome pairing in hybrids between them, as well as morphology, suggest that they have a tetraploid (2n = 34) genome in common.  The two species are most readily distinguished by their immature capsules, which in C. affinis are slender, at least ten times longer than wide, beaked, and shallowly grooved, whereas those of C. purpurea are stout, not more than eight times longer than wide, not prominently beaked, and deeply grooved; the sepals of the former are generally reflexed together in fours whereas those of the latter are reflexed individually or in twos.  Based on morphology and molecular data, the diploid genome probably came from C. cylindrica or a related species.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in woodlands and chaparral.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in woodlands and chaparral" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>