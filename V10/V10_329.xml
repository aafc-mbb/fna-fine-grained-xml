<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Endlicher" date="1830" rank="tribe">Epilobieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EPILOBIUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Epilobium</taxon_name>
    <taxon_name authority="Trelease" date="1906" rank="species">mirabile</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>11: 404.  1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section epilobium;species mirabile</taxon_hierarchy>
  </taxon_identification>
  <number>35.</number>
  <other_name type="common_name">Olympic Mountain willowherb</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with sessile, compact, fleshy turions that leave dark basal scales.</text>
      <biological_entity id="o5326" name="herb" name_original="herbs" src="d0_s0" type="structure" />
      <biological_entity id="o5327" name="turion" name_original="turions" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="sessile" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="compact" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fleshy" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5328" name="scale" name_original="scales" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="dark" />
      </biological_entity>
      <relation from="o5326" id="r963" name="with" negation="false" src="d0_s0" to="o5327" />
      <relation from="o5327" id="r964" name="leave" negation="false" src="d0_s0" to="o5328" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, loosely or not clumped, terete, 7–30 cm, usually simple, rarely branched, subglabrous with raised strig­illose lines decurrent from margins of petioles, or densely strigillose and without raised lines.</text>
      <biological_entity id="o5329" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="loosely; not" name="arrangement" src="d0_s1" value="clumped" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="branched" />
        <character constraint="with lines" constraintid="o5330" is_modifier="false" name="pubescence" src="d0_s1" value="subglabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s1" value="strigillose" />
      </biological_entity>
      <biological_entity id="o5330" name="line" name_original="lines" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="raised" />
        <character constraint="from margins" constraintid="o5331" is_modifier="false" name="shape" src="d0_s1" value="decurrent" />
      </biological_entity>
      <biological_entity id="o5331" name="margin" name_original="margins" src="d0_s1" type="structure" />
      <biological_entity id="o5332" name="petiole" name_original="petioles" src="d0_s1" type="structure" />
      <biological_entity id="o5333" name="line" name_original="lines" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="raised" />
      </biological_entity>
      <relation from="o5331" id="r965" name="part_of" negation="false" src="d0_s1" to="o5332" />
      <relation from="o5329" id="r966" name="without" negation="false" src="d0_s1" to="o5333" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite proximal to inflorescence, alter­nate distally, petioles 1–3 mm proximally, subsessile distally;</text>
      <biological_entity id="o5334" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character constraint="to inflorescence" constraintid="o5335" is_modifier="false" name="position" src="d0_s2" value="proximal" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="proximally" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="subsessile" />
      </biological_entity>
      <biological_entity id="o5335" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
      <biological_entity id="o5336" name="petiole" name_original="petioles" src="d0_s2" type="structure" />
      <relation from="o5334" id="r967" name="alter ­ nate" negation="false" src="d0_s2" to="o5336" />
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to narrowly ovate, coriaceous, 1.5–3 × 0.7–1.2 cm, base rounded to cuneate, margins denticulate, 8–12 teeth per side, veins indistinct, 4–9 per side, apex obtuse proximally to acute distally, surfaces sparsely strigillose, mainly on margins and midrib;</text>
      <biological_entity id="o5337" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="ovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="ovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="narrowly ovate" />
        <character is_modifier="false" name="texture" src="d0_s3" value="coriaceous" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s3" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5338" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="rounded" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="cuneate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="cuneate" />
      </biological_entity>
      <biological_entity id="o5339" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="denticulate" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s3" to="12" />
      </biological_entity>
      <biological_entity id="o5340" name="tooth" name_original="teeth" src="d0_s3" type="structure" />
      <biological_entity id="o5341" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o5342" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="indistinct" />
        <character char_type="range_value" constraint="per side" constraintid="o5343" from="4" name="quantity" src="d0_s3" to="9" />
      </biological_entity>
      <biological_entity id="o5343" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o5344" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s3" value="obtuse" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="acute" />
      </biological_entity>
      <biological_entity id="o5345" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="strigillose" />
      </biological_entity>
      <biological_entity id="o5346" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o5347" name="midrib" name_original="midrib" src="d0_s3" type="structure" />
      <relation from="o5340" id="r968" name="per" negation="false" src="d0_s3" to="o5341" />
      <relation from="o5345" id="r969" modifier="mainly" name="on" negation="false" src="d0_s3" to="o5346" />
      <relation from="o5345" id="r970" modifier="mainly" name="on" negation="false" src="d0_s3" to="o5347" />
    </statement>
    <statement id="d0_s4">
      <text>bracts not much reduced.</text>
      <biological_entity id="o5348" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not much" name="size" src="d0_s4" value="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences erect racemes, rarely branched, densely strigillose.</text>
      <biological_entity id="o5349" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="rarely" name="architecture" notes="" src="d0_s5" value="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="strigillose" />
      </biological_entity>
      <biological_entity id="o5350" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o5351" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>buds 3–4 × 1.5–2.2 mm;</text>
      <biological_entity id="o5352" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 4–5 mm;</text>
      <biological_entity id="o5353" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>floral-tube 1.5–2 × 1.6–2.2 mm, sparsely glandular puberulent, sometimes mixed strigillose;</text>
      <biological_entity id="o5354" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s9" to="2.2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s9" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals often purplish red, 2–3.2 ×1.5–2.4 mm;</text>
      <biological_entity id="o5355" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish red" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white, often red-tinged at apex, 3.8–5 × 2–3 mm, apical notch 0.4–0.8 mm;</text>
      <biological_entity id="o5356" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" />
        <character constraint="at apex" constraintid="o5357" is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="red-tinged" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" notes="" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5357" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity constraint="apical" id="o5358" name="notch" name_original="notch" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments cream, those of longer stamens 1.4–2.3 mm, those of shorter ones 0.8–1.4 mm;</text>
      <biological_entity id="o5359" name="filament" name_original="filaments" src="d0_s12" type="structure" constraint="stamen">
        <character is_modifier="false" name="coloration" src="d0_s12" value="cream" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o5360" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity constraint="shorter" id="o5361" name="one" name_original="ones" src="d0_s12" type="structure" />
      <relation from="o5359" id="r971" name="part_of" negation="false" src="d0_s12" to="o5360" />
      <relation from="o5359" id="r972" name="part_of" negation="false" src="d0_s12" to="o5361" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.4–0.6 × 0.3–0.5 mm;</text>
      <biological_entity id="o5362" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s13" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 10–18 mm, densely strigillose and glan­dular puberulent;</text>
      <biological_entity id="o5363" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5364" name="whole_organism" name_original="" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style yellow or light pink, 2–2.3 mm, stigma broadly clavate, 0.8–1 × 0.6–0.7 mm, surrounded by longer anthers.</text>
      <biological_entity id="o5365" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="light pink" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5366" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="clavate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s15" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s15" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o5367" name="anther" name_original="anthers" src="d0_s15" type="structure" />
      <relation from="o5366" id="r973" name="surrounded by" negation="false" src="d0_s15" to="o5367" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules 30–45 mm, relatively thick (2–3 mm), surfaces ± sparsely glandular puberulent and mixed strigillose;</text>
      <biological_entity id="o5368" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s16" to="45" to_unit="mm" />
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s16" value="thick" />
      </biological_entity>
      <biological_entity id="o5369" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less sparsely" name="architecture_or_function_or_pubescence" src="d0_s16" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="puberulent" />
        <character is_modifier="false" name="arrangement" src="d0_s16" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 5–16 mm.</text>
      <biological_entity id="o5370" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds narrowly obovoid, 1.7–2.2 × 0.6–0.8 mm, chalazal collar inconspicuous, gray to light-brown, surface low papil­lose or reticulate;</text>
      <biological_entity id="o5371" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="obovoid" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s18" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s18" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="chalazal" id="o5372" name="collar" name_original="collar" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="inconspicuous" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s18" value="gray" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s18" value="light-brown" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s18" to="light-brown" />
      </biological_entity>
      <biological_entity id="o5373" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="low" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s18" value="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>coma readily detached, white, very full, 10–15 mm. 2n = 36.</text>
      <biological_entity id="o5374" name="coma" name_original="coma" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="readily" name="fusion" src="d0_s19" value="detached" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="white" />
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s19" value="full" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s19" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5375" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Epilobium mirabile also has the CC chromosomal arrangement and is one of the least common species of Epilobium in North America; fewer than 20 collections are known, even though its range is quite large.  Most collections are from the Olympic Peninsula in Washington or Waterton-Glacier International Peace Park in Alberta and adjacent Montana.  However, one collection is known from Powell County in central Montana, and one from Manning Provincial Park in British Columbia.  The species may be more widespread but under-collected due to its restricted habitat, mainly on subalpine south-facing scree slopes.</discussion>
  <discussion>Specimens of Epilobium mirabile from the northern Rocky Mountains (Alberta and Montana) have subglabrous stems with strong, raised, strigillose lines and seeds with low papillose surfaces, whereas specimens from the northern Cascades (British Columbia) and Olympic Mountains (Washington) have densely strigillose stems with no raised lines and seeds with reticulate surfaces.  The plants otherwise have very similar and distinctive morphology and ecology.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine scree slopes, gravelly tussock meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine scree slopes" />
        <character name="habitat" value="meadows" modifier="gravelly tussock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Mont., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>