<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Oenothera</taxon_name>
    <taxon_name authority="(Rose ex Britton &amp; A. Brown) W. Dietrich" date="1978" rank="subsection">Raimannia</taxon_name>
    <taxon_name authority="Hill" date="1767" rank="species">laciniata</taxon_name>
    <place_of_publication>
      <publication_title>Veg. Syst.</publication_title>
      <place_in_publication>12(app.): 64, plate 10.  1767</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section oenothera;subsection raimannia;species laciniata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">minima</taxon_name>
    <taxon_hierarchy>genus oenothera;species minima</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="Medikus" date="unknown" rank="species">repanda</taxon_name>
    <taxon_hierarchy>genus o.;species repanda</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">sinuata</taxon_name>
    <taxon_hierarchy>genus o.;species sinuata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sinuata</taxon_name>
    <taxon_name authority="(Pursh) Nuttall" date="unknown" rank="variety">minima</taxon_name>
    <taxon_hierarchy>genus o.;species sinuata;variety minima</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Onagra</taxon_name>
    <taxon_name authority="(Linnaeus) Moench" date="unknown" rank="species">sinuata</taxon_name>
    <taxon_hierarchy>genus onagra;species sinuata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Raimannia</taxon_name>
    <taxon_name authority="(Hill) Rose ex Britton &amp; A. Brown" date="unknown" rank="species">laciniata</taxon_name>
    <taxon_hierarchy>genus raimannia;species laciniata</taxon_hierarchy>
  </taxon_identification>
  <number>89.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, sparsely to moderately strigillose, some­times also villous, sometimes also becoming glandular puberulent distally.</text>
      <biological_entity id="o2213" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s0" value="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" />
        <character is_modifier="false" modifier="sometimes; becoming" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, un­branched to much branched, 5–50 cm.</text>
      <biological_entity id="o2214" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="erect" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="ascending" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched to much" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in a basal rosette and cauline, basal 4–15 × 1–3 cm, cauline 2–10 × 0.5–3.5 cm;</text>
      <biological_entity id="o2215" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s2" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2216" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o2217" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o2218" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <relation from="o2215" id="r503" name="in" negation="false" src="d0_s2" to="o2216" />
    </statement>
    <statement id="d0_s3">
      <text>blade green, narrowly oblanceolate to narrowly elliptic or narrowly oblong, margins usually dentate or deeply lobed;</text>
      <biological_entity id="o2219" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblong" />
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s3" to="narrowly elliptic or narrowly oblong" />
      </biological_entity>
      <biological_entity id="o2220" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="dentate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s3" value="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts spreading, flat.</text>
      <biological_entity id="o2221" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers usually 1 opening per day near sunset;</text>
      <biological_entity id="o2222" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" />
      </biological_entity>
      <biological_entity id="o2223" name="day" name_original="day" src="d0_s5" type="structure" />
      <biological_entity id="o2224" name="sunset" name_original="sunset" src="d0_s5" type="structure" />
      <relation from="o2222" id="r504" name="opening per" negation="false" src="d0_s5" to="o2223" />
      <relation from="o2222" id="r505" name="near" negation="false" src="d0_s5" to="o2224" />
    </statement>
    <statement id="d0_s6">
      <text>buds erect, with free tips erect, 0.3–3 mm;</text>
      <biological_entity id="o2225" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2226" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="free" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
      </biological_entity>
      <relation from="o2225" id="r506" name="with" negation="false" src="d0_s6" to="o2226" />
    </statement>
    <statement id="d0_s7">
      <text>floral-tube 12–35 mm;</text>
      <biological_entity id="o2227" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 5–15 mm;</text>
      <biological_entity id="o2228" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals yellow, fading orange or reddish tinged, broadly obovate or obcordate, 5–22 mm;</text>
      <biological_entity id="o2229" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="fading orange" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish tinged" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obcordate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 3–14 mm, anthers 4–5 mm, pollen ca. 50% fertile;</text>
      <biological_entity id="o2230" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2231" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2232" name="pollen" name_original="pollen" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="50%" name="reproduction" src="d0_s10" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 20–50 mm, stigma surrounded by anthers at anthesis.</text>
      <biological_entity id="o2233" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s11" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2234" name="stigma" name_original="stigma" src="d0_s11" type="structure" />
      <biological_entity id="o2235" name="anther" name_original="anthers" src="d0_s11" type="structure" />
      <relation from="o2234" id="r507" name="surrounded by" negation="false" src="d0_s11" to="o2235" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules cylindrical, sometimes slightly enlarged toward apex, 20–50 × 2–4 mm.</text>
      <biological_entity id="o2236" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindrical" />
        <character constraint="toward apex" constraintid="o2237" is_modifier="false" modifier="sometimes slightly" name="size" src="d0_s12" value="enlarged" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" notes="" src="d0_s12" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2237" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds ellipsoid to subglobose, 0.9–1.8 × 0.4–0.9 mm. 2n = 14.</text>
      <biological_entity id="o2238" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="subglobose" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s13" to="subglobose" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s13" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s13" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2239" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera laciniata is a PTH species and forms aring of 14 chromosomes in meiosis, and is self-compatible and autogamous (W. Dietrich and W. L. Wagner 1988).</discussion>
  <discussion>Oenothera laciniata is known in New Mexico from Doña Ana and Roosevelt counties from non-montane habitats and thus do not appear to represent O. pubescens; however, a few collections from Brewster and Jeff Davis counties, Texas, reported by W. Dietrich and W. L. Wagner (1988) as O. laciniata appear to represent collections of O. pubescens.  Dietrich and Wagner found that O. laciniata hybridizes not only with O. grandis, but also with O. drummondii subsp. drummondii, O. humifusa, and O. mexicana.  It is naturalized nearly worldwide in temperate and subtropical areas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Apr–Sep(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>introduced nearly worldwide in temperate and subtropical areas..</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="temperate" modifier="introduced nearly worldwide in" />
        <character name="habitat" value="subtropical areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000(–1300) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_1">
      <text>introduced nearly worldwide in temperate and subtropical areas..</text>
      <biological_entity id="hab_o1" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="temperate" modifier="introduced nearly worldwide in" />
        <character name="habitat" value="subtropical areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Calif., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis., Wyo.; introduced nearly worldwide in temperate and subtropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="nearly worldwide in temperate and subtropical areas" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>