<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(P. H. Raven) W. L. Wagner &amp; Hoch" date="2007" rank="genus">EREMOTHERA</taxon_name>
    <taxon_name authority="(A. Gray) W. L. Wagner &amp; Hoch" date="2007" rank="species">chamaenerioides</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 209.  2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus eremothera;species chamaenerioides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Gray" date="1853" rank="species">chamaenerioidesA.</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 58.  1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species chamaenerioidesa.</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(A. Gray) P. H. Raven" date="unknown" rank="species">chamaenerioides</taxon_name>
    <taxon_hierarchy>genus camissonia;species chamaenerioides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="(Davidson) J. F. Macbride" date="unknown" rank="species">erythra</taxon_name>
    <taxon_hierarchy>genus o.;species erythra</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphaerostigma</taxon_name>
    <taxon_name authority="(A. Gray) Small" date="unknown" rank="species">chamaenerioides</taxon_name>
    <taxon_hierarchy>genus sphaerostigma;species chamaenerioides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="Davidson" date="unknown" rank="species">erythrum</taxon_name>
    <taxon_hierarchy>genus s.;species erythrum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs glandular puberulent and sparsely strigillose distally, especially in inflorescence.</text>
      <biological_entity id="o4753" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s0" value="strigillose" />
      </biological_entity>
      <biological_entity id="o4754" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure" />
      <relation from="o4753" id="r611" modifier="especially" name="in" negation="false" src="d0_s0" to="o4754" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually well branched from base, 8–50 cm, flowering only distally.</text>
      <biological_entity id="o4755" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="from base" constraintid="o4756" is_modifier="false" modifier="usually well" name="architecture" src="d0_s1" value="branched" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="50" to_unit="cm" />
        <character is_modifier="false" modifier="only distally; distally" name="life_cycle" src="d0_s1" value="flowering" />
      </biological_entity>
      <biological_entity id="o4756" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, with lower ones clustered near base, (0.7–) 2–8 (–10) × 0.1–2.5 cm;</text>
      <biological_entity id="o4757" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" />
      </biological_entity>
      <biological_entity constraint="lower" id="o4758" name="one" name_original="ones" src="d0_s2" type="structure">
        <character constraint="near base" constraintid="o4759" is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" />
      </biological_entity>
      <biological_entity id="o4759" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_length" notes="alterIDs:o4759" src="d0_s2" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" notes="alterIDs:o4759" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" notes="alterIDs:o4759" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" notes="alterIDs:o4759" src="d0_s2" to="2.5" to_unit="cm" />
      </biological_entity>
      <relation from="o4757" id="r612" name="with" negation="false" src="d0_s2" to="o4758" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.1–3.5 cm;</text>
      <biological_entity id="o4760" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s3" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade very narrowly elliptic to narrowly elliptic, margins entire or sparsely denticulate.</text>
      <biological_entity id="o4761" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="very narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic to narrowly" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" />
      </biological_entity>
      <biological_entity id="o4762" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" />
        <character is_modifier="false" modifier="sparsely" name="shape" src="d0_s4" value="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences nodding.</text>
      <biological_entity id="o4763" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers opening at sunset;</text>
      <biological_entity id="o4764" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4765" name="sunset" name_original="sunset" src="d0_s6" type="structure" />
      <relation from="o4764" id="r613" name="opening at" negation="false" src="d0_s6" to="o4765" />
    </statement>
    <statement id="d0_s7">
      <text>floral-tube 1.5–3 mm, villous in proximal 1/2 inside;</text>
      <biological_entity id="o4766" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character constraint="in inside" constraintid="o4767" is_modifier="false" name="pubescence" src="d0_s7" value="villous" />
      </biological_entity>
      <biological_entity id="o4767" name="inside" name_original="inside" src="d0_s7" type="structure">
        <character is_modifier="true" name="position" src="d0_s7" value="proximal" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 1.5–2.5 mm;</text>
      <biological_entity id="o4768" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white, fading pinkish, 1.8–3 mm;</text>
      <biological_entity id="o4769" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="fading pinkish" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>episepalous filaments 0.7–1.5 mm, epipetalous filaments slightly shorter, anthers 0.5–1.1 mm;</text>
      <biological_entity constraint="episepalous" id="o4770" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="epipetalous" id="o4771" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="height_or_length_or_size" src="d0_s10" value="shorter" />
      </biological_entity>
      <biological_entity id="o4772" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 2.3–4.5 mm, villous prox­imally, stigma 0.7–1 mm diam., surrounded by anthers at anthesis.</text>
      <biological_entity id="o4773" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="villous" />
      </biological_entity>
      <biological_entity id="o4774" name="stigma" name_original="stigma" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" modifier="imally" name="diameter" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4775" name="anther" name_original="anthers" src="d0_s11" type="structure" />
      <relation from="o4774" id="r614" name="surrounded by" negation="false" src="d0_s11" to="o4775" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules narrowly cylindrical throughout, spreading, straight, terete, 35–60 × 0.7–1 mm, regu­larly but tardily dehiscent.</text>
      <biological_entity id="o4776" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly; throughout" name="shape" src="d0_s12" value="cylindrical" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s12" to="60" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="larly; tardily" name="dehiscence" src="d0_s12" value="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds monomorphic, gray, 0.9–1 × 0.3–0.4 mm, finely reticulate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o4777" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="gray" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s13" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s13" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="architecture_or_coloration_or_relief" src="d0_s13" value="reticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4778" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eremothera chamaenerioides occurs in sub-Mogollon Arizona, southeastern California, southern Nevada, southern New Mexico, trans-Pecos Texas, and Kane, Millard, Tooele, and Washington counties, Utah.  P. H. Raven (1969) determined Eremothera chamaenerioides to be self-compatible and autogamous.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jan–)Feb–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Feb" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy desert slopes and flats.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy desert" />
        <character name="habitat" value="flats" modifier="slopes and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-50–1700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="50" from_unit="m" constraint="- " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., N.Mex., Tex., Utah; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>