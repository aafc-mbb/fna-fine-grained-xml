<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">CHYLISMIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Chylismia</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) A. Heller" date="1906" rank="species">claviformis</taxon_name>
    <taxon_name authority="(Munz) W. L. Wagner &amp; Hoch" date="2007" rank="subspecies">peeblesii</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 206. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus chylismia;section chylismia;species claviformis;subspecies peeblesii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont" date="unknown" rank="species">claviformis</taxon_name>
    <taxon_name authority="Munz" date="1939" rank="variety">peeblesii</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>2: 158.  1939</place_in_publication>
      <other_info_on_pub>(as clavaeformis)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species claviformis;variety peeblesii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) P. H. Raven" date="unknown" rank="species">claviformis</taxon_name>
    <taxon_name authority="(Munz) P. H. Raven" date="unknown" rank="subspecies">peeblesii</taxon_name>
    <taxon_hierarchy>genus camissonia;species claviformis;subspecies peeblesii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">claviformis</taxon_name>
    <taxon_name authority="(Munz) P. H. Raven" date="unknown" rank="subspecies">peeblesii</taxon_name>
    <taxon_hierarchy>genus o.;species claviformis;subspecies peeblesii</taxon_hierarchy>
  </taxon_identification>
  <number>6g.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs glandular puberulent and strigillose.</text>
      <biological_entity id="o1013" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 5–60 cm.</text>
      <biological_entity id="o1014" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade lateral lobes irregular, well developed, terminal lobe narrowly ovate, to 7 × 3 cm, margins irregularly sinuate-dentate.</text>
      <biological_entity id="o1015" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1016" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity constraint="lateral" id="o1017" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_course" src="d0_s2" value="irregular" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s2" value="developed" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o1018" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="ovate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1019" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s2" value="sinuate-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers opening at sunset;</text>
      <biological_entity id="o1020" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o1021" name="sunset" name_original="sunset" src="d0_s3" type="structure" />
      <relation from="o1020" id="r215" name="opening at" negation="false" src="d0_s3" to="o1021" />
    </statement>
    <statement id="d0_s4">
      <text>buds without free tips;</text>
      <biological_entity id="o1022" name="bud" name_original="buds" src="d0_s4" type="structure" />
      <biological_entity id="o1023" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" />
      </biological_entity>
      <relation from="o1022" id="r216" name="without" negation="false" src="d0_s4" to="o1023" />
    </statement>
    <statement id="d0_s5">
      <text>floral-tube orangebrown inside, 3–5.5 mm;</text>
      <biological_entity id="o1024" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="orangebrown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white, often fading purple, 3–7.5 mm. 2n = 14.</text>
      <biological_entity id="o1025" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="fading purple" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1026" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies peeblesii is known throughout almost all the southwestern half of Arizona and locally in northwesternmost Sonora, and was recently collected in Grant and Hildago counties in New Mexico.  It intergrades with subspp. aurantiaca and rubescens, and hybridizes with all subspecies of Chylismia brevipes.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Dec–)Jan–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Jan" />
        <character name="flowering time" char_type="atypical_range" to="Apr" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Flat, sandy plains, washes, with Ambrosia dumosa, Carnegiea gigantea, Larrea tridentata, and Prosopis.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="flat" />
        <character name="habitat" value="sandy plains" />
        <character name="habitat" value="washes" constraint="with ambrosia dumosa , carnegiea gigantea , larrea tridentata , and prosopis" />
        <character name="habitat" value="ambrosia dumosa" />
        <character name="habitat" value="carnegiea gigantea" />
        <character name="habitat" value="larrea tridentata" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>