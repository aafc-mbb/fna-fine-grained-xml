<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="L’Heritier" date="1792" rank="genus">EUCALYPTUS</taxon_name>
    <taxon_name authority="Labillardière" date="1800" rank="species">globulus</taxon_name>
    <place_of_publication>
      <publication_title>Voy. Rech. Pérouse</publication_title>
      <place_in_publication>1: 153, plate 13.  1800</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus eucalyptus;species globulus</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Blue gum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 60 m;</text>
      <biological_entity id="o2952" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="60" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk bluish gray, straight, smooth;</text>
      <biological_entity id="o2953" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="bluish gray" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark shed in irregular strips distally, sometimes persistent toward trunk base;</text>
      <biological_entity id="o2954" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character constraint="toward trunk base" constraintid="o2956" is_modifier="false" modifier="sometimes" name="duration" notes="" src="d0_s2" value="persistent" />
      </biological_entity>
      <biological_entity id="o2955" name="strip" name_original="strips" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s2" value="irregular" />
      </biological_entity>
      <biological_entity constraint="trunk" id="o2956" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o2954" id="r556" name="shed in" negation="false" src="d0_s2" to="o2955" />
    </statement>
    <statement id="d0_s3">
      <text>twigs ± square or winged.</text>
      <biological_entity id="o2957" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="square" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly strongly aromatic;</text>
      <biological_entity id="o2958" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly strongly" name="odor" src="d0_s4" value="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1.5–2.5 cm, flattened;</text>
      <biological_entity id="o2959" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade green, usually narrowly lanceolate, often sickle-shaped, 10–30 × 2.5–4 cm.</text>
      <biological_entity id="o2960" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" />
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s6" value="lanceolate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s6" value="sickle--shaped" value_original="sickle-shaped" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 0.1–1 cm.</text>
      <biological_entity id="o2961" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s7" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences: flowers solitary, sessile or subsessile.</text>
      <biological_entity id="o2962" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o2963" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium obconic, ± 4-ribbed, to 20 mm, glaucous;</text>
      <biological_entity id="o2964" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2965" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obconic" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s9" value="4-ribbed" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calyptra flattened-hemispheric, with central knob, warty, glaucous;</text>
      <biological_entity id="o2966" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2967" name="calyptra" name_original="calyptra" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="flattened-hemispheric" />
        <character is_modifier="false" name="pubescence_or_relief" notes="" src="d0_s10" value="warty" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glaucous" />
      </biological_entity>
      <biological_entity constraint="central" id="o2968" name="knob" name_original="knob" src="d0_s10" type="structure" />
      <relation from="o2967" id="r557" name="with" negation="false" src="d0_s10" to="o2968" />
    </statement>
    <statement id="d0_s11">
      <text>stamens creamy white.</text>
      <biological_entity id="o2969" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2970" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="creamy white" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules hemispheric or obconic, ± 4-ribbed, 5–21 mm, glaucous, thickened, warty, rim wide;</text>
      <biological_entity id="o2971" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obconic" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s12" value="4-ribbed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="21" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glaucous" />
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="thickened" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="warty" />
      </biological_entity>
      <biological_entity id="o2972" name="rim" name_original="rim" src="d0_s12" type="structure">
        <character is_modifier="false" name="width" src="d0_s12" value="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves 3–5, ± level with apex or exserted.</text>
      <biological_entity id="o2974" name="apex" name_original="apex" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 22.</text>
      <biological_entity id="o2973" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="5" />
        <character constraint="with apex" constraintid="o2974" is_modifier="false" modifier="more or less" name="position_relational" src="d0_s13" value="level" />
        <character is_modifier="false" name="position" src="d0_s13" value="exserted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2975" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eucalyptus globulus is known from the Outer North Coast Ranges, Great Central Valley, and central-western and southwestern California.</discussion>
  <discussion>Eucalyptus globulus is commonly cultivated in warm regions of the world for its fast-growing timber and for paper pulp.  The species is the tallest angiosperm in North America, easily recognized by the large, solitary flowers and fruit.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall–winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="winter" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; se Australia</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="se Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>