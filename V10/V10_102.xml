<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007" rank="section">Gaura</taxon_name>
    <taxon_name authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007" rank="subsection">Gaura</taxon_name>
    <taxon_name authority="(Rydberg) W. L. Wagner &amp; Hoch" date="2007" rank="species">coloradensis</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 211.  2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section gaura;subsection gaura;species coloradensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaura</taxon_name>
    <taxon_name authority="Rydberg" date="1904" rank="species">coloradensis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>31: 572.  1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gaura;species coloradensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="Wooton" date="unknown" rank="species">neomexicana</taxon_name>
    <taxon_name authority="(Rydberg) P. H. Raven &amp; D. P. Gregory" date="unknown" rank="subspecies">coloradensis</taxon_name>
    <taxon_hierarchy>genus g.;species neomexicana;subspecies coloradensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">neomexicana</taxon_name>
    <taxon_name authority="(Rydberg) Munz" date="unknown" rank="variety">coloradensis</taxon_name>
    <taxon_hierarchy>genus g.;species neomexicana;variety coloradensis</taxon_hierarchy>
  </taxon_identification>
  <number>50.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs monocarpic perennial, strigillose proximally, short-hirtellous and strigillose distally, leaves sometimes glabrate;</text>
      <biological_entity id="o150" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monocarpic" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="short-hirtellous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>from stout, fleshy taproot.</text>
      <biological_entity id="o151" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–few-branched from base, 50–80 (–100) cm.</text>
      <biological_entity id="o152" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o153" is_modifier="false" name="architecture" src="d0_s2" value="1-few-branched" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="100" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="80" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o153" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, basal 4–18 × 1.5–4 cm, blade very narrowly elliptic, lanceolate, or oblanceolate;</text>
      <biological_entity id="o154" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o155" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o156" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="cauline" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="18" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <relation from="o154" id="r27" name="in" negation="false" src="d0_s3" to="o155" />
      <relation from="o154" id="r28" name="in" negation="false" src="d0_s3" to="o156" />
    </statement>
    <statement id="d0_s4">
      <text>cauline 5–13 × 1–4 cm, blade narrowly elliptic, narrowly lanceolate, or narrowly oblanceolate, margins subentire or repand-denticulate.</text>
      <biological_entity id="o157" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="very narrowly" name="shape" src="d0_s3" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" />
      </biological_entity>
      <biological_entity id="o158" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblanceolate" />
      </biological_entity>
      <biological_entity id="o159" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="repand-denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 4-merous, zygomorphic, opening at sunset;</text>
      <biological_entity id="o160" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-merous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="zygomorphic" />
      </biological_entity>
      <biological_entity id="o161" name="sunset" name_original="sunset" src="d0_s5" type="structure" />
      <relation from="o160" id="r29" name="opening at" negation="false" src="d0_s5" to="o161" />
    </statement>
    <statement id="d0_s6">
      <text>floral-tube 8–12 mm;</text>
      <biological_entity id="o162" name="floral-tube" name_original="floral-tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals 9.5–13 mm;</text>
      <biological_entity id="o163" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="9.5" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, fading pink, rhombic-obovate, 7–12 mm;</text>
      <biological_entity id="o164" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="fading pink" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rhombic-obovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 6.5–9 mm, anthers 2.5–4 mm, pollen 90–100% fertile;</text>
      <biological_entity id="o165" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o166" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o167" name="pollen" name_original="pollen" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="90-100%" name="reproduction" src="d0_s9" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 19–25 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o168" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="19" from_unit="mm" name="some_measurement" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o169" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o170" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o169" id="r30" name="exserted beyond" negation="false" src="d0_s10" to="o170" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules ellipsoid or ovoid, sharply 4-angled, with fairly deep furrows alternating with angles, 6–8.5 × 2–3 mm;</text>
      <biological_entity id="o172" name="furrow" name_original="furrows" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="fairly" name="depth" src="d0_s11" value="deep" />
        <character constraint="with angles" constraintid="o173" is_modifier="false" name="arrangement" src="d0_s11" value="alternating" />
      </biological_entity>
      <biological_entity id="o173" name="angle" name_original="angles" src="d0_s11" type="structure" />
      <relation from="o171" id="r31" name="with" negation="false" src="d0_s11" to="o172" />
    </statement>
    <statement id="d0_s12">
      <text>sessile.</text>
      <biological_entity id="o171" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s11" value="4-angled" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" notes="" src="d0_s11" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1–4, yellowish to light-brown, 2–3 × 1 mm. 2n = 14.</text>
      <biological_entity id="o174" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="4" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s13" value="yellowish" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s13" value="light-brown" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s13" to="light-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character name="width" src="d0_s13" unit="mm" value="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o175" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera coloradensis is currently known from fewer than two dozen populations from southern Laramie and Platte counties in Wyoming, northern Weld County, Colorado, formerly near Fort Collins, Larimer County, Colorado, and in western Kimball County, Nebraska.  It is federally listed as a threatened species in the United States.  The primary threats are agricultural use of habitat, herbicide spraying to control weed species, and livestock trampling and grazing (see W. L. Wagner et al. 2013).  Recent study by K. N. Krakos (unpubl.) has determined this species to be self-compatible.  P. H. Raven and D. P. Gregory (1972[1973]) described this species as glandular puberulent in inflorescence, which was repeated in the recent revised taxonomy (Wagner et al.); however, examination of specimens show that P. A. Munz (1965) was correct in describing the pubescence of the inflorescence as non-glandular.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>In wet meadow vegetation of North and South Platte River watersheds on high plains, sloping floodplains, drainage basins in heavy soil.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadow vegetation" modifier="in" constraint="of north and south platte river watersheds on high plains , sloping floodplains" />
        <character name="habitat" value="platte river watersheds" modifier="south" constraint="on high plains , sloping floodplains" />
        <character name="habitat" value="high plains" />
        <character name="habitat" value="sloping floodplains" />
        <character name="habitat" value="drainage" />
        <character name="habitat" value="heavy soil" modifier="basins in" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Nebr., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>