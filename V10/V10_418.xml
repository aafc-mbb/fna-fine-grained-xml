<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Spach) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Phaeostoma</taxon_name>
    <taxon_name authority="(H. Lewis &amp; M. E. Lewis) W. L. Wagner &amp; Hoch" date="2007" rank="subsection">Connubium</taxon_name>
    <taxon_name authority="(Abrams) A. Nelson &amp; J. F. Macbride" date="1905" rank="species">delicata</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>65: 60.  1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section phaeostoma;subsection connubium;species delicata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Godetia</taxon_name>
    <taxon_name authority="Abrams" date="1905" rank="species">delicata</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>32: 539.  1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus godetia;species delicata</taxon_hierarchy>
  </taxon_identification>
  <number>33.</number>
  <other_name type="common_name">Campo or delicate clarkia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 20–70 cm, glabrous and glaucous distally, usually puberulent basally.</text>
      <biological_entity id="o300" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="glaucous" />
        <character is_modifier="false" modifier="usually; basally" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole to 10 mm;</text>
      <biological_entity id="o301" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o302" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate to elliptic or ovate, 1.5–4 cm.</text>
      <biological_entity id="o303" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o304" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="elliptic or ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences open racemes, sometimes branched, axis straight;</text>
      <biological_entity id="o305" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="branched" />
      </biological_entity>
      <biological_entity id="o306" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o307" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" />
      </biological_entity>
      <relation from="o305" id="r27" name="open" negation="false" src="d0_s3" to="o306" />
    </statement>
    <statement id="d0_s4">
      <text>buds pendent.</text>
      <biological_entity id="o308" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: floral-tube 2 mm;</text>
      <biological_entity id="o309" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o310" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed together to 1 side;</text>
      <biological_entity id="o311" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o312" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="together" name="orientation" src="d0_s6" value="reflexed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="1" />
      </biological_entity>
      <biological_entity id="o313" name="side" name_original="side" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>corolla rotate, petals oblanceolate to obovate, 8–12 mm, claw tapered, shorter than blade, apex entire;</text>
      <biological_entity id="o314" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o315" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rotate" />
      </biological_entity>
      <biological_entity id="o316" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="obovate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s7" to="obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o317" name="claw" name_original="claw" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="tapered" />
        <character constraint="than blade" constraintid="o318" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" />
      </biological_entity>
      <biological_entity id="o318" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o319" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 8, unequal, outer anthers orange-red, inner smaller, paler.</text>
      <biological_entity id="o320" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o321" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" />
      </biological_entity>
      <biological_entity constraint="outer" id="o322" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="orange-red" />
      </biological_entity>
      <biological_entity constraint="inner" id="o323" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="smaller" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="paler" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 15–35 mm;</text>
    </statement>
    <statement id="d0_s10">
      <text>subsessile.</text>
      <biological_entity id="o324" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds brown, 1–1.5 mm, tuberculate (especially on raphe), crest inconspicuous.</text>
      <biological_entity id="o325" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 36.</text>
      <biological_entity id="o326" name="crest" name_original="crest" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="inconspicuous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o327" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Clarkia delicata is known in California only from the Peninsular Ranges, mainly in San Diego County with outliers in Riverside and San Bernardino counties, and in northern Baja California, Mexico.  Because of its limited range, it is listed as rare by the California Native Plant Society.  It is a tetraploid derived from hybri­dization between C. epilobioides and C. unguiculata.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oak woodlands, chaparral.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>