<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007" rank="section">Gaura</taxon_name>
    <taxon_name authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007" rank="subsection">Gaura</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="species">gaura</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 212.  2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section gaura;subsection gaura;species gaura</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaura</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">biennis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 347.  1753</place_in_publication>
      <other_info_on_pub>not Oenothera biennis Linnaeus 1753</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus gaura;species biennis</taxon_hierarchy>
  </taxon_identification>
  <number>53.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs usually robust winter-annual, sometimes biennial, usually moderately to densely villous, rarely short-hirtellous, also glandular puberulent;</text>
    </statement>
    <statement id="d0_s1">
      <text>from fleshy taproot.</text>
      <biological_entity id="o223" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s0" value="robust" />
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" />
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s0" value="biennial" />
        <character is_modifier="false" modifier="usually moderately; moderately to densely" name="pubescence" src="d0_s0" value="villous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="short-hirtellous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually well-branched distal to base, 50–180 cm.</text>
      <biological_entity id="o224" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="well-branched" />
        <character constraint="to base" constraintid="o225" is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="180" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o225" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a basal rosette and cauline, basal 8–20 × 1.5–3 cm, blade oblanceolate, margins irregularly toothed to lobed;</text>
      <biological_entity id="o226" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o227" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o228" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o229" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" />
      </biological_entity>
      <relation from="o226" id="r39" name="in" negation="false" src="d0_s3" to="o227" />
    </statement>
    <statement id="d0_s4">
      <text>cauline 1.5–12 × 0.5–3 cm, blade narrowly elliptic to elliptic or lanceolate, margins subentire or undulate-denticulate.</text>
      <biological_entity id="o230" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" notes="[duplicate value]" src="d0_s3" value="toothed" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="lobed" />
        <character char_type="range_value" from="irregularly toothed" name="shape" src="d0_s3" to="lobed" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" />
      </biological_entity>
      <biological_entity id="o231" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" />
      </biological_entity>
      <biological_entity id="o232" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate-denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 4-merous, zygomorphic, opening at sunset;</text>
      <biological_entity id="o233" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-merous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="zygomorphic" />
      </biological_entity>
      <biological_entity id="o234" name="sunset" name_original="sunset" src="d0_s5" type="structure" />
      <relation from="o233" id="r40" name="opening at" negation="false" src="d0_s5" to="o234" />
    </statement>
    <statement id="d0_s6">
      <text>floral-tube 6–13 mm;</text>
      <biological_entity id="o235" name="floral-tube" name_original="floral-tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals 5–13 mm;</text>
      <biological_entity id="o236" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, fading pink to red, narrowly elliptic-obovate, 6–12 mm;</text>
      <biological_entity id="o237" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="fading pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s8" value="red" />
        <character char_type="range_value" from="fading pink" name="coloration" src="d0_s8" to="red" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="elliptic-obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 5–10 mm, anthers 2–4 mm, pollen 35–65% fertile;</text>
      <biological_entity id="o238" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o239" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o240" name="pollen" name_original="pollen" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="35-65%" name="reproduction" src="d0_s9" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 12–15 mm, stigma surrounded by anthers.</text>
      <biological_entity id="o241" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o242" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o243" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o242" id="r41" name="surrounded by" negation="false" src="d0_s10" to="o243" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules ellipsoid, 4-angled, 5–9 × 2–3 mm;</text>
    </statement>
    <statement id="d0_s12">
      <text>sessile.</text>
      <biological_entity id="o244" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="4-angled" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 3–6, light to reddish-brown, 2–2.5 × 1–1.3 mm. 2n = 14.</text>
      <biological_entity id="o245" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="6" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s13" value="light" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s13" value="reddish-brown" />
        <character char_type="range_value" from="light" name="coloration" src="d0_s13" to="reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s13" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o246" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera gaura is a PTH species and forms a ring of 14 chromosomes in meiosis.  It is self-compatible and autogamous (P. H. Raven and D. P. Gregory 1972[1973]), and may have been derived from O. filiformis.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woods, fields, along streams, disturbed sites, ditch banks, roadsides, railway embankments.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woods" />
        <character name="habitat" value="fields" constraint="along streams , disturbed sites , ditch banks , roadsides , railway embankments" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="ditch banks" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railway embankments" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Conn., Del., D.C., Ill., Ind., Iowa, Ky., Md., Mass., Mich., Minn., N.J., N.Y., N.C., Ohio, Pa., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>