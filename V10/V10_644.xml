<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">CHYLISMIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Chylismia</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) A. Heller" date="1906" rank="species">claviformis</taxon_name>
    <taxon_name authority="(A. Heller) W. L. Wagner &amp; Hoch" date="2007" rank="subspecies">lancifolia</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 206. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus chylismia;section chylismia;species claviformis;subspecies lancifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chylismia</taxon_name>
    <taxon_name authority="A. Heller" date="1906" rank="species">lancifolia</taxon_name>
    <place_of_publication>
      <publication_title>Muhlenbergia</publication_title>
      <place_in_publication>2: 226.  1906</place_in_publication>
      <other_info_on_pub>(as Chylisma)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus chylismia;species lancifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) P. H. Raven" date="unknown" rank="species">claviformis</taxon_name>
    <taxon_name authority="(A. Heller) P. H. Raven" date="unknown" rank="subspecies">lancifolia</taxon_name>
    <taxon_hierarchy>genus camissonia;species claviformis;subspecies lancifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">claviformis</taxon_name>
    <taxon_name authority="(A. Heller) Cronquist" date="unknown" rank="variety">lancifolia</taxon_name>
    <taxon_hierarchy>genus c.;species claviformis;variety lancifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont" date="unknown" rank="species">claviformis</taxon_name>
    <taxon_name authority="(A. Heller) P. H. Raven" date="unknown" rank="subspecies">lancifolia</taxon_name>
    <taxon_hierarchy>genus oenothera;species claviformis;subspecies lancifolia</taxon_hierarchy>
  </taxon_identification>
  <number>6f.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs strigillose proximally, glabrous and often glaucous distally.</text>
      <biological_entity id="o997" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" modifier="often; distally" name="pubescence" src="d0_s0" value="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 5–60 cm.</text>
      <biological_entity id="o998" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade lateral lobes usually poorly developed or absent, terminal lobe lanceolate, to 5 × 2.8 cm, margins irregularly serrate-dentate.</text>
      <biological_entity id="o999" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1000" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity constraint="lateral" id="o1001" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually poorly" name="development" src="d0_s2" value="developed" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o1002" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="2.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1003" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s2" value="serrate-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers usually opening at sunset;</text>
      <biological_entity id="o1004" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o1005" name="sunset" name_original="sunset" src="d0_s3" type="structure" />
      <relation from="o1004" id="r212" name="opening at" negation="false" src="d0_s3" to="o1005" />
    </statement>
    <statement id="d0_s4">
      <text>buds usually without free tips, sometimes with apical free tips less than 1 mm;</text>
      <biological_entity id="o1006" name="bud" name_original="buds" src="d0_s4" type="structure" />
      <biological_entity id="o1007" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" />
      </biological_entity>
      <biological_entity constraint="apical" id="o1008" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o1006" id="r213" name="without" negation="false" src="d0_s4" to="o1007" />
      <relation from="o1006" id="r214" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o1008" />
    </statement>
    <statement id="d0_s5">
      <text>floral-tube orangebrown inside, 3.5–6 mm;</text>
      <biological_entity id="o1009" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="orangebrown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals bright-yellow, sometimes red-dotted in proximal 1/2, fading pale orange, 3.5–7 mm. 2n = 14.</text>
      <biological_entity id="o1010" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="bright-yellow" />
        <character constraint="in proximal 1/2" constraintid="o1011" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="red-dotted" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="fading pale" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="orange" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1011" name="1/2" name_original="1/2" src="d0_s6" type="structure" />
      <biological_entity constraint="2n" id="o1012" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies lancifolia is known from east of the Sierra Nevada in southern Mono and Inyo counties, California, and adjacent Mineral County, Nevada.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy slopes and flats, often with Artemisia tridentata or Ericameria.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy slopes" />
        <character name="habitat" value="flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1700(–1900) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1900" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>