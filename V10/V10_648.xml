<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">CHYLISMIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Chylismia</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) A. Heller" date="1906" rank="species">claviformis</taxon_name>
    <taxon_name authority="(P. H. Raven) W. L. Wagner &amp; Hoch" date="2007" rank="subspecies">yumae</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 207. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus chylismia;section chylismia;species claviformis;subspecies yumae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont" date="unknown" rank="species">claviformis</taxon_name>
    <taxon_name authority="P. H. Raven" date="1962" rank="subspecies">yumae</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>34: 104.  1962</place_in_publication>
      <other_info_on_pub>(as clavaeformis)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species claviformis;subspecies yumae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(Torrey &amp; Frémont) P. H. Raven" date="unknown" rank="species">claviformis</taxon_name>
    <taxon_name authority="(P. H. Raven) P. H. Raven" date="unknown" rank="subspecies">yumae</taxon_name>
    <taxon_hierarchy>genus camissonia;species claviformis;subspecies yumae</taxon_hierarchy>
  </taxon_identification>
  <number>6j.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs strigillose, often densely so, sometimes also glandular puberulent distally.</text>
      <biological_entity id="o1055" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigillose" />
        <character is_modifier="false" modifier="often densely; densely; sometimes" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 5–40 cm.</text>
      <biological_entity id="o1056" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade lateral lobes poorly or well developed, terminal lobe lanceolate, to 6.5 × 2 cm, margins irregularly sinuate-dentate.</text>
      <biological_entity id="o1057" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1058" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity constraint="lateral" id="o1059" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s2" value="developed" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o1060" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1061" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s2" value="sinuate-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers opening at sunset;</text>
      <biological_entity id="o1062" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o1063" name="sunset" name_original="sunset" src="d0_s3" type="structure" />
      <relation from="o1062" id="r221" name="opening at" negation="false" src="d0_s3" to="o1063" />
    </statement>
    <statement id="d0_s4">
      <text>buds usually without free tips, sometimes with apical free tips less than 1 mm;</text>
      <biological_entity id="o1064" name="bud" name_original="buds" src="d0_s4" type="structure" />
      <biological_entity id="o1065" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" />
      </biological_entity>
      <biological_entity constraint="apical" id="o1066" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o1064" id="r222" name="without" negation="false" src="d0_s4" to="o1065" />
      <relation from="o1064" id="r223" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o1066" />
    </statement>
    <statement id="d0_s5">
      <text>floral-tube orangebrown inside, 2.5–4 mm;</text>
      <biological_entity id="o1067" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="orangebrown" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals pale-yellow, fading reddish or not changing color, 3–5 mm. 2n = 14.</text>
      <biological_entity id="o1068" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="fading reddish" />
        <character name="coloration" src="d0_s6" value="not" />
        <character char_type="range_value" from="3" from_unit="mm" name="coloration" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1069" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies yumae is known from southeastern Imperial County, California, Yuma Desert, Arizona, and from El Gran Desierto to Puerto Peñasco in north­western Sonora, and in northeastern Baja California.  The subspecies is probably from hybridization between subspp. aurantiaca and peirsonii; it intergrades with subsp. aurantiaca and rarely hybridizes with Chylismia brevipes subsp. arizonica.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Very arid dunes and sandy flats, with Ambrosia dumosa and Larrea tridentata.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dunes" modifier="very arid" />
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="ambrosia dumosa" modifier="with" />
        <character name="habitat" value="larrea tridentata" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>