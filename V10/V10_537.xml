<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Shirley A. Graham</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LYTHRUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 446.  1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 205.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus lythrum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek lythron, gore, alluding to use of L. salicaria in arresting hemorrhages</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Loosestrife</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial,or subshrubs, terrestrial or amphibious, 0.5–15 (–30) dm, green, gray-green, or gray-white glaucous, usually glabrous, sometimes puberulent.</text>
      <biological_entity id="o3605" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="terrestrial" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="amphibious" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray-white" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray-white" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="glaucous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
      <biological_entity id="o3606" name="subshrub" name_original="subshrubs" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, weakly erect, or procumbent, usually branched, youngest growth narrowly 4-ridged or winged, submerged stems sometimes thickened by spongy tissue.</text>
      <biological_entity id="o3607" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" />
      </biological_entity>
      <biological_entity id="o3608" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="youngest" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="4-ridged" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="winged" />
      </biological_entity>
      <biological_entity id="o3609" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="submerged" />
        <character constraint="by tissue" constraintid="o3610" is_modifier="false" modifier="sometimes" name="size_or_width" src="d0_s1" value="thickened" />
      </biological_entity>
      <biological_entity id="o3610" name="tissue" name_original="tissue" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="spongy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite, subopposite, alternate, subalternate, or whorled;</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile or subsessile;</text>
      <biological_entity id="o3611" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subopposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subalternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="whorled" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subalternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate, obovate, oblong, oblong-lanceolate, lanceolate, oblanceolate, linear, linear-oblong, linear-lanceolate, orbiculate, or suborbiculate, base rounded, cordate, or attenuate.</text>
      <biological_entity id="o3612" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate" />
      </biological_entity>
      <biological_entity id="o3613" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal and spikelike or racemes.</text>
      <biological_entity id="o3614" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="spikelike" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o3615" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o3616" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers sessile, subsessile, or short-pedicellate, actinomorphic, mono, di, or tristylous;</text>
      <biological_entity id="o3617" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="short-pedicellate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="short-pedicellate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral-tube perigynous, usually cylindrical or obconic (campanulate in L. portula), 8–12-ribbed;</text>
      <biological_entity id="o3618" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="perigynous" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obconic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="8-12-ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>epicalyx segments shorter to or up to 2 times longer than sepals;</text>
      <biological_entity constraint="epicalyx" id="o3619" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" />
        <character constraint="sepal" constraintid="o3620" is_modifier="false" name="length_or_size" src="d0_s8" value="0-2 times longer than sepals" />
      </biological_entity>
      <biological_entity id="o3620" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 6, narrowly deltate to subulate, obtuse and thickened in L. tribracteatum;</text>
      <biological_entity id="o3621" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s9" value="deltate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s9" value="subulate" />
        <character char_type="range_value" from="narrowly deltate" name="shape" src="d0_s9" to="subulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" />
        <character constraint="in l" is_modifier="false" name="size_or_width" src="d0_s9" value="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals caducous, (0 or) 6, purple, lavender, rose, rose-purple, pink, or white, sometimes with purple or red midvein;</text>
      <biological_entity id="o3622" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="caducous" />
        <character name="quantity" src="d0_s10" value="6" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="rose" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="rose-purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
      </biological_entity>
      <biological_entity id="o3623" name="midvein" name_original="midvein" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="purple" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="red" />
      </biological_entity>
      <relation from="o3622" id="r433" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o3623" />
    </statement>
    <statement id="d0_s11">
      <text>nectary encircling base of ovary or absent;</text>
      <biological_entity id="o3624" name="nectary" name_original="nectary" src="d0_s11" type="structure" constraint="ovary">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" />
      </biological_entity>
      <biological_entity id="o3625" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o3626" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
      <relation from="o3624" id="r434" name="encircling" negation="false" src="d0_s11" to="o3625" />
      <relation from="o3624" id="r435" name="part_of" negation="false" src="d0_s11" to="o3626" />
    </statement>
    <statement id="d0_s12">
      <text>stamens (2–) 4–6 (–12), usually 6 or 12, in 1 or 2 whorls, complementing style lengths;</text>
      <biological_entity id="o3627" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s12" to="4" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="12" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s12" to="6" />
        <character modifier="usually" name="quantity" src="d0_s12" unit="or" value="6" />
        <character modifier="usually" name="quantity" src="d0_s12" unit="or" value="12" />
      </biological_entity>
      <biological_entity id="o3628" name="complementing" name_original="complementing" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" />
      </biological_entity>
      <biological_entity id="o3629" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" />
      </biological_entity>
      <relation from="o3627" id="r436" name="in" negation="false" src="d0_s12" to="o3628" />
      <relation from="o3627" id="r437" name="in" negation="false" src="d0_s12" to="o3629" />
    </statement>
    <statement id="d0_s13">
      <text>ovary 2-locular;</text>
      <biological_entity id="o3630" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="2-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>placenta elongate;</text>
      <biological_entity id="o3631" name="placenta" name_original="placenta" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style slender, included or well exserted;</text>
      <biological_entity id="o3632" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="slender" />
        <character is_modifier="false" name="position" src="d0_s15" value="included" />
        <character is_modifier="false" modifier="well" name="position" src="d0_s15" value="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate.</text>
      <biological_entity id="o3633" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsules, walls thin and dry, usually dehiscent, dehiscence septicidal or septifragal (indehiscent and splitting irregularly in L. portula, placenta and seeds remaining within capsules).</text>
      <biological_entity constraint="fruits" id="o3634" name="capsule" name_original="capsules" src="d0_s17" type="structure" />
      <biological_entity id="o3635" name="wall" name_original="walls" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="thin" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s17" value="dry" />
        <character is_modifier="false" modifier="usually" name="dehiscence" src="d0_s17" value="dehiscent" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscence" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="septifragal" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 10–90 (+), obovoid to fusiform or subglobose, to 1 mm;</text>
      <biological_entity id="o3636" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="90" from_inclusive="false" name="average_count" src="d0_s18" to="90" unit="10-90[+]" upper_restricted="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s18" to="90" unit="10-90[+]" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s18" value="obovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s18" value="fusiform" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s18" value="subglobose" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s18" to="fusiform or subglobose" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s18" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cotyledons ± complanate.</text>
      <biological_entity id="o3637" name="cotyledon" name_original="cotyledons" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s19" value="complanate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 35 (12 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, South America, Europe, Asia, Africa, Australia; introduced in West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="in West Indies" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lythrum is represented in North America by equal numbers of native and introduced species.  Lythrum thymifolia Linnaeus, native to the Mediterranean, was reported from Mobile, Alabama, on ballast in 1893 (C. T. Mohr 1901).  It is similar to L. hyssopifolia but consistently has stamens reduced to two or three, leaves usually less than 2 mm wide, and is monostylous.</discussion>
  <discussion>Lythrum is one of six genera in Lythraceae with a heterostylous breeding system and one of three in the family with tristylous species (also Decodon and the incipiently tristylous tropical genus Adenaria Kunth).  Lythrum includes mono-, di-, and tristylous species.  All native North American Lythrum species are distylous.  Among the introduced species, L. junceum, L. salicaria, and L. virgatum are tristylous and L. hyssopifolia, L. portula, and L. tribracteatum are monostylous.  The native North American species represent a single lineage corresponding taxonomically to subsect. Pythagorea Koehne.  Some species of the subsection are taxonomically difficult and have shown little molecular divergence (J. A. Morris 2007).  Hybridization and introgression are suspected where two native species co-occur, and also possibly between non-native species or native with non-native (B. Ertter and D. Gowen 2019).</discussion>
  <references>
    <reference>Shinners, L. H.  1953.  Synopsis of the United States species of Lythrum (Lythraceae).  Field &amp; Lab. 21: 80–89.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Floral tubes broadly campanulate, 1 × 1.5 mm, widths greater than lengths; capsules indehiscent, splitting irregularly; stems mostly decumbent to creeping.</description>
      <determination>9. Lythrum portula</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Floral tubes cylindrical or obconic, 3–7 × (0.4–)1–3 mm, lengths distinctly greater than widths; capsules septicidal or septifragal; stems erect, decumbent, or prostrate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences spikelike, terminal (in L. virgatum racemose proximally, spikelike distally); flowers tristylous; stamens 12.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades lanceolate, bases cordate or rounded; plants usually gray-puberulent, sometimes glabrate.</description>
      <determination>10. Lythrum salicaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades lanceolate to narrowly linear, bases attenuate; plants glabrous.</description>
      <determination>12. Lythrum virgatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences racemose, diffuse, leafy; flowers mono-, di-, or tristylous; stamens (2–)4–12.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs or subshrubs 0.5–6 dm; stems decumbent or procumbent to erect, un­branched, branched from near base, or sparsely branched (much-branched distally in L. ovalifolium), sometimes with short accessory branches distally.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Floral tubes obconic, with red spots on proximal half; epicalyx segments about equal to and more prominent than sepals; flowers tristylous; stamens 12.</description>
      <determination>6. Lythrum junceum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Floral tubes obconic or cylindric, without red spots; epicalyx segments shorter than, equal to, or longer than sepals; flowers mono- or distylous; stamens (2–)4–6(–12).</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Floral tubes obcconic, lengths 8–10 times width at tube base; epicalyx segments as long as or longer than sepals; flowers monostylous.</description>
      <determination>11. Lythrum tribracteatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Floral tubes cylindric or slightly obconic, lengths 5 times or less widths; epi­calyx segments about 2 times longer than sepals; flowers mono- or distylous.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves opposite throughout, equal to or shorter than internodes; flowers distylous.</description>
      <determination>4. Lythrum flagellare</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves mostly alternate, sometimes opposite proximally, mostly longer than internodes and closely overlapping distally; flowers mono- or distylous.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Plants gray-green glaucous; leaf blade bases rounded; petals pink or rose, 1/2 length of floral tube; flowers mono- stylous.</description>
      <determination>5. Lythrum hyssopifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Plants green to slightly gray glaucous; leaf blade bases attenuate; petals pale purple to purple, sometimes with red midvein, about as long as floral tube; flowers distylous.</description>
      <determination>8. Lythrum ovalifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs or subshrubs (3–)5–15 dm; stems erect, much-branched distally; flowers distylous.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves usually opposite or subopposite throughout, rarely alternate; floral nectaries absent.</description>
      <determination>7. Lythrum lineare</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves usually opposite to subopposite proximally, alternate distally or sometimes throughout; floral nectaries encircling bases of ovaries.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Branch leaves abruptly and conspicuously smaller than those on main stems; floral tube obconic; pedicels slender; epicalyx segments and sepals about equal length.</description>
      <determination>3. Lythrum curtissii</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Branch leaves gradually smaller than those on main stems; floral tube cylindrical; pedicels stout; epicalyx segments equal to or to 2 times length of sepals.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Herbs or subshrubs green or slightly gray glaucous; leaf blades ovate to oblong and bases subcordate to rounded, or lanceolate to linear- lanceolate and bases attenuate.</description>
      <determination>1. Lythrum alatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Herbs whitish gray glaucous; leaf blades oblong-lanceolate proximally, mostly linear or linear-oblong distally, bases rounded.</description>
      <determination>2. Lythrum californicum</determination>
    </key_statement>
  </key>
</bio:treatment>