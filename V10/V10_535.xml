<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Shirley A. Graham</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">LAGERSTROEMIA</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1068, 1076, 1372.  1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus lagerstroemia</taxon_hierarchy>
    <other_info_on_name type="etymology">For Magnus Lagerstroem, 1696–1759, friend of Linnaeus and supporter of Uppsala University</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Crape or crepe myrtle</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, terrestrial, 30–70 dm, outer bark whitish, smooth, flaking in thin plates, inner bark light-brown or coral, glabrous or puberulent.</text>
      <biological_entity id="o3560" name="shrub" name_original="shrubs" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" />
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="70" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o3561" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="small" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" />
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="70" to_unit="dm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o3562" name="bark" name_original="bark" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="whitish" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s0" value="smooth" />
      </biological_entity>
      <biological_entity id="o3563" name="plate" name_original="plates" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3564" name="bark" name_original="bark" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="coral" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
      <relation from="o3562" id="r431" name="flaking in" negation="false" src="d0_s0" to="o3563" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched.</text>
      <biological_entity id="o3565" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually alternate or subalternate;</text>
    </statement>
    <statement id="d0_s3">
      <text>subsessile [petiolate];</text>
      <biological_entity id="o3566" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subalternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade obovate or elliptic [lanceolate, oblong], base rounded or cuneate [attenuate].</text>
      <biological_entity id="o3567" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" />
      </biological_entity>
      <biological_entity id="o3568" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences determinate, terminal or axillary panicles.</text>
      <biological_entity id="o3569" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="development" src="d0_s5" value="determinate" />
      </biological_entity>
      <biological_entity id="o3570" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers subsessile, actinomorphic, monostylous;</text>
      <biological_entity id="o3571" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral-tube perigynous, campanulate [semiglobose to turbinate], externally smooth [ridged];</text>
      <biological_entity id="o3572" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="perigynous" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" />
        <character is_modifier="false" modifier="externally" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>epicalyx segments absent;</text>
      <biological_entity constraint="epicalyx" id="o3573" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals (5 or) 6 (or 7), 1/3–1/2 floral-tube length;</text>
      <biological_entity id="o3574" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s9" to="1/2" />
      </biological_entity>
      <biological_entity id="o3575" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals caducous or deciduous, (5 or) 6 (or 7) [–9], rose, purple, or white, obovate to orbiculate, often clawed;</text>
      <biological_entity id="o3576" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="caducous" />
        <character is_modifier="false" name="duration" src="d0_s10" value="deciduous" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="9" />
        <character name="quantity" src="d0_s10" value="6" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="rose" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="orbiculate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s10" to="orbiculate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s10" value="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>nectary absent;</text>
      <biological_entity id="o3577" name="nectary" name_original="nectary" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 36–42 [–200], dimorphic [uniform];</text>
      <biological_entity id="o3578" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="42" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="200" />
        <character char_type="range_value" from="36" name="quantity" src="d0_s12" to="42" />
        <character is_modifier="false" name="growth_form" src="d0_s12" value="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary (3–) 6-locular;</text>
      <biological_entity id="o3579" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="(3-)6-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>placenta elongate;</text>
      <biological_entity id="o3580" name="placenta" name_original="placenta" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style slender;</text>
      <biological_entity id="o3581" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate to punctiform.</text>
      <biological_entity id="o3582" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s16" value="capitate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s16" value="punctiform" />
        <character char_type="range_value" from="capitate" name="shape" src="d0_s16" to="punctiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsules, woody, dehiscence loculicidal.</text>
      <biological_entity constraint="fruits" id="o3583" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="texture" src="d0_s17" value="woody" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds ca. 20–50, obpyramidal, unilaterally winged from raphe;</text>
      <biological_entity id="o3584" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s18" to="50" />
        <character is_modifier="false" name="shape" src="d0_s18" value="obpyramidal" />
        <character constraint="from raphe" constraintid="o3585" is_modifier="false" modifier="unilaterally" name="architecture" src="d0_s18" value="winged" />
      </biological_entity>
      <biological_entity id="o3585" name="raphe" name_original="raphe" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>cotyledons rolled.</text>
      <biological_entity id="o3586" name="cotyledon" name_original="cotyledons" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rolled" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 56 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; Asia, Pacific Islands, n Australia; introduced also widely in tropical and subtropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="n Australia" establishment_means="introduced" />
        <character name="distribution" value="also widely in tropical and subtropical areas" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <references>
    <reference>Furtado, C. X. and S. Montien.  1969.  A revision of Lagerstroemia Linnaeus (Lythraceae).  Gard. Bull. Straits Settlem. 24: 185–334.</reference>
    <reference>Kim, S. C., S. A. Graham, and A. Graham.  1994.  Palynology and pollen dimorphism in the genus Lagerstroemia (Lythraceae).  Grana 33: 1–20.</reference>
  </references>
  
</bio:treatment>