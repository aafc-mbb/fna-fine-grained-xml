<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Myxocarpa</taxon_name>
    <taxon_name authority="E. Small" date="1971" rank="species">australis</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>49: 1216, fig. 4D.  1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section myxocarpa;species australis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clarkia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">virgata</taxon_name>
    <taxon_name authority="(E. Small) D. W. Taylor" date="unknown" rank="variety">australis</taxon_name>
    <taxon_hierarchy>genus clarkia;species virgata;variety australis</taxon_hierarchy>
  </taxon_identification>
  <number>15.</number>
  <other_name type="common_name">Small’s southern clarkia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, to 100 cm, puber­ulent.</text>
      <biological_entity id="o6613" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 10–30 mm;</text>
      <biological_entity id="o6614" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o6615" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear to lanceolate, 2–5 cm.</text>
      <biological_entity id="o6616" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6617" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences open racemes, axis recurved only at tip in bud, straight 4+ nodes distal to open flowers;</text>
      <biological_entity id="o6618" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o6619" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o6620" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character constraint="at tip" constraintid="o6621" is_modifier="false" name="orientation" src="d0_s3" value="recurved" />
        <character is_modifier="false" name="course" notes="" src="d0_s3" value="straight" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6621" name="tip" name_original="tip" src="d0_s3" type="structure" />
      <biological_entity id="o6622" name="bud" name_original="bud" src="d0_s3" type="structure" />
      <biological_entity id="o6623" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" />
      </biological_entity>
      <biological_entity id="o6624" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <relation from="o6618" id="r1177" name="open" negation="false" src="d0_s3" to="o6619" />
      <relation from="o6621" id="r1178" name="in" negation="false" src="d0_s3" to="o6622" />
      <relation from="o6623" id="r1179" name="open" negation="false" src="d0_s3" to="o6624" />
    </statement>
    <statement id="d0_s4">
      <text>buds pendent, narrowly obovoid, tip obtuse.</text>
      <biological_entity id="o6625" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="pendent" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="obovoid" />
      </biological_entity>
      <biological_entity id="o6626" name="tip" name_original="tip" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: floral-tube 2–4 mm;</text>
      <biological_entity id="o6627" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o6628" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed individually;</text>
      <biological_entity id="o6629" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6630" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="individually" name="orientation" src="d0_s6" value="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla rotate, petals lavender-purple, mottled or spotted reddish purple, ± rhombic, unlobed, 6–12 (–14) ×3–7 mm, length 2.2–3 times width;</text>
      <biological_entity id="o6631" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6632" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rotate" />
      </biological_entity>
      <biological_entity id="o6633" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender-purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="mottled" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="spotted reddish" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s7" value="purple" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="unlobed" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="14" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s7" value="2.2-3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 8, sub­equal, subtended by ciliate scales, pollen blue-gray;</text>
      <biological_entity id="o6634" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6635" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" />
      </biological_entity>
      <biological_entity id="o6636" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" />
      </biological_entity>
      <biological_entity id="o6637" name="pollen" name_original="pollen" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue-gray" />
      </biological_entity>
      <relation from="o6635" id="r1180" name="subtended by" negation="false" src="d0_s8" to="o6636" />
    </statement>
    <statement id="d0_s9">
      <text>ovary shallowly 4-grooved;</text>
      <biological_entity id="o6638" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6639" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture" src="d0_s9" value="4-grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigma exserted beyond anthers.</text>
      <biological_entity id="o6640" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6641" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o6642" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o6641" id="r1181" name="exserted beyond" negation="false" src="d0_s10" to="o6642" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules 10–20 mm;</text>
      <biological_entity id="o6643" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicel 0–4 mm.</text>
      <biological_entity id="o6644" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds brown, 1–1.5 mm, scaly.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 10.</text>
      <biological_entity id="o6645" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s13" value="scaly" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6646" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Clarkia australis is found in the foothills of the cen­tral Sierra Nevada range, from Calaveras, Madera, Mari­posa, and Tuolumne counties, and has been designated as rare by the California Native Plant Society.</discussion>
  <discussion>Clarkia australis is morphologically very similar to C. virgata and, based on its more southern distribution, may be derived from it.  They are most readily distinguished morphologically by the narrower leaves of C. australis.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Yellow-pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" modifier="yellow-pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>