<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POLYGALA</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">setacea</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 52.  1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species setacea</taxon_hierarchy>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Coastal-plain or scale-leaf milkwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual or short-lived perennial, usually single-stemmed, sometimes 2 or 3 stems near base, 1–5 dm, usually branched distally;</text>
      <biological_entity id="o2240" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>from taproot or, when perennial, sometimes with slender taprootlike caudex with persistent stem base.</text>
      <biological_entity id="o2239" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="single-stemmed" />
        <character modifier="sometimes" name="quantity" src="d0_s0" unit="or stems" value="2" />
        <character constraint="near base" constraintid="o2240" modifier="sometimes" name="quantity" src="d0_s0" unit="or stems" value="3" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s0" value="branched" />
      </biological_entity>
      <biological_entity id="o2241" name="taproot" name_original="taproot" src="d0_s1" type="structure" />
      <biological_entity constraint="slender" id="o2242" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="taprootlike" />
        <character is_modifier="true" name="duration" src="d0_s1" value="perennial" />
      </biological_entity>
      <biological_entity constraint="stem" id="o2243" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" />
      </biological_entity>
      <relation from="o2239" id="r274" name="from" negation="false" src="d0_s1" to="o2241" />
      <relation from="o2239" id="r275" name="from" negation="false" src="d0_s1" to="o2242" />
      <relation from="o2242" id="r276" name="with" negation="false" src="d0_s1" to="o2243" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, glabrous.</text>
      <biological_entity id="o2244" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alter­nate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o2245" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade subulate, squamiform, 0.5–1.6 (–2) × 0.3–0.7 mm, base obtuse, apex acute, surfaces glabrous.</text>
      <biological_entity id="o2246" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="squamiform" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s5" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2247" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" />
      </biological_entity>
      <biological_entity id="o2248" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" />
      </biological_entity>
      <biological_entity id="o2249" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes cylindric, 0.4–3.5 × 0.3–0.5 cm;</text>
      <biological_entity id="o2250" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s6" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle 0.1–0.5 cm;</text>
      <biological_entity id="o2251" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s7" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts deciduous, lanceolate.</text>
      <biological_entity id="o2252" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0.2–0.5 mm, glabrous.</text>
      <biological_entity id="o2253" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers usually white, sometimes pinkish tinged, 1.8–2.7 mm;</text>
      <biological_entity id="o2254" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="pinkish tinged" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals ovate to lanceolate-ovate, 0.6–1 (–1.5) mm;</text>
      <biological_entity id="o2255" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="lanceolate-ovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="lanceolate-ovate" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>wings elliptic to obovate, 1.5–2.5 × 0.6–1.1 mm, apex usually obtuse to bluntly rounded, rarely acute, often minutely apiculate or cuspidate;</text>
      <biological_entity id="o2256" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="obovate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s12" to="obovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s12" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2257" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" notes="[duplicate value]" src="d0_s12" value="obtuse" />
        <character is_modifier="false" modifier="bluntly" name="shape" notes="[duplicate value]" src="d0_s12" value="rounded" />
        <character char_type="range_value" from="usually obtuse" name="shape" src="d0_s12" to="bluntly rounded" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s12" value="acute" />
        <character is_modifier="false" modifier="often minutely" name="architecture_or_shape" src="d0_s12" value="apiculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keel 1.5–2.2 mm, crest 2-parted, with 2or 3 lobes on each side.</text>
      <biological_entity id="o2258" name="keel" name_original="keel" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2259" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="2-parted" />
      </biological_entity>
      <biological_entity id="o2260" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="3" />
      </biological_entity>
      <biological_entity id="o2261" name="side" name_original="side" src="d0_s13" type="structure" />
      <relation from="o2259" id="r277" name="with" negation="false" src="d0_s13" to="o2260" />
      <relation from="o2260" id="r278" name="on" negation="false" src="d0_s13" to="o2261" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid to ellipsoid, 1.7–2.2 × 1.2–1.5 mm, margins not winged.</text>
      <biological_entity id="o2262" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="ellipsoid" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="ellipsoid" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s14" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2263" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 0.8–1.2 mm, pubescent;</text>
      <biological_entity id="o2264" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>aril vestigial.</text>
      <biological_entity id="o2265" name="aril" name_original="aril" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="vestigial" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The protologue description of Polygala setacea with “in Carolina septentrionali” is the likely source of later reports of the species occurring in the Carolinas, (for example, J. K. Small 1933; R. W. Long and O. Lakela 1971; R. K. Godfrey and J. W. Wooten 1981).  There are no known specimens from either of the Carolinas; the locality reported by Michaux may be erroneous.  Small reported Polygala setacea also from Mississippi; no supporting specimens are known.  The presence of this species in Mississippi or either of the Carolinas would represent a disjunction from the range documented by known vouchers.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to somewhat dry flatwoods, pine-palmetto woodlands, margins of seepage bogs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry flatwoods" modifier="somewhat" />
        <character name="habitat" value="pine-palmetto woodlands" />
        <character name="habitat" value="moist to dry flatwoods" modifier="somewhat" constraint="of seepage bogs" />
        <character name="habitat" value="pine-palmetto woodlands" constraint="of seepage bogs" />
        <character name="habitat" value="margins" constraint="of seepage bogs" />
        <character name="habitat" value="seepage bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>