<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Warren L. Wagner</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893" rank="genus">CHYLISMIA</taxon_name>
    <place_of_publication>
      <publication_title>Nat. Pflanzenfam.</publication_title>
      <place_in_publication>96[III,7]: 217.  1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus chylismia</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chylos, juice or succulence, and -isma, condition, alluding to fleshy leaves of C. scapoidea, the type species</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus [unranked] Chylismia Torrey &amp; A. Gray" date=" 1840" rank="genus">Oenothera</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 506.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Link" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) P. H. Raven" date="unknown" rank="section">Chylismia</taxon_name>
    <taxon_hierarchy>genus camissonia;section chylismia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) P. H. Raven" date="unknown" rank="section">Chylismia</taxon_name>
    <taxon_hierarchy>genus oenothera;section chylismia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Jepson" date="unknown" rank="subgenus">Chylismia</taxon_name>
    <taxon_hierarchy>genus oenothera;subgenus chylismia</taxon_hierarchy>
  </taxon_identification>
  <number>16.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs,usually annual, sometimes perennial, rarely biennial, usually caulescent.</text>
      <biological_entity id="o589" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="biennial" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="caulescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascend­ing to erect, usually branched.</text>
      <biological_entity id="o590" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, cauline often reduced, basal often form­ing well-developed rosette, alternate;</text>
      <biological_entity id="o591" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s2" value="reduced" />
        <character is_modifier="false" modifier="often" name="position" src="d0_s2" value="basal" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s2" value="alternate" />
      </biological_entity>
      <biological_entity id="o592" name="rosette" name_original="rosette" src="d0_s2" type="structure">
        <character is_modifier="true" name="development" src="d0_s2" value="well-developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules absent;</text>
    </statement>
    <statement id="d0_s4">
      <text>long-petiolate;</text>
      <biological_entity id="o593" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="long-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade often pin­nately (rarely bipinnately) lobed, sometimes unlobed, or lateral lobes greatly reduced or absent, ter­minal lobe usually large, margins usually regularly or irregularly dentate to serrate, sometimes denticu­late, serrulate, or entire, abaxial surface or margin with ± conspicuous, usually brown, oil cells.</text>
      <biological_entity id="o594" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s5" value="reduced" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o595" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="nately" name="shape" src="d0_s5" value="lobed" />
        <character is_modifier="true" modifier="sometimes" name="shape" src="d0_s5" value="unlobed" />
      </biological_entity>
      <biological_entity id="o596" name="lobe" name_original="lobe" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s5" value="large" />
      </biological_entity>
      <biological_entity id="o597" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" notes="[duplicate value]" src="d0_s5" value="dentate" />
        <character is_modifier="false" name="architecture_or_shape" notes="[duplicate value]" src="d0_s5" value="serrate" />
        <character char_type="range_value" from="dentate" modifier="irregularly" name="architecture_or_shape" src="d0_s5" to="serrate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s5" value="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o598" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o599" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <biological_entity constraint="oil" id="o600" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="prominence" src="d0_s5" value="conspicuous" />
        <character is_modifier="true" modifier="usually" name="coloration" src="d0_s5" value="brown" />
      </biological_entity>
      <relation from="o594" id="r137" name="pin ­" negation="false" src="d0_s5" to="o595" />
      <relation from="o598" id="r138" name="with" negation="false" src="d0_s5" to="o600" />
      <relation from="o599" id="r139" name="with" negation="false" src="d0_s5" to="o600" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences racemes, erect or nodding.</text>
      <biological_entity constraint="inflorescences" id="o601" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual, actinomorphic, buds usually erect, sometimes reflexed;</text>
      <biological_entity id="o602" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="actinomorphic" />
      </biological_entity>
      <biological_entity id="o603" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s7" value="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral-tube deciduous (with sepals, petals, and stamens after anthesis), with basal nectary;</text>
      <biological_entity id="o604" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o605" name="nectary" name_original="nectary" src="d0_s8" type="structure" />
      <relation from="o604" id="r140" name="with" negation="false" src="d0_s8" to="o605" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 4, reflexed singly;</text>
      <biological_entity id="o606" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 4, usually yellow or white, often fading orange-red, sometimes lavender or purple, rarely cream, often with 1+ red dots near base;</text>
      <biological_entity id="o607" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="fading orange-red" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="cream" />
      </biological_entity>
      <biological_entity id="o608" name="base" name_original="base" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="red dots" />
      </biological_entity>
      <relation from="o607" id="r141" modifier="often" name="with" negation="false" src="d0_s10" to="o608" />
    </statement>
    <statement id="d0_s11">
      <text>stamens usually 8, in 2 subequal series, rarely 4 in 1 series (usually in C. exilis), anthers versatile, pollen shed singly or in tetrads;</text>
      <biological_entity id="o609" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="8" />
      </biological_entity>
      <biological_entity id="o610" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" />
        <character is_modifier="true" name="size" src="d0_s11" value="subequal" />
        <character constraint="in series" constraintid="o611" modifier="rarely" name="quantity" src="d0_s11" value="4" />
      </biological_entity>
      <biological_entity id="o611" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" />
      </biological_entity>
      <biological_entity id="o612" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s11" value="versatile" />
      </biological_entity>
      <biological_entity id="o613" name="pollen" name_original="pollen" src="d0_s11" type="structure" />
      <biological_entity id="o614" name="tetrad" name_original="tetrads" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="singly" />
      </biological_entity>
      <relation from="o609" id="r142" name="in" negation="false" src="d0_s11" to="o610" />
      <relation from="o613" id="r143" name="shed" negation="false" src="d0_s11" to="o614" />
    </statement>
    <statement id="d0_s12">
      <text>ovary 4-locular, stigma usually entire and capitate, rarely conical-peltate and ± 4-lobed, surface unknown, probably wet and non-papillate.</text>
      <biological_entity id="o615" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="4-locular" />
      </biological_entity>
      <biological_entity id="o616" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s12" value="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="capitate" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s12" value="conical-peltate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="4-lobed" />
      </biological_entity>
      <biological_entity id="o617" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="probably" name="condition" src="d0_s12" value="wet" />
        <character is_modifier="false" name="relief" src="d0_s12" value="non-papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruit a capsule, straight or slightly curved, subterete and clavate or oblong-cylindrical, regularly loculicidal;</text>
    </statement>
    <statement id="d0_s14">
      <text>pedicellate.</text>
      <biological_entity id="o618" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s13" value="curved" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subterete" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-cylindrical" />
        <character is_modifier="false" modifier="regularly" name="dehiscence" src="d0_s13" value="loculicidal" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="pedicellate" />
      </biological_entity>
      <biological_entity id="o619" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s13" value="curved" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subterete" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-cylindrical" />
        <character is_modifier="false" modifier="regularly" name="dehiscence" src="d0_s13" value="loculicidal" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds numerous, in 2 rows per locule, lenticular to narrowly ovoid to narrowly obovoid, finely pitted, with ± pronounced membranous margin when immature.</text>
      <biological_entity id="o621" name="row" name_original="rows" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" />
      </biological_entity>
      <biological_entity id="o622" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <biological_entity id="o623" name="margin" name_original="margin" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="more or less" name="prominence" src="d0_s15" value="pronounced" />
        <character is_modifier="true" name="texture" src="d0_s15" value="membranous" />
      </biological_entity>
      <relation from="o620" id="r144" name="in" negation="false" src="d0_s15" to="o621" />
      <relation from="o621" id="r145" name="per" negation="false" src="d0_s15" to="o622" />
      <relation from="o620" id="r146" modifier="when immature" name="with" negation="false" src="d0_s15" to="o623" />
    </statement>
    <statement id="d0_s16">
      <text>= 7.</text>
      <biological_entity id="o620" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="numerous" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s15" value="lenticular" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s15" value="ovoid" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s15" value="obovoid" />
        <character char_type="range_value" from="lenticular" name="shape" notes="" src="d0_s15" to="narrowly ovoid" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s15" value="pitted" />
        <character name="quantity" src="d0_s16" value="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 16 (16 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chylismia is distinguished from other genera formerly included in Camissonia by straight to arcuate (never twisted or curled) capsules on distinct pedicels and seeds in 2 rows per locule.  R. A. Levin et al. (2004) included only one species each from Camissonia sects. Chylismia and Lignothera; the two formed a moderately supported branch, which led W. L. Wagner et al. (2007) to recognize Chylismia as a distinct genus.  Chylismia is strongly supported in a sister relationship to the realigned Oenothera.  This clade is in turn sister to Eulobus.  Reproductive features include: self-incompatible (C. brevipes, C. claviformis, C. multijuga, C. munzii, and probably C. confertiflora, C. eastwoodiae, and C. parryi; P. H. Raven 1962, 1969) or self-compatible; flowers diurnal, outcrossing and pollinated by mostly oligolectic bees or autogamous, or opening one to two hours before sunset (in one subspecies of C. claviformis and the two species of sect. Lignothera); the evening-opening subspecies of C. claviformis pol­linated mostly by oligolectic bees and moths, C. cardiophylla mainly by small moths, and C. arenaria, with its long floral tubes, by hawkmoths (E. G. Linsley et al. 1963, 1963b, 1964).  Most species are diploid (2n = 14) but there are occasional tetraploids (2n = 28); floating translocations are relatively common (Raven 1962, 1969).</discussion>
  <references>
    <reference>Raven, P. H.  1962.  The systematics of Oenothera subgenus Chylismia.  Univ. Calif. Publ. Bot. 34: 1–122.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Floral tubes 0.4–9 mm; pollen shed singly; leaves basal and cauline, usually with well-developed basal rosettes, blades usually pinnately or bipinnately lobed, lateral lobes sometimes greatly reduced or absent; plants usually annual, sometimes perennial, rarely biennial.</description>
      <determination>16a. Chylismia sect. Chylismia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Floral tubes 4.5–40 mm; pollen shed in tetrads; leaves cauline, blades unlobed; plants usually perennial, sometimes annual.</description>
      <determination>16b. Chylismia sect. Lignothera</determination>
    </key_statement>
  </key>
</bio:treatment>