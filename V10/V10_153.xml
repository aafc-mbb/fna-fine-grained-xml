<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Oenothera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subsection">Oenothera</taxon_name>
    <taxon_name authority="Mackenzie" date="1904" rank="species">argillicola</taxon_name>
    <place_of_publication>
      <publication_title>Torreya</publication_title>
      <place_in_publication>4: 56.  1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section oenothera;subsection oenothera;species argillicola</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">argillicola</taxon_name>
    <taxon_name authority="Core &amp; H. A. Davis" date="unknown" rank="variety">pubescens</taxon_name>
    <taxon_hierarchy>genus oenothera;species argillicola;variety pubescens</taxon_hierarchy>
  </taxon_identification>
  <number>78.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs biennial or short-lived perennial, strigillose and sparsely to moderately villous, hairs sometimes pustulate, pustules with green or red bases, inflorescence glabrous or sparsely glandular puberulent, sometimes also sparsely villous.</text>
      <biological_entity id="o1740" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigillose" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s0" value="villous" />
      </biological_entity>
      <biological_entity id="o1741" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s0" value="pustulate" />
      </biological_entity>
      <biological_entity id="o1742" name="pustule" name_original="pustules" src="d0_s0" type="structure" />
      <biological_entity id="o1743" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="green" />
        <character is_modifier="true" name="coloration" src="d0_s0" value="red" />
      </biological_entity>
      <biological_entity id="o1744" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
        <character is_modifier="false" modifier="sometimes; sparsely" name="pubescence" src="d0_s0" value="villous" />
      </biological_entity>
      <relation from="o1742" id="r399" name="with" negation="false" src="d0_s0" to="o1743" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, green or red, unbranched or with branches obliquely arising from rosette or in distal 1/2 of main-stem.</text>
      <biological_entity id="o1745" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="erect" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="ascending" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="with branches" />
      </biological_entity>
      <biological_entity id="o1746" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character constraint="from rosette or in " constraintid="o1747" is_modifier="false" modifier="obliquely" name="orientation" src="d0_s1" value="arising" />
      </biological_entity>
      <biological_entity id="o1747" name="main-stem" name_original="main-stem" src="d0_s1" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s1" value="distal" />
        <character is_modifier="true" name="quantity" src="d0_s1" value="1/2" />
      </biological_entity>
      <relation from="o1745" id="r400" name="with" negation="false" src="d0_s1" to="o1746" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in a basal rosette and cauline, basal 7–25 × 0.7–2 cm, cauline 6–13 × 0.4–1 cm;</text>
      <biological_entity id="o1748" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s2" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1749" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o1750" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o1751" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="13" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s2" to="1" to_unit="cm" />
      </biological_entity>
      <relation from="o1748" id="r401" name="in" negation="false" src="d0_s2" to="o1749" />
    </statement>
    <statement id="d0_s3">
      <text>blade dark green, somewhat glossy, very narrowly oblanceolate to narrowly oblanceolate, linear-elliptic, lanceolate, or nearly linear, margins flat, entire or remotely and bluntly dentate, sometimes with larger teeth near base;</text>
      <biological_entity id="o1752" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark green" />
        <character is_modifier="false" modifier="somewhat" name="reflectance" src="d0_s3" value="glossy" />
        <character is_modifier="false" modifier="very narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="linear-elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" modifier="nearly" name="shape" notes="[duplicate value]" src="d0_s3" value="linear" />
        <character char_type="range_value" from="very narrowly oblanceolate" name="shape" src="d0_s3" to="narrowly oblanceolate linear-elliptic lanceolate or nearly linear" />
        <character is_modifier="false" modifier="very narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="linear-elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" modifier="nearly" name="shape" notes="[duplicate value]" src="d0_s3" value="linear" />
        <character char_type="range_value" from="very narrowly oblanceolate" name="shape" src="d0_s3" to="narrowly oblanceolate linear-elliptic lanceolate or nearly linear" />
        <character is_modifier="false" modifier="very narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="linear-elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" modifier="nearly" name="shape" notes="[duplicate value]" src="d0_s3" value="linear" />
        <character char_type="range_value" from="very narrowly oblanceolate" name="shape" src="d0_s3" to="narrowly oblanceolate linear-elliptic lanceolate or nearly linear" />
        <character is_modifier="false" modifier="very narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="linear-elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" modifier="nearly" name="shape" notes="[duplicate value]" src="d0_s3" value="linear" />
        <character char_type="range_value" from="very narrowly oblanceolate" name="shape" src="d0_s3" to="narrowly oblanceolate linear-elliptic lanceolate or nearly linear" />
      </biological_entity>
      <biological_entity id="o1753" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" />
        <character name="architecture_or_shape" src="d0_s3" value="remotely" />
        <character is_modifier="false" modifier="bluntly" name="architecture_or_shape" src="d0_s3" value="dentate" />
      </biological_entity>
      <biological_entity constraint="larger" id="o1754" name="tooth" name_original="teeth" src="d0_s3" type="structure" />
      <biological_entity id="o1755" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o1753" id="r402" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o1754" />
      <relation from="o1754" id="r403" name="near" negation="false" src="d0_s3" to="o1755" />
    </statement>
    <statement id="d0_s4">
      <text>bracts persistent.</text>
      <biological_entity id="o1756" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences curved with ascending tip, unbranched.</text>
      <biological_entity id="o1757" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character constraint="with tip" constraintid="o1758" is_modifier="false" name="course" src="d0_s5" value="curved" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="unbranched" />
      </biological_entity>
      <biological_entity id="o1758" name="tip" name_original="tip" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers opening near sunset;</text>
      <biological_entity id="o1759" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1760" name="sunset" name_original="sunset" src="d0_s6" type="structure" />
      <relation from="o1759" id="r404" name="opening near" negation="false" src="d0_s6" to="o1760" />
    </statement>
    <statement id="d0_s7">
      <text>buds erect, 4–8 mm diam., with free tips subterminal, divergent and hornlike, 3–9 mm;</text>
      <biological_entity id="o1761" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s7" value="divergent" />
        <character is_modifier="false" name="shape" src="d0_s7" value="hornlike" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1762" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
        <character is_modifier="false" name="position" src="d0_s7" value="subterminal" />
      </biological_entity>
      <relation from="o1761" id="r405" name="with" negation="false" src="d0_s7" to="o1762" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube 32–52 mm;</text>
      <biological_entity id="o1763" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="32" from_unit="mm" name="some_measurement" src="d0_s8" to="52" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals yellowish green to yellow, sometimes flushed with red, especially at apex, 27–38 mm;</text>
      <biological_entity id="o1764" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s9" value="yellowish green" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s9" value="yellow" />
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s9" to="yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="flushed with red" />
        <character char_type="range_value" from="27" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="38" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1765" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <relation from="o1764" id="r406" modifier="especially" name="at" negation="false" src="d0_s9" to="o1765" />
    </statement>
    <statement id="d0_s10">
      <text>petals yellow to pale-yellow, fading pale-yellow to pale yellowish orange, very broadly obcordate or obovate, 25–42 mm;</text>
      <biological_entity id="o1766" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="yellow" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="pale-yellow" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="fading pale-yellow" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="pale yellowish" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="pale-yellow fading pale-yellow" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="yellow" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="pale-yellow" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="fading pale-yellow" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="pale yellowish" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="pale-yellow fading pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" />
        <character is_modifier="false" modifier="very broadly" name="shape" src="d0_s10" value="obcordate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s10" to="42" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 20–27 mm, anthers 9–13 mm, pollen 90–100% fertile;</text>
      <biological_entity id="o1767" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s11" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1768" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1769" name="pollen" name_original="pollen" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="90-100%" name="reproduction" src="d0_s11" value="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 60–85 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o1770" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" from_unit="mm" name="some_measurement" src="d0_s12" to="85" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1771" name="stigma" name_original="stigma" src="d0_s12" type="structure" />
      <biological_entity id="o1772" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o1771" id="r407" name="exserted beyond" negation="false" src="d0_s12" to="o1772" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules spreading at nearly a right angle to stem, curved upward, sometimes secund, dull green or rusty brown when dry, narrowly lanceoloid to lanceoloid, 20–40 × 4–6 mm, free tips of valves 1–2 mm.</text>
      <biological_entity id="o1773" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character constraint="at angle" constraintid="o1774" is_modifier="false" name="orientation" src="d0_s13" value="spreading" />
        <character is_modifier="false" name="course" notes="" src="d0_s13" value="curved" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s13" value="secund" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dull" />
        <character is_modifier="false" modifier="when dry" name="coloration" src="d0_s13" value="green" />
        <character is_modifier="false" modifier="when dry" name="coloration" src="d0_s13" value="rusty brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s13" value="lanceoloid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="lanceoloid" />
        <character char_type="range_value" from="narrowly lanceoloid" name="shape" src="d0_s13" to="lanceoloid" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s13" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1774" name="angle" name_original="angle" src="d0_s13" type="structure" />
      <biological_entity id="o1775" name="stem" name_original="stem" src="d0_s13" type="structure" />
      <biological_entity id="o1776" name="tip" name_original="tips" src="d0_s13" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s13" value="free" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1777" name="valve" name_original="valves" src="d0_s13" type="structure" />
      <relation from="o1774" id="r408" name="to" negation="false" src="d0_s13" to="o1775" />
      <relation from="o1776" id="r409" name="part_of" negation="false" src="d0_s13" to="o1777" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds 1.3–1.9 × 0.7–1.1 mm. 2n = 14.</text>
      <biological_entity id="o1778" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s14" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s14" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1779" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera argillicola is one of eight angiosperm species restricted to the Devonian Brallier shale barrens, but among them only O. argillicola and Trifolium virginicum occur throughout the shale barren region.  Oenothera argillicola has plastome V and a CC genome composition.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites on Devonian Brallier shale slopes, barrens, outcrops or adjacent roadsides in mid-Appalachian Allegheny Mountains.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sites" constraint="on devonian" />
        <character name="habitat" value="shale slopes" />
        <character name="habitat" value="barrens" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="adjacent roadsides" constraint="in mid-appalachian allegheny mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md., Pa., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>