<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POLYGALA</taxon_name>
    <taxon_name authority="Vellozo" date="1831" rank="species">appendiculata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Flumin.,</publication_title>
      <place_in_publication>292.  1829</place_in_publication>
      <publication_title>plate</publication_title>
      <place_in_publication>66.  1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species appendiculata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygala</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">leptocaulis</taxon_name>
    <taxon_hierarchy>genus polygala;species leptocaulis</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Swamp milkwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, single-stemmed, (1–) 2–5 (–6) dm, branched distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>from taproot, usually quickly becoming fibrous-root cluster.</text>
      <biological_entity id="o1492" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="single-stemmed" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s0" value="branched" />
      </biological_entity>
      <biological_entity id="o1493" name="fibrou-root" name_original="fibrous-root" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, glabrous.</text>
      <biological_entity id="o1494" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or subsessile;</text>
      <biological_entity id="o1495" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear to subfiliform, 8–25 × 0.5–1 mm, base obtuse to cuneate, apex acute to acuminate, surfaces glabrous.</text>
      <biological_entity id="o1496" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="subfiliform" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="subfiliform" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1497" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="obtuse" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="cuneate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o1498" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="acuminate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o1499" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes cylindric, 1.5–13 × 0.5–0.6 cm;</text>
      <biological_entity id="o1500" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="13" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s6" to="0.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle 0.5–1 cm;</text>
      <biological_entity id="o1501" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts deciduous, narrowly lanceolate-ovate.</text>
      <biological_entity id="o1502" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="lanceolate-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0.8–1 mm, glabrous.</text>
      <biological_entity id="o1503" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers usually purplish-pink or lavender-pink, rarely white, 1.6–2.2 mm;</text>
      <biological_entity id="o1504" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="purplish-pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="lavender-pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="white" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s10" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals ovate to narrowly lanceolate-ovate, 0.6–1 mm;</text>
      <biological_entity id="o1505" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="ovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s11" value="lanceolate-ovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="narrowly lanceolate-ovate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>wings obovate or elliptic, 1.5–2 × 0.8–1 mm, apex obtuse to bluntly rounded;</text>
      <biological_entity id="o1506" name="wing" name_original="wings" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1507" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="obtuse" />
        <character is_modifier="false" modifier="bluntly" name="shape" notes="[duplicate value]" src="d0_s12" value="rounded" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="bluntly rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keel 1.5–2 mm, crest 2-parted, with 2–3 lobes on each side.</text>
      <biological_entity id="o1508" name="keel" name_original="keel" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1509" name="crest" name_original="crest" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="2-parted" />
      </biological_entity>
      <biological_entity id="o1510" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s13" to="3" />
      </biological_entity>
      <biological_entity id="o1511" name="side" name_original="side" src="d0_s13" type="structure" />
      <relation from="o1509" id="r192" name="with" negation="false" src="d0_s13" to="o1510" />
      <relation from="o1510" id="r193" name="on" negation="false" src="d0_s13" to="o1511" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules oblong to ellipsoid, 1.4–2 × 0.7–1.1 mm, margins not winged.</text>
      <biological_entity id="o1512" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="oblong" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="ellipsoid" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s14" to="ellipsoid" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s14" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s14" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1513" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1–1.2 mm, pubescent;</text>
      <biological_entity id="o1514" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>aril 0.1 (–0.2) mm, lobes less than 1/8 length of seed.</text>
      <biological_entity id="o1515" name="aril" name_original="aril" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="0.2" to_unit="mm" />
        <character name="some_measurement" src="d0_s16" unit="mm" value="0.1" />
      </biological_entity>
      <biological_entity id="o1516" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character char_type="range_value" from="0 length of seed" name="length" src="d0_s16" to="1/8 length of seed" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Polygala appendiculata is part of a widespread complex in Latin America, potentially representing more than one evolutionary lineage.  Even if found to be a single lineage, it is one with a complicated nomenclatural history (J. F. B. Pastore 2013).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Savannas, pastures, bogs, open pine woods, pond margins.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="savannas" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="open pine woods" />
        <character name="habitat" value="margins" modifier="pond" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., La., Miss., Tex.; Mexico (Campeche, México, Michoacán, Tabasco, Veracruz); West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Campeche)" establishment_means="native" />
        <character name="distribution" value="Mexico (México)" establishment_means="native" />
        <character name="distribution" value="Mexico (Michoacán)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tabasco)" establishment_means="native" />
        <character name="distribution" value="Mexico (Veracruz)" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>