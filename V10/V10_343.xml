<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Eucharidium</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>20: 359. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section eucharidium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Fischer &amp; C. A. Meyer" date=" 1836" rank="genus">Eucharidium</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (St. Petersburg)</publication_title>
      <place_in_publication>2: 36.  1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus eucharidium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clarkia</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) Jepson" date="unknown" rank="subgenus">Eucharidium</taxon_name>
    <taxon_hierarchy>genus clarkia;subgenus eucharidium</taxon_hierarchy>
  </taxon_identification>
  <number>6a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences: axis suberect or slightly recurved;</text>
      <biological_entity id="o5900" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
      <biological_entity id="o5901" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="suberect" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s0" value="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>buds pendent.</text>
      <biological_entity id="o5902" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o5903" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: floral-tube narrowly tubular, 13–35 mm;</text>
      <biological_entity id="o5904" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o5905" name="floral-tube" name_original="floral-tube" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="tubular" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s2" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals reflexed together to 1 side;</text>
      <biological_entity id="o5906" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o5907" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="together" name="orientation" src="d0_s3" value="reflexed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="1" />
      </biological_entity>
      <biological_entity id="o5908" name="side" name_original="side" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petals pink, sometimes white-streaked, fan-shaped, conspicuously 3-lobed, middle lobe often longer than laterals, claw slender, not lobed;</text>
      <biological_entity id="o5909" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o5910" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pink" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="white-streaked" />
        <character is_modifier="false" name="shape" src="d0_s4" value="fan--shaped" value_original="fan-shaped" />
        <character is_modifier="false" modifier="conspicuously" name="shape" src="d0_s4" value="3-lobed" />
      </biological_entity>
      <biological_entity constraint="middle" id="o5911" name="lobe" name_original="lobe" src="d0_s4" type="structure">
        <character constraint="than laterals" constraintid="o5912" is_modifier="false" name="length_or_size" src="d0_s4" value="often longer" />
      </biological_entity>
      <biological_entity id="o5912" name="lateral" name_original="laterals" src="d0_s4" type="structure" />
      <biological_entity id="o5913" name="claw" name_original="claw" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="slender" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens 4.</text>
      <biological_entity id="o5914" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o5915" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsules subterete;</text>
    </statement>
    <statement id="d0_s7">
      <text>sessile or subsessile.</text>
      <biological_entity id="o5916" name="capsule" name_original="capsules" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subterete" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="California." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Section Eucharidium includes two species characterized by large tri-lobed petals, four rather than eight stamens, and a long floral tube that adapts them to pollination by long-tongued Lepidoptera or Diptera (G. A. Allen et al. 1990).</discussion>
  
</bio:treatment>