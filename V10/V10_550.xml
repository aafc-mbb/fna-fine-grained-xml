<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LYTHRUM</taxon_name>
    <taxon_name authority="Salzmann ex Sprengel" date="1827" rank="species">tribracteatum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Veg.</publication_title>
      <place_in_publication>4(2): 190.  1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus lythrum;species tribracteatum</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, slender, 0.5–3 dm, green, glabrous or sparsely, minutely glandular-hispid.</text>
      <biological_entity id="o3857" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character name="pubescence" src="d0_s0" value="sparsely" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s0" value="glandular-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to weakly erect, often branched near base, with rela­tively short accessory branches distally.</text>
      <biological_entity id="o3858" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="prostrate" />
        <character is_modifier="false" modifier="weakly" name="orientation" notes="[duplicate value]" src="d0_s1" value="erect" />
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="weakly erect" />
        <character constraint="near base" constraintid="o3859" is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="branched" />
      </biological_entity>
      <biological_entity id="o3859" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o3860" name="relum" name_original="rela" src="d0_s1" type="structure" />
      <biological_entity constraint="accessory" id="o3861" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="tively" name="height_or_length_or_size" src="d0_s1" value="short" />
      </biological_entity>
      <relation from="o3858" id="r464" name="with" negation="false" src="d0_s1" to="o3860" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves op­posite prox­imally, subalter­nate distally;</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity id="o3862" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblong to oblan­ceolate, 3–25 × 0.1–0.5 mm, base attenuate.</text>
      <biological_entity id="o3863" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3864" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences spikelike.</text>
      <biological_entity id="o3865" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers alternate, often crowded on branches, sessile or subsessile, mono­-stylous;</text>
      <biological_entity id="o3866" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="alternate" />
        <character constraint="on branches" constraintid="o3867" is_modifier="false" modifier="often" name="arrangement" src="d0_s6" value="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="­-stylous" />
      </biological_entity>
      <biological_entity id="o3867" name="branch" name_original="branches" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>floral-tube without red spots, narrowly cylindrical, 4–6 × 0.4–0.5 mm;</text>
      <biological_entity id="o3868" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="without red spots; narrowly" name="shape" src="d0_s7" value="cylindrical" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>epicalyx segments equal to length of sepals;</text>
      <biological_entity constraint="epicalyx" id="o3869" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character constraint="to " constraintid="o3870" is_modifier="false" name="variability" src="d0_s8" value="equal" />
      </biological_entity>
      <biological_entity id="o3870" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals obtuse, thick;</text>
      <biological_entity id="o3871" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" />
        <character is_modifier="false" name="width" src="d0_s9" value="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals lavender, oblong, 1–2 (–3) × 0.5 mm;</text>
      <biological_entity id="o3872" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration_or_odor" src="d0_s10" value="lavender" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>nectary absent;</text>
      <biological_entity id="o3873" name="nectary" name_original="nectary" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4–6.</text>
      <biological_entity id="o3874" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s12" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules septicidal or septifragal.</text>
      <biological_entity id="o3875" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="septifragal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds ca. 11, subglobose.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 10.</text>
      <biological_entity id="o3876" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="11" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subglobose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3877" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>First collected in Solano County, California, in 1930 (J. T. Howell 5208, UC), Lythrum tribracteatum has subsequently been documented in 12 counties in California and in southwestern Idaho and Utah.  In Utah the species may have been introduced through contamination of seed stock of the cultivated dye-plant indigo (Indigofera).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally wet areas, drying ponds, vernal pools, ditches.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet areas" modifier="seasonally" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="vernal pools" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Idaho, Utah; s Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>