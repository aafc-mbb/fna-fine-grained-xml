<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MELASTOMATACEAE</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">RHEXIA</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">petiolata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>130.  1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family melastomataceae;genus rhexia;species petiolata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhexia</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">ciliosa</taxon_name>
    <taxon_hierarchy>genus rhexia;species ciliosa</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Fringed or short-stemmed meadow beauty</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices developed;</text>
      <biological_entity id="o2232" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" name="development" src="d0_s0" value="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots short, fibrous, lignescent, non-tuberiferous.</text>
      <biological_entity id="o2233" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" />
        <character is_modifier="false" name="texture" src="d0_s1" value="lignescent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="non-tuberiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unbranched or few-branched, 10–50 cm, faces subequal, flat to convex, angles weakly ridged, internodes glabrous, nodes sparsely hir­sute, hairs eglandular.</text>
      <biological_entity id="o2234" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="few-branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2235" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="subequal" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="flat" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="convex" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s2" to="convex" />
      </biological_entity>
      <biological_entity id="o2236" name="angle" name_original="angles" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="ridged" />
      </biological_entity>
      <biological_entity id="o2237" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" />
      </biological_entity>
      <biological_entity id="o2238" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o2239" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 0.5–1.5 mm;</text>
      <biological_entity id="o2240" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2241" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to short-elliptic or suborbiculate, 1–2 cm × 4–14 mm, margins serrate, surfaces glabrous abaxially, sparsely villous adaxially.</text>
      <biological_entity id="o2242" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2243" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="short-elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="suborbiculate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="short-elliptic or suborbiculate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2244" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" />
      </biological_entity>
      <biological_entity id="o2245" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s4" value="glabrous" />
        <character is_modifier="false" modifier="sparsely; adaxially" name="pubescence" src="d0_s4" value="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences condensed, mostly obscured by foliaceous bracts.</text>
      <biological_entity id="o2246" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="condensed" />
        <character constraint="by bracts" constraintid="o2247" is_modifier="false" modifier="mostly" name="prominence" src="d0_s5" value="obscured" />
      </biological_entity>
      <biological_entity id="o2247" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium globose, much longer than the constricted neck, 5–7 (–9) mm, mostly glabrous except along calyx lobes;</text>
      <biological_entity id="o2248" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2249" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="globose" />
        <character constraint="than the constricted neck" constraintid="o2250" is_modifier="false" name="length_or_size" src="d0_s6" value="much longer" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character constraint="except along lobes" constraintid="o2251" is_modifier="false" modifier="mostly" name="pubescence" src="d0_s6" value="glabrous" />
      </biological_entity>
      <biological_entity id="o2250" name="neck" name_original="neck" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="constricted" />
      </biological_entity>
      <biological_entity id="o2251" name="lobe" name_original="lobes" src="d0_s6" type="structure" constraint="calyx" />
    </statement>
    <statement id="d0_s7">
      <text>calyx lobes oblong-lanceolate, apices acute, spreading-ciliate, eglandular;</text>
      <biological_entity id="o2252" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o2253" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o2254" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="spreading-ciliate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals ascending to divergent, lavender-rose, 1–2 cm;</text>
      <biological_entity id="o2255" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2256" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="lavender-rose" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers straight, 1.2–1.8 mm.</text>
      <biological_entity id="o2257" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2258" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 0.6 mm, surfaces pebbled or with ridges of domelike processes.</text>
      <biological_entity id="o2259" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.6" />
      </biological_entity>
      <biological_entity id="o2261" name="ridge" name_original="ridges" src="d0_s10" type="structure" />
      <biological_entity id="o2262" name="process" name_original="processes" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="domelike" />
      </biological_entity>
      <relation from="o2260" id="r484" name="with" negation="false" src="d0_s10" to="o2261" />
      <relation from="o2261" id="r485" name="part_of" negation="false" src="d0_s10" to="o2262" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 22.</text>
      <biological_entity id="o2260" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="pebbled" />
        <character is_modifier="false" name="relief" src="d0_s10" value="with ridges" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2263" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet pine flatwoods and savannas, pine-cypress flats, cypress-pine-gum flats, cabbage palm hummocks, hillside bogs, swales, swamp and pocosin borders, borrow pits, ditches, roadsides, disturbed sites, sandy peat.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet pine flatwoods" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="pine-cypress flats" />
        <character name="habitat" value="cypress-pine-gum flats" />
        <character name="habitat" value="cabbage palm hummocks" />
        <character name="habitat" value="hillside bogs" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="pocosin borders" modifier="swamp and" />
        <character name="habitat" value="pits" modifier="borrow" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="sandy peat" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Md., Miss., N.C., S.C., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>