<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) W. L. Wagner &amp; Hoch" date="2007" rank="section">Anogra</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont in J. C. Frémont" date="1845" rank="species">deltoides</taxon_name>
    <taxon_name authority="(Munz) W. M. Klein" date="1962" rank="subspecies">howellii</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>5: 180. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section anogra;species deltoides;subspecies howellii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">deltoides</taxon_name>
    <taxon_name authority="Munz" date="1949" rank="variety">howellii</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>2: 81.  1949</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species deltoides;variety howellii</taxon_hierarchy>
  </taxon_identification>
  <number>66b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, densely strigillose, also villous and glandular puberulent distally;</text>
    </statement>
    <statement id="d0_s1">
      <text>with relatively long, fleshy roots, grayish.</text>
      <biological_entity id="o949" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
      <biological_entity id="o950" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="relatively" name="length_or_size" src="d0_s1" value="long" />
        <character is_modifier="true" name="texture" src="d0_s1" value="fleshy" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="grayish" />
      </biological_entity>
      <relation from="o949" id="r241" name="with" negation="false" src="d0_s1" to="o950" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending, several from base, not thickened near base, with numerous shorter lateral branches, these not encircling stems in older plants, 40–80 cm.</text>
      <biological_entity id="o951" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" />
        <character constraint="from base" constraintid="o952" is_modifier="false" name="quantity" src="d0_s2" value="several" />
        <character constraint="near base" constraintid="o953" is_modifier="false" modifier="not" name="size_or_width" notes="" src="d0_s2" value="thickened" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="80" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o952" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o953" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="shorter lateral" id="o954" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="numerous" />
      </biological_entity>
      <biological_entity id="o955" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o956" name="plant" name_original="plants" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" />
      </biological_entity>
      <relation from="o951" id="r242" name="with" negation="false" src="d0_s2" to="o954" />
      <relation from="o951" id="r243" name="encircling" negation="true" src="d0_s2" to="o955" />
      <relation from="o951" id="r244" name="in" negation="false" src="d0_s2" to="o956" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, without clear basal rosette, at least at anthesis;</text>
      <biological_entity id="o957" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o958" name="rosette" name_original="rosette" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="clear" />
      </biological_entity>
      <relation from="o957" id="r245" modifier="at anthesis" name="without" negation="false" src="d0_s3" to="o958" />
    </statement>
    <statement id="d0_s4">
      <text>blade lanceolate, margins runcinate-pinnatifid.</text>
      <biological_entity id="o959" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" />
      </biological_entity>
      <biological_entity id="o960" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="runcinate-pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: buds fluted in distal 1/2, with free tips 1–9 mm;</text>
      <biological_entity id="o961" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o962" name="bud" name_original="buds" src="d0_s5" type="structure">
        <character constraint="in distal 1/2" constraintid="o963" is_modifier="false" name="shape" src="d0_s5" value="fluted" />
      </biological_entity>
      <biological_entity constraint="distal" id="o963" name="1/2" name_original="1/2" src="d0_s5" type="structure" />
      <biological_entity id="o964" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s5" value="free" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
      <relation from="o962" id="r246" name="with" negation="false" src="d0_s5" to="o964" />
    </statement>
    <statement id="d0_s6">
      <text>sepals 20–30 mm;</text>
      <biological_entity id="o965" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o966" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 20–40 mm.</text>
      <biological_entity id="o967" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o968" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 45–60 × 3–4 mm. 2n = 14.</text>
      <biological_entity id="o969" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="45" from_unit="mm" name="length" src="d0_s8" to="60" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o970" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>W. M. Klein (1964) determined subsp. howellii to be self-incompatible.</discussion>
  <discussion>Subspecies howellii is known from the Antioch Dunes in Contra Costa County.  Subspecies howellii is federally listed as endangered and is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Jun–Jul(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand dunes and bluffs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>