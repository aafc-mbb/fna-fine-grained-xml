<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) W. L. Wagner &amp; Hoch" date="2007" rank="section">Calylophus</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) W. L. Wagner &amp; Hoch" date="2007" rank="subsection">Salpingia</taxon_name>
    <taxon_name authority="Bentham" date="1839" rank="species">hartwegii</taxon_name>
    <taxon_name authority="(A. Gray) W. L. Wagner &amp; Hoch" date="2007" rank="subspecies">fendleri</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 212. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section calylophus;subsection salpingia;species hartwegii;subspecies fendleri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="species">fendleri</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 45.  1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species fendleri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calylophus</taxon_name>
    <taxon_name authority="(Bentham) P. H. Raven" date="unknown" rank="species">hartwegii</taxon_name>
    <taxon_name authority="(A. Gray) Towner &amp; P. H. Raven" date="unknown" rank="subspecies">fendleri</taxon_name>
    <taxon_hierarchy>genus calylophus;species hartwegii;subspecies fendleri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Galpinsia</taxon_name>
    <taxon_name authority="(A. Gray) A. Heller" date="unknown" rank="species">fendleri</taxon_name>
    <taxon_hierarchy>genus galpinsia;species fendleri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="(Bentham) Britton" date="unknown" rank="species">hartwegii</taxon_name>
    <taxon_name authority="(A. Gray) Small" date="unknown" rank="variety">fendleri</taxon_name>
    <taxon_hierarchy>genus g.;species hartwegii;variety fendleri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hartwegii</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="variety">fendleri</taxon_name>
    <taxon_hierarchy>genus o.;species hartwegii;variety fendleri</taxon_hierarchy>
  </taxon_identification>
  <number>8d.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs usually glabrous throughout, sometimes glan­dular puberulent on distal parts, especially on ovaries.</text>
      <biological_entity id="o3835" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually; throughout" name="pubescence" src="d0_s0" value="glabrous" />
        <character constraint="on distal parts" constraintid="o3836" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="puberulent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3836" name="part" name_original="parts" src="d0_s0" type="structure" />
      <biological_entity id="o3837" name="ovary" name_original="ovaries" src="d0_s0" type="structure" />
      <relation from="o3835" id="r754" modifier="especially" name="on" negation="false" src="d0_s0" to="o3837" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 1–5 × 0.15–1 cm, fascicles of small leaves to 1 cm (when present);</text>
      <biological_entity id="o3838" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.15" from_unit="cm" name="width" src="d0_s1" to="1" to_unit="cm" />
        <character constraint="of leaves" constraintid="o3839" is_modifier="false" name="arrangement" src="d0_s1" value="fascicles" />
      </biological_entity>
      <biological_entity id="o3839" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear to oblanceolate or lance­olate, base attenuate to obtuse, rarely nearly clasping, margins entire or subentire, rarely undulate.</text>
      <biological_entity id="o3840" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="oblanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="oblanceolate" />
        <character name="shape" src="d0_s2" value="lance" />
      </biological_entity>
      <biological_entity id="o3841" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="attenuate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="obtuse" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s2" to="obtuse" />
        <character is_modifier="false" modifier="rarely nearly" name="architecture_or_fixation" src="d0_s2" value="clasping" />
      </biological_entity>
      <biological_entity id="o3842" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subentire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: buds with free tips 0.5–3 mm;</text>
      <biological_entity id="o3843" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o3844" name="bud" name_original="buds" src="d0_s3" type="structure" />
      <biological_entity id="o3845" name="tip" name_original="tips" src="d0_s3" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s3" value="free" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o3844" id="r755" name="with" negation="false" src="d0_s3" to="o3845" />
    </statement>
    <statement id="d0_s4">
      <text>floral-tube 30–50 mm;</text>
      <biological_entity id="o3846" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o3847" name="floral-tube" name_original="floral-tube" src="d0_s4" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 9–28 mm;</text>
      <biological_entity id="o3848" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o3849" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="28" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 10–30 mm;</text>
      <biological_entity id="o3850" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3851" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 5–12 mm, anthers 5–13 mm;</text>
      <biological_entity id="o3852" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3853" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3854" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style 40–75 mm. 2n = 14.</text>
      <biological_entity id="o3855" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3856" name="style" name_original="style" src="d0_s8" type="structure">
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s8" to="75" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3857" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies fendleri is known from Barber, Comanche, and Morton counties, Kansas, south through western Oklahoma and scattered sites in the Texas Panhandle to eastern Chihuahua, central trans-Pecos Texas, central and western New Mexico, and east-central Arizona.  It is the most distinctive subspecies in the complex.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>In scattered populations on clay or gravelly soil, sometimes calcareous, in grasslands, often with Juniperus and Prosopis, to woodlands with Juniperus, Pinus edulis, sometimes Pinus ponderosa.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="scattered populations" modifier="in" constraint="on clay or gravelly soil" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="gravelly soil" />
        <character name="habitat" value="calcareous" />
        <character name="habitat" value="grasslands" modifier="in" />
        <character name="habitat" value="juniperus" modifier="often with" />
        <character name="habitat" value="prosopis" />
        <character name="habitat" value="to woodlands" constraint="with juniperus" />
        <character name="habitat" value="juniperus" />
        <character name="habitat" value="pinus edulis" />
        <character name="habitat" value="ponderosa" modifier="sometimes pinus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Kans., N.Mex., Okla., Tex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>