<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) Walpers" date="1843" rank="section">Pachylophus</taxon_name>
    <taxon_name authority="Munz" date="1941" rank="species">cavernae</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>3: 50.  1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section pachylophus;species cavernae</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs winter or spring annual, acaulescent or short-caulescent, glandular puberulent, some­times also sparsely hirsute;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a taproot.</text>
      <biological_entity id="o3470" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="season" src="d0_s0" value="winter" />
        <character is_modifier="false" name="season" src="d0_s0" value="spring" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-caulescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (when present) 1–several, ascending, usually unbranched, 2–4 cm.</text>
      <biological_entity id="o3471" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="unbranched" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves primarily in a basal rosette, sometimes also cauline, (0.5–) 2.5–13 (–19.5) × (0.2–) 0.6–2.3 (–2.7) cm;</text>
      <biological_entity id="o3472" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="position" notes="" src="d0_s3" value="cauline" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s3" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="19.5" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s3" to="13" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s3" to="0.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="2.7" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s3" to="2.3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3473" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o3472" id="r703" name="in" negation="false" src="d0_s3" to="o3473" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–5.2 cm;</text>
      <biological_entity id="o3474" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="5.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblanceolate to elliptic-oblanceo­late (in some exceptionally large leaves), margins lyrate-pinnatifid to subentire (in very small ones), apex usually rounded, rarely acute.</text>
      <biological_entity id="o3475" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" />
      </biological_entity>
      <biological_entity id="o3476" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="lyrate-pinnatifid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s5" value="subentire" />
        <character char_type="range_value" from="lyrate-pinnatifid" name="shape" src="d0_s5" to="subentire" />
      </biological_entity>
      <biological_entity id="o3477" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="rounded" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1–3 (–10) per stem opening per day near sunset, without noticeable scent;</text>
      <biological_entity id="o3478" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="10" />
        <character char_type="range_value" constraint="per stem" constraintid="o3479" from="1" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o3479" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <biological_entity id="o3480" name="day" name_original="day" src="d0_s6" type="structure" />
      <biological_entity id="o3481" name="sunset" name_original="sunset" src="d0_s6" type="structure" />
      <relation from="o3479" id="r704" name="opening per" negation="false" src="d0_s6" to="o3480" />
      <relation from="o3478" id="r705" name="near" negation="false" src="d0_s6" to="o3481" />
    </statement>
    <statement id="d0_s7">
      <text>buds sometimes ± recurved before anthesis;</text>
      <biological_entity id="o3482" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character constraint="before anthesis" is_modifier="false" modifier="sometimes more or less" name="orientation" src="d0_s7" value="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral-tube (20–) 30–37 (–47) mm;</text>
      <biological_entity id="o3483" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="37" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="47" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s8" to="37" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 4.5–12 mm;</text>
      <biological_entity id="o3484" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, fading pale-pink, (6.5–) 8–20 (–25) mm;</text>
      <biological_entity id="o3485" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading pale-pink" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 5.2–7.5 (–12) mm, anthers (1.4–) 3–4.5 (–6) mm;</text>
      <biological_entity id="o3486" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="5.2" from_unit="mm" name="some_measurement" src="d0_s11" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3487" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style (24–) 35–45 (–56) mm, stigma surrounded by anthers at anthesis.</text>
      <biological_entity id="o3488" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="24" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="56" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s12" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3489" name="stigma" name_original="stigma" src="d0_s12" type="structure" />
      <biological_entity id="o3490" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <relation from="o3489" id="r706" name="surrounded by" negation="false" src="d0_s12" to="o3490" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules falcate (especially before maturity), ellipsoid-ovoid to ovoid, obtusely 4-angled, 12–38 × 6–14 mm, tapering to a sterile beak 2–8 mm, dehis­cent to 1/2 their length, valve margins with a sinuate ridge or 8–20 nearly distinct tubercles;</text>
      <biological_entity id="o3491" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="falcate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ellipsoid-ovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ovoid" />
        <character char_type="range_value" from="ellipsoid-ovoid" name="shape" src="d0_s13" to="ovoid" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s13" value="4-angled" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s13" to="38" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s13" to="14" to_unit="mm" />
        <character constraint="to beak" constraintid="o3492" is_modifier="false" name="shape" src="d0_s13" value="tapering" />
      </biological_entity>
      <biological_entity id="o3492" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="sterile" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="0" name="length" src="d0_s13" to="1/2" />
      </biological_entity>
      <biological_entity constraint="valve" id="o3493" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <biological_entity id="o3494" name="ridge" name_original="ridge" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="sinuate" />
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s13" value="distinct" />
      </biological_entity>
      <biological_entity id="o3495" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="sinuate" />
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s13" value="distinct" />
      </biological_entity>
      <relation from="o3493" id="r707" name="with" negation="false" src="d0_s13" to="o3494" />
      <relation from="o3493" id="r708" name="with" negation="false" src="d0_s13" to="o3495" />
    </statement>
    <statement id="d0_s14">
      <text>pedicel 0–10 mm.</text>
      <biological_entity id="o3496" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds usually numerous, sometimes as few as 5, in 2 adjacent rows per locule, obovoid, 2.5–3.1 × 1.1–1.4 mm, embryo 1/2 of seed volume, surface minutely papillose to reticu­late;</text>
      <biological_entity id="o3497" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s15" value="numerous" />
        <character name="quantity" src="d0_s15" value="5" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="obovoid" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s15" to="3.1" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s15" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3498" name="row" name_original="rows" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" />
        <character is_modifier="true" name="arrangement" src="d0_s15" value="adjacent" />
      </biological_entity>
      <biological_entity id="o3499" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <biological_entity id="o3500" name="embryo" name_original="embryo" src="d0_s15" type="structure">
        <character constraint="of seed" constraintid="o3501" name="quantity" src="d0_s15" value="1/2" />
      </biological_entity>
      <biological_entity id="o3501" name="seed" name_original="seed" src="d0_s15" type="structure" />
      <biological_entity id="o3502" name="surface" name_original="surface" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s15" value="papillose" />
      </biological_entity>
      <relation from="o3497" id="r709" modifier="sometimes" name="in" negation="false" src="d0_s15" to="o3498" />
      <relation from="o3498" id="r710" name="per" negation="false" src="d0_s15" to="o3499" />
    </statement>
    <statement id="d0_s16">
      <text>seed collar without membrane, producing a large empty cavity, margin irregularly sinuate.</text>
      <biological_entity constraint="seed" id="o3503" name="collar" name_original="collar" src="d0_s16" type="structure" />
      <biological_entity id="o3504" name="membrane" name_original="membrane" src="d0_s16" type="structure" />
      <biological_entity id="o3505" name="cavity" name_original="cavity" src="d0_s16" type="structure">
        <character is_modifier="true" name="size" src="d0_s16" value="large" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="empty" />
      </biological_entity>
      <relation from="o3503" id="r711" name="without" negation="false" src="d0_s16" to="o3504" />
      <relation from="o3503" id="r712" name="producing a" negation="false" src="d0_s16" to="o3505" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 14.</text>
      <biological_entity id="o3506" name="margin" name_original="margin" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s16" value="sinuate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3507" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera cavernae is known from the Arrow Canyon, Las Vegas, and Sheep ranges and the low hills near Arden and Sloan in Clark County, Nevada, eastward along the Grand Canyon to the vicinity of Page, Arizona, and perhaps Washington County, Utah and formerly in Glenn Canyon, and more recently collected in eastern San Bernardino County, Cali­fornia (eastern Clark Mountain Range, and the base of range in Ivanpah Valley).  W. L. Wagner et al. (1985) deter­mined O. cavernae to be self-compatible and autogamous.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed calcareous slopes, crevices in limestone, dolomite, or loose talus, sandy arroyos, sandstone, granitic crevices, volcanic cinders in Mojave Desert or Great Basin scrub communities, rarely in arid juniper woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous slopes" modifier="exposed" />
        <character name="habitat" value="crevices" constraint="in limestone" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="dolomite" />
        <character name="habitat" value="loose talus" modifier="or" />
        <character name="habitat" value="sandy arroyos" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="granitic crevices" />
        <character name="habitat" value="volcanic cinders" constraint="in mojave desert or great basin scrub" />
        <character name="habitat" value="mojave desert" />
        <character name="habitat" value="great basin scrub" />
        <character name="habitat" value="arid juniper woodlands" modifier="rarely in" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>