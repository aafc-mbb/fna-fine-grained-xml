<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Endlicher" date="1830" rank="tribe">Epilobieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EPILOBIUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Epilobium</taxon_name>
    <taxon_name authority="Fischer ex Sprengel" date="1818" rank="species">davuricum</taxon_name>
    <place_of_publication>
      <publication_title>Novi Provent.,</publication_title>
      <place_in_publication>44.  1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section epilobium;species davuricum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epilobium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">palustre</taxon_name>
    <taxon_name authority="(Fischer ex Sprengel) S. L. Welsh" date="unknown" rank="variety">davuricum</taxon_name>
    <taxon_hierarchy>genus epilobium;species palustre;variety davuricum</taxon_hierarchy>
  </taxon_identification>
  <number>20.</number>
  <other_name type="common_name">Dahurian willowherb</other_name>
  <other_name type="common_name">épilobe de Daourie</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with basal rosettes of linear leaves 12–40 × 2–5 mm.</text>
      <biological_entity id="o4516" name="herb" name_original="herbs" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o4517" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s0" to="40" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s0" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4518" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s0" value="linear" />
      </biological_entity>
      <relation from="o4516" id="r797" name="with" negation="false" src="d0_s0" to="o4517" />
      <relation from="o4517" id="r798" name="part_of" negation="false" src="d0_s0" to="o4518" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, rarely clumped, terete, 10–40 (–45) cm, usually simple, rarely branched, glabrous proxi­mal to inflorescence with sparsely strigillose raised lines decurrent from margins of petioles, mixed strigillose and glandular puberulent distally.</text>
      <biological_entity id="o4519" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s1" value="clumped" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="branched" />
        <character constraint="to inflorescence" constraintid="o4520" is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" />
        <character constraint="from margins" constraintid="o4522" is_modifier="false" name="shape" src="d0_s1" value="decurrent" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s1" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="puberulent" />
      </biological_entity>
      <biological_entity id="o4520" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure">
        <character constraint="with" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="strigillose" />
      </biological_entity>
      <biological_entity id="o4521" name="line" name_original="lines" src="d0_s1" type="structure" />
      <biological_entity id="o4522" name="margin" name_original="margins" src="d0_s1" type="structure" />
      <biological_entity id="o4523" name="petiole" name_original="petioles" src="d0_s1" type="structure" />
      <relation from="o4519" id="r799" name="raised" negation="false" src="d0_s1" to="o4521" />
      <relation from="o4522" id="r800" name="part_of" negation="false" src="d0_s1" to="o4523" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite and crowded proximal to inflorescence, alternate and scattered distally, sub­sessile;</text>
      <biological_entity id="o4524" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" />
        <character constraint="to inflorescence" constraintid="o4525" is_modifier="false" name="position" src="d0_s2" value="proximal" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s2" value="alternate" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s2" value="scattered" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" />
      </biological_entity>
      <biological_entity id="o4525" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly oblong or narrowly lanceolate to linear, (1.2–) 2–4.5 × 0.1–0.5 cm, base attenuate, margins irregularly denticulate, 2–4 teeth per side, veins inconspicuous, 3 or 4 per side, apex subacute to obtuse or ± truncate, surfaces glabrous with sparsely strigillose margins and adaxial midrib;</text>
      <biological_entity id="o4526" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="linear" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="atypical_length" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4527" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" />
      </biological_entity>
      <biological_entity id="o4528" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s3" value="denticulate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o4529" name="tooth" name_original="teeth" src="d0_s3" type="structure" />
      <biological_entity id="o4530" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o4531" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" />
        <character name="quantity" src="d0_s3" unit="or per" value="3" />
        <character name="quantity" src="d0_s3" unit="or per" value="4" />
      </biological_entity>
      <biological_entity id="o4532" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o4533" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="subacute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="obtuse" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="[duplicate value]" src="d0_s3" value="truncate" />
        <character char_type="range_value" from="subacute" name="shape" src="d0_s3" to="obtuse or more or less truncate" />
      </biological_entity>
      <biological_entity id="o4534" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character constraint="with margins" constraintid="o4535" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" />
      </biological_entity>
      <biological_entity id="o4535" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s3" value="strigillose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4536" name="midrib" name_original="midrib" src="d0_s3" type="structure" />
      <relation from="o4529" id="r801" name="per" negation="false" src="d0_s3" to="o4530" />
    </statement>
    <statement id="d0_s4">
      <text>bracts much reduced and narrower.</text>
      <biological_entity id="o4537" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s4" value="reduced" />
        <character is_modifier="false" name="width" src="d0_s4" value="narrower" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences nodding in bud, suberect later, racemes, simple, mixed strigillose and glandular puberulent, sometimes subglabrous.</text>
      <biological_entity id="o4538" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character constraint="in bud" constraintid="o4539" is_modifier="false" name="orientation" src="d0_s5" value="nodding" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s5" value="suberect" />
        <character is_modifier="false" name="condition" src="d0_s5" value="later" />
      </biological_entity>
      <biological_entity id="o4539" name="bud" name_original="bud" src="d0_s5" type="structure" />
      <biological_entity id="o4540" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect, sometimes starting in third most-proximal node;</text>
      <biological_entity id="o4541" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" />
      </biological_entity>
      <biological_entity constraint="most-proximal" id="o4542" name="node" name_original="node" src="d0_s6" type="structure" />
      <relation from="o4541" id="r802" modifier="sometimes" name="starting in third" negation="false" src="d0_s6" to="o4542" />
    </statement>
    <statement id="d0_s7">
      <text>buds 3–6.5 × 1–2 mm;</text>
      <biological_entity id="o4543" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicel 4–15 mm;</text>
      <biological_entity id="o4544" name="pedicel" name_original="pedicel" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>floral-tube 0.7–1.6 × 1–2.2 mm, with or often without ring of spreading hairs at mouth inside;</text>
      <biological_entity id="o4545" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s9" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4546" name="ring" name_original="ring" src="d0_s9" type="structure" />
      <biological_entity id="o4547" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="spreading" />
      </biological_entity>
      <biological_entity id="o4548" name="mouth" name_original="mouth" src="d0_s9" type="structure" />
      <relation from="o4545" id="r803" modifier="often" name="without" negation="false" src="d0_s9" to="o4546" />
      <relation from="o4546" id="r804" name="part_of" negation="false" src="d0_s9" to="o4547" />
      <relation from="o4546" id="r805" name="at" negation="false" src="d0_s9" to="o4548" />
    </statement>
    <statement id="d0_s10">
      <text>sepals 1.2–3.5 × 0.7–1.2 mm, apex subacute;</text>
      <biological_entity id="o4549" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s10" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4550" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white, 2–5.5 × 1.2–3.1 mm, apical notch 0.3–1 mm;</text>
      <biological_entity id="o4551" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s11" to="3.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o4552" name="notch" name_original="notch" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments white or cream, those of longer stamens 1–2.5 mm, those of shorter ones 0.5–1.4 mm;</text>
      <biological_entity id="o4553" name="filament" name_original="filaments" src="d0_s12" type="structure" constraint="stamen">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="cream" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o4554" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity constraint="shorter" id="o4555" name="one" name_original="ones" src="d0_s12" type="structure" />
      <relation from="o4553" id="r806" name="part_of" negation="false" src="d0_s12" to="o4554" />
      <relation from="o4553" id="r807" name="part_of" negation="false" src="d0_s12" to="o4555" />
    </statement>
    <statement id="d0_s13">
      <text>anthers cream, 0.4–0.6 × 0.3–0.5 mm;</text>
      <biological_entity id="o4556" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="cream" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s13" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary usually purplish red, 10–20 mm, sparsely mixed strigillose and glandular puberulent or subglabrous;</text>
      <biological_entity id="o4557" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish red" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="arrangement" src="d0_s14" value="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style cream or white, 0.7–2.5 mm, stigma cylindrical to clavate, entire, 0.8–2 × 0.2–1.2 mm, surrounded by anthers.</text>
      <biological_entity id="o4558" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="cream" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4559" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s15" value="cylindrical" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s15" value="clavate" />
        <character char_type="range_value" from="cylindrical" name="shape" src="d0_s15" to="clavate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s15" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4560" name="anther" name_original="anthers" src="d0_s15" type="structure" />
      <relation from="o4559" id="r808" name="surrounded by" negation="false" src="d0_s15" to="o4560" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules slender, 30–55 mm, surfaces sparsely strigillose and glandular puberulent;</text>
      <biological_entity id="o4561" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="slender" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s16" to="55" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4562" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="strigillose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s16" value="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 8–38 mm.</text>
      <biological_entity id="o4563" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s17" to="38" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds narrowly attenuate, 1.4–2 × 0.3–0.5 mm, chalazal collar conspicuous, 0.1–0.3 mm, light-brown or blond, surface papillose;</text>
      <biological_entity id="o4564" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="attenuate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s18" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s18" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="chalazal" id="o4565" name="collar" name_original="collar" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="conspicuous" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s18" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" />
        <character name="coloration" src="d0_s18" value="blond" />
      </biological_entity>
      <biological_entity id="o4566" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="false" name="relief" src="d0_s18" value="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>coma persistent, white, 3–5 mm. 2n = 36.</text>
      <biological_entity id="o4567" name="coma" name_original="coma" src="d0_s19" type="structure">
        <character is_modifier="false" name="duration" src="d0_s19" value="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s19" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4568" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Even though Epilobium davuricum has not yet been characterized by chromosome group, its morphology and general distribution suggest that it (plus E. arcticum) is related to E. palustre as a member of the Palustriformes group.  It does not have the turion-tipped stolons that characterize E. palustre and relatives but has similarly large seeds with a distinct chalazal collar.  Some specimens with aberrant combinations of characters suggest that E. davuricum may hybridize with E. palustre and possibly E. arcticum in areas where their distributions overlap.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subarctic open balsam poplar and spruce forests, taiga, wet meadows, boggy coastal areas, limestone barrens, wet marly soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subarctic open balsam" />
        <character name="habitat" value="forests" modifier="poplar and spruce" />
        <character name="habitat" value="taiga" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="boggy coastal areas" />
        <character name="habitat" value="limestone barrens" />
        <character name="habitat" value="wet marly soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500(–2000) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Nfld. and Labr. (Nfld.), N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska; Europe (Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Europe (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>