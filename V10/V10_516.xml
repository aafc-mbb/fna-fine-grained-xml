<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Shirley A. Graham</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">AMMANNIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 119.  1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 55.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus ammannia</taxon_hierarchy>
    <other_info_on_name type="etymology">For Paul Ammann, 1634–1691, Professor of Botany at Leipzig</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="A. Fernandes &amp; Diniz" date="unknown" rank="genus">Hionanthera</taxon_name>
    <taxon_hierarchy>genus hionanthera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Commerson ex Kunth" date="unknown" rank="genus">Nesaea</taxon_name>
    <taxon_hierarchy>genus nesaea</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Toothcup</other_name>
  <other_name type="common_name">redstem</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, terrestrial or amphibious [aquatic], 1–10 dm, glabrous [puberulent].</text>
      <biological_entity id="o3078" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="terrestrial" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="amphibious" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or trailing, glaucous or not throughout, branched or unbranched, anthocyanic in age, submerged stems sometimes thickened by aerenchymatous spongy tissue.</text>
      <biological_entity id="o3079" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="trailing" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" />
        <character name="pubescence" src="d0_s1" value="not throughout" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" />
        <character constraint="in age" constraintid="o3080" is_modifier="false" name="coloration" src="d0_s1" value="anthocyanic" />
      </biological_entity>
      <biological_entity id="o3080" name="age" name_original="age" src="d0_s1" type="structure" />
      <biological_entity id="o3081" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="submerged" />
        <character constraint="by tissue" constraintid="o3082" is_modifier="false" modifier="sometimes" name="size_or_width" src="d0_s1" value="thickened" />
      </biological_entity>
      <biological_entity id="o3082" name="tissue" name_original="tissue" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="aerenchymatous" />
        <character is_modifier="true" name="texture" src="d0_s1" value="spongy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite [subopposite or whorled];</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity id="o3083" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, linear-lanceolate, oblong, linear-oblong, lanceolate, elliptic, or spatulate [ovate], base cordate or auriculate [attenuate].</text>
      <biological_entity id="o3084" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" />
      </biological_entity>
      <biological_entity id="o3085" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences indeterminate, axillary, simple or compound cymes or solitary flowers [racemes or capitula].</text>
      <biological_entity id="o3086" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="development" src="d0_s5" value="indeterminate" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="compound" />
      </biological_entity>
      <biological_entity id="o3087" name="cyme" name_original="cymes" src="d0_s5" type="structure" />
      <biological_entity id="o3088" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers sessile, subsessile, or pedicellate, actinomorphic, monostylous [di or tristylous];</text>
      <biological_entity id="o3089" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedicellate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedicellate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral-tube perigynous, campanulate or urceolate, semiglobose to globose in fruit, often 8–12 [–16] -ribbed;</text>
      <biological_entity id="o3090" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="perigynous" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="campanulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="urceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="semiglobose" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="globose" />
        <character char_type="range_value" from="urceolate semiglobose" name="shape" src="d0_s7" to="globose" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="campanulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="urceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="semiglobose" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="globose" />
        <character char_type="range_value" constraint="in fruit" constraintid="o3091" from="urceolate semiglobose" name="shape" src="d0_s7" to="globose" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" notes="" src="d0_s7" value="8-12[-16]-ribbed" />
      </biological_entity>
      <biological_entity id="o3091" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>epicalyx segments shorter than or as long as sepals [longer than or absent];</text>
      <biological_entity constraint="epicalyx" id="o3092" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character constraint="than or as-long-as sepals" constraintid="o3093" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" />
      </biological_entity>
      <biological_entity id="o3093" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 4–6 [–8], to 1/4 floral-tube length;</text>
      <biological_entity id="o3094" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="8" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="6" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="1/4" />
      </biological_entity>
      <biological_entity id="o3095" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals caducous [persistent], (0–) 4–8, deep rose-purple, pale-pink, white, or pale lavender, sometimes with rose-purple midvein or basal spot, obovate to suborbiculate;</text>
      <biological_entity id="o3096" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="caducous" />
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s10" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="deep" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="rose-purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale lavender" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale lavender" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="suborbiculate" />
        <character char_type="range_value" from="obovate" name="shape" notes="" src="d0_s10" to="suborbiculate" />
      </biological_entity>
      <biological_entity id="o3097" name="midvein" name_original="midvein" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="rose-purple" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3098" name="spot" name_original="spot" src="d0_s10" type="structure" />
      <relation from="o3096" id="r372" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o3097" />
      <relation from="o3096" id="r373" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o3098" />
    </statement>
    <statement id="d0_s11">
      <text>nectariferous tissue present at ovary-floral tube junction;</text>
      <biological_entity constraint="nectariferous" id="o3099" name="tissue" name_original="tissue" src="d0_s11" type="structure">
        <character constraint="at ovary-floral tube junction" constraintid="o3100" is_modifier="false" name="presence" src="d0_s11" value="absent" />
      </biological_entity>
      <biological_entity constraint="tube" id="o3100" name="junction" name_original="junction" src="d0_s11" type="structure" constraint_original="ovary-floral tube" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 4–12 (–14) [–27];</text>
      <biological_entity id="o3101" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="14" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary incompletely (1 or) 2–5-locular;</text>
      <biological_entity id="o3102" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="2-5-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>placenta globose [elongate];</text>
      <biological_entity id="o3103" name="placenta" name_original="placenta" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style slender and well-exserted or stout and included;</text>
      <biological_entity id="o3104" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="slender" />
        <character is_modifier="false" name="position" src="d0_s15" value="well-exserted" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s15" value="stout" />
        <character is_modifier="false" name="position" src="d0_s15" value="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate or punctiform.</text>
      <biological_entity id="o3105" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="capitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="punctiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsules, walls thin and dry, indehiscent, splitting irregularly at maturity or initially circumscissile then splitting irregularly.</text>
      <biological_entity constraint="fruits" id="o3106" name="capsule" name_original="capsules" src="d0_s17" type="structure" />
      <biological_entity id="o3107" name="wall" name_original="walls" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="thin" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s17" value="dry" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="indehiscent" />
        <character constraint="at maturity" is_modifier="false" name="architecture_or_dehiscence" src="d0_s17" value="splitting" />
        <character is_modifier="false" modifier="initially" name="dehiscence" src="d0_s17" value="circumscissile" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_dehiscence" src="d0_s17" value="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds [2–] 100+, obovoid to semiglobose, concave-convex, 0.3–0.4 × 0.3–0.5 mm;</text>
      <biological_entity id="o3108" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s18" to="100" to_inclusive="false" />
        <character char_type="range_value" from="100" name="quantity" src="d0_s18" upper_restricted="false" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s18" value="obovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s18" value="semiglobose" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s18" to="semiglobose" />
        <character is_modifier="false" name="shape" src="d0_s18" value="concave-convex" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s18" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s18" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cotyledons ± complanate.</text>
      <biological_entity id="o3109" name="cotyledon" name_original="cotyledons" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s19" value="complanate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 80 (5 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Europe, Asia, Africa, Pacific Islands, Australia; worldwide in temperate to subtropical and tropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="worldwide in temperate to subtropical and tropical areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ammannia was expanded from ca. 25 species to ca. 80 with the inclusion of Hionanthera and Nesaea following multi-gene molecular phylogenetic evidence that the genera form a mono­phyly (S. A. Graham et al. 2011).  Africa is the center of diversity for the genus.</discussion>
  <discussion>Ammannia frequently grows with the closely similar Rotala ramosior.  In the flora area, the auriculate leaf bases of Ammannia expediently separate it from Rotala; leaf bases are tapered in R. ramosior.</discussion>
  <references>
    <reference>Graham, S. A.  1985.  A revision of Ammannia (Lythraceae) in the Western Hemisphere.  J. Arnold Arbor. 66: 395–420.</reference>
    <reference>Howell, J. T.  1985.  The genus Ammannia (Lythraceae) in western United States and Canada.  Wasmann J. Biol. 43: 72–74.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles stout, 0.5 mm, included; petals 0–4(–6).</description>
      <determination>3. Ammannia latifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles slender, 1.5–9 mm, exserted; petals 4–6(–8).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences solitary flowers; peduncles 7–19 mm; petals 6, 4–7 mm; stigmas punctiform.</description>
      <determination>4. Ammannia grayi</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences simple or compound cymes (flowers sometimes solitary at proximalmost and distalmost nodes); peduncles 0–9 mm; petals 4(–8), 1.5–3 mm; stigmas capitate.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Cymes 1–3(–5)-flowered mid stem; petals pale lavender, sometimes with rose purple midvein or rose purple basal spot; anthers light yellow.</description>
      <determination>5. Ammannia robusta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Cymes (1–)3–15-flowered mid stem; petals deep rose purple, sometimes with darker rose purple basal spot; anthers deep yellow.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Peduncles slender (nearly filiform); cymes (3–)7–15-flowered mid stem; capsules (1–)1.5–2.5(–3.5) mm diam.</description>
      <determination>1. Ammannia auriculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Peduncles stout; cymes (1–)3–7(–15)-flowered mid stem; capsules 3.5–5 mm diam.</description>
      <determination>2. Ammannia coccinea</determination>
    </key_statement>
  </key>
</bio:treatment>