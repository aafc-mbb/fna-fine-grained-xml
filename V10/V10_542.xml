<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LYTHRUM</taxon_name>
    <taxon_name authority="Fernald" date="1902" rank="species">curtissii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>33: 155.  1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus lythrum;species curtissii</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, slender, 5–10 dm, green, glabrous.</text>
      <biological_entity id="o3689" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, much-branched distally.</text>
      <biological_entity id="o3690" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="much-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite proximally, alternate distally, branch leaves abruptly and conspicuously smaller than those on main-stem;</text>
      <biological_entity id="o3691" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s2" value="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sessile or subsessile;</text>
      <biological_entity constraint="branch" id="o3692" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade (on main-stem) broadly to narrowly lanceolate or oblong, 20–75 × 5–17 mm, (on branches) oblong to narrowly oblong, 3–15 × 1.5–3 mm, base narrowly attenuate.</text>
      <biological_entity id="o3693" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="75" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="17" to_unit="mm" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s4" value="oblong" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="narrowly oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3694" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemes.</text>
      <biological_entity constraint="inflorescences" id="o3695" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers alternate, subsessile or pedicellate, pedicel slender, 1–1.5 mm, distylous;</text>
      <biological_entity id="o3696" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedicellate" />
      </biological_entity>
      <biological_entity id="o3697" name="pedicel" name_original="pedicel" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="slender" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="distylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral-tube obconic, 3–6 × 1 mm;</text>
      <biological_entity id="o3698" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obconic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>epicalyx segments about equal to length of sepals;</text>
      <biological_entity constraint="epicalyx" id="o3699" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character constraint="to " constraintid="o3700" is_modifier="false" name="variability" src="d0_s8" value="equal" />
      </biological_entity>
      <biological_entity id="o3700" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>petals deep to pale-purple with dark central vein, oblanceolate or oblong, 1.5–2 × 0.5–1 mm;</text>
      <biological_entity id="o3701" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" />
        <character constraint="with central vein" constraintid="o3702" is_modifier="false" name="coloration" src="d0_s9" value="pale-purple" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="central" id="o3702" name="vein" name_original="vein" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nectary encircling base of ovary;</text>
      <biological_entity id="o3703" name="nectary" name_original="nectary" src="d0_s10" type="structure" constraint="ovary" />
      <biological_entity id="o3704" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o3705" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
      <relation from="o3703" id="r444" name="encircling" negation="false" src="d0_s10" to="o3704" />
      <relation from="o3703" id="r445" name="part_of" negation="false" src="d0_s10" to="o3705" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 6.</text>
      <biological_entity id="o3706" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules septicidal or septifragal.</text>
      <biological_entity id="o3707" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="septifragal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds ca. 20, narrowly obovoid to fusiform.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 20.</text>
      <biological_entity id="o3708" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s13" value="obovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="fusiform" />
        <character char_type="range_value" from="narrowly obovoid" name="shape" src="d0_s13" to="fusiform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3709" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The shady habitat of Lythrum curtissii, a rare, delicate-flowered species, is unlike that of other native species of Lythrum, which tend toward more open, sunny areas.  Putative hybrids of L. alatum var. lanceolatum and L. curtissii have been noted in the vicinity of Leary, Calhoun County, Georgia.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet or moist shady wood­lands, streamsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="moist shady woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>