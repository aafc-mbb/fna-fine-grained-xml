<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1767" rank="genus">MELALEUCA</taxon_name>
    <taxon_name authority="(Solander ex Gaertner) Byrnes" date="1984" rank="species">viminalis</taxon_name>
    <place_of_publication>
      <publication_title>Austrobaileya</publication_title>
      <place_in_publication>2: 75.  1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus melaleuca;species viminalis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Metrosiderosviminalis</taxon_name>
    <taxon_name authority="ex Gaertner" date="1788" rank="species">Solander</taxon_name>
    <place_of_publication>
      <publication_title>Fruct. Sem. Pl.</publication_title>
      <place_in_publication>1: 171, plate 34, fig. 4.  1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus metrosiderosviminalis;species solander</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Callistemon</taxon_name>
    <taxon_name authority="(Solander ex Gaertner) G. Don ex Loudon" date="unknown" rank="species">viminalis</taxon_name>
    <taxon_hierarchy>genus callistemon;species viminalis</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Weeping bottlebrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 1–35 m;</text>
      <biological_entity id="o3350" name="shrub" name_original="shrubs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="35" to_unit="m" />
      </biological_entity>
      <biological_entity id="o3351" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="35" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bark fibrous, hard.</text>
      <biological_entity id="o3352" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" />
        <character is_modifier="false" name="texture" src="d0_s1" value="hard" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o3353" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly elliptic to elliptic or narrowly obovate, 2.5–13.8 × 0.3–2.7 cm, veins pinnate, surfaces glabrescent.</text>
      <biological_entity id="o3354" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="to veins, surfaces" constraintid="o3355, o3356" is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" />
      </biological_entity>
      <biological_entity id="o3355" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="elliptic" />
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s3" value="obovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="13.8" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="2.7" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" />
      </biological_entity>
      <biological_entity id="o3356" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="elliptic" />
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s3" value="obovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="13.8" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="2.7" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 15–50-flowered, flowers in monads, pseudoterminal or interstitial, 35–50 mm wide.</text>
      <biological_entity id="o3357" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="15-50-flowered" />
      </biological_entity>
      <biological_entity id="o3358" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="pseudoterminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="interstitial" />
        <character char_type="range_value" from="35" from_unit="mm" name="width" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3359" name="monad" name_original="monads" src="d0_s4" type="structure" />
      <relation from="o3358" id="r586" name="in" negation="false" src="d0_s4" to="o3359" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx lobes hairy or glabrescent abaxially, margins herbaceous;</text>
      <biological_entity id="o3360" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="calyx" id="o3361" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrescent" />
      </biological_entity>
      <biological_entity id="o3362" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s5" value="herbaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals deciduous, 3.4–5.9 mm;</text>
      <biological_entity id="o3363" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3364" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s6" to="5.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments connate in bundles of 9–14, usually in 5 distinct bundles, these sometimes obscure, especially when claw very short, red or crimson, 13–26 mm, bundle claw to 2.2 mm;</text>
      <biological_entity id="o3365" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3366" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character constraint="in bundles" constraintid="o3367" is_modifier="false" name="fusion" src="d0_s7" value="connate" />
        <character is_modifier="false" modifier="sometimes" name="prominence" notes="" src="d0_s7" value="obscure" />
        <character is_modifier="false" modifier="especially; when claw very short" name="coloration" src="d0_s7" value="red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="crimson" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s7" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3367" name="bundle" name_original="bundles" src="d0_s7" type="structure" />
      <biological_entity id="o3368" name="bundle" name_original="bundles" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="5" />
        <character is_modifier="true" name="fusion" src="d0_s7" value="distinct" />
      </biological_entity>
      <biological_entity constraint="bundle" id="o3369" name="claw" name_original="claw" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="2.2" to_unit="mm" />
      </biological_entity>
      <relation from="o3366" id="r587" modifier="of 9-14; usually" name="in" negation="false" src="d0_s7" to="o3368" />
    </statement>
    <statement id="d0_s8">
      <text>style 16–29 mm;</text>
      <biological_entity id="o3370" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3371" name="style" name_original="style" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s8" to="29" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovules ca. 100 per locule.</text>
      <biological_entity id="o3372" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3373" name="ovule" name_original="ovules" src="d0_s9" type="structure">
        <character constraint="per locule" constraintid="o3374" name="quantity" src="d0_s9" value="100" />
      </biological_entity>
      <biological_entity id="o3374" name="locule" name_original="locule" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules 3.8–4.8 mm.</text>
      <biological_entity id="o3375" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s10" to="4.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cotyledons obvolute.</text>
      <biological_entity id="o3376" name="cotyledon" name_original="cotyledons" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s11" value="obvolute" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Melaleuca viminalis is well known in cultivation and is widely grown for its showy flowers.  The species is an unusual member of the bottlebrush group of Melaleuca in that its staminal filaments are connate into five bundles; most other species of bottlebrush have distinct stamens.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed riparian areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riparian areas" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Fla.; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>