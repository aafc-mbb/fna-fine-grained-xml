<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="genus">CLARKIA</taxon_name>
    <taxon_name authority="(Spach) H. Lewis &amp; M. E. Lewis" date="1955" rank="section">Phaeostoma</taxon_name>
    <taxon_name authority="H. Lewis &amp; M. E. Lewis" date="1955" rank="subsection">Lautiflorae</taxon_name>
    <taxon_name authority="(Durand) A. Nelson &amp; J. F. Macbride" date="1918" rank="species">biloba</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>65: 60.  1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus clarkia;section phaeostoma;subsection lautiflorae;species biloba</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Durand" date="1855" rank="species">biloba</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Pratten. Calif.,</publication_title>
      <place_in_publication>87.  1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species biloba</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Godetia</taxon_name>
    <taxon_name authority="(Durand) S. Watson" date="unknown" rank="species">biloba</taxon_name>
    <taxon_hierarchy>genus godetia;species biloba</taxon_hierarchy>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Two lobed clarkia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 30–100 cm, strigil­lose.</text>
      <biological_entity id="o77" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole to 15 mm;</text>
      <biological_entity id="o78" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o79" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear to lanceolate, 2–8 cm.</text>
      <biological_entity id="o80" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o81" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="linear" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="lanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences open racemes, axis recurved at tip in bud;</text>
      <biological_entity id="o82" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o83" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="open" />
      </biological_entity>
      <biological_entity id="o84" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character constraint="at tip" constraintid="o85" is_modifier="false" name="orientation" src="d0_s3" value="recurved" />
      </biological_entity>
      <biological_entity id="o85" name="tip" name_original="tip" src="d0_s3" type="structure" />
      <biological_entity id="o86" name="bud" name_original="bud" src="d0_s3" type="structure" />
      <relation from="o85" id="r11" name="in" negation="false" src="d0_s3" to="o86" />
    </statement>
    <statement id="d0_s4">
      <text>buds pendent.</text>
      <biological_entity id="o87" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: floral-tube 1–4 mm;</text>
      <biological_entity id="o88" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o89" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed together to 1 side;</text>
      <biological_entity id="o90" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o91" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="together" name="orientation" src="d0_s6" value="reflexed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="1" />
      </biological_entity>
      <biological_entity id="o92" name="side" name_original="side" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>corolla rotate to bowl-shaped, petals purplish to pale-pink, lavender, or bright pink to magenta, often red-flecked, broadly to narrowly fan-shaped, 10–25 mm, shallowly to deeply 2-lobed;</text>
      <biological_entity id="o93" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o94" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="rotate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="bowl--shaped" value_original="bowl-shaped" />
        <character char_type="range_value" from="rotate" name="shape" src="d0_s7" to="bowl-shaped" />
      </biological_entity>
      <biological_entity id="o95" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="purplish" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="pale-pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="lavender" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="bright pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="magenta" />
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s7" to="pale-pink lavender or bright pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="purplish" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="pale-pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="lavender" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="bright pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="magenta" />
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s7" to="pale-pink lavender or bright pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="purplish" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="pale-pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="lavender" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="bright pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s7" value="magenta" />
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s7" to="pale-pink lavender or bright pink" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="red-flecked" />
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s7" value="fan--shaped" value_original="fan-shaped" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="shallowly to deeply" name="shape" src="d0_s7" value="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 8, unequal, outer anthers lavender, inner ones smaller, paler.</text>
      <biological_entity id="o96" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o97" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" />
      </biological_entity>
      <biological_entity constraint="outer" id="o98" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration_or_odor" src="d0_s8" value="lavender" />
      </biological_entity>
      <biological_entity constraint="inner" id="o99" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="smaller" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="paler" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 10–25 mm.</text>
      <biological_entity id="o100" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds brown, 1 mm, minutely scaly to puberulent, crest inconspicuous.</text>
      <biological_entity id="o101" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" />
        <character is_modifier="false" modifier="minutely" name="pubescence" notes="[duplicate value]" src="d0_s10" value="scaly" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s10" value="puberulent" />
        <character char_type="range_value" from="minutely scaly" name="pubescence" src="d0_s10" to="puberulent" />
      </biological_entity>
      <biological_entity id="o102" name="crest" name_original="crest" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="inconspicuous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="California." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Clarkia biloba is most closely related to C. lingulata, which is derived from C. biloba subsp. australis.  Some populations of C. biloba subsp. brandegeeae (originally described as a form of C. dudleyana) are morphologi­cally very similar to some individuals of C. dudleyana but the two taxa are separated geographically, have different chromosome numbers, and hybrids between them are sterile.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals bright pink to magenta, narrowly fan-shaped, length greater than 1.5 times width.</description>
      <determination>27c. Clarkia biloba subsp. australis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals lavender to pale or purplish pink, broadly fan-shaped, length not greater than 1.5 times width.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals purplish to pale pink, deeply 2-lobed, lobes usually 1/5–1/2 petal length.</description>
      <determination>27a. Clarkia biloba subsp. biloba</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals lavender, shallowly 2-lobed, lobes usually less than 1/5 petal length, sometimes obscure.</description>
      <determination>27b. Clarkia biloba subsp. brandegeeae</determination>
    </key_statement>
  </key>
</bio:treatment>