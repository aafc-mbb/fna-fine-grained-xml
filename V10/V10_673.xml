<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">COMBRETACEAE</taxon_name>
    <taxon_name authority="Willdenow" date="1803" rank="genus">LUMNITZERA</taxon_name>
    <place_of_publication>
      <publication_title>Ges. Naturf. Freunde Berlin Neue Schriften</publication_title>
      <place_in_publication>4: 186.  1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family combretaceae;genus lumnitzera</taxon_hierarchy>
    <other_info_on_name type="etymology">For Istrán Lumnitzer, 1750–1806, Hungarian botanist</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, rarely [sometimes] with pneumatophores, these looping upward then downward into substrate.</text>
      <biological_entity id="o1653" name="shrub" name_original="shrubs" src="d0_s0" type="structure" />
      <biological_entity id="o1654" name="tree" name_original="trees" src="d0_s0" type="structure" />
      <biological_entity id="o1655" name="pneumatophore" name_original="pneumatophores" src="d0_s0" type="structure" />
      <biological_entity id="o1656" name="substrate" name_original="substrate" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="downward" />
      </biological_entity>
      <relation from="o1653" id="r342" modifier="rarely" name="with" negation="false" src="d0_s0" to="o1655" />
      <relation from="o1654" id="r343" modifier="rarely" name="with" negation="false" src="d0_s0" to="o1655" />
      <relation from="o1653" id="r344" name="looping" negation="false" src="d0_s0" to="o1656" />
      <relation from="o1654" id="r345" name="looping" negation="false" src="d0_s0" to="o1656" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, equal;</text>
      <biological_entity id="o1657" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="variability" src="d0_s1" value="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs glabrous or sparsely hairy, hairs short, combretaceous.</text>
      <biological_entity id="o1658" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" />
      </biological_entity>
      <biological_entity id="o1659" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, alternate and spiral;</text>
      <biological_entity id="o1660" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" />
        <character is_modifier="false" name="arrangement_or_course" src="d0_s3" value="spiral" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules absent;</text>
      <biological_entity id="o1661" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole not differentiated proximally and no part of it persistent, nectar glands absent;</text>
      <biological_entity id="o1662" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not; proximally" name="variability" src="d0_s5" value="differentiated" />
      </biological_entity>
      <biological_entity id="o1663" name="part" name_original="part" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="no" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o1664" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="true" name="duration" src="d0_s5" value="persistent" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" />
      </biological_entity>
      <relation from="o1663" id="r346" name="consist_of" negation="false" src="d0_s5" to="o1664" />
    </statement>
    <statement id="d0_s6">
      <text>blade fleshy-leathery, venation obscure, apex rounded to retuse;</text>
      <biological_entity id="o1665" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="fleshy-leathery" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>with pit-domatia on distal margins and at apex, each containing a basal gland.</text>
      <biological_entity id="o1666" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="rounded" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s6" value="retuse" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s6" to="retuse" />
      </biological_entity>
      <biological_entity id="o1667" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o1668" name="gland" name_original="gland" src="d0_s7" type="structure" />
      <relation from="o1666" id="r347" name="with pit-domatia on distal margins and at" negation="false" src="d0_s7" to="o1667" />
      <relation from="o1667" id="r348" name="containing a" negation="false" src="d0_s7" to="o1668" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences axillary [terminal], spikes [racemes];</text>
      <biological_entity id="o1669" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" />
      </biological_entity>
      <biological_entity id="o1670" name="spike" name_original="spikes" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>bracteoles present.</text>
      <biological_entity id="o1671" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers somewhat conspicuous, bisexual;</text>
      <biological_entity id="o1672" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="somewhat" name="prominence" src="d0_s10" value="conspicuous" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>hypanthium cylindrical, free portion 1.3–2.5 mm, slightly contracted beyond ovary, slightly conic distally, glabrous abaxially [pubescent];</text>
      <biological_entity id="o1673" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cylindrical" />
      </biological_entity>
      <biological_entity id="o1674" name="portion" name_original="portion" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="free" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character constraint="beyond ovary" constraintid="o1675" is_modifier="false" modifier="slightly" name="condition_or_size" src="d0_s11" value="contracted" />
        <character is_modifier="false" modifier="slightly; distally" name="shape" notes="" src="d0_s11" value="conic" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="glabrous" />
      </biological_entity>
      <biological_entity id="o1675" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>sepals 5, green, triangular, glabrous [pubescent], margins ciliate, without [with] glands;</text>
      <biological_entity id="o1676" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" />
        <character is_modifier="false" name="shape" src="d0_s12" value="triangular" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" />
      </biological_entity>
      <biological_entity id="o1677" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" />
      </biological_entity>
      <biological_entity id="o1678" name="gland" name_original="glands" src="d0_s12" type="structure" />
      <relation from="o1677" id="r349" name="without" negation="false" src="d0_s12" to="o1678" />
    </statement>
    <statement id="d0_s13">
      <text>petals 5, white or cream [yellow, red], ovate to elliptic, 4–5 mm, apex acute to obtuse, glabrous, margins sparsely ciliate;</text>
      <biological_entity id="o1679" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="cream" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="elliptic" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s13" to="elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1680" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="obtuse" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" />
      </biological_entity>
      <biological_entity id="o1681" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s13" value="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens [5–] 10, exserted;</text>
      <biological_entity id="o1682" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s14" to="10" to_inclusive="false" />
        <character name="quantity" src="d0_s14" value="10" />
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectary disc inconspicuous, from proximal portion of hypanthium adaxial surface, glabrous;</text>
      <biological_entity constraint="nectary" id="o1683" name="disc" name_original="disc" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="inconspicuous" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s15" value="glabrous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1684" name="portion" name_original="portion" src="d0_s15" type="structure" />
      <biological_entity id="o1685" name="hypanthium" name_original="hypanthium" src="d0_s15" type="structure" />
      <biological_entity constraint="adaxial" id="o1686" name="surface" name_original="surface" src="d0_s15" type="structure" />
      <relation from="o1683" id="r350" name="from" negation="false" src="d0_s15" to="o1684" />
      <relation from="o1684" id="r351" name="part_of" negation="false" src="d0_s15" to="o1685" />
    </statement>
    <statement id="d0_s16">
      <text>ovary somewhat flattened;</text>
      <biological_entity id="o1687" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s16" value="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>style straight to slightly curved, free from hypanthium;</text>
      <biological_entity id="o1688" name="style" name_original="style" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" notes="[duplicate value]" src="d0_s17" value="straight" />
        <character is_modifier="false" modifier="slightly" name="course" notes="[duplicate value]" src="d0_s17" value="curved" />
        <character char_type="range_value" from="straight" name="course" src="d0_s17" to="slightly curved" />
        <character constraint="from hypanthium" constraintid="o1689" is_modifier="false" name="fusion" src="d0_s17" value="free" />
      </biological_entity>
      <biological_entity id="o1689" name="hypanthium" name_original="hypanthium" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 2–5.</text>
      <biological_entity id="o1690" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s18" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Drupes slightly flattened, ovoid, slightly ridged;</text>
    </statement>
    <statement id="d0_s20">
      <text>with 2 major ridges, those subtending bracteoles better developed and expressed proximally, the remaining minor ridges only developed distally;</text>
      <biological_entity id="o1691" name="drupe" name_original="drupes" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s19" value="flattened" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ovoid" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s19" value="ridged" />
      </biological_entity>
      <biological_entity id="o1692" name="ridge" name_original="ridges" src="d0_s20" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s20" value="2" />
        <character is_modifier="true" name="size" src="d0_s20" value="major" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o1693" name="bracteole" name_original="bracteoles" src="d0_s20" type="structure">
        <character is_modifier="false" name="development" src="d0_s20" value="developed" />
        <character is_modifier="false" modifier="only; distally" name="development" src="d0_s20" value="developed" />
      </biological_entity>
      <biological_entity constraint="minor" id="o1694" name="ridge" name_original="ridges" src="d0_s20" type="structure" />
      <relation from="o1691" id="r352" name="with" negation="false" src="d0_s20" to="o1692" />
      <relation from="o1693" id="r353" name="expressed" negation="false" src="d0_s20" to="o1694" />
    </statement>
    <statement id="d0_s21">
      <text>hypanthium and calyx persistent.</text>
      <biological_entity id="o1695" name="hypanthium" name_original="hypanthium" src="d0_s21" type="structure">
        <character is_modifier="false" name="duration" src="d0_s21" value="persistent" />
      </biological_entity>
      <biological_entity id="o1696" name="calyx" name_original="calyx" src="d0_s21" type="structure">
        <character is_modifier="false" name="duration" src="d0_s21" value="persistent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced, Florida; Asia, e Africa, Indian Ocean Islands (Mada­gascar), w Pacific Islands (Fiji, Tonga), n Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Florida" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="e Africa" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands (Mada­gascar)" establishment_means="native" />
        <character name="distribution" value="w Pacific Islands (Fiji)" establishment_means="native" />
        <character name="distribution" value="w Pacific Islands (Tonga)" establishment_means="native" />
        <character name="distribution" value="n Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>