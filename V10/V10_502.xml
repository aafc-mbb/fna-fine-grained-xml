<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="(S. F. Blake) J. R. Abbott" date="2011" rank="genus">RHINOTROPIS</taxon_name>
    <taxon_name authority="(A. Gray) J. R. Abbott" date="2011" rank="species">lindheimeri</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>5: 135.  2011</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus rhinotropis;species lindheimeri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygalalindheimeri</taxon_name>
    <taxon_name authority="Gray" date="1850" rank="species">A.</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 150.  1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus polygalalindheimeri;species a.</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Shrubby milkwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, multistemmed, 0.3–3 (–3.5) dm (rarely straggling to 10 dm).</text>
      <biological_entity id="o2697" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_growth_form" src="d0_s0" value="multistemmed" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect, usually pubescent, rarely glabrous, hairs spreading or incurved.</text>
      <biological_entity id="o2698" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="decumbent" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="erect" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" />
      </biological_entity>
      <biological_entity id="o2699" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves subsessile to petiolate, petiole to 1 (–1.5) mm;</text>
      <biological_entity id="o2700" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" notes="[duplicate value]" src="d0_s2" value="subsessile" />
        <character is_modifier="false" name="architecture" notes="[duplicate value]" src="d0_s2" value="petiolate" />
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s2" to="petiolate" />
      </biological_entity>
      <biological_entity id="o2701" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic to linear, lanceolate, ovate, obovate, or scalelike, (3–) 4–41 × (0.5–) 1–12 (–18) mm, base rounded to cuneate, apex obtuse to rounded, surfaces pubescent or glabrous, hairs incurved or spreading.</text>
      <biological_entity id="o2702" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic to linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="scalelike" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s3" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="41" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_width" src="d0_s3" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2703" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="rounded" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="cuneate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="cuneate" />
      </biological_entity>
      <biological_entity id="o2704" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="obtuse" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="rounded" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o2705" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" />
      </biological_entity>
      <biological_entity id="o2706" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="incurved" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Racemes terminal, usually leaf-opposed, often also from near base of plant, these usually with chasmogamous flowers, occasionally bearing reduced, beakless cleistogamous or semi-cleistogamous flowers, rarely with cleistogamous or semi-cleistogamous flowers throughout, 1–12 (–15) × 0.3–1.5 cm;</text>
      <biological_entity id="o2707" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s4" value="leaf-opposed" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" notes="" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" notes="" src="d0_s4" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2708" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o2709" name="plant" name_original="plant" src="d0_s4" type="structure" />
      <biological_entity id="o2710" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="chasmogamous" />
      </biological_entity>
      <biological_entity id="o2711" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="reduced" />
        <character is_modifier="true" name="reproduction" src="d0_s4" value="beakless" />
        <character is_modifier="true" modifier="occasionally" name="reproduction" src="d0_s4" value="cleistogamous" />
        <character is_modifier="true" name="reproduction" src="d0_s4" value="semi-cleistogamous" />
      </biological_entity>
      <biological_entity id="o2712" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="cleistogamous" />
        <character is_modifier="true" name="reproduction" src="d0_s4" value="semi-cleistogamous" />
      </biological_entity>
      <relation from="o2707" id="r329" modifier="often" name="from" negation="false" src="d0_s4" to="o2708" />
      <relation from="o2708" id="r330" name="part_of" negation="false" src="d0_s4" to="o2709" />
      <relation from="o2707" id="r331" modifier="usually" name="with" negation="false" src="d0_s4" to="o2710" />
      <relation from="o2707" id="r332" modifier="occasionally" name="bearing" negation="false" src="d0_s4" to="o2711" />
      <relation from="o2707" id="r333" modifier="occasionally; rarely" name="with" negation="false" src="d0_s4" to="o2712" />
    </statement>
    <statement id="d0_s5">
      <text>rachis not thorn-tipped;</text>
      <biological_entity id="o2713" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="thorn-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncle 0–1 cm;</text>
      <biological_entity id="o2714" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts usually persistent, ovate, lanceolate, or elliptic.</text>
      <biological_entity id="o2715" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s7" value="persistent" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1–4.5 mm, pubescent.</text>
      <biological_entity id="o2716" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers usually pink to purple, rarely white, keel yellowish distally, wings pink or rose, (3.7–) 4–7.4 (–7.7) mm;</text>
      <biological_entity id="o2717" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" notes="[duplicate value]" src="d0_s9" value="pink" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s9" value="purple" />
        <character char_type="range_value" from="usually pink" name="coloration" src="d0_s9" to="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="white" />
      </biological_entity>
      <biological_entity id="o2718" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="yellowish" />
      </biological_entity>
      <biological_entity id="o2719" name="wing" name_original="wings" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="rose" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="7.7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper sepal persistent, other sepals deciduous, upper sepal ovate, 1.7–4.5 (–5.2) mm, lower sepals lanceolate to obovate, (1.3–) 1.6–3.5 (–3.8) mm, pubescent or glabrous;</text>
      <biological_entity constraint="upper" id="o2720" name="sepal" name_original="sepal" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" />
      </biological_entity>
      <biological_entity id="o2721" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="deciduous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o2722" name="sepal" name_original="sepal" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5.2" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o2723" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="1.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>wings obovate to oblong-obovate, 3–6.4 (–7.2) × (1.2–) 1.4–3.2 mm, glabrous or pubescent;</text>
      <biological_entity id="o2724" name="wing" name_original="wings" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="obovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s11" value="oblong-obovate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s11" to="oblong-obovate" />
        <character char_type="range_value" from="6.4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="7.2" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="6.4" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_width" src="d0_s11" to="1.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s11" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>keel (2.7–) 3.1–6.2 mm, sac glabrous or with scattered hairs, beak linear (or bluntly rounded), (0–) 0.5–2 × (0–) 0.2–0.6 mm, glabrous or pubescent.</text>
      <biological_entity id="o2725" name="keel" name_original="keel" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="3.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.1" from_unit="mm" name="some_measurement" src="d0_s12" to="6.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2726" name="sac" name_original="sac" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="with scattered hairs" />
      </biological_entity>
      <biological_entity id="o2727" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="scattered" />
      </biological_entity>
      <biological_entity id="o2728" name="beak" name_original="beak" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_length" src="d0_s12" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_width" src="d0_s12" to="0.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" />
      </biological_entity>
      <relation from="o2726" id="r334" name="with" negation="false" src="d0_s12" to="o2727" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules ellipsoid, oblong, slightly ovoid, or obovoid, 3.3–6 (–6.8) × 2–4 mm, base rounded to subtruncate, often oblique, margins with narrow wing or not winged, usually pubescent, rarely subglabrous.</text>
      <biological_entity id="o2729" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovoid" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovoid" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="6.8" to_unit="mm" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2730" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="rounded" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="subtruncate" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s13" to="subtruncate" />
        <character is_modifier="false" modifier="often" name="orientation_or_shape" src="d0_s13" value="oblique" />
      </biological_entity>
      <biological_entity id="o2731" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s13" value="winged" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s13" value="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s13" value="subglabrous" />
      </biological_entity>
      <biological_entity id="o2732" name="wing" name_original="wing" src="d0_s13" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s13" value="narrow" />
      </biological_entity>
      <relation from="o2731" id="r335" name="with" negation="false" src="d0_s13" to="o2732" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds 2.8–4.3 mm, pubescent;</text>
      <biological_entity id="o2733" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s14" to="4.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>aril 0.7–2.5 mm, lobes to 3/4 length of seed.</text>
      <biological_entity id="o2734" name="aril" name_original="aril" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2735" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="0 length of seed" name="length" src="d0_s15" to="3/4 length of seed" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw, sc United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw" establishment_means="native" />
        <character name="distribution" value="sc United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety eucosma (S. F. Blake) T. Wendt is known from northern Mexico.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually with spreading hairs, hairs rarely somewhat crisped, mostly 0.3–0.5 mm; leaf blades usually elliptic, ovate, or obovate proximally, distally becoming narrowly so, venation usually prominently reticulate, surfaces pubescent (not glabrous); keel sacs glabrous or with scattered, spreading hairs proximally, hairs not incurved in distal 1/2.</description>
      <determination>6a. Rhinotropis lindheimeri var. lindheimeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually with incurved hairs, hairs rarely irregularly spreading, 0.07–0.15 mm, rarely glabrous; leaf blades lanceolate, linear, or scalelike to elliptic, ovate, or obovate, venation usually not prominently reticulate (usually midvein prominent abaxially, occasionally reticulate), surfaces pubescent or glabrous; keel sacs glabrous or, rarely, with incurved hairs in distal 1/2.</description>
      <determination>6b. Rhinotropis lindheimeri var. parvifolia</determination>
    </key_statement>
  </key>
</bio:treatment>