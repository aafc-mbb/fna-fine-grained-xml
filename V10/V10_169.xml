<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Oenothera</taxon_name>
    <taxon_name authority="(Rose ex Britton &amp; A. Brown) W. Dietrich" date="1978" rank="subsection">Raimannia</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>61: plate 3361.  1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section oenothera;subsection raimannia;species drummondii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Raimannia</taxon_name>
    <taxon_name authority="(Hooker) Rose ex Sprague &amp; L. Riley" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_hierarchy>genus raimannia;species drummondii</taxon_hierarchy>
  </taxon_identification>
  <number>90.</number>
  <discussion>Subspecies 2 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>s United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subspecies thalassaphila (Brandegee) W.</text>
    </statement>
    <statement id="d0_s1">
      <text>Dietrich and W. L. Wagner differs from subsp.</text>
      <biological_entity id="o2241" name="subsp" name_original="subsp" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>drummondii in a number of modally distinctive morphological features, especially floral-tubes 2–3.5 cm, sepal tips 0.3–1 mm, capsules 2–4 cm × 2.5–5 mm in diameter and those, coupled with the great disjunction from the Atlantic coast of the United States and Mexico to the southern tip of Baja California, make it worthy of recognition.</text>
      <biological_entity id="o2242" name="floral-tube" name_original="floral-tubes" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" modifier="in a number; of modally distinctive morphological" name="some_measurement" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="sepal" id="o2243" name="tip" name_original="tips" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2245" name="disjunction" name_original="disjunction" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="great" />
      </biological_entity>
      <biological_entity id="o2246" name="state" name_original="states" src="d0_s2" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s2" value="united" />
      </biological_entity>
      <biological_entity id="o2247" name="tip" name_original="tip" src="d0_s2" type="structure">
        <character is_modifier="true" name="geographical_terms" src="d0_s2" value="southern" />
      </biological_entity>
      <biological_entity id="o2248" name="california" name_original="california" src="d0_s2" type="structure" />
      <biological_entity id="o2249" name="recognition" name_original="recognition" src="d0_s2" type="structure" />
      <relation from="o2244" id="r508" name="coupled with" negation="false" src="d0_s2" to="o2245" />
      <relation from="o2245" id="r509" modifier="from the atlantic coast" name="part_of" negation="false" src="d0_s2" to="o2246" />
      <relation from="o2244" id="r510" name="to" negation="false" src="d0_s2" to="o2247" />
      <relation from="o2247" id="r511" name="part_of" negation="false" src="d0_s2" to="o2248" />
      <relation from="o2244" id="r512" name="make it worthy" negation="false" src="d0_s2" to="o2249" />
    </statement>
    <statement id="d0_s3">
      <text>Oenothera drummondii self-compatible and outcrossing.</text>
      <biological_entity id="o2244" name="capsule" name_original="capsules" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="self-compatible" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="outcrossing" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>