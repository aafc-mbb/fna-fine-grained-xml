<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="(Spach) W. L. Wagner &amp; Hoch" date="2007" rank="section">Anogra</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 179. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section anogra</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Spach" date=" 1835" rank="genus">Anogra</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Sci. Nat., Bot., sér.</publication_title>
      <place_in_publication>2, 4: 164.  1835</place_in_publication>
      <other_info_on_pub>based on Baumannia Spach, Hist. Nat. Vég. 4: 351.  1835, not de Candolle 1834</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus anogra</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="[unranked] Anogra (Spach) Endlicher" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_hierarchy>genus oenothera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="(Spach) Reichenbach" date="unknown" rank="subgenus">Anogra</taxon_name>
    <taxon_hierarchy>genus oenothera;subgenus anogra</taxon_hierarchy>
  </taxon_identification>
  <number>17o.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs winter-annual or perennial, caulescent;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a taproot, sometimes lateral roots pro­ducing adventitious shoots.</text>
      <biological_entity id="o592" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" />
      </biological_entity>
      <biological_entity id="o593" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s1" value="lateral" />
      </biological_entity>
      <biological_entity id="o594" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s1" value="adventitious" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent to ascending or erect, unbranched or with short, lateral branches, epidermis white or pink, exfoliating proximally.</text>
      <biological_entity id="o595" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="decumbent" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="ascending" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="erect" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending or erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="with short , lateral branches" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o596" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" />
      </biological_entity>
      <biological_entity id="o597" name="epidermis" name_original="epidermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink" />
        <character is_modifier="false" modifier="proximally" name="relief" src="d0_s2" value="exfoliating" />
      </biological_entity>
      <relation from="o595" id="r144" name="with" negation="false" src="d0_s2" to="o596" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, sometimes forming conspicuous basal rosette, sometimes this weakly developed or absent (at least during flowering), 1–13 (–26) cm;</text>
      <biological_entity id="o598" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" />
        <character is_modifier="false" modifier="sometimes; sometimes; weakly" name="development" src="d0_s3" value="developed" />
        <character is_modifier="false" modifier="sometimes; sometimes" name="presence" src="d0_s3" value="absent" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="26" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="13" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o599" name="rosette" name_original="rosette" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="conspicuous" />
      </biological_entity>
      <relation from="o598" id="r145" modifier="sometimes" name="forming" negation="false" src="d0_s3" to="o599" />
    </statement>
    <statement id="d0_s4">
      <text>blade margins sinuate-dentate to pinnatifid, denticulate, subentire, or entire.</text>
      <biological_entity constraint="blade" id="o600" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="sinuate-dentate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="pinnatifid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="denticulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="subentire" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="entire" />
        <character char_type="range_value" from="sinuate-dentate" name="shape" src="d0_s4" to="pinnatifid denticulate subentire or entire" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="sinuate-dentate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="pinnatifid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="denticulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="subentire" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="entire" />
        <character char_type="range_value" from="sinuate-dentate" name="shape" src="d0_s4" to="pinnatifid denticulate subentire or entire" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="sinuate-dentate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="pinnatifid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="denticulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="subentire" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="entire" />
        <character char_type="range_value" from="sinuate-dentate" name="shape" src="d0_s4" to="pinnatifid denticulate subentire or entire" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="sinuate-dentate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="pinnatifid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="denticulate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="subentire" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="entire" />
        <character char_type="range_value" from="sinuate-dentate" name="shape" src="d0_s4" to="pinnatifid denticulate subentire or entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers in axils of distal leaves.</text>
      <biological_entity id="o601" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o602" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" />
      </biological_entity>
      <biological_entity id="o603" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o604" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o602" id="r146" name="in" negation="false" src="d0_s5" to="o603" />
      <relation from="o603" id="r147" name="part_of" negation="false" src="d0_s5" to="o604" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers opening near sunset, with a sweet scent or nearly unscented;</text>
      <biological_entity id="o605" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="nearly" name="odor" src="d0_s6" value="unscented" />
      </biological_entity>
      <biological_entity id="o606" name="sunset" name_original="sunset" src="d0_s6" type="structure" />
      <relation from="o605" id="r148" name="opening near" negation="false" src="d0_s6" to="o606" />
    </statement>
    <statement id="d0_s7">
      <text>buds nodding by recurved floral-tube, usually sharply or bluntly quadrangular in cross-section (sometimes fluted in distal 1/2 in O. deltoides), without free tips or free tips short (sometimes to 9 mm in O. deltoides);</text>
      <biological_entity id="o607" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character constraint="by floral-tube" constraintid="o608" is_modifier="false" name="orientation" src="d0_s7" value="nodding" />
        <character constraint="in cross-section" constraintid="o609" is_modifier="false" modifier="usually sharply; sharply; bluntly" name="shape" notes="" src="d0_s7" value="quadrangular" />
      </biological_entity>
      <biological_entity id="o608" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="recurved" />
      </biological_entity>
      <biological_entity id="o609" name="cross-section" name_original="cross-section" src="d0_s7" type="structure" />
      <biological_entity id="o610" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
      </biological_entity>
      <biological_entity id="o611" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" />
      </biological_entity>
      <relation from="o607" id="r149" name="without" negation="false" src="d0_s7" to="o610" />
      <relation from="o607" id="r150" name="without" negation="false" src="d0_s7" to="o611" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube 15–40 (–50) mm;</text>
      <biological_entity id="o612" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="50" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals separatingin pairs or individually;</text>
      <biological_entity id="o613" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals white, fading pink, obcordate to obovate;</text>
      <biological_entity id="o614" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="fading pink" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obcordate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obovate" />
        <character char_type="range_value" from="obcordate" name="shape" src="d0_s10" to="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigma deeply divided into 4 linear lobes.</text>
      <biological_entity id="o615" name="stigma" name_original="stigma" src="d0_s11" type="structure">
        <character constraint="into lobes" constraintid="o616" is_modifier="false" modifier="deeply" name="shape" src="d0_s11" value="divided" />
      </biological_entity>
      <biological_entity id="o616" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="4" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules straight, curved upward, spreading, or contorted, sometimes woody in age, cylindrical, obtusely 4-angled, gradually tapering from base to apex, dehiscent 1/2 to nearly throughout;</text>
      <biological_entity id="o618" name="age" name_original="age" src="d0_s12" type="structure" />
      <biological_entity id="o619" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o620" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <relation from="o619" id="r151" name="to" negation="false" src="d0_s12" to="o620" />
    </statement>
    <statement id="d0_s13">
      <text>sessile.</text>
      <biological_entity id="o617" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" />
        <character is_modifier="false" name="course" src="d0_s12" value="curved" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s12" value="contorted" />
        <character constraint="in age" constraintid="o618" is_modifier="false" modifier="sometimes" name="texture" src="d0_s12" value="woody" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="cylindrical" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s12" value="4-angled" />
        <character constraint="from base" constraintid="o619" is_modifier="false" modifier="gradually" name="shape" src="d0_s12" value="tapering" />
        <character is_modifier="false" name="dehiscence" notes="" src="d0_s12" value="dehiscent" />
        <character modifier="throughout; nearly throughout" name="quantity" src="d0_s12" value="1/2" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds numerous, in 1 row per locule, obovoid, surface minutely alveolate, but appearing smooth.</text>
      <biological_entity id="o621" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="numerous" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="obovoid" />
      </biological_entity>
      <biological_entity id="o622" name="row" name_original="row" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" />
      </biological_entity>
      <biological_entity id="o623" name="locule" name_original="locule" src="d0_s14" type="structure" />
      <relation from="o621" id="r152" name="in" negation="false" src="d0_s14" to="o622" />
      <relation from="o622" id="r153" name="per" negation="false" src="d0_s14" to="o623" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 14, 28.</text>
      <biological_entity id="o624" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s14" value="alveolate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o625" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" />
        <character name="quantity" src="d0_s15" value="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 8 (7 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w, c North America, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="c North America" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Section Anogra consists of eight species (17 taxa) native to western North America including Mexico, found usually in dry, sandy soil in a wide variety of habitats in the Chihuahuan, Great Basin, Mojave, and Sonoran deserts, to grasslands and open sites in montane forest, -50 to 3300 m.  Only one species, Oenothera wigginsii Klein, occurs entirely outside the United States, while four others occur within the flora area but extend into northern Mexico.  Section Anogra is included within a strongly supported clade with the two species of sect. Kleinia in recent molecular studies (R. A. Levin et al. 2004; M. E. K. Evans et al. 2005, 2009).  The support levels for the topology within this clade are generally very weak, with only a few taxa grouping into moderately to strongly supported groups (for example, members of O. pallida complex, O. deltoides + O. wigginsii, O. californica + O. arizonica and O. neomexicana).</discussion>
  <discussion>Species of sect. Anogra have vespertine flowers that are outcrossed and pollinated by hawkmoths or have flowers that are partly autogamous (D. P. Gregory 1964; W. M. Klein 1964, 1970).  In Oenothera deltoides the capsule valves split open widely and disperse seeds, while the entire plant forms a so-called tumbleweed.  Other species in the section appear to have more passive seed dispersal; the capsules dehisce while the plant remains rooted.  The basal rosette may not be evident at time of flowering or not developed.  When this is the case, or when the dimensions of the basal leaves are very similar to the cauline ones, only one range for leaf dimensions is given.</discussion>
  <references>
    <reference>Klein, W. M.  1964.  A Biosystematic Study of Four Species of Oenothera Subgenus Anogra.  Ph.D. dissertation.  Claremont Graduate School.</reference>
    <reference>Klein, W. M.  1970.  The evolution of three diploid species of Oenothera subgenus Anogra (Onagraceae).  Evolution 24: 578–597.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs winter-annual or short-lived perennial from a taproot.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals conspicuously maroon-spotted.</description>
      <determination>64. Oenothera arizonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepals without maroon spots.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants villous throughout, also strigillose on leaves and distal parts; leaf blade margins coarsely repand-dentate or -pinnatifid.</description>
      <determination>62. Oenothera engelmannii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants villous, strigillose, or glabrous, sometimes more densely villous or strigillose distally; leaf blade margins subentire to sinuate-dentate or remotely denticulate, sometimes pinnatifid.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules 2.5–5 mm diam.; sepals (13–)15–35 mm.</description>
      <determination>66. Oenothera deltoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules 1.5–2.5 mm diam.; sepals 10–18 mm.</description>
      <determination>67. Oenothera pallida</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs perennial, from a taproot, also with lateral roots producing adventitious shoots or with long, fleshy roots.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Buds fluted in distal 1/2, with free tips 1–9 mm; plants from relatively long, fleshy roots.</description>
      <determination>66. Oenothera deltoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Buds quadrangular in distal 1/2, with free tips 0–4 mm; plants from a taproot and with lateral roots producing adventitious shoots.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants glabrous, sometimes strigillose on leaves and/or glandular puberulent distally, at least on floral tube; leaf blades 0.3–0.6(–1) cm wide, narrowly oblong to oblong- lanceolate, margins usually entire.</description>
      <determination>61. Oenothera nuttallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants villous, strigillose, glabrate, or glabrous, not glandular puberulent; leaf blades (0.3–)1–2.5 cm wide, usually ovate, oblong to lanceolate or linear-lanceolate, oblanceolate, or spatulate, rarely rhombic-ovate, margins usually sinuate-dentate to pinnatifid or subentire, rarely entire.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Capsules erect or strongly ascending.</description>
      <determination>63. Oenothera neomexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Capsules spreading to reflexed.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Capsules 2–3.5 mm diam.; stems decumbent or ascending.</description>
      <determination>65. Oenothera californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Capsules 1.5–2.5 mm diam.; stems erect or ascending.</description>
      <determination>67. Oenothera pallida</determination>
    </key_statement>
  </key>
</bio:treatment>