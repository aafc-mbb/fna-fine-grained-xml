<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">HALORAGACEAE</taxon_name>
    <taxon_name authority="J. R. Forster &amp; G. Forster" date="1776" rank="genus">HALORAGIS</taxon_name>
    <taxon_name authority="(Banks ex Murray) Oken" date="1841" rank="species">erecta</taxon_name>
    <place_of_publication>
      <publication_title>Allg. Naturgesch.</publication_title>
      <place_in_publication>3: 1871.  1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family haloragaceae;genus haloragis;species erecta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cercodia</taxon_name>
    <taxon_name authority="Banks ex Murray" date="1780" rank="species">erecta</taxon_name>
    <place_of_publication>
      <publication_title>Commentat. Soc. Regiae Sci. Gott.</publication_title>
      <place_in_publication>3: 3, plate 1.  1780</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus cercodia;species erecta</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Erect seaberry</other_name>
  <discussion>Subspecies 2 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced, California; Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="California" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <description type="morphology">
    <statement id="d0_s0">
      <text>A.</text>
    </statement>
    <statement id="d0_s1">
      <text>E. Orchard (1975) recognized two subspecies of Haloragis erecta distinguished primarily on leaf characteristics.</text>
      <biological_entity id="o2558" name="haloragi" name_original="haloragis" src="d0_s1" type="structure">
        <character name="atypical_quantity" src="d0_s1" value="1975" />
        <character is_modifier="true" name="quantity" src="d0_s1" value="2" />
        <character constraint="on leaf" constraintid="o2559" is_modifier="false" name="prominence" src="d0_s1" value="distinguished" />
      </biological_entity>
      <biological_entity id="o2559" name="leaf" name_original="leaf" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="characteristic" value_original="characteristics" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>North American specimens have been called subsp.</text>
      <biological_entity id="o2560" name="subsp" name_original="subsp" src="d0_s2" type="structure">
        <character is_modifier="false" name="geographical_terms" src="d0_s2" value="north" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>erecta, which has relatively thin, lanceo­late to ovate leaf-blades, and margins with 20–45 teeth.</text>
      <biological_entity id="o2561" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s3" value="thin" />
        <character is_modifier="true" name="shape" src="d0_s3" value="ovate" />
      </biological_entity>
      <biological_entity id="o2563" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s3" to="45" />
      </biological_entity>
      <relation from="o2562" id="r569" name="with" negation="false" src="d0_s3" to="o2563" />
    </statement>
    <statement id="d0_s4">
      <text>Subspecies cartilaginea (Cheeseman) Orchard known from the North Cape Peninsula of the North Island of New Zealand and has relatively thick, orbiculate to broadly ovate leaf-blades and margins with 10–15 teeth (Orchard).</text>
      <biological_entity id="o2562" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o2564" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="relatively" name="width" src="d0_s4" value="thick" />
        <character is_modifier="true" modifier="of of known from the north cape peninsula the north island new zealand" name="shape" notes="[duplicate value]" src="d0_s4" value="orbiculate" />
        <character is_modifier="true" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s4" value="ovate" />
        <character char_type="range_value" from="orbiculate" is_modifier="true" modifier="of of known from the north cape peninsula the north island new zealand" name="shape" src="d0_s4" to="broadly ovate" />
      </biological_entity>
      <biological_entity id="o2565" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o2566" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <relation from="o2562" id="r570" modifier="of of known from the north cape peninsula the north island new zealand" name="has" negation="false" src="d0_s4" to="o2564" />
      <relation from="o2565" id="r571" modifier="of of known from the north cape peninsula the north island new zealand" name="with" negation="false" src="d0_s4" to="o2566" />
    </statement>
  </description>
  
</bio:treatment>