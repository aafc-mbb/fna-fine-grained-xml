<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 00:06:58</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OENOTHERA</taxon_name>
    <taxon_name authority="Munz in N. L. Britton et al." date="1965" rank="section">Kleinia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1840" rank="species">coronopifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 495.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section kleinia;species coronopifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anogra</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Britton" date="unknown" rank="species">coronopifolia</taxon_name>
    <taxon_hierarchy>genus anogra;species coronopifolia</taxon_hierarchy>
  </taxon_identification>
  <number>60.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, strigillose, usually also hirsute;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a taproot, lateral roots producing adventitious shoots.</text>
      <biological_entity id="o559" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigillose" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="hirsute" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o560" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o561" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s1" value="adventitious" />
      </biological_entity>
      <relation from="o560" id="r135" name="producing" negation="false" src="d0_s1" to="o561" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, 1–several from base, these unbranched to well-branched, 10–60 cm.</text>
      <biological_entity id="o562" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="ascending" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="erect" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" constraint="from base" constraintid="o563" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="architecture" notes="[duplicate value]" src="d0_s2" value="unbranched" />
        <character is_modifier="false" name="architecture" notes="[duplicate value]" src="d0_s2" value="well-branched" />
        <character char_type="range_value" from="unbranched" name="architecture" notes="" src="d0_s2" to="well-branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o563" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in a weakly developed basal rosette and cauline, 2–7 × 0.2–1.5 cm, axillary fascicles of reduced leaves often present;</text>
      <biological_entity id="o564" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="cauline" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" />
        <character constraint="of leaves" constraintid="o566" is_modifier="false" name="arrangement" src="d0_s3" value="fascicles" />
      </biological_entity>
      <biological_entity constraint="basal" id="o565" name="rosette" name_original="rosette" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="weakly" name="development" src="d0_s3" value="developed" />
      </biological_entity>
      <biological_entity id="o566" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="reduced" />
        <character is_modifier="false" modifier="often" name="presence" src="d0_s3" value="absent" />
      </biological_entity>
      <relation from="o564" id="r136" name="in" negation="false" src="d0_s3" to="o565" />
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate to oblong, margins usually pinnatifid, sometimes proximal ones coarsely few-toothed.</text>
      <biological_entity id="o567" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="oblanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="oblong" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="oblong" />
      </biological_entity>
      <biological_entity id="o568" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="pinnatifid" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s4" value="proximal" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="few-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 1–3 opening per day near sunset;</text>
      <biological_entity id="o569" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o570" name="day" name_original="day" src="d0_s5" type="structure" />
      <biological_entity id="o571" name="sunset" name_original="sunset" src="d0_s5" type="structure" />
      <relation from="o569" id="r137" name="opening per" negation="false" src="d0_s5" to="o570" />
      <relation from="o569" id="r138" name="near" negation="false" src="d0_s5" to="o571" />
    </statement>
    <statement id="d0_s6">
      <text>buds nodding, weakly quadrangular, without free tips;</text>
      <biological_entity id="o572" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s6" value="quadrangular" />
      </biological_entity>
      <biological_entity id="o573" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="free" />
      </biological_entity>
      <relation from="o572" id="r139" name="without" negation="false" src="d0_s6" to="o573" />
    </statement>
    <statement id="d0_s7">
      <text>floral-tube 10–25 mm, mouth conspicuously pubescent, closed with straight, white hairs, 1–2 mm;</text>
      <biological_entity id="o574" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o575" name="mouth" name_original="mouth" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s7" value="pubescent" />
        <character constraint="with hairs" constraintid="o576" is_modifier="false" name="condition" src="d0_s7" value="closed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o576" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="course" src="d0_s7" value="straight" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 10–20 mm;</text>
      <biological_entity id="o577" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white, fading pink, ovate or shallowly obcordate, 10–15 (–20) mm;</text>
      <biological_entity id="o578" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="fading pink" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s9" value="obcordate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 10–15 mm, anthers 4–7 mm;</text>
      <biological_entity id="o579" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o580" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 17–42 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o581" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s11" to="42" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o582" name="stigma" name_original="stigma" src="d0_s11" type="structure" />
      <biological_entity id="o583" name="anther" name_original="anthers" src="d0_s11" type="structure" />
      <relation from="o582" id="r140" name="exserted beyond" negation="false" src="d0_s11" to="o583" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules ascending to erect, straight, fusiform, weakly 4-angled, 10–20 × 3–5 mm, dehiscent 1/2 their length;</text>
    </statement>
    <statement id="d0_s13">
      <text>sessile.</text>
      <biological_entity id="o584" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s12" value="ascending" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s12" value="erect" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s12" to="erect" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" />
        <character is_modifier="false" name="shape" src="d0_s12" value="fusiform" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s12" value="4-angled" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s12" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="dehiscent" />
        <character name="length" src="d0_s12" value="1/2" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds in 2 rows per locule, ellipsoid to subglobose, 1.5–2 × 1.2–1.5 mm, surface regularly pitted, pits in longitudinal lines.</text>
      <biological_entity id="o585" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="ellipsoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s14" value="subglobose" />
        <character char_type="range_value" from="ellipsoid" name="shape" notes="" src="d0_s14" to="subglobose" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s14" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o586" name="row" name_original="rows" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="2" />
      </biological_entity>
      <biological_entity id="o587" name="locule" name_original="locule" src="d0_s14" type="structure" />
      <biological_entity id="o588" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="regularly" name="relief" src="d0_s14" value="pitted" />
      </biological_entity>
      <biological_entity id="o590" name="line" name_original="lines" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" />
      </biological_entity>
      <relation from="o585" id="r141" name="in" negation="false" src="d0_s14" to="o586" />
      <relation from="o586" id="r142" name="per" negation="false" src="d0_s14" to="o587" />
      <relation from="o589" id="r143" name="in" negation="false" src="d0_s14" to="o590" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 14, 28.</text>
      <biological_entity id="o589" name="pit" name_original="pits" src="d0_s14" type="structure" />
      <biological_entity constraint="2n" id="o591" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" />
        <character name="quantity" src="d0_s15" value="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oenothera coronopifolia apparently has both self-incompatible and self-compatible populations (P. H. Raven 1979; W. L. Wagner et al. 2007; K. E. Theiss et al. 2010).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open sites, grassy meadows, slopes, along drainages, foothills and mountains.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="open sites" />
        <character name="habitat" value="grassy meadows" />
        <character name="habitat" value="drainages" modifier="slopes along" />
        <character name="habitat" value="foothills" />
        <character name="habitat" value="mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–3000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Idaho, Nebr., N.Mex., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>