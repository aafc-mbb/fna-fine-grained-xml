<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hoffmannsegg &amp; Link" date="unknown" rank="family">POLYGALACEAE</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavon" date="1798" rank="genus">MONNINA</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 31.  1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus monnina;species wrightii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pteromonnina</taxon_name>
    <taxon_name authority="(A. Gray) B. Eriksen" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_hierarchy>genus pteromonnina;species wrightii</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Blue pygmyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs (2–) 3–6 (–7) dm.</text>
      <biological_entity id="o1406" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="7" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched distally, hairs in­curved.</text>
      <biological_entity id="o1407" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" />
      </biological_entity>
      <biological_entity id="o1408" name="hair" name_original="hairs" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1–2.5 mm;</text>
      <biological_entity id="o1409" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1410" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade mostly lanceolate or narrowly ovate to elliptic, sometimes obovate proximally, 15–60 × 2–10 mm, base cuneate, apex usually acute to attenuate-acuminate, sometimes obtuse proximally.</text>
      <biological_entity id="o1411" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1412" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" notes="[duplicate value]" src="d0_s3" value="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s3" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="elliptic" />
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s3" to="elliptic" />
        <character is_modifier="false" modifier="sometimes; proximally" name="shape" src="d0_s3" value="obovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1413" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" />
      </biological_entity>
      <biological_entity id="o1414" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" notes="[duplicate value]" src="d0_s3" value="acute" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s3" value="attenuate-acuminate" />
        <character char_type="range_value" from="usually acute" name="shape" src="d0_s3" to="attenuate-acuminate" />
        <character is_modifier="false" modifier="sometimes; proximally" name="shape" src="d0_s3" value="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Racemes 2–20 (–30) × 0.3–0.6 cm;</text>
      <biological_entity id="o1415" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s4" to="0.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncle to 6 cm;</text>
      <biological_entity id="o1416" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts narrowly lanceolate or ovate-subulate.</text>
      <biological_entity id="o1417" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.4–0.5 mm, pubescent.</text>
      <biological_entity id="o1418" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s7" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cleistogamous flowers usually present on much-reduced axillary branches, chasmogamous flowers sometimes absent, 1.5–2.1 mm.</text>
      <biological_entity id="o1419" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="cleistogamous" />
        <character constraint="on axillary branches" constraintid="o1420" is_modifier="false" modifier="usually" name="presence" src="d0_s8" value="absent" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o1420" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="much-reduced" />
      </biological_entity>
      <biological_entity id="o1421" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="chasmogamous" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s8" value="absent" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: non-wing sepals ovate to lanceolate, 1.2–1.8 mm;</text>
      <biological_entity id="o1422" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="non-wing" id="o1423" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s9" value="ovate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s9" value="lanceolate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>wings obovate, 1–2 mm wide;</text>
      <biological_entity id="o1424" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1425" name="wing" name_original="wings" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>keel 1.5–3 mm.</text>
      <biological_entity id="o1426" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1427" name="keel" name_original="keel" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits ovoid, subglobose, or subobovoid, 3–5 × 2.8–4 mm.</text>
      <biological_entity id="o1428" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subobovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subobovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 2–3 mm.</text>
      <biological_entity id="o1429" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Monnina wrightii is amphitropically disjunct in Argentina and Bolivia, with single collections each from Peru and Uruguay.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (mid–)late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
        <character name="flowering time" char_type="atypical_range" to="fall" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Igneous, lime­stone, gravelly, sandy, or silty loam soils, in dry to moist, open to shaded grasslands, shrublands, woodlands, disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="igneous" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="loam soils" modifier="or silty" />
        <character name="habitat" value="in dry to moist" />
        <character name="habitat" value="open to shaded grasslands" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua, Colima, Durango, Guanajuato, Jalisco, Michoacán, Nayarit, Sinaloa, Sonora, Zacatecas); South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Colima)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Guanajuato)" establishment_means="native" />
        <character name="distribution" value="Mexico (Jalisco)" establishment_means="native" />
        <character name="distribution" value="Mexico (Michoacán)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nayarit)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>