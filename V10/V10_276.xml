<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 01:15:51</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Endlicher" date="1830" rank="tribe">Epilobieae</taxon_name>
    <taxon_name authority="Séguier" date="1754" rank="genus">CHAMAENERION</taxon_name>
    <taxon_name authority="(Linnaeus) Scopoli" date="1771" rank="species">angustifolium</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carniol. ed.</publication_title>
      <place_in_publication>2, 1: 271.  1771</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus chamaenerion;species angustifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Epilobium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">angustifolium</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 347.  1753</place_in_publication>
      <other_info_on_pub>(as angustifolia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus epilobium;species angustifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamerion</taxon_name>
    <taxon_name authority="(Linnaeus) Holub" date="unknown" rank="species">angustifolium</taxon_name>
    <taxon_hierarchy>genus chamerion;species angustifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrogennema</taxon_name>
    <taxon_name authority="(Linnaeus) Lunell" date="unknown" rank="species">angustifolium</taxon_name>
    <taxon_hierarchy>genus pyrogennema;species angustifolium</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Fireweed</other_name>
  <other_name type="common_name">rosebay willowherb</other_name>
  <other_name type="common_name">épilobe à feuilles étroites</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs robust, caudex woody, often forming large clones by horizontal rhizomes 0.5–2 cmdiam., extending 0.5–5 m, forming shoots from caudex and along rhizomes.</text>
      <biological_entity id="o3032" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" />
      </biological_entity>
      <biological_entity id="o3033" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="false" name="texture" src="d0_s0" value="woody" />
      </biological_entity>
      <biological_entity id="o3034" name="clone" name_original="clones" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="large" />
      </biological_entity>
      <biological_entity id="o3035" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="horizontal" />
        <character char_type="range_value" from="0.5" name="quantity" src="d0_s0" to="2" />
      </biological_entity>
      <biological_entity id="o3036" name="shoot" name_original="shoots" src="d0_s0" type="structure" />
      <biological_entity id="o3037" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o3033" id="r483" modifier="often" name="forming" negation="false" src="d0_s0" to="o3034" />
      <relation from="o3033" id="r484" modifier="often" name="by" negation="false" src="d0_s0" to="o3035" />
      <relation from="o3033" id="r485" name="forming" negation="false" src="d0_s0" to="o3036" />
      <relation from="o3033" id="r486" name="from caudex and along" negation="false" src="d0_s0" to="o3037" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, terete, usually unbranched, rarely branched distally, 20–200 cm, glabrous or sparsely to densely strigillose, often exfo­liating proximally.</text>
      <biological_entity id="o3038" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="unbranched" />
        <character is_modifier="false" modifier="rarely; distally" name="architecture" src="d0_s1" value="branched" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="200" to_unit="cm" />
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s1" value="glabrous" />
        <character is_modifier="false" modifier="sparsely densely" name="pubescence" notes="[duplicate value]" src="d0_s1" value="strigillose" />
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s1" to="sparsely densely strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: usually spirally arranged, very rarely subopposite proximally;</text>
      <biological_entity id="o3039" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually spirally" name="arrangement" src="d0_s2" value="arranged" />
        <character is_modifier="false" modifier="very rarely; proximally" name="arrangement" src="d0_s2" value="subopposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0–7 mm;</text>
      <biological_entity id="o3040" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3041" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade lanceolate to linear or proximally oblong to obovate, (3–) 5–23 × 0.3–3.4 cm, proximal ones much reduced, base obtuse or cuneate to attenuate, margins entire or scarcely denticulate, apex attenuate-acute, lateral-veins 10–25 on each side of midrib, diverging nearly at right angles, confluent into ± prominent submarginal vein, surfaces glabrous or strigillose on abaxial midrib;</text>
      <biological_entity id="o3042" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3043" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="linear" />
        <character is_modifier="false" modifier="proximally" name="shape" notes="[duplicate value]" src="d0_s4" value="oblong" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="obovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear or proximally oblong" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="23" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s4" to="3.4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3044" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s4" value="reduced" />
      </biological_entity>
      <biological_entity id="o3045" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="obtuse" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="cuneate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="attenuate" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
      </biological_entity>
      <biological_entity id="o3046" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" />
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s4" value="denticulate" />
      </biological_entity>
      <biological_entity id="o3047" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate-acute" />
      </biological_entity>
      <biological_entity id="o3048" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o3049" from="10" name="quantity" src="d0_s4" to="25" />
        <character constraint="at angles" constraintid="o3051" is_modifier="false" name="orientation" notes="" src="d0_s4" value="diverging" />
        <character constraint="into submarginal vein" constraintid="o3052" is_modifier="false" name="arrangement" notes="" src="d0_s4" value="confluent" />
      </biological_entity>
      <biological_entity id="o3049" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o3050" name="midrib" name_original="midrib" src="d0_s4" type="structure" />
      <biological_entity id="o3051" name="angle" name_original="angles" src="d0_s4" type="structure" />
      <biological_entity constraint="submarginal" id="o3052" name="vein" name_original="vein" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="prominence" src="d0_s4" value="prominent" />
      </biological_entity>
      <biological_entity id="o3053" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" />
        <character constraint="on abaxial midrib" constraintid="o3054" is_modifier="false" name="pubescence" src="d0_s4" value="strigillose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3054" name="midrib" name_original="midrib" src="d0_s4" type="structure" />
      <relation from="o3049" id="r487" name="part_of" negation="false" src="d0_s4" to="o3050" />
    </statement>
    <statement id="d0_s5">
      <text>bracts much reduced and linear.</text>
      <biological_entity id="o3055" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o3056" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s5" value="reduced" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences erect racemes, 5–50 cm, glabrous or strigillose.</text>
      <biological_entity id="o3057" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" notes="" src="d0_s6" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="strigillose" />
      </biological_entity>
      <biological_entity id="o3058" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers nodding in bud, erect at anthesis, opening laterally;</text>
      <biological_entity id="o3059" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character constraint="in bud" constraintid="o3060" is_modifier="false" name="orientation" src="d0_s7" value="nodding" />
        <character constraint="at anthesis" is_modifier="false" name="orientation" notes="" src="d0_s7" value="erect" />
      </biological_entity>
      <biological_entity id="o3060" name="bud" name_original="bud" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>buds oblong to oblanceoloid, 7–14 × 3–35 mm;</text>
      <biological_entity id="o3061" name="bud" name_original="buds" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="oblong" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="oblanceoloid" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="oblanceoloid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals purplish green, oblong-lanceolate or upper pair somewhat oblanceolate, 8–19 × 1.5–3 mm, base usually attenuate, rarely ± clawed, apex acuminate or attenuate, canescent abaxially, glabrous adaxially;</text>
      <biological_entity id="o3062" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong-lanceolate" />
        <character is_modifier="false" name="position" src="d0_s9" value="upper" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s9" value="oblanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="19" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3063" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s9" value="attenuate" />
        <character is_modifier="false" modifier="rarely more or less" name="shape" src="d0_s9" value="clawed" />
      </biological_entity>
      <biological_entity id="o3064" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="attenuate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="canescent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s9" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals usually rose-purple to pale-pink, very rarely white, obovate to narrowly obovate or nearly rotund, 9–25 × 3–15 mm, upper pair often ± longer and broader, base attenuate, apex entire or shallowly emarginate;</text>
      <biological_entity id="o3065" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" notes="[duplicate value]" src="d0_s10" value="rose-purple" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s10" value="pale-pink" />
        <character char_type="range_value" from="usually rose-purple" name="coloration" src="d0_s10" to="pale-pink" />
        <character is_modifier="false" modifier="very rarely" name="coloration" src="d0_s10" value="white" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s10" value="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s10" value="obovate" />
        <character is_modifier="false" modifier="nearly" name="shape" notes="[duplicate value]" src="d0_s10" value="rotund" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s10" to="narrowly obovate or nearly rotund" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o3066" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often more or less" name="length_or_size" src="d0_s10" value="longer" />
        <character is_modifier="false" name="width" src="d0_s10" value="broader" />
      </biological_entity>
      <biological_entity id="o3067" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="attenuate" />
      </biological_entity>
      <biological_entity id="o3068" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s10" value="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments pink, 7–15 mm, subequal;</text>
      <biological_entity id="o3069" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers red to rose-purple, oblong, 2–3 × 1–1.7 mm;</text>
      <biological_entity id="o3070" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s12" value="red" />
        <character is_modifier="false" name="coloration" notes="[duplicate value]" src="d0_s12" value="rose-purple" />
        <character char_type="range_value" from="red" name="coloration" src="d0_s12" to="rose-purple" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary 6–25 mm, surface densely canescent;</text>
      <biological_entity id="o3071" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3072" name="surface" name_original="surface" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>nectary disc raised 0.5–1 mm on ovary apex, 2–4 mm diam., smooth or slightly 4-lobed, glabrous;</text>
      <biological_entity constraint="nectary" id="o3073" name="disc" name_original="disc" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" modifier="on ovary apex" name="diameter" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="4-lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style sharply deflexed initially, later erect, white or flushed pink, 8–16 mm, proximally villous;</text>
      <biological_entity id="o3074" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sharply; initially" name="orientation" src="d0_s15" value="deflexed" />
        <character is_modifier="false" modifier="later" name="orientation" src="d0_s15" value="erect" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="flushed pink" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s15" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma spreading to revolute, lobes 3–6 × 0.6–1 mm, surfaces white, densely dry-papillose.</text>
      <biological_entity id="o3075" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="spreading" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s16" value="revolute" />
      </biological_entity>
      <biological_entity id="o3076" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s16" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3077" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s16" value="dry-papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules 4–9.5 cm, densely appressed-canescent;</text>
    </statement>
    <statement id="d0_s18">
      <text>ped­icel 0.5–3 cm.</text>
      <biological_entity id="o3078" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s17" to="9.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s17" value="appressed-canescent" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s18" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds narrowly obovoid to oblong, 0.9–1.3 × 0.3–0.4 mm, chalazal end tapering abruptly to very short neck (to 0.1 mm), surface appearing smooth and often shiny, but irregularly reticulate;</text>
      <biological_entity id="o3079" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="[duplicate value]" src="d0_s19" value="obovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s19" value="oblong" />
        <character char_type="range_value" from="narrowly obovoid" name="shape" src="d0_s19" to="oblong" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s19" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s19" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="chalazal" id="o3080" name="end" name_original="end" src="d0_s19" type="structure">
        <character constraint="to neck" constraintid="o3081" is_modifier="false" name="shape" src="d0_s19" value="tapering" />
      </biological_entity>
      <biological_entity id="o3081" name="neck" name_original="neck" src="d0_s19" type="structure">
        <character is_modifier="true" modifier="very" name="height_or_length_or_size" src="d0_s19" value="short" />
      </biological_entity>
      <biological_entity id="o3082" name="surface" name_original="surface" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" />
        <character is_modifier="false" modifier="often" name="reflectance" src="d0_s19" value="shiny" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_coloration_or_relief" src="d0_s19" value="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>coma 10–17 mm, white or dingy, dense, not easily detached.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 36, 72, 108.</text>
      <biological_entity id="o3083" name="coma" name_original="coma" src="d0_s20" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s20" to="17" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="white" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="dingy" />
        <character is_modifier="false" name="density" src="d0_s20" value="dense" />
        <character is_modifier="false" modifier="not easily" name="fusion" src="d0_s20" value="detached" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3084" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="36" />
        <character name="quantity" src="d0_s21" value="72" />
        <character name="quantity" src="d0_s21" value="108" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, n Mexico, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chamaenerion angustifolium, which in North America is known mostly as fireweed, is a widespread circumpolar, circumboreal species that can be locally dominant, often in disturbed habitats, particularly following fires (T. Mosquin 1966, 1967; G. Henderson et al. 1979).  In addition to producing numerous, highly vagile, comose seeds, aggressive rhizomatous growth enables it to survive clonally and spread rapidly after fires (L. E. Vodolazskij 1979).  As succession proceeds, production of the familiar spikes of magenta flowers is inhibited, but fireweed often persists in non-flowering condition even in relatively mature forests, where it is able to sprout and spread quickly following disturb­ance.  The species is one of the few within the genus and tribe to form true rhizomes (R. C. Keating et al. 1982), enabling it to colonize large areas very rapidly.</discussion>
  <discussion>Polyploidy is well documented in Chamaenerion angustifolium, but unlike the similar situation in C. latifolium (T. Mosquin and E. Small 1971), diploid and tetraploid populations differ morphologically and have partially overlapping, but distinct, geographical ranges (Mosquin 1966; Chen C. J. et al. 1992).  The diploid subsp. angustifolium occupies the northern part of its range, in North America across Canada and interior Alaska, and in Asia across Siberia and northern Europe, ranging southward at higher elevations.  Farther south in Eurasia and North America, it is replaced by the tetraploid subsp. circumvagum (Mosquin 1966).  Hexaploid (n = 54) populations have been detected only in Japan, and cannot be distinguished morphologically from tetraploids.  B. C. Husband and D. W. Schemske (1998), Husband and H. A. Sabara (2004), and Sabara et al. (2013) have documented population segregation and mixing in contact areas between the two ploidy levels, including populations with a single cytotype, a high proportion of mixed populations, and presence of triploids (n = 27), but at low levels, indicating strong reproductive isolation between cytotypes.</discussion>
  <discussion>The floral phenology of Chamaenerion angustifolium was described in 1793 by C. K. Sprengel (P. Knuth 1906–1909, vol. 2) as a classic example of protandry.  The flowers are presented in tall spikes, opening from the base of the spike, initially with stamens erect and style sharply reflexed.  By the second or third day after opening, the stamens reflex as the style straightens into the floral axis, the four lobes of the stigma spread open, and nectaries commence production.  Bees start at the lowest flowers in search of nectar, moving up the spike until there is no more nectar, by which time they are well dusted with pollen, which they bring to the lower functionally (female) flowers of the next spike.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves subsessile, leaf blades glabrous on abaxial midrib, (3–)7–14(–18.5) × (0.3–)0.7–1.3(–2.5) cm, base rounded to cuneate, margins usually entire, rarely low-denticulate; stems subglabrous; petals 9–15(–19) × 3–9(–11) mm; pollen usually 3-porate, to 75 µm diam.</description>
      <determination>2a. Chamaenerion angustifolium subsp. angustifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves with petioles 2–7 mm, leaf blades usually strigillose, rarely glabrous on abaxial midrib, (6–)9–23 × (0.7–)1.5–3.4 cm, base subcuneate to attenuate, margins ± denticulate; stems strigillose, usually glabrous proximally; petals 14–25 × 7–14 mm; pollen mixed 3- and 4-porate (rarely 5-porate), more than 75 µm diam.</description>
      <determination>2b. Chamaenerion angustifolium subsp. circumvagum</determination>
    </key_statement>
  </key>
</bio:treatment>