<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">COMBRETACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CONOCARPUS</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">erectus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 176.  1753</place_in_publication>
      <other_info_on_pub>(as erecta)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family combretaceae;genus conocarpus;species erectus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Conocarpus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">erectus</taxon_name>
    <taxon_name authority="Fors ex de Candolle" date="unknown" rank="variety">sericeus</taxon_name>
    <taxon_hierarchy>genus conocarpus;species erectus;variety sericeus</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Buttonwood</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees to 12 (–20) m, sometimes ± prostrate.</text>
      <biological_entity id="o1769" name="shrub" name_original="shrubs" src="d0_s0" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="12" to_unit="m" />
        <character is_modifier="false" modifier="sometimes more or less" name="growth_form_or_orientation" src="d0_s0" value="prostrate" />
      </biological_entity>
      <biological_entity id="o1770" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="12" to_unit="m" />
        <character is_modifier="false" modifier="sometimes more or less" name="growth_form_or_orientation" src="d0_s0" value="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 1.5–16 mm;</text>
      <biological_entity id="o1771" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o1772" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s1" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade elliptic to obovate, 1.5–10 × 0.5–4 [–5] cm, smaller in terminal inflorescences, base attenuate to cuneate, surfaces glabrous or sparsely to densely pubescent, hairs appressed to ± erect, straight to crisped/tangled, combretaceous, some also minute, glandular.</text>
      <biological_entity id="o1773" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1774" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="obovate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="obovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
        <character constraint="in terminal inflorescences" constraintid="o1775" is_modifier="false" name="size" src="d0_s2" value="smaller" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o1775" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o1776" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="attenuate" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s2" value="cuneate" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s2" to="cuneate" />
      </biological_entity>
      <biological_entity id="o1777" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" notes="[duplicate value]" src="d0_s2" value="glabrous" />
        <character is_modifier="false" modifier="sparsely densely" name="pubescence" notes="[duplicate value]" src="d0_s2" value="pubescent" />
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s2" to="sparsely densely pubescent" />
      </biological_entity>
      <biological_entity id="o1778" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s2" value="appressed" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="[duplicate value]" src="d0_s2" value="erect" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s2" to="more or less erect" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="tangled" value_original="crisped/tangled" />
        <character is_modifier="false" name="size" src="d0_s2" value="minute" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemes or panicles of spherical or oblong heads (appearing paniculate), branches reproductive 3–25 cm distally, axis of lateral inflorescence 1–10 cm, heads conelike, 4–9 × 4–8 mm.</text>
      <biological_entity id="o1779" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o1780" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o1781" name="panicle" name_original="panicles" src="d0_s3" type="structure" />
      <biological_entity id="o1782" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="spherical" />
        <character is_modifier="true" name="shape" src="d0_s3" value="oblong" />
      </biological_entity>
      <biological_entity id="o1783" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="function_or_structure_subtype" src="d0_s3" value="reproductive" />
        <character char_type="range_value" from="3" from_unit="cm" modifier="distally" name="some_measurement" src="d0_s3" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1784" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o1785" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <biological_entity id="o1786" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="conelike" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o1779" id="r376" name="part_of" negation="false" src="d0_s3" to="o1782" />
      <relation from="o1780" id="r377" name="part_of" negation="false" src="d0_s3" to="o1782" />
      <relation from="o1781" id="r378" name="part_of" negation="false" src="d0_s3" to="o1782" />
      <relation from="o1784" id="r379" name="part_of" negation="false" src="d0_s3" to="o1785" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals 0.4–0.8 mm;</text>
      <biological_entity id="o1787" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o1788" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens 1.8–3.5 mm;</text>
      <biological_entity id="o1789" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1790" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>style 1–1.8 mm.</text>
      <biological_entity id="o1791" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1792" name="style" name_original="style" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Drupes greenish or brown, 2.5–4.5 × 2.7–5.5 mm, pubescent distally;</text>
    </statement>
    <statement id="d0_s8">
      <text>clusters 6–12 mm. 2n = 24.</text>
      <biological_entity id="o1793" name="drupe" name_original="drupes" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s7" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="width" src="d0_s7" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s7" value="pubescent" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="cluster" value_original="clusters" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1794" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In Florida, Conocarpus erectus is distributed from the Keys northward along the Gulf Coast to Levy County and along the Atlantic Coast to Volusia County.  It has recently been reported from Willacy County, Texas.</discussion>
  <discussion>Conocarpus erectus is commonly present on the landward margins of mangrove communities, as a mangrove associate.  Although salt tolerant, it lacks obvious biological specializations of the mangrove community (for example, it is not viviparous and does not show root modifications).  It is also common in open wetland communities and in tropical hammocks (tree islands).  The densely packed flowers likely are generalist-insect pollinated; the fruits float and are dispersed by water.  Some plants are consistently and densely pubescent, others are consistently glabrous, and still others show intermediate conditions such as a few slightly pubescent leaves on an otherwise densely pubescent plant, a few sparsely pubescent leaves on an otherwise glabrous plant, and plants producing both pubescent and glabrous leaves in a cyclic pattern.  The densely pubescent form, “silver-leaved buttonwood,” is a common ornamental in coastal regions, and its hairs give the leaves a gray to silver, occasionally even golden, coloration.  Conocarpus erectus is extremely variable in stature; some plants form low-growing and almost prostrate shrubs, whereas others are erect trees.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>in coastal and tropical areas..</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal" modifier="in" />
        <character name="habitat" value="tropical areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_1">
      <text>in coastal and tropical areas..</text>
      <biological_entity id="hab_o1" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal" modifier="in" />
        <character name="habitat" value="tropical areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Tex.; Mexico; West Indies; South America; w Africa; in coastal and tropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="w Africa" establishment_means="native" />
        <character name="distribution" value="in coastal and tropical areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>