<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/09 02:18:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">LYTHRACEAE</taxon_name>
    <taxon_name authority="P. Browne" date="1756" rank="genus">CUPHEA</taxon_name>
    <taxon_name authority="(Jacquin) J. F. Macbride" date="1930" rank="species">carthagenensis</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Field Mus. Nat. Hist., Bot. Ser.</publication_title>
      <place_in_publication>8: 124.  1930</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus cuphea;species carthagenensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lythrum</taxon_name>
    <taxon_name authority="Jacquin" date="1760" rank="species">carthagenense</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Syst. Pl.,</publication_title>
      <place_in_publication>22.  1760</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus lythrum;species carthagenense</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Balsamona</taxon_name>
    <taxon_name authority="Vandelli" date="unknown" rank="species">pinto</taxon_name>
    <taxon_hierarchy>genus balsamona;species pinto</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cuphea</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="species">balsamona</taxon_name>
    <taxon_hierarchy>genus cuphea;species balsamona</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parsonsia</taxon_name>
    <taxon_name authority="(Vandelli) A. Heller" date="unknown" rank="species">pinto</taxon_name>
    <taxon_hierarchy>genus parsonsia;species pinto</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, [subshrubs], 1–6 dm, with fibrous-roots.</text>
      <biological_entity id="o3302" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o3303" name="fibrou-root" name_original="fibrous-roots" src="d0_s0" type="structure" />
      <relation from="o3302" id="r402" name="with" negation="false" src="d0_s0" to="o3303" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to decumbent and spreading, usually much-branched, hispid and setose, sometimes also puberulent.</text>
      <biological_entity id="o3304" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="erect" />
        <character is_modifier="false" name="orientation" notes="[duplicate value]" src="d0_s1" value="decumbent" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="much-branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="setose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite, subsessile or sessile;</text>
      <biological_entity id="o3305" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0–2 mm;</text>
      <biological_entity id="o3306" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly elliptic to lanceolate, 12–55 × 5–25 mm, base attenuate.</text>
      <biological_entity id="o3307" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s4" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s4" value="lanceolate" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s4" to="55" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3308" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes leafy.</text>
      <biological_entity id="o3309" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–2 mm.</text>
      <biological_entity id="o3310" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers alternate, 1 interpet­iolar, with 1–3 flowers on axillary branchlets;</text>
      <biological_entity id="o3311" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="alternate" />
        <character name="quantity" src="d0_s7" value="1" />
      </biological_entity>
      <biological_entity id="o3312" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o3313" name="branchlet" name_original="branchlets" src="d0_s7" type="structure" />
      <relation from="o3311" id="r403" name="with" negation="false" src="d0_s7" to="o3312" />
      <relation from="o3312" id="r404" name="on" negation="false" src="d0_s7" to="o3313" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube purple adaxially and distally, or green throughout, 4–6 × 1–1.5 mm, glabrous except veins sparsely and coarsely setose;</text>
      <biological_entity id="o3314" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration_or_density" src="d0_s8" value="purple" />
        <character is_modifier="false" modifier="distally; throughout" name="coloration" src="d0_s8" value="green" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
        <character constraint="except veins" constraintid="o3315" is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" notes="" src="d0_s8" value="setose" />
      </biological_entity>
      <biological_entity id="o3315" name="vein" name_original="veins" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>base rounded or a descending spur, 0.5 mm;</text>
      <biological_entity id="o3316" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" />
        <character name="some_measurement" notes="" src="d0_s9" unit="mm" value="0.5" />
      </biological_entity>
      <biological_entity id="o3317" name="spur" name_original="spur" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="descending" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner surface glabrous;</text>
      <biological_entity constraint="inner" id="o3318" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>epicalyx segments thick, often terminated by a bristle;</text>
      <biological_entity constraint="epicalyx" id="o3319" name="segment" name_original="segments" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thick" />
      </biological_entity>
      <biological_entity id="o3320" name="bristle" name_original="bristle" src="d0_s11" type="structure" />
      <relation from="o3319" id="r405" modifier="often" name="terminated by a" negation="false" src="d0_s11" to="o3320" />
    </statement>
    <statement id="d0_s12">
      <text>sepals equal;</text>
      <biological_entity id="o3321" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 6, deep purple or rose-purple, subspatulate, subequal, 1.5–2.5 × 0.5–1 mm;</text>
      <biological_entity id="o3322" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="deep" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="rose-purple" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subspatulate" />
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s13" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 11, extending 2/3 distance to sinus of sepals.</text>
      <biological_entity id="o3323" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="11" />
      </biological_entity>
      <biological_entity id="o3324" name="distance" name_original="distance" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="2/3" />
      </biological_entity>
      <biological_entity id="o3325" name="sinus" name_original="sinus" src="d0_s14" type="structure" />
      <biological_entity id="o3326" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <relation from="o3323" id="r406" name="extending" negation="false" src="d0_s14" to="o3324" />
      <relation from="o3323" id="r407" name="to" negation="false" src="d0_s14" to="o3325" />
      <relation from="o3325" id="r408" name="part_of" negation="false" src="d0_s14" to="o3326" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds (4–) 6 (–9), elliptic to suborbiculate in outline, 1.5–1.7 × 0.2–1.5 mm, mar­gin narrow, flat­tened, thin.</text>
      <biological_entity id="o3328" name="outline" name_original="outline" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" notes="alterIDs:o3328" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 16.</text>
      <biological_entity id="o3327" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s15" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="9" />
        <character name="quantity" src="d0_s15" value="6" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s15" value="elliptic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s15" value="suborbiculate" />
        <character char_type="range_value" constraint="in outline" constraintid="o3328" from="elliptic" name="shape" src="d0_s15" to="suborbiculate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" notes="" src="d0_s15" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="narrow" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s15" value="flat" />
        <character is_modifier="false" name="width" src="d0_s15" value="thin" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3329" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The weedy, self-fertilizing Cuphea carthagenensis is the most widely distributed species of the genus and one of the more common in South America.  It was first collected in the United States in Florida and North Carolina in the 1920s.  Fossilized pollen very similar to pollen of C. carthagenensis and close relatives is known from the late Miocene of Alabama (S. A. Graham 2013).  The species flowers year-round in subtropical and tropical regions.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Atlantic and Gulf coastal plain, ditches, margins of moist woods, roadsides, moist open, disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal plain" modifier="atlantic and" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="margins" constraint="of moist woods , roadsides" />
        <character name="habitat" value="moist woods" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="open" modifier="moist" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Atlantic and Gulf coastal plain, ditches, margins of moist woods, roadsides, moist open, disturbed areas; Introduced; Ala., Ark., Fla., Ga., La., Miss., N.C., S.C., Tenn., Tex.; Mexico; Central America; South America; introduced also in Pacific Islands (Fiji, Guam, Hawaii, Philippines), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Atlantic and Gulf coastal plain" establishment_means="native" />
        <character name="distribution" value="ditches" establishment_means="native" />
        <character name="distribution" value="margins of moist woods" establishment_means="native" />
        <character name="distribution" value="roadsides" establishment_means="native" />
        <character name="distribution" value="moist open" establishment_means="native" />
        <character name="distribution" value="disturbed areas" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value=" also in Pacific Islands (Fiji)" establishment_means="introduced" />
        <character name="distribution" value=" also in Pacific Islands (Guam)" establishment_means="introduced" />
        <character name="distribution" value=" also in Pacific Islands (Hawaii)" establishment_means="introduced" />
        <character name="distribution" value=" also in Pacific Islands (Philippines)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>