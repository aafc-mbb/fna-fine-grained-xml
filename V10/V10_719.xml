<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="L’Heritier" date="1792" rank="genus">EUCALYPTUS</taxon_name>
    <taxon_name authority="Smith" date="1795" rank="species">robusta</taxon_name>
    <place_of_publication>
      <publication_title>Spec. Bot. New Holland,</publication_title>
      <place_in_publication>39, plate 13.  1795</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus eucalyptus;species robusta</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Swamp mahogany</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, to 30 m;</text>
      <biological_entity id="o3070" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="30" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk reddish-brown, rough;</text>
      <biological_entity id="o3071" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark persistent, deeply furrowed, soft, spongy, fibrous, rough on small branches.</text>
      <biological_entity id="o3072" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" />
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s2" value="furrowed" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="soft" />
        <character is_modifier="false" name="texture" src="d0_s2" value="spongy" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" />
        <character constraint="on branches" constraintid="o3073" is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="rough" />
      </biological_entity>
      <biological_entity id="o3073" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 1.5–3 cm, slightly flattened;</text>
      <biological_entity id="o3074" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3075" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade lighter green abaxially, broadly lanceolate, 8.5–17 × 2.5–7 cm, surfaces glossy.</text>
      <biological_entity id="o3076" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3077" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s4" value="lighter green" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="lanceolate" />
        <character char_type="range_value" from="8.5" from_unit="cm" name="length" src="d0_s4" to="17" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3078" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s4" value="glossy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles broadly flat­tened, 1.5–3 cm.</text>
      <biological_entity id="o3079" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="prominence_or_shape" src="d0_s5" value="flat" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 9–15-flowered, umbels.</text>
      <biological_entity id="o3080" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="9-15-flowered" />
      </biological_entity>
      <biological_entity id="o3081" name="umbel" name_original="umbels" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium obconic to pyriform, 6–7 mm, length ± equaling calyptra;</text>
      <biological_entity id="o3082" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3083" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="obconic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s7" value="pyriform" />
        <character char_type="range_value" from="obconic" name="shape" src="d0_s7" to="pyriform" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3084" name="calyptra" name_original="calyptra" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calyptra conic to rostrate, 10–12 mm;</text>
      <biological_entity id="o3085" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3086" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="conic" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s8" value="rostrate" />
        <character char_type="range_value" from="conic" name="shape" src="d0_s8" to="rostrate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens white.</text>
      <biological_entity id="o3087" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3088" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules cylindric, 10–18 mm, not glaucous;</text>
      <biological_entity id="o3089" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s10" value="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves 3 or 4, fused after dehiscence, ± level with apex or included and joined across orifice.</text>
      <biological_entity id="o3090" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" unit="or" value="3" />
        <character name="quantity" src="d0_s11" unit="or" value="4" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="fused" />
        <character constraint="with " constraintid="o3094" is_modifier="false" modifier="more or less" name="position_relational" src="d0_s11" value="level" />
      </biological_entity>
      <biological_entity id="o3091" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity id="o3092" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity id="o3093" name="orifice" name_original="orifice" src="d0_s11" type="structure">
        <character is_modifier="true" name="position" src="d0_s11" value="included" />
        <character is_modifier="true" name="fusion" src="d0_s11" value="joined" />
      </biological_entity>
      <biological_entity id="o3094" name="orifice" name_original="orifice" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="joined" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eucalyptus robusta is known from Los Angeles, Monterey, Orange, San Diego, San Luis Obispo, Santa Barbara, Santa Cruz, and Ventura counties in California, and from Charlotte, Lee, Martin, and St. Lucie counties in Florida.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Fla.; e Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="e Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>