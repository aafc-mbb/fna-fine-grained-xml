<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ONAGRACEAE</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="subfamily">Onagroideae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">Onagreae</taxon_name>
    <taxon_name authority="W. L. Wagner &amp; Hoch" date="2007" rank="genus">CAMISSONIOPSIS</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) W. L. Wagner &amp; Hoch" date="2007" rank="species">bistorta</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 204.  2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus camissoniopsis;species bistorta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oenothera</taxon_name>
    <taxon_name authority="Nuttall ex Torrey &amp; A. Gray" date="1840" rank="species">bistorta</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 508.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species bistorta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camissonia</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) P. H. Raven" date="unknown" rank="species">bistorta</taxon_name>
    <taxon_hierarchy>genus camissonia;species bistorta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bistorta</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="variety">veitchiana</taxon_name>
    <taxon_hierarchy>genus o.;species bistorta;variety veitchiana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphaerostigma</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) Walpers" date="unknown" rank="species">bistortum</taxon_name>
    <taxon_hierarchy>genus sphaerostigma;species bistortum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bistortum</taxon_name>
    <taxon_name authority="(Hooker) A. Nelson" date="unknown" rank="variety">veitchianum</taxon_name>
    <taxon_hierarchy>genus s.;species bistortum;variety veitchianum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="(Hooker) Small" date="unknown" rank="species">veitchianum</taxon_name>
    <taxon_hierarchy>genus s.;species veitchianum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, rarely short-lived perennial, usually villous, sometimes strigillose.</text>
      <biological_entity id="o0" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–several from base, ascending or decumbent, to 80 cm.</text>
      <biological_entity id="o1" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="from base" constraintid="o2" from="1" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves 1.2–12 × 0.2–1.5 cm;</text>
      <biological_entity id="o3" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s2" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0–4 cm, distal ones 0–0.3 cm;</text>
      <biological_entity id="o4" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="0.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade (basal) narrowly elliptic or (cauline) usually narrowly lanceolate or lanceolate, rarely linear, base (basal) narrowly cuneate, (cauline) cuneate or subcordate, margins usually sparsely and incon­spicuously denticulate, apex acute.</text>
      <biological_entity id="o5" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" />
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" />
      </biological_entity>
      <biological_entity id="o6" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subcordate" />
      </biological_entity>
      <biological_entity id="o7" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="spicuously" name="shape" src="d0_s4" value="denticulate" />
      </biological_entity>
      <biological_entity id="o8" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers opening near sunrise;</text>
      <biological_entity id="o9" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o10" name="sunrise" name_original="sunrise" src="d0_s5" type="structure" />
      <relation from="o9" id="r0" name="opening near" negation="false" src="d0_s5" to="o10" />
    </statement>
    <statement id="d0_s6">
      <text>floral-tube 2–5 (–7.5) mm;</text>
      <biological_entity id="o11" name="floral-tube" name_original="floral-tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals (2.3–) 5–8 (–11) mm;</text>
      <biological_entity id="o12" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow, each usually with 1 bright red dot, rarely 2, near base, (4.2–) 7–15 mm;</text>
      <biological_entity id="o14" name="dot" name_original="dot" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="bright red" />
        <character modifier="rarely" name="quantity" src="d0_s8" value="2" />
      </biological_entity>
      <biological_entity id="o15" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o13" id="r1" modifier="usually" name="with" negation="false" src="d0_s8" to="o14" />
      <relation from="o13" id="r2" name="near" negation="false" src="d0_s8" to="o15" />
    </statement>
    <statement id="d0_s9">
      <text>epise­palous filaments (1–) 1.5–3.5 mm, epipetalous filaments (0.5–) 1–2.5 mm, anthers (0.5–) 1.3–2 (–2.5) mm, less than 5% of pollen-grains 4-pored or 5-pored;</text>
      <biological_entity id="o13" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s8" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="epipetalous" id="o17" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19" name="pollen-grain" name_original="pollen-grains" src="d0_s9" type="structure" />
      <relation from="o18" id="r3" modifier="0-5%" name="part_of" negation="false" src="d0_s9" to="o19" />
    </statement>
    <statement id="d0_s10">
      <text>style (5.5–) 7–12 mm, stigma exserted beyond anthers at anthesis.</text>
      <biological_entity id="o20" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21" name="stigma" name_original="stigma" src="d0_s10" type="structure" />
      <biological_entity id="o22" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o21" id="r4" name="exserted beyond" negation="false" src="d0_s10" to="o22" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules straight or somewhat contorted, weakly 4-angled, 12–40 × 1.5–2.5 mm.</text>
      <biological_entity id="o23" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" />
        <character is_modifier="false" modifier="somewhat" name="arrangement_or_shape" src="d0_s11" value="contorted" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s11" value="4-angled" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s11" to="40" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 0.9–1 mm. 2n = 14.</text>
      <biological_entity id="o24" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Camissoniopsis bistorta occurs in California from Ventura County south and east through the counties of southern Los Angeles, southwestern San Bernardino, Orange, western Riverside, and the western two-thirds of San Diego, reaching the margins of the desert in San Bernardino and San Diego counties, and southward in cismontane Baja California to Ojos Negros and San Vicente.  The species occurs at exceptionally high elevations in the Santa Ana drainage of the San Bernardino Mountains.  P. H. Raven (1969) indicated that there were occasional apparent hybrids between C. cheiranthifolia subsp. suffruticosa and C. bistorta occurring in intermediate habitats in areas where the two species co-occur.  He determined that C. bistorta is self-incompatible.</discussion>
  <discussion>Camissoniopsis bistorta was apparently introduced with stream gravel in 1959 in Goleta Marsh, Santa Barbara, California, and on ballast heaps at Nanaimo, Vancouver Island, British Columbia, in 1893.  It has apparently not persisted at either site.</discussion>
  <discussion>Oenothera heterophylla Nuttall ex Hooker &amp; Arnott (1839), not Spach (1836), is an illegitimate name that pertains to Camissoniopsis bistorta.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or clayey soils, coastal strands, grasslands, coastal sage scrub, chaparral, oak woodlands, margins of Sonoran and Mojave deserts, rarely higher elevation meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="coastal strands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="coastal sage scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="margins" constraint="of sonoran and mojave deserts" />
        <character name="habitat" value="sonoran" />
        <character name="habitat" value="mojave deserts" />
        <character name="habitat" value="elevation meadows" modifier="rarely higher" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1600(–2600) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>