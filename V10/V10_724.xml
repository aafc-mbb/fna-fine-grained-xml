<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Elizabeth McClintock†</author>
    </source>
    <processed_by>
      <processor>
        <date>2022/05/12 01:28:10</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">MYRTACEAE</taxon_name>
    <taxon_name authority="J. R. Forster &amp; G. Forster" date="1776" rank="genus">LEPTOSPERMUM</taxon_name>
    <place_of_publication>
      <publication_title>Char. Gen. Pl. ed.</publication_title>
      <place_in_publication>2, 71, plate 36.  1776</place_in_publication>
      <other_info_on_pub>name conserved</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus leptospermum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek leptos, slender or small, and sperma, seed, alluding to form and size</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Tea tree</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, glabrous or pubescent, hairs simple.</text>
      <biological_entity id="o3188" name="shrub" name_original="shrubs" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" />
      </biological_entity>
      <biological_entity id="o3189" name="tree" name_original="trees" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="small" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" />
      </biological_entity>
      <biological_entity id="o3190" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate;</text>
      <biological_entity id="o3191" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade venation parallelodromous, faint, often with visible midvein, sometimes also 2–4 veins arising from base, arching to apex.</text>
      <biological_entity id="o3192" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="parallelodromous" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="faint" />
      </biological_entity>
      <biological_entity id="o3193" name="midvein" name_original="midvein" src="d0_s2" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s2" value="visible" />
        <character char_type="range_value" from="2" modifier="sometimes" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o3194" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o3195" is_modifier="false" name="orientation" src="d0_s2" value="arising" />
        <character constraint="to apex" constraintid="o3196" is_modifier="false" name="orientation" notes="" src="d0_s2" value="arching" />
      </biological_entity>
      <biological_entity id="o3195" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o3196" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <relation from="o3192" id="r567" modifier="often" name="with" negation="false" src="d0_s2" to="o3193" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 1–3-flowered, axillary, flowers solitary or clustered.</text>
      <biological_entity id="o3197" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-3-flowered" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" />
      </biological_entity>
      <biological_entity id="o3198" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s3" value="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 5-merous, subsessile;</text>
      <biological_entity id="o3199" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-merous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>hypanthium mostly widely cupshaped;</text>
      <biological_entity id="o3200" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly widely" name="shape" src="d0_s5" value="cup-shaped" value_original="cupshaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>calyx lobes distinct;</text>
      <biological_entity constraint="calyx" id="o3201" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white [pink or red];</text>
      <biological_entity id="o3202" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 15–35, about as long as perianth;</text>
      <biological_entity id="o3203" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s8" to="35" />
      </biological_entity>
      <biological_entity id="o3204" name="perianth" name_original="perianth" src="d0_s8" type="structure" />
      <relation from="o3203" id="r568" name="as long as" negation="false" src="d0_s8" to="o3204" />
    </statement>
    <statement id="d0_s9">
      <text>ovary [2–] 6–12-locular;</text>
      <biological_entity id="o3205" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="[2-]6-12-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style equaling or shorter than stamens;</text>
      <biological_entity id="o3206" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equaling" />
        <character constraint="than stamens" constraintid="o3207" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" />
      </biological_entity>
      <biological_entity id="o3207" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovules [6–] 15 [–28] per locule.</text>
      <biological_entity id="o3208" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s11" to="15" to_inclusive="false" />
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="28" />
        <character constraint="per locule" constraintid="o3209" name="quantity" src="d0_s11" value="15" />
      </biological_entity>
      <biological_entity id="o3209" name="locule" name_original="locule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits capsules, brown or gray, obconic to broadly bowl-shaped, woody, opening apically.</text>
      <biological_entity constraint="fruits" id="o3210" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="gray" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s12" value="obconic" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="[duplicate value]" src="d0_s12" value="bowl--shaped" value_original="bowl-shaped" />
        <character char_type="range_value" from="obconic" name="shape" src="d0_s12" to="broadly bowl-shaped" />
        <character is_modifier="false" name="texture" src="d0_s12" value="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds numerous, obovoid to irregular-linear, flattened, 1–2.5 mm;</text>
      <biological_entity id="o3211" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="numerous" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="obovoid" />
        <character is_modifier="false" name="shape" notes="[duplicate value]" src="d0_s13" value="irregular-linear" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s13" to="irregular-linear" />
        <character is_modifier="false" name="shape" src="d0_s13" value="flattened" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat reticulate or striate.</text>
      <biological_entity id="o3212" name="seed-coat" name_original="seed-coat" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reticulate" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="striate" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 70 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced, California; Australasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="California" establishment_means="introduced" />
        <character name="distribution" value="Australasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>