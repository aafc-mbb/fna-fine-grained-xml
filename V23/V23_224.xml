<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">139</other_info_on_meta>
    <other_info_on_meta type="treatment_page">140</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Nees in C. F. P. von Martius et al." date="1842" rank="genus">oxycaryum</taxon_name>
    <taxon_name authority="(Poeppig &amp; Kunth) Lye" date="1971" rank="species">cubense</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Not.</publication_title>
      <place_in_publication>124: 281. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus oxycaryum;species cubense</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357856</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Poeppig &amp; Kunth" date="unknown" rank="species">cubensis</taxon_name>
    <place_of_publication>
      <publication_title>in C. S. Kunth, Enum. Pl.</publication_title>
      <place_in_publication>2: 172. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species cubensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stolons scaly, rooting at nodes or with long-hanging roots.</text>
      <biological_entity id="o24052" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s0" value="scaly" value_original="scaly" />
        <character constraint="at nodes or with roots" constraintid="o24053" is_modifier="false" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o24053" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="long-hanging" value_original="long-hanging" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 7.5–76 (–100) cm × 0.7–5 mm.</text>
      <biological_entity id="o24054" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="76" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="100" to_unit="cm" />
        <character char_type="range_value" from="7.5" from_unit="cm" name="length" src="d0_s1" to="76" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves elaminate proximally, laminate distally;</text>
      <biological_entity id="o24055" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s2" value="elaminate" value_original="elaminate" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="laminate" value_original="laminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (33–) 50–70 (–110) cm × (2–) 4.5–5.5 (–7) mm.</text>
      <biological_entity id="o24056" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="33" from_unit="cm" name="atypical_length" src="d0_s3" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="110" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="length" src="d0_s3" to="70" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s3" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s3" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with 1–13 heads, globose to broadly ovoid, 10–23 × 9–19 mm;</text>
      <biological_entity id="o24057" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="globose" name="shape" notes="" src="d0_s4" to="broadly ovoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="23" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s4" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24058" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="13" />
      </biological_entity>
      <relation from="o24057" id="r2951" name="with" negation="false" src="d0_s4" to="o24058" />
    </statement>
    <statement id="d0_s5">
      <text>spikelets 5–many.</text>
      <biological_entity id="o24059" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" is_modifier="false" name="quantity" src="d0_s5" to="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 3–6 × 2.5–6 mm;</text>
      <biological_entity id="o24060" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>scales transparent to pale-brown with redbrown or purple marks, ovate to oblong, apex acuminate.</text>
      <biological_entity id="o24061" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="with marks" constraintid="o24062" from="transparent" name="coloration" src="d0_s7" to="pale-brown" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s7" to="oblong" />
      </biological_entity>
      <biological_entity id="o24062" name="mark" name_original="marks" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="redbrown" value_original="redbrown" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o24063" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes pale or redbrown, ovoid or ellipsoid, 2.2–2.8 × 1–1.2 mm;</text>
      <biological_entity id="o24064" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s8" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak to 0.7 mm.</text>
      <biological_entity id="o24065" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lakes and streams, floating, or raft-forming, or along margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lakes" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="margins" modifier="along" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., La., Tex.; Mexico; West Indies (Cuba); Central America; South America; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>The named variants of Oxycaryum cubense appear to represent extremes of this morphologically plastic species.</discussion>
  
</bio:treatment>