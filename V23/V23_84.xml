<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="treatment_page">57</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="(Reichenbach) Palla" date="1888" rank="genus">schoenoplectus</taxon_name>
    <taxon_name authority="(Reichenbach) J. Raynal" date="1976" rank="section">actaeogeton</taxon_name>
    <taxon_name authority="(A. Gray) Soják" date="1972" rank="species">smithii</taxon_name>
    <taxon_name authority="(Fernald) S. G. Smith" date="2002" rank="variety">setosus</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 107. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus schoenoplectus;section actaeogeton;species smithii;variety setosus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357951</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">smithii</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">setosus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>3: 252. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species smithii;variety setosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perianth bristles 4–6, equaling to twice as long as achene, densely spinulose.</text>
      <biological_entity constraint="perianth" id="o12322" name="bristle" name_original="bristles" src="d0_s0" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s0" to="6" />
        <character constraint="to " constraintid="o12323" is_modifier="false" name="variability" src="d0_s0" value="equaling" value_original="equaling" />
        <character is_modifier="false" modifier="densely" name="architecture_or_shape" notes="" src="d0_s0" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity id="o12323" name="achene" name_original="achene" src="d0_s0" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Freshwater, sandy or muddy shores with relatively stable water levels, floating mats, bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="freshwater" />
        <character name="habitat" value="sandy" constraint="with relatively stable water levels , floating mats" />
        <character name="habitat" value="muddy shores" constraint="with relatively stable water levels , floating mats" />
        <character name="habitat" value="stable water levels" modifier="relatively" />
        <character name="habitat" value="floating mats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Conn., Del., Ill., Ind., Maine, Md., Mass., Mich., Minn., N.H., N.Y., N.C., Ohio, Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13b.</number>
  <other_name type="common_name">Scirpe à soies longues</other_name>
  
</bio:treatment>