<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">cyperus</taxon_name>
    <taxon_name authority="Millspaugh &amp; Chase" date="1903" rank="species">lentiginosus</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Field Columbian Mus., Bot. Ser.</publication_title>
      <place_in_publication>3: 74. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus cyperus;species lentiginosus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357681</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">strigosus</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="variety">gracilis</taxon_name>
    <taxon_hierarchy>genus Cyperus;species strigosus;variety gracilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="species">tenuis</taxon_name>
    <taxon_name authority="(Millspaugh &amp; Chase) Kükenthal" date="unknown" rank="variety">lentiginosus</taxon_name>
    <taxon_hierarchy>genus Cyperus;species tenuis;variety lentiginosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, rhizomatous.</text>
      <biological_entity id="o18216" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms trigonous, (1–) 20–40 (–75) cm × 1–1.5 (–2.5) mm, glabrous.</text>
      <biological_entity id="o18217" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s1" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="75" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves inversely W-shaped, 10–40 (–70) cm × (1–) 3–6 (–10) mm.</text>
      <biological_entity id="o18218" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="inversely" name="shape" src="d0_s2" value="w--shaped" value_original="w--shaped" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="70" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s2" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: spikes 1 (–3), broadly and loosely ovoid to oblongellipsoid, (15–) 20–30 (–50) × 20–30 (–45) mm;</text>
      <biological_entity id="o18219" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o18220" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="3" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" modifier="broadly; loosely" name="shape" src="d0_s3" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s3" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="45" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts (3–) 5–7 (–10), ascending at 30 (–45) °, inversely W-shaped, 10–30 (–50) cm × (1–) 3–5 (–11) mm;</text>
      <biological_entity id="o18221" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o18222" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="10" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" modifier="30(-45)°" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="inversely" name="shape" src="d0_s4" value="w--shaped" value_original="w--shaped" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rays (2–) 3–6 (–12), (1–) 3–10 (–17) cm;</text>
      <biological_entity id="o18223" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o18224" name="ray" name_original="rays" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="12" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="17" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachilla deciduous, wings ca. 0.3 mm wide.</text>
      <biological_entity id="o18225" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o18226" name="rachillum" name_original="rachilla" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o18227" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character name="width" src="d0_s6" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets (10–) 25–50 (–100), ± terete, (appearing compressed due to excurved apices of floral scales), (8–) 12–15 (–24) × (0.8–) 1.1–1.4 (–1.8) mm;</text>
      <biological_entity id="o18228" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s7" to="25" to_inclusive="false" />
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="100" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s7" to="50" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="terete" value_original="terete" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s7" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="24" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_width" src="d0_s7" to="1.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s7" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral scales persistent, (4–) 6–8 (–14), golden brown, off-white to stramineous, densely red-glandular punctate, green medially, 3–4-ribbed laterally, 3–5-ribbed medially, oblong-lanceolate, (3.2–) 4–4.5 × 1.2–1.6 mm, apex blunt, mucronulate to mucronate, mucro 0.2–0.5 mm;</text>
      <biological_entity constraint="floral" id="o18229" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="6" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="14" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="golden brown" value_original="golden brown" />
        <character char_type="range_value" from="off-white" name="coloration" src="d0_s8" to="stramineous" />
        <character is_modifier="false" modifier="densely" name="architecture_or_function_or_pubescence" src="d0_s8" value="red-glandular" value_original="red-glandular" />
        <character is_modifier="false" name="coloration_or_relief" src="d0_s8" value="punctate" value_original="punctate" />
        <character is_modifier="false" modifier="medially" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="laterally" name="architecture_or_shape" src="d0_s8" value="3-4-ribbed" value_original="3-4-ribbed" />
        <character is_modifier="false" modifier="medially" name="architecture_or_shape" src="d0_s8" value="3-5-ribbed" value_original="3-5-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="atypical_length" src="d0_s8" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18230" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="blunt" value_original="blunt" />
        <character char_type="range_value" from="mucronulate" name="shape" src="d0_s8" to="mucronate" />
      </biological_entity>
      <biological_entity id="o18231" name="mucro" name_original="mucro" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>proximal scales mucronulate, distal scales mucronate;</text>
      <biological_entity constraint="proximal" id="o18232" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18233" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>terminal scale conduplicate.</text>
      <biological_entity constraint="terminal" id="o18234" name="scale" name_original="scale" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s10" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: anthers 0.5–0.8 (–1.4) mm;</text>
      <biological_entity id="o18235" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o18236" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 0.8–1 mm;</text>
      <biological_entity id="o18237" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o18238" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas 1–2 mm.</text>
      <biological_entity id="o18239" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o18240" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes brown, ± sessile to stipitate, oblongellipsoid, 1.7–2 × 0.6–0.7 mm, 0.1 (–0.2) × 0.2 mm, apex rounded, apiculate from dark purple style base, surfaces papillose.</text>
      <biological_entity id="o18241" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character char_type="range_value" from="less sessile" name="architecture" src="d0_s14" to="stipitate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s14" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s14" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="0.2" to_unit="mm" />
        <character name="length" src="d0_s14" unit="mm" value="0.1" value_original="0.1" />
        <character name="width" src="d0_s14" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o18242" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character constraint="from style base" constraintid="o18243" is_modifier="false" name="architecture_or_shape" src="d0_s14" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="style" id="o18243" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s14" value="dark purple" value_original="dark purple" />
      </biological_entity>
      <biological_entity id="o18244" name="surface" name_original="surfaces" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Tex.; Mexico; West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>85.</number>
  <discussion>Cyperus lentiginosus has been treated as a variety of C. tenuis (G. Kükenthal 1935–1936). The two taxa differ in the longer scales with cuspidate apices of C. lentiginosus, the inversely W-shaped leaves and bracts (those of C. tenuis are V-shaped), and the open spikes (those of C. tenuis are dense).</discussion>
  
</bio:treatment>