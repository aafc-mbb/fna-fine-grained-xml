<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Joy Mastrogiuseppe</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="treatment_page">198</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Persoon" date="1805" rank="genus">DULICHIUM</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>1: 65. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus DULICHIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin dulichium, a kind of sedge</other_info_on_name>
    <other_info_on_name type="fna_id">111048</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose or not, rhizomatous.</text>
      <biological_entity id="o16013" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" src="d0_s0" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms terete.</text>
      <biological_entity id="o16014" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
      <biological_entity id="o16015" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules present;</text>
      <biological_entity id="o16016" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal leaves bladeless, distal with well-developed blades, flat.</text>
      <biological_entity constraint="proximal" id="o16017" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="bladeless" value_original="bladeless" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16018" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o16019" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="development" src="d0_s4" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <relation from="o16018" id="r1960" name="with" negation="false" src="d0_s4" to="o16019" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, spicate, with 3–10 spikelets per spike;</text>
      <biological_entity id="o16020" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
      </biological_entity>
      <biological_entity id="o16021" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o16022" name="spike" name_original="spike" src="d0_s5" type="structure" />
      <relation from="o16020" id="r1961" name="with" negation="false" src="d0_s5" to="o16021" />
      <relation from="o16021" id="r1962" name="per" negation="false" src="d0_s5" to="o16022" />
    </statement>
    <statement id="d0_s6">
      <text>primary bracts leaflike, each subtending spike.</text>
      <biological_entity constraint="primary" id="o16023" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o16024" name="spike" name_original="spike" src="d0_s6" type="structure" />
      <relation from="o16023" id="r1963" name="subtending" negation="false" src="d0_s6" to="o16024" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets: scales 4–8, distichously arranged, each subtending flower.</text>
      <biological_entity id="o16025" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o16026" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="8" />
        <character is_modifier="false" modifier="distichously" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o16027" name="flower" name_original="flower" src="d0_s7" type="structure" />
      <relation from="o16026" id="r1964" name="subtending" negation="false" src="d0_s7" to="o16027" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o16028" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth of 6–9 retrorsely barbed bristles, slightly longer than achene;</text>
      <biological_entity id="o16029" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character constraint="than achene" constraintid="o16031" is_modifier="false" name="length_or_size" notes="" src="d0_s9" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o16030" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s9" to="9" />
        <character is_modifier="true" modifier="retrorsely" name="architecture_or_shape" src="d0_s9" value="barbed" value_original="barbed" />
      </biological_entity>
      <biological_entity id="o16031" name="achene" name_original="achene" src="d0_s9" type="structure" />
      <relation from="o16029" id="r1965" name="consist_of" negation="false" src="d0_s9" to="o16030" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 3;</text>
      <biological_entity id="o16032" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 2-fid, base persistent, linear.</text>
      <biological_entity id="o16033" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o16034" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes biconvex.</text>
    </statement>
    <statement id="d0_s13">
      <text>x = 16.</text>
      <biological_entity id="o16035" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="biconvex" value_original="biconvex" />
      </biological_entity>
      <biological_entity constraint="x" id="o16036" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">Duliche</other_name>
  <discussion>Species 1.</discussion>
  
</bio:treatment>