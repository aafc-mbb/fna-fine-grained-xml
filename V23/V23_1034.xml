<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>William J. Crins</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="G. Don in J. C. Loudon" date="1830" rank="section">Clandestinae</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Loudon, Hort. Brit.,</publication_title>
      <place_in_publication>376. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Clandestinae</taxon_hierarchy>
    <other_info_on_name type="fna_id">302686</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="(Fries) H. Christ" date="unknown" rank="section">Digitatae</taxon_name>
    <taxon_hierarchy>genus Carex;section Digitatae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, short to long rhizomatous.</text>
      <biological_entity id="o10186" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="size_or_length" src="d0_s0" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms red-purple at the base, shorter or longer than leaves.</text>
      <biological_entity id="o10187" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o10188" is_modifier="false" name="coloration_or_density" src="d0_s1" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s1" value="shorter" value_original="shorter" />
        <character constraint="than leaves" constraintid="o10189" is_modifier="false" name="length_or_size" src="d0_s1" value="shorter or longer" value_original="shorter or longer" />
      </biological_entity>
      <biological_entity id="o10188" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o10189" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths not fibrous;</text>
      <biological_entity id="o10190" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o10191" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous;</text>
      <biological_entity id="o10192" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o10193" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades V-shaped in cross-section when young, glabrous, widest leaf-blades 2–4 mm wide, distal leaves bladeless or with blades 1 mm or less, not longer than sheaths.</text>
      <biological_entity id="o10194" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o10195" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="in cross-section when young , glabrous , widest leaf-blades 2-4 mm wide , distal leaves bladeless or with blades 1 mm or less" is_modifier="false" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
        <character constraint="than sheaths" constraintid="o10196" is_modifier="false" name="length_or_size" src="d0_s4" value="not longer" value_original="not longer" />
      </biological_entity>
      <biological_entity id="o10196" name="sheath" name_original="sheaths" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, with 2–6 spikes;</text>
      <biological_entity id="o10197" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o10198" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <relation from="o10197" id="r1236" name="with" negation="false" src="d0_s5" to="o10198" />
    </statement>
    <statement id="d0_s6">
      <text>proximal nonbasal bracts bladeless or blade usually less than 2 mm, purple-tinged, sheathing;</text>
      <biological_entity constraint="proximal nonbasal" id="o10199" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="bladeless" value_original="bladeless" />
      </biological_entity>
      <biological_entity id="o10200" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral spikes pistillate or androgynous, with not more than 25 perigynia, sometimes basal, pedunculate, prophyllate, ovoid;</text>
      <biological_entity constraint="lateral" id="o10201" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="androgynous" value_original="androgynous" />
        <character is_modifier="false" modifier="sometimes" name="position" notes="" src="d0_s7" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="prophyllate" value_original="prophyllate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o10202" name="perigynium" name_original="perigynia" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" modifier="not" name="quantity" src="d0_s7" upper_restricted="false" />
      </biological_entity>
      <relation from="o10201" id="r1237" name="with" negation="false" src="d0_s7" to="o10202" />
    </statement>
    <statement id="d0_s8">
      <text>terminal spike staminate or androgynous.</text>
      <biological_entity constraint="terminal" id="o10203" name="spike" name_original="spike" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="androgynous" value_original="androgynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Proximal pistillate scales dark-brown to black, apex obtuse to subacute, cuspidate or awned.</text>
      <biological_entity constraint="proximal" id="o10204" name="spike" name_original="spikes" src="d0_s9" type="structure" />
      <biological_entity id="o10205" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s9" to="black" />
      </biological_entity>
      <biological_entity id="o10206" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="subacute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Perigynia erect or ascending, veinless or veined with 2 prominent marginal veins, stipitate, obovoid, rounded-trigonous, 2–3.5 mm, base tapering or cuneate, apex abruptly contracted to beak, pubescent, sometimes glabrescent;</text>
      <biological_entity id="o10207" name="perigynium" name_original="perigynia" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="veinless" value_original="veinless" />
        <character constraint="with marginal veins" constraintid="o10208" is_modifier="false" name="architecture" src="d0_s10" value="veined" value_original="veined" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded-trigonous" value_original="rounded-trigonous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o10208" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o10209" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o10210" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character constraint="to beak" constraintid="o10211" is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s10" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o10211" name="beak" name_original="beak" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>beak straight or bent, 0.2–0.5 mm, orifice entire or emarginate.</text>
      <biological_entity id="o10212" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="bent" value_original="bent" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10213" name="orifice" name_original="orifice" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Stigmas 3–4.</text>
      <biological_entity id="o10214" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes trigonous, rarely quadrangular, almost as large as bodies of perigynia;</text>
      <biological_entity id="o10215" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s13" value="quadrangular" value_original="quadrangular" />
      </biological_entity>
      <biological_entity id="o10216" name="body" name_original="bodies" src="d0_s13" type="structure" />
      <biological_entity id="o10217" name="perigynium" name_original="perigynia" src="d0_s13" type="structure" />
      <relation from="o10215" id="r1238" modifier="almost" name="as large as" negation="false" src="d0_s13" to="o10216" />
      <relation from="o10216" id="r1239" name="part_of" negation="false" src="d0_s13" to="o10217" />
    </statement>
    <statement id="d0_s14">
      <text>style deciduous.</text>
      <biological_entity id="o10218" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Circumboreal (circumpolar and north temperate).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Circumboreal (circumpolar and north temperate)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26ccc.</number>
  <discussion>Species ca. 20 (4 in the flora).</discussion>
  <discussion>Carex sect. Clandestinae has close affinities with sect. Pictae Kükenthal and perhaps with sect. Acrocystis Dumortier. The Eurasian species, C. ericetorum Pollich, which has been placed in sect. Acrocystis by Eurasian authors, actually belongs in sect. Clandestinae, based on its bract morphology and perigynium pubescence. Although most species of sect. Clandestinae, as circumscribed here, form a cohesive morphologic group, C. pedunculata shows a number of similarities to C. baltzellii Chapman of sect. Pictae, including inflorescence morphology, scale morphology, and perigynium pubescence. Developmental and anatomic studies may help to determine whether the sections with pubescent perigynia exhibit homologous or analogous character states.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Most pistillate spikes emerging from basal nodes on long peduncles to 13 cm; pistillate scales retuse to obtuse, cuspidate; perigynia 3.7–6 mm; leaves dark green, equaling or mostly exceeding culms.</description>
      <determination>442 Carex pedunculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">All pistillate spikes emerging from cauline nodes on shorter peduncles or sessile; pistillate scales acute to obtuse; perigynia less than 3.5 mm; leaves pale green, shorter than culms.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Staminate spikes less than 7 mm; anthers less than 2 mm; plants densely cespitose, short-rhizomatous.</description>
      <determination>445 Carex concinna</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Staminate spikes usually more than 10 mm; anthers more than 2 mm; plants loosely cespitose, long-rhizomatous.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stigmas 3, flexuous, thin, strongly papillose; pistillate spikes widely separated, proximal spikes long-pedunculate, peduncles to 7 cm; proximal bracts long-sheathing; transcontinental.</description>
      <determination>443 Carex richardsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stigmas 4, erect or convolute, thick, weakly papillose; pistillate spikes aggregated, subsessile or short-pedunclulate; proximal bracts short-sheathing; northwestern.</description>
      <determination>444 Carex concinnoides</determination>
    </key_statement>
  </key>
</bio:treatment>