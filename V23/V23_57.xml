<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">40</other_info_on_meta>
    <other_info_on_meta type="treatment_page">41</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="(Ascherson) Palla in W. D. J. Koch et al." date="1905" rank="genus">bolboschoenus</taxon_name>
    <taxon_name authority="(Linnaeus) Palla in W. D. J. Koch et al." date="1905" rank="species">maritimus</taxon_name>
    <taxon_name authority="(A. Nelson) T. Koyama" date="1980" rank="subspecies">paludosus</taxon_name>
    <place_of_publication>
      <publication_title>Acta Phytotax. Geobot.</publication_title>
      <place_in_publication>31: 148. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus bolboschoenus;species maritimus;subspecies paludosus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357004</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">paludosus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 5. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species paludosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">campestris</taxon_name>
    <place_of_publication>
      <place_in_publication>1896</place_in_publication>
      <other_info_on_pub>not Rottbøll 1795</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species campestris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Britton ex Parish" date="unknown" rank="species">maritimus</taxon_name>
    <taxon_name authority="(A. Nelson) Kükenthal" date="unknown" rank="variety">paludosus</taxon_name>
    <taxon_hierarchy>genus Scirpus;species maritimus;variety paludosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pacificus</taxon_name>
    <taxon_hierarchy>genus Scirpus;species pacificus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Scales medium brown to nearly colorless, rarely dark-brown.</text>
      <biological_entity id="o14839" name="scale" name_original="scales" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="medium" value_original="medium" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s0" to="nearly colorless" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s0" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Styles all 2-fid (rarely, some 3-fid).</text>
      <biological_entity id="o14840" name="style" name_original="styles" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Achenes medium brown, sometimes darker or paler, biconvex.</text>
      <biological_entity id="o14841" name="achene" name_original="achenes" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="medium" value_original="medium" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s2" value="darker" value_original="darker" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="paler" value_original="paler" />
        <character is_modifier="false" name="shape" src="d0_s2" value="biconvex" value_original="biconvex" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–fall (south and lowland California), summer (north).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" modifier="south and lowland California" to="fall" from="spring" />
        <character name="fruiting time" char_type="range_value" modifier="north" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brackish to saline coastal and inland shores, marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brackish to saline coastal" />
        <character name="habitat" value="brackish to saline shores" modifier="inland" />
        <character name="habitat" value="marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.W.T., N.S., Ont., P.E.I., Que., Sask.; Alaska, Ariz., Calif., Colo., Conn., Idaho, Ill., Iowa, Kans., Maine, Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.Dak., Okla., Oreg., R.I., S.Dak., Tex., Utah, Wash., Wyo.; Mexico; South America (Argentina, Peru); Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="common_name">Bolboschoenus des marais salés</other_name>
  <other_name type="common_name">scirpe des marais salés</other_name>
  <discussion>Many bipistillate specimens from Eurasia and Africa are very similar to American plants. Further study may show that these plants should be included in Bolboschoenus maritimus subsp. paludosus.</discussion>
  <discussion>Plants from seashores have bright brown floral scales and medium to dark brown achenes; plants from the western interior have bright brown to very pale floral scales and/or achenes.</discussion>
  <discussion>Around Chicago, Illinois, Bolboschoenus maritimus subsp. paludosus is spreading with other halophytes in roadside ditches where salts accumulate; it is likely to occur elsewhere in similar conditions. Bolboschoenus maritimus subsp. paludosus is planted for waterfowl food (H. A. George 1963, as Scirpus robustus), and in California it is sometimes mixed with B. glaucus and hybrids. The tough inner vascular cores of the rhizomes are used by Native Americans of the Pacific Coast in making baskets.</discussion>
  
</bio:treatment>