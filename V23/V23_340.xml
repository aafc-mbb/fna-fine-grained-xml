<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="treatment_page">194</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Rottbøll" date="1773" rank="genus">kyllinga</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">pumila</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 28. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus kyllinga;species pumila</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357850</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="Mattfeld &amp; Kükenthal" date="unknown" rank="species">densicaespitosus</taxon_name>
    <taxon_hierarchy>genus Cyperus;species densicaespitosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="(Steudel) Dandy" date="unknown" rank="species">tenuifolius</taxon_name>
    <taxon_hierarchy>genus Cyperus;species tenuifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kyllinga</taxon_name>
    <taxon_name authority="Steudel" date="unknown" rank="species">tenuifolia</taxon_name>
    <taxon_hierarchy>genus Kyllinga;species tenuifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, cespitose, not rhizomatous.</text>
      <biological_entity id="o18877" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (5–) 15–30 (–55) cm, smooth.</text>
      <biological_entity id="o18878" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="55" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves flat to V-shaped, 4–20 (–30) cm × 1.5–3 (–3.6) mm.</text>
      <biological_entity id="o18879" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s2" to="v-shaped" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: spikes 1–3, pale greenish, (3–) 5–8 (–11) × 5–7 mm;</text>
      <biological_entity id="o18880" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o18881" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale greenish" value_original="pale greenish" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s3" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts (3–) 4 (–5), horizontal to ascending at 30°, flat, (1–) 3–10 (–22) cm × 1–2.5 (–3) mm.</text>
      <biological_entity id="o18882" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o18883" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="5" />
        <character name="quantity" src="d0_s4" value="4" value_original="4" />
        <character char_type="range_value" from="horizontal" modifier="30°" name="orientation" src="d0_s4" to="ascending" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s4" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="22" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 50–150, lanceolate to ovatelanceolate, (1.9–) 2.4–2.8 (–3.8) × 0.6–0.9 mm;</text>
      <biological_entity id="o18884" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s5" to="150" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="ovatelanceolate" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="atypical_length" src="d0_s5" to="2.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s5" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s5" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral scales pale brownish to transparent, midvein not winged, laterally 2–3 (–4) -veined, ovate, (1.8–) 2.2–3.1 (–3.4) × 1–1.7 mm;</text>
      <biological_entity constraint="floral" id="o18885" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character char_type="range_value" from="pale brownish" name="coloration" src="d0_s6" to="transparent" />
      </biological_entity>
      <biological_entity id="o18886" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="laterally" name="architecture" src="d0_s6" value="2-3(-4)-veined" value_original="2-3(-4)-veined" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="atypical_length" src="d0_s6" to="2.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.1" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s6" to="3.1" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 2;</text>
      <biological_entity id="o18887" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers elliptic, (0.2–) 0.3–0.4 mm;</text>
      <biological_entity id="o18888" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="0.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 0.5–0.8 mm;</text>
      <biological_entity id="o18889" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 0.5–0.9 mm.</text>
      <biological_entity id="o18890" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Achenes light-brown, stipitate, oblong, 1–1.2 (–1.4) × 0.5–0.6 (–0.7) mm, base cuneate to rounded, stipe to 0.1 mm, apex subtruncate, apiculate, finely papillose.</text>
      <biological_entity id="o18891" name="achene" name_original="achenes" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18892" name="base" name_original="base" src="d0_s11" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s11" to="rounded" />
      </biological_entity>
      <biological_entity id="o18893" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18894" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="subtruncate" value_original="subtruncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp grasslands, shorelines, ditches, lawns, gardens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" modifier="damp" />
        <character name="habitat" value="shorelines" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="gardens" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., Ill., Ind., Kans., Ky., La., Md., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.; Mexico; West Indies; Central America; South America; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Annual greenhead sedge</other_name>
  
</bio:treatment>