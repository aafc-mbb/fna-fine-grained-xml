<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="T. V. Egorova in An. A. Fedorov" date="1976" rank="section">Parvulae</taxon_name>
    <place_of_publication>
      <publication_title>in An. A. Fedorov, Fl. Evr. Chasti SSSR</publication_title>
      <place_in_publication>2: 110. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;section Parvulae</taxon_hierarchy>
    <other_info_on_name type="fna_id">302998</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o27973" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes creeping, 0.1–0.2 mm thick, soft, internodes to 4 cm, scales not evident, often terminating in a 2–4 mm tuber with large, acute, terminal bud, or tubers among culm bases.</text>
      <biological_entity id="o27974" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="thickness" src="d0_s1" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s1" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o27975" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27976" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s1" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o27977" name="tuber" name_original="tuber" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" modifier="often" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o27978" name="bud" name_original="bud" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o27979" name="tuber" name_original="tubers" src="d0_s1" type="structure" />
      <biological_entity constraint="culm" id="o27980" name="base" name_original="bases" src="d0_s1" type="structure" />
      <relation from="o27976" id="r3454" modifier="often" name="terminating in a" negation="false" src="d0_s1" to="o27977" />
      <relation from="o27976" id="r3455" modifier="often" name="with" negation="false" src="d0_s1" to="o27978" />
      <relation from="o27979" id="r3456" name="among" negation="false" src="d0_s1" to="o27980" />
    </statement>
    <statement id="d0_s2">
      <text>Culms terete, 2–9 cm × 0.2–0.5 mm, soft, spongy.</text>
      <biological_entity id="o27981" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s2" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character is_modifier="false" name="texture" src="d0_s2" value="spongy" value_original="spongy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: distal leaf-sheaths often disintegrating, thinly membranous, apex rounded.</text>
      <biological_entity id="o27982" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o27983" name="sheath" name_original="leaf-sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="dehiscence" src="d0_s3" value="disintegrating" value_original="disintegrating" />
        <character is_modifier="false" modifier="thinly" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o27984" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets: basal spikelets absent;</text>
      <biological_entity id="o27985" name="spikelet" name_original="spikelets" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o27986" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal scale empty, amplexicaulous;</text>
      <biological_entity id="o27987" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o27988" name="scale" name_original="scale" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="empty" value_original="empty" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="amplexicaulous" value_original="amplexicaulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral scales membranous.</text>
      <biological_entity id="o27989" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <biological_entity constraint="floral" id="o27990" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: anthers 0.6–1.2 mm;</text>
      <biological_entity id="o27991" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o27992" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>styles 3-fid, rarely some 2-fid.</text>
      <biological_entity id="o27993" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o27994" name="style" name_original="styles" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes trigonous, rarely some biconvex.</text>
      <biological_entity id="o27995" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s9" value="biconvex" value_original="biconvex" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Tubercles often rudimentary, often narrower than achene summit or sunken in a depression in achene, color and texture either similar to achene summit and tubercle merging with achene, or different and clearly distinct from achene.</text>
      <biological_entity id="o27996" name="tubercle" name_original="tubercles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
        <character constraint="than achene summit" constraintid="o27997" is_modifier="false" name="width" src="d0_s10" value="often narrower" value_original="often narrower" />
        <character constraint="in depression" constraintid="o27998" is_modifier="false" name="prominence" notes="" src="d0_s10" value="sunken" value_original="sunken" />
        <character constraint="to achene summit" constraintid="o28000" is_modifier="false" modifier="either" name="coloration" notes="" src="d0_s10" value="texture" value_original="texture" />
      </biological_entity>
      <biological_entity constraint="achene" id="o27997" name="summit" name_original="summit" src="d0_s10" type="structure" />
      <biological_entity id="o27998" name="depression" name_original="depression" src="d0_s10" type="structure" />
      <biological_entity id="o27999" name="achene" name_original="achene" src="d0_s10" type="structure" />
      <biological_entity constraint="achene" id="o28000" name="summit" name_original="summit" src="d0_s10" type="structure" />
      <biological_entity id="o28001" name="tubercle" name_original="tubercle" src="d0_s10" type="structure">
        <character constraint="from achene" constraintid="o28003" is_modifier="false" modifier="clearly" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o28002" name="achene" name_original="achene" src="d0_s10" type="structure" />
      <biological_entity id="o28003" name="achene" name_original="achene" src="d0_s10" type="structure" />
      <relation from="o27998" id="r3457" name="in" negation="false" src="d0_s10" to="o27999" />
      <relation from="o28001" id="r3458" name="merging with" negation="false" src="d0_s10" to="o28002" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate to tropical North America, Central America, South America, West Indies, Europe, North Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate to tropical North America" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="North Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8a3.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Eleocharis sect. Parvulae is usually treated as monotypic. The taxonomy of its species should be carefully studied worldwide. H. K. Svenson (1957) grouped the species of sect. Parvulae with subg. Eleocharis (sect. Eleocharis) ser. Rostellatae and subg. Zinserlingia in E. ser. Pauciflorae (Beauverd) Svenson on the basis of the often poor differentiation between the tubercle and achene. DNA data presented by E. H. Roalson and E. A. Friar (2000) suggest that sect. Parvulae is closely related to 8a1. sect. Eleocharis.</discussion>
  
</bio:treatment>