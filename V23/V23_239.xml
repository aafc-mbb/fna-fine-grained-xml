<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">153</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="treatment_page">154</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="C. B. Clarke in J. D. Hooker" date="1893" rank="subgenus">pycnostachys</taxon_name>
    <taxon_name authority="Steudel" date="1855" rank="species">distinctus</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl. Glumac.</publication_title>
      <place_in_publication>2: 24. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus pycnostachys;species distinctus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357649</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">virens</taxon_name>
    <taxon_name authority="C. B. Clarke" date="unknown" rank="variety">brittonii</taxon_name>
    <taxon_hierarchy>genus Cyperus;species virens;variety brittonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, rhizomatous.</text>
      <biological_entity id="o5822" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms terete, 40–60 (–90) cm, glabrous.</text>
      <biological_entity id="o5823" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves flat, 35–70 cm × 4–9 (–12) mm.</text>
      <biological_entity id="o5824" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="35" from_unit="cm" name="length" src="d0_s2" to="70" to_unit="cm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: heads hemispheric, 10–20 mm diam.;</text>
      <biological_entity id="o5825" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o5826" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rays 5–9 (–14), 3.5–10 cm;</text>
      <biological_entity id="o5827" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o5828" name="ray" name_original="rays" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="14" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="9" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>2d order rays 0.5–2 cm;</text>
      <biological_entity id="o5829" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o5830" name="ray" name_original="rays" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts 5–10, ± horizontal, flat, 12–40 (–50) cm × 2–7 mm.</text>
      <biological_entity id="o5831" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o5832" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s6" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="50" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s6" to="40" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets (25–) 35–50 (–60), ovoid, compressed, (3.5–) 5–12 (–14) × 2.5–3.5 (–4) mm;</text>
      <biological_entity id="o5833" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" name="atypical_quantity" src="d0_s7" to="35" to_inclusive="false" />
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="60" />
        <character char_type="range_value" from="35" name="quantity" src="d0_s7" to="50" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral scales 10–32, golden brown to brown, 2-keeled, laterally ribless, narrowly lanceolate, 2–2.2 (–3) × 0.8–1.4 mm.</text>
      <biological_entity constraint="floral" id="o5834" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="32" />
        <character char_type="range_value" from="golden brown" name="coloration" src="d0_s8" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s8" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" modifier="laterally" name="architecture" src="d0_s8" value="ribless" value_original="ribless" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: stamen 1;</text>
      <biological_entity id="o5835" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5836" name="stamen" name_original="stamen" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 1 mm;</text>
      <biological_entity id="o5837" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5838" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 1 mm;</text>
      <biological_entity id="o5839" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5840" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 0.5 mm.</text>
      <biological_entity id="o5841" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o5842" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes brown, stipitate, narrowly ellipsoid, 1.5–2 × 0.2–0.4 mm, base swollen, spongy, stipe 0.2 × 0.2–0.3 mm, apex acute, beak slender, 0.2 mm, surfaces puncticulate.</text>
      <biological_entity id="o5843" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s13" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s13" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5844" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="swollen" value_original="swollen" />
        <character is_modifier="false" name="texture" src="d0_s13" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o5845" name="stipe" name_original="stipe" src="d0_s13" type="structure">
        <character name="length" src="d0_s13" unit="mm" value="0.2" value_original="0.2" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s13" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5846" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o5847" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="slender" value_original="slender" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o5848" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s13" value="puncticulate" value_original="puncticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp grasslands, roadsides, ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" modifier="damp" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., La., S.C.; West Indies (Bahamas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>The slender achenes with conspicuously swollen bases distinguish Cyperus distinctus from other species of subg. Pycnostachys.</discussion>
  
</bio:treatment>