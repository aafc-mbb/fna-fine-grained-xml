<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="(Palisot de Beauvois) J. Carey" date="1847" rank="subgenus">Pycreus</taxon_name>
    <place_of_publication>
      <publication_title>Carices North. U. S.,</publication_title>
      <place_in_publication>517. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus Pycreus</taxon_hierarchy>
    <other_info_on_name type="fna_id">312931</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Palisot de Beauvois" date="unknown" rank="subgenus">Pycreus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Oware</publication_title>
      <place_in_publication>2: 48. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>subgenus Pycreus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="(Palisot de Beauvois) Grisebach" date="unknown" rank="section">Pycreus</taxon_name>
    <taxon_hierarchy>genus Cyperus;section Pycreus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms and leaves with Kranz (chlorocyperoid) anatomy.</text>
      <biological_entity id="o18635" name="culm" name_original="culms" src="d0_s0" type="structure" />
      <biological_entity id="o18636" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: spikes;</text>
      <biological_entity id="o18637" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o18638" name="spike" name_original="spikes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>rachilla persistent, wingless or nearly so.</text>
      <biological_entity id="o18639" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o18640" name="rachillum" name_original="rachilla" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="wingless" value_original="wingless" />
        <character name="architecture" src="d0_s2" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikelets borne in loosely spicate to ± digitate clusters;</text>
      <biological_entity id="o18641" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="loosely spicate" name="architecture" src="d0_s3" to="more or less digitate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="cluster" value_original="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>floral scales deciduous, 2-keeled or folded.</text>
      <biological_entity constraint="floral" id="o18642" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: stamens (1–) 2–3;</text>
      <biological_entity id="o18643" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o18644" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pistils 2;</text>
      <biological_entity id="o18645" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18646" name="pistil" name_original="pistils" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stigmas 2.</text>
      <biological_entity id="o18647" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18648" name="stigma" name_original="stigmas" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes biconvex or ± cylindric, laterally compressed, edge borne toward rachilla.</text>
      <biological_entity id="o18649" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o18650" name="edge" name_original="edge" src="d0_s8" type="structure" />
      <biological_entity id="o18651" name="rachillum" name_original="rachilla" src="d0_s8" type="structure" />
      <relation from="o18650" id="r2249" name="borne toward" negation="false" src="d0_s8" to="o18651" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15b.</number>
  <discussion>Species ca. 100 (11 in the flora).</discussion>
  <discussion>Laterally compressed, biconvex achenes, a feature not occurring elsewhere in the family, distinguish Cyperus subg. Pycreus. The taxon has been treated as a genus in Flora Mesoamericana (C. D. Adams 1994), an idea that may have merit.</discussion>
  
</bio:treatment>