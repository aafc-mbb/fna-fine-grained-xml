<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lisa A. Standley</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="(Mackenzie) Mackenzie in N. L. Britton et al." date="1935" rank="section">Collinsiae</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>18: 425. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Collinsiae</taxon_hierarchy>
    <other_info_on_name type="fna_id">302687</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="Mackenzie" date="unknown" rank="unranked">Collinsiae</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and A. Brown, Ill. Fl. N. U.S., ed.</publication_title>
      <place_in_publication>2, 1: 353. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carex;unranked Collinsiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, short-rhizomatous.</text>
      <biological_entity id="o12726" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-rhizomatous" value_original="short-rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms purple-brown at base.</text>
      <biological_entity id="o12727" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o12728" is_modifier="false" name="coloration" src="d0_s1" value="purple-brown" value_original="purple-brown" />
      </biological_entity>
      <biological_entity id="o12728" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths not fibrous;</text>
      <biological_entity id="o12729" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o12730" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous only in short obtriangular region at mouth;</text>
      <biological_entity id="o12731" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o12732" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character constraint="in region" constraintid="o12733" is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o12733" name="region" name_original="region" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s3" value="obtriangular" value_original="obtriangular" />
      </biological_entity>
      <biological_entity id="o12734" name="mouth" name_original="mouth" src="d0_s3" type="structure" />
      <relation from="o12733" id="r1571" name="at" negation="false" src="d0_s3" to="o12734" />
    </statement>
    <statement id="d0_s4">
      <text>blades V-shaped in cross-section when young, glabrous.</text>
      <biological_entity id="o12735" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12736" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o12737" is_modifier="false" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <biological_entity id="o12737" name="cross-section" name_original="cross-section" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, with 3–5 spikes;</text>
      <biological_entity id="o12738" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o12739" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <relation from="o12738" id="r1572" name="with" negation="false" src="d0_s5" to="o12739" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts leaflike, sheathing, sheath 4+ mm, longer than diameter of stem;</text>
      <biological_entity constraint="proximal" id="o12740" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o12741" name="sheath" name_original="sheath" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" upper_restricted="false" />
        <character constraint="than diameter of stem" constraintid="o12742" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o12742" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral spikes androgynous or pistillate, pedunculate, prophyllate;</text>
      <biological_entity constraint="lateral" id="o12743" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="androgynous" value_original="androgynous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="prophyllate" value_original="prophyllate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>terminal spike staminate.</text>
      <biological_entity constraint="terminal" id="o12744" name="spike" name_original="spike" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Proximal pistillate scales 1-veined, apex cuspidate or awned.</text>
      <biological_entity constraint="proximal" id="o12745" name="spike" name_original="spikes" src="d0_s9" type="structure" />
      <biological_entity id="o12746" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o12747" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Perigynia spreading or reflexed, many-veined, stipitate, subulate, round in cross-section, base tapered, apex tapered to beak, glabrous;</text>
      <biological_entity id="o12748" name="perigynium" name_original="perigynia" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="many-veined" value_original="many-veined" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
        <character constraint="in cross-section" constraintid="o12749" is_modifier="false" name="shape" src="d0_s10" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o12749" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o12750" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o12751" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character constraint="to beak" constraintid="o12752" is_modifier="false" name="shape" src="d0_s10" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12752" name="beak" name_original="beak" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>beak straight, 3–4 mm, bidentate, teeth reflexed.</text>
      <biological_entity id="o12753" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="bidentate" value_original="bidentate" />
      </biological_entity>
      <biological_entity id="o12754" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Stigmas 3.</text>
      <biological_entity id="o12755" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes linearoblong, smaller than bodies of perigynia;</text>
      <biological_entity id="o12756" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character constraint="than bodies" constraintid="o12757" is_modifier="false" name="size" src="d0_s13" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o12757" name="body" name_original="bodies" src="d0_s13" type="structure" />
      <biological_entity id="o12758" name="perigynium" name_original="perigynia" src="d0_s13" type="structure" />
      <relation from="o12757" id="r1573" name="part_of" negation="false" src="d0_s13" to="o12758" />
    </statement>
    <statement id="d0_s14">
      <text>style persistent.</text>
      <biological_entity id="o12759" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26tt.</number>
  <discussion>Species 1.</discussion>
  <discussion>Carex collinsii belongs to a monotypic section of uncertain relationships; it has been placed within sections Folliculatae and Lupulinae and considered closely related to section Microglochin. Carex collinsii lacks the entire beak and protruding rachilla characteristic of the latter section. The almost hooked teeth of the perigynium and obliquely cleft beak are not found in any other species of Carex. No intersectional hybrids are reported.</discussion>
  
</bio:treatment>