<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">cyperus</taxon_name>
    <taxon_name authority="(Kükenthal) J. Rich. Carter &amp; S. D. Jones" date="1998" rank="species">floribundus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>99: 330. 1998</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus cyperus;species floribundus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357665</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="Torrey &amp; Hooker" date="unknown" rank="species">uniflorus</taxon_name>
    <taxon_name authority="Kükenthal" date="unknown" rank="variety">floribundus</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler, Pflanzenr.</publication_title>
      <place_in_publication>20[IV,101]: 521. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cyperus;species uniflorus;variety floribundus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="Torrey &amp; Hooker" date="unknown" rank="species">uniflorus</taxon_name>
    <place_of_publication>
      <place_in_publication>1836</place_in_publication>
      <other_info_on_pub>not Thunberg 1825</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Cyperus;species uniflorus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, rhizomatous.</text>
      <biological_entity id="o28065" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms trigonous, basally tuberous thickened, 15–40 cm × 1–2.5 mm, glabrous.</text>
      <biological_entity id="o28066" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves flat to V-shaped, 10–30 cm × 1–2 mm.</text>
      <biological_entity id="o28067" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s2" to="v-shaped" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: spike 1, densely oblong to ellipsoid, 8–36 × 8–20 mm;</text>
      <biological_entity id="o28068" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o28069" name="spike" name_original="spike" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character char_type="range_value" from="densely oblong" name="shape" src="d0_s3" to="ellipsoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="36" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rays 4–12, 10–20 cm;</text>
      <biological_entity id="o28070" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o28071" name="ray" name_original="rays" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="12" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s4" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 3–6, horizontal to ascending at 30°, V-shaped, 3–15 cm × (3–) 4–5.8 mm;</text>
      <biological_entity id="o28072" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o28073" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="horizontal" modifier="30°" name="orientation" src="d0_s5" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s5" value="v--shaped" value_original="v--shaped" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s5" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="5.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachilla deciduous, wings 0.5–0.7 mm wide.</text>
      <biological_entity id="o28074" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o28075" name="rachillum" name_original="rachilla" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o28076" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets (5–) 20–35 (–60), linear to oblong, quadrangular;</text>
    </statement>
    <statement id="d0_s8">
      <text>4–12 (–21) × 0.7–1.2 mm, base narrowed to 0.4–1 mm;</text>
      <biological_entity id="o28077" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s7" to="20" to_inclusive="false" />
        <character char_type="range_value" from="35" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="60" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s7" to="35" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrangular" value_original="quadrangular" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="21" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28078" name="base" name_original="base" src="d0_s8" type="structure">
        <character constraint="to 0.4-1 mm" is_modifier="false" name="shape" src="d0_s8" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>floral scales persistent, 1–3 (–5), appressed, laterally whitish to reddish-brown, stramineous with reddish spots, medially green, laterally 3–6-ribbed (midrib distinctly scabrid at 30X), narrowly oblong to ovate, 2.6–4.8 × 1.6–2 mm;</text>
      <biological_entity constraint="floral" id="o28079" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="laterally whitish" name="coloration" src="d0_s9" to="reddish-brown" />
        <character constraint="with reddish spots" is_modifier="false" name="coloration" src="d0_s9" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="medially" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="3-6-ribbed" value_original="3-6-ribbed" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="length" src="d0_s9" to="4.8" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>apex rounded to ± acute, entire, with mucro 0.3–0.5 mm;</text>
      <biological_entity id="o28080" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s10" to="more or less acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o28081" name="mucro" name_original="mucro" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
      <relation from="o28080" id="r3462" name="with" negation="false" src="d0_s10" to="o28081" />
    </statement>
    <statement id="d0_s11">
      <text>sterile terminal scale involute, 0.4–0.6 mm wide, uncinate;</text>
      <biological_entity constraint="terminal" id="o28082" name="scale" name_original="scale" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="involute" value_original="involute" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s11" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="uncinate" value_original="uncinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>distal floral scales with cusp 0.6–1.9.</text>
      <biological_entity constraint="distal floral" id="o28083" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o28084" name="cusp" name_original="cusp" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.6" name="quantity" src="d0_s12" to="1.9" />
      </biological_entity>
      <relation from="o28083" id="r3463" name="with" negation="false" src="d0_s12" to="o28084" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: anthers 0.5–1.3 mm;</text>
      <biological_entity id="o28085" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o28086" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.4–0.5 mm;</text>
      <biological_entity id="o28087" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o28088" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas 0.5–0.9 mm.</text>
      <biological_entity id="o28089" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o28090" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes light-brown to reddish-brown, sessile to slightly stipitate, narrowly ellipsoid, 1.8–2.4 × 0.6–0.8 mm, apex slightly apiculate, surfaces puncticulate.</text>
      <biological_entity id="o28091" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s16" to="reddish-brown" />
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s16" to="slightly stipitate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s16" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s16" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28092" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s16" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o28093" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s16" value="puncticulate" value_original="puncticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting mid summer–early fall (Jul–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="early fall" from="mid summer" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, disturbed soils, croplands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="damp" />
        <character name="habitat" value="disturbed soils" />
        <character name="habitat" value="croplands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>87.</number>
  <discussion>Included previously in Cyperus retroflexus (or its synonym C. uniflorus), C. floribundus has a much narrower geographic range (southern Texas and northeastern Mexico) and differs in features of spikelet morphology (J. R. Carter and S. D. Jones 1997).</discussion>
  
</bio:treatment>