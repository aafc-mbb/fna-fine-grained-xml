<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan T. Whittemore,Alfred E. Schuyler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">28</other_info_on_meta>
    <other_info_on_meta type="mention_page">56</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="treatment_page">8</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">SCIRPUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 47. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 26. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus SCIRPUS</taxon_hierarchy>
    <other_info_on_name type="etymology">classical Latin name for Schoenoplectus lacustris, derivation unknown</other_info_on_name>
    <other_info_on_name type="fna_id">129748</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose or not, rhizomatous or not.</text>
      <biological_entity id="o32007" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" src="d0_s0" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="architecture" src="d0_s0" value="not" value_original="not" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms solitary or not, ± trigonous.</text>
      <biological_entity id="o32008" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline or all cauline;</text>
      <biological_entity id="o32009" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character name="position" src="d0_s2" value="all" value_original="all" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths not fibrous;</text>
      <biological_entity id="o32010" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules present, rarely absent;</text>
      <biological_entity id="o32011" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades flat or V-shaped in cross-section, prominently keeled abaxially.</text>
      <biological_entity id="o32012" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character constraint="in cross-section" constraintid="o32013" is_modifier="false" name="shape" src="d0_s5" value="v--shaped" value_original="v--shaped" />
        <character is_modifier="false" modifier="prominently; abaxially" name="shape" notes="" src="d0_s5" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o32013" name="cross-section" name_original="cross-section" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, sometimes also axillary in 1–3 distal leaves, subumbellate or corymbose-paniculate;</text>
      <biological_entity id="o32014" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character constraint="in distal leaves" constraintid="o32015" is_modifier="false" modifier="sometimes" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="subumbellate" value_original="subumbellate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="corymbose-paniculate" value_original="corymbose-paniculate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o32015" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikelets 50–500;</text>
      <biological_entity id="o32016" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s7" to="500" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts usually 3, leaflike.</text>
      <biological_entity id="o32017" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets less than 3.5 (–5) mm diam.;</text>
      <biological_entity id="o32018" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>scales 10–50, spirally arranged, each scale subtending flower, glabrous.</text>
      <biological_entity id="o32019" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="50" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s10" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o32020" name="scale" name_original="scale" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o32021" name="flower" name_original="flower" src="d0_s10" type="structure" />
      <relation from="o32020" id="r3971" name="subtending" negation="false" src="d0_s10" to="o32021" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers bisexual;</text>
      <biological_entity id="o32022" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth of (0–) 3–6 bristles;</text>
      <biological_entity id="o32023" name="perianth" name_original="perianth" src="d0_s12" type="structure" />
      <biological_entity id="o32024" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="atypical_quantity" src="d0_s12" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s12" to="6" />
      </biological_entity>
      <relation from="o32023" id="r3972" name="consist_of" negation="false" src="d0_s12" to="o32024" />
    </statement>
    <statement id="d0_s13">
      <text>bristles straight or strongly curled, smooth, or variously toothed, or barbed, shorter to much longer than achene, not obscuring scales in fruit;</text>
      <biological_entity id="o32025" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s13" value="curled" value_original="curled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="variously" name="shape" src="d0_s13" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="barbed" value_original="barbed" />
        <character is_modifier="false" modifier="variously" name="shape" src="d0_s13" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="barbed" value_original="barbed" />
      </biological_entity>
      <biological_entity id="o32027" name="achene" name_original="achene" src="d0_s13" type="structure">
        <character is_modifier="true" name="size_or_length" src="d0_s13" value="shorter to much" value_original="shorter to much" />
        <character is_modifier="true" modifier="much" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o32026" name="achene" name_original="achene" src="d0_s13" type="structure">
        <character is_modifier="true" name="size_or_length" src="d0_s13" value="shorter to much" value_original="shorter to much" />
        <character is_modifier="true" modifier="much" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o32028" name="scale" name_original="scales" src="d0_s13" type="structure" />
      <biological_entity id="o32029" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
      <relation from="o32025" id="r3973" name="shorter to much" negation="false" src="d0_s13" to="o32027" />
      <relation from="o32025" id="r3974" name="obscuring" negation="true" src="d0_s13" to="o32028" />
      <relation from="o32025" id="r3975" name="in" negation="true" src="d0_s13" to="o32029" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 1–3;</text>
      <biological_entity id="o32030" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s14" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles (2–) 3-fid, linear, base persistent.</text>
      <biological_entity id="o32031" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="(2-)3-fid" value_original="(2-)3-fid" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o32032" name="base" name_original="base" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes trigonous, biconvex or planoconvex, 0.6–1.8 mm, minutely papillose.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 14.</text>
      <biological_entity id="o32033" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s16" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" name="shape" src="d0_s16" value="planoconvex" value_original="planoconvex" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="x" id="o32034" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Eurasia, Australia, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Bulrush</other_name>
  <other_name type="common_name">scirpe</other_name>
  <discussion>Species ca. 35 (18 in the flora).</discussion>
  <discussion>Culms are unbranched proximal to the inflorescence and reach 30–200 cm. Leaves range from 3 to 22(–26) per culm and are 3-ranked; blades are always well developed, 110–800 × 3–23 mm, smooth or with scabrous margins and midribs.</discussion>
  <discussion>The inflorescence in Scirpus is a large compound cyme of 50–500 spikelets per inflorescence. The spikelets in the individual cymules may be sessile, with the cymules forming dense glomerules of spikelets, or the lateral (never terminal!) spikelets may be pedicellate, with the inflorescence larger and more open. Involucral bracts are spreading or ascending, 13–230 mm, or exceeding spikelets, smooth or margins and midrib scabrous.</discussion>
  <discussion>Scales are deciduous, green, brown, or blackish, not keeled, without lateral ribs, uniform in length along spikelet, and the apex rounded to obtuse, mucronate, or short-awned.</discussion>
  <discussion>When present the perianth is usually persistent, rarely caducous, white or brown; bristles are vestigial or to 8 mm, shorter to much longer than the scale; margins are smooth or antrorsely or retrorsely toothed or barbed. The filaments often persist after the anthers have been shed, and they are sometimes mistaken for perianth bristles. Only three filaments occur per flower; they are thicker than the bristles, do not taper distally like the bristles, and, unlike bristles in some species, are neither toothed nor contorted. The perigynium is absent.</discussion>
  <discussion>Scirpus hybrids are usually sterile, or at least show greatly reduced fertility. Interspecific hybrids are usually easy to recognize because most or all of their ovaries are empty. In addition, their spikelets are often more elongate than the spikelets of the parent species; this is probably a result of low seed sets, because growth of the spikelet is not halted as nutrition is diverted to developing seeds.</discussion>
  <discussion>Some species of Scirpus are weedy, and their small achenes are well adapted for accidental transport by humans. Most of the disjunct, outlying populations in species such as Scirpus pendulus and S. pallidus probably represent human introductions.</discussion>
  <references>
    <reference>Schuyler, A. E. 1967. A taxonomic revision of North American leafy species of Scirpus. Proc. Acad. Nat. Sci. Philadelphia 119: 295–323.</reference>
    <reference>Strong, M. T. 1994. Taxonomy of Scirpus, Trichophorum, and Schoenoplectus (Cyperaceae) in Virginia. Bartonia 58: 29–68.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth bristles always present, smooth, strongly contorted, much longer than achenes (sometimes not projecting beyond them because of their contortion); lateral heads of cymules pedicellate, all heads in open cymes (heads sometimes sessile in S. cyperinus).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth bristles present (usually rudimentary or absent in S. georgianus), margins toothed or barbate (except rudimentary bristles of S. georgianus, which are much shorter than achenes); bristles straight, curved, or contorted, shorter than or longer than achenes; all heads of cymules sessile (sometimes, minority of cymules with only 1 head), thus some or all heads sessile in dense glomerules (lateral heads of cymules pedicellate in S. divaricatus).</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scales usually with prominent green midribs; mature perianth bristles enclosed within scales or scarcely projecting beyond them; achenes 1–1.5 mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scales usually with pale or inconspicuous midribs; mature perianth bristles exceeding scales and giving inflorescence a woolly appearance; achenes 0.6–1 mm.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Rays and pedicels scabrous or scabrellous throughout or only in distal 1/2, often with axillary bulblets; mature culms reclining, inflorescences bending to (or nearly to) ground, with 2–3 lateral inflorescences in axils of distal leaves in addition to terminal inflorescence; leaves 6–13 mm wide.</description>
      <determination>2 Scirpus lineatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Rays and pedicels scabrous near nodes, otherwise smooth, axillary bulblets absent; mature culms erect or ascending, with terminal inflorescences only or sometimes with 1(–2) lateral inflorescences in axils of distal leaves; leaves 4–8(–12) mm wide.</description>
      <determination>3 Scirpus pendulus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants spreading; rhizomes elongate, not branching; involucral bracts glutinous at base; scales mostly 2–3.1 mm; achenes reddish brown.</description>
      <determination>15 Scirpus longii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants aggregated in dense tussocks; rhizomes short, branching; involucral bracts not glutinous at base; scales mostly 1.1–2.2 mm; achenes whitish to very pale brown.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Spikelets mostly solitary, with distinct pedicels; scales usually blackish, at least distally; achenes maturing late Jun–early Jul, later in far north.</description>
      <determination>16 Scirpus atrocinctus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Spikelets solitary, with distinct pedicels or in glomerules and sessile; scales pale brown, reddish brown, brown, or sometimes blackish; achenes maturing Jul–Sep in ne United States.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Spikelets in open cymes, central spikelet of each cyme sessile, others usually pedicellate; scales usually pale brown, black pigment absent (or sometimes a little beside distal midrib); achenes maturing Jul.</description>
      <determination>17 Scirpus pedicellatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Spikelets in cymes of 2–15, central spikelet of each cyme sessile, others sessile or pedicellate; scales reddish brown, brownish, or blackish; achenes maturing Aug–Sep.</description>
      <determination>18 Scirpus cyperinus</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Spikelets in open cymes, central spikelet of each cyme sessile, others long-pedicellate; mature scales with broad green midribs; achenes strongly trigonous, with very strong angles, concave sides.</description>
      <determination>1 Scirpus divaricatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Spikelets in dense clusters of (1–)3–130, sessile; mature scales with midribs sometimes green, more often pale or not differentiated; achenes biconvex, plumply trigonous, or plano-convex, angles less strongly protruding, sides convex, plane, or slightly concave.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves 14–22(–26) per culm; spikelets broadly ovoid; scales reddish brown, circular or nearly so.</description>
      <determination>10 Scirpus polyphyllus</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves 3–12 per culm; spikelets broadly to narrowly ovoid; scales green, brown, or black, elliptic to broadly elliptic, ovate or broadly ovate to nearly triangular or broadly triangular (almost always longer than broad).</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Teeth of perianth bristles thick-walled, sharp-pointed, densely crowded over distal 0.6 or more of bristle length; distal branches of inflorescence scabrous, proximal branches smooth or scabrellous.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Teeth of perianth bristles thin-walled, round-tipped, mostly restricted to distal 0.6 or less of bristle length; branches of inflorescence smooth throughout or distal branches scabrous.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants cespitose, rhizomes short, brownish; sheaths of proximal leaves green to whitish or brown; achenes 0.6–0.8 mm wide.</description>
      <determination>9 Scirpus ancistrochaetus</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants not cespitose, rhizomes long, reddish, with conspicuous nodes and internodes; sheaths of proximal leaves red; achenes (0.6–)0.8–1 mm wide.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Styles 3-fid; achenes plano-convex or sometimes plumply trigonous; perianth bristles brittle-based, readily detached.</description>
      <determination>13 Scirpus expansus</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Styles 2-fid (rarely, small minority of flowers with 3-fid styles); achenes biconvex to plano-convex; perianth bristles not brittle, persistent.</description>
      <determination>14 Scirpus microcarpus</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Perianth bristles contorted, much longer than achene and projecting beyond it, with scattered, often inconspicuous, antrorse teeth in distal 1/2.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Perianth bristles contorted or nearly straight, shorter than to 1.5 times as long as achene, if long then contorted, not projecting beyond achene, with retrorse, thin-walled, round-tipped barbs in distal (0.1–)0.2–0.6 (bristles usually rudimentary, often ± smooth in S. georgianus).</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Achenes well formed, 0.9–1.3 mm, seeds fertile; w United States.</description>
      <determination>11 Scirpus congdonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Achenes poorly formed, 0.5 mm, seeds aborted; ne United States.</description>
      <determination>Scirpus atrocinctus x S. hattorianus</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Perianth bristles contorted (or some almost straight), shorter than to 1.5 times as long as achene; plants spreading; rhizomes elongate; inflorescence branches spreading or ascending, smooth throughout; achene 0.7–0.8 mm wide; California.</description>
      <determination>12 Scirpus diffusus</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Perianth bristles straight or curved, shorter than or scarcely longer than achenes; plants cespitose; rhizomes short; inflorescence branches ascending or divergent (commonly both in same inflorescence), smooth to scabrous distally, smooth proximally; achene 0.3–0.6 mm wide; widespread, not California.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Perianth bristles 0–3, much shorter than (rarely to 0.7 times as long as) achene, with teeth, if present, only near tips of bristles.</description>
      <determination>4 Scirpus georgianus</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Perianth bristles usually 5–6, shorter than or scarcely longer than achenes, with retrorse, thin-walled, round-tipped barbs in distal (0.1–)0.2–0.6.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Scales 1.6–2.8 mm, apex terete or flat-awned to 0.4–0.6(–1.2) mm; mostly w of Mississippi River.</description>
      <determination>8 Scirpus pallidus</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Scales 1–2.1 mm, apex mucronate, mucro to 0.1–0.3(–0.4) mm; mostly e of Great Plains.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Mature culms lax, reclining (inflorescences lopping over to or nearly to ground); spikelets in clusters of 3–19 (largest cluster with 12 or more spikelets); proximal scales of spikelets blackish; se Virginia, ne North Carolina.</description>
      <determination>5 Scirpus flaccidifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Mature culms erect or nearly so; spikelets in clusters of 4–110 (largest cluster with 15 or more spikelets); proximal scales of spikelets blackish or brownish; widespread, North America.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Septa in blades and sheaths of proximal leaves many, ± conspicuous; spikelets ovoid or narrowly ovoid, 2–5(–8) mm; scales dark brown; longest bristles often slightly exceeding achenes; achenes mostly (0.8–)1–1.3 mm.</description>
      <determination>7 Scirpus atrovirens</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Septa in blades and sheaths of proximal leaves few to many, rather inconspicuous; spikelets broadly ovoid or ovoid, 2–3.5 mm; scales blackish or occasionally brownish; longest bristles usually shorter than or ± equaling achenes; achenes mostly (0.6–)0.7–1.1 mm.</description>
      <determination>6 Scirpus hattorianus</determination>
    </key_statement>
  </key>
</bio:treatment>