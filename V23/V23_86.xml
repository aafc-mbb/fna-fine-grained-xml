<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="treatment_page">58</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="(Reichenbach) Palla" date="1888" rank="genus">schoenoplectus</taxon_name>
    <taxon_name authority="(Reichenbach) J. Raynal" date="1976" rank="section">actaeogeton</taxon_name>
    <taxon_name authority="(Linnaeus) Palla" date="1888" rank="species">mucronatus</taxon_name>
    <place_of_publication>
      <publication_title>Verh. K. K. Zool.-Bot. Ges. Wien</publication_title>
      <place_in_publication>38(Sitzungsber.): 49. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus schoenoplectus;section actaeogeton;species mucronatus</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101173</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">mucronatus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 50. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species mucronatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial (or annual?);</text>
      <biological_entity id="o17275" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes hard.</text>
      <biological_entity id="o17276" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="hard" value_original="hard" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms sharply trigonous, 0.4–1 m × 2–3 mm.</text>
      <biological_entity id="o17277" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s2" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="0.4" from_unit="m" name="length" src="d0_s2" to="1" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 1–2;</text>
      <biological_entity id="o17278" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheath fronts not pinnate-fibrillose;</text>
      <biological_entity constraint="sheath" id="o17279" name="front" name_original="fronts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s4" value="pinnate-fibrillose" value_original="pinnate-fibrillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules absent;</text>
      <biological_entity id="o17280" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades absent.</text>
      <biological_entity id="o17281" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences capitate;</text>
      <biological_entity id="o17282" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>proximal bract divergent to reflexed or rarely erect, trigonous, adaxially channeled, 1–10 cm.</text>
      <biological_entity constraint="proximal" id="o17283" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s8" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s8" value="channeled" value_original="channeled" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4–20, 7–12 × 4 mm;</text>
      <biological_entity id="o17284" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="20" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>scales orangebrown to straw-colored, central region often greenish, broadly obovate, 3–3.5 × 2–2.5 mm, margins ciliolate, apex obtuse to broadly acute, entire, mucronate.</text>
      <biological_entity id="o17285" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="orangebrown" name="coloration" src="d0_s10" to="straw-colored" />
      </biological_entity>
      <biological_entity constraint="central" id="o17286" name="region" name_original="region" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17287" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o17288" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="broadly acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s10" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: perianth members 6, brown, bristlelike, equaling achene, stout, retrorsely spinulose;</text>
      <biological_entity id="o17289" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="perianth" id="o17290" name="member" name_original="members" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="bristlelike" value_original="bristlelike" />
      </biological_entity>
      <biological_entity id="o17291" name="achene" name_original="achene" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="retrorsely" name="architecture_or_shape" src="d0_s11" value="spinulose" value_original="spinulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 0.8 mm;</text>
      <biological_entity id="o17292" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o17293" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.8" value_original="0.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 3-fid (or some 2-fid in same spikelet).</text>
      <biological_entity id="o17294" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o17295" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes dark to blackish brown, thickly planoconvex to obtusely trigonous, broadly obovoid, 1.7–2.2 (–2.5) × 1.2–1.7 mm;</text>
      <biological_entity id="o17296" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character char_type="range_value" from="dark" name="coloration" src="d0_s14" to="blackish brown" />
        <character char_type="range_value" from="thickly planoconvex" name="shape" src="d0_s14" to="obtusely trigonous" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s14" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s14" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak 0.2 mm.</text>
      <biological_entity id="o17297" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil to emergent in fresh water, ponds, ditches, rice fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fresh water" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="rice fields" modifier="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Ill., Iowa, Ky., Mo., Tenn.; Eurasia; Africa; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Rough-seed bulrush</other_name>
  <other_name type="common_name">ricefield bulrush</other_name>
  <discussion>Schoenoplectus mucronatus was collected before 1900 in New Brunswick and New Jersey; apparently the plants did not persist. It has also been reported from New York and Pennsylvania; I have not seen specimens. Elsewhere, it has become firmly established. It is an important ricefield weed in California (M. K. Bellue 1947), where it was first observed in 1942 and is called “ricefield bulrush.” It was first observed in the Midwest in 1971. Schoenoplectus mucronatus is cultivated for wildlife food near the Columbia River in Clark County, Washington, but apparently is not established in that area. Schoenoplectus mucronatus is very similar to S. triangulatus (Roxburgh) Soják of Asia, which differs in its larger spikelets, spikelet scales, and anthers.</discussion>
  
</bio:treatment>