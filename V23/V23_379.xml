<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="genus">rhynchospora</taxon_name>
    <taxon_name authority="E. L. Bridges &amp; Orzell" date="2000" rank="species">megaplumosa</taxon_name>
    <place_of_publication>
      <publication_title>Lundellia</publication_title>
      <place_in_publication>3: 20, fig. 1. 2000</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus rhynchospora;species megaplumosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">242357904</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose, 20–90 cm, base pale-brown to dark-brown;</text>
      <biological_entity id="o15529" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15530" name="base" name_original="base" src="d0_s0" type="structure">
        <character char_type="range_value" from="pale-brown" name="coloration" src="d0_s0" to="dark-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent or compact, knotty, scaly.</text>
      <biological_entity id="o15531" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
        <character is_modifier="false" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect to arching-ascending, leafy, wandlike.</text>
      <biological_entity id="o15532" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="arching-ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="shape" src="d0_s2" value="wand-like" value_original="wandlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal, few and increasingly distant upculm, shorter than scape;</text>
      <biological_entity id="o15533" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character constraint="than scape" constraintid="o15535" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o15534" name="upculm" name_original="upculm" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="increasingly" name="arrangement" src="d0_s3" value="distant" value_original="distant" />
      </biological_entity>
      <biological_entity id="o15535" name="scape" name_original="scape" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blades narrowly linear, concave proximally, (1–) 2–3 mm wide, tapering and increasingly involute-sulcate proximally, margins scabrid, apex triquetrous, tip narrow but blunt.</text>
      <biological_entity id="o15536" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s4" value="concave" value_original="concave" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="increasingly; proximally" name="architecture" src="d0_s4" value="involute-sulcate" value_original="involute-sulcate" />
      </biological_entity>
      <biological_entity id="o15537" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrid" value_original="scabrid" />
      </biological_entity>
      <biological_entity id="o15538" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="triquetrous" value_original="triquetrous" />
      </biological_entity>
      <biological_entity id="o15539" name="tip" name_original="tip" src="d0_s4" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="shape" src="d0_s4" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: clusters 1 (–2), if 2 then close together, dense, broadly turbinate to hemispheric;</text>
      <biological_entity id="o15540" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="2" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" modifier="together" name="arrangement" src="d0_s5" value="close" value_original="close" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character char_type="range_value" from="broadly turbinate" name="shape" src="d0_s5" to="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary leafy bracts linear, stiff, exceeding clusters.</text>
      <biological_entity id="o15541" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o15542" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="fragility" src="d0_s6" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="position_relational" src="d0_s6" value="exceeding" value_original="exceeding" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets light-brown, narrowly lanceoloid, 8–10 mm, apex acuminate;</text>
      <biological_entity id="o15543" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15544" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile scales lanceolate, convex, (6–) 7–8 mm, apex narrowly acute, low midrib shortexcurrent or not.</text>
      <biological_entity id="o15545" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="convex" value_original="convex" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15546" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o15547" name="midrib" name_original="midrib" src="d0_s8" type="structure">
        <character is_modifier="true" name="position" src="d0_s8" value="low" value_original="low" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth bristles 6, excurved, plumose from base to midbristle, 5–7.5 mm, antrorsely barbellate to tip.</text>
      <biological_entity id="o15548" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="perianth" id="o15549" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="shape" src="d0_s9" value="excurved" value_original="excurved" />
        <character constraint="from base" constraintid="o15550" is_modifier="false" name="shape" src="d0_s9" value="plumose" value_original="plumose" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="7.5" to_unit="mm" />
        <character constraint="to tip" constraintid="o15552" is_modifier="false" modifier="antrorsely" name="architecture" src="d0_s9" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o15550" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o15551" name="midbristle" name_original="midbristle" src="d0_s9" type="structure" />
      <biological_entity id="o15552" name="tip" name_original="tip" src="d0_s9" type="structure" />
      <relation from="o15550" id="r1900" name="to" negation="false" src="d0_s9" to="o15551" />
    </statement>
    <statement id="d0_s10">
      <text>Fruits 1–2 per spikelet, 2.3–2.6 × 1.1–1.2 mm;</text>
      <biological_entity id="o15553" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="per spikelet" constraintid="o15554" from="1" name="quantity" src="d0_s10" to="2" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" notes="" src="d0_s10" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" notes="" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15554" name="spikelet" name_original="spikelet" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>body brown, short-stipitate, tumidly obovoid, subterete, 1.8–2 mm, margin low, broad;</text>
      <biological_entity id="o15555" name="body" name_original="body" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="short-stipitate" value_original="short-stipitate" />
        <character is_modifier="false" modifier="tumidly" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15556" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="low" value_original="low" />
        <character is_modifier="false" name="width" src="d0_s11" value="broad" value_original="broad" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>surfaces interruptedly transversely wavyrugulose;</text>
      <biological_entity id="o15557" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="interruptedly transversely" name="relief" src="d0_s12" value="wavyrugulose" value_original="wavyrugulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tubercle broadly and concavely conic, 0.5–0.7 mm high, base shallowly 2-lobed, discoid, abruptly narrowed to blunt tip.</text>
      <biological_entity id="o15558" name="tubercle" name_original="tubercle" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="concavely" name="shape" src="d0_s13" value="conic" value_original="conic" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="height" src="d0_s13" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15559" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s13" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="discoid" value_original="discoid" />
        <character char_type="range_value" from="abruptly narrowed" name="shape" src="d0_s13" to="blunt" />
      </biological_entity>
      <biological_entity id="o15560" name="tip" name_original="tip" src="d0_s13" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–fall or all year.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" constraint=" -year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sands and sandy peats of pine flatwoods scrub and flatwoods-sandscrub transition</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sands" />
        <character name="habitat" value="sandy peats" constraint="of pine flatwoods" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="scrub" />
        <character name="habitat" value="flatwoods-sandscrub transition" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Rhynchospora megaplumosa is local in central peninsular Florida. It often shares habitat with R. pineticola, and it is taxonomically nearest it in series Plumosae. Distinctive are the longer, paler, narrower spikelets, the longer fertile scales, and perianth bristles of R. megaplumosa. In fact, the perianth bristles of R. megaplumosa are the longest known in the series. While the bristles of all other Plumosae are erect, hugging the achene body, those of R. megaplumosa bend outward so strongly that they push away subtending scales; bristles are conspicuously exposed at maturity.</discussion>
  
</bio:treatment>