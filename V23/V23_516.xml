<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Heikki Toivonen</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="Ohwi" date="1936" rank="section">Dispermae</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Coll. Sci. Kyoto Imp. Univ., Ser. B, Biol.</publication_title>
      <place_in_publication>11: 237. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Dispermae</taxon_hierarchy>
    <other_info_on_name type="fna_id">302690</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely cespitose, long-rhizomatous.</text>
      <biological_entity id="o28548" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="long-rhizomatous" value_original="long-rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms brown at base.</text>
      <biological_entity id="o28549" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o28550" is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o28550" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths fibrous or not;</text>
      <biological_entity id="o28551" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o28552" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
        <character name="texture" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous;</text>
      <biological_entity id="o28553" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o28554" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades V-shaped in cross-section when young, scabrid.</text>
      <biological_entity id="o28555" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o28556" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o28557" is_modifier="false" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <biological_entity id="o28557" name="cross-section" name_original="cross-section" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence_or_relief" src="d0_s4" value="scabrid" value_original="scabrid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose;</text>
      <biological_entity id="o28558" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts bristlelike, sheathless;</text>
      <biological_entity constraint="proximal" id="o28559" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="bristlelike" value_original="bristlelike" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sheathless" value_original="sheathless" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikes 1–6, androgynous, often with only 1 or 2 staminate flowers, sessile, without prophylls.</text>
      <biological_entity id="o28560" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="6" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="androgynous" value_original="androgynous" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o28561" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="only" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o28562" name="prophyll" name_original="prophylls" src="d0_s7" type="structure" />
      <relation from="o28560" id="r3520" modifier="often" name="with" negation="false" src="d0_s7" to="o28561" />
      <relation from="o28560" id="r3521" name="without" negation="false" src="d0_s7" to="o28562" />
    </statement>
    <statement id="d0_s8">
      <text>Proximal pistillate scales with apex acuminate or mucronate.</text>
      <biological_entity constraint="proximal" id="o28563" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o28564" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <relation from="o28563" id="r3522" name="with" negation="false" src="d0_s8" to="o28564" />
    </statement>
    <statement id="d0_s9">
      <text>Perigynia spreading, veined on both faces, stipitate, oblongovate, unequally biconvex in cross-section, base rounded, with spongy tissue, margins acutely angled, apex rounded, beaked, glabrous;</text>
      <biological_entity id="o28565" name="perigynium" name_original="perigynia" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character constraint="on faces" constraintid="o28566" is_modifier="false" name="architecture" src="d0_s9" value="veined" value_original="veined" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="stipitate" value_original="stipitate" />
        <character constraint="in cross-section" constraintid="o28567" is_modifier="false" modifier="unequally" name="shape" src="d0_s9" value="biconvex" value_original="biconvex" />
      </biological_entity>
      <biological_entity id="o28566" name="face" name_original="faces" src="d0_s9" type="structure" />
      <biological_entity id="o28567" name="cross-section" name_original="cross-section" src="d0_s9" type="structure" />
      <biological_entity id="o28568" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o28569" name="tissue" name_original="tissue" src="d0_s9" type="structure">
        <character is_modifier="true" name="texture" src="d0_s9" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o28570" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="acutely" name="shape" src="d0_s9" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o28571" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="beaked" value_original="beaked" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o28568" id="r3523" name="with" negation="false" src="d0_s9" to="o28569" />
    </statement>
    <statement id="d0_s10">
      <text>beak not more than 0.25 mm, with abaxial suture, entire, smooth.</text>
      <biological_entity id="o28572" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s10" upper_restricted="false" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28573" name="suture" name_original="suture" src="d0_s10" type="structure" />
      <relation from="o28572" id="r3524" name="with" negation="false" src="d0_s10" to="o28573" />
    </statement>
    <statement id="d0_s11">
      <text>Stigmas 2.</text>
      <biological_entity id="o28574" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes biconvex, almost filling bodies of perigynia;</text>
      <biological_entity id="o28575" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="biconvex" value_original="biconvex" />
      </biological_entity>
      <biological_entity id="o28576" name="body" name_original="bodies" src="d0_s12" type="structure" />
      <biological_entity id="o28577" name="perigynium" name_original="perigynia" src="d0_s12" type="structure" />
      <relation from="o28575" id="r3525" modifier="almost" name="filling" negation="false" src="d0_s12" to="o28576" />
      <relation from="o28575" id="r3526" modifier="almost" name="part_of" negation="false" src="d0_s12" to="o28577" />
    </statement>
    <statement id="d0_s13">
      <text>style deciduous.</text>
    </statement>
    <statement id="d0_s14">
      <text>x = 35.</text>
      <biological_entity id="o28578" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="x" id="o28579" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="35" value_original="35" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26e.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>