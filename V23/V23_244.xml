<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="mention_page">156</other_info_on_meta>
    <other_info_on_meta type="treatment_page">157</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="C. B. Clarke in J. D. Hooker" date="1893" rank="subgenus">pycnostachys</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">fuscus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 46. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus pycnostachys;species fuscus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200026687</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, cespitose.</text>
      <biological_entity id="o28792" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms trigonous, 2–30 cm × 0.6–1.1 mm, glabrous.</text>
      <biological_entity id="o28793" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s1" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves flat, 4–10 cm × 2–4 mm.</text>
      <biological_entity id="o28794" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: rays 1–3, 0.2–1.5 cm;</text>
      <biological_entity id="o28795" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o28796" name="ray" name_original="rays" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>2d order rays present in robust plants, to 5 mm;</text>
      <biological_entity id="o28797" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o28798" name="ray" name_original="rays" src="d0_s4" type="structure">
        <character constraint="in plants" constraintid="o28799" is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28799" name="plant" name_original="plants" src="d0_s4" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s4" value="robust" value_original="robust" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 2–3, longest erect, others spreading, 1–20 cm × 1.5–3 mm.</text>
      <biological_entity id="o28800" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o28801" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" name="length" src="d0_s5" value="longest" value_original="longest" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o28802" name="other" name_original="others" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 3–12, narrowly ellipsoid, flattened, 3–7 × 0.9–1.2 mm;</text>
      <biological_entity id="o28803" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="12" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s6" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral scales 8–12 (–16), laterally purplish brown, medially greenish yellow, 3-ribbed medially, orbiculate, 0.9–1.1 × 1 mm, apex mucronate.</text>
      <biological_entity constraint="floral" id="o28804" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="16" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="12" />
        <character is_modifier="false" modifier="laterally" name="coloration" src="d0_s7" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" modifier="medially" name="coloration" src="d0_s7" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="medially" name="architecture_or_shape" src="d0_s7" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s7" to="1.1" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o28805" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: stamen 1;</text>
      <biological_entity id="o28806" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o28807" name="stamen" name_original="stamen" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers ellipsoid, 0.2 mm, connective not prolonged;</text>
      <biological_entity id="o28808" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o28809" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o28810" name="connective" name_original="connective" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="length" src="d0_s9" value="prolonged" value_original="prolonged" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 0.3–0.4 mm;</text>
      <biological_entity id="o28811" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o28812" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas 0.3 mm.</text>
      <biological_entity id="o28813" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o28814" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes light-brown, ellipsoid, 0.7–0.9 × 0.4 mm, base barely stipelike to narrowly cuneate, apex acute, surfaces glabrous.</text>
      <biological_entity id="o28815" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s12" to="0.9" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
      <biological_entity id="o28816" name="base" name_original="base" src="d0_s12" type="structure">
        <character char_type="range_value" from="barely stipelike" name="shape" src="d0_s12" to="narrowly cuneate" />
      </biological_entity>
      <biological_entity id="o28817" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28818" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, disturbed soils, emergent shorelines, puddles</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed soils" modifier="damp" />
        <character name="habitat" value="emergent shorelines" />
        <character name="habitat" value="puddles" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont., Que.; Calif., Conn., Md., Mass., Mo., Nebr., Nev., N.J., Pa., S.Dak., Va.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Brown galingale</other_name>
  <discussion>A native of temperate Eurasia, Cyperus fuscus is intermittently adventive and locally established 35°–45° N latitude. The report from New York (M. L. Fernald 1950) is based on a misidentification of C. diandrus (R. S. Mitchell and G. C. Tucker 1997).</discussion>
  
</bio:treatment>