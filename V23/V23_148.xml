<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="treatment_page">100</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="(Nees) Bentham in G. Bentham and J. D. Hooker" date="unknown" rank="section">eleogenus</taxon_name>
    <taxon_name authority="Svenson" date="1929" rank="series">maculosae</taxon_name>
    <taxon_name authority="(Poiret) Urban" date="1903" rank="species">flavescens</taxon_name>
    <place_of_publication>
      <publication_title>Symb. Antill.</publication_title>
      <place_in_publication>4: 116. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleogenus;series maculosae;species flavescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357756</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Poiret" date="unknown" rank="species">flavescens</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>6: 756. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species flavescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with creeping rhizomes 0.5–1 mm thick.</text>
      <biological_entity id="o28142" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o28143" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o28142" id="r3469" name="with" negation="false" src="d0_s0" to="o28143" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 3–42 cm × 0.3–0.6 mm.</text>
      <biological_entity id="o28144" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s1" to="42" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s1" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: distal leaf-sheaths often disintegrating, thinly membranous-translucent, inflated distally, often wrinkled, apex blunt.</text>
      <biological_entity id="o28145" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o28146" name="sheath" name_original="leaf-sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="dehiscence" src="d0_s2" value="disintegrating" value_original="disintegrating" />
        <character is_modifier="false" modifier="thinly" name="coloration_or_reflectance" src="d0_s2" value="membranous-translucent" value_original="membranous-translucent" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="often" name="relief" src="d0_s2" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <biological_entity id="o28147" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikelets ellipsoid, 1.5–9 × 1–3.5 mm, apex acute to acuminate;</text>
      <biological_entity id="o28148" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28149" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal scale without flower, not amplexicaulous;</text>
      <biological_entity constraint="proximal" id="o28150" name="scale" name_original="scale" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s4" value="amplexicaulous" value_original="amplexicaulous" />
      </biological_entity>
      <biological_entity id="o28151" name="flower" name_original="flower" src="d0_s4" type="structure" />
      <relation from="o28150" id="r3470" name="without" negation="false" src="d0_s4" to="o28151" />
    </statement>
    <statement id="d0_s5">
      <text>floral scales to 65, 5–7 per mm of rachilla, loosely appressed to appressed, elliptic, 1–3 × 0.4–1.6 mm, membranous, apex acute.</text>
      <biological_entity constraint="floral" id="o28152" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="65" />
        <character char_type="range_value" constraint="per mm" constraintid="o28153" from="5" name="quantity" src="d0_s5" to="7" />
        <character char_type="range_value" from="loosely appressed" name="fixation_or_orientation" notes="" src="d0_s5" to="appressed" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s5" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o28153" name="mm" name_original="mm" src="d0_s5" type="structure" />
      <biological_entity id="o28154" name="rachillum" name_original="rachilla" src="d0_s5" type="structure" />
      <biological_entity id="o28155" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o28153" id="r3471" name="part_of" negation="false" src="d0_s5" to="o28154" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth bristles (0–) 5–8, typically 7, white to stramineous, spinules dense to few;</text>
      <biological_entity id="o28156" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="perianth" id="o28157" name="bristle" name_original="bristles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s6" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="8" />
        <character modifier="typically" name="quantity" src="d0_s6" value="7" value_original="7" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s6" to="stramineous" />
      </biological_entity>
      <biological_entity id="o28158" name="spinule" name_original="spinules" src="d0_s6" type="structure">
        <character is_modifier="false" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="few" value_original="few" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>styles 2-fid, rarely 3-fid.</text>
      <biological_entity id="o28159" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o28160" name="style" name_original="styles" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes stramineous to green or dark-brown, biconvex, rarely trigonous, obovoid to obpryiform, 0.4–1.1 × 0.3–0.8 mm, very finely reticulate at 40X.</text>
      <biological_entity id="o28161" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s8" to="green or dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s8" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="trigonous" value_original="trigonous" />
        <character constraint="to obpryiform" constraintid="o28162" is_modifier="false" name="shape" src="d0_s8" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" notes="" src="d0_s8" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" notes="" src="d0_s8" to="0.8" to_unit="mm" />
        <character constraint="at 40x" is_modifier="false" modifier="very finely" name="architecture_or_coloration_or_relief" src="d0_s8" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o28162" name="obpryiform" name_original="obpryiform" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Tubercles whitish to stramineous or green, 0.2–0.7 × 0.2–0.4 mm, apex acute to acuminate.</text>
      <biological_entity id="o28163" name="tubercle" name_original="tubercles" src="d0_s9" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s9" to="stramineous or green" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s9" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s9" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28164" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que.; Ala., Ariz., Ark., Calif., Conn., Del., Fla., Ga., Idaho, Ill., Ind., La., Maine, Mass., Md., Mich., Minn., Miss., Mont., N.C., N.H., N.J., N.Y., Ohio, Pa., R.I., S.C., Tex., Utah, Va., Vt., Wis., Wyo.; temperate North America, West Indies, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="temperate North America" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39.</number>
  <other_name type="common_name">Pale spike-rush</other_name>
  <other_name type="common_name">wrinkle-sheathed spike-rush</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The varieties of Eleocharis flavescens are difficult to delimit, especially in the south, and identifications of some specimens to variety are problematic.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achenes red-brown to dark brown when ripe, 0.4–0.8(–1.1) × 0.3–0.6 mm, apex rarely highly constricted proximal to tubercle; flowers with perianth bristles typically shorter than to as long as achene.</description>
      <determination>39a Eleocharis flavescens var. flavescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achenes green to golden-brown, 0.5–1.1 × 0.4–0.8 mm, often highly constricted proximal to tubercle; flowers with perianth bristles typically longer than achene.</description>
      <determination>39b Eleocharis flavescens var. olivacea</determination>
    </key_statement>
  </key>
</bio:treatment>