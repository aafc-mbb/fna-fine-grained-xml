<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">204</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="treatment_page">223</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="genus">rhynchospora</taxon_name>
    <taxon_name authority="C. Wright ex Grisebach" date="1866" rank="species">odorata</taxon_name>
    <place_of_publication>
      <publication_title>Cat. Pl. Cub.,</publication_title>
      <place_in_publication>242. 1866</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus rhynchospora;species odorata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357911</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phaeocephalum</taxon_name>
    <taxon_name authority="(Chapman) House" date="unknown" rank="species">stipitatum</taxon_name>
    <taxon_hierarchy>genus Phaeocephalum;species stipitatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchospora</taxon_name>
    <taxon_name authority="Chapman" date="unknown" rank="species">stipitata</taxon_name>
    <taxon_hierarchy>genus Rhynchospora;species stipitata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose, 100–180 cm;</text>
      <biological_entity id="o4682" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="180" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes often present, short, scaly.</text>
      <biological_entity id="o4683" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect to ascending, leafy, slender, angular.</text>
      <biological_entity id="o4684" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="angular" value_original="angular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves exceeded by culm;</text>
      <biological_entity id="o4685" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4686" name="culm" name_original="culm" src="d0_s3" type="structure" />
      <relation from="o4685" id="r569" name="exceeded by" negation="false" src="d0_s3" to="o4686" />
    </statement>
    <statement id="d0_s4">
      <text>blades linear, proximally flat, 3–6 mm wide, apex tapering, trigonous, subulate.</text>
      <biological_entity id="o4687" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="proximally" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4688" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s4" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences of terminal and axillary clusters, 3–5, proximalmost widely spaced, turbinate or lobed, fascicles dense, branches ascending;</text>
      <biological_entity id="o4689" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o4690" name="terminal" name_original="terminal" src="d0_s5" type="structure" />
      <biological_entity constraint="proximalmost" id="o4691" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s5" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o4692" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o4689" id="r570" name="part_of" negation="false" src="d0_s5" to="o4690" />
    </statement>
    <statement id="d0_s6">
      <text>leafy bracts exceeding all but proximalmost clusters.</text>
      <biological_entity id="o4693" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o4694" name="bract" name_original="bracts" src="d0_s6" type="structure" />
      <relation from="o4693" id="r571" name="exceeding all but" negation="false" src="d0_s6" to="o4694" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets rich redbrown, ovoid, (4–) 5–6 (–7) mm, apex acuminate;</text>
      <biological_entity id="o4695" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="rich" value_original="rich" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4696" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile scales ovate, (3.5–) 4–5 mm, apex acuminate, midrib short or longexcurrent.</text>
      <biological_entity id="o4697" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4698" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o4699" name="midrib" name_original="midrib" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character name="height_or_length_or_size" src="d0_s8" value="longexcurrent" value_original="longexcurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth bristles 6, reaching past tubercle, antrorsely barbellate.</text>
      <biological_entity id="o4700" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="perianth" id="o4701" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" modifier="antrorsely" name="architecture" src="d0_s9" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o4702" name="tubercle" name_original="tubercle" src="d0_s9" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s9" value="past" value_original="past" />
      </biological_entity>
      <relation from="o4701" id="r572" name="reaching" negation="false" src="d0_s9" to="o4702" />
    </statement>
    <statement id="d0_s10">
      <text>Fruits mostly 3–4 (–7) per spikelet, 3 mm with pedicellar joint and tubercle;</text>
      <biological_entity id="o4703" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="7" />
        <character char_type="range_value" constraint="per spikelet" constraintid="o4704" from="3" name="quantity" src="d0_s10" to="4" />
        <character constraint="with tubercle" constraintid="o4706" name="some_measurement" notes="" src="d0_s10" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o4704" name="spikelet" name_original="spikelet" src="d0_s10" type="structure" />
      <biological_entity constraint="pedicellar" id="o4705" name="joint" name_original="joint" src="d0_s10" type="structure" />
      <biological_entity id="o4706" name="tubercle" name_original="tubercle" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>body pale yellowbrown, obovoidlenticular, 1.4–1.7 × 1.4 × 1.5;</text>
      <biological_entity id="o4707" name="body" name_original="body" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale yellowbrown" value_original="pale yellowbrown" />
        <character char_type="range_value" from="1.4" name="quantity" src="d0_s11" to="1.7" />
        <character name="quantity" src="d0_s11" value="1.4" value_original="1.4" />
        <character name="quantity" src="d0_s11" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>surfaces transversely finely wavyrugulose, intervals vertically rectangularalveolate;</text>
      <biological_entity id="o4708" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="transversely finely" name="relief" src="d0_s12" value="wavyrugulose" value_original="wavyrugulose" />
      </biological_entity>
      <biological_entity id="o4709" name="interval" name_original="intervals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>pedicellar joint 0.3–0.6 mm;</text>
      <biological_entity constraint="pedicellar" id="o4710" name="joint" name_original="joint" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tubercle compressed, triangularacuminate, 0.5–0.6 (–1) mm, margin setulose.</text>
      <biological_entity id="o4711" name="tubercle" name_original="tubercle" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4712" name="margin" name_original="margin" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="setulose" value_original="setulose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall or all year (south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" modifier="south" to="fall" from="summer" constraint=" -year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sands and peats of swamps, marshes, interdunal swales, low meadows, savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sands" constraint="of swamps , marshes , interdunal swales , low meadows , savannas" />
        <character name="habitat" value="peats" constraint="of swamps , marshes , interdunal swales , low meadows , savannas" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="interdunal swales" />
        <character name="habitat" value="low meadows" />
        <character name="habitat" value="savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., N.C., S.C.; West Indies (Bahamas, Cuba, Dominican Republic, Jamaica).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="West Indies (Dominican Republic)" establishment_means="native" />
        <character name="distribution" value="West Indies (Jamaica)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>37.</number>
  
</bio:treatment>