<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="treatment_page">117</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="(Palisot de Beauvois ex T. Lestiboudois) Torrey" date="1836" rank="subgenus">limnochloa</taxon_name>
    <taxon_name authority="Oakes" date="1841" rank="species">robbinsii</taxon_name>
    <place_of_publication>
      <publication_title>Mag. Hort. Bot.</publication_title>
      <place_in_publication>7: 178. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus limnochloa;species robbinsii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357784</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o8374" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes (0.5–) 1–2 mm thick, longer internodes 2–3 cm, scales 5–7 mm;</text>
      <biological_entity id="o8375" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s1" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o8376" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8377" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tubers sometimes present, apical, ovoid, 4–8 × 3–4 mm.</text>
      <biological_entity id="o8378" name="tuber" name_original="tubers" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="apical" id="o8379" name="tuber" name_original="tubers" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms acutely trigonous;</text>
    </statement>
    <statement id="d0_s4">
      <text>spikelet-bearing culms 16–70 cm × 0.7–0.9 mm;</text>
      <biological_entity id="o8380" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="acutely" name="shape" src="d0_s3" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>when submersed plants often forming numerous, filiform, flaccid culms without spikelets, sometimes with whorls of slender branches, 0.1–0.3 mm wide;</text>
      <biological_entity id="o8382" name="culm" name_original="culms" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
        <character is_modifier="true" name="texture" src="d0_s5" value="flaccid" value_original="flaccid" />
      </biological_entity>
      <biological_entity id="o8383" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity id="o8384" name="whorl" name_original="whorls" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" notes="alterIDs:o8384" src="d0_s5" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="slender" id="o8385" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <relation from="o8381" id="r1003" modifier="when submersed plants often forming numerous" name="forming" negation="false" src="d0_s5" to="o8382" />
      <relation from="o8381" id="r1004" modifier="when submersed plants often forming numerous" name="without" negation="false" src="d0_s5" to="o8383" />
      <relation from="o8381" id="r1005" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o8384" />
      <relation from="o8384" id="r1006" name="part_of" negation="false" src="d0_s5" to="o8385" />
    </statement>
    <statement id="d0_s6">
      <text>soft, sometimes septate-nodulose when aquatic, internally spongy, transverse septa incomplete.</text>
      <biological_entity id="o8381" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character char_type="range_value" from="16" from_unit="cm" name="length" src="d0_s4" to="70" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s4" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s6" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="when aquatic" name="shape" src="d0_s6" value="septate-nodulose" value_original="septate-nodulose" />
        <character is_modifier="false" modifier="internally" name="texture" src="d0_s6" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o8386" name="septum" name_original="septa" src="d0_s6" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s6" value="transverse" value_original="transverse" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="incomplete" value_original="incomplete" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves: distal leaf-sheaths persistent or decaying, membranous, apex obtuse to acuminate.</text>
      <biological_entity id="o8387" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o8388" name="sheath" name_original="leaf-sheaths" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="duration" src="d0_s7" value="decaying" value_original="decaying" />
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o8389" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets sometimes proliferous (when submerged), 9–33 × 1.5–3 mm;</text>
      <biological_entity id="o8390" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s8" value="proliferous" value_original="proliferous" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s8" to="33" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachilla joints bearing prominent winglike remnants of floral scales;</text>
      <biological_entity constraint="scale" id="o8391" name="joint" name_original="joints" src="d0_s9" type="structure" constraint_original="scale rachilla; scale" />
      <biological_entity id="o8392" name="remnant" name_original="remnants" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s9" value="winglike" value_original="winglike" />
      </biological_entity>
      <biological_entity constraint="floral" id="o8393" name="scale" name_original="scales" src="d0_s9" type="structure" />
      <relation from="o8391" id="r1007" name="bearing" negation="false" src="d0_s9" to="o8392" />
      <relation from="o8391" id="r1008" name="part_of" negation="false" src="d0_s9" to="o8393" />
    </statement>
    <statement id="d0_s10">
      <text>proximal scale with a flower, amplexicaulous, (5–) 6–9.8 mm;</text>
      <biological_entity constraint="proximal" id="o8394" name="scale" name_original="scale" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="amplexicaulous" value_original="amplexicaulous" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="9.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8395" name="flower" name_original="flower" src="d0_s10" type="structure" />
      <relation from="o8394" id="r1009" name="with" negation="false" src="d0_s10" to="o8395" />
    </statement>
    <statement id="d0_s11">
      <text>floral scales 4–18, 0.5–1 per mm of rachilla, stramineous to pale-brown, often minutely dotted reddish, without or rarely with darker submarginal band, narrowly ovate to lanceolate, 5–7.8 × 2–3 mm, thickly papery, membranous toward margins, apex narrowly rounded to acute.</text>
      <biological_entity constraint="floral" id="o8396" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="18" />
        <character char_type="range_value" constraint="per mm" constraintid="o8397" from="0.5" name="quantity" src="d0_s11" to="1" />
        <character char_type="range_value" from="stramineous" name="coloration" notes="" src="d0_s11" to="pale-brown" />
        <character is_modifier="false" modifier="often minutely; minutely" name="coloration" src="d0_s11" value="dotted reddish" value_original="dotted reddish" />
        <character char_type="range_value" from="narrowly ovate" name="shape" notes="" src="d0_s11" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="7.8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="thickly" name="texture" src="d0_s11" value="papery" value_original="papery" />
        <character constraint="toward margins" constraintid="o8400" is_modifier="false" name="texture" src="d0_s11" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o8397" name="mm" name_original="mm" src="d0_s11" type="structure" />
      <biological_entity id="o8398" name="rachillum" name_original="rachilla" src="d0_s11" type="structure" />
      <biological_entity constraint="submarginal" id="o8399" name="band" name_original="band" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o8400" name="margin" name_original="margins" src="d0_s11" type="structure" />
      <biological_entity id="o8401" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="narrowly rounded" name="shape" src="d0_s11" to="acute" />
      </biological_entity>
      <relation from="o8397" id="r1010" name="part_of" negation="false" src="d0_s11" to="o8398" />
      <relation from="o8396" id="r1011" modifier="rarely" name="with" negation="false" src="d0_s11" to="o8399" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers: perianth bristles 6–7, stramineous to reddish-brown, proximally slightly flattened, subequal to equal, much exceeding to rarely shorter than achene, 3–5 mm, retrorsely spinulose;</text>
      <biological_entity id="o8402" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="perianth" id="o8403" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s12" to="7" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s12" to="reddish-brown" />
        <character is_modifier="false" modifier="proximally slightly" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="retrorsely" name="architecture_or_shape" src="d0_s12" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity id="o8405" name="achene" name_original="achene" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="rarely" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8404" name="achene" name_original="achene" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="rarely" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <relation from="o8403" id="r1012" modifier="much" name="exceeding to" negation="false" src="d0_s12" to="o8405" />
    </statement>
    <statement id="d0_s13">
      <text>anthers yellow to reddish, 1.6–3.2 mm;</text>
      <biological_entity id="o8406" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8407" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="reddish" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s13" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 3-fid.</text>
      <biological_entity id="o8408" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o8409" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes stramineous or medium brown, biconvex or compressed trigonous, narrowly obpyriform, 1.9–2.6 × 1–1.4 mm, adaxial face with 15–22 rows of rectangular, transversely elongated or nearly isodiametric cells, clearly sculptured at 10–15X, apex usually conspicuously constricted to short neck 0.4–0.7 mm wide, usually wider at tubercle base.</text>
      <biological_entity id="o8410" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="medium" value_original="medium" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="obpyriform" value_original="obpyriform" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s15" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8411" name="face" name_original="face" src="d0_s15" type="structure">
        <character constraint="at 10-15x" is_modifier="false" modifier="clearly" name="relief" notes="" src="d0_s15" value="sculptured" value_original="sculptured" />
      </biological_entity>
      <biological_entity id="o8412" name="row" name_original="rows" src="d0_s15" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s15" to="22" />
      </biological_entity>
      <biological_entity id="o8413" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="rectangular" value_original="rectangular" />
        <character is_modifier="true" modifier="transversely" name="length" src="d0_s15" value="elongated" value_original="elongated" />
        <character is_modifier="true" modifier="nearly" name="architecture_or_shape" src="d0_s15" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o8414" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character char_type="range_value" from="usually conspicuously constricted" name="size" src="d0_s15" to="short" />
      </biological_entity>
      <biological_entity id="o8415" name="neck" name_original="neck" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s15" to="0.7" to_unit="mm" />
        <character constraint="at tubercle base" constraintid="o8416" is_modifier="false" modifier="usually" name="width" src="d0_s15" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity constraint="tubercle" id="o8416" name="base" name_original="base" src="d0_s15" type="structure" />
      <relation from="o8411" id="r1013" name="with" negation="false" src="d0_s15" to="o8412" />
      <relation from="o8412" id="r1014" name="part_of" negation="false" src="d0_s15" to="o8413" />
    </statement>
    <statement id="d0_s16">
      <text>Tubercles stramineous to medium brown, high-pyramidal, 0.5–1.1 × 0.3–0.7 mm.</text>
      <biological_entity id="o8417" name="tubercle" name_original="tubercles" src="d0_s16" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s16" to="medium brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="high-pyramidal" value_original="high-pyramidal" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s16" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s16" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late spring–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="late fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow waters of fresh lakes and ponds with sandy-peaty soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow waters" constraint="of fresh lakes and ponds" />
        <character name="habitat" value="fresh lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="sandy-peaty soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que.; Ala., Conn., Del., Fla., Ga., Maine, Mass., Mich., Minn., N.H., N.J., N.C., Ohio, S.C., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>62.</number>
  <other_name type="common_name">Robbin’s sppike-rush</other_name>
  <other_name type="common_name">éléocharide Robbins</other_name>
  <discussion>I have not seen voucher specimens for literature reports of Eleocharis robbinsii from Indiana, Pennsylvania, or Rhode Island. Plants from South Carolina with the achene surface cells nearly isodiametric, the achene apex spongy, and the anthers to 3.2 mm may represent an undescribed taxon.</discussion>
  
</bio:treatment>