<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="treatment_page">229</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="genus">rhynchospora</taxon_name>
    <taxon_name authority="A. Dietrich" date="1833" rank="species">elliottii</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 69. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus rhynchospora;species elliottii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357877</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phaeocephalum</taxon_name>
    <taxon_name authority="(Alph. Wood) House" date="unknown" rank="species">schoenoides</taxon_name>
    <taxon_hierarchy>genus Phaeocephalum;species schoenoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchospora</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">multiflora</taxon_name>
    <taxon_hierarchy>genus Rhynchospora;species multiflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchospora</taxon_name>
    <taxon_name authority="Alph. Wood" date="unknown" rank="species">schoenoides</taxon_name>
    <taxon_hierarchy>genus Rhynchospora;species schoenoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Elliott" date="unknown" rank="species">schoenoides</taxon_name>
    <place_of_publication>
      <place_in_publication>1816</place_in_publication>
      <other_info_on_pub>not Retzius 1789</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species schoenoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose, 80–150 cm;</text>
      <biological_entity id="o27258" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent.</text>
      <biological_entity id="o27259" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect with arching tops, leafy, obscurely trigonous, slender.</text>
      <biological_entity id="o27260" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character constraint="with tops" constraintid="o27261" is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s2" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o27261" name="top" name_original="tops" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves overtopped by inflorescence;</text>
      <biological_entity id="o27262" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o27263" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <relation from="o27262" id="r3369" name="overtopped by" negation="false" src="d0_s3" to="o27263" />
    </statement>
    <statement id="d0_s4">
      <text>blades linear, proximally flat, 3–5 mm wide, apex trigonous, subulate, tapering.</text>
      <biological_entity id="o27264" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="proximally" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27265" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: spikelet clusters mostly 4–6, various in shape and crowding, narrowly to broadly turbinate;</text>
      <biological_entity id="o27266" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o27267" name="spikelet" name_original="spikelet" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="false" name="shape" src="d0_s5" value="various" value_original="various" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="crowding" value_original="crowding" />
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles erect, branches slender, ascending;</text>
      <biological_entity id="o27268" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o27269" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o27270" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leafy bracts exceeding all but most distal clusters.</text>
      <biological_entity id="o27271" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o27272" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="position_relational" src="d0_s7" value="exceeding" value_original="exceeding" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27273" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets redbrown, broadly ellipsoid, (1.5–) 2–3 (–3.5) mm, apex acute;</text>
      <biological_entity id="o27274" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27275" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>fertile scales broadly ovate, 2–3 mm, midrib excurrent as apiculus or awn.</text>
      <biological_entity id="o27276" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27277" name="midrib" name_original="midrib" src="d0_s9" type="structure">
        <character constraint="as awn" constraintid="o27279" is_modifier="false" name="architecture" src="d0_s9" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o27278" name="apiculu" name_original="apiculus" src="d0_s9" type="structure" />
      <biological_entity id="o27279" name="awn" name_original="awn" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: perianth bristles 6, mostly spreading, usually exceeding tubercle, antrorsely barbellate.</text>
      <biological_entity id="o27280" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="perianth" id="o27281" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="antrorsely" name="architecture" src="d0_s10" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o27282" name="tubercle" name_original="tubercle" src="d0_s10" type="structure" />
      <relation from="o27281" id="r3370" modifier="usually" name="exceeding" negation="false" src="d0_s10" to="o27282" />
    </statement>
    <statement id="d0_s11">
      <text>Fruits 2–3 (–4) per spikelet, 1.5 (–1.7) mm;</text>
      <biological_entity id="o27283" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="4" />
        <character char_type="range_value" constraint="per spikelet" constraintid="o27284" from="2" name="quantity" src="d0_s11" to="3" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s11" to="1.7" to_unit="mm" />
        <character name="some_measurement" notes="" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o27284" name="spikelet" name_original="spikelet" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>body pale-brown to brown, strongly flattened, obovoidorbicular, 1–1.2 × 0.8–1.1;</text>
      <biological_entity id="o27285" name="body" name_original="body" src="d0_s12" type="structure">
        <character char_type="range_value" from="pale-brown" name="coloration" src="d0_s12" to="brown" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="1.2" />
        <character char_type="range_value" from="0.8" name="quantity" src="d0_s12" to="1.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>surfaces transversely wavyrugose, intervals vertically striate with very narrowly rectangular alveolae;</text>
      <biological_entity id="o27286" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s13" value="wavyrugose" value_original="wavyrugose" />
      </biological_entity>
      <biological_entity id="o27287" name="interval" name_original="intervals" src="d0_s13" type="structure">
        <character constraint="with alveolae" constraintid="o27288" is_modifier="false" modifier="vertically" name="coloration_or_pubescence_or_relief" src="d0_s13" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity id="o27288" name="alveola" name_original="alveolae" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="very narrowly" name="shape" src="d0_s13" value="rectangular" value_original="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tubercle flat, triangular or concavely triangular, 0.3–0.5 (–0.7) mm.</text>
      <biological_entity id="o27289" name="tubercle" name_original="tubercle" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="concavely" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late spring–fall or all year (south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" modifier="south" to="fall" from="late spring" constraint=" -year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sands and peats of bogs, shorelines, interdunal swales, savannas, and pine flatwoods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sands" constraint="of bogs , shorelines , interdunal swales , savannas , and pine flatwoods" />
        <character name="habitat" value="peats" constraint="of bogs , shorelines , interdunal swales , savannas , and pine flatwoods" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="shorelines" />
        <character name="habitat" value="interdunal swales" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="pine flatwoods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>47.</number>
  <discussion>Rhynchospora elliottii is most likely to be confused in the field with its frequent associates R. microcarpa and R. perplexa. Most of the time it can be distinguished from both by its taller, coarser, broader-leaved habit and by its distinctly redder spikelets. Inspection of the fruit reveals the spreading character of the perianth bristles, these usually a length level with the tubercle tip or longer and giving the whole structure the appearance of an unengorged tick.</discussion>
  
</bio:treatment>