<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jeremy J. Bruhl,Gordon C. Tucker</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="treatment_page">199</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Oteng-Yeboah" date="1974" rank="genus">BLYSMOPSIS</taxon_name>
    <place_of_publication>
      <publication_title>Notes Roy. Bot. Gard. Edinburgh</publication_title>
      <place_in_publication>33: 309. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus BLYSMOPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Blysmus, a genus name, and Greek - opsis, likeness</other_info_on_name>
    <other_info_on_name type="fna_id">104130</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, rhizomatous.</text>
      <biological_entity id="o25605" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms terete or distally rounded-trigonous.</text>
      <biological_entity id="o25606" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s1" value="rounded-trigonous" value_original="rounded-trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal;</text>
      <biological_entity id="o25607" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules present;</text>
      <biological_entity id="o25608" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades flat.</text>
      <biological_entity id="o25609" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, spicate;</text>
      <biological_entity id="o25610" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucral-bracts several, suberect, proximal leaflike, distal scalelike;</text>
      <biological_entity id="o25611" name="involucral-bract" name_original="involucral-bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="several" value_original="several" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="position" src="d0_s6" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity constraint="distal" id="o25612" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="scale-like" value_original="scalelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikelets 2–25 per spike.</text>
      <biological_entity id="o25613" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per spike" constraintid="o25614" from="2" name="quantity" src="d0_s7" to="25" />
      </biological_entity>
      <biological_entity id="o25614" name="spike" name_original="spike" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets: scales 2–5, lateral spikelets spirally arranged, each subtending flower, terminal spikelets pseudodistichous.</text>
      <biological_entity id="o25615" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o25616" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o25617" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o25618" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity constraint="terminal" id="o25619" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="pseudodistichous" value_original="pseudodistichous" />
      </biological_entity>
      <relation from="o25617" id="r3152" name="subtending" negation="false" src="d0_s8" to="o25618" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual;</text>
      <biological_entity id="o25620" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth bristles (0–) 3–5 (–6), barbed, shorter than achene;</text>
      <biological_entity constraint="perianth" id="o25621" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s10" to="3" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="6" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="barbed" value_original="barbed" />
        <character constraint="than achene" constraintid="o25622" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25622" name="achene" name_original="achene" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 3;</text>
      <biological_entity id="o25623" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles linear, 2-fid, base persistent.</text>
      <biological_entity id="o25624" name="style" name_original="styles" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s12" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o25625" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes biconvex or planoconvex.</text>
    </statement>
    <statement id="d0_s14">
      <text>x = 20.</text>
      <biological_entity id="o25626" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" name="shape" src="d0_s13" value="planoconvex" value_original="planoconvex" />
      </biological_entity>
      <biological_entity constraint="x" id="o25627" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Boreal regions of North America, Europe, and Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Boreal regions of North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="and Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Blysmopsis</other_name>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Oteng-Yeboah, A. A. 1977. Observations on Blysmus and Blysmopsis. Notes Roy. Bot. Gard. Edinburgh 33: 399–406.</reference>
  </references>
  
</bio:treatment>