<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="treatment_page">100</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="(Nees) Bentham in G. Bentham and J. D. Hooker" date="unknown" rank="section">eleogenus</taxon_name>
    <taxon_name authority="Svenson" date="1929" rank="series">maculosae</taxon_name>
    <taxon_name authority="(Poiret) Urban" date="1903" rank="species">flavescens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">flavescens</taxon_name>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleogenus;series maculosae;species flavescens;variety flavescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357757</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">flavescens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">thermalis</taxon_name>
    <taxon_hierarchy>genus Eleocharis;species flavescens;variety thermalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">thermalis</taxon_name>
    <taxon_hierarchy>genus Eleocharis;species thermalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms to 42 cm.</text>
      <biological_entity id="o1996" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="42" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Floral scales brown to stramineous.</text>
      <biological_entity constraint="floral" id="o1997" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s1" to="stramineous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: perianth bristles vestigial to slightly exceeding achene, rarely longer.</text>
      <biological_entity id="o1998" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity constraint="perianth" id="o1999" name="bristle" name_original="bristles" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" modifier="rarely" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o2000" name="achene" name_original="achene" src="d0_s2" type="structure" />
      <relation from="o1999" id="r248" modifier="slightly" name="exceeding" negation="false" src="d0_s2" to="o2000" />
    </statement>
    <statement id="d0_s3">
      <text>Achenes redbrown to dark-brown when ripe, 0.4–0.8 (–1.1) × 0.3–0.6 mm, apex rarely highly constricted proximal to tubercle.</text>
      <biological_entity id="o2001" name="achene" name_original="achenes" src="d0_s3" type="structure">
        <character char_type="range_value" from="redbrown" modifier="when ripe" name="coloration" src="d0_s3" to="dark-brown" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s3" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2002" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="rarely highly" name="size" src="d0_s3" value="constricted" value_original="constricted" />
        <character constraint="to tubercle" constraintid="o2003" is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o2003" name="tubercle" name_original="tubercle" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Tubercles 0.2–0.5 × 0.2–0.4 mm. 2n = 30.</text>
      <biological_entity id="o2004" name="tubercle" name_original="tubercles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s4" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2005" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting mid spring–winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="winter" from="mid spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brackish creeks, canal banks, dune depressions, hammocks, hot springs, irrigation ditches, maritime mud flats, salt marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brackish creeks" />
        <character name="habitat" value="canal banks" />
        <character name="habitat" value="dune depressions" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="hot springs" />
        <character name="habitat" value="irrigation ditches" />
        <character name="habitat" value="maritime mud flats" />
        <character name="habitat" value="salt marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300(–1600) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Ark., Calif., Fla., Ga., Idaho, La., Miss., Mont., N.C., S.C., Tex., Utah, Va., Wyo.; West Indies; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39a.</number>
  <other_name type="common_name">Pale spikerush</other_name>
  
</bio:treatment>