<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Theodore S. Cochrane,Robert F. C. Naczi</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="(O. Lang) Mackenzie in N. L. Britton et al." date="1935" rank="section">Granulares</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>18: 260. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Granulares</taxon_hierarchy>
    <other_info_on_name type="fna_id">302698</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="O. Lang" date="unknown" rank="unranked">Granulares</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>24: 582. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carex;unranked Granulares;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose or not, short to long rhizomatous, sometimes inconspicuously rhizomatous.</text>
      <biological_entity id="o3442" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" src="d0_s0" value="not" value_original="not" />
        <character is_modifier="false" name="size_or_length" src="d0_s0" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="sometimes inconspicuously" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms sometimes solitary, brown at base.</text>
      <biological_entity id="o3443" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character constraint="at base" constraintid="o3444" is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o3444" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths not fibrous;</text>
      <biological_entity id="o3445" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o3446" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous;</text>
      <biological_entity id="o3447" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o3448" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades M-shaped in cross-section when young, adaxial side of blade with 2 lateral-veins more prominent than midvein, widest leaves not more than 10 mm wide, glabrous.</text>
      <biological_entity id="o3449" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3450" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o3451" is_modifier="false" name="shape" src="d0_s4" value="m--shaped" value_original="m--shaped" />
      </biological_entity>
      <biological_entity id="o3451" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o3452" name="side" name_original="side" src="d0_s4" type="structure">
        <character constraint="than midvein" constraintid="o3455" is_modifier="false" name="prominence" src="d0_s4" value="more prominent" value_original="more prominent" />
      </biological_entity>
      <biological_entity id="o3453" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o3454" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
        <character constraint="than midvein" constraintid="o3455" is_modifier="false" name="prominence" src="d0_s4" value="more prominent" value_original="more prominent" />
      </biological_entity>
      <biological_entity id="o3455" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity constraint="widest" id="o3456" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o3452" id="r412" modifier="when young" name="part_of" negation="false" src="d0_s4" to="o3453" />
      <relation from="o3452" id="r413" name="with" negation="false" src="d0_s4" to="o3454" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, with (2–) 3–6 spikes;</text>
      <biological_entity id="o3457" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o3458" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <relation from="o3457" id="r414" name="with" negation="false" src="d0_s5" to="o3458" />
    </statement>
    <statement id="d0_s6">
      <text>proximal nonbasal bracts leaflike, long-sheathing, sheath more than 4 mm, longer than diameter of stem;</text>
      <biological_entity constraint="proximal nonbasal" id="o3459" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="long-sheathing" value_original="long-sheathing" />
      </biological_entity>
      <biological_entity id="o3460" name="sheath" name_original="sheath" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" upper_restricted="false" />
        <character constraint="than diameter of stem" constraintid="o3461" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o3461" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral spikes pistillate or androgynous, rarely distal 1–3 spikes staminate, sometimes some basal, pedunculate, prophyllate;</text>
      <biological_entity constraint="lateral" id="o3462" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="androgynous" value_original="androgynous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o3463" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="rarely" name="position_or_shape" src="d0_s7" value="distal" value_original="distal" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o3464" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" notes="" src="d0_s7" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="prophyllate" value_original="prophyllate" />
      </biological_entity>
      <biological_entity constraint="basal lateral" id="o3465" name="spike" name_original="spikes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>terminal spike staminate.</text>
      <biological_entity constraint="terminal" id="o3466" name="spike" name_original="spike" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Proximal pistillate scales with apex acute, acuminate, or short-awned.</text>
      <biological_entity constraint="proximal" id="o3467" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-awned" value_original="short-awned" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-awned" value_original="short-awned" />
      </biological_entity>
      <biological_entity id="o3468" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o3467" id="r415" name="with" negation="false" src="d0_s9" to="o3468" />
    </statement>
    <statement id="d0_s10">
      <text>Perigynia ascending to spreading, minute redbrown or yellowish streaks and dots, distinctly veined, sessile, ellipsoid or rhomboid to broadly ovoid, obovoid, or subglobose, obscurely trigonous to round in cross-section, base rounded, apex rounded, abruptly beaked, glabrous;</text>
      <biological_entity id="o3469" name="perigynium" name_original="perigynia" src="d0_s10" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="minute" value_original="minute" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o3470" name="streak" name_original="streaks" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s10" value="dots" value_original="dots" />
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s10" value="veined" value_original="veined" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="rhomboid" name="shape" src="d0_s10" to="broadly ovoid obovoid or subglobose obscurely trigonous" />
        <character char_type="range_value" from="rhomboid" name="shape" src="d0_s10" to="broadly ovoid obovoid or subglobose obscurely trigonous" />
        <character char_type="range_value" from="rhomboid" name="shape" src="d0_s10" to="broadly ovoid obovoid or subglobose obscurely trigonous" />
        <character char_type="range_value" constraint="in cross-section" constraintid="o3471" from="rhomboid" name="shape" src="d0_s10" to="broadly ovoid obovoid or subglobose obscurely trigonous" />
      </biological_entity>
      <biological_entity id="o3471" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o3472" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o3473" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abruptly" name="architecture_or_shape" src="d0_s10" value="beaked" value_original="beaked" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>beak 0.1–0.9 mm, orifice entire to minutely bidentulate, teeth 0–0.3 mm.</text>
      <biological_entity id="o3474" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3475" name="orifice" name_original="orifice" src="d0_s11" type="structure">
        <character char_type="range_value" from="entire" name="architecture" src="d0_s11" to="minutely bidentulate" />
      </biological_entity>
      <biological_entity id="o3476" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Stigmas 3.</text>
      <biological_entity id="o3477" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes trigonous, smaller than bodies of perigynia;</text>
      <biological_entity id="o3478" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="trigonous" value_original="trigonous" />
        <character constraint="than bodies" constraintid="o3479" is_modifier="false" name="size" src="d0_s13" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o3479" name="body" name_original="bodies" src="d0_s13" type="structure" />
      <biological_entity id="o3480" name="perigynium" name_original="perigynia" src="d0_s13" type="structure" />
      <relation from="o3479" id="r416" name="part_of" negation="false" src="d0_s13" to="o3480" />
    </statement>
    <statement id="d0_s14">
      <text>style deciduous.</text>
      <biological_entity id="o3481" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate and subtropical regions of North America, s Mexico, Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate and subtropical regions of North America" establishment_means="native" />
        <character name="distribution" value="s Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26cc.</number>
  <discussion>Species 6 (4 in the flora).</discussion>
  <discussion>Carex sect. Granulares is a monophyletic section diagnosed by at least three apormorphies: red-brown or yellowish cells scattered in the epidermis of the perigynia, often 25 or more perigynia per well-developed lateral spike, and perigynia loosely enveloping the achenes (R. F. C. Naczi 1992, 1997). Recent phylogenetic analyses indicate section Granulares is the sister group of a clade composed of sections Careyanae and Griseae (R. F. C. Naczi 1992).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants with short or inconspicuous rhizomes; culms in tufts; terminal spike and distal lateral spike usually overlapping; proximal spikes usually arising from distal 1/2 of culms.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants with long-creeping rhizomes; culms mostly solitary; terminal spike and distal lateral spike (unless staminate) usually separated; proximal spikes usually arising from proximal 1/2 of culms.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves green, not glaucous; longest bract blade (per plant) of distal lateral spike 1.6–4.6(–7.1) cm; ligule of proximal bract 0.5–6.5 mm; perigynia (1.6–)1.9–3 times as long as thick.</description>
      <determination>274 Carex gholsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves not green, usually glaucous; longest bract blade (per plant) of distal lateral spike 4.1–15.8 cm; ligule of proximal bract (2–)3–17.5(–26) mm; perigynia 1.4–2.2(–2.4) times as long as thick.</description>
      <determination>275 Carex granularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Staminate scales with apex acute to awned; widest leaves 2.8–8.3 mm wide; perigynium beak 0.3–0.9 mm.</description>
      <determination>276 Carex microdonta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Staminate scales with apex rounded to obtuse; widest leaves 1.8–3(–4.4) mm wide; perigynium beak 0.1–0.3 mm.</description>
      <determination>277 Carex crawei</determination>
    </key_statement>
  </key>
</bio:treatment>