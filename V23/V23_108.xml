<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="mention_page">81</other_info_on_meta>
    <other_info_on_meta type="treatment_page">80</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="series">eleocharis</taxon_name>
    <taxon_name authority="Britton" date="1889" rank="species">parishii</taxon_name>
    <place_of_publication>
      <publication_title>J. New York Microscop. Soc.</publication_title>
      <place_in_publication>5: 110. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleocharis;series eleocharis;species parishii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357777</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="Parish" date="unknown" rank="species">disciformis</taxon_name>
    <taxon_hierarchy>genus Eleocharis;species disciformis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">montevidensis</taxon_name>
    <taxon_name authority="(Parish) V. E. Grant" date="unknown" rank="variety">disciformis</taxon_name>
    <taxon_hierarchy>genus Eleocharis;species montevidensis;variety disciformis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">montevidensis</taxon_name>
    <taxon_name authority="(Britton) V. E. Grant" date="unknown" rank="variety">parishii</taxon_name>
    <taxon_hierarchy>genus Eleocharis;species montevidensis;variety parishii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, mat-forming;</text>
      <biological_entity id="o9129" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes evident, long, 0.5–1 (–2) mm thick, firm, cortex persistent, longer internodes 5–30 mm, scales often fugaceous, 5–10 mm, membranous, not fibrous.</text>
      <biological_entity id="o9130" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="evident" value_original="evident" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s1" to="1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o9131" name="cortex" name_original="cortex" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="longer" id="o9132" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9133" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="duration" src="d0_s1" value="fugaceous" value_original="fugaceous" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms terete or cross-section elliptic (or rectangular), usually with to 8 blunt ridges when dry, 10–50 cm × 0.2–0.7 (–1) mm, soft to firm, spongy.</text>
      <biological_entity id="o9134" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o9135" name="cross-section" name_original="cross-section" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o9136" name="ridge" name_original="ridges" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s2" to="8" />
        <character is_modifier="true" name="shape" src="d0_s2" value="blunt" value_original="blunt" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s2" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="soft" modifier="when dry" name="texture" src="d0_s2" to="firm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="spongy" value_original="spongy" />
      </biological_entity>
      <relation from="o9135" id="r1099" modifier="usually" name="with" negation="false" src="d0_s2" to="o9136" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: distal leaf-sheaths persistent, not splitting, mostly proximally dark red, distally red to brown or green, thinly papery to thickly membranous, apex usually redbrown, subtruncate to obtuse or subacute, callose, tooth often present on most or all culms, to 1 mm.</text>
      <biological_entity id="o9137" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o9138" name="sheath" name_original="leaf-sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s3" value="splitting" value_original="splitting" />
        <character is_modifier="false" modifier="mostly proximally" name="coloration" src="d0_s3" value="dark red" value_original="dark red" />
        <character char_type="range_value" from="distally red" name="coloration" src="d0_s3" to="brown or green" />
        <character char_type="range_value" from="thinly papery" name="texture" src="d0_s3" to="thickly membranous" />
      </biological_entity>
      <biological_entity id="o9139" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="subtruncate" name="shape" src="d0_s3" to="obtuse or subacute" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="callose" value_original="callose" />
      </biological_entity>
      <biological_entity id="o9140" name="tooth" name_original="tooth" src="d0_s3" type="structure">
        <character constraint="on culms" constraintid="o9141" is_modifier="false" modifier="often" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9141" name="culm" name_original="culms" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Spikelets narrowly lanceoloid (to narrowly cylindric), 3–20 × 1.5–2.5 mm, apex acute;</text>
      <biological_entity id="o9142" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9143" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal scale amplexicaulous, entire;</text>
      <biological_entity constraint="proximal" id="o9144" name="scale" name_original="scale" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="amplexicaulous" value_original="amplexicaulous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>subproximal scale empty or with flower;</text>
      <biological_entity constraint="subproximal" id="o9145" name="scale" name_original="scale" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="empty" value_original="empty" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="with flower" value_original="with flower" />
      </biological_entity>
      <biological_entity id="o9146" name="flower" name_original="flower" src="d0_s6" type="structure" />
      <relation from="o9145" id="r1100" name="with" negation="false" src="d0_s6" to="o9146" />
    </statement>
    <statement id="d0_s7">
      <text>floral scales appressed or spreading in fruit, 15–40, 3–4 per mm of rachilla, orange brown, midrib regions often greenish, ovate, 2–3 × 1 mm, apex entire, rounded to obtuse in proximal part of spikelet, acute in distal part, carinate, at least in distal part of spikelet.</text>
      <biological_entity constraint="floral" id="o9147" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character constraint="in fruit" constraintid="o9148" is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="orange brown" value_original="orange brown" />
      </biological_entity>
      <biological_entity id="o9148" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s7" to="40" />
        <character char_type="range_value" constraint="per mm" constraintid="o9149" from="3" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o9149" name="mm" name_original="mm" src="d0_s7" type="structure" />
      <biological_entity id="o9150" name="rachillum" name_original="rachilla" src="d0_s7" type="structure" />
      <biological_entity constraint="midrib" id="o9151" name="region" name_original="regions" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="3" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9152" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character char_type="range_value" constraint="in proximal part" constraintid="o9153" from="rounded" name="shape" src="d0_s7" to="obtuse" />
        <character constraint="in distal part" constraintid="o9155" is_modifier="false" name="shape" notes="" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="carinate" value_original="carinate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9153" name="part" name_original="part" src="d0_s7" type="structure" />
      <biological_entity id="o9154" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o9155" name="part" name_original="part" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o9156" name="part" name_original="part" src="d0_s7" type="structure" />
      <biological_entity id="o9157" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <relation from="o9149" id="r1101" name="part_of" negation="false" src="d0_s7" to="o9150" />
      <relation from="o9153" id="r1102" name="part_of" negation="false" src="d0_s7" to="o9154" />
      <relation from="o9152" id="r1103" modifier="at-least" name="in" negation="false" src="d0_s7" to="o9156" />
      <relation from="o9156" id="r1104" name="part_of" negation="false" src="d0_s7" to="o9157" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth bristles 3–7, rarely apparently absent, stramineous to whitish, slender to stout, often unequal, rudimentary to slightly exceeding tubercle;</text>
      <biological_entity id="o9158" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="perianth" id="o9159" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="7" />
        <character is_modifier="false" modifier="rarely apparently" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s8" to="whitish" />
        <character char_type="range_value" from="slender" name="size" src="d0_s8" to="stout" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o9160" name="tubercle" name_original="tubercle" src="d0_s8" type="structure" />
      <relation from="o9159" id="r1105" name="slightly exceeding" negation="false" src="d0_s8" to="o9160" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 3;</text>
      <biological_entity id="o9161" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9162" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers yellow to stramineous, 1.1–2 mm;</text>
      <biological_entity id="o9163" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9164" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="stramineous" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3-fid, 0.3–0.4 × 0.2–0.3 mm.</text>
      <biological_entity id="o9165" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9166" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="3-fid" value_original="3-fid" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s11" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes falling with scales, yellowbrown or dark-brown, ellipsoid or obovoid to obpyriform, compressedtrigonous, angles evident to obscure, rarely prominent, 0.8–1.4 × 0.5–0.7 mm, neck absent to long, smooth or sometimes minutely cancellate at 20–30X.</text>
      <biological_entity id="o9167" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character constraint="with scales" constraintid="o9168" is_modifier="false" name="life_cycle" src="d0_s12" value="falling" value_original="falling" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s12" to="obpyriform" />
      </biological_entity>
      <biological_entity id="o9168" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o9169" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character char_type="range_value" from="evident" name="prominence" src="d0_s12" to="obscure" />
        <character is_modifier="false" modifier="rarely" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s12" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9170" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="long" value_original="long" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character constraint="at 20-30x" is_modifier="false" modifier="sometimes minutely" name="architecture" src="d0_s12" value="cancellate" value_original="cancellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Tubercles whitish, pyramidal, often higher than wide, 0.3–0.4 × 0.25–0.35 mm. 2n = 10.</text>
      <biological_entity id="o9171" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="shape" src="d0_s13" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="position" src="d0_s13" value="often higher than wide" value_original="often higher than wide" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s13" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="width" src="d0_s13" to="0.35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9172" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh to brackish, wet soil or emergent, often drying lakeshores, ponds, streams, springs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" />
        <character name="habitat" value="emergent" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="fresh to brackish" />
        <character name="habitat" value="drying lakeshores" modifier="often" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Kans., Nev., N.Mex., Oreg., Tex., Utah; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Eleocharis parishii is very similar to E. montevidensis, with which it sometimes grows. It differs from E. montevidensis mainly in its narrowly lanceoloid to cylindric spikelets and its floral scale apices not recurved or horizontally wrinkled. Some apparent intermediates occur in regions of sympatry. The floral scales of E. montevidensis are more densely placed on the rachilla.</discussion>
  
</bio:treatment>