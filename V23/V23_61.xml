<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">38</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="mention_page">40</other_info_on_meta>
    <other_info_on_meta type="mention_page">41</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="treatment_page">43</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="(Ascherson) Palla in W. D. J. Koch et al." date="1905" rank="genus">bolboschoenus</taxon_name>
    <taxon_name authority="(Lamarck) S. G. Smith" date="1995" rank="species">glaucus</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>5: 101. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus bolboschoenus;species glaucus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101033</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">glaucus</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck and J. Poiret, Tabl. Encycl.</publication_title>
      <place_in_publication>1: 142. 1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species glaucus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms to 150 cm × 1.5–3 mm.</text>
      <biological_entity id="o26848" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: sheaths reaching ± to middle of culm, fronts truncate to convex, membranous at mouth, veins diverging proximal to apex leaving acutely triangular veinless area;</text>
      <biological_entity id="o26849" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o26850" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="position_relational" src="d0_s1" value="reaching" value_original="reaching" />
        <character constraint="to apex" constraintid="o26851" is_modifier="false" name="position" src="d0_s1" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o26851" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity id="o26852" name="culm" name_original="culm" src="d0_s1" type="structure" />
      <biological_entity id="o26853" name="front" name_original="fronts" src="d0_s1" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s1" to="convex" />
        <character constraint="at mouth" constraintid="o26854" is_modifier="false" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o26854" name="mouth" name_original="mouth" src="d0_s1" type="structure" />
      <biological_entity id="o26855" name="vein" name_original="veins" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="diverging" value_original="diverging" />
        <character constraint="to apex" constraintid="o26856" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o26856" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity id="o26857" name="area" name_original="area" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="acutely" name="shape" src="d0_s1" value="triangular" value_original="triangular" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="veinless" value_original="veinless" />
      </biological_entity>
      <relation from="o26851" id="r3329" name="part_of" negation="false" src="d0_s1" to="o26852" />
      <relation from="o26856" id="r3330" name="leaving" negation="false" src="d0_s1" to="o26857" />
    </statement>
    <statement id="d0_s2">
      <text>widest blade 2–6 mm wide.</text>
      <biological_entity id="o26858" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="widest" id="o26859" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences subumbellate, all or most spikelets solitary or in clusters of 2–7 on 3–11 rays, rays not exceeding 9 cm;</text>
      <biological_entity id="o26860" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="subumbellate" value_original="subumbellate" />
      </biological_entity>
      <biological_entity id="o26861" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character constraint="in clusters; of" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="in clusters" value_original="in clusters" />
        <character char_type="range_value" constraint="on rays" constraintid="o26862" from="2" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity id="o26862" name="ray" name_original="rays" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="11" />
      </biological_entity>
      <biological_entity id="o26863" name="ray" name_original="rays" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>involucral-bracts that surpass inflorescence 1–3, widest bract 2–3 mm wide.</text>
      <biological_entity id="o26864" name="involucral-bract" name_original="involucral-bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o26865" name="inflorescence" name_original="inflorescence" src="d0_s4" type="structure" />
      <biological_entity constraint="widest" id="o26866" name="bract" name_original="bract" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o26864" id="r3331" name="surpass" negation="false" src="d0_s4" to="o26865" />
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 3–30, ovoid to narrowly lanceoloid or cylindric, 10–40 × 3–5 mm;</text>
      <biological_entity id="o26867" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="30" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s5" to="narrowly lanceoloid or cylindric" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scales loosely imbricate, orangebrown to stramineous, 5–6 × 2.5 mm, membranous, transparent, apex 2-fid 0.5–1 mm deep, awns slender, 1–2 × 0.25 mm at base.</text>
      <biological_entity id="o26868" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="orangebrown" name="coloration" src="d0_s6" to="stramineous" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="2.5" value_original="2.5" />
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="transparent" value_original="transparent" />
      </biological_entity>
      <biological_entity id="o26869" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="2-fid" value_original="2-fid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s6" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o26870" name="awn" name_original="awns" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="2" to_unit="mm" />
        <character constraint="at base" constraintid="o26871" name="width" src="d0_s6" unit="mm" value="0.25" value_original="0.25" />
      </biological_entity>
      <biological_entity id="o26871" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth bristles mostly tightly attached to shed achene, medium brown, much shorter than to equaling achene;</text>
      <biological_entity id="o26872" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="perianth" id="o26873" name="bristle" name_original="bristles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly tightly" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character is_modifier="false" name="size" src="d0_s7" value="medium" value_original="medium" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character constraint="than to equaling achene" constraintid="o26875" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o26874" name="achene" name_original="achene" src="d0_s7" type="structure" />
      <biological_entity id="o26875" name="achene" name_original="achene" src="d0_s7" type="structure">
        <character is_modifier="true" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o26873" id="r3332" name="shed" negation="false" src="d0_s7" to="o26874" />
    </statement>
    <statement id="d0_s8">
      <text>anthers yellow, 3 mm;</text>
      <biological_entity id="o26876" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26877" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles (2–) 3-fid.</text>
      <biological_entity id="o26878" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o26879" name="style" name_original="styles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="(2-)3-fid" value_original="(2-)3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Achenes pale-brown to blackish brown, cuneate-obovate, clearly to obscurely trigonous or some thickly biconvex, angles rounded, 2.5–3.3 × 1.3–2.3 mm, apex rounded, beak minute, surface glossy, exocarp cells not evident at 20X, in cross-section 1/3 of mesocarp thickness, cells isodiametric;</text>
      <biological_entity id="o26880" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="pale-brown" name="coloration" src="d0_s10" to="blackish brown" />
        <character char_type="range_value" from="cuneate-obovate" name="shape" src="d0_s10" to="clearly obscurely trigonous" />
        <character char_type="range_value" from="cuneate-obovate" name="shape" src="d0_s10" to="clearly obscurely trigonous" />
        <character name="shape" src="d0_s10" value="some" value_original="some" />
        <character is_modifier="false" modifier="thickly" name="shape" src="d0_s10" value="biconvex" value_original="biconvex" />
      </biological_entity>
      <biological_entity id="o26881" name="angle" name_original="angles" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s10" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s10" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26882" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o26883" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o26884" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s10" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity constraint="exocarp" id="o26885" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character constraint="at cross-section" constraintid="o26886" is_modifier="false" modifier="not" name="prominence" src="d0_s10" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o26886" name="cross-section" name_original="cross-section" src="d0_s10" type="structure">
        <character constraint="of mesocarp" constraintid="o26887" name="quantity" src="d0_s10" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o26887" name="mesocarp" name_original="mesocarp" src="d0_s10" type="structure" />
      <biological_entity id="o26888" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>achene specific gravity greater than water.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 110 (Czech Republic).</text>
      <biological_entity id="o26889" name="achene" name_original="achene" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="greater than water" value_original="greater than water" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26890" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="110" value_original="110" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh to brackish shores and marshes, rice fields, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shores" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="rice fields" />
        <character name="habitat" value="fresh to brackish shores" />
        <character name="habitat" value="waste" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Idaho, N.Y., Oreg.; Eurasia; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>The earliest known collections of Bolboschoenus glaucus in North America, one a rice-field weed in Colusa County, California, and the other from Long Island, New York, are both dated 1923. The reports (as Scirpus tuberosus Desfontaines) from Quebec and Alabama are based on misidentifications.</discussion>
  <discussion>Bolboschoenus glaucus was confused with B. maritimus worldwide until recently (J. Browning et al. 1995, 1997; S. G. Smith 1995). The name Scirpus tuberosus Desfontaines (= S. maritimus Linnaeus var. tuberosus (Desfontaines) Roemer &amp; Schultes) was misapplied to B. glaucus by A. H. Beetle (1942) and T. Koyama (1962b). Putative B. glaucus × B. maritimus hybrids are locally common at Lake Lowell near Nampa, Canyon County, Idaho, and in California, where B. glaucus and putative hybrids are common weeds of rice fields and are cultivated with B. maritimus as food for waterfowl (J. Browning et al. 1995). Some specimens of B. maritimus from Long Island, New York, where B. glaucus has also been collected, suggest introgression from B. glaucus.</discussion>
  
</bio:treatment>