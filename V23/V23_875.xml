<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Peter W. Ball,Joy Mastrogiuseppe</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="section">Aulocystis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Belg.,</publication_title>
      <place_in_publication>147. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Aulocystis</taxon_hierarchy>
    <other_info_on_name type="fna_id">302677</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="(Tuckerman) Kükenthal" date="unknown" rank="section">Ferrugineae</taxon_name>
    <taxon_hierarchy>genus Carex;section Ferrugineae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, short to long rhizomatous, sometimes stoloniferous.</text>
      <biological_entity id="o12431" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="size_or_length" src="d0_s0" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms brown at base, shorter than leaves.</text>
      <biological_entity id="o12432" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o12433" is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character constraint="than leaves" constraintid="o12434" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s1" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o12433" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o12434" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths not fibrous;</text>
      <biological_entity id="o12435" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o12436" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous;</text>
      <biological_entity id="o12437" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o12438" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades V-shaped in cross-section when young, proximal leaf-blades with lateral-veins equally prominent, distal leaf-blades at least 2 mm, glabrous.</text>
      <biological_entity id="o12439" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12440" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o12441" is_modifier="false" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <biological_entity id="o12441" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o12442" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure" />
      <biological_entity id="o12443" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="equally" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12444" name="blade-leaf" name_original="leaf-blades" src="d0_s4" type="structure">
        <character name="distance" src="d0_s4" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o12442" id="r1536" modifier="when young" name="with" negation="false" src="d0_s4" to="o12443" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, with 2–8+ spikes;</text>
      <biological_entity id="o12445" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o12446" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="8" upper_restricted="false" />
      </biological_entity>
      <relation from="o12445" id="r1537" name="with" negation="false" src="d0_s5" to="o12446" />
    </statement>
    <statement id="d0_s6">
      <text>proximal nonbasal bracts filiform or scalelike, sheathing at least 3 mm, longer than diameter of stem;</text>
      <biological_entity constraint="proximal nonbasal" id="o12447" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s6" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" modifier="at least" name="architecture_or_shape" src="d0_s6" value="sheathing" value_original="sheathing" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="3" value_original="3" />
        <character constraint="than diameter of stem" constraintid="o12448" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o12448" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral spikes pistillate, androgynous, or, rarely, staminate, pedunculate, prophyllate, pistillate scales ovoid, with not more than 25 perigynia;</text>
      <biological_entity constraint="lateral" id="o12449" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="androgynous" value_original="androgynous" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="prophyllate" value_original="prophyllate" />
      </biological_entity>
      <biological_entity id="o12450" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o12451" name="perigynium" name_original="perigynia" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" modifier="not" name="quantity" src="d0_s7" upper_restricted="false" />
      </biological_entity>
      <relation from="o12450" id="r1538" name="with" negation="false" src="d0_s7" to="o12451" />
    </statement>
    <statement id="d0_s8">
      <text>terminal spike staminate, androgynous, or gynecandrous.</text>
      <biological_entity constraint="terminal" id="o12452" name="spike" name_original="spike" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="androgynous" value_original="androgynous" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="gynecandrous" value_original="gynecandrous" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="androgynous" value_original="androgynous" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="gynecandrous" value_original="gynecandrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Proximal pistillate scales dark-brown to almost black, 1–3-veined, apex obtuse to acuminate.</text>
      <biological_entity constraint="proximal" id="o12453" name="spike" name_original="spikes" src="d0_s9" type="structure" />
      <biological_entity id="o12454" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark-brown to almost" value_original="dark-brown to almost" />
        <character is_modifier="false" modifier="almost" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o12455" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Perigynia ascending or spreading, purplish black at least distally, or black-mottled, veinless or weakly veined, sometimes with 2, strong, marginal veins, stipitate, lanceolate to ovate, biconvex or trigonous in cross-section, less than 10 mm, base tapered to rounded, apex tapering or abruptly beaked, glabrous, setose, or hispidulous;</text>
      <biological_entity id="o12456" name="perigynium" name_original="perigynia" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="at-least distally; distally" name="coloration" src="d0_s10" value="purplish black" value_original="purplish black" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="black-mottled" value_original="black-mottled" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="veinless" value_original="veinless" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s10" value="veined" value_original="veined" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="ovate biconvex or trigonous" />
        <character char_type="range_value" constraint="in cross-section" constraintid="o12458" from="lanceolate" name="shape" src="d0_s10" to="ovate biconvex or trigonous" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o12457" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="fragility" src="d0_s10" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o12458" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o12459" name="base" name_original="base" src="d0_s10" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s10" to="rounded" />
      </biological_entity>
      <biological_entity id="o12460" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s10" value="beaked" value_original="beaked" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <relation from="o12456" id="r1539" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o12457" />
    </statement>
    <statement id="d0_s11">
      <text>beak straight, ± bidentate.</text>
      <biological_entity id="o12461" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="bidentate" value_original="bidentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Stigmas (2–) 3.</text>
      <biological_entity id="o12462" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s12" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes trigonous, rarely biconvex, smaller than bodies of perigynia;</text>
      <biological_entity id="o12463" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s13" value="biconvex" value_original="biconvex" />
        <character constraint="than bodies" constraintid="o12464" is_modifier="false" name="size" src="d0_s13" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o12464" name="body" name_original="bodies" src="d0_s13" type="structure" />
      <biological_entity id="o12465" name="perigynium" name_original="perigynia" src="d0_s13" type="structure" />
      <relation from="o12464" id="r1540" name="part_of" negation="false" src="d0_s13" to="o12465" />
    </statement>
    <statement id="d0_s14">
      <text>style deciduous.</text>
      <biological_entity id="o12466" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate and artic regions of North America and Eurasia, primarily in temperate Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate and artic regions of North America and Eurasia" establishment_means="native" />
        <character name="distribution" value="primarily in temperate Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26hh.</number>
  <discussion>Species probably over 50 (8 in the flora).</discussion>
  <references>
    <reference>Ball, P. W. and M. Zoladz. 1994. The taxonomy of Carex petricosa (Cyperaceae) and related species in North America. Rhodora 96: 295–310.</reference>
    <reference>Dietrich, W. 1967. Die Cytotaxonomie der Carex-Sektion Frigidae in Europa. Feddes Repert. 75: 1–40.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perigynia minutely papillose; pistillate scales minutely papillose at least at center.</description>
      <determination>336 Carex atrofusca</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perigynia and pistillate scales not papillose.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Proximal bract sheaths expanded distally, (1.8–)2–3 mm wide at mouth, usually with wide purple band at mouth.</description>
      <determination>334 Carex luzulifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Proximal bract sheaths cylindric or slightly expanded distally, less than 2 mm wide at mouth, without or with narrow purple band at mouth.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Terminal spike gynecandrous.</description>
      <determination>330 Carex fuliginosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Terminal spike staminate or androgynous.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distance from achene apex to perigynium beak tip less than 1.5 mm; exserted portion of peduncle of proximal spike usually less than 1 cm.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distance from achene apex to perigynium beak tip 1.5+ mm; exserted portion of peduncle of proximal spike usually more than 1 cm, sometimes shorter.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Widest leaf blades 3–6 mm wide; inflorescences more than 15 cm.</description>
      <determination>331 Carex albida</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Widest leaf blades 1.5–3(–4) mm wide; inflorescences usually less than 15 cm.</description>
      <determination>333 Carex lemmonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perigynia glabrous on faces.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perigynia setose or hispidulous on faces, sometimes sparsely.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Pistillate scales 3–5.8 mm; perigynia tapering to indistinct beak to 0.5 (–1) mm, faces distinctly 3–9-veined.</description>
      <determination>329 Carex petricosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Pistillate scales 1.9–3.5 mm; perigynia usually abruptly beaked to 0.5– 1.5 mm, faces indistinctly veined or distinctly 9–20-veined.</description>
      <determination>335 Carex luzulina</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Perigynia without flat margins around achene, faces distinctly 3–9-veined, apex tapering to indistinct beak to 0.5(–1) mm; leaf blades 1–3.5 mm wide.</description>
      <determination>329 Carex petricosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Perigynia with narrow flat margins around achene, faces indistinctly veined, apex tapering to abrupt beak to 0.5–1.5 mm; leaf blades 3–8 mm wide.</description>
      <determination>332 Carex fissuricola</determination>
    </key_statement>
  </key>
</bio:treatment>