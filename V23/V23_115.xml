<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="treatment_page">84</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="series">eleocharis</taxon_name>
    <taxon_name authority="Sullivant" date="1842" rank="species">compressa</taxon_name>
    <taxon_name authority="(Buckley) S. G. Smith" date="2001" rank="variety">acutisquamata</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>11: 249. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleocharis;series eleocharis;species compressa;variety acutisquamata;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357745</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="Buckley" date="unknown" rank="species">acutisquamata</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>14: 10. 1863</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eleocharis;species acutisquamata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms subterete to slightly compressed, to 2 times as wide as thick, (0.2–) 0.5–1 mm wide.</text>
      <biological_entity id="o4315" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="subterete" name="shape" src="d0_s0" to="slightly compressed" />
        <character is_modifier="false" name="width_or_width" src="d0_s0" value="0-2 times as wide as thick" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s0" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Distal leaf-sheaths often with tooth, to 0.1 (–0.5) mm.</text>
      <biological_entity constraint="distal" id="o4316" name="sheath" name_original="leaf-sheaths" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.1" from_inclusive="false" from_unit="mm" name="atypical_distance" notes="" src="d0_s1" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="distance" notes="" src="d0_s1" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4317" name="tooth" name_original="tooth" src="d0_s1" type="structure" />
      <relation from="o4316" id="r515" name="with" negation="false" src="d0_s1" to="o4317" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally wet seeps and depressions in calcareous grasslands, meadows, open oak woods, ditches, sometimes limestone, rarely saline marsh or lakeshores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet seeps" modifier="seasonally" />
        <character name="habitat" value="depressions" constraint="in calcareous grasslands , meadows , open oak woods , ditches , sometimes limestone ," />
        <character name="habitat" value="calcareous grasslands" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="open oak woods" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="marsh" modifier="limestone rarely saline" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="saline" modifier="rarely" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Man., Sask.; Colo., Ill., Iowa, Kans., Minn., Mo., Nebr., N.Mex., N.Dak., Okla., S.Dak., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17b.</number>
  <discussion>Some specimens herein included in E. compressa var. acutisquamata are intermediate with E. elliptica in their floral scales, including the single record from British Columbia. Eleocharis compressa var. acutisquamata probably occurs in Alberta; I have not seen specimens.</discussion>
  
</bio:treatment>