<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="(Reichenbach) Palla" date="1888" rank="genus">schoenoplectus</taxon_name>
    <taxon_name authority="(Reichenbach) J. Raynal" date="1976" rank="section">Actaeogeton</taxon_name>
    <place_of_publication>
      <publication_title>Adansonia, n.s.</publication_title>
      <place_in_publication>16: 130. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus schoenoplectus;section Actaeogeton</taxon_hierarchy>
    <other_info_on_name type="fna_id">303155</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Reichenbach" date="unknown" rank="section">Actaegeton</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Germ. Excurs.,</publication_title>
      <place_in_publication>78. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;section Actaegeton;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or rarely perennial, cespitose;</text>
      <biological_entity id="o28931" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes inconspicuous, very short, firm [perennials, rhizomes long].</text>
      <biological_entity id="o28932" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="texture" src="d0_s1" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 0.5–3 mm diam.</text>
      <biological_entity id="o28933" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves all basal.</text>
      <biological_entity id="o28934" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o28935" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences capitate or of 1 spikelet [sometimes branched].</text>
      <biological_entity id="o28936" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="of 1 spikelet" value_original="of 1 spikelet" />
      </biological_entity>
      <biological_entity id="o28937" name="spikelet" name_original="spikelet" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <relation from="o28936" id="r3563" name="consist_of" negation="false" src="d0_s4" to="o28937" />
    </statement>
    <statement id="d0_s5">
      <text>Spikelets: scales each with 3–11 distinct parallel ribs in proximal part of spikelet, apex entire, mostly mucronate.</text>
      <biological_entity id="o28938" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity id="o28939" name="scale" name_original="scales" src="d0_s5" type="structure" />
      <biological_entity id="o28940" name="rib" name_original="ribs" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="11" />
        <character is_modifier="true" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character is_modifier="true" name="arrangement" src="d0_s5" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o28941" name="part" name_original="part" src="d0_s5" type="structure" />
      <biological_entity id="o28942" name="spikelet" name_original="spikelet" src="d0_s5" type="structure" />
      <biological_entity id="o28943" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <relation from="o28939" id="r3564" name="with" negation="false" src="d0_s5" to="o28940" />
      <relation from="o28940" id="r3565" name="in" negation="false" src="d0_s5" to="o28941" />
      <relation from="o28941" id="r3566" name="part_of" negation="false" src="d0_s5" to="o28942" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth present or absent, segments bristlelike, spinulose;</text>
      <biological_entity id="o28944" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o28945" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o28946" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="bristlelike" value_original="bristlelike" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="spinulose" value_original="spinulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>amphicarpic flowers absent.</text>
      <biological_entity id="o28947" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o28948" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="amphicarpic" value_original="amphicarpic" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes plane or biconvex, sometimes trigonous, rugulose, without prominent, transverse, wavy ridges.</text>
      <biological_entity id="o28949" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s8" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="relief" src="d0_s8" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity id="o28950" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s8" value="transverse" value_original="transverse" />
        <character is_modifier="true" name="shape" src="d0_s8" value="wavy" value_original="wavy" />
      </biological_entity>
      <relation from="o28949" id="r3567" name="without" negation="false" src="d0_s8" to="o28950" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide, mostly in eastern Asia, the Pacific Islands, and e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
        <character name="distribution" value="mostly in eastern Asia" establishment_means="native" />
        <character name="distribution" value="the Pacific Islands" establishment_means="native" />
        <character name="distribution" value="and e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7c.</number>
  <discussion>Species ca. 23 (3 in the flora).</discussion>
  
</bio:treatment>