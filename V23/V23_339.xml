<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="treatment_page">194</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Rottbøll" date="1773" rank="genus">kyllinga</taxon_name>
    <taxon_name authority="Thonning ex Vahl" date="1805" rank="species">squamulata</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>2: 381. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus kyllinga;species squamulata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200026877</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="(Hochstetter ex Steudel) Mattfeld &amp; Kükenthal" date="unknown" rank="species">metzii</taxon_name>
    <taxon_hierarchy>genus Cyperus;species metzii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, cespitose, not rhizomatous.</text>
      <biological_entity id="o19596" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 5–35 cm, smooth.</text>
      <biological_entity id="o19597" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="35" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves flat, 5–30 cm × 1.5–2 mm.</text>
      <biological_entity id="o19598" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: spike 1, subglobose, 6–10 mm diam.;</text>
      <biological_entity id="o19599" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o19600" name="spike" name_original="spike" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts 3–4, horizontal to slightly reflexed, flat, 1–6 cm × 1–2 mm.</text>
      <biological_entity id="o19601" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o19602" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="4" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s4" to="slightly reflexed" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 50–70 (–100), stipitate, ovate, 2.5–4 × 2–3 mm;</text>
      <biological_entity id="o19603" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="70" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="100" />
        <character char_type="range_value" from="50" name="quantity" src="d0_s5" to="70" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral scales dull greenish white to stramineous, midvein with laciniate keel ± as long as scale, 1 mm wide, laterally 2-veined, ovate, 2.2–3.8 × 2 mm, acuminate;</text>
      <biological_entity constraint="floral" id="o19604" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character char_type="range_value" from="greenish white" name="coloration" src="d0_s6" to="stramineous" />
      </biological_entity>
      <biological_entity id="o19605" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="laterally" name="architecture" src="d0_s6" value="2-veined" value_original="2-veined" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s6" to="3.8" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o19606" name="keel" name_original="keel" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity id="o19607" name="scale" name_original="scale" src="d0_s6" type="structure">
        <character name="width" notes="" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <relation from="o19605" id="r2360" name="with" negation="false" src="d0_s6" to="o19606" />
      <relation from="o19606" id="r2361" name="as long as" negation="false" src="d0_s6" to="o19607" />
    </statement>
    <statement id="d0_s7">
      <text>stamens (1–) 2;</text>
      <biological_entity id="o19608" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s7" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 2 mm;</text>
      <biological_entity id="o19609" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 1 mm;</text>
      <biological_entity id="o19610" name="style" name_original="style" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 1.5–2 mm.</text>
      <biological_entity id="o19611" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Achenes chestnut-brown to purplish brown, ovoid, 1.5–2 × 1.2 mm, base sessile, apex apiculate, puncticulate.</text>
      <biological_entity id="o19612" name="achene" name_original="achenes" src="d0_s11" type="structure">
        <character char_type="range_value" from="chestnut-brown" name="coloration" src="d0_s11" to="purplish brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s11" to="2" to_unit="mm" />
        <character name="width" src="d0_s11" unit="mm" value="1.2" value_original="1.2" />
      </biological_entity>
      <biological_entity id="o19613" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o19614" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="coloration_or_relief" src="d0_s11" value="puncticulate" value_original="puncticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lawns, farmlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lawns" />
        <character name="habitat" value="farmlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; West Indies; native tropical Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="native tropical Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Crested greenhead sedge</other_name>
  
</bio:treatment>