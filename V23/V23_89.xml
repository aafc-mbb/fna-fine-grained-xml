<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">47</other_info_on_meta>
    <other_info_on_meta type="mention_page">55</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="treatment_page">59</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="(Reichenbach) Palla" date="1888" rank="genus">schoenoplectus</taxon_name>
    <taxon_name authority="(Chermezon) J. Raynal" date="1976" rank="section">supini</taxon_name>
    <taxon_name authority="(A. Gray) S. G. Smith" date="1995" rank="species">hallii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>5: 101. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus schoenoplectus;section supini;species hallii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357938</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">hallii</taxon_name>
    <place_of_publication>
      <publication_title>Manual ed.</publication_title>
      <place_in_publication>3, 97. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species hallii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">supinus</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="variety">hallii</taxon_name>
    <taxon_hierarchy>genus Scirpus;species supinus;variety hallii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
      <biological_entity id="o12467" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 1 mm diam.</text>
      <biological_entity id="o12468" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character name="diameter" src="d0_s1" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms often arching to decumbent, cylindric, ridged when dry, 0.05–0.8 m × 0.5–1 mm.</text>
      <biological_entity id="o12469" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="arching" name="orientation" src="d0_s2" to="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s2" value="ridged" value_original="ridged" />
        <character char_type="range_value" from="0.05" from_unit="m" name="length" src="d0_s2" to="0.8" to_unit="m" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 3–4, 1 cauline;</text>
      <biological_entity id="o12470" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="4" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o12471" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>sheaths straw-colored to brown and disintegrating to fibers, front translucent and splitting;</text>
      <biological_entity id="o12472" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character char_type="range_value" from="straw-colored" name="coloration" src="d0_s4" to="brown" />
        <character constraint="to fibers" constraintid="o12473" is_modifier="false" name="dehiscence" src="d0_s4" value="disintegrating" value_original="disintegrating" />
      </biological_entity>
      <biological_entity id="o12473" name="fiber" name_original="fibers" src="d0_s4" type="structure" />
      <biological_entity id="o12474" name="front" name_original="front" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s4" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s4" value="splitting" value_original="splitting" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1–2, proximally thickly C-shaped in cross-section, distally flat, from much shorter to longer than sheath, 1–200+ × 0.2–1 mm, smooth or margins distally spinulose.</text>
      <biological_entity id="o12475" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
        <character constraint="in cross-section" constraintid="o12476" is_modifier="false" modifier="proximally thickly" name="shape" src="d0_s5" value="c--shaped" value_original="c--shaped" />
        <character is_modifier="false" modifier="distally" name="prominence_or_shape" notes="" src="d0_s5" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" notes="" src="d0_s5" to="200" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" notes="" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12476" name="cross-section" name_original="cross-section" src="d0_s5" type="structure" />
      <biological_entity id="o12478" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="much" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="size_or_length" src="d0_s5" value="shorter to longer" value_original="shorter to longer" />
      </biological_entity>
      <biological_entity id="o12477" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="much" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="size_or_length" src="d0_s5" value="shorter to longer" value_original="shorter to longer" />
      </biological_entity>
      <biological_entity id="o12479" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s5" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <relation from="o12475" id="r1541" name="from" negation="false" src="d0_s5" to="o12478" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences capitate or 1 spikelet or rarely with 1 or 2 branches to 12 mm;</text>
      <biological_entity id="o12480" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="capitate" value_original="capitate" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12481" name="spikelet" name_original="spikelet" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" modifier="with 1 or 2branches" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal bract erect, resembling leaf-blades, 5–150 mm.</text>
      <biological_entity constraint="proximal" id="o12482" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="150" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12483" name="leaf-blade" name_original="leaf-blades" src="d0_s7" type="structure" />
      <relation from="o12482" id="r1542" name="resembling" negation="false" src="d0_s7" to="o12483" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 1–5, 5–20 × 2–3 mm;</text>
      <biological_entity id="o12484" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="5" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>scales straw-colored to pale orangebrown to nearly colorless, midrib region usually greenish or pale, ovate, 2.5–3 (–5) × 1.5 mm, smooth or awn sparsely spinulose, margins ciliolate, midrib keeled distally, apex entire, rounded, awn 0.2–0.3 mm or to 3 mm on proximal scale.</text>
      <biological_entity id="o12485" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character char_type="range_value" from="straw-colored" name="coloration" src="d0_s9" to="pale orangebrown" />
      </biological_entity>
      <biological_entity constraint="midrib" id="o12486" name="region" name_original="region" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale" value_original="pale" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12487" name="awn" name_original="awn" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s9" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity id="o12488" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o12489" name="midrib" name_original="midrib" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o12490" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o12491" name="awn" name_original="awn" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.3" to_unit="mm" />
        <character name="some_measurement" src="d0_s9" value="0-3 mm" value_original="0-3 mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o12492" name="scale" name_original="scale" src="d0_s9" type="structure" />
      <relation from="o12491" id="r1543" name="on" negation="false" src="d0_s9" to="o12492" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: perianth absent;</text>
      <biological_entity id="o12493" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12494" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 0.4 mm;</text>
      <biological_entity id="o12495" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12496" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 2-fid.</text>
      <biological_entity id="o12497" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o12498" name="style" name_original="styles" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spikelet achenes blackish brown, planoconvex or usually the adaxial surface longitudinally concave, ovoid to obovoid, 1.3–1.7 × 1.2–1.3 mm, with 15–18 mostly sharp ridges;</text>
      <biological_entity constraint="spikelet" id="o12499" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="blackish brown" value_original="blackish brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="planoconvex" value_original="planoconvex" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12500" name="surface" name_original="surface" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="longitudinally" name="shape" src="d0_s13" value="concave" value_original="concave" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s13" to="obovoid" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s13" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s13" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12501" name="ridge" name_original="ridges" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s13" to="18" />
        <character is_modifier="true" modifier="mostly" name="shape" src="d0_s13" value="sharp" value_original="sharp" />
      </biological_entity>
      <relation from="o12500" id="r1544" name="with" negation="false" src="d0_s13" to="o12501" />
    </statement>
    <statement id="d0_s14">
      <text>beak 0.1 mm.</text>
      <biological_entity id="o12502" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Amphicarpic achenes thickly trigonous with convex abaxial face, 1.7–2.5 × 1.1–1.6 mm, rugose with rounded ridges;</text>
      <biological_entity id="o12503" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="amphicarpic" value_original="amphicarpic" />
        <character constraint="with abaxial face" constraintid="o12504" is_modifier="false" modifier="thickly" name="shape" src="d0_s15" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" notes="" src="d0_s15" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" notes="" src="d0_s15" to="1.6" to_unit="mm" />
        <character constraint="with ridges" constraintid="o12505" is_modifier="false" name="relief" src="d0_s15" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12504" name="face" name_original="face" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o12505" name="ridge" name_original="ridges" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>beak 0.3 mm. 2n = 22.</text>
      <biological_entity id="o12506" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12507" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial to emergent, freshwater shores, temporary ponds, wet places in cultivated fields, pastures, ditches, sinkholes, prairie</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="terrestrial to emergent" />
        <character name="habitat" value="freshwater shores" />
        <character name="habitat" value="temporary ponds" />
        <character name="habitat" value="wet places" constraint="in cultivated fields , pastures , ditches , sinkholes , prairie" />
        <character name="habitat" value="cultivated fields" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="sinkholes" />
        <character name="habitat" value="prairie" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>70–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="70" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., Ill., Ind., Kans., Ky., Mass., Mich., Mo., Nebr., Okla., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Hall’s bulrush</other_name>
  <discussion>Many reports of Schoenoplectus hallii are based on misidentified specimens of S. saximontanus or S. erectus. The Iowa specimen was probably collected in 1890. The species is probably extirpated from Massachusetts. I have identified a specimen from the Georgia coastal plain, where S. hallii and S. erectus are sympatric, as intermediate between the two.</discussion>
  
</bio:treatment>