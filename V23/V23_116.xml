<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">86</other_info_on_meta>
    <other_info_on_meta type="treatment_page">85</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="series">eleocharis</taxon_name>
    <taxon_name authority="S. G. Smith" date="2001" rank="species">bifida</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>11: 243, figs. 1F–J, 2. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleocharis;series eleocharis;species bifida;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242357737</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, densely cespitose;</text>
      <biological_entity id="o22234" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes concealed by persistent culm bases, short, 4–5 mm thick, hard, cortex persistent, internodes crowded, scales decaying to coarse fibers, 1 cm, papery.</text>
      <biological_entity id="o22235" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character constraint="by culm bases" constraintid="o22236" is_modifier="false" name="prominence" src="d0_s1" value="concealed" value_original="concealed" />
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s1" value="short" value_original="short" />
        <character char_type="range_value" from="4" from_unit="mm" name="thickness" src="d0_s1" to="5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity constraint="culm" id="o22236" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o22237" name="cortex" name_original="cortex" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o22238" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o22239" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character name="some_measurement" src="d0_s1" unit="cm" value="1" value_original="1" />
        <character is_modifier="false" name="texture" src="d0_s1" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o22240" name="fiber" name_original="fibers" src="d0_s1" type="structure">
        <character is_modifier="true" name="relief" src="d0_s1" value="coarse" value_original="coarse" />
      </biological_entity>
      <relation from="o22239" id="r2693" name="decaying to" negation="false" src="d0_s1" to="o22240" />
    </statement>
    <statement id="d0_s2">
      <text>Culms greatly compressed, 4–10 times wider than thick, often with 1 or 2 sharp ridges on 1 side, (8–) 20–35 cm × 0.7–2.3 mm, hard, finely striate, spongy.</text>
      <biological_entity id="o22241" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="greatly" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="width_or_width" src="d0_s2" value="4-10 times wider than thick" />
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_length" notes="" src="d0_s2" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" notes="" src="d0_s2" to="35" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" notes="" src="d0_s2" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="finely" name="coloration_or_pubescence_or_relief" src="d0_s2" value="striate" value_original="striate" />
        <character is_modifier="false" name="texture" src="d0_s2" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o22242" name="ridge" name_original="ridges" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o22243" name="side" name_original="side" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <relation from="o22241" id="r2694" modifier="often" name="with" negation="false" src="d0_s2" to="o22242" />
      <relation from="o22242" id="r2695" name="on" negation="false" src="d0_s2" to="o22243" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: distal leaf-sheaths persistent, not splitting, proximally red or stramineous, distally green to stramineous, inflated, papery, apex dark-brown, broadly obtuse to subtruncate, callose, tooth absent.</text>
      <biological_entity id="o22244" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o22245" name="sheath" name_original="leaf-sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s3" value="splitting" value_original="splitting" />
        <character char_type="range_value" from="stramineous distally green" name="coloration" src="d0_s3" to="stramineous" />
        <character char_type="range_value" from="stramineous distally green" name="coloration" src="d0_s3" to="stramineous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="texture" src="d0_s3" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o22246" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="broadly obtuse" name="shape" src="d0_s3" to="subtruncate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="callose" value_original="callose" />
      </biological_entity>
      <biological_entity id="o22247" name="tooth" name_original="tooth" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets ovoid, 4–9 × 2.5–4 mm, apex acute;</text>
      <biological_entity id="o22248" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22249" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal scale clasping 2/3–3/4 of culm, apex 2-fid;</text>
      <biological_entity constraint="proximal" id="o22250" name="scale" name_original="scale" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character char_type="range_value" constraint="of culm" constraintid="o22251" from="2/3" name="quantity" src="d0_s5" to="3/4" />
      </biological_entity>
      <biological_entity id="o22251" name="culm" name_original="culm" src="d0_s5" type="structure" />
      <biological_entity id="o22252" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>subproximal scale empty or with a flower;</text>
      <biological_entity constraint="subproximal" id="o22253" name="scale" name_original="scale" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="empty" value_original="empty" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="with a flower" value_original="with a flower" />
      </biological_entity>
      <biological_entity id="o22254" name="flower" name_original="flower" src="d0_s6" type="structure" />
      <relation from="o22253" id="r2696" name="with" negation="false" src="d0_s6" to="o22254" />
    </statement>
    <statement id="d0_s7">
      <text>floral scales spreading in fruit, 30–60, 6–9 per mm of rachilla, medium or pale-brown, midrib region often paler, ovatelanceolate, 2.5–3.5 × 1.5 mm, apex 2-fid, carinate in distal part of spikelet.</text>
      <biological_entity constraint="floral" id="o22255" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o22256" is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="medium" value_original="medium" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
      <biological_entity id="o22256" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s7" to="60" />
        <character char_type="range_value" constraint="per mm" constraintid="o22257" from="6" name="quantity" src="d0_s7" to="9" />
      </biological_entity>
      <biological_entity id="o22257" name="mm" name_original="mm" src="d0_s7" type="structure" />
      <biological_entity id="o22258" name="rachillum" name_original="rachilla" src="d0_s7" type="structure" />
      <biological_entity constraint="midrib" id="o22259" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="paler" value_original="paler" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s7" to="3.5" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o22260" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character constraint="in distal part" constraintid="o22261" is_modifier="false" name="shape" src="d0_s7" value="carinate" value_original="carinate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o22261" name="part" name_original="part" src="d0_s7" type="structure" />
      <biological_entity id="o22262" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <relation from="o22257" id="r2697" name="part_of" negation="false" src="d0_s7" to="o22258" />
      <relation from="o22261" id="r2698" name="part_of" negation="false" src="d0_s7" to="o22262" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth bristles 0 (–5), stramineous to pale-brown, 1/2 of to equaling achene length;</text>
      <biological_entity id="o22263" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="perianth" id="o22264" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s8" to="pale-brown" />
        <character name="length" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 3;</text>
      <biological_entity id="o22265" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o22266" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers orangebrown, 0.7–1.5 mm;</text>
      <biological_entity id="o22267" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o22268" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="orangebrown" value_original="orangebrown" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3-fid.</text>
      <biological_entity id="o22269" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o22270" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes falling with scales, yellowbrown to medium brown, obovoid to obpyriform, nearly equilaterally trigonous, cross-section circular to slightly compressed, angles obscure or evident, 0.9–1.1 × 0.6–0.75, neck usually very short, finely rugulose at 10–30X, 20 or more low, blunt horizontal ridges in vertical series.</text>
      <biological_entity id="o22271" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character constraint="with scales" constraintid="o22272" is_modifier="false" name="life_cycle" src="d0_s12" value="falling" value_original="falling" />
        <character char_type="range_value" from="yellowbrown" name="coloration" notes="" src="d0_s12" to="medium brown" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s12" to="obpyriform" />
        <character is_modifier="false" modifier="nearly equilaterally" name="shape" src="d0_s12" value="trigonous" value_original="trigonous" />
      </biological_entity>
      <biological_entity id="o22272" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o22273" name="cross-section" name_original="cross-section" src="d0_s12" type="structure">
        <character char_type="range_value" from="circular" name="shape" src="d0_s12" to="slightly compressed" />
      </biological_entity>
      <biological_entity id="o22274" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="prominence" src="d0_s12" value="evident" value_original="evident" />
        <character char_type="range_value" from="0.9" name="quantity" src="d0_s12" to="1.1" />
        <character char_type="range_value" from="0.6" name="quantity" src="d0_s12" to="0.75" />
      </biological_entity>
      <biological_entity id="o22275" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually very" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character constraint="at ridges" constraintid="o22276" is_modifier="false" modifier="finely" name="relief" src="d0_s12" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity id="o22276" name="ridge" name_original="ridges" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="20" value_original="20" />
        <character is_modifier="true" name="shape" src="d0_s12" value="blunt" value_original="blunt" />
        <character is_modifier="true" name="orientation" src="d0_s12" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <biological_entity id="o22277" name="series" name_original="series" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o22276" id="r2699" name="in" negation="false" src="d0_s12" to="o22277" />
    </statement>
    <statement id="d0_s13">
      <text>Tubercles brown, depressed-pyramidal, often rudimentary, 0.1–0.25 × 0.2–0.3 mm.</text>
      <biological_entity id="o22278" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="depressed-pyramidal" value_original="depressed-pyramidal" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s13" value="rudimentary" value_original="rudimentary" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s13" to="0.25" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s13" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting spring–summer (May–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally wet places on limestone, cedar (juniper) glades, stream beds, prairies, ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet places" modifier="seasonally" constraint="on limestone , cedar ( juniper )" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="cedar" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="stream beds" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>Eleocharis bifida was previously included in E. compressa. A very few specimens of E. compressa var. compressa from the states where E. bifida is known are like E. bifida except for their entire proximal floral scales and evident rhizomes.</discussion>
  
</bio:treatment>