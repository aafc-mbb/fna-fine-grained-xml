<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce A. Ford,A. A. Reznicek</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="J. Carey" date="1847" rank="section">Squarrosae</taxon_name>
    <place_of_publication>
      <publication_title>Carices North. U.S.,</publication_title>
      <place_in_publication>564. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Squarrosae</taxon_hierarchy>
    <other_info_on_name type="fna_id">302737</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose or colonial, short to long rhizomatous.</text>
      <biological_entity id="o21707" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="size_or_length" src="d0_s0" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms brown or redbrown at base.</text>
      <biological_entity id="o21708" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character constraint="at base" constraintid="o21709" is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity id="o21709" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths not fibrous;</text>
      <biological_entity id="o21710" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o21711" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous;</text>
      <biological_entity id="o21712" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o21713" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades septate-nodulose, usually V-shaped in cross-section when young, usually glabrous.</text>
      <biological_entity id="o21714" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21715" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="septate-nodulose" value_original="septate-nodulose" />
        <character constraint="in cross-section" constraintid="o21716" is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <biological_entity id="o21716" name="cross-section" name_original="cross-section" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="when young; usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences single spike or racemose, with 2–9 spikes;</text>
      <biological_entity id="o21717" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o21718" name="spike" name_original="spike" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o21719" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <relation from="o21718" id="r2622" name="with" negation="false" src="d0_s5" to="o21719" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts leaflike or threadlike, sheathless or sheath longer than diameter of stem, at least 3 times as long as inflorescence;</text>
      <biological_entity constraint="proximal" id="o21720" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="shape" src="d0_s6" value="thread-like" value_original="threadlike" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sheathless" value_original="sheathless" />
      </biological_entity>
      <biological_entity id="o21721" name="sheath" name_original="sheath" src="d0_s6" type="structure">
        <character constraint="than diameter of stem , at-least 3 times as-long-as inflorescence" constraintid="o21722, o21723" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o21722" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
      </biological_entity>
      <biological_entity id="o21723" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral spikes pistillate or infrequently gynecandrous, larger spikes usually with more than 50 perigynia, pedunculate, prophyllate;</text>
      <biological_entity constraint="lateral" id="o21724" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="infrequently" name="reproduction" src="d0_s7" value="gynecandrous" value_original="gynecandrous" />
      </biological_entity>
      <biological_entity constraint="larger" id="o21725" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="prophyllate" value_original="prophyllate" />
      </biological_entity>
      <biological_entity id="o21726" name="perigynium" name_original="perigynia" src="d0_s7" type="structure">
        <character char_type="range_value" from="50" is_modifier="true" name="quantity" src="d0_s7" upper_restricted="false" />
      </biological_entity>
      <relation from="o21725" id="r2623" name="with" negation="false" src="d0_s7" to="o21726" />
    </statement>
    <statement id="d0_s8">
      <text>terminal spike staminate or gyncecandrous.</text>
      <biological_entity constraint="terminal" id="o21727" name="spike" name_original="spike" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character name="architecture" src="d0_s8" value="gyncecandrous" value_original="gyncecandrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Proximal pistillate scales sometimes with narrow, indistinct body, apex obtuse to awned.</text>
      <biological_entity constraint="proximal" id="o21728" name="spike" name_original="spikes" src="d0_s9" type="structure" />
      <biological_entity id="o21729" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21730" name="body" name_original="body" src="d0_s9" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="prominence" src="d0_s9" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o21731" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="awned" />
      </biological_entity>
      <relation from="o21729" id="r2624" name="with" negation="false" src="d0_s9" to="o21730" />
    </statement>
    <statement id="d0_s10">
      <text>Perigynia ascending to spreading, many-veined, sessile or shortly stipitate, inflated, obconic, round in cross-section, not more than 10 mm, base tapering or rounded-tapering, apex truncate, abruptly beaked, glabrous;</text>
      <biological_entity id="o21732" name="perigynium" name_original="perigynia" src="d0_s10" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="many-veined" value_original="many-veined" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obconic" value_original="obconic" />
        <character constraint="in cross-section" constraintid="o21733" is_modifier="false" name="shape" src="d0_s10" value="round" value_original="round" />
        <character char_type="range_value" from="10" from_unit="mm" modifier="not" name="some_measurement" notes="" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o21733" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o21734" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded-tapering" value_original="rounded-tapering" />
      </biological_entity>
      <biological_entity id="o21735" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="abruptly" name="architecture_or_shape" src="d0_s10" value="beaked" value_original="beaked" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>beak 1.2–3.8, bidentate, teeth 0.3 mm or longer.</text>
      <biological_entity id="o21736" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" name="quantity" src="d0_s11" to="3.8" />
        <character is_modifier="false" name="shape" src="d0_s11" value="bidentate" value_original="bidentate" />
      </biological_entity>
      <biological_entity id="o21737" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Stigmas 3.</text>
      <biological_entity id="o21738" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes trigonous, 1.2–3 mm, smaller than bodies of perigynia;</text>
      <biological_entity id="o21739" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character constraint="than bodies" constraintid="o21740" is_modifier="false" name="size" src="d0_s13" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o21740" name="body" name_original="bodies" src="d0_s13" type="structure" />
      <biological_entity id="o21741" name="perigynium" name_original="perigynia" src="d0_s13" type="structure" />
      <relation from="o21740" id="r2625" name="part_of" negation="false" src="d0_s13" to="o21741" />
    </statement>
    <statement id="d0_s14">
      <text>style persistent or deciduous.</text>
      <biological_entity id="o21742" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate regions of e and mid w North America and temperate South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate regions of e and mid w North America and temperate South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26uu.</number>
  <discussion>Species 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Terminal spike largely staminate; pistillate scales with long awns, exceeding perigynia bodies; achenes 1.2–2.1 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Terminal spike gynecandrous; pistillate scales with short awns or awnless, completely hidden by perigynia; achenes 2–3 mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pistillate scales with narrow, indistinct body, 0.1–0.4 mm wide; staminate scales linear, 0.3–0.8 mm wide, loosely, irregularly imbricated with tips spreading; plants cespitose, not colonial, short-rhizomatous.</description>
      <determination>403 Carex frankii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pistillate scales with wider, translucent body, 0.4–0.9(–1.1) mm wide; staminate scales lanceolate, narrowly ovate, or oblong-obovate, 0.9–1.6 mm wide, ± tightly imbricate in spike; plants not cespitose, colonial, long-rhizomatous.</description>
      <determination>404 Carex aureolensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Style persistent, sinuous; achenes 1.9–2.5 times as long as wide; spikes 1–2(–3) per stem; pistillate portion of spike ovate to oblong; perigynia widely radiating, the proximal reflexed; beak usually smooth.</description>
      <determination>405 Carex squarrosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Style deciduous, straight; achenes 1.2–1.9 times as long as wide; spikes (1–)2–4(–6) per stem; pistillate portion of spike oblong to elliptic; perigynia, including the proximal, appressed-ascending; beak often sparingly scabrous.</description>
      <determination>406 Carex typhina</determination>
    </key_statement>
  </key>
</bio:treatment>