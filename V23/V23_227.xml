<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="treatment_page">150</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="C. B. Clarke in J. D. Hooker" date="1893" rank="subgenus">pycnostachys</taxon_name>
    <taxon_name authority="Rottbøll" date="1772" rank="species">involucratus</taxon_name>
    <place_of_publication>
      <publication_title>Descr. Pl. Rar.,</publication_title>
      <place_in_publication>22. 1772</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus pycnostachys;species involucratus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357677</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, rhizomatous.</text>
      <biological_entity id="o13228" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms trigonous, 30–150 cm × 1–5 (–8) mm.</text>
      <biological_entity id="o13229" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s1" to="150" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves bladeless.</text>
      <biological_entity id="o13230" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="bladeless" value_original="bladeless" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: heads digitate, 15–30 (–36) mm diam.;</text>
      <biological_entity id="o13231" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o13232" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="digitate" value_original="digitate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s3" to="36" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rays (14–) 20–22, (2–) 5–12 (–20) cm;</text>
      <biological_entity id="o13233" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o13234" name="ray" name_original="rays" src="d0_s4" type="structure">
        <character char_type="range_value" from="14" name="atypical_quantity" src="d0_s4" to="20" to_inclusive="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s4" to="22" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>2d order rays 0.3–3 (–4) cm;</text>
      <biological_entity id="o13235" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o13236" name="ray" name_original="rays" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>3d order rays sometimes present, 0.3–2.5 cm;</text>
      <biological_entity id="o13237" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o13238" name="ray" name_original="rays" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts (4–) 18–22, ± horizontal, flat, 15–27 cm × (1.5–) 8–12 mm.</text>
      <biological_entity id="o13239" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o13240" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="18" to_inclusive="false" />
        <character char_type="range_value" from="18" name="quantity" src="d0_s7" to="22" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s7" to="27" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s7" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 8–20, ovoid to linear-lanceoloid, compressed, 5–25 × 1.5–2 mm;</text>
      <biological_entity id="o13241" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="20" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s8" to="linear-lanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>floral scales 8–28, laterally whitish or light-brown, ± hyaline, medially light-brown, laterally ribless, medially 3-ribbed, 2-keeled in proximal 30–60%, deltate-ovate, 1.6–2.4 × (1–) 1.2–1.5 (–1.7) mm, apex acute.</text>
      <biological_entity constraint="floral" id="o13242" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="28" />
        <character is_modifier="false" modifier="laterally" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="medially" name="coloration" src="d0_s9" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="laterally" name="architecture" src="d0_s9" value="ribless" value_original="ribless" />
        <character is_modifier="false" modifier="medially" name="architecture_or_shape" src="d0_s9" value="3-ribbed" value_original="3-ribbed" />
        <character constraint="in proximal floral scales" constraintid="o13243" is_modifier="false" name="shape" src="d0_s9" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" modifier="30-60%" name="shape" notes="" src="d0_s9" value="deltate-ovate" value_original="deltate-ovate" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s9" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s9" to="1.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal floral" id="o13243" name="scale" name_original="scales" src="d0_s9" type="structure" />
      <biological_entity id="o13244" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: stamens 3;</text>
      <biological_entity id="o13245" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13246" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 0.7–1 mm;</text>
      <biological_entity id="o13247" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13248" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 0.5–1 mm;</text>
      <biological_entity id="o13249" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o13250" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas 0.6–1 mm.</text>
      <biological_entity id="o13251" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o13252" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes brown, sessile or stipitate, broadly ellipsoid, 0.6–0.8 × 0.4–0.6 mm, stipe if present to 0.1 mm, apex obtuse, apiculate, surfaces puncticulate.</text>
      <biological_entity id="o13253" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s14" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13254" name="stipe" name_original="stipe" src="d0_s14" type="structure">
        <character constraint="to 0.1 mm" is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13255" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o13256" name="surface" name_original="surfaces" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s14" value="puncticulate" value_original="puncticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting early summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, disturbed soils, ditches, stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed soils" modifier="damp" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100(–800) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Fla., La., Tex.; e Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="e Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Umbrella-plant</other_name>
  <discussion>Cyperus involucratus has been collected in New York (R. S. Mitchell and G. C. Tucker 1997).</discussion>
  <discussion>Cyperus involucratus is widely cultivated as a water plant in greenhouses and outdoors in warm-temperate or tropical climates. It has long been misidentified in the flora as C. alternifolius Linnaeus, an endemic of Madagascar (G. C. Tucker 1983).</discussion>
  
</bio:treatment>