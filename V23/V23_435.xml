<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="treatment_page">244</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="P. J. Bergius" date="1765" rank="genus">scleria</taxon_name>
    <taxon_name authority="Muhlenberg ex Willdenow" date="1805" rank="species">verticillata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>4(1): 317. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus scleria;species verticillata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357991</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
      <biological_entity id="o20849" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent;</text>
      <biological_entity id="o20850" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>roots reddish, fibrous.</text>
      <biological_entity id="o20851" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms solitary or in tufts, erect, slender, (7–) 10–60 cm, glabrous.</text>
      <biological_entity id="o20852" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="in tufts" value_original="in tufts" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20853" name="tuft" name_original="tufts" src="d0_s3" type="structure" />
      <relation from="o20852" id="r2514" name="in" negation="false" src="d0_s3" to="o20853" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: sheaths green or streaked with purple, smooth or weakly ribbed, usually narrowly winged, long-villous;</text>
      <biological_entity id="o20854" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o20855" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="streaked with purple" value_original="streaked with purple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s4" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" modifier="usually narrowly" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="long-villous" value_original="long-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>contraligules minute;</text>
      <biological_entity id="o20856" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o20857" name="contraligule" name_original="contraligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades linear or filiform, plane or keeled, shorter than culms, 0.5–2 mm wide, glabrous.</text>
      <biological_entity id="o20858" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o20859" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s6" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s6" value="keeled" value_original="keeled" />
        <character constraint="than culms" constraintid="o20860" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20860" name="culm" name_original="culms" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, glomerate-spicate, 2.5–13 cm;</text>
      <biological_entity id="o20861" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="glomerate-spicate" value_original="glomerate-spicate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s7" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>glomerules 2–9, erect, brown, compact, 3–7 mm wide, each with 5–12 (–15) spikelets;</text>
      <biological_entity id="o20862" name="glomerule" name_original="glomerules" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="9" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="compact" value_original="compact" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20863" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="15" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
      <relation from="o20862" id="r2515" name="with" negation="false" src="d0_s8" to="o20863" />
    </statement>
    <statement id="d0_s9">
      <text>proximal glomerules occasionally on short, erect peduncles;</text>
      <biological_entity constraint="proximal" id="o20864" name="glomerule" name_original="glomerules" src="d0_s9" type="structure" />
      <biological_entity id="o20865" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o20864" id="r2516" name="on" negation="false" src="d0_s9" to="o20865" />
    </statement>
    <statement id="d0_s10">
      <text>rachis glabrous;</text>
      <biological_entity id="o20866" name="rachis" name_original="rachis" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracts subtending inflorescence bristlelike, minutely ciliate or glabrous, inconspicuous.</text>
      <biological_entity id="o20867" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="bristlelike" value_original="bristlelike" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s11" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o20868" name="inflorescence" name_original="inflorescence" src="d0_s11" type="structure" />
      <relation from="o20867" id="r2517" name="subtending" negation="false" src="d0_s11" to="o20868" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets bisexual, alternate, often appearing cyclic or whorled, 2–3 (–4) mm;</text>
      <biological_entity id="o20869" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s12" value="cyclic" value_original="cyclic" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="whorled" value_original="whorled" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>scales oblong-lanceolate.</text>
      <biological_entity id="o20870" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-lanceolate" value_original="oblong-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes whitish or often gray or brownish or with dark interangular markings, trigonous-globose, 1–1.5 (–1.9) mm, base trigonous, stipelike, short, narrowly constricted, and somewhat pitted or ribbed, apex distinctly mucronate, surface transversely tuberculate with quadrate ridges;</text>
      <biological_entity id="o20871" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
        <character name="coloration" src="d0_s14" value="often" value_original="often" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="with dark interangular markings" value_original="with dark interangular markings" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="trigonous-globose" value_original="trigonous-globose" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="interangular" id="o20872" name="marking" name_original="markings" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s14" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o20873" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="stipelike" value_original="stipelike" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
        <character is_modifier="false" modifier="narrowly" name="size" src="d0_s14" value="constricted" value_original="constricted" />
        <character is_modifier="false" modifier="somewhat" name="relief" src="d0_s14" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o20874" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s14" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o20875" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character constraint="with ridges" constraintid="o20876" is_modifier="false" modifier="transversely" name="relief" src="d0_s14" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o20876" name="ridge" name_original="ridges" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <relation from="o20871" id="r2518" name="with" negation="false" src="d0_s14" to="o20872" />
    </statement>
    <statement id="d0_s15">
      <text>hypogynium obsolete, represented by narrow brownish ridge at base of achene.</text>
      <biological_entity id="o20877" name="hypogynium" name_original="hypogynium" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="obsolete" value_original="obsolete" />
      </biological_entity>
      <biological_entity id="o20878" name="ridge" name_original="ridge" src="d0_s15" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s15" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o20879" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity id="o20880" name="achene" name_original="achene" src="d0_s15" type="structure" />
      <relation from="o20877" id="r2519" name="represented by" negation="false" src="d0_s15" to="o20878" />
      <relation from="o20878" id="r2520" name="at" negation="false" src="d0_s15" to="o20879" />
      <relation from="o20879" id="r2521" name="part_of" negation="false" src="d0_s15" to="o20880" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, marly, sandy, or peaty soils in marshes, bogs, savannas, moist meadows, wet pinelands, and lakeshores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" constraint="in marshes , bogs , savannas , moist meadows , wet pinelands , and lakeshores" />
        <character name="habitat" value="sandy" constraint="in marshes , bogs , savannas , moist meadows , wet pinelands , and lakeshores" />
        <character name="habitat" value="peaty soils" constraint="in marshes , bogs , savannas , moist meadows , wet pinelands , and lakeshores" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="wet pinelands" />
        <character name="habitat" value="lakeshores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Iowa, La., Md., Mich., Minn., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., Wis.; West Indies (Bahamas, Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Scleria verticillata has very close affinities with S. hirtella and S. tenella Kunth. It is relatively wide-ranging, extending along the Gulf and Atlantic coasts and inland to the Great Lakes, where it remains a very distinct species. At its southern limit in the West Indies, it tends to merge with both S. hirtella and S. tenella, producing intermediate forms and blurring its specific boundaries.</discussion>
  
</bio:treatment>