<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">117</other_info_on_meta>
    <other_info_on_meta type="mention_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="treatment_page">120</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="(Palisot de Beauvois ex T. Lestiboudois) Torrey" date="1836" rank="subgenus">limnochloa</taxon_name>
    <taxon_name authority="(Michaux) Roemer &amp; Schultes in J. J. Roemer et al." date="1817" rank="species">quadrangulata</taxon_name>
    <place_of_publication>
      <publication_title>in J. J. Roemer et al., Syst. Veg.</publication_title>
      <place_in_publication>2: 155. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus limnochloa;species quadrangulata</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357779</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">quadrangulatus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 30. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species quadrangulatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="Schultes" date="unknown" rank="species">quadrangulata</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">crassior</taxon_name>
    <taxon_hierarchy>genus Eleocharis;species quadrangulata;variety crassior;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Muhlenberg" date="unknown" rank="species">albomarginatus</taxon_name>
    <taxon_hierarchy>genus Scirpus;species albomarginatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">marginatus</taxon_name>
    <taxon_hierarchy>genus Scirpus;species marginatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o22736" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 1.5–4 mm thick, soft to hard, longer internodes 3–8 cm, scales 5–10 mm, tubers absent.</text>
      <biological_entity id="o22737" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s1" to="4" to_unit="mm" />
        <character char_type="range_value" from="soft" name="texture" src="d0_s1" to="hard" />
      </biological_entity>
      <biological_entity constraint="longer" id="o22738" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22739" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22740" name="tuber" name_original="tubers" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms acutely quadrangular, (30–) 45–105 cm × (1–) 2–5.4 mm, soft to firm, internally spongy, transverse septa incomplete;</text>
      <biological_entity id="o22741" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="acutely" name="shape" src="d0_s2" value="quadrangular" value_original="quadrangular" />
        <character char_type="range_value" from="30" from_unit="cm" name="atypical_length" src="d0_s2" to="45" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="45" from_unit="cm" name="length" src="d0_s2" to="105" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s2" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="5.4" to_unit="mm" />
        <character char_type="range_value" from="soft" name="texture" src="d0_s2" to="firm" />
        <character is_modifier="false" modifier="internally" name="texture" src="d0_s2" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o22742" name="septum" name_original="septa" src="d0_s2" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s2" value="transverse" value_original="transverse" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="incomplete" value_original="incomplete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>plants never forming filiform, flaccid culms.</text>
      <biological_entity id="o22743" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22744" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character is_modifier="true" name="texture" src="d0_s3" value="flaccid" value_original="flaccid" />
      </biological_entity>
      <relation from="o22743" id="r2771" name="forming" negation="false" src="d0_s3" to="o22744" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: distal leaf-sheaths persistent, membranous, apex narrowly acute to acuminate, sometimes prolonged into a bladelike portion to 8 cm.</text>
      <biological_entity id="o22745" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o22746" name="sheath" name_original="leaf-sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o22747" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s4" to="acuminate" />
        <character constraint="into portion" constraintid="o22748" is_modifier="false" modifier="sometimes" name="length" src="d0_s4" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o22748" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="bladelike" value_original="bladelike" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets not proliferous, (15–) 20–76 × 3–5 (–6) mm;</text>
      <biological_entity id="o22749" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s5" value="proliferous" value_original="proliferous" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s5" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="76" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachilla joints bearing obscure winglike remnants of floral scales;</text>
      <biological_entity constraint="scale" id="o22750" name="joint" name_original="joints" src="d0_s6" type="structure" constraint_original="scale rachilla; scale" />
      <biological_entity id="o22751" name="remnant" name_original="remnants" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="winglike" value_original="winglike" />
      </biological_entity>
      <biological_entity constraint="floral" id="o22752" name="scale" name_original="scales" src="d0_s6" type="structure" />
      <relation from="o22750" id="r2772" name="bearing" negation="false" src="d0_s6" to="o22751" />
      <relation from="o22750" id="r2773" name="part_of" negation="false" src="d0_s6" to="o22752" />
    </statement>
    <statement id="d0_s7">
      <text>proximal scale empty, amplexicaulous, (1–) 2.2–5.4 mm;</text>
      <biological_entity constraint="proximal" id="o22753" name="scale" name_original="scale" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="empty" value_original="empty" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="amplexicaulous" value_original="amplexicaulous" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s7" to="5.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>floral scales (28–) 45–135, 2–3 per mm of rachilla, stramineous to pale-brown, usually with pale to dark-brown submarginal band, midrib region sometimes greenish, broadly obovate to ovate, (4–) 4.5–6.2 × 2.8–5 mm, subcartilaginous, apex rounded to obtuse.</text>
      <biological_entity constraint="floral" id="o22754" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character char_type="range_value" from="28" name="atypical_quantity" src="d0_s8" to="45" to_inclusive="false" />
        <character char_type="range_value" from="45" name="quantity" src="d0_s8" to="135" />
        <character char_type="range_value" constraint="per mm" constraintid="o22755" from="2" name="quantity" src="d0_s8" to="3" />
        <character char_type="range_value" from="stramineous" name="coloration" notes="" src="d0_s8" to="pale-brown" />
      </biological_entity>
      <biological_entity id="o22755" name="mm" name_original="mm" src="d0_s8" type="structure" />
      <biological_entity id="o22756" name="rachillum" name_original="rachilla" src="d0_s8" type="structure" />
      <biological_entity constraint="submarginal" id="o22757" name="band" name_original="band" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" is_modifier="true" name="coloration" src="d0_s8" to="dark-brown" />
      </biological_entity>
      <biological_entity constraint="midrib" id="o22758" name="region" name_original="region" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s8" to="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s8" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s8" to="6.2" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="subcartilaginous" value_original="subcartilaginous" />
      </biological_entity>
      <biological_entity id="o22759" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
      <relation from="o22755" id="r2774" name="part_of" negation="false" src="d0_s8" to="o22756" />
      <relation from="o22754" id="r2775" modifier="usually" name="with" negation="false" src="d0_s8" to="o22757" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth bristles 6–7, whitish to brown, slender, often markedly unequal, shorter than achene or some equalling tubercle, sparsely retrorsely spinulose to smooth;</text>
      <biological_entity id="o22760" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="perianth" id="o22761" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="7" />
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s9" to="brown" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="often markedly" name="size" src="d0_s9" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="sparsely retrorsely spinulose" name="architecture" src="d0_s9" to="smooth" />
      </biological_entity>
      <biological_entity id="o22762" name="achene" name_original="achene" src="d0_s9" type="structure" />
      <biological_entity id="o22763" name="tubercle" name_original="tubercle" src="d0_s9" type="structure" />
      <relation from="o22761" id="r2776" name="shorter than achene or some equalling tubercle" negation="false" src="d0_s9" to="o22762" />
      <relation from="o22761" id="r2777" name="shorter than achene or some equalling tubercle" negation="false" src="d0_s9" to="o22763" />
    </statement>
    <statement id="d0_s10">
      <text>anthers stramineous to redbrown, 2.3–2.9 mm;</text>
      <biological_entity id="o22764" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o22765" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s10" to="redbrown" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s10" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3-fid, sometimes 2-fid.</text>
      <biological_entity id="o22766" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o22767" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes yellow or pale green to brown or purplish, biconvex, obovoid to obpyriform, 1.8–3 × 1.3–2 mm, almost smooth to markedly sculptured at 10–15X, each face with 19–38 rows of almost linear, transversely elongated cells, which sometimes isodiametric at achene base, apex often constricted to neck 0.3–0.4 mm wide.</text>
      <biological_entity id="o22768" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s12" to="brown or purplish" />
        <character is_modifier="false" name="shape" src="d0_s12" value="biconvex" value_original="biconvex" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s12" to="obpyriform" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" constraint="at face" constraintid="o22769" from="smooth" modifier="almost" name="relief" src="d0_s12" to="markedly sculptured" />
      </biological_entity>
      <biological_entity id="o22769" name="face" name_original="face" src="d0_s12" type="structure" />
      <biological_entity id="o22770" name="row" name_original="rows" src="d0_s12" type="structure">
        <character char_type="range_value" from="19" is_modifier="true" name="quantity" src="d0_s12" to="38" />
        <character constraint="at achene base" constraintid="o22772" is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s12" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o22771" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="almost" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="true" modifier="transversely" name="length" src="d0_s12" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity constraint="achene" id="o22772" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o22773" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character constraint="to neck" constraintid="o22774" is_modifier="false" modifier="often" name="size" src="d0_s12" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o22774" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s12" to="0.4" to_unit="mm" />
      </biological_entity>
      <relation from="o22769" id="r2778" name="with" negation="false" src="d0_s12" to="o22770" />
      <relation from="o22770" id="r2779" name="part_of" negation="false" src="d0_s12" to="o22771" />
    </statement>
    <statement id="d0_s13">
      <text>Tubercles dark-brown or whitish, deltoid to high-pyramidal or lanceoloid, 0.7–1.5 × 0.4–1 mm, often spongy.</text>
      <biological_entity id="o22775" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="deltoid" name="shape" src="d0_s13" to="high-pyramidal or lanceoloid" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s13" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="texture" src="d0_s13" value="spongy" value_original="spongy" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting early summer–winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="winter" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow water of fresh lake and pond shores, marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow water" constraint="of fresh lake and pond shores , marshes" />
        <character name="habitat" value="fresh lake" />
        <character name="habitat" value="pond shores" />
        <character name="habitat" value="marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ark., Calif., Conn., Del., Fla., Ga., Ill., Ind., Ky., La., Mass., Mich., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Oreg., Pa., S.C., Tex., Va., W.Va., Wis.; s to c Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="s to c Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>67.</number>
  <other_name type="common_name">Square-stemmed spike-rush</other_name>
  <discussion>We have not seen voucher specimens for the reports of Eleocharis quadrangulata from Kansas. Plants with greenish achenes, longer bristles, and longer anthers than the average are known from Tennessee.</discussion>
  <discussion>The tubercles of Eleocharis quadrangulata are often spongy as in E. obtusetrigona.</discussion>
  
</bio:treatment>