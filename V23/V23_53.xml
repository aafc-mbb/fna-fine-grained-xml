<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">37</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Rottböll" date="1773" rank="genus">fuirena</taxon_name>
    <taxon_name authority="(Torrey) Sprengel" date="1825" rank="species">pumila</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Veg.</publication_title>
      <place_in_publication>1: 237. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus fuirena;species pumila</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357837</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fuirena</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">squarrosa</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="variety">pumila</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Middle United States</publication_title>
      <place_in_publication>1: 68. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Fuirena;species squarrosa;variety pumila;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, cespitose, (8–) 20–60 cm.</text>
      <biological_entity id="o16790" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms erect or spreading, slender, stiff.</text>
      <biological_entity id="o16791" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: sheaths hirsute;</text>
      <biological_entity id="o16792" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16793" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>principal blades linear to lance-linear, 5–12 cm, hirsute-hispid-ciliate, surfaces strigose-hispid or glabrous.</text>
      <biological_entity id="o16794" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="principal" id="o16795" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="arrangement_or_course_or_shape" src="d0_s3" to="lance-linear" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsute-hispid-ciliate" value_original="hirsute-hispid-ciliate" />
      </biological_entity>
      <biological_entity id="o16796" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="strigose-hispid" value_original="strigose-hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences in solitary terminal clusters or several terminal clusters, less often from proximal 1–2 nodes, principal involucral-bract mostly exceeding compound or cluster.</text>
      <biological_entity id="o16797" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="terminal" id="o16798" name="cluster" name_original="clusters" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o16799" name="node" name_original="nodes" src="d0_s4" type="structure" constraint="terminal">
        <character is_modifier="true" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="several" value_original="several" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o16800" name="cluster" name_original="clusters" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o16801" name="node" name_original="nodes" src="d0_s4" type="structure" constraint="terminal">
        <character is_modifier="true" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="several" value_original="several" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity constraint="principal" id="o16802" name="involucral-bract" name_original="involucral-bract" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="position_relational" src="d0_s4" value="exceeding" value_original="exceeding" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound" value_original="compound" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
      </biological_entity>
      <relation from="o16797" id="r2045" name="in" negation="false" src="d0_s4" to="o16798" />
      <relation from="o16797" id="r2046" name="in" negation="false" src="d0_s4" to="o16799" />
      <relation from="o16799" id="r2047" name="in" negation="false" src="d0_s4" to="o16800" />
      <relation from="o16799" id="r2048" name="in" negation="false" src="d0_s4" to="o16801" />
    </statement>
    <statement id="d0_s5">
      <text>Spikelets lanceovoid to cylindric, 5–8 (–12) mm, apex acute;</text>
      <biological_entity id="o16803" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceovoid" name="shape" src="d0_s5" to="cylindric" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16804" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>fertile scales oblong to obovate, 2.5–3 mm;</text>
      <biological_entity id="o16805" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cusp excurved, nearly length of scale, scabridulous;</text>
      <biological_entity id="o16806" name="cusp" name_original="cusp" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="excurved" value_original="excurved" />
        <character is_modifier="false" name="nearly length" notes="" src="d0_s7" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o16807" name="scale" name_original="scale" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>median ribs 3, strong.</text>
      <biological_entity constraint="median" id="o16808" name="rib" name_original="ribs" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="fragility" src="d0_s8" value="strong" value_original="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth bristles extending at least to base of perianth blades, often near to tips, retrorsely barbellate;</text>
      <biological_entity id="o16809" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="perianth" id="o16810" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="architecture" notes="" src="d0_s9" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o16811" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity constraint="perianth" id="o16812" name="blade" name_original="blades" src="d0_s9" type="structure" />
      <biological_entity id="o16813" name="near-to-tip" name_original="near-to-tips" src="d0_s9" type="structure" />
      <relation from="o16810" id="r2049" name="extending" negation="false" src="d0_s9" to="o16811" />
      <relation from="o16810" id="r2050" name="part_of" negation="false" src="d0_s9" to="o16812" />
    </statement>
    <statement id="d0_s10">
      <text>perianth blades long-clawed, mostly ovate, base 3–5-ribbed, apex slenderly acuminate, incurved, awned;</text>
      <biological_entity id="o16814" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="perianth" id="o16815" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="long-clawed" value_original="long-clawed" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o16816" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="3-5-ribbed" value_original="3-5-ribbed" />
      </biological_entity>
      <biological_entity id="o16817" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slenderly" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1–3, 0.5–0.7 mm.</text>
      <biological_entity id="o16818" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o16819" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="3" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes: stipe slender;</text>
      <biological_entity id="o16820" name="achene" name_original="achenes" src="d0_s12" type="structure" />
      <biological_entity id="o16821" name="stipe" name_original="stipe" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>body angles wirelike, faces lustrous, deep brown to redbrown, 1 mm;</text>
      <biological_entity id="o16822" name="achene" name_original="achenes" src="d0_s13" type="structure" />
      <biological_entity constraint="body" id="o16823" name="angle" name_original="angles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="wirelike" value_original="wirelike" />
      </biological_entity>
      <biological_entity id="o16824" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s13" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="deep" value_original="deep" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s13" to="redbrown" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak slender, stiff, tip papillate.</text>
      <biological_entity id="o16825" name="achene" name_original="achenes" src="d0_s14" type="structure" />
      <biological_entity id="o16826" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="slender" value_original="slender" />
        <character is_modifier="false" name="fragility" src="d0_s14" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 46.</text>
      <biological_entity id="o16827" name="tip" name_original="tip" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16828" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet pond shores, seeps, savannas and swales, moist sandy waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet pond shores" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="moist sandy waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Del., Fla., Ga., Ind., La., Md., Mass., Mich., N.J., N.Y., N.C., R.I., S.C., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  
</bio:treatment>