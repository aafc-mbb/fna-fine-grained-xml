<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="T. V. Egorova" date="1981" rank="subgenus">Zinserlingia</taxon_name>
    <place_of_publication>
      <publication_title>Novosti Sist. Vyssh. Rast.</publication_title>
      <place_in_publication>18: 101. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus Zinserlingia</taxon_hierarchy>
    <other_info_on_name type="fna_id">313009</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="(Gray) S. González &amp; P. M. Peterson" date="unknown" rank="section">Baeothryon</taxon_name>
    <taxon_hierarchy>genus Eleocharis;section Baeothryon;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Gray" date="unknown" rank="section">Baeothyron</taxon_name>
    <taxon_hierarchy>genus Scirpus;section Baeothyron;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, never with spikelets proliferating or culms rooting at tips.</text>
      <biological_entity id="o21283" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21284" name="spikelet" name_original="spikelets" src="d0_s0" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s0" value="proliferating" value_original="proliferating" />
      </biological_entity>
      <biological_entity id="o21285" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character constraint="at tips" constraintid="o21286" is_modifier="false" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o21286" name="tip" name_original="tips" src="d0_s0" type="structure" />
      <relation from="o21283" id="r2566" modifier="never" name="with" negation="false" src="d0_s0" to="o21284" />
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes present, creeping.</text>
      <biological_entity id="o21287" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bulbs (resting buds) often present, terminating short rhizomes or among culm bases.</text>
      <biological_entity id="o21288" name="bulb" name_original="bulbs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21289" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="culm" id="o21290" name="base" name_original="bases" src="d0_s2" type="structure" />
      <relation from="o21288" id="r2567" name="terminating" negation="false" src="d0_s2" to="o21289" />
      <relation from="o21288" id="r2568" name="among" negation="false" src="d0_s2" to="o21290" />
    </statement>
    <statement id="d0_s3">
      <text>Culms 5–40 cm × 0.3–1.2 (–2.5) mm, spongy, transverse septa incomplete.</text>
      <biological_entity id="o21291" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="40" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o21292" name="septum" name_original="septa" src="d0_s3" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s3" value="transverse" value_original="transverse" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="incomplete" value_original="incomplete" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets ovoid or oblong, terete, usually much wider than their culms, 3–10 mm;</text>
      <biological_entity id="o21293" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="terete" value_original="terete" />
        <character constraint="than their culms" constraintid="o21294" is_modifier="false" name="width" src="d0_s4" value="usually much wider" value_original="usually much wider" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21294" name="culm" name_original="culms" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>rachilla with several proximal internodes shorter and thicker than internodes in middle of spikelet;</text>
      <biological_entity id="o21295" name="rachillum" name_original="rachilla" src="d0_s5" type="structure">
        <character constraint="than internodes" constraintid="o21297" is_modifier="false" name="width" src="d0_s5" value="shorter and thicker" value_original="shorter and thicker" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21296" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="several" value_original="several" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o21297" name="internode" name_original="internodes" src="d0_s5" type="structure" />
      <biological_entity constraint="middle" id="o21298" name="spikelet" name_original="spikelet" src="d0_s5" type="structure" />
      <biological_entity id="o21299" name="spikelet" name_original="spikelet" src="d0_s5" type="structure" />
      <relation from="o21295" id="r2569" name="with" negation="false" src="d0_s5" to="o21296" />
      <relation from="o21297" id="r2570" name="in" negation="false" src="d0_s5" to="o21298" />
      <relation from="o21298" id="r2571" name="part_of" negation="false" src="d0_s5" to="o21299" />
    </statement>
    <statement id="d0_s6">
      <text>proximal scale subtending a flower or empty;</text>
      <biological_entity constraint="proximal" id="o21300" name="scale" name_original="scale" src="d0_s6" type="structure" />
      <biological_entity id="o21301" name="flower" name_original="flower" src="d0_s6" type="structure">
        <character is_modifier="true" name="position" src="d0_s6" value="subtending" value_original="subtending" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="empty" value_original="empty" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral scales ca. 4–12 per spikelet, spiraled, 2.5–6 mm, with 1 vein, membranous.</text>
      <biological_entity constraint="floral" id="o21302" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per spikelet" constraintid="o21303" from="4" name="quantity" src="d0_s7" to="12" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s7" value="spiraled" value_original="spiraled" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" name="texture" notes="" src="d0_s7" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o21303" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <biological_entity id="o21304" name="vein" name_original="vein" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <relation from="o21302" id="r2572" name="with" negation="false" src="d0_s7" to="o21304" />
    </statement>
    <statement id="d0_s8">
      <text>Styles 3-fid, or 3-fid and 2-fid.</text>
      <biological_entity id="o21305" name="style" name_original="styles" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes trigonous, or trigonous and biconvex, usually distally narrowed into a distinct beak, 1.5–2.7 mm, smooth or finely longitudinally ridged, the outer anticlinal epidermal-cell walls often forming a fine whitish reticulum.</text>
      <biological_entity id="o21306" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="biconvex" value_original="biconvex" />
        <character constraint="into beak" constraintid="o21307" is_modifier="false" modifier="usually distally" name="shape" src="d0_s9" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="finely longitudinally" name="shape" src="d0_s9" value="ridged" value_original="ridged" />
      </biological_entity>
      <biological_entity id="o21307" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="epidermal-cell" id="o21308" name="wall" name_original="walls" src="d0_s9" type="structure" constraint_original="outer epidermal-cell">
        <character is_modifier="true" name="orientation" src="d0_s9" value="anticlinal" value_original="anticlinal" />
      </biological_entity>
      <biological_entity id="o21309" name="reticulum" name_original="reticulum" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="fine" value_original="fine" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
      </biological_entity>
      <relation from="o21308" id="r2573" name="forming a" negation="false" src="d0_s9" to="o21309" />
    </statement>
    <statement id="d0_s10">
      <text>Tubercles often merging with achene summit in form, texture, and color, not dorsoventrally greatly compressed.</text>
      <biological_entity id="o21310" name="tubercle" name_original="tubercles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not dorsoventrally greatly" name="coloration" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="achene" id="o21311" name="summit" name_original="summit" src="d0_s10" type="structure" />
      <relation from="o21310" id="r2574" name="merging with" negation="false" src="d0_s10" to="o21311" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate and boreal North America, South America (Argentina, Chile), Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate and boreal North America" establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8c.</number>
  <discussion>Species ca. 8 (4 in the flora).</discussion>
  <discussion>The species of Eleocharis subg. Zinserlingia appear to be restricted to specialized, mostly groundwater-fed, often calcareous fens and other habitats. The achene beak is often interpreted as the tubercle because the tubercle often merges with and is difficult to distinguish from the achene apex. On the basis of similar form and color of the achene apex and tubercle, H. K. Svenson (1957) grouped in ser. Pauciflorae (Beauverd) Svenson all of the species that are here segregated into 8c. Eleocharis subg. Zinserlingia, 8a1a. E. ser. Rostellatae, and 8a3. E. sect. Parvulae, following M. S. González-E. and P. M. Peterson (1997).</discussion>
  <discussion>All North American plants of Eleocharis subg. Zinserlingia were placed in E. pauciflora (Lightfoot) Link by H. K. Svenson (1957). Pending a much-needed worldwide revision, the treatment herein is based on the partial revision for North America by S. G. Smith (2001).</discussion>
  <discussion>Vegetative reproduction in species of Eleocharis subg. Zinserlingia should be studied. The rhizomes are rather short and weak, and they are not evident on most specimens. The structures herein called bulbs are present on only about ten percent of North American and European herbarium specimens I have seen; bulbs are present on most collections from some populations in the Southwest. The bulbs were called “resting buds” by S.-O. Strandhede and R. M. T. Dahlgren (1968), who described and illustrated them for E. quinqueflora in Scandinavia. Bulbs or resting buds are not mentioned in major Eurasian floras (S. M. Walters 1980; I. D. Zinserling 1935), and it is not clear that they are produced by all Eurasian populations that are currently treated as E. quinqueflora. Bulbs vary greatly in size and shape, and they are tunicated with papery scales. Because the bases of some culm tufts of specimens of E. quinqueflora are slightly to greatly swollen (bulbous) and are tunicated by papery, two-veined, fibrous scales (prophylls) like those on the bulbs (but often longer), it seems likely that these bulbous-based culm tufts have developed from bulbs. With the exception of some high-elevation populations of E. quinqueflora in California, specimens with bulbs or bulbous culm-tuft bases do not have the well developed, hard caudices that are characteristic of the North American E. suksdorfiana, E. bernardina, and E. torticulmis, in all of which the terminal buds of the rhizomes are enlarged and are not known to form resting bulbs. Although bulbs are present on only a small percentage of herbarium specimens, most specimens of E. quinqueflora in North America can be identified using the presence of bulbous culm-tuft bases, the absence of well-developed, hard caudices, and/or spikelets with a flower in the axil of the proximal scale.</discussion>
  <key>
    <key_head>Key to the species of Eleocharis subg. Zinserlingia</key_head>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Caudices absent, or if present then not more than 0.5 mm thick, soft, very rarely hard, proximal scale of all or some spikelets with a flower, seldom empty in all spikelets; culms 0.2–0.5(–0.9) mm wide; culm tufts often proximally bulbous; resting buds (bulbs) often present on rhizomes or among culm bases.</description>
      <determination>57 Eleocharis quinqueflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Caudices present, 1–2(–3) mm thick, hard; proximal scale of all spikelets usually empty; culms 0.5–1.2(–2.5) mm wide; culm tufts not proximally bulbous; resting buds (bulbs) on rhizomes absent (non-resting buds often present).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Culms greatly compressed, 1.5–2.5 mm wide, 3–4 times wider than thick, mostly spirally twisted, markedly obliquely contracted near spikelet.</description>
      <determination>60 Eleocharis torticulmis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Culms subterete to slightly compressed, 0.5–1.2 mm wide, not more than 2 times wider than thick, not spirally twisted, not contracted near spikelet.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Culms markedly arching; perianth bristles very unequal in same flower, some shorter than 1/2 of achene length and smooth.</description>
      <determination>58 Eleocharis bernardina</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Culms erect; perianth bristles all equaling achene to exceeding tubercle, spinulose.</description>
      <determination>59 Eleocharis suksdorfiana</determination>
    </key_statement>
  </key>
</bio:treatment>