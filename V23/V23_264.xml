<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">141</other_info_on_meta>
    <other_info_on_meta type="treatment_page">164</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cyperus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Cyperus</taxon_name>
    <taxon_hierarchy>family cyperaceae;genus cyperus;subgenus Cyperus</taxon_hierarchy>
    <other_info_on_name type="fna_id">312136</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(Vahl) C. B. Clarke" date="unknown" rank="subgenus">Chlorocyperus</taxon_name>
    <taxon_hierarchy>subgenus Chlorocyperus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="subgenus">Mariscus</taxon_name>
    <taxon_hierarchy>genus Cyperus;subgenus Mariscus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Papyrus</taxon_name>
    <taxon_hierarchy>subgenus Papyrus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms and leaves with Kranz (chlorocyperoid) anatomy.</text>
      <biological_entity id="o28434" name="culm" name_original="culms" src="d0_s0" type="structure" />
      <biological_entity id="o28435" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: spikes loose to dense, ovoid to cylindric;</text>
      <biological_entity id="o28436" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o28437" name="spike" name_original="spikes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s1" value="loose" value_original="loose" />
        <character is_modifier="false" name="density" src="d0_s1" value="dense" value_original="dense" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s1" to="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rachilla falling early or persistent, wingless or winged.</text>
      <biological_entity id="o28438" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o28439" name="rachillum" name_original="rachilla" src="d0_s2" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="falling" value_original="falling" />
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="wingless" value_original="wingless" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spikelets: if rachilla articulate at base, spikelets borne in spikes on conspicuous rachis, falling as achenes mature, if rachilla continuous at base, persistent after achenes mature;</text>
      <biological_entity id="o28440" name="spikelet" name_original="spikelets" src="d0_s3" type="structure" />
      <biological_entity id="o28441" name="rachillum" name_original="rachilla" src="d0_s3" type="structure">
        <character constraint="at base" constraintid="o28442" is_modifier="false" name="architecture" src="d0_s3" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o28442" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o28443" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character constraint="as achenes" constraintid="o28446" is_modifier="false" name="life_cycle" src="d0_s3" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o28444" name="spike" name_original="spikes" src="d0_s3" type="structure" />
      <biological_entity id="o28445" name="rachis" name_original="rachis" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o28446" name="achene" name_original="achenes" src="d0_s3" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o28447" name="rachillum" name_original="rachilla" src="d0_s3" type="structure">
        <character constraint="at base" constraintid="o28448" is_modifier="false" name="architecture" src="d0_s3" value="continuous" value_original="continuous" />
        <character constraint="after achenes" constraintid="o28449" is_modifier="false" name="duration" notes="" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o28448" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o28449" name="achene" name_original="achenes" src="d0_s3" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o28443" id="r3505" name="borne in" negation="false" src="d0_s3" to="o28444" />
      <relation from="o28443" id="r3506" name="borne in" negation="false" src="d0_s3" to="o28445" />
    </statement>
    <statement id="d0_s4">
      <text>floral scales persistent or deciduous;</text>
      <biological_entity id="o28450" name="spikelet" name_original="spikelets" src="d0_s4" type="structure" />
      <biological_entity constraint="floral" id="o28451" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens 3 (1, less frequently 2 in C. squarrosus and C. granitophilus).</text>
      <biological_entity id="o28452" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity id="o28453" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: stigmas 3.</text>
      <biological_entity id="o28454" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o28455" name="stigma" name_original="stigmas" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Achenes trigonous, sometimes dorsiventrally flattened.</text>
      <biological_entity id="o28456" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" modifier="sometimes dorsiventrally" name="shape" src="d0_s7" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate and tropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate and tropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15d.</number>
  <discussion>Species ca. 350–400 (59 in the flora).</discussion>
  <references>
    <reference>Carter, J. R. and S. D. Jones. 1997. Notes on the Cyperus retroflexus complex (Cyperaceae) with three nomenclatural proposals. Rhodora 99: 319–334.</reference>
  </references>
  
</bio:treatment>