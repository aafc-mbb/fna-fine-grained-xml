<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="treatment_page">238</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="genus">rhynchospora</taxon_name>
    <taxon_name authority="M. A. Curtis" date="1849" rank="species">pallida</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci. Arts, ser.</publication_title>
      <place_in_publication>2: 7: 409. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus rhynchospora;species pallida</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357913</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phaeocephalum</taxon_name>
    <taxon_name authority="(M. A. Curtis) House" date="unknown" rank="species">pallidum</taxon_name>
    <taxon_hierarchy>genus Phaeocephalum;species pallidum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchospora</taxon_name>
    <taxon_name authority="Steudel" date="unknown" rank="species">curtisii</taxon_name>
    <place_of_publication>
      <place_in_publication>1855</place_in_publication>
      <other_info_on_pub>not R. curtissii Britton 1903</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Rhynchospora;species curtisii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose, 40–100 cm, base bulbous;</text>
      <biological_entity id="o4865" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4866" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes stoloniferous, short, wiry.</text>
      <biological_entity id="o4867" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="wiry" value_original="wiry" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect or excurved, linear, leafy, trigonous, slender.</text>
      <biological_entity id="o4868" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="excurved" value_original="excurved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="shape" src="d0_s2" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves slightly to much exceeded by culm;</text>
      <biological_entity id="o4869" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4870" name="culm" name_original="culm" src="d0_s3" type="structure" />
      <relation from="o4869" id="r583" modifier="much" name="exceeded by" negation="false" src="d0_s3" to="o4870" />
    </statement>
    <statement id="d0_s4">
      <text>blades ascending, narrowly linear, proximally flat, 1–3 mm wide, apex trigonous, tapering gradually, setaceous.</text>
      <biological_entity id="o4871" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="proximally" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4872" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s4" value="setaceous" value_original="setaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal;</text>
      <biological_entity id="o4873" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spikelet single, terminal cluster of spikelets crowded, hemispheric, 2.5 cm wide;</text>
      <biological_entity id="o4874" name="spikelet" name_original="spikelet" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character constraint="of spikelets" constraintid="o4875" is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character name="width" src="d0_s6" unit="cm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity id="o4875" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leafy bracts linearsetaceous, much exceeding cluster.</text>
      <biological_entity id="o4876" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="much" name="position_relational" src="d0_s7" value="exceeding" value_original="exceeding" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets whitish to tan, narrowly lanceoloid, (3.5–) 4–5.5 mm, apex acuminate;</text>
      <biological_entity id="o4877" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s8" to="tan" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4878" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>fertile scales lanceolate, 3.5–4 (–4.5) mm, apex narrowly acute, minutely awned or apiculate.</text>
      <biological_entity id="o4879" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4880" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: bristles vestigial or obsolete.</text>
      <biological_entity id="o4881" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4882" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="obsolete" value_original="obsolete" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits 1 per spikelet, (1.9–) 2–2.3 mm;</text>
      <biological_entity id="o4883" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character constraint="per spikelet" constraintid="o4884" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s11" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4884" name="spikelet" name_original="spikelet" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>body brown with pale center, lenticular, broadly ellipsoid, 1.5–2 × 1.5 mm, margins flowing to tubercle;</text>
      <biological_entity id="o4885" name="body" name_original="body" src="d0_s12" type="structure">
        <character constraint="with center" constraintid="o4886" is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="2" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o4886" name="center" name_original="center" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o4887" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <biological_entity id="o4888" name="tubercle" name_original="tubercle" src="d0_s12" type="structure" />
      <relation from="o4887" id="r584" name="flowing to" negation="false" src="d0_s12" to="o4888" />
    </statement>
    <statement id="d0_s13">
      <text>surfaces longitudinally finely striate;</text>
      <biological_entity id="o4889" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="longitudinally finely" name="coloration_or_pubescence_or_relief" src="d0_s13" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tubercle depressedtriangular, 0.2–0.3 (–0.4) mm.</text>
      <biological_entity id="o4890" name="tubercle" name_original="tubercle" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s14" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sands and peats of clearings in pine flatwoods, barrens, and savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sands" constraint="of clearings in pine flatwoods , barrens , and savannas" />
        <character name="habitat" value="peats" constraint="of clearings in pine flatwoods , barrens , and savannas" />
        <character name="habitat" value="clearings" constraint="in pine flatwoods , barrens , and savannas" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="barrens" />
        <character name="habitat" value="savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del., Md., N.J., N.Y., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>67.</number>
  
</bio:treatment>