<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">82</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="series">eleocharis</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">bolanderi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 392. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleocharis;series eleocharis;species bolanderi;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357738</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eleocharis</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">montevidensis</taxon_name>
    <taxon_name authority="(A. Gray) V. E. Grant" date="unknown" rank="variety">bolanderi</taxon_name>
    <taxon_hierarchy>genus Eleocharis;species montevidensis;variety bolanderi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, densely tufted;</text>
      <biological_entity id="o4891" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes caudexlike, mostly hidden by culms and roots, short, 1.5–3 mm thick, hard, cortex persistent, internodes very short, scales not evident.</text>
      <biological_entity id="o4892" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="caudex-like" value_original="caudexlike" />
        <character constraint="by roots" constraintid="o4894" is_modifier="false" modifier="mostly" name="prominence" src="d0_s1" value="hidden" value_original="hidden" />
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s1" value="short" value_original="short" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o4893" name="culm" name_original="culms" src="d0_s1" type="structure" />
      <biological_entity id="o4894" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o4895" name="cortex" name_original="cortex" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o4896" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o4897" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s1" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms subterete, often with to 6 prominent ridges when dry, sulcate, 10–30 cm × 0.3–0.5 mm, firm to rigid, spongy.</text>
      <biological_entity id="o4898" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="subterete" value_original="subterete" />
      </biological_entity>
      <biological_entity id="o4899" name="ridge" name_original="ridges" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s2" to="6" />
        <character is_modifier="true" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="sulcate" value_original="sulcate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s2" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="firm" name="texture" src="d0_s2" to="rigid" />
        <character is_modifier="false" name="texture" src="d0_s2" value="spongy" value_original="spongy" />
      </biological_entity>
      <relation from="o4898" id="r585" modifier="often" name="with" negation="false" src="d0_s2" to="o4899" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: distal leaf-sheaths persistent, not splitting, proximally brown, red, or stramineous, distally stramineous, green or reddish, papery, apex sometimes reddish, obtuse, rarely callose, tooth absent.</text>
      <biological_entity id="o4900" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o4901" name="sheath" name_original="leaf-sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s3" value="splitting" value_original="splitting" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s3" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s3" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s3" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s3" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="texture" src="d0_s3" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o4902" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="callose" value_original="callose" />
      </biological_entity>
      <biological_entity id="o4903" name="tooth" name_original="tooth" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets ovoid, 3–8 × 2–3 mm, apex acute to obtuse;</text>
      <biological_entity id="o4904" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4905" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal scale amplexicaulous, entire;</text>
      <biological_entity constraint="proximal" id="o4906" name="scale" name_original="scale" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="amplexicaulous" value_original="amplexicaulous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>subproximal scale with flower;</text>
      <biological_entity constraint="subproximal" id="o4907" name="scale" name_original="scale" src="d0_s6" type="structure" />
      <biological_entity id="o4908" name="flower" name_original="flower" src="d0_s6" type="structure" />
      <relation from="o4907" id="r586" name="with" negation="false" src="d0_s6" to="o4908" />
    </statement>
    <statement id="d0_s7">
      <text>floral scales spreading in fruit, 8–30, 4–5 per mm of rachilla, dark-brown to blackish, midrib regions often stramineous or greenish, ovate to lanceolate, 2–3 × 1.5 mm, apex entire, acute, often carinate in distal part of spikelet.</text>
      <biological_entity constraint="floral" id="o4909" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o4910" is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="dark-brown" name="coloration" notes="" src="d0_s7" to="blackish" />
      </biological_entity>
      <biological_entity id="o4910" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="30" />
        <character char_type="range_value" constraint="per mm" constraintid="o4911" from="4" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o4911" name="mm" name_original="mm" src="d0_s7" type="structure" />
      <biological_entity id="o4912" name="rachillum" name_original="rachilla" src="d0_s7" type="structure" />
      <biological_entity constraint="midrib" id="o4913" name="region" name_original="regions" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="3" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o4914" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character constraint="in distal part" constraintid="o4915" is_modifier="false" modifier="often" name="shape" src="d0_s7" value="carinate" value_original="carinate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4915" name="part" name_original="part" src="d0_s7" type="structure" />
      <biological_entity id="o4916" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <relation from="o4911" id="r587" name="part_of" negation="false" src="d0_s7" to="o4912" />
      <relation from="o4915" id="r588" name="part_of" negation="false" src="d0_s7" to="o4916" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth bristles 3–6, whitish to stramineous, stout to slender, often unequal, from rudimentary to 1/2 of achene length;</text>
      <biological_entity id="o4917" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="perianth" id="o4918" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="6" />
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s8" to="stramineous" />
        <character char_type="range_value" from="stout" name="size" src="d0_s8" to="slender" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character constraint="from" is_modifier="false" name="prominence" src="d0_s8" value="rudimentary" value_original="rudimentary" />
        <character char_type="range_value" constraint="of achene" constraintid="o4919" from="0" name="quantity" src="d0_s8" to="1/2" />
      </biological_entity>
      <biological_entity id="o4919" name="achene" name_original="achene" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 3;</text>
      <biological_entity id="o4920" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4921" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers dark yellow to brown, 0.9–1.4 mm;</text>
      <biological_entity id="o4922" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4923" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark yellow" name="coloration" src="d0_s10" to="brown" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s10" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3-fid.</text>
      <biological_entity id="o4924" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4925" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes falling with scales, stramineous, rarely dark-brown, ovoid to obpyriform, slightly to greatly compressedtrigonous, rarely thickly lenticular, angles prominent or abaxial angle obscure, 0.9–1.2 × 0.65–0.8 mm, apex narrowly to broadly truncate, neck short, often compressed more than body, at 20–30X finely rugulose with more than 20 horizontal ridges in a vertical series or reticulate or cancellate.</text>
      <biological_entity id="o4926" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character constraint="with scales" constraintid="o4927" is_modifier="false" name="life_cycle" src="d0_s12" value="falling" value_original="falling" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s12" to="obpyriform" />
        <character is_modifier="false" modifier="slightly to greatly; greatly; rarely thickly" name="shape" src="d0_s12" value="lenticular" value_original="lenticular" />
      </biological_entity>
      <biological_entity id="o4927" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o4928" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4929" name="angle" name_original="angle" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="obscure" value_original="obscure" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s12" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.65" from_unit="mm" name="width" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4930" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o4931" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="cancellate" value_original="cancellate" />
      </biological_entity>
      <biological_entity id="o4932" name="body" name_original="body" src="d0_s12" type="structure">
        <character constraint="with ridges" constraintid="o4933" is_modifier="false" modifier="at; finely" name="relief" src="d0_s12" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity id="o4933" name="ridge" name_original="ridges" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s12" upper_restricted="false" />
        <character is_modifier="true" name="orientation" src="d0_s12" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <biological_entity id="o4934" name="series" name_original="series" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o4933" id="r589" name="in" negation="false" src="d0_s12" to="o4934" />
    </statement>
    <statement id="d0_s13">
      <text>Tubercles whitish to brown, pyramidal, lower than wide, often 3-lobed as viewed from the top, 0.1–0.3 × 0.4–0.65 mm.</text>
      <biological_entity id="o4935" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s13" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="position" src="d0_s13" value="lower than wide" value_original="lower than wide" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s13" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s13" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s13" to="0.65" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4936" name="top" name_original="top" src="d0_s13" type="structure" />
      <relation from="o4935" id="r590" name="viewed from the" negation="false" src="d0_s13" to="o4936" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh, often summer-dry meadows, springs, seeps, stream margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="summer-dry meadows" modifier="fresh often" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="stream margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Colo., Idaho, Nev., Oreg., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <discussion>Eleocharis bolanderi is clearly distinct from E. montevidensis, from which it differs in its dense, tufted habit with short, caudexlike rhizomes, its leaf sheaths without a tooth, its achene and tubercle shapes, and its acute floral scales.</discussion>
  <discussion>Specimens of Eleocharis bolanderi without rhizomes or achenes are easily confused with E. decumbens, which often may be distinguished by culms 0.5–2 mm wide, and spikelets with scales sometimes more than 3 mm long. The tubercles of E. bolanderi are usually poorly developed and much lower than wide; in E. decumbens they are usually well developed and about as high as wide.</discussion>
  
</bio:treatment>