<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>S. Galen Smith</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="treatment_page">137</other_info_on_meta>
    <other_info_on_meta type="illustrator">Amanda Humphrey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">Isolepis</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>221. 1810</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus Isolepis</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, isos, equal, similar, and lepis, a scale</other_info_on_name>
    <other_info_on_name type="fna_id">116586</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="(R. Brown) Grisebach" date="unknown" rank="section">Isolepis</taxon_name>
    <taxon_hierarchy>genus Scirpus;section Isolepis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, cespitose, rhizomatous or not, smooth, glabrous.</text>
      <biological_entity id="o1002" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="architecture" src="d0_s0" value="not" value_original="not" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s0" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms terete.</text>
      <biological_entity id="o1003" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves all basal;</text>
      <biological_entity id="o1004" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o1005" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>sheaths green to stramineous, sometimes reddish proximally;</text>
      <biological_entity id="o1006" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="stramineous" />
        <character is_modifier="false" modifier="sometimes; proximally" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules absent;</text>
      <biological_entity id="o1007" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades rudimentary to exceeding culms.</text>
      <biological_entity id="o1008" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o1009" name="culm" name_original="culms" src="d0_s5" type="structure" />
      <relation from="o1008" id="r131" name="exceeding" negation="false" src="d0_s5" to="o1009" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, sometimes pseudolateral, capitate or solitary spikelet;</text>
      <biological_entity id="o1010" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s6" value="pseudolateral" value_original="pseudolateral" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o1011" name="spikelet" name_original="spikelet" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spikelets 1–3 (–15);</text>
      <biological_entity id="o1012" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts 1 (–2), spreading to erect, like foliage leaf-blades.</text>
      <biological_entity id="o1013" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="2" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s8" to="erect" />
      </biological_entity>
      <biological_entity constraint="foliage" id="o1014" name="leaf-blade" name_original="leaf-blades" src="d0_s8" type="structure" />
      <relation from="o1013" id="r132" name="like" negation="false" src="d0_s8" to="o1014" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets: scales 8–25, spirally arranged, each subtending flower.</text>
      <biological_entity id="o1015" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o1016" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="25" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s9" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o1017" name="flower" name_original="flower" src="d0_s9" type="structure" />
      <relation from="o1016" id="r133" name="subtending" negation="false" src="d0_s9" to="o1017" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers bisexual;</text>
      <biological_entity id="o1018" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perianth absent;</text>
      <biological_entity id="o1019" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 1–3;</text>
      <biological_entity id="o1020" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles linear, 2–3-fid, base persistent, sometimes slightly enlarged.</text>
      <biological_entity id="o1021" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s13" value="2-3-fid" value_original="2-3-fid" />
      </biological_entity>
      <biological_entity id="o1022" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="sometimes slightly" name="size" src="d0_s13" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes biconvex or trigonous, papillose or longitudinally ribbed.</text>
      <biological_entity id="o1023" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" name="shape" src="d0_s14" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="longitudinally" name="architecture_or_shape" src="d0_s14" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide in cool-tropical and temperate regions, especially Africa and Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide in cool-tropical and temperate regions" establishment_means="native" />
        <character name="distribution" value="especially Africa and Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>Species 69 (4 in the flora).</discussion>
  <discussion>Isolepis is difficult to delimit on a worldwide basis and has been included in Scirpus in the broad sense. Data derived from embryologic, genetic, and other studies led in recent years to the acceptance of Isolepis as a distinct genus (J. J. Bruhl 1995; P. Goetghebeur 1998; A. M. Muasya et al. 2001).</discussion>
  <references>
    <reference>Muasya, A. M. 1998. Systematics of the Genus Isolepis R. Br. (Cyperaceae). Ph.D. thesis. University of Reading.</reference>
    <reference>Muasya, A. M., D. A. Simpson, M. W. Chase, and A. Culham. 2001. A phylogeny of Isolepis (Cyperaceae) inferred using plastid rbcL and trnL-F sequence data. Syst. Bot. 26: 342–353.</reference>
    <reference>Muasya, A. M. and D. A. Simpson. 2002. A monograph of the genus Isolepis R. Br. (Cyperaceae). Kew Bull. 57: 257–362.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Spikelets with scales markedly gibbous, often clasping shed achenes, colorless to stramineous, or orangish, or greenish; achenes acutely equilaterally trigonous; leaf sheaths green to stramineous or brown.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Spikelets with scales neither gibbous nor clasping shed achenes, usually at least partly orange- to red-brown or blackish; achenes compressed-trigonous or thickly plano-convex; leaf sheaths usually reddish proximally.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scales from middle of spikelet 1.8–2 mm, with awns 0.2–0.5 mm; achenes 1–1.5 mm.</description>
      <determination>1 Isolepis carinata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Scales from middle of spikelet 1–1.2 mm, with mucros to 0.1 mm; achenes 0.7–0.9 mm.</description>
      <determination>2 Isolepis pseudosetacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Achenes papillose, not ribbed; culms, leaves, and involucral bracts not orange-punctate or sheaths sparsely so.</description>
      <determination>3 Isolepis cernua</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Achenes not papillose, prominently longitudinally ribbed at 10X and minutely transversely ridged at 20–30X; culms, leaves, and involucral bracts orange-punctate at 10– 15X.</description>
      <determination>4 Isolepis setacea</determination>
    </key_statement>
  </key>
</bio:treatment>