<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="treatment_page">139</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">isolepis</taxon_name>
    <taxon_name authority="(Vahl) Roemer &amp; Schultes in J. J. Roemer et al." date="1817" rank="species">cernua</taxon_name>
    <place_of_publication>
      <publication_title>in J. J. Roemer et al., Syst. Veg.</publication_title>
      <place_in_publication>2: 106. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus isolepis;species cernua</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357844</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="species">cernuus</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>2: 245. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scirpus;species cernuus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cernuus</taxon_name>
    <taxon_name authority="(Torrey) Beetle" date="unknown" rank="variety">californicus</taxon_name>
    <taxon_hierarchy>genus Scirpus;species cernuus;variety californicus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scirpus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cernuus</taxon_name>
    <taxon_name authority="(Torrey) Thorne" date="unknown" rank="subspecies">californicus</taxon_name>
    <taxon_hierarchy>genus Scirpus;species cernuus;subspecies californicus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual (or perennial?);</text>
      <biological_entity id="o11950" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes usually obscured by culm bases and very short, sometimes vertical and elongated.</text>
      <biological_entity id="o11951" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character constraint="by culm bases" constraintid="o11952" is_modifier="false" modifier="usually" name="prominence" src="d0_s1" value="obscured" value_original="obscured" />
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" notes="" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="length" src="d0_s1" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity constraint="culm" id="o11952" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 4–40 cm × 0.2–0.5 mm.</text>
      <biological_entity id="o11953" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s2" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves sometimes sparsely orange-punctate at 10–15X;</text>
      <biological_entity id="o11954" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at 10-15x" is_modifier="false" modifier="sometimes sparsely" name="coloration_or_relief" src="d0_s3" value="orange-punctate" value_original="orange-punctate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths usually reddish proximally;</text>
      <biological_entity id="o11955" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually; proximally" name="coloration" src="d0_s4" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal blade rudimentary to much longer than sheath, often exceeding culm, to 20 cm × 0.2–1 mm.</text>
      <biological_entity constraint="distal" id="o11956" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="rudimentary" value_original="rudimentary" />
        <character constraint="than sheath" constraintid="o11957" is_modifier="false" name="length_or_size" src="d0_s5" value="much longer" value_original="much longer" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11957" name="sheath" name_original="sheath" src="d0_s5" type="structure" />
      <biological_entity id="o11958" name="culm" name_original="culm" src="d0_s5" type="structure" />
      <relation from="o11956" id="r1475" modifier="often" name="exceeding" negation="false" src="d0_s5" to="o11958" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: involucral-bract 1, sometimes subtending flower or resembling enlarged floral scale, 2–6 (–23) mm.</text>
      <biological_entity id="o11959" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o11960" name="involucral-bract" name_original="involucral-bract" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="23" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11961" name="flower" name_original="flower" src="d0_s6" type="structure" />
      <biological_entity constraint="floral" id="o11962" name="scale" name_original="scale" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <relation from="o11960" id="r1476" name="sometimes subtending" negation="false" src="d0_s6" to="o11961" />
      <relation from="o11960" id="r1477" name="resembling" negation="false" src="d0_s6" to="o11962" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 2–5 × 1–2 mm;</text>
      <biological_entity id="o11963" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>scales partly or completely dark orange to redbrown, rarely stramineous, midrib greenish to stramineous, not gibbous, obscurely to prominently 3–11-veined, midrib keeled near apex, membranous, hyaline, apex rounded to acute, with mucro less than 0.1 mm;</text>
      <biological_entity id="o11964" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character char_type="range_value" from="dark orange" modifier="completely" name="coloration" src="d0_s8" to="redbrown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity id="o11965" name="midrib" name_original="midrib" src="d0_s8" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s8" to="stramineous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" modifier="obscurely to prominently" name="architecture" src="d0_s8" value="3-11-veined" value_original="3-11-veined" />
      </biological_entity>
      <biological_entity id="o11966" name="midrib" name_original="midrib" src="d0_s8" type="structure">
        <character constraint="near apex" constraintid="o11967" is_modifier="false" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="texture" notes="" src="d0_s8" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o11967" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o11968" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
      <biological_entity id="o11969" name="mucro" name_original="mucro" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="0.1" to_unit="mm" />
      </biological_entity>
      <relation from="o11968" id="r1478" name="with" negation="false" src="d0_s8" to="o11969" />
    </statement>
    <statement id="d0_s9">
      <text>proximal scale to 2 mm;</text>
    </statement>
    <statement id="d0_s10">
      <text>other scales 1.2–1.8 × 1–1.3 mm.</text>
      <biological_entity constraint="proximal" id="o11970" name="scale" name_original="scale" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11971" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s10" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: anthers 0.3–0.6 mm;</text>
      <biological_entity id="o11972" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11973" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 3-fid or 3-fid and 2-fid.</text>
      <biological_entity id="o11974" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o11975" name="style" name_original="styles" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes falling separately from scales, medium to dark-brown or stramineous, ellipsoid to obovoid, compressedtrigonous to thickly biconvex, lateral angles usually prominent, abaxial angle prominent to obscure, faces convex or adaxial face slightly concave, 0.8–1 × 0.5–0.7 mm, distinctly papillose at 10–15X to obscurely papillose at 40X, often with thin whitish surface layer.</text>
      <biological_entity id="o11976" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character constraint="from scales" constraintid="o11977" is_modifier="false" name="life_cycle" src="d0_s13" value="falling" value_original="falling" />
        <character constraint="to lateral angles, angle, faces" constraintid="o11978, o11979, o11980" is_modifier="false" name="size" notes="" src="d0_s13" value="medium" value_original="medium" />
      </biological_entity>
      <biological_entity id="o11977" name="scale" name_original="scales" src="d0_s13" type="structure" />
      <biological_entity constraint="lateral" id="o11978" name="angle" name_original="angles" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="ellipsoid" is_modifier="true" name="shape" src="d0_s13" to="obovoid" />
        <character is_modifier="true" modifier="thickly" name="shape" src="d0_s13" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="prominent" name="prominence" src="d0_s13" to="obscure" />
        <character is_modifier="false" name="shape" src="d0_s13" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o11979" name="angle" name_original="angle" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="ellipsoid" is_modifier="true" name="shape" src="d0_s13" to="obovoid" />
        <character is_modifier="true" modifier="thickly" name="shape" src="d0_s13" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="prominent" name="prominence" src="d0_s13" to="obscure" />
        <character is_modifier="false" name="shape" src="d0_s13" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o11980" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="ellipsoid" is_modifier="true" name="shape" src="d0_s13" to="obovoid" />
        <character is_modifier="true" modifier="thickly" name="shape" src="d0_s13" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="prominent" name="prominence" src="d0_s13" to="obscure" />
        <character is_modifier="false" name="shape" src="d0_s13" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11981" name="angle" name_original="angles" src="d0_s13" type="structure" />
      <biological_entity id="o11983" name="40x" name_original="40x" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="obscurely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o11984" name="layer" name_original="layer" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="obscurely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 30.</text>
      <biological_entity constraint="adaxial" id="o11982" name="face" name_original="face" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="concave" value_original="concave" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s13" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="0.7" to_unit="mm" />
        <character constraint="to 40x, layer" constraintid="o11983, o11984" is_modifier="false" modifier="distinctly" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11985" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late spring–winter (Pacific Coast), winter–spring (Texas).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" modifier="Pacific Coast" to="winter" from="late spring" />
        <character name="fruiting time" char_type="range_value" modifier="Texas" to="spring" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, freshwater to brackish places on beaches, dunes, marine bluffs, sandy areas, mostly coastal</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="places" constraint="on beaches , dunes ," />
        <character name="habitat" value="beaches" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="wet" />
        <character name="habitat" value="freshwater to brackish places on beaches" />
        <character name="habitat" value="marine bluffs" />
        <character name="habitat" value="sandy areas" />
        <character name="habitat" value="coastal" modifier="mostly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Tex., Wash.; Mexico (Baja California); temperate South America; Eurasia; Africa; Australia; New Zealand.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="temperate South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="New Zealand" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Isolepis cernua is widespread and variable. Four varieties were recognized by A. M. Muasya and D. M. Simpson (2002). Only var. ceruna is known from North America. The earliest collection I have seen from the Pacific Coast is from 1888; the earliest collection I have seen from Texas is from 1974.</discussion>
  
</bio:treatment>