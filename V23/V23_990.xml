<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David F. Murray</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="Meinshausen" date="1901" rank="section">Circinatae</taxon_name>
    <place_of_publication>
      <publication_title>Trudy Imp. S.-Petersburgsk. Bot. Sada</publication_title>
      <place_in_publication>18(3): 280. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Circinatae</taxon_hierarchy>
    <other_info_on_name type="fna_id">302685</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose or not, short or long-rhizomatous.</text>
      <biological_entity id="o2065" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" src="d0_s0" value="not" value_original="not" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="long-rhizomatous" value_original="long-rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms pale-brown at base.</text>
      <biological_entity id="o2066" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o2067" is_modifier="false" name="coloration" src="d0_s1" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
      <biological_entity id="o2067" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths not or scarcely fibrous;</text>
      <biological_entity id="o2068" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o2069" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="scarcely" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous;</text>
      <biological_entity id="o2070" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o2071" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades threadlike or V-shaped in cross-section, glabrous.</text>
      <biological_entity id="o2072" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2073" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="thread-like" value_original="threadlike" />
        <character constraint="in cross-section" constraintid="o2074" is_modifier="false" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2074" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescence 1 spike;</text>
      <biological_entity id="o2075" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2076" name="spike" name_original="spike" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts absent;</text>
      <biological_entity id="o2077" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spike androgynous or pistillate, lax.</text>
      <biological_entity id="o2078" name="spike" name_original="spike" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="androgynous" value_original="androgynous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Proximal pistillate scales persistent, exceeding perigynia, apex cuspidate or awned.</text>
      <biological_entity constraint="proximal" id="o2079" name="spike" name_original="spikes" src="d0_s8" type="structure" />
      <biological_entity id="o2080" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o2081" name="perigynium" name_original="perigynia" src="d0_s8" type="structure" />
      <biological_entity id="o2082" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="awned" value_original="awned" />
      </biological_entity>
      <relation from="o2080" id="r257" name="exceeding" negation="false" src="d0_s8" to="o2081" />
    </statement>
    <statement id="d0_s9">
      <text>Perigynia not more than 15, erect, weakly veined, linear-lanceolate, obscurely trigonous, 3–6 mm, 3–4 times as long as wide, base tapering, margins rounded, apex tapering to beak, glabrous;</text>
      <biological_entity id="o2083" name="perigynium" name_original="perigynia" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s9" value="veined" value_original="veined" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s9" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s9" value="3-4" value_original="3-4" />
      </biological_entity>
      <biological_entity id="o2084" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o2085" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o2086" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character constraint="to beak" constraintid="o2087" is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2087" name="beak" name_original="beak" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>beak less than 2 mm, orifice obliquely cleft.</text>
      <biological_entity id="o2088" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2089" name="orifice" name_original="orifice" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="obliquely" name="architecture_or_shape" src="d0_s10" value="cleft" value_original="cleft" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas (2–) 3.</text>
      <biological_entity id="o2090" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s11" to="3" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes usually trigonous, 1.5–2.5 mm, smaller than bodies of perigynia;</text>
      <biological_entity id="o2091" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s12" value="trigonous" value_original="trigonous" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character constraint="than bodies" constraintid="o2092" is_modifier="false" name="size" src="d0_s12" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o2092" name="body" name_original="bodies" src="d0_s12" type="structure" />
      <biological_entity id="o2093" name="perigynium" name_original="perigynia" src="d0_s12" type="structure" />
      <relation from="o2092" id="r258" name="part_of" negation="false" src="d0_s12" to="o2093" />
    </statement>
    <statement id="d0_s13">
      <text>style deciduous.</text>
      <biological_entity id="o2094" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, amphi-Pacific.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="amphi-Pacific" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26yy.</number>
  <discussion>Species 4 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants cespitose, short-rhizomatous; leaves involute; distal margins of perigynia finely serrulate.</description>
      <determination>416 Carex circinata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants not cespitose, long-rhizomatous; leaves flat; distal margins of perigynia smooth.</description>
      <determination>417 Carex anthoxanthea</determination>
    </key_statement>
  </key>
</bio:treatment>