<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">33</other_info_on_meta>
    <other_info_on_meta type="treatment_page">34</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Rottböll" date="1773" rank="genus">fuirena</taxon_name>
    <taxon_name authority="Chapman" date="1897" rank="species">longa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South U.S. ed.</publication_title>
      <place_in_publication>3, 541. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus fuirena;species longa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242357836</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, rhizomatous, 20–60 cm, nearly glabrous;</text>
      <biological_entity id="o17885" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes much branched.</text>
      <biological_entity id="o17886" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms tufted or regularly spaced along rhizome, slender, wandlike, frequently hispidulous distally.</text>
      <biological_entity id="o17887" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="tufted" value_original="tufted" />
        <character constraint="along rhizome" constraintid="o17888" is_modifier="false" modifier="regularly" name="arrangement" src="d0_s2" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="size" notes="" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s2" value="wand-like" value_original="wandlike" />
        <character is_modifier="false" modifier="frequently; distally" name="pubescence" src="d0_s2" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o17888" name="rhizome" name_original="rhizome" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: principal blades at mid and distal culm, shortlinear, 1.5–5 cm, puberulent distally.</text>
      <biological_entity id="o17889" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="principal" id="o17890" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="mid and distal" id="o17891" name="culm" name_original="culm" src="d0_s3" type="structure" />
      <relation from="o17890" id="r2164" name="at" negation="false" src="d0_s3" to="o17891" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, of 1–2 clusters;</text>
      <biological_entity id="o17892" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spikelets 1 or 2–5 per cluster, subtending involucral-bract longer than spikelets.</text>
      <biological_entity id="o17893" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" constraint="per subtending involucral-bract" constraintid="o17894" from="2" name="quantity" src="d0_s5" to="5" />
        <character constraint="than spikelets" constraintid="o17895" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o17894" name="involucral-bract" name_original="involucral-bract" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character constraint="than spikelets" constraintid="o17895" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o17895" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Spikelets cylindric-lanceoloid or lanceovoid, 10 mm, apex acute;</text>
      <biological_entity id="o17896" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric-lanceoloid" value_original="cylindric-lanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceovoid" value_original="lanceovoid" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o17897" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>fertile scales mostly obovate, 2.5–3.5 mm;</text>
      <biological_entity id="o17898" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>mucro erect, 1/2 or more length of scale;</text>
      <biological_entity id="o17899" name="mucro" name_original="mucro" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o17900" name="scale" name_original="scale" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>median ribs 5–7, 3 convergent to mucro.</text>
      <biological_entity constraint="median" id="o17901" name="rib" name_original="ribs" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="7" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character constraint="to mucro" constraintid="o17902" is_modifier="false" name="arrangement" src="d0_s9" value="convergent" value_original="convergent" />
      </biological_entity>
      <biological_entity id="o17902" name="mucro" name_original="mucro" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: perianth bristles reaching base of perianth blades, retrorsely scabrid;</text>
      <biological_entity id="o17903" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="perianth" id="o17904" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence_or_relief" src="d0_s10" value="scabrid" value_original="scabrid" />
      </biological_entity>
      <biological_entity id="o17905" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity constraint="perianth" id="o17906" name="blade" name_original="blades" src="d0_s10" type="structure" />
      <relation from="o17904" id="r2165" name="reaching" negation="false" src="d0_s10" to="o17905" />
      <relation from="o17904" id="r2166" name="part_of" negation="false" src="d0_s10" to="o17906" />
    </statement>
    <statement id="d0_s11">
      <text>perianth blades oblong-obovate, as long as claws, base flattened, 3-ribbed, apex thickened, narrowly acuminate, sharp;</text>
      <biological_entity id="o17907" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="perianth" id="o17908" name="blade" name_original="blades" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong-obovate" value_original="oblong-obovate" />
      </biological_entity>
      <biological_entity id="o17909" name="claw" name_original="claws" src="d0_s11" type="structure" />
      <biological_entity id="o17910" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="3-ribbed" value_original="3-ribbed" />
      </biological_entity>
      <biological_entity id="o17911" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s11" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="sharp" value_original="sharp" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers mostly 3, 1.3–1.5 mm.</text>
      <biological_entity id="o17912" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o17913" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes: body angles pale, wirelike, faces lustrous redbrown or chestnut-brown, 1 mm;</text>
      <biological_entity id="o17914" name="achene" name_original="achenes" src="d0_s13" type="structure" />
      <biological_entity constraint="body" id="o17915" name="angle" name_original="angles" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale" value_original="pale" />
        <character is_modifier="false" name="shape" src="d0_s13" value="wirelike" value_original="wirelike" />
      </biological_entity>
      <biological_entity id="o17916" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="chestnut-brown" value_original="chestnut-brown" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak narrow, linear, hispidulous at tip.</text>
      <biological_entity id="o17917" name="achene" name_original="achenes" src="d0_s14" type="structure" />
      <biological_entity id="o17919" name="tip" name_original="tip" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 46.</text>
      <biological_entity id="o17918" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s14" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character constraint="at tip" constraintid="o17919" is_modifier="false" name="pubescence" src="d0_s14" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17920" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brackish and freshwater marsh edges, flatwoods seeps, ditches, moist savanna</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brackish" />
        <character name="habitat" value="freshwater marsh edges" />
        <character name="habitat" value="flatwoods seeps" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="moist savanna" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Fuirena longa is ostensibly a stable hybrid between F. breviseta and F. scirpoidea.</discussion>
  
</bio:treatment>