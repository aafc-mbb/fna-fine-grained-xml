<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="genus">rhynchospora</taxon_name>
    <taxon_name authority="(Nees) Boeckeler" date="1873" rank="species">eximia</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>37: 601. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus rhynchospora;species eximia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357878</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spermodon</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="species">eximius</taxon_name>
    <place_of_publication>
      <publication_title>in B. Seemann, Bot. Voy. Herald,</publication_title>
      <place_in_publication>222. 1854</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spermodon;species eximius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Psilocarya</taxon_name>
    <taxon_name authority="(Nees) Liebmann" date="unknown" rank="species">schiedeana</taxon_name>
    <taxon_hierarchy>genus Psilocarya;species schiedeana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchospora</taxon_name>
    <taxon_name authority="C. Wright" date="unknown" rank="species">oxycephala</taxon_name>
    <taxon_hierarchy>genus Rhynchospora;species oxycephala;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchospora</taxon_name>
    <taxon_name authority="Grisebach" date="unknown" rank="species">psilocaroides</taxon_name>
    <taxon_hierarchy>genus Rhynchospora;species psilocaroides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial or annual, single or cespitose, (10–) 20–50 cm;</text>
      <biological_entity id="o25167" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" value_original="single" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent.</text>
      <biological_entity id="o25168" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms spreading to erect, leafy, obtusely triangular.</text>
      <biological_entity id="o25169" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s2" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves often exceeding inflorescences;</text>
      <biological_entity id="o25170" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o25171" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <relation from="o25170" id="r3086" modifier="often" name="exceeding" negation="false" src="d0_s3" to="o25171" />
    </statement>
    <statement id="d0_s4">
      <text>blades narrowly linear, proximally flat, 1–3 mm wide, apex trigonous, tapering.</text>
      <biological_entity id="o25172" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="proximally" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25173" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal and axillary, clusters of 1–5 corymbs;</text>
      <biological_entity id="o25174" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character constraint="of corymbs" constraintid="o25175" is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o25175" name="corymb" name_original="corymbs" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leafy bracts much exceeding corymbs.</text>
      <biological_entity id="o25176" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o25177" name="corymb" name_original="corymbs" src="d0_s6" type="structure" />
      <relation from="o25176" id="r3087" modifier="much" name="exceeding" negation="false" src="d0_s6" to="o25177" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets few to several, on ascending, stiff, short-to-elongate branches, redbrown to brown, lanceoloid, (5–) 6–10 mm, apex acuminate;</text>
      <biological_entity id="o25178" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s7" to="several" />
        <character char_type="range_value" from="redbrown" name="coloration" notes="" src="d0_s7" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25179" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="true" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
        <character is_modifier="true" name="size" src="d0_s7" value="short-to-elongate" value_original="short-to-elongate" />
      </biological_entity>
      <biological_entity id="o25180" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o25178" id="r3088" name="on" negation="false" src="d0_s7" to="o25179" />
    </statement>
    <statement id="d0_s8">
      <text>fertile scales many, ovate, shallowly convex, 5 mm, apex acuminate;</text>
      <biological_entity id="o25181" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="quantity" src="d0_s8" value="many" value_original="many" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s8" value="convex" value_original="convex" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o25182" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>midrib shortexcurrent or not.</text>
      <biological_entity id="o25183" name="midrib" name_original="midrib" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: perianth absent.</text>
      <biological_entity id="o25184" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25185" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits 1.5 mm;</text>
      <biological_entity id="o25186" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>body dark-brown to black, tumidly lenticular, nearly orbicular, 0.8–0.9 × 0.6–0.7 mm, margins grooved, discontinuous with tubercle;</text>
      <biological_entity id="o25187" name="body" name_original="body" src="d0_s12" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s12" to="black" />
        <character is_modifier="false" modifier="tumidly" name="shape" src="d0_s12" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s12" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s12" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25188" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="grooved" value_original="grooved" />
        <character constraint="with tubercle" constraintid="o25189" is_modifier="false" name="architecture_or_arrangement" src="d0_s12" value="discontinuous" value_original="discontinuous" />
      </biological_entity>
      <biological_entity id="o25189" name="tubercle" name_original="tubercle" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>surfaces transversely wavyrugose, ridges of contiguous rows of vertical, linear, raised cells;</text>
      <biological_entity id="o25190" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s13" value="wavyrugose" value_original="wavyrugose" />
      </biological_entity>
      <biological_entity id="o25191" name="ridge" name_original="ridges" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="of vertical" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o25192" name="row" name_original="rows" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="contiguous" value_original="contiguous" />
      </biological_entity>
      <biological_entity id="o25193" name="cell" name_original="cells" src="d0_s13" type="structure" />
      <relation from="o25191" id="r3089" name="part_of" negation="false" src="d0_s13" to="o25192" />
      <relation from="o25191" id="r3090" name="raised" negation="false" src="d0_s13" to="o25193" />
    </statement>
    <statement id="d0_s14">
      <text>tubercle broad, low triangular, 0.2–0.3 mm, crustaceous, base capping fruit summit, raised at ends, apex shortacuminate.</text>
      <biological_entity id="o25194" name="tubercle" name_original="tubercle" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="broad" value_original="broad" />
        <character is_modifier="false" name="position" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s14" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s14" value="crustaceous" value_original="crustaceous" />
      </biological_entity>
      <biological_entity id="o25195" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity constraint="fruit" id="o25196" name="summit" name_original="summit" src="d0_s14" type="structure" />
      <biological_entity id="o25197" name="end" name_original="ends" src="d0_s14" type="structure" />
      <biological_entity id="o25198" name="apex" name_original="apex" src="d0_s14" type="structure" />
      <relation from="o25195" id="r3091" name="capping" negation="false" src="d0_s14" to="o25196" />
      <relation from="o25195" id="r3092" name="raised at" negation="false" src="d0_s14" to="o25197" />
      <relation from="o25195" id="r3093" name="raised at" negation="false" src="d0_s14" to="o25198" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting all year.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet sandy peaty swales, pond shores, depressions in savannas, moist waste areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet sandy peaty swales" />
        <character name="habitat" value="pond shores" />
        <character name="habitat" value="moist to wet sandy peaty swales" constraint="in savannas , moist waste areas" />
        <character name="habitat" value="pond shores" constraint="in savannas , moist waste areas" />
        <character name="habitat" value="depressions" constraint="in savannas , moist waste areas" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="moist waste areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100[–1000] m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="foreign_range" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; South America; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion>Rhynchospora eximia is often found at elevations from near sea level to over 1000 m in the tropics.</discussion>
  
</bio:treatment>