<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="treatment_page">233</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="genus">rhynchospora</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">harperi</taxon_name>
    <place_of_publication>
      <publication_title>Man. S.E. Fl.,</publication_title>
      <place_in_publication>182, 1503. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus rhynchospora;species harperi</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242357891</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchospora</taxon_name>
    <taxon_name authority="(Michaux) Vahl" date="unknown" rank="species">fascicularis</taxon_name>
    <taxon_name authority="(Small) Kükenthal" date="unknown" rank="variety">harperi</taxon_name>
    <taxon_hierarchy>genus Rhynchospora;species fascicularis;variety harperi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchospora</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">leptorhyncha</taxon_name>
    <place_of_publication>
      <place_in_publication>1903</place_in_publication>
      <other_info_on_pub>not C. Wright 1871</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Rhynchospora;species leptorhyncha;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, solitary or cespitose, 50–70 cm;</text>
      <biological_entity id="o25281" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent.</text>
      <biological_entity id="o25282" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect to excurved, leafybased, narrowly linear, ± terete.</text>
      <biological_entity id="o25283" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="excurved" value_original="excurved" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafybased" value_original="leafybased" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves shorter than culm;</text>
      <biological_entity id="o25284" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="than culm" constraintid="o25285" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25285" name="culm" name_original="culm" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blades ascending, narrowly linear, proximally flat or margins slightly involute, 0.5–1 (–2) mm wide, distally canaliculate, apex trigonous, tapering, subulate.</text>
      <biological_entity id="o25286" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="proximally" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o25287" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="canaliculate" value_original="canaliculate" />
      </biological_entity>
      <biological_entity id="o25288" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: spikelet clusters 1–3, laterals 0–2, all turbinate to hemispheric, terminal internode usually excurved;</text>
      <biological_entity id="o25289" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o25290" name="spikelet" name_original="spikelet" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o25291" name="lateral" name_original="laterals" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="2" />
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s5" to="hemispheric" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o25292" name="internode" name_original="internode" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="excurved" value_original="excurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leafy bracts setaceous, overtopping inflorescence.</text>
      <biological_entity id="o25293" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o25294" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="shape" src="d0_s6" value="setaceous" value_original="setaceous" />
      </biological_entity>
      <biological_entity id="o25295" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure" />
      <relation from="o25294" id="r3106" name="overtopping" negation="false" src="d0_s6" to="o25295" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets redbrown, lanceoloid, 5–7 mm, apex acute;</text>
      <biological_entity id="o25296" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25297" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile scales lanceolate, (2.5–) 4–5 mm, apex acute to acuminate;</text>
      <biological_entity id="o25298" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25299" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>midrib paralleled by several indistinct ribs, excurrent as short awns.</text>
      <biological_entity id="o25300" name="midrib" name_original="midrib" src="d0_s9" type="structure">
        <character constraint="as awns" constraintid="o25302" is_modifier="false" name="architecture" src="d0_s9" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o25301" name="rib" name_original="ribs" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="several" value_original="several" />
        <character is_modifier="true" name="prominence" src="d0_s9" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o25302" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
      </biological_entity>
      <relation from="o25300" id="r3107" name="paralleled by" negation="false" src="d0_s9" to="o25301" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: bristles 6, reaching from mid tubercle to beyond tip.</text>
      <biological_entity id="o25303" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25304" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
      </biological_entity>
      <biological_entity constraint="mid" id="o25305" name="tubercle" name_original="tubercle" src="d0_s10" type="structure" />
      <biological_entity id="o25306" name="tip" name_original="tip" src="d0_s10" type="structure" />
      <relation from="o25304" id="r3108" name="reaching from" negation="false" src="d0_s10" to="o25305" />
      <relation from="o25304" id="r3109" name="to" negation="false" src="d0_s10" to="o25306" />
    </statement>
    <statement id="d0_s11">
      <text>Fruits 3 (–4) per spikelet, 2.1–2.5 mm;</text>
      <biological_entity id="o25307" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="4" />
        <character constraint="per spikelet" constraintid="o25308" name="quantity" src="d0_s11" value="3" value_original="3" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25308" name="spikelet" name_original="spikelet" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stipe and receptacle 0.2–0.3 mm, sparsely setose and setulose;</text>
      <biological_entity id="o25309" name="stipe" name_original="stipe" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="setulose" value_original="setulose" />
      </biological_entity>
      <biological_entity id="o25310" name="receptacle" name_original="receptacle" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="setulose" value_original="setulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>body glossy, brown with pale center, obovoidlenticular, 1.1–1.5 × 1–1.1 mm, surfaces finely longitudinally lined, variably low papillatecancellate, also often transversely with wavy lines of dark dots;</text>
      <biological_entity id="o25311" name="body" name_original="body" src="d0_s13" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s13" value="glossy" value_original="glossy" />
        <character constraint="with center" constraintid="o25312" is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" notes="" src="d0_s13" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s13" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25312" name="center" name_original="center" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o25313" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="finely longitudinally" name="architecture" src="d0_s13" value="lined" value_original="lined" />
        <character is_modifier="false" modifier="variably" name="position" src="d0_s13" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o25314" name="line" name_original="lines" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="wavy" value_original="wavy" />
      </biological_entity>
      <relation from="o25313" id="r3110" modifier="often transversely; transversely; of dark dots" name="with" negation="false" src="d0_s13" to="o25314" />
    </statement>
    <statement id="d0_s14">
      <text>tubercle flattened, triangularsubulate, (0.8–) 0.9–1 (–1.1) mm, setulose-ciliate.</text>
      <biological_entity id="o25315" name="tubercle" name_original="tubercle" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="0.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s14" value="setulose-ciliate" value_original="setulose-ciliate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sands and peats of bogs, stream banks, edges of pineland savanna ponds, Hypericum ponds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sands" constraint="of bogs , stream banks ," />
        <character name="habitat" value="peats" constraint="of bogs , stream banks ," />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="edges" constraint="of pineland savanna ponds , hypericum ponds" />
        <character name="habitat" value="pineland savanna ponds" />
        <character name="habitat" value="hypericum ponds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Del., Fla., Ga., Md., Miss., N.C., S.C.; Central America (Belize).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Central America (Belize)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <other_name type="past_name">Rynchospora</other_name>
  <discussion>Rhynchospora harperi is most abundant in a very special habitat referred to here as the “Hypericum pond.” These are typically shallow ponds in pine savannas, frequently ringed by stands of Nyssa, Taxodium, Ilex, and Cyrilla, but most of the pond itself is dominated by one or more myriandrous shrubby Hypericum species. Here R. harperi is distinguished from other species by the often abrupt bend of its ultimate internode.</discussion>
  
</bio:treatment>