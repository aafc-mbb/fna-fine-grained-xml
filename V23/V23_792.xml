<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles T. Bryson,Robert F. C. Naczi</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carex</taxon_name>
    <taxon_name authority="(Kunth) Mackenzie in N. L. Britton et al." date="1935" rank="section">Laxiflorae</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>18: 244. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Laxiflorae</taxon_hierarchy>
    <other_info_on_name type="fna_id">302708</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carex</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="unranked">Laxiflorae</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>2: 452. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carex;unranked Laxiflorae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually densely cespitose, short to long rhizomatous.</text>
      <biological_entity id="o27225" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually; densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="size_or_length" src="d0_s0" value="short to long" value_original="short to long" />
        <character is_modifier="false" modifier="usually; densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="size_or_length" src="d0_s0" value="short to long" value_original="short to long" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms solitary or not, brown or purple at base.</text>
      <biological_entity id="o27226" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character constraint="at base" constraintid="o27227" is_modifier="false" name="coloration" src="d0_s1" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o27227" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sheaths not fibrous;</text>
      <biological_entity id="o27228" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o27229" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath fronts membranous;</text>
      <biological_entity id="o27230" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="sheath" id="o27231" name="front" name_original="fronts" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades usually M-shaped in cross-section when young, adaxial side of blades with 2 lateral-veins more prominent than midvein, widest leaf-blades 5+ mm, glabrous.</text>
      <biological_entity id="o27232" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o27233" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o27234" is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="m--shaped" value_original="m--shaped" />
      </biological_entity>
      <biological_entity id="o27234" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o27235" name="side" name_original="side" src="d0_s4" type="structure">
        <character constraint="than midvein" constraintid="o27238" is_modifier="false" name="prominence" src="d0_s4" value="more prominent" value_original="more prominent" />
      </biological_entity>
      <biological_entity id="o27236" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <biological_entity id="o27237" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
        <character constraint="than midvein" constraintid="o27238" is_modifier="false" name="prominence" src="d0_s4" value="more prominent" value_original="more prominent" />
      </biological_entity>
      <biological_entity id="o27238" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity constraint="widest" id="o27239" name="blade-leaf" name_original="leaf-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="distance" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o27235" id="r3365" modifier="when young" name="part_of" negation="false" src="d0_s4" to="o27236" />
      <relation from="o27235" id="r3366" name="with" negation="false" src="d0_s4" to="o27237" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemose, with 3–6 spikes;</text>
      <biological_entity id="o27240" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o27241" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <relation from="o27240" id="r3367" name="with" negation="false" src="d0_s5" to="o27241" />
    </statement>
    <statement id="d0_s6">
      <text>proximal nonbasal bracts leaflike, long-sheathing, more than 4 mm, longer than diameter of stem;</text>
      <biological_entity constraint="proximal nonbasal" id="o27242" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="long-sheathing" value_original="long-sheathing" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" upper_restricted="false" />
        <character constraint="than diameter of stem" constraintid="o27243" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o27243" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character is_modifier="true" name="character" src="d0_s6" value="diameter" value_original="diameter" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral spikes pistillate, rarely basal, pedunculate, prophyllate;</text>
      <biological_entity constraint="lateral" id="o27244" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="rarely" name="position" src="d0_s7" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="prophyllate" value_original="prophyllate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>terminal spike staminate.</text>
      <biological_entity constraint="terminal" id="o27245" name="spike" name_original="spike" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Proximal pistillate scales with apex obtuse, acute, or awned.</text>
      <biological_entity constraint="proximal" id="o27246" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="awned" value_original="awned" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o27247" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o27246" id="r3368" name="with" negation="false" src="d0_s9" to="o27247" />
    </statement>
    <statement id="d0_s10">
      <text>Perigynia ascending, yellowbrown, to dark-brown when mature, not black-mottled, distinctly 8-veined or more, stipitate, obovate to narrowly obovate, trigonous to rounded-trigonous in cross-section, base tapering, apex tapering to rounded, abruptly or gradually beak, smooth, glabrous;</text>
      <biological_entity id="o27248" name="perigynium" name_original="perigynia" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s10" to="dark-brown" />
        <character char_type="range_value" from="yellowbrown" modifier="when mature" name="coloration" src="d0_s10" to="dark-brown" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s10" value="black-mottled" value_original="black-mottled" />
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s10" value="8-veined" value_original="8-veined" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s10" to="narrowly obovate trigonous" />
        <character char_type="range_value" constraint="in cross-section" constraintid="o27249" from="obovate" name="shape" src="d0_s10" to="narrowly obovate trigonous" />
      </biological_entity>
      <biological_entity id="o27249" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o27250" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o27251" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="tapering" name="shape" src="d0_s10" to="rounded" />
        <character is_modifier="false" modifier="abruptly" name="architecture_or_pubescence_or_relief" notes="" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27252" name="beak" name_original="beak" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>beak 0.1–1.8 mm, usually more than 5 mm, orifice entire.</text>
      <biological_entity id="o27253" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" modifier="usually" name="some_measurement" src="d0_s11" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o27254" name="orifice" name_original="orifice" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Stigmas 3.</text>
      <biological_entity id="o27255" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes trigonous;</text>
      <biological_entity id="o27256" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style deciduous.</text>
      <biological_entity id="o27257" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26bb.</number>
  <discussion>Species 16 (15 in the flora).</discussion>
  <discussion>Carex sect. Laxiflorae is a monophyletic section diagnosed by at least two apomorphies: acute culm angles and epidermal cells of the culms conspicously larger than underlying cells (R. F. C. Naczi 1992). Recent phylogenetic analyses indicate Carex sect. Laxiflorae is a sister group of a clade composed of Carex sects. Granulares, Careyanae, and Griseae (R. F. C. Naczi 1992; R. F. C. Naczi et al. 2002). Eight species formerly included in sect. Laxiflorae in most of the botanical literature have been segregated into sect. Careyanae.</discussion>
  <discussion>Complete, ample specimens bearing mature perigynia are necessary for identification. Magnification of 10X and bright illumination should be used when examining basal sheaths for presence or absence of purplish coloration, because the coloration is often limited to small areas at the very bases of the plants. When measuring width of the perigynium, care should be taken to measure a perigynium at its widest point, as one face of a perigynium is usually slightly wider than the other two. When measuring length of the peduncle of lateral spike, include the portion of the peduncle enclosed in the bract sheath.</discussion>
  <references>
    <reference>Bryson, C. T. 1980. A Revision of the North American Carex Sect. Laxiflorae (Cyperaceae). Ph.D. dissertation. Mississippi State University</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bract blades of distal lateral spikes lanceolate or narrowly lanceolate, wider than spikes, concealing them (viewed from abaxial surface), widest bract blade of distalmost lateral spike (2.9–)3.2–8.3 mm wide.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bract blades of distal lateral spikes linear, narrower than spikes, not concealing them (viewed from abaxial surface), widest bract blade of distalmost lateral spike 0.5–3.4 mm wide.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Widest leaf or bract blade 13–38(–62) mm wide; pistillate scales from proximal portions of spikes truncate, awnless (sometimes mucronate); perigynia 1.8–2.1 mm wide.</description>
      <determination>273 Carex albursina</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Widest leaf or bract blade 5.3–11 mm wide; pistillate scales from proximal portions of spikes acute or awned; perigynia 1.3–1.7(–1.8) mm wide.</description>
      <determination>272 Carex kraliana</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perigynia 8–18-veined, 2(–3) veins conspicuous.</description>
      <determination>263 Carex leptonervia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perigynia (22–)25–32-veined, all veins conspicuous.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Basal sheaths purple, reddish purple, or purple tinged.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Basal sheaths brownish, not purplish.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Perigynia 2.4–3.3 mm.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Perigynia 3.4–4.5 mm.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perigynia closely overlapping, 1.8–2.7 times long as wide; beaks 0.2–0.8 mm; internodes in proximalmost spikes 1.1–3.2(–4.8) mm; angles of bract sheaths denticulate.</description>
      <determination>270 Carex gracilescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perigynia loosely overlapping or separate, 1.7–2.1 times long as wide; beak 0.1–0.3 mm; internodes in proximalmost spikes 3.3–14 mm; angles of bract sheaths smooth or minutely papillose.</description>
      <determination>269 Carex ormostachya</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Longest peduncle of terminal spike (0.4–)3.1–5.4(–15.7) cm; terminal spike exceeding bract blade of distalmost lateral spike; blades of overwintered leaves densely papillose abaxially.</description>
      <determination>267 Carex purpurifera</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Longest peduncle of terminal spike (0.9–)1.2–3.2(–5.3) cm; terminal spike usually exceeded by bract blade of distalmost lateral spike; blades of over wintered leaves smooth abaxially.</description>
      <determination>268 Carex manhartii</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Perigynia closely overlapping: ratio of longer lateral spike length (in mm) to perigynia number = 0.8–1.7.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Perigynia loosely overlapping or separate: ratio of longer lateral spike length (in mm)/perigynia number = 1.9–3.4.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Perigynia 2.5–3.8(–4.1) mm, 1.5–1.9 times as long as achene bodies; beak 0.2–0.6 mm.</description>
      <determination>271 Carex blanda</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Perigynia (3.3–)3.9–5.5 mm, 1.9–2.3 times as long as achene bodies; beak 0.5–1.7 mm.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Distal lateral spikes overlapping; terminal spike exceeded by or subequal to distal lateral spike; perigynia ascending; peduncle of proximalmost spike arising in distal 1/2 of culm.</description>
      <determination>262 Carex crebriflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Distal lateral spikes separate; terminal spike clearly exceeding distal lateral spike; perigynia spreading; peduncle of proximalmost spike arising in proximal 1/3 of culm.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Plants densely cespitose, short-rhizomatous.</description>
      <determination>260 Carex styloflexa</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Plants loosely cespitose or not cespitose, long-rhizomatous.</description>
      <determination>261 Carex chapmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pistillate scales 1.9–2.3 mm wide.</description>
      <determination>259 Carex hendersonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pistillate scales 1.1–1.8 mm wide (to 2.1 mm in C. radfordii, to 2.2 in C. striatula).</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Longer peduncles of proximal lateral spikes 4.6–14 times as long as spikes they subtend; perigynia spreading.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Longer peduncles of proximal lateral spikes 1.4–3.3(–5.3) times as long as spikes they subtend; perigynia ascending.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Culms densely tufted.</description>
      <determination>260 Carex styloflexa</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Culms loosely tufted or solitary.</description>
      <determination>261 Carex chapmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Longest bract blade 5–8 cm; blades of overwintered leaves densely papillose abaxially.</description>
      <determination>266 Carex radfordii</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Longest bract blade (4.5–)6.5–15 cm; blades of overwintered leaves usually smooth, rarely, sparsely papillose abaxially.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Achenes 1.8–2.2(–3.4) mm; perigynia (2.6–)3.2–4.1(–4.6) mm; terminal spike 12–24(–34) mm.</description>
      <determination>264 Carex laxiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Achenes 2.2–2.8(–4.6) mm; perigynia (3.4–)3.9–5.1 mm; terminal spike 22–32(–36) mm.</description>
      <determination>265 Carex striatula</determination>
    </key_statement>
  </key>
</bio:treatment>