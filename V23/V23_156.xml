<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">105</other_info_on_meta>
    <other_info_on_meta type="treatment_page">104</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">eleocharis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">ELEOCHARIS</taxon_name>
    <taxon_name authority="(Nees) Bentham in G. Bentham and J. D. Hooker" date="unknown" rank="section">eleogenus</taxon_name>
    <taxon_name authority="Svenson" date="1929" rank="series">ovatae</taxon_name>
    <taxon_name authority="Hines ex A. Haines" date="2001" rank="species">aestuum</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>11: 45. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus eleocharis;subgenus eleocharis;section eleogenus;series ovatae;species aestuum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242357730</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms often spreading or declining, 3–30 cm × 0.5–1 mm.</text>
      <biological_entity id="o12258" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character name="orientation" src="d0_s0" value="declining" value_original="declining" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: apex of distal leaf-sheath obtuse, tooth to 0.2 (–0.5) mm.</text>
      <biological_entity id="o12259" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o12260" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12261" name="sheath" name_original="leaf-sheath" src="d0_s1" type="structure" />
      <biological_entity id="o12262" name="tooth" name_original="tooth" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="0.2" to_unit="mm" />
      </biological_entity>
      <relation from="o12260" id="r1510" name="part_of" negation="false" src="d0_s1" to="o12261" />
    </statement>
    <statement id="d0_s2">
      <text>Spikelets ellipsoid to ovoid, 3–10 × 2–4 mm, apex blunt to subacute;</text>
      <biological_entity id="o12263" name="spikelet" name_original="spikelets" src="d0_s2" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s2" to="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12264" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s2" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>floral scales 10–100+, 10 per mm of rachilla, colorless to pale brownish except for green midrib, 1.5–2 × 1 mm, midribs not keeled, apex rounded.</text>
      <biological_entity constraint="floral" id="o12265" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s3" to="100" upper_restricted="false" />
        <character constraint="per mm" constraintid="o12266" name="quantity" src="d0_s3" value="10" value_original="10" />
        <character char_type="range_value" constraint="except-for midrib" constraintid="o12268" from="colorless" name="coloration" notes="" src="d0_s3" to="pale brownish" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" notes="" src="d0_s3" to="2" to_unit="mm" />
        <character name="width" notes="" src="d0_s3" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12266" name="mm" name_original="mm" src="d0_s3" type="structure" />
      <biological_entity id="o12267" name="rachillum" name_original="rachilla" src="d0_s3" type="structure" />
      <biological_entity id="o12268" name="midrib" name_original="midrib" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o12269" name="midrib" name_original="midribs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o12270" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o12266" id="r1511" name="part_of" negation="false" src="d0_s3" to="o12267" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: perianth bristles absent or 2–4, stramineous or whitish, shorter than to nearly 1/2 achene, very slender, without spinules;</text>
      <biological_entity id="o12271" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="perianth" id="o12272" name="bristle" name_original="bristles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="whitish" value_original="whitish" />
        <character constraint="than to nearly 1/2 achene" constraintid="o12273" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s4" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o12273" name="achene" name_original="achene" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="nearly" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o12274" name="spinule" name_original="spinules" src="d0_s4" type="structure" />
      <relation from="o12272" id="r1512" name="without" negation="false" src="d0_s4" to="o12274" />
    </statement>
    <statement id="d0_s5">
      <text>stamens 2 (–3);</text>
      <biological_entity id="o12275" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o12276" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="3" />
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers 0.3–0.5 mm;</text>
      <biological_entity id="o12277" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o12278" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>styles 2-fid or some 3-fid.</text>
      <biological_entity id="o12279" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o12280" name="style" name_original="styles" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character name="shape" src="d0_s7" value="some" value_original="some" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes 0.75–1 × 0.6–0.8 mm.</text>
      <biological_entity id="o12281" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.75" from_unit="mm" name="length" src="d0_s8" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Tubercles deltoid, 0.2–0.3 × 0.35–0.45 (–0.55) mm, 1/2–3/4as high as wide, 1/4–1/3 as high and 1/2–2/3 (–4/5) as wide as achene.</text>
      <biological_entity id="o12282" name="tubercle" name_original="tubercles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="deltoid" value_original="deltoid" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s9" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.45" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="0.55" to_unit="mm" />
        <character char_type="range_value" from="0.35" from_unit="mm" name="width" src="d0_s9" to="0.45" to_unit="mm" />
        <character constraint="as wide" is_modifier="false" name="height" src="d0_s9" value="high" value_original="high" />
        <character char_type="range_value" constraint="as high" from="1/4" name="quantity" src="d0_s9" to="1/3" />
        <character char_type="range_value" constraint="as-wide-as achene" constraintid="o12283" from="1/2" name="quantity" src="d0_s9" to="2/3-4/5" />
      </biological_entity>
      <biological_entity id="o12283" name="achene" name_original="achene" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting summer–fall (Jul–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fresh tidal river shores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fresh tidal river" />
        <character name="habitat" value="shores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn., Del., Maine, Mass., N.J., N.Y., Pa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44.</number>
  <discussion>Eleocharis aestuum is very similar to E. diandra and E. ovata; E. diandra differs from E. aestuum in its lower tubercles, brown floral scales, and absence of perianth bristles; E. ovata differs as given in the key to species. D. M. Hines (1975) described E. aestuum using the epithet “palidostachys” [ined.]. Specimens from shores of the Lake-of-the-Woods in Minnesota, and a reservoir and farm in Hardin and Fayette counties, Tennessee, may be referable to E. aestuum. The only recent records of E. aestuum are from the Androscoggin and Kennebec rivers in Maine and the Hudson River in New York (A. Haines 2001).</discussion>
  
</bio:treatment>