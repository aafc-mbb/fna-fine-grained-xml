<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/26 16:19:49</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="treatment_page">193</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cyperaceae</taxon_name>
    <taxon_name authority="Rottbøll" date="1773" rank="genus">KYLLINGA</taxon_name>
    <place_of_publication>
      <publication_title>Descr. Icon. Rar. Pl.,</publication_title>
      <place_in_publication>12, plate 4, fig. 3. 1773</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus KYLLINGA</taxon_hierarchy>
    <other_info_on_name type="etymology">for Peter Kylling, Danish botanist, d. 1696</other_info_on_name>
    <other_info_on_name type="fna_id">117357</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Cyperus</taxon_name>
    <taxon_name authority="(Rottbøll) J. V. Suringar" date="unknown" rank="subgenus">Kyllinga</taxon_name>
    <taxon_hierarchy>genus Cyperus;subgenus Kyllinga;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, cespitose or not, rhizomatous or not.</text>
      <biological_entity id="o8822" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" src="d0_s0" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="architecture" src="d0_s0" value="not" value_original="not" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms solitary or not, trigonous.</text>
      <biological_entity id="o8823" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" name="shape" src="d0_s1" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal;</text>
      <biological_entity id="o8824" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules absent;</text>
      <biological_entity id="o8825" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades flat or V-shaped in cross-section.</text>
      <biological_entity id="o8826" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character constraint="in cross-section" constraintid="o8827" is_modifier="false" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <biological_entity id="o8827" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, rarely pseudolateral, spikes 1–4, sessile, densely ovoid or cylindric;</text>
      <biological_entity id="o8828" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="rarely" name="position" src="d0_s5" value="pseudolateral" value_original="pseudolateral" />
      </biological_entity>
      <biological_entity id="o8829" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="densely" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spikelets [15–] 40–150 per spike, not readily distinguished by unaided eye;</text>
      <biological_entity id="o8830" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" name="atypical_quantity" src="d0_s6" to="40" to_inclusive="false" />
        <character char_type="range_value" constraint="per spike" constraintid="o8831" from="40" name="quantity" src="d0_s6" to="150" />
        <character constraint="by eye" constraintid="o8832" is_modifier="false" modifier="not readily" name="prominence" notes="" src="d0_s6" value="distinguished" value_original="distinguished" />
      </biological_entity>
      <biological_entity id="o8831" name="spike" name_original="spike" src="d0_s6" type="structure" />
      <biological_entity id="o8832" name="eye" name_original="eye" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>involucral-bracts 2–4, spreading or erect, leaflike.</text>
      <biological_entity id="o8833" name="involucral-bract" name_original="involucral-bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="4" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets: scales 2 (–3), distichous;</text>
      <biological_entity id="o8834" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o8835" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="3" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="distichous" value_original="distichous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>proximal scale subtending bisexual flower;</text>
      <biological_entity id="o8836" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity constraint="proximal" id="o8837" name="scale" name_original="scale" src="d0_s9" type="structure" />
      <biological_entity constraint="subtending" id="o8838" name="flower" name_original="flower" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>distal scale empty or subtending 1–2 stamens, often abortive.</text>
      <biological_entity id="o8839" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity constraint="distal" id="o8840" name="scale" name_original="scale" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="empty" value_original="empty" />
        <character is_modifier="false" name="position" src="d0_s10" value="subtending" value_original="subtending" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="2" />
        <character is_modifier="false" modifier="often" name="development" notes="" src="d0_s10" value="abortive" value_original="abortive" />
      </biological_entity>
      <biological_entity id="o8841" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers bisexual or staminate;</text>
      <biological_entity id="o8842" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth absent;</text>
      <biological_entity id="o8843" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 1–3;</text>
      <biological_entity id="o8844" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles linear, 2-fid, base persistent.</text>
      <biological_entity id="o8845" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o8846" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes biconvex, laterally compressed.</text>
      <biological_entity id="o8847" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly tropical or warm-temperate regions worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly tropical or warm-temperate regions worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Greenhead sedge</other_name>
  <other_name type="common_name">spikesedge</other_name>
  <discussion>Species 40–45 (5 in the flora).</discussion>
  <discussion>Tropical Africa contains the greatest diversity of Kyllinga species.</discussion>
  <discussion>The genus is closely related to Cyperus and has been treated as a subgenus.</discussion>
  <references>
    <reference>Delahoussaye, A. J. and J. W. Thieret. 1967. Cyperus subgenus Kyllinga (Cyperaceae) in the continental United States. Sida 3: 128–136.</reference>
    <reference>Padhye, M. D. 1971. Studies in the Cyperaceae. III. Life history of Kyllinga brevifolia Rottb. with a brief discussion on the taxonomic position of Kyllinga. Bot. Gaz. 132: 172–179.</reference>
    <reference>Tucker, G. C. 1984. A revision of the genus Kyllinga Rottb. (Cyperaceae) in Mexico and Central America. Rhodora 86: 507–538.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Midvein of floral scales winged, laciniate; anthers 2 mm.</description>
      <determination>4 Kyllinga squamulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Midvein of floral scales not winged, ciliate or glabrous; anthers less than 2 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants widely creeping, rhizomatous.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants densely cespitose, rhizomatous or not.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Longest bract erect; style 0.6–1.2 mm.</description>
      <determination>1 Kyllinga brevifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Longest bract horizontal to slightly reflexed; style 1.8–2.2 mm.</description>
      <determination>2 Kyllinga gracillima</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants perennial; spikes whitish; achenes with whitish stipitate base.</description>
      <determination>3 Kyllinga odorata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants annual; spikes pale greenish; achenes light brown.</description>
      <determination>5 Kyllinga pumila</determination>
    </key_statement>
  </key>
</bio:treatment>