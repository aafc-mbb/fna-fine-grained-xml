<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">422</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">droseraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">drosera</taxon_name>
    <taxon_name authority="Poiret in J. Lamarck et al." date="1804" rank="species">capillaris</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>6: 299. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family droseraceae;genus drosera;species capillaris</taxon_hierarchy>
    <other_info_on_name type="fna_id">250015187</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Pink sundew</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not forming winter hibernaculae, rosettes (2–) 3–4 (–12) cm diam.;</text>
      <biological_entity id="o8301" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8302" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s0" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s0" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="diameter" src="d0_s0" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stem base not bulbous-cormose.</text>
      <biological_entity constraint="stem" id="o8303" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="bulbous-cormose" value_original="bulbous-cormose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves prostrate;</text>
      <biological_entity id="o8304" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules free from or adnate to petioles to 1 mm, then breaking into setaceous segments 3–5 mm;</text>
      <biological_entity id="o8305" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="free" value_original="free" />
        <character constraint="to petioles" constraintid="o8306" is_modifier="false" name="fusion" src="d0_s3" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8306" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8307" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="setaceous" value_original="setaceous" />
      </biological_entity>
      <relation from="o8305" id="r872" name="breaking into" negation="false" src="d0_s3" to="o8307" />
    </statement>
    <statement id="d0_s4">
      <text>petiole differentiated from blade, 0.6–4 cm, sparsely glandular-pilose;</text>
      <biological_entity id="o8308" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character constraint="from blade" constraintid="o8309" is_modifier="false" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="glandular-pilose" value_original="glandular-pilose" />
      </biological_entity>
      <biological_entity id="o8309" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade broadly spatulate to orbiculate, 0.5–1 cm × 3–5 mm, usually at least slightly longer than broad, usually shorter than petiole.</text>
      <biological_entity id="o8310" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly spatulate" name="shape" src="d0_s5" to="orbiculate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="usually at-least slightly longer than broad" />
        <character constraint="than petiole" constraintid="o8311" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o8311" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2–20-flowered;</text>
      <biological_entity id="o8312" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-20-flowered" value_original="2-20-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>scapes 4–20 (–35) cm, glabrous.</text>
      <biological_entity id="o8313" name="scape" name_original="scapes" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="35" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 10 mm diam.;</text>
      <biological_entity id="o8314" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character name="diameter" src="d0_s8" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals connate basally, oblongelliptic, 3–4 × 1–2 mm, apex obtuse, glabrous;</text>
      <biological_entity id="o8315" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8316" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals usually pink, sometimes white, obovate, 6–7 × 2–3 mm.</text>
      <biological_entity id="o8317" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 4–5 mm, longer than sepals.</text>
      <biological_entity id="o8318" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character constraint="than sepals" constraintid="o8319" is_modifier="false" name="length_or_size" src="d0_s11" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8319" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds brown, ellipsoid to oblong-ovoid, asymmetric, 0.4–0.5 mm, coarsely papillose-corrugated, 14–16-ridged.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 20.</text>
      <biological_entity id="o8320" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s12" to="oblong-ovoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s12" value="papillose-corrugated" value_original="papillose-corrugated" />
        <character is_modifier="false" name="shape" src="d0_s12" value="14-16-ridged" value_original="14-16-ridged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8321" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soil of pine flatwoods and savannas, seepage slopes, peat-sedge bogs, pocosin borders, wet, sandy ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soil" constraint="of pine flatwoods and savannas , seepage slopes , peat-sedge bogs , pocosin borders , wet , sandy ditches" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="seepage slopes" />
        <character name="habitat" value="peat-sedge bogs" />
        <character name="habitat" value="pocosin borders" />
        <character name="habitat" value="wet" />
        <character name="habitat" value="sandy ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., D.C., Fla., Ga., La., Md., Miss., N.C., S.C., Tenn., Tex., Va., W.Va.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Drosera capillaris is the most-frequently encountered species of the genus in the South in moist habitats that can support carnivorous plants, especially in fire-maintained pinelands. Plants can be quite small, or form surprisingly large and robust rosettes (to 12 cm broad) in some places along the Gulf Coast. It is disjunct from the Coastal Plain to Arkansas and Tennessee, as are several other species from coastal wetland habitats. Since the leaf blades of D. capillaris can be somewhat orbiculate, it may be confused with the much more northern D. rotundifolia, which grows more typically in sphagnum (although in its northern range it often grows on moist sand substrates), has adnate stipules, white flowers, and forms hibernaculae.</discussion>
  <discussion>Drosera capillaris is easy to grow, often behaving as an annual.</discussion>
  
</bio:treatment>