<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">145</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">pinetorum</taxon_name>
    <taxon_name authority="(Jepson) R. J. Little" date="unknown" rank="variety">grisea</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>5: 633. 2011</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species pinetorum;variety grisea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100939</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">grisea</taxon_name>
    <place_of_publication>
      <publication_title>Jepson, Fl. Calif.</publication_title>
      <place_in_publication>2: 521. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Viola;species purpurea;variety grisea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">pinetorum</taxon_name>
    <taxon_name authority="(Jepson) R. J. Little" date="unknown" rank="subspecies">grisea</taxon_name>
    <taxon_hierarchy>genus V.;species pinetorum;subspecies grisea</taxon_hierarchy>
  </taxon_identification>
  <number>46b.</number>
  <other_name type="common_name">Gray-leaved violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–7 (–9) cm, usually cespitose.</text>
      <biological_entity id="o10749" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="9" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems canescent to gray-tomentose.</text>
      <biological_entity id="o10750" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="canescent" name="pubescence" src="d0_s1" to="gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal: petiole 2.3–5.5 (–7.5) cm;</text>
      <biological_entity id="o10751" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o10752" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10753" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="2.3" from_unit="cm" name="some_measurement" src="d0_s2" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually not purple-tinted abaxially, usually linear to narrowly lanceolate, sometimes oblanceolate or obovate, 1.7–4 × 0.3–1 cm, margins irregularly serrate or lacerate, surfaces canescent, sometimes appearing gray-tomentose;</text>
      <biological_entity id="o10754" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10755" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually not; abaxially" name="coloration" src="d0_s3" value="purple-tinted" value_original="purple-tinted" />
        <character char_type="range_value" from="usually linear" name="shape" src="d0_s3" to="narrowly lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10756" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o10757" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="canescent" value_original="canescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline similar to basal except: petiole 0.9–4.4 cm, canescent;</text>
      <biological_entity constraint="cauline" id="o10758" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s4" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10759" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <biological_entity id="o10760" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.9" from_unit="cm" name="some_measurement" src="d0_s4" to="4.4" to_unit="cm" />
      </biological_entity>
      <relation from="o10758" id="r1181" name="to" negation="false" src="d0_s4" to="o10759" />
      <relation from="o10759" id="r1182" name="except" negation="false" src="d0_s4" to="o10760" />
    </statement>
    <statement id="d0_s5">
      <text>blade 1.5–4 × 0.3–0.5 (–0.9) cm.</text>
      <biological_entity constraint="cauline" id="o10761" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o10762" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity id="o10763" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="0.9" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s5" to="0.5" to_unit="cm" />
      </biological_entity>
      <relation from="o10761" id="r1183" name="to" negation="false" src="d0_s5" to="o10762" />
      <relation from="o10762" id="r1184" name="except" negation="false" src="d0_s5" to="o10763" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 2.9–6 (–7) cm.</text>
      <biological_entity id="o10764" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="2.9" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: lowest petal 5–9 mm.</text>
      <biological_entity id="o10765" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="lowest" id="o10766" name="petal" name_original="petal" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane slopes and peaks, often in moist, eroding soil, alpine zones</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane slopes" />
        <character name="habitat" value="peaks" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="zones" modifier="alpine" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>