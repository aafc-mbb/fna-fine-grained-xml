<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>R. John Little,Landon E. McKinney†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">106</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">VIOLACEAE</taxon_name>
    <taxon_hierarchy>family VIOLACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10944</other_info_on_name>
  </taxon_identification>
  <number>0</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, [subshrubs, shrubs, lianas, and trees], glabrous or hairy, hairs simple;</text>
      <biological_entity id="o9722" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taprooted or rhizomatous, sometimes stoloniferous.</text>
      <biological_entity id="o9723" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 0–20, prostrate to erect.</text>
      <biological_entity id="o9724" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="20" />
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline or basal, (attached directly to rhizome, some Viola), alternate (and opposite in Hybanthus [and other genera]), simple or compound, stipulate [estipulate], petiolate or sessile;</text>
      <biological_entity id="o9725" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="stipulate" value_original="stipulate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="stipulate" value_original="stipulate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="stipulate" value_original="stipulate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade unlobed or lobed.</text>
      <biological_entity id="o9726" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1 (–4) [–5] -flowered, axillary from leaf-axils or scapose from rhizomes or stolons (or in racemes of umbels), pedunculate;</text>
      <biological_entity id="o9727" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1(-4)[-5]-flowered" value_original="1(-4)[-5]-flowered" />
        <character constraint="from rhizomes, stolons" constraintid="o9729, o9730" is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity id="o9728" name="leaf-axil" name_original="leaf-axils" src="d0_s5" type="structure" />
      <biological_entity id="o9729" name="rhizome" name_original="rhizomes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="scapose" value_original="scapose" />
      </biological_entity>
      <biological_entity id="o9730" name="stolon" name_original="stolons" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracteoles usually present on peduncles, usually alternate.</text>
      <biological_entity id="o9731" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character constraint="on peduncles" constraintid="o9732" is_modifier="false" modifier="usually" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="usually" name="arrangement" notes="" src="d0_s6" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o9732" name="peduncle" name_original="peduncles" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual [unisexual, plants dioecious], perianth and unequal, imbricate in bud [convolute], lowermost petal often larger with gibbous or elongated spur;</text>
      <biological_entity id="o9733" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o9734" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character constraint="in bud" constraintid="o9735" is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o9735" name="bud" name_original="bud" src="d0_s7" type="structure" />
      <biological_entity constraint="lowermost" id="o9736" name="petal" name_original="petal" src="d0_s7" type="structure">
        <character constraint="with spur" constraintid="o9737" is_modifier="false" modifier="often" name="size" src="d0_s7" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o9737" name="spur" name_original="spur" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="gibbous" value_original="gibbous" />
        <character is_modifier="true" name="length" src="d0_s7" value="elongated" value_original="elongated" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 5, alternate with petals, surrounding ovary, connivent or syngenesious;</text>
      <biological_entity id="o9738" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character constraint="with petals" constraintid="o9739" is_modifier="false" name="arrangement" src="d0_s8" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o9739" name="petal" name_original="petals" src="d0_s8" type="structure" />
      <biological_entity id="o9740" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s8" value="surrounding" value_original="surrounding" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="syngenesious" value_original="syngenesious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 0–1 mm, filaments of 2 anterior stamens often with nectaries protruding into spur, anther dehiscence by longitudinal slits;</text>
      <biological_entity id="o9741" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9742" name="filament" name_original="filaments" src="d0_s9" type="structure" />
      <biological_entity constraint="anterior" id="o9743" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9744" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character constraint="into spur" constraintid="o9745" is_modifier="false" name="prominence" src="d0_s9" value="protruding" value_original="protruding" />
      </biological_entity>
      <biological_entity id="o9745" name="spur" name_original="spur" src="d0_s9" type="structure" />
      <biological_entity id="o9746" name="anther" name_original="anther" src="d0_s9" type="structure" />
      <biological_entity id="o9747" name="slit" name_original="slits" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o9742" id="r1039" name="consist_of" negation="false" src="d0_s9" to="o9743" />
      <relation from="o9743" id="r1040" name="with" negation="false" src="d0_s9" to="o9744" />
      <relation from="o9746" id="r1041" name="by" negation="false" src="d0_s9" to="o9747" />
    </statement>
    <statement id="d0_s10">
      <text>pistil 1, [2–] 3 [–5] -carpellate;</text>
      <biological_entity id="o9748" name="pistil" name_original="pistil" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="[2-]3[-5]-carpellate" value_original="[2-]3[-5]-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary superior, 1-locular;</text>
    </statement>
    <statement id="d0_s12">
      <text>placentation parietal;</text>
      <biological_entity id="o9749" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="placentation" src="d0_s12" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules [1–2] 8–75, anatropous, bitegmic, crassinucellate;</text>
      <biological_entity id="o9750" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s13" to="2" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s13" to="75" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="bitegmic" value_original="bitegmic" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="crassinucellate" value_original="crassinucellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style [0–] 1, usually enlarged distally, solid or hollow;</text>
      <biological_entity id="o9751" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s14" to="1" to_inclusive="false" />
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="false" modifier="usually; distally" name="size" src="d0_s14" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="solid" value_original="solid" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma 1 [3–5], with or without hairs.</text>
      <biological_entity id="o9752" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s15" to="5" />
      </biological_entity>
      <biological_entity id="o9753" name="hair" name_original="hairs" src="d0_s15" type="structure" />
      <relation from="o9752" id="r1042" name="with or without" negation="false" src="d0_s15" to="o9753" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsular [berry, nut], 3-valved, dehiscence loculicidal.</text>
      <biological_entity id="o9754" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-valved" value_original="3-valved" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds [1–] (3–) 6–75, hard, embryo not developed at time of dispersal, spheroid or ovoid [strongly flattened], glabrous [hairy], some arillate, some with elaiosome [seeds winged in some woody vines].</text>
      <biological_entity id="o9755" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s17" to="#6" to_inclusive="false" />
        <character char_type="range_value" from="#6" name="quantity" src="d0_s17" to="75" />
        <character is_modifier="false" name="texture" src="d0_s17" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o9756" name="embryo" name_original="embryo" src="d0_s17" type="structure" />
      <biological_entity id="o9757" name="time" name_original="time" src="d0_s17" type="structure" />
      <biological_entity id="o9758" name="elaiosome" name_original="elaiosome" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="spheroid" value_original="spheroid" />
        <character is_modifier="true" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="true" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="arillate" value_original="arillate" />
      </biological_entity>
      <relation from="o9756" id="r1043" name="developed at" negation="true" src="d0_s17" to="o9757" />
      <relation from="o9756" id="r1044" name="developed at" negation="false" src="d0_s17" to="o9758" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 23, species 1000–1100 (2 genera, 78 species in the flora).</discussion>
  <discussion>The Violaceae is predominantly tropical with worldwide distribution. Most genera are monotypic or oligotypic and are restricted to the New World or Old World tropics (H. E. Ballard et al. 1998; G. A. Wahlert et al. 2014). Except for Viola, Hybanthus, and Rinorea, which together account for 98% of all species in the family, most genera are limited to one continent or island system (M. Feng 2005).</discussion>
  <discussion>Violaceae has been placed in the Violales by most authors (A. Cronquist 1981; R. F. Thorne 1992; A. L. Takhtajan 1997). Based on data from cladistic analyses, it was included in the Malpighiales in 1998 (Angiosperm Phylogeny Group 1998, 2003, 2009).</discussion>
  <discussion>The Malpighiales clade was first identified by M. W. Chase et al. (1993) in a phylogenetic analysis of nucleotide sequences from the plastid gene rbcL (K. J. Wurdack and C. C. Davis 2009). Currently, 35 families are included in Malpighiales (Angiosperm Phylogeny Group 2009). Molecular studies employing multiple gene regions have confirmed the monophyly of Malpighiales, which includes about 16,000 species (Wurdack and Davis). Relationships within Malpighiales remain poorly understood and it is the most poorly resolved large rosid clade (Wurdack and Davis).</discussion>
  <discussion>Violaceae were previously organized into three subfamilies, Fusispermoideae, Leonioideae, and Violoideae (W. H. A. Hekking 1988; S. A. Hodges et al. 1995). Evidence confirms that Fusispermum is basal in Violaceae and belongs in the monotypic subfamily Fusispermoideae (M. Feng 2005; T. Tokuoka 2008) and Leonioideae should be subsumed in Violoideae (Feng; Feng and H. E. Ballard 2005; Tokuoka). All genera in Violaceae except Fusispermum are currently included in the subfamily Violoideae. Usually described as having an actinomorphic corolla, the calyx and corolla of Fusispermum were reported to actually be weakly zygomorphic (G. A. Wahlert et al. 2014).</discussion>
  <discussion>W. H. A. Hekking (1988) divided subfamily Violoideae into two tribes, Violeae and Rinoreeae. Viola and Hybanthus, the only two genera in the flora area, are placed in the Violeae.</discussion>
  <discussion>In a study of Violaceae based on plastid and nuclear DNA sequences (rbcL, atpB, matK, and 18s rDNA), T. Tokuoka (2008) found that monophyly of the family is strongly supported. A study of 39 species of Viola occurring primarily in China using chloroplast sequences trnL-trnF, psbA-trnH, rpL16, and ITS showed that “subgenus” Viola is not monophyletic (Liang G. X. and Xing F. W. 2010). Their data imply that 1) erect stems may be more primitive than stolons or rosettes, 2) species with stigmatic beaks might have been trends in sections Trigonocarpae and Adnatae, respectively.</discussion>
  <discussion>A study of Violaceae based on plastid DNA sequences showed that most intrafamilial taxa from previous classifications of Violaceae were not supported, that previously unsuspected generic affinities were revealed, and that reliance on floral symmetry (that is, actinomorphy versus zygomorphy) alone provides misleading inferences of relationships and heterogeneous generic circumscriptions (G. A. Wahlert et al. 2014).</discussion>
  <references>
    <reference>Ballard, H. E., J. de Paula-Souza, and G. A. Wahlert. 2014. Violaceae. In: The Families and Genera of Vascular Plants. Vol. 11, pp. 303–322. Berlin, Heidelberg.</reference>
    <reference>Brainerd, E. 1921. Violets of North America. Bull. Vermont Agric. Exp. Sta. 224.</reference>
    <reference>Brizicky, G. K. 1961b. The genera of Violaceae in the southeastern United States. J. Arnold Arbor. 42: 321–333.</reference>
    <reference>Feng, M. 2005. Floral Morphogenesis and Molecular Systematics of the Family Violaceae. Ph.D. dissertation. Ohio University.</reference>
    <reference>Feng, M. and H. E. Ballard. 2005. Molecular systematic, floral developmental and anatomical revelations on generic relationships and evolutionary patterns in the Violaceae. In: International Botanical Congress. 2005. XVII International Botanical Congress, Vienna, Austria, Europe, Austria Center Vienna, 17–23 July 2005. Abstracts. P. 169.</reference>
    <reference>Gershoy, A. 1928. Studies in North American violets. I. General considerations. Bull. Vermont Agric. Exp. Sta. 279.</reference>
    <reference>Hodges, S. A. et al. 1995. Generic relationships in the Violaceae: Data from morphology, anatomy, chromosome numbers and rbcL sequences. [Abstract.] Amer. J. Bot. 82(6, suppl.): 136.</reference>
    <reference>McKinney, L. E. and N. H. Russell. 2002. Violaceae of the southeastern United States. Castanea 4: 369–379.</reference>
    <reference>Tokuoka, T. 2008. Molecular phylogenetic analysis of Violaceae (Malpighiales) based on plastid and nuclear DNA sequences. J. Pl. Res. 121: 253–260.</reference>
    <reference>Wahlert, G. A. et al. 2014. Phylogeny of the Violaceae (Malpighiales) inferred from plastid DNA sequences: Implications for generic diversity and intrafamilial classification. Syst. Bot. 39: 239–252.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants caulescent; sepals not auriculate; upper 2 and lateral 2 petals not showy, 0.5–5 mm; lowest petal showy, narrowed at middle; stamens connate, lowest 2 filaments not spurred with nectary; seeds (3–)6–9.</description>
      <determination>1 Hybanthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants caulescent or acaulescent; sepals auriculate; upper 2 and lateral 2 petals showy, 5+ mm; lowest petal showy, not narrowed at middle; stamens connivent, but distinct, lower 2 filaments spurred with nectary that protrudes into petal spur; seeds 6–75.</description>
      <determination>2 Viola</determination>
    </key_statement>
  </key>
</bio:treatment>