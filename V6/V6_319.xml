<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu ex Roussel" date="unknown" rank="family">passifloraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">passiflora</taxon_name>
    <taxon_name authority="(Killip) D. H. Goldman" date="2004" rank="species">arizonica</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>50: 249. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family passifloraceae;genus passiflora;species arizonica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100985</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Passiflora</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">foetida</taxon_name>
    <taxon_name authority="Killip" date="unknown" rank="variety">arizonica</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Field Mus. Nat. Hist., Bot. Ser.</publication_title>
      <place_in_publication>19: 490. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Passiflora;species foetida;variety arizonica</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems terete, densely hairy.</text>
      <biological_entity id="o2901" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves pungent, densely hairy, glandular-ciliate;</text>
      <biological_entity id="o2902" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="odor_or_shape" src="d0_s1" value="pungent" value_original="pungent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-ciliate" value_original="glandular-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules pectinate, 1–8 × 1–4 mm, with glandular bristles or hairs;</text>
      <biological_entity id="o2903" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="pectinate" value_original="pectinate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o2904" name="bristle" name_original="bristles" src="d0_s2" type="structure" />
      <biological_entity id="o2905" name="hair" name_original="hairs" src="d0_s2" type="structure" />
      <relation from="o2903" id="r357" name="with" negation="false" src="d0_s2" to="o2904" />
      <relation from="o2903" id="r358" name="with" negation="false" src="d0_s2" to="o2905" />
    </statement>
    <statement id="d0_s3">
      <text>petiole with glandular bristles or hairs;</text>
      <biological_entity id="o2906" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity constraint="glandular" id="o2907" name="bristle" name_original="bristles" src="d0_s3" type="structure" />
      <biological_entity id="o2908" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <relation from="o2906" id="r359" name="with" negation="false" src="d0_s3" to="o2907" />
      <relation from="o2906" id="r360" name="with" negation="false" src="d0_s3" to="o2908" />
    </statement>
    <statement id="d0_s4">
      <text>blade roughly symmetric, 1.5–5 × 1–7 cm, moderately to deeply 3–5-lobed, middle lobe as long as or longer than lateral lobes, margins sharply dentate;</text>
      <biological_entity id="o2909" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="roughly" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="moderately to deeply" name="shape" src="d0_s4" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
      <biological_entity constraint="middle" id="o2910" name="lobe" name_original="lobe" src="d0_s4" type="structure">
        <character constraint="than lateral lobes" constraintid="o2911" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o2911" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o2912" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial fine veins weakly to moderately raised, abaxial nectaries absent.</text>
      <biological_entity constraint="abaxial" id="o2913" name="nectary" name_original="nectaries" src="d0_s5" type="structure" />
      <biological_entity id="o2914" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="fine" value_original="fine" />
        <character is_modifier="false" modifier="weakly to moderately" name="prominence" src="d0_s5" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2915" name="nectary" name_original="nectaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Floral bracts pinnatifid, 15–35 × 10–28 mm, margins sharply dentate, with glandular bristles or hairs.</text>
      <biological_entity constraint="floral" id="o2916" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s6" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2917" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o2918" name="bristle" name_original="bristles" src="d0_s6" type="structure" />
      <biological_entity id="o2919" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <relation from="o2917" id="r361" name="with" negation="false" src="d0_s6" to="o2918" />
      <relation from="o2917" id="r362" name="with" negation="false" src="d0_s6" to="o2919" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: floral-tube cuplike, 5–7 mm deep;</text>
      <biological_entity id="o2920" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2921" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuplike" value_original="cuplike" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals white, 17–38 × 6–9 mm;</text>
      <biological_entity id="o2922" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2923" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s8" to="38" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white, 16–30 × 6–12 mm;</text>
      <biological_entity id="o2924" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2925" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s9" to="30" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corona filament whorls 5 or 6, outer 2 whorls white basally, pale-purple apically, linear, terete to transversely compressed, 9–25 mm.</text>
      <biological_entity id="o2926" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="filament" id="o2927" name="whorl" name_original="whorls" src="d0_s10" type="structure" constraint_original="corona filament">
        <character name="quantity" src="d0_s10" unit="or" value="5" value_original="5" />
        <character name="quantity" src="d0_s10" unit="or" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s10" value="outer" value_original="outer" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o2928" name="whorl" name_original="whorls" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="apically" name="coloration" src="d0_s10" value="pale-purple" value_original="pale-purple" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s10" to="transversely compressed" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Berries green to yellow-green, ovoid, 20–35 × 18–30 mm.</text>
      <biological_entity id="o2929" name="berry" name_original="berries" src="d0_s11" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s11" to="yellow-green" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s11" to="35" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="width" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, igneous slopes in semidesert grasslands and oak savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="igneous slopes" modifier="rocky" constraint="in semidesert grasslands and oak savannas" />
        <character name="habitat" value="semidesert grasslands" />
        <character name="habitat" value="oak savannas" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Passiflora arizonica is known only from portions of Pima and Santa Cruz counties, Arizona, and eastern Sonora, and has been confused with P. arida (D. H. Goldman 2003). It flowers during the summer rainy season, usually August and September. Unlike most other members of the genus, particularly those of sect. Dysosmia, to which it belongs, P. arizonica flowers in the evening, closing around midnight. The fragrant flowers have a deep floral cup, and may be pollinated by nocturnal moths.</discussion>
  
</bio:treatment>