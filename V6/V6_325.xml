<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">179</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu ex Roussel" date="unknown" rank="family">passifloraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">passiflora</taxon_name>
    <taxon_name authority="Engelmann" date="1850" rank="species">affinis</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6.: 233. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family passifloraceae;genus passiflora;species affinis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100988</other_info_on_name>
  </taxon_identification>
  <number>12.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems terete, glabrous, minutely puberulent to scabrous when young.</text>
      <biological_entity id="o1970" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="terete" value_original="terete" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="minutely puberulent" modifier="when young" name="pubescence" src="d0_s0" to="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves not pungent, glabrous or minutely puberulent;</text>
      <biological_entity id="o1971" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="odor_or_shape" src="d0_s1" value="pungent" value_original="pungent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules linear-setaceous, 1–2 × 0.5 mm, eglandular;</text>
      <biological_entity id="o1972" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-setaceous" value_original="linear-setaceous" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s2" to="2" to_unit="mm" />
        <character name="width" src="d0_s2" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole eglandular;</text>
      <biological_entity id="o1973" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade roughly symmetric, 1–8 (–10) × 1.5–10 (–14) cm, shallowly to deeply 3 (–5) -lobed, middle lobe as long as or longer than lateral lobes, margins entire;</text>
      <biological_entity id="o1974" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="roughly" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="shallowly to deeply" name="shape" src="d0_s4" value="3(-5)-lobed" value_original="3(-5)-lobed" />
      </biological_entity>
      <biological_entity constraint="middle" id="o1975" name="lobe" name_original="lobe" src="d0_s4" type="structure">
        <character constraint="than lateral lobes" constraintid="o1976" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o1976" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o1977" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial fine veins weakly to moderately raised, abaxial nectaries circular, usually in 2 short lines or also scattered near leaf margins.</text>
      <biological_entity constraint="abaxial" id="o1978" name="nectary" name_original="nectaries" src="d0_s5" type="structure" />
      <biological_entity id="o1979" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="fine" value_original="fine" />
        <character is_modifier="false" modifier="weakly to moderately" name="prominence" src="d0_s5" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1980" name="nectary" name_original="nectaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="circular" value_original="circular" />
      </biological_entity>
      <biological_entity id="o1981" name="line" name_original="lines" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o1982" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o1980" id="r239" modifier="usually" name="in" negation="false" src="d0_s5" to="o1981" />
      <relation from="o1980" id="r240" modifier="usually" name="in" negation="false" src="d0_s5" to="o1982" />
    </statement>
    <statement id="d0_s6">
      <text>Floral bracts linear-subulate, 1–3 × 0.5 mm, margins entire, eglandular.</text>
      <biological_entity constraint="floral" id="o1983" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="3" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o1984" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: floral-tube absent;</text>
      <biological_entity id="o1985" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1986" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals pale green to white, 10–16 × 2–4 mm;</text>
      <biological_entity id="o1987" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1988" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s8" to="white" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="16" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals pale green to white, 6–13 × 1–2 mm;</text>
      <biological_entity id="o1989" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1990" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s9" to="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corona filament whorls 2, outer filaments purple basally, white medially, green apically, linear-filiform, terete, apically clavate, 9–18 mm.</text>
      <biological_entity id="o1991" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="filament" id="o1992" name="whorl" name_original="whorls" src="d0_s10" type="structure" constraint_original="corona filament">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="outer" id="o1993" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="medially" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="apically" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-filiform" value_original="linear-filiform" />
        <character is_modifier="false" name="shape" src="d0_s10" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Berries purple-black, globose to ovoid, 10–15 × 10 mm.</text>
      <biological_entity id="o1994" name="berry" name_original="berries" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple-black" value_original="purple-black" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s11" to="ovoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="15" to_unit="mm" />
        <character name="width" src="d0_s11" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oak-juniper woodlands, shrublands, and savannas, in moist to dry, loamy soil over limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak-juniper woodlands" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="dry" constraint="over limestone" />
        <character name="habitat" value="loamy soil" constraint="over limestone" />
        <character name="habitat" value="limestone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Passiflora affinis is similar to P. lutea in leaf shape and flower appearance. However, P. affinis has nectaries on the abaxial surface of the leaves, which are absent in P. lutea. Passiflora affinis also has floral bracts, and flowers greater than 25 mm in diameter with clavate to capitate, sinuous outer corona filaments more than 10 mm long. Passiflora lutea lacks floral bracts and has flowers less than 25 mm in diameter with apically unornamented, typically straight outer corona filaments that are usually less than 10 mm long.</discussion>
  <discussion>In the flora area Passiflora affinis is restricted to central Texas, where its leaves are often appropriately shaped like cowboy hats. E. P. Killip (1938) erroneously suggested that it is native to New Mexico, based upon the misinterpretation of herbarium label data (D. H. Goldman 2004).</discussion>
  
</bio:treatment>