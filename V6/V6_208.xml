<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">126</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_name authority="(Greene) C. L. Hitchcock in C. L. Hitchcock et al." date="unknown" rank="variety">rugulosa</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>3: 442. 1961</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species canadensis;variety rugulosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100905</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">rugulosa</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>5: 26. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Viola;species rugulosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lophion</taxon_name>
    <taxon_name authority="(Greene) Lunell" date="unknown" rank="species">rugulosum</taxon_name>
    <taxon_hierarchy>genus Lophion;species rugulosum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">discurrens</taxon_name>
    <taxon_hierarchy>genus V.;species discurrens</taxon_hierarchy>
  </taxon_identification>
  <number>10b.</number>
  <other_name type="common_name">Tall white or rugose violet</other_name>
  <other_name type="common_name">violette de l’Ouest</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming colonies, 10–40 (–60) cm.</text>
      <biological_entity id="o11793" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes branched.</text>
      <biological_entity id="o11794" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–3 (–4).</text>
      <biological_entity id="o11795" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal: petiole 4.1–23 cm;</text>
      <biological_entity id="o11796" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o11797" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11798" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="4.1" from_unit="cm" name="some_measurement" src="d0_s3" to="23" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly ovate to ovate-reniform, 1.9–12.4 × 2.1–11.1 (–12.3) cm, base cordate, margins crenate, sometimes serrulate proximally;</text>
      <biological_entity id="o11799" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o11800" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s4" to="ovate-reniform" />
        <character char_type="range_value" from="1.9" from_unit="cm" name="length" src="d0_s4" to="12.4" to_unit="cm" />
        <character char_type="range_value" from="11.1" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="12.3" to_unit="cm" />
        <character char_type="range_value" from="2.1" from_unit="cm" name="width" src="d0_s4" to="11.1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11801" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o11802" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="sometimes; proximally" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline: stipules lanceolate to deltate, margins entire to laciniate, apex acuminate;</text>
      <biological_entity constraint="cauline" id="o11803" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity id="o11804" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="deltate" />
      </biological_entity>
      <biological_entity id="o11805" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s5" to="laciniate" />
      </biological_entity>
      <biological_entity id="o11806" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole 0.2–6.9 (–15.2) cm;</text>
      <biological_entity constraint="cauline" id="o11807" name="blade" name_original="blades" src="d0_s6" type="structure" />
      <biological_entity id="o11808" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="6.9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="15.2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s6" to="6.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade ovate, 2.9–7.7 × 1.8–7.8 cm, base cordate to ± truncate, margins crenate to ± serrulate, ciliate.</text>
      <biological_entity constraint="cauline" id="o11809" name="blade" name_original="blades" src="d0_s7" type="structure" />
      <biological_entity id="o11810" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.9" from_unit="cm" name="length" src="d0_s7" to="7.7" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s7" to="7.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11811" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s7" to="more or less truncate" />
      </biological_entity>
      <biological_entity id="o11812" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="crenate to more" value_original="crenate to more" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 1.2–5 cm.</text>
      <biological_entity id="o11813" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: lowest petal 10–20 mm;</text>
      <biological_entity id="o11814" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="lowest" id="o11815" name="petal" name_original="petal" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>cleistogamous flowers sometimes absent.</text>
      <biological_entity id="o11816" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11817" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 6–10 mm, sometimes muriculate.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 24.</text>
      <biological_entity id="o11818" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s11" value="muriculate" value_original="muriculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11819" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shady areas, woodlands, forest edges, thickets, riparian corridors</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shady areas" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="forest edges" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="riparian corridors" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask., Yukon; Alaska, Ariz., Colo., Idaho, Ill., Iowa, Minn., Mont., Nebr., N.Mex., N.Dak., Oreg., S.Dak., Utah, Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Viola discurrens is cited here in synonymy because in his original description, based on a plant from Tennessee, Greene reported that it forms somewhat extensive colonies by a system of loosely connected, slender, horizontal whitish rootstocks. Subsequent authors have referred to these structures as stoloniferous rhizomes, superficial rhizomes, runners, or stolons. Greene based his description of V. rugulosa on a plant collected in Minnesota stating that it had a thick, suberect rootstock, but did not say that it had stolons, whitish rootstalks, or stoloniferous rhizomes or that it formed colonies. It is unclear why E. Brainerd (1921) dismissed V. discurrens as an “unsatisfactory segregate” and instead assumed that colony-forming plants interconnected by subterranean, branching rhizomes should be termed V. rugulosa instead of V. discurrens. Most authors have followed Brainerd’s lead. W. J. Cody (2000) applied the name V. canadensis var. rydbergii (Greene) House to rhizomatous V. canadensis plants in Yukon; when Greene described V. rydbergii, he did not mention rhizomes as he did for V. discurrens.</discussion>
  <discussion>V. B. Baird (1942) stated that V. rugulosa lacks cleistogamous flowers; R. E. Brooks and R. L. McGregor (1986) noted that cleistogamous flowers are borne for a short time in axils of distal leaves.</discussion>
  
</bio:treatment>