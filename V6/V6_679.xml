<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">368</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. St.-Hilaire in A. St.-Hilaire et al." date="1825" rank="genus">sphaeralcea</taxon_name>
    <taxon_name authority="La Duke" date="1985" rank="species">polychroma</taxon_name>
    <place_of_publication>
      <publication_title>SouthW. Naturalist</publication_title>
      <place_in_publication>30: 433, fig. 1. 1985</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sphaeralcea;species polychroma;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101197</other_info_on_name>
  </taxon_identification>
  <number>22.</number>
  <other_name type="common_name">Hot Springs globemallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o17730" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, white or yellow, to 10–20 dm, rubbery, densely soft stellate-pubescent.</text>
      <biological_entity id="o17731" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s1" to="20" to_unit="dm" />
        <character is_modifier="false" modifier="densely" name="pubescence_or_texture" src="d0_s1" value="soft" value_original="soft" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades white or yellow, deltate to lanceolate, subhastate to 3-lobed, 4–7 cm, secondary lobes to 2.2 cm, not rugose, base cuneate, margins crenate to dentate, surfaces green to white-canescent.</text>
      <biological_entity id="o17732" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s2" to="lanceolate subhastate" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s2" to="lanceolate subhastate" />
        <character char_type="range_value" from="4" from_unit="cm" name="distance" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o17733" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="2.2" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o17734" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o17735" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s2" to="dentate" />
      </biological_entity>
      <biological_entity id="o17736" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="white-canescent" value_original="white-canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences paniculate, open, many-flowered, interrupted, tip leafy;</text>
      <biological_entity id="o17737" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="many-flowered" value_original="many-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>involucellar bractlets green to tan.</text>
      <biological_entity id="o17738" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o17739" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 6–7 mm;</text>
      <biological_entity id="o17740" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o17741" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white, pink, lavender, purple, red-orange, or red, 10–13 mm;</text>
      <biological_entity id="o17742" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o17743" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red" value_original="red" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers yellow.</text>
      <biological_entity id="o17744" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o17745" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Schizocarps short-urceolate;</text>
      <biological_entity id="o17746" name="schizocarp" name_original="schizocarps" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="short-urceolate" value_original="short-urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>mericarps 12–14, (3.5–) 4–5.5 × 2–3 mm, chartaceous, nonreticulate dehiscent part 60% of height, with usually reflexed cusps to 2 mm, indehiscent part not wider than dehiscent part.</text>
      <biological_entity id="o17747" name="mericarp" name_original="mericarps" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="14" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o17748" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o17749" name="cusp" name_original="cusps" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="usually" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17750" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
        <character constraint="than dehiscent part" constraintid="o17751" is_modifier="false" name="width" src="d0_s9" value="not wider" value_original="not wider" />
      </biological_entity>
      <biological_entity id="o17751" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <relation from="o17748" id="r1889" modifier="60%" name="with" negation="false" src="d0_s9" to="o17749" />
    </statement>
    <statement id="d0_s10">
      <text>Seeds 1 or 2 per mericarp, gray, pubescent.</text>
      <biological_entity id="o17753" name="mericarp" name_original="mericarp" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n =20.</text>
      <biological_entity id="o17752" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s10" unit="or per" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17754" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert lowlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert lowlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sphaeralcea polychroma is frequent in central New Mexico and western Texas. It is closely related to S. procera.</discussion>
  
</bio:treatment>