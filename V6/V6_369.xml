<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">204</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Byttnerioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1756" rank="genus">ayenia</taxon_name>
    <taxon_name authority="Cristóbal" date="1960" rank="species">pilosa</taxon_name>
    <place_of_publication>
      <publication_title>Opera Lilloana</publication_title>
      <place_in_publication>4: 185, fig. 65. 1960</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily byttnerioideae;genus ayenia;species pilosa;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101006</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Dwarf ayenia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, decumbent, 0.1–0.2 (–0.3) m.</text>
      <biological_entity id="o10368" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="0.3" to_unit="m" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.2" to_unit="m" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems hairy, hairs simple and retrorse, or simple, fasciculate, and stellate.</text>
      <biological_entity id="o10369" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o10370" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 0.5–1 cm;</text>
      <biological_entity id="o10371" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10372" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades of proximal leaves broadly ovate to orbiculate, 0.5–1.3 × 0.5–1.1 cm, distal ovate to oblong-ovate or oblong-lanceolate, unlobed, 0.5–2 (–3.5) × 0.4–1.4 (–1.7) cm, base cordate, margins serrate to doubly serrate, ciliate with 1+ bristles per tooth, apex acute, 3–5-veined from base, surfaces moderately to sparingly hirsute, hairs usually simple, bifurcate, and fasciculate, sometimes also stellate.</text>
      <biological_entity id="o10373" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10374" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s3" to="orbiculate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="1.3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.1" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10375" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o10376" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="oblong-ovate or oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10377" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o10378" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="serrate to doubly" value_original="serrate to doubly" />
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character constraint="with bristles" constraintid="o10379" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o10379" name="bristle" name_original="bristles" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o10380" name="tooth" name_original="tooth" src="d0_s3" type="structure" />
      <biological_entity id="o10381" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character constraint="from base" constraintid="o10382" is_modifier="false" name="architecture" src="d0_s3" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o10382" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o10383" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="moderately to sparingly" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o10384" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s3" value="bifurcate" value_original="bifurcate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_shape" src="d0_s3" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o10374" id="r1132" name="part_of" negation="false" src="d0_s3" to="o10375" />
      <relation from="o10379" id="r1133" name="per" negation="false" src="d0_s3" to="o10380" />
    </statement>
    <statement id="d0_s4">
      <text>Cymes axillary, not borne on short-shoots (brachyblasts), (1 or) 2 or 3-flowered;</text>
      <biological_entity id="o10385" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="3-flowered" value_original="3-flowered" />
      </biological_entity>
      <biological_entity id="o10386" name="short-shoot" name_original="short-shoots" src="d0_s4" type="structure" />
      <relation from="o10385" id="r1134" name="borne on" negation="true" src="d0_s4" to="o10386" />
    </statement>
    <statement id="d0_s5">
      <text>peduncle 2–3 mm.</text>
      <biological_entity id="o10387" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1.5–2 mm.</text>
      <biological_entity id="o10388" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals persistent, not reflexed at anthesis, ovate, 2 mm, sparingly stellate-hairy abaxially;</text>
      <biological_entity id="o10389" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o10390" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character constraint="at anthesis" is_modifier="false" modifier="not" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" modifier="sparingly; abaxially" name="pubescence" src="d0_s7" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petal claws 4 mm, lamina rhombic, 1 × 1 mm, base attenuate on claw, margins entire, apex notched, surfaces sparingly hairy abaxially, hairs simple, multicellular, abaxial appendage cylindric to slightly clavate, 0.5 mm;</text>
      <biological_entity id="o10391" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="petal" id="o10392" name="claw" name_original="claws" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o10393" name="lamina" name_original="lamina" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rhombic" value_original="rhombic" />
        <character name="length" src="d0_s8" unit="mm" value="1" value_original="1" />
        <character name="width" src="d0_s8" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o10394" name="base" name_original="base" src="d0_s8" type="structure">
        <character constraint="on claw" constraintid="o10395" is_modifier="false" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o10395" name="claw" name_original="claw" src="d0_s8" type="structure" />
      <biological_entity id="o10396" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10397" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity id="o10398" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparingly; abaxially" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o10399" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10400" name="appendage" name_original="appendage" src="d0_s8" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s8" to="slightly clavate" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>androgynophore 1.5 mm;</text>
      <biological_entity id="o10401" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10402" name="androgynophore" name_original="androgynophore" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamen filaments present;</text>
      <biological_entity id="o10403" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="stamen" id="o10404" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas slightly exserted.</text>
      <biological_entity id="o10405" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10406" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules subspheric, 3–3.5 × 3.5 mm, sparingly stellate-hairy, prickles 0.5 mm.</text>
      <biological_entity id="o10407" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="subspheric" value_original="subspheric" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="3.5" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="3.5" value_original="3.5" />
        <character is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s12" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o10408" name="prickle" name_original="prickles" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 2–2.5 mm, sparingly tuberculate.</text>
      <biological_entity id="o10409" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparingly" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Edges of thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="edges" constraint="of thickets" />
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Coahuila, San Luis Potosí, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leaf shape and vestiture are the principal characters used to distinguish Ayenia pilosa from A. filiformis. Where they co-occur in southwestern Texas there are intermediates in which the leaves are oblong-ovate as in A. pilosa but the vestiture is more like that of A. filiformis.</discussion>
  
</bio:treatment>