<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">18</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">22</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="genus">ECHINOCYSTIS</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 542. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus ECHINOCYSTIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek echinos, hedgehog, and kystis, bladder, alluding to prickly, hollow fruits</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">111224</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Wild or wild mock cucumber</other_name>
  <other_name type="common_name">wild balsam apple</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, monoecious, climbing or trailing;</text>
      <biological_entity id="o247" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems glabrate;</text>
    </statement>
    <statement id="d0_s2">
      <text>taprooted with branching secondary-roots or roots slender-fibrous;</text>
      <biological_entity id="o248" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character constraint="with roots" constraintid="o250" is_modifier="false" name="architecture" src="d0_s2" value="taprooted" value_original="taprooted" />
      </biological_entity>
      <biological_entity id="o249" name="secondary-root" name_original="secondary-roots" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o250" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="slender-fibrous" value_original="slender-fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tendrils 3-branched.</text>
      <biological_entity id="o251" name="tendril" name_original="tendrils" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-branched" value_original="3-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade depressed-orbiculate to suborbiculate or ovate, palmately (3–) 5 (–7) -lobed, lobes triangular to oblong-triangular, margins entire or serrulate-cuspidate, surfaces eglandular, glabrous or scabrous.</text>
      <biological_entity id="o252" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o253" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="depressed-orbiculate" name="shape" src="d0_s4" to="suborbiculate or ovate" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s4" value="(3-)5(-7)-lobed" value_original="(3-)5(-7)-lobed" />
      </biological_entity>
      <biological_entity id="o254" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s4" to="oblong-triangular" />
      </biological_entity>
      <biological_entity id="o255" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate-cuspidate" value_original="serrulate-cuspidate" />
      </biological_entity>
      <biological_entity id="o256" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: staminate flowers 50–200 in axillary racemes or racemoid panicles;</text>
      <biological_entity id="o257" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o258" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="in panicles" constraintid="o260" from="50" name="quantity" src="d0_s5" to="200" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o259" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
      <biological_entity id="o260" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>pistillate flowers 1 (–3) from same axils as staminate, peduncles erect at apex;</text>
      <biological_entity id="o261" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o262" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="3" />
        <character constraint="from axils" constraintid="o263" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o263" name="axil" name_original="axils" src="d0_s6" type="structure" />
      <biological_entity id="o264" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character constraint="at apex" constraintid="o265" is_modifier="false" modifier="as staminate" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o265" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts absent.</text>
      <biological_entity id="o266" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o267" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: hypanthium shallowly cupulate;</text>
      <biological_entity id="o268" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o269" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s8" value="cupulate" value_original="cupulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals (5–) 6, filiform, almost pricklelike;</text>
      <biological_entity id="o270" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o271" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s9" to="6" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="almost" name="architecture_or_shape" src="d0_s9" value="pricklelike" value_original="pricklelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals (5–) 6, connate 1/4 length, white to greenish white, narrowly triangular to linearlanceolate or oblong-lanceolate, 3–6 mm, minutely glandularpuberulent, corolla rotate.</text>
      <biological_entity id="o272" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o273" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s10" to="6" to_inclusive="false" />
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character name="length" src="d0_s10" value="1/4" value_original="1/4" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="greenish white" />
        <character constraint="to " constraintid="o275" is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o274" name="linearlanceolate" name_original="linearlanceolate" src="d0_s10" type="structure" />
      <biological_entity id="o275" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" constraint="to " constraintid="o277" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o276" name="linearlanceolate" name_original="linearlanceolate" src="d0_s10" type="structure" />
      <biological_entity id="o277" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="oblong-lanceolate" value_original="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o278" name="linearlanceolate" name_original="linearlanceolate" src="d0_s10" type="structure" />
      <biological_entity id="o279" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rotate" value_original="rotate" />
      </biological_entity>
      <relation from="o277" id="r34" name="to" negation="false" src="d0_s10" to="o278" />
      <relation from="o277" id="r35" name="to" negation="false" src="d0_s10" to="o279" />
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: stamens 3;</text>
      <biological_entity id="o280" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o281" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments inserted at corolla base, connate, column 0.4 mm;</text>
      <biological_entity id="o282" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o283" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o284" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o285" name="column" name_original="column" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
      <relation from="o283" id="r36" name="inserted at" negation="false" src="d0_s12" to="o284" />
    </statement>
    <statement id="d0_s13">
      <text>thecae connate, forming subsessile capitate androecium but not fused into single ring, sigmoid-flexuous, connective broad;</text>
      <biological_entity id="o286" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o287" name="theca" name_original="thecae" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character constraint="into ring" constraintid="o289" is_modifier="false" modifier="not" name="fusion" src="d0_s13" value="fused" value_original="fused" />
        <character is_modifier="false" name="course" notes="" src="d0_s13" value="sigmoid-flexuous" value_original="sigmoid-flexuous" />
      </biological_entity>
      <biological_entity id="o288" name="androecium" name_original="androecium" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="subsessile" value_original="subsessile" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s13" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o289" name="ring" name_original="ring" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o290" name="connective" name_original="connective" src="d0_s13" type="structure">
        <character is_modifier="false" name="width" src="d0_s13" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o287" id="r37" name="forming" negation="false" src="d0_s13" to="o288" />
    </statement>
    <statement id="d0_s14">
      <text>pistillodes absent.</text>
      <biological_entity id="o291" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o292" name="pistillode" name_original="pistillodes" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: ovary 2-locular, subglobose;</text>
      <biological_entity id="o293" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o294" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subglobose" value_original="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 2 per locule;</text>
      <biological_entity id="o295" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o296" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character constraint="per locule" constraintid="o297" name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o297" name="locule" name_original="locule" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 1, nearly vestigial;</text>
      <biological_entity id="o298" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o299" name="style" name_original="style" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character is_modifier="false" modifier="nearly" name="prominence" src="d0_s17" value="vestigial" value_original="vestigial" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma 3, forming a subglobose head;</text>
      <biological_entity id="o300" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o301" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o302" name="head" name_original="head" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="subglobose" value_original="subglobose" />
      </biological_entity>
      <relation from="o301" id="r38" name="forming a" negation="false" src="d0_s18" to="o302" />
    </statement>
    <statement id="d0_s19">
      <text>staminodes absent.</text>
      <biological_entity id="o303" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o304" name="staminode" name_original="staminodes" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits pepos, light green to blue-green, globose-ovoid to ovoid or ellipsoid, bladdery-inflated, symmetric, dry, thin-walled, surface moderately to densely echinate, spinules weak, flexible, whitish-glaucous with green mottling at bases, dehiscence apically irregularly lacerate, sometimes explosively.</text>
      <biological_entity constraint="fruits" id="o305" name="pepo" name_original="pepos" src="d0_s20" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s20" to="blue-green" />
        <character char_type="range_value" from="globose-ovoid" name="shape" src="d0_s20" to="ovoid or ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s20" value="bladdery-inflated" value_original="bladdery-inflated" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s20" value="dry" value_original="dry" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o306" name="surface" name_original="surface" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="architecture" src="d0_s20" value="echinate" value_original="echinate" />
      </biological_entity>
      <biological_entity id="o307" name="spinule" name_original="spinules" src="d0_s20" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s20" value="weak" value_original="weak" />
        <character is_modifier="false" name="fragility" src="d0_s20" value="pliable" value_original="flexible" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="whitish-glaucous" value_original="whitish-glaucous" />
        <character constraint="at bases" constraintid="o308" is_modifier="false" name="coloration" src="d0_s20" value="green mottling" value_original="green mottling" />
        <character is_modifier="false" modifier="apically irregularly" name="dehiscence" notes="" src="d0_s20" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o308" name="base" name_original="bases" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>Seeds 4, broadly oblong-ellipsoid, strongly flattened, not arillate, margins not differentiated, surface slightly pitted-roughened.</text>
      <biological_entity id="o309" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="4" value_original="4" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s21" value="oblong-ellipsoid" value_original="oblong-ellipsoid" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s21" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s21" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity id="o310" name="margin" name_original="margins" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s21" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>x = 32.</text>
      <biological_entity id="o311" name="surface" name_original="surface" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="slightly" name="relief_or_texture" src="d0_s21" value="pitted-roughened" value_original="pitted-roughened" />
      </biological_entity>
      <biological_entity constraint="x" id="o312" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  
</bio:treatment>